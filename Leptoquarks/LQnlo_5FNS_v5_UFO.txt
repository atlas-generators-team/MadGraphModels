Requestor: Mahsana Haleem
Contents: Scalar Leptoquark (several models, e.g S1, S1tilde, R2,..)
Paper: https://arxiv.org/abs/2108.11404 and https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.115017 and https://journals.aps.org/prd/abstract/10.1103/PhysRevD.71.057503
Source: https://www.uni-muenster.de/Physik.TP/research/kulesza/leptoquarks.html
JIRA: https://its.cern.ch/jira/projects/AGENE/issues/AGENE-2307


