# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 31 Oct 2015 20:00:39


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_102_2 = Coupling(name = 'R2GC_102_2',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_103_3 = Coupling(name = 'R2GC_103_3',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_103_4 = Coupling(name = 'R2GC_103_4',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_104_5 = Coupling(name = 'R2GC_104_5',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_104_6 = Coupling(name = 'R2GC_104_6',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_105_7 = Coupling(name = 'R2GC_105_7',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_105_8 = Coupling(name = 'R2GC_105_8',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_106_9 = Coupling(name = 'R2GC_106_9',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_106_10 = Coupling(name = 'R2GC_106_10',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_107_11 = Coupling(name = 'R2GC_107_11',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_107_12 = Coupling(name = 'R2GC_107_12',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_108_13 = Coupling(name = 'R2GC_108_13',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_108_14 = Coupling(name = 'R2GC_108_14',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_109_15 = Coupling(name = 'R2GC_109_15',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_109_16 = Coupling(name = 'R2GC_109_16',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_110_17 = Coupling(name = 'R2GC_110_17',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_110_18 = Coupling(name = 'R2GC_110_18',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_111_19 = Coupling(name = 'R2GC_111_19',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_111_20 = Coupling(name = 'R2GC_111_20',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_112_21 = Coupling(name = 'R2GC_112_21',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_113_22 = Coupling(name = 'R2GC_113_22',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_115_23 = Coupling(name = 'R2GC_115_23',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_116_24 = Coupling(name = 'R2GC_116_24',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_129_25 = Coupling(name = 'R2GC_129_25',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_129_26 = Coupling(name = 'R2GC_129_26',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_130_27 = Coupling(name = 'R2GC_130_27',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_130_28 = Coupling(name = 'R2GC_130_28',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_131_29 = Coupling(name = 'R2GC_131_29',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_131_30 = Coupling(name = 'R2GC_131_30',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_132_31 = Coupling(name = 'R2GC_132_31',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_133_32 = Coupling(name = 'R2GC_133_32',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_133_33 = Coupling(name = 'R2GC_133_33',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_135_34 = Coupling(name = 'R2GC_135_34',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_135_35 = Coupling(name = 'R2GC_135_35',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_136_36 = Coupling(name = 'R2GC_136_36',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_136_37 = Coupling(name = 'R2GC_136_37',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_137_38 = Coupling(name = 'R2GC_137_38',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_138_39 = Coupling(name = 'R2GC_138_39',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_139_40 = Coupling(name = 'R2GC_139_40',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_140_41 = Coupling(name = 'R2GC_140_41',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_146_42 = Coupling(name = 'R2GC_146_42',
                       value = '(-13*complex(0,1)*G**4)/(144.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_146_43 = Coupling(name = 'R2GC_146_43',
                       value = '(13*complex(0,1)*G**4)/(1728.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_147_44 = Coupling(name = 'R2GC_147_44',
                       value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_148_45 = Coupling(name = 'R2GC_148_45',
                       value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_149_46 = Coupling(name = 'R2GC_149_46',
                       value = '(-53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_150_47 = Coupling(name = 'R2GC_150_47',
                       value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_151_48 = Coupling(name = 'R2GC_151_48',
                       value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_49 = Coupling(name = 'R2GC_151_49',
                       value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_191_50 = Coupling(name = 'R2GC_191_50',
                       value = '-(complex(0,1)*G**2*gsbvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_192_51 = Coupling(name = 'R2GC_192_51',
                       value = '-(complex(0,1)*G**2*gscvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_193_52 = Coupling(name = 'R2GC_193_52',
                       value = '-(complex(0,1)*G**2*gsslL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_195_53 = Coupling(name = 'R2GC_195_53',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_196_54 = Coupling(name = 'R2GC_196_54',
                       value = '-(complex(0,1)*G**2*gsdvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_198_55 = Coupling(name = 'R2GC_198_55',
                       value = '-(complex(0,1)*G**2*gsulL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_199_56 = Coupling(name = 'R2GC_199_56',
                       value = '(-11*complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_199_57 = Coupling(name = 'R2GC_199_57',
                       value = '(11*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_199_58 = Coupling(name = 'R2GC_199_58',
                       value = '(-55*complex(0,1)*G**4)/(3456.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_59 = Coupling(name = 'R2GC_200_59',
                       value = '(-5*complex(0,1)*G**4)/(96.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_60 = Coupling(name = 'R2GC_200_60',
                       value = '(5*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_61 = Coupling(name = 'R2GC_200_61',
                       value = '(-25*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_229_62 = Coupling(name = 'R2GC_229_62',
                       value = '-(complex(0,1)*G**2*gsclL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_230_63 = Coupling(name = 'R2GC_230_63',
                       value = '-(complex(0,1)*G**2*gssvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_232_64 = Coupling(name = 'R2GC_232_64',
                       value = '-(complex(0,1)*G**2*gsblL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_234_65 = Coupling(name = 'R2GC_234_65',
                       value = '-(complex(0,1)*G**2*gsdlL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_236_66 = Coupling(name = 'R2GC_236_66',
                       value = '-(complex(0,1)*G**2*gsuvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_241_67 = Coupling(name = 'R2GC_241_67',
                       value = '-(complex(0,1)*G**2*gstlL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_242_68 = Coupling(name = 'R2GC_242_68',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_245_69 = Coupling(name = 'R2GC_245_69',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_246_70 = Coupling(name = 'R2GC_246_70',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_247_71 = Coupling(name = 'R2GC_247_71',
                       value = '(complex(0,1)*G**2*Msb**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_248_72 = Coupling(name = 'R2GC_248_72',
                       value = '(complex(0,1)*G**2*Msc**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_249_73 = Coupling(name = 'R2GC_249_73',
                       value = '(complex(0,1)*G**2*Msd**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_250_74 = Coupling(name = 'R2GC_250_74',
                       value = '(complex(0,1)*G**2*Mss**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_251_75 = Coupling(name = 'R2GC_251_75',
                       value = '(complex(0,1)*G**2*Mst**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_252_76 = Coupling(name = 'R2GC_252_76',
                       value = '(complex(0,1)*G**2*Msu**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_253_77 = Coupling(name = 'R2GC_253_77',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_253_78 = Coupling(name = 'R2GC_253_78',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_254_79 = Coupling(name = 'R2GC_254_79',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_255_80 = Coupling(name = 'R2GC_255_80',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_255_81 = Coupling(name = 'R2GC_255_81',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_258_82 = Coupling(name = 'R2GC_258_82',
                       value = '-G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_258_83 = Coupling(name = 'R2GC_258_83',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_261_84 = Coupling(name = 'R2GC_261_84',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_262_85 = Coupling(name = 'R2GC_262_85',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_262_86 = Coupling(name = 'R2GC_262_86',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_263_87 = Coupling(name = 'R2GC_263_87',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_264_88 = Coupling(name = 'R2GC_264_88',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_265_89 = Coupling(name = 'R2GC_265_89',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_266_90 = Coupling(name = 'R2GC_266_90',
                       value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_271_91 = Coupling(name = 'R2GC_271_91',
                       value = '-(complex(0,1)*G**2*gstvL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_273_92 = Coupling(name = 'R2GC_273_92',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_277_93 = Coupling(name = 'R2GC_277_93',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_278_94 = Coupling(name = 'R2GC_278_94',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_279_95 = Coupling(name = 'R2GC_279_95',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_280_96 = Coupling(name = 'R2GC_280_96',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_281_97 = Coupling(name = 'R2GC_281_97',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_282_98 = Coupling(name = 'R2GC_282_98',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_118_1 = Coupling(name = 'UVGC_118_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_119_2 = Coupling(name = 'UVGC_119_2',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_121_3 = Coupling(name = 'UVGC_121_3',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_122_4 = Coupling(name = 'UVGC_122_4',
                      value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_128_5 = Coupling(name = 'UVGC_128_5',
                      value = {-1:'(9*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_128_6 = Coupling(name = 'UVGC_128_6',
                      value = {-1:'-G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_129_7 = Coupling(name = 'UVGC_129_7',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_129_8 = Coupling(name = 'UVGC_129_8',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_130_9 = Coupling(name = 'UVGC_130_9',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_130_10 = Coupling(name = 'UVGC_130_10',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_132_11 = Coupling(name = 'UVGC_132_11',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_132_12 = Coupling(name = 'UVGC_132_12',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_133_13 = Coupling(name = 'UVGC_133_13',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_133_14 = Coupling(name = 'UVGC_133_14',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_134_15 = Coupling(name = 'UVGC_134_15',
                       value = {-1:'(-9*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_134_16 = Coupling(name = 'UVGC_134_16',
                       value = {-1:'G**3/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_134_17 = Coupling(name = 'UVGC_134_17',
                       value = {-1:'G**3/(96.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_135_18 = Coupling(name = 'UVGC_135_18',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_135_19 = Coupling(name = 'UVGC_135_19',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_135_20 = Coupling(name = 'UVGC_135_20',
                       value = {-1:'(complex(0,1)*G**4)/(48.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_136_21 = Coupling(name = 'UVGC_136_21',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_136_22 = Coupling(name = 'UVGC_136_22',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_137_23 = Coupling(name = 'UVGC_137_23',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_138_24 = Coupling(name = 'UVGC_138_24',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_25 = Coupling(name = 'UVGC_138_25',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_138_26 = Coupling(name = 'UVGC_138_26',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_138_27 = Coupling(name = 'UVGC_138_27',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_138_28 = Coupling(name = 'UVGC_138_28',
                       value = {-1:'( 0 if Msb else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_29 = Coupling(name = 'UVGC_138_29',
                       value = {-1:'( 0 if Msc else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_30 = Coupling(name = 'UVGC_138_30',
                       value = {-1:'( 0 if Msd else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_31 = Coupling(name = 'UVGC_138_31',
                       value = {-1:'( 0 if Mss else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_32 = Coupling(name = 'UVGC_138_32',
                       value = {-1:'( 0 if Mst else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_33 = Coupling(name = 'UVGC_138_33',
                       value = {-1:'( 0 if Msu else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_34 = Coupling(name = 'UVGC_138_34',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_138_35 = Coupling(name = 'UVGC_138_35',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_139_36 = Coupling(name = 'UVGC_139_36',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_140_37 = Coupling(name = 'UVGC_140_37',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_146_38 = Coupling(name = 'UVGC_146_38',
                       value = {-1:'(-13*complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_146_39 = Coupling(name = 'UVGC_146_39',
                       value = {-1:'(13*complex(0,1)*G**4)/(288.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_147_40 = Coupling(name = 'UVGC_147_40',
                       value = {-1:'(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_147_41 = Coupling(name = 'UVGC_147_41',
                       value = {-1:'(3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_148_42 = Coupling(name = 'UVGC_148_42',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msb else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msb else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_149_43 = Coupling(name = 'UVGC_149_43',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msb else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msb else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_44 = Coupling(name = 'UVGC_150_44',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_45 = Coupling(name = 'UVGC_150_45',
                       value = {-1:'-(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_46 = Coupling(name = 'UVGC_150_46',
                       value = {-1:'(19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_47 = Coupling(name = 'UVGC_150_47',
                       value = {-1:'(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_48 = Coupling(name = 'UVGC_150_48',
                       value = {-1:'( 0 if Msb else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_49 = Coupling(name = 'UVGC_150_49',
                       value = {-1:'( 0 if Msc else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_50 = Coupling(name = 'UVGC_150_50',
                       value = {-1:'( 0 if Msd else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_51 = Coupling(name = 'UVGC_150_51',
                       value = {-1:'( 0 if Mss else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_52 = Coupling(name = 'UVGC_150_52',
                       value = {-1:'( 0 if Mst else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_53 = Coupling(name = 'UVGC_150_53',
                       value = {-1:'( 0 if Msu else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_54 = Coupling(name = 'UVGC_150_54',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_150_55 = Coupling(name = 'UVGC_150_55',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msb else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msb else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_151_56 = Coupling(name = 'UVGC_151_56',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(24.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_57 = Coupling(name = 'UVGC_151_57',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_58 = Coupling(name = 'UVGC_151_58',
                       value = {-1:'(-7*complex(0,1)*G**4)/(16.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_59 = Coupling(name = 'UVGC_151_59',
                       value = {-1:'-(complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_60 = Coupling(name = 'UVGC_151_60',
                       value = {-1:'( 0 if Msb else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_61 = Coupling(name = 'UVGC_151_61',
                       value = {-1:'( 0 if Msc else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_62 = Coupling(name = 'UVGC_151_62',
                       value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_63 = Coupling(name = 'UVGC_151_63',
                       value = {-1:'( 0 if Mss else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_64 = Coupling(name = 'UVGC_151_64',
                       value = {-1:'( 0 if Mst else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_65 = Coupling(name = 'UVGC_151_65',
                       value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_66 = Coupling(name = 'UVGC_151_66',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(24.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_151_67 = Coupling(name = 'UVGC_151_67',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msb else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msb else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_68 = Coupling(name = 'UVGC_154_68',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msc else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msc else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_155_69 = Coupling(name = 'UVGC_155_69',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msc else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msc else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_156_70 = Coupling(name = 'UVGC_156_70',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msc else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msc else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_157_71 = Coupling(name = 'UVGC_157_71',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msc else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msc else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_160_72 = Coupling(name = 'UVGC_160_72',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_161_73 = Coupling(name = 'UVGC_161_73',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_162_74 = Coupling(name = 'UVGC_162_74',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msd else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msd else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_163_75 = Coupling(name = 'UVGC_163_75',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_166_76 = Coupling(name = 'UVGC_166_76',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Mss else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Mss else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_167_77 = Coupling(name = 'UVGC_167_77',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Mss else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Mss else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_168_78 = Coupling(name = 'UVGC_168_78',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Mss else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Mss else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_169_79 = Coupling(name = 'UVGC_169_79',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Mss else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Mss else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_172_80 = Coupling(name = 'UVGC_172_80',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Mst else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Mst else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_173_81 = Coupling(name = 'UVGC_173_81',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Mst else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Mst else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_174_82 = Coupling(name = 'UVGC_174_82',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Mst else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Mst else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_175_83 = Coupling(name = 'UVGC_175_83',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Mst else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Mst else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_178_84 = Coupling(name = 'UVGC_178_84',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_179_85 = Coupling(name = 'UVGC_179_85',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_86 = Coupling(name = 'UVGC_180_86',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msu else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msu else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_181_87 = Coupling(name = 'UVGC_181_87',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_191_88 = Coupling(name = 'UVGC_191_88',
                       value = {-1:'( -(complex(0,1)*G**2*gsbvL)/(12.*cmath.pi**2) if Msb else -(complex(0,1)*G**2*gsbvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbvL)/(144.*cmath.pi**2) if Msb else (complex(0,1)*G**2*gsbvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbvL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_191_89 = Coupling(name = 'UVGC_191_89',
                       value = {-1:'-(complex(0,1)*G**2*gsbvL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_192_90 = Coupling(name = 'UVGC_192_90',
                       value = {-1:'(complex(0,1)*G**2*gscvL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_192_91 = Coupling(name = 'UVGC_192_91',
                       value = {-1:'( -(complex(0,1)*G**2*gscvL)/(12.*cmath.pi**2) if Msc else -(complex(0,1)*G**2*gscvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscvL)/(144.*cmath.pi**2) if Msc else (complex(0,1)*G**2*gscvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscvL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_192_92 = Coupling(name = 'UVGC_192_92',
                       value = {-1:'-(complex(0,1)*G**2*gscvL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_193_93 = Coupling(name = 'UVGC_193_93',
                       value = {-1:'( -(complex(0,1)*G**2*gsslL)/(12.*cmath.pi**2) if Mss else -(complex(0,1)*G**2*gsslL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsslL)/(144.*cmath.pi**2) if Mss else (complex(0,1)*G**2*gsslL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsslL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_193_94 = Coupling(name = 'UVGC_193_94',
                       value = {-1:'-(complex(0,1)*G**2*gsslL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_194_95 = Coupling(name = 'UVGC_194_95',
                       value = {-1:'(complex(0,1)*G**2*gsslL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_195_96 = Coupling(name = 'UVGC_195_96',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_195_97 = Coupling(name = 'UVGC_195_97',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_196_98 = Coupling(name = 'UVGC_196_98',
                       value = {-1:'( -(complex(0,1)*G**2*gsdvL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsdvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdvL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsdvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdvL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_196_99 = Coupling(name = 'UVGC_196_99',
                       value = {-1:'-(complex(0,1)*G**2*gsdvL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_197_100 = Coupling(name = 'UVGC_197_100',
                        value = {-1:'(complex(0,1)*G**2*gsdvL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_198_101 = Coupling(name = 'UVGC_198_101',
                        value = {-1:'(complex(0,1)*G**2*gsulL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_198_102 = Coupling(name = 'UVGC_198_102',
                        value = {-1:'( -(complex(0,1)*G**2*gsulL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsulL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsulL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsulL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsulL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_198_103 = Coupling(name = 'UVGC_198_103',
                        value = {-1:'-(complex(0,1)*G**2*gsulL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_199_104 = Coupling(name = 'UVGC_199_104',
                        value = {-1:'(-11*complex(0,1)*G**4)/(144.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_105 = Coupling(name = 'UVGC_199_105',
                        value = {-1:'(11*complex(0,1)*G**4)/(576.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_106 = Coupling(name = 'UVGC_199_106',
                        value = {-1:'(-11*complex(0,1)*G**4)/(576.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_200_107 = Coupling(name = 'UVGC_200_107',
                        value = {-1:'(-5*complex(0,1)*G**4)/(48.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_200_108 = Coupling(name = 'UVGC_200_108',
                        value = {-1:'(5*complex(0,1)*G**4)/(192.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_200_109 = Coupling(name = 'UVGC_200_109',
                        value = {-1:'(-5*complex(0,1)*G**4)/(192.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_229_110 = Coupling(name = 'UVGC_229_110',
                        value = {-1:'( -(complex(0,1)*G**2*gsclL)/(12.*cmath.pi**2) if Msc else -(complex(0,1)*G**2*gsclL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsclL)/(144.*cmath.pi**2) if Msc else (complex(0,1)*G**2*gsclL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsclL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_229_111 = Coupling(name = 'UVGC_229_111',
                        value = {-1:'(complex(0,1)*G**2*gsclL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_229_112 = Coupling(name = 'UVGC_229_112',
                        value = {-1:'-(complex(0,1)*G**2*gsclL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_230_113 = Coupling(name = 'UVGC_230_113',
                        value = {-1:'( -(complex(0,1)*G**2*gssvL)/(12.*cmath.pi**2) if Mss else -(complex(0,1)*G**2*gssvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssvL)/(144.*cmath.pi**2) if Mss else (complex(0,1)*G**2*gssvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssvL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_230_114 = Coupling(name = 'UVGC_230_114',
                        value = {-1:'-(complex(0,1)*G**2*gssvL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_231_115 = Coupling(name = 'UVGC_231_115',
                        value = {-1:'(complex(0,1)*G**2*gssvL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_232_116 = Coupling(name = 'UVGC_232_116',
                        value = {-1:'( -(complex(0,1)*G**2*gsblL)/(12.*cmath.pi**2) if Msb else -(complex(0,1)*G**2*gsblL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsblL)/(144.*cmath.pi**2) if Msb else (complex(0,1)*G**2*gsblL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsblL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_232_117 = Coupling(name = 'UVGC_232_117',
                        value = {-1:'-(complex(0,1)*G**2*gsblL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_234_118 = Coupling(name = 'UVGC_234_118',
                        value = {-1:'( -(complex(0,1)*G**2*gsdlL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsdlL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdlL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsdlL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdlL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_234_119 = Coupling(name = 'UVGC_234_119',
                        value = {-1:'-(complex(0,1)*G**2*gsdlL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_235_120 = Coupling(name = 'UVGC_235_120',
                        value = {-1:'(complex(0,1)*G**2*gsdlL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_236_121 = Coupling(name = 'UVGC_236_121',
                        value = {-1:'( -(complex(0,1)*G**2*gsuvL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsuvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuvL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsuvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuvL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_236_122 = Coupling(name = 'UVGC_236_122',
                        value = {-1:'(complex(0,1)*G**2*gsuvL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_236_123 = Coupling(name = 'UVGC_236_123',
                        value = {-1:'-(complex(0,1)*G**2*gsuvL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_237_124 = Coupling(name = 'UVGC_237_124',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_238_125 = Coupling(name = 'UVGC_238_125',
                        value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_239_126 = Coupling(name = 'UVGC_239_126',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_240_127 = Coupling(name = 'UVGC_240_127',
                        value = {-1:'( -(complex(0,1)*G**2*gsbvL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbvL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbvL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbvL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbvL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbvL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_241_128 = Coupling(name = 'UVGC_241_128',
                        value = {-1:'( -(complex(0,1)*G**2*gstlL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gstlL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstlL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstlL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gstlL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstlL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_241_129 = Coupling(name = 'UVGC_241_129',
                        value = {-1:'( -(complex(0,1)*G**2*gstlL)/(12.*cmath.pi**2) if Mst else -(complex(0,1)*G**2*gstlL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstlL)/(144.*cmath.pi**2) if Mst else (complex(0,1)*G**2*gstlL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstlL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_241_130 = Coupling(name = 'UVGC_241_130',
                        value = {-1:'-(complex(0,1)*G**2*gstlL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_242_131 = Coupling(name = 'UVGC_242_131',
                        value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_243_132 = Coupling(name = 'UVGC_243_132',
                        value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_244_133 = Coupling(name = 'UVGC_244_133',
                        value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_245_134 = Coupling(name = 'UVGC_245_134',
                        value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_246_135 = Coupling(name = 'UVGC_246_135',
                        value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_247_136 = Coupling(name = 'UVGC_247_136',
                        value = {-1:'( (complex(0,1)*G**2*Msb**2)/(6.*cmath.pi**2) if Msb else (complex(0,1)*G**2*Msb**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msb**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msb**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msb**2*reglog(Msb/MU_R))/(2.*cmath.pi**2) if Msb else -(complex(0,1)*G**2*Msb**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msb**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_248_137 = Coupling(name = 'UVGC_248_137',
                        value = {-1:'( (complex(0,1)*G**2*Msc**2)/(6.*cmath.pi**2) if Msc else (complex(0,1)*G**2*Msc**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msc**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msc**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msc**2*reglog(Msc/MU_R))/(2.*cmath.pi**2) if Msc else -(complex(0,1)*G**2*Msc**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msc**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_249_138 = Coupling(name = 'UVGC_249_138',
                        value = {-1:'( (complex(0,1)*G**2*Msd**2)/(6.*cmath.pi**2) if Msd else (complex(0,1)*G**2*Msd**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msd**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msd**2*reglog(Msd/MU_R))/(2.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_250_139 = Coupling(name = 'UVGC_250_139',
                        value = {-1:'( (complex(0,1)*G**2*Mss**2)/(6.*cmath.pi**2) if Mss else (complex(0,1)*G**2*Mss**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Mss**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Mss**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Mss**2*reglog(Mss/MU_R))/(2.*cmath.pi**2) if Mss else -(complex(0,1)*G**2*Mss**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Mss**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_251_140 = Coupling(name = 'UVGC_251_140',
                        value = {-1:'( (complex(0,1)*G**2*Mst**2)/(6.*cmath.pi**2) if Mst else (complex(0,1)*G**2*Mst**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Mst**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Mst**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Mst**2*reglog(Mst/MU_R))/(2.*cmath.pi**2) if Mst else -(complex(0,1)*G**2*Mst**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Mst**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_252_141 = Coupling(name = 'UVGC_252_141',
                        value = {-1:'( (complex(0,1)*G**2*Msu**2)/(6.*cmath.pi**2) if Msu else (complex(0,1)*G**2*Msu**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msu**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msu**2*reglog(Msu/MU_R))/(2.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_142 = Coupling(name = 'UVGC_253_142',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':2})

UVGC_253_143 = Coupling(name = 'UVGC_253_143',
                        value = {-1:'( 0 if Msb else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_144 = Coupling(name = 'UVGC_253_144',
                        value = {-1:'( 0 if Msc else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_145 = Coupling(name = 'UVGC_253_145',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_146 = Coupling(name = 'UVGC_253_146',
                        value = {-1:'( 0 if Mss else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_147 = Coupling(name = 'UVGC_253_147',
                        value = {-1:'( 0 if Mst else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_148 = Coupling(name = 'UVGC_253_148',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_253_149 = Coupling(name = 'UVGC_253_149',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_254_150 = Coupling(name = 'UVGC_254_150',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':2})

UVGC_254_151 = Coupling(name = 'UVGC_254_151',
                        value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_152 = Coupling(name = 'UVGC_254_152',
                        value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_153 = Coupling(name = 'UVGC_254_153',
                        value = {-1:'( 0 if Msb else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_154 = Coupling(name = 'UVGC_254_154',
                        value = {-1:'( 0 if Msc else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_155 = Coupling(name = 'UVGC_254_155',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_156 = Coupling(name = 'UVGC_254_156',
                        value = {-1:'( 0 if Mss else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_157 = Coupling(name = 'UVGC_254_157',
                        value = {-1:'( 0 if Mst else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_158 = Coupling(name = 'UVGC_254_158',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_254_159 = Coupling(name = 'UVGC_254_159',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_255_160 = Coupling(name = 'UVGC_255_160',
                        value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':3})

UVGC_255_161 = Coupling(name = 'UVGC_255_161',
                        value = {-1:'-G**3/(48.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_162 = Coupling(name = 'UVGC_255_162',
                        value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_163 = Coupling(name = 'UVGC_255_163',
                        value = {-1:'( 0 if Msb else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_164 = Coupling(name = 'UVGC_255_164',
                        value = {-1:'( 0 if Msc else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_165 = Coupling(name = 'UVGC_255_165',
                        value = {-1:'( 0 if Msd else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_166 = Coupling(name = 'UVGC_255_166',
                        value = {-1:'( 0 if Mss else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_167 = Coupling(name = 'UVGC_255_167',
                        value = {-1:'( 0 if Mst else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_168 = Coupling(name = 'UVGC_255_168',
                        value = {-1:'( 0 if Msu else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_255_169 = Coupling(name = 'UVGC_255_169',
                        value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':3})

UVGC_256_170 = Coupling(name = 'UVGC_256_170',
                        value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_256_171 = Coupling(name = 'UVGC_256_171',
                        value = {-1:'G**3/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_172 = Coupling(name = 'UVGC_257_172',
                        value = {-1:'(33*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_173 = Coupling(name = 'UVGC_257_173',
                        value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_174 = Coupling(name = 'UVGC_257_174',
                        value = {-1:'( 0 if Msb else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_175 = Coupling(name = 'UVGC_257_175',
                        value = {-1:'( 0 if Msc else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_176 = Coupling(name = 'UVGC_257_176',
                        value = {-1:'( 0 if Msd else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_177 = Coupling(name = 'UVGC_257_177',
                        value = {-1:'( 0 if Mss else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_178 = Coupling(name = 'UVGC_257_178',
                        value = {-1:'( 0 if Mst else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_257_179 = Coupling(name = 'UVGC_257_179',
                        value = {-1:'( 0 if Msu else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_180 = Coupling(name = 'UVGC_258_180',
                        value = {-1:'( 0 if MB else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':3})

UVGC_258_181 = Coupling(name = 'UVGC_258_181',
                        value = {-1:'G**3/(48.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_182 = Coupling(name = 'UVGC_258_182',
                        value = {-1:'(-51*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_183 = Coupling(name = 'UVGC_258_183',
                        value = {-1:'( 0 if Msb else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_184 = Coupling(name = 'UVGC_258_184',
                        value = {-1:'( 0 if Msc else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_185 = Coupling(name = 'UVGC_258_185',
                        value = {-1:'( 0 if Msd else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_186 = Coupling(name = 'UVGC_258_186',
                        value = {-1:'( 0 if Mss else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_187 = Coupling(name = 'UVGC_258_187',
                        value = {-1:'( 0 if Mst else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_188 = Coupling(name = 'UVGC_258_188',
                        value = {-1:'( 0 if Msu else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_258_189 = Coupling(name = 'UVGC_258_189',
                        value = {-1:'( 0 if MT else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':3})

UVGC_259_190 = Coupling(name = 'UVGC_259_190',
                        value = {-1:'(-33*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_191 = Coupling(name = 'UVGC_259_191',
                        value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_192 = Coupling(name = 'UVGC_259_192',
                        value = {-1:'( 0 if Msb else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_193 = Coupling(name = 'UVGC_259_193',
                        value = {-1:'( 0 if Msc else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_194 = Coupling(name = 'UVGC_259_194',
                        value = {-1:'( 0 if Msd else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_195 = Coupling(name = 'UVGC_259_195',
                        value = {-1:'( 0 if Mss else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_196 = Coupling(name = 'UVGC_259_196',
                        value = {-1:'( 0 if Mst else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_259_197 = Coupling(name = 'UVGC_259_197',
                        value = {-1:'( 0 if Msu else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_260_198 = Coupling(name = 'UVGC_260_198',
                        value = {-1:'(-21*G**3)/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_260_199 = Coupling(name = 'UVGC_260_199',
                        value = {-1:'-G**3/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_261_200 = Coupling(name = 'UVGC_261_200',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_261_201 = Coupling(name = 'UVGC_261_201',
                        value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_202 = Coupling(name = 'UVGC_261_202',
                        value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_203 = Coupling(name = 'UVGC_261_203',
                        value = {-1:'( 0 if Msb else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_204 = Coupling(name = 'UVGC_261_204',
                        value = {-1:'( 0 if Msc else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_205 = Coupling(name = 'UVGC_261_205',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_206 = Coupling(name = 'UVGC_261_206',
                        value = {-1:'( 0 if Mss else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_207 = Coupling(name = 'UVGC_261_207',
                        value = {-1:'( 0 if Mst else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_208 = Coupling(name = 'UVGC_261_208',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_261_209 = Coupling(name = 'UVGC_261_209',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_262_210 = Coupling(name = 'UVGC_262_210',
                        value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_211 = Coupling(name = 'UVGC_262_211',
                        value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_212 = Coupling(name = 'UVGC_262_212',
                        value = {-1:'( 0 if Msb else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_213 = Coupling(name = 'UVGC_262_213',
                        value = {-1:'( 0 if Msc else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_214 = Coupling(name = 'UVGC_262_214',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_215 = Coupling(name = 'UVGC_262_215',
                        value = {-1:'( 0 if Mss else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_216 = Coupling(name = 'UVGC_262_216',
                        value = {-1:'( 0 if Mst else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_262_217 = Coupling(name = 'UVGC_262_217',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_218 = Coupling(name = 'UVGC_263_218',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_263_219 = Coupling(name = 'UVGC_263_219',
                        value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_220 = Coupling(name = 'UVGC_263_220',
                        value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_221 = Coupling(name = 'UVGC_263_221',
                        value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_222 = Coupling(name = 'UVGC_263_222',
                        value = {-1:'( 0 if Msb else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_223 = Coupling(name = 'UVGC_263_223',
                        value = {-1:'( 0 if Msc else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_224 = Coupling(name = 'UVGC_263_224',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_225 = Coupling(name = 'UVGC_263_225',
                        value = {-1:'( 0 if Mss else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_226 = Coupling(name = 'UVGC_263_226',
                        value = {-1:'( 0 if Mst else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_227 = Coupling(name = 'UVGC_263_227',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_263_228 = Coupling(name = 'UVGC_263_228',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_264_229 = Coupling(name = 'UVGC_264_229',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_264_230 = Coupling(name = 'UVGC_264_230',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_231 = Coupling(name = 'UVGC_264_231',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_232 = Coupling(name = 'UVGC_264_232',
                        value = {-1:'( 0 if Msb else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_233 = Coupling(name = 'UVGC_264_233',
                        value = {-1:'( 0 if Msc else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_234 = Coupling(name = 'UVGC_264_234',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_235 = Coupling(name = 'UVGC_264_235',
                        value = {-1:'( 0 if Mss else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_236 = Coupling(name = 'UVGC_264_236',
                        value = {-1:'( 0 if Mst else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_237 = Coupling(name = 'UVGC_264_237',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_264_238 = Coupling(name = 'UVGC_264_238',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_265_239 = Coupling(name = 'UVGC_265_239',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_265_240 = Coupling(name = 'UVGC_265_240',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_241 = Coupling(name = 'UVGC_266_241',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_266_242 = Coupling(name = 'UVGC_266_242',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_243 = Coupling(name = 'UVGC_266_243',
                        value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_244 = Coupling(name = 'UVGC_266_244',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_245 = Coupling(name = 'UVGC_266_245',
                        value = {-1:'( 0 if Msb else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msb/MU_R))/(48.*cmath.pi**2) if Msb else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_246 = Coupling(name = 'UVGC_266_246',
                        value = {-1:'( 0 if Msc else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msc/MU_R))/(48.*cmath.pi**2) if Msc else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_247 = Coupling(name = 'UVGC_266_247',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_248 = Coupling(name = 'UVGC_266_248',
                        value = {-1:'( 0 if Mss else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Mss/MU_R))/(48.*cmath.pi**2) if Mss else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_249 = Coupling(name = 'UVGC_266_249',
                        value = {-1:'( 0 if Mst else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Mst/MU_R))/(48.*cmath.pi**2) if Mst else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_250 = Coupling(name = 'UVGC_266_250',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_266_251 = Coupling(name = 'UVGC_266_251',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_267_252 = Coupling(name = 'UVGC_267_252',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_268_253 = Coupling(name = 'UVGC_268_253',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_269_254 = Coupling(name = 'UVGC_269_254',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_270_255 = Coupling(name = 'UVGC_270_255',
                        value = {-1:'( -(complex(0,1)*G**2*gsblL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gsblL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsblL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsblL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gsblL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsblL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_271_256 = Coupling(name = 'UVGC_271_256',
                        value = {-1:'( -(complex(0,1)*G**2*gstvL)/(12.*cmath.pi**2) if Mst else -(complex(0,1)*G**2*gstvL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstvL)/(144.*cmath.pi**2) if Mst else (complex(0,1)*G**2*gstvL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstvL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_271_257 = Coupling(name = 'UVGC_271_257',
                        value = {-1:'( -(complex(0,1)*G**2*gstvL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstvL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstvL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstvL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstvL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstvL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_271_258 = Coupling(name = 'UVGC_271_258',
                        value = {-1:'-(complex(0,1)*G**2*gstvL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_272_259 = Coupling(name = 'UVGC_272_259',
                        value = {0:'( (complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_273_260 = Coupling(name = 'UVGC_273_260',
                        value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2*MT)/(12.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_274_261 = Coupling(name = 'UVGC_274_261',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_274_262 = Coupling(name = 'UVGC_274_262',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_275_263 = Coupling(name = 'UVGC_275_263',
                        value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_276_264 = Coupling(name = 'UVGC_276_264',
                        value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_277_265 = Coupling(name = 'UVGC_277_265',
                        value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_277_266 = Coupling(name = 'UVGC_277_266',
                        value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_277_267 = Coupling(name = 'UVGC_277_267',
                        value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_278_268 = Coupling(name = 'UVGC_278_268',
                        value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_278_269 = Coupling(name = 'UVGC_278_269',
                        value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_278_270 = Coupling(name = 'UVGC_278_270',
                        value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_279_271 = Coupling(name = 'UVGC_279_271',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_279_272 = Coupling(name = 'UVGC_279_272',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_279_273 = Coupling(name = 'UVGC_279_273',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_280_274 = Coupling(name = 'UVGC_280_274',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_280_275 = Coupling(name = 'UVGC_280_275',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_280_276 = Coupling(name = 'UVGC_280_276',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_281_277 = Coupling(name = 'UVGC_281_277',
                        value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_282_278 = Coupling(name = 'UVGC_282_278',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

