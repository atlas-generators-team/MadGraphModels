# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 8.0 for Mac OS X x86 (64-bit) (October 5, 2011)
# Date: Fri 5 Aug 2022 15:53:21


from object_library import all_decays, Decay
import particles as P


Decay_H = Decay(name = 'Decay_H',
                particle = P.H,
                partial_widths = {(P.t,P.t__tilde__):'((3*MH**2*yt**2 - 12*MT**2*yt**2)*cmath.sqrt(MH**4 - 4*MH**2*MT**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((MH**2*ytau**2 - 4*MTA**2*ytau**2)*cmath.sqrt(MH**4 - 4*MH**2*MTA**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.W__minus__,P.W__plus__):'(((3*ee**4*vev**2)/(4.*sw**4) + (ee**4*MH**4*vev**2)/(16.*MW**4*sw**4) - (ee**4*MH**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(MH**4 - 4*MH**2*MW**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.Z,P.Z):'(((9*ee**4*vev**2)/2. + (3*ee**4*MH**4*vev**2)/(8.*MZ**4) - (3*ee**4*MH**2*vev**2)/(2.*MZ**2) + (3*cw**4*ee**4*vev**2)/(4.*sw**4) + (cw**4*ee**4*MH**4*vev**2)/(16.*MZ**4*sw**4) - (cw**4*ee**4*MH**2*vev**2)/(4.*MZ**2*sw**4) + (3*cw**2*ee**4*vev**2)/sw**2 + (cw**2*ee**4*MH**4*vev**2)/(4.*MZ**4*sw**2) - (cw**2*ee**4*MH**2*vev**2)/(MZ**2*sw**2) + (3*ee**4*sw**2*vev**2)/cw**2 + (ee**4*MH**4*sw**2*vev**2)/(4.*cw**2*MZ**4) - (ee**4*MH**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*ee**4*sw**4*vev**2)/(4.*cw**4) + (ee**4*MH**4*sw**4*vev**2)/(16.*cw**4*MZ**4) - (ee**4*MH**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_R2p23 = Decay(name = 'Decay_R2p23',
                    particle = P.R2p23,
                    partial_widths = {(P.b,P.e__plus__):'(MR2**4*yLRR21x3**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.b,P.mu__plus__):'(MR2**4*yLRR22x3**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.b,P.ta__plus__):'((MR2**2 - MTA**2)*(3*MR2**2*yLRR23x3**2 - 3*MTA**2*yLRR23x3**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.c,P.ve__tilde__):'(MR2**4*yRLR22x1**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.c,P.vm__tilde__):'(MR2**4*yRLR22x2**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.c,P.vt__tilde__):'(MR2**4*yRLR22x3**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.d,P.e__plus__):'(MR2**4*yLRR21x1**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.d,P.mu__plus__):'(MR2**4*yLRR22x1**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.d,P.ta__plus__):'((MR2**2 - MTA**2)*(3*MR2**2*yLRR23x1**2 - 3*MTA**2*yLRR23x1**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.s,P.e__plus__):'(MR2**4*yLRR21x2**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.s,P.mu__plus__):'(MR2**4*yLRR22x2**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.s,P.ta__plus__):'((MR2**2 - MTA**2)*(3*MR2**2*yLRR23x2**2 - 3*MTA**2*yLRR23x2**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.ve__tilde__):'((MR2**2 - MT**2)*(3*MR2**2*yRLR23x1**2 - 3*MT**2*yRLR23x1**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.vm__tilde__):'((MR2**2 - MT**2)*(3*MR2**2*yRLR23x2**2 - 3*MT**2*yRLR23x2**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.vt__tilde__):'((MR2**2 - MT**2)*(3*MR2**2*yRLR23x3**2 - 3*MT**2*yRLR23x3**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.ve__tilde__):'(MR2**4*yRLR21x1**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.vm__tilde__):'(MR2**4*yRLR21x2**2)/(16.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.vt__tilde__):'(MR2**4*yRLR21x3**2)/(16.*cmath.pi*abs(MR2)**3)'})

Decay_R2p53 = Decay(name = 'Decay_R2p53',
                    particle = P.R2p53,
                    partial_widths = {(P.c,P.e__plus__):'(MR2**2*(3*MR2**2*yLRR21x2**2 + 3*MR2**2*yRLR22x1**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.c,P.mu__plus__):'(MR2**2*(3*MR2**2*yLRR22x2**2 + 3*MR2**2*yRLR22x2**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.c,P.ta__plus__):'((MR2**2 - MTA**2)*(3*MR2**2*yLRR23x2**2 - 3*MTA**2*yLRR23x2**2 + 3*MR2**2*yRLR22x3**2 - 3*MTA**2*yRLR22x3**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.e__plus__):'((MR2**2 - MT**2)*(3*MR2**2*yLRR21x3**2 - 3*MT**2*yLRR21x3**2 + 3*MR2**2*yRLR23x1**2 - 3*MT**2*yRLR23x1**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.mu__plus__):'((MR2**2 - MT**2)*(3*MR2**2*yLRR22x3**2 - 3*MT**2*yLRR22x3**2 + 3*MR2**2*yRLR23x2**2 - 3*MT**2*yRLR23x2**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.t,P.ta__plus__):'((3*MR2**2*yLRR23x3**2 - 3*MT**2*yLRR23x3**2 - 3*MTA**2*yLRR23x3**2 + 12*MT*MTA*yLRR23x3*yRLR23x3 + 3*MR2**2*yRLR23x3**2 - 3*MT**2*yRLR23x3**2 - 3*MTA**2*yRLR23x3**2)*cmath.sqrt(MR2**4 - 2*MR2**2*MT**2 + MT**4 - 2*MR2**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.e__plus__):'(MR2**2*(3*MR2**2*yLRR21x1**2 + 3*MR2**2*yRLR21x1**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.mu__plus__):'(MR2**2*(3*MR2**2*yLRR22x1**2 + 3*MR2**2*yRLR21x2**2))/(48.*cmath.pi*abs(MR2)**3)',
                                      (P.u,P.ta__plus__):'((MR2**2 - MTA**2)*(3*MR2**2*yLRR23x1**2 - 3*MTA**2*yLRR23x1**2 + 3*MR2**2*yRLR21x3**2 - 3*MTA**2*yRLR21x3**2))/(48.*cmath.pi*abs(MR2)**3)'})

Decay_S1m13 = Decay(name = 'Decay_S1m13',
                    particle = P.S1m13,
                    partial_widths = {(P.b,P.ve):'(MS1**4*yLLS13x1**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.b,P.vm):'(MS1**4*yLLS13x2**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.b,P.vt):'(MS1**4*yLLS13x3**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.c,P.e__minus__):'(MS1**2*(3*MS1**2*yLLS12x1**2 + 3*MS1**2*yRRS12x1**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.c,P.mu__minus__):'(MS1**2*(3*MS1**2*yLLS12x2**2 + 3*MS1**2*yRRS12x2**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.c,P.ta__minus__):'((MS1**2 - MTA**2)*(3*MS1**2*yLLS12x3**2 - 3*MTA**2*yLLS12x3**2 + 3*MS1**2*yRRS12x3**2 - 3*MTA**2*yRRS12x3**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.d,P.ve):'(MS1**4*yLLS11x1**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.d,P.vm):'(MS1**4*yLLS11x2**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.d,P.vt):'(MS1**4*yLLS11x3**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.e__minus__,P.t):'((MS1**2 - MT**2)*(3*MS1**2*yLLS13x1**2 - 3*MT**2*yLLS13x1**2 + 3*MS1**2*yRRS13x1**2 - 3*MT**2*yRRS13x1**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.e__minus__,P.u):'(MS1**2*(3*MS1**2*yLLS11x1**2 + 3*MS1**2*yRRS11x1**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.mu__minus__,P.t):'((MS1**2 - MT**2)*(3*MS1**2*yLLS13x2**2 - 3*MT**2*yLLS13x2**2 + 3*MS1**2*yRRS13x2**2 - 3*MT**2*yRRS13x2**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.mu__minus__,P.u):'(MS1**2*(3*MS1**2*yLLS11x2**2 + 3*MS1**2*yRRS11x2**2))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.s,P.ve):'(MS1**4*yLLS12x1**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.s,P.vm):'(MS1**4*yLLS12x2**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.s,P.vt):'(MS1**4*yLLS12x3**2)/(16.*cmath.pi*abs(MS1)**3)',
                                      (P.t,P.ta__minus__):'((3*MS1**2*yLLS13x3**2 - 3*MT**2*yLLS13x3**2 - 3*MTA**2*yLLS13x3**2 - 12*MT*MTA*yLLS13x3*yRRS13x3 + 3*MS1**2*yRRS13x3**2 - 3*MT**2*yRRS13x3**2 - 3*MTA**2*yRRS13x3**2)*cmath.sqrt(MS1**4 - 2*MS1**2*MT**2 + MT**4 - 2*MS1**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(48.*cmath.pi*abs(MS1)**3)',
                                      (P.ta__minus__,P.u):'((MS1**2 - MTA**2)*(3*MS1**2*yLLS11x3**2 - 3*MTA**2*yLLS11x3**2 + 3*MS1**2*yRRS11x3**2 - 3*MTA**2*yRRS11x3**2))/(48.*cmath.pi*abs(MS1)**3)'})

Decay_t = Decay(name = 'Decay_t',
                particle = P.t,
                partial_widths = {(P.R2p23,P.ve):'((-MR2**2 + MT**2)*(-3*MR2**2*yRLR23x1**2 + 3*MT**2*yRLR23x1**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.R2p23,P.vm):'((-MR2**2 + MT**2)*(-3*MR2**2*yRLR23x2**2 + 3*MT**2*yRLR23x2**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.R2p23,P.vt):'((-MR2**2 + MT**2)*(-3*MR2**2*yRLR23x3**2 + 3*MT**2*yRLR23x3**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.R2p53,P.e__minus__):'((-MR2**2 + MT**2)*(-3*MR2**2*yLRR21x3**2 + 3*MT**2*yLRR21x3**2 - 3*MR2**2*yRLR23x1**2 + 3*MT**2*yRLR23x1**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.R2p53,P.mu__minus__):'((-MR2**2 + MT**2)*(-3*MR2**2*yLRR22x3**2 + 3*MT**2*yLRR22x3**2 - 3*MR2**2*yRLR23x2**2 + 3*MT**2*yRLR23x2**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.R2p53,P.ta__minus__):'((-3*MR2**2*yLRR23x3**2 + 3*MT**2*yLRR23x3**2 + 3*MTA**2*yLRR23x3**2 - 12*MT*MTA*yLRR23x3*yRLR23x3 - 3*MR2**2*yRLR23x3**2 + 3*MT**2*yRLR23x3**2 + 3*MTA**2*yRLR23x3**2)*cmath.sqrt(MR2**4 - 2*MR2**2*MT**2 + MT**4 - 2*MR2**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.S1m13,P.e__plus__):'((-MS1**2 + MT**2)*(-3*MS1**2*yLLS13x1**2 + 3*MT**2*yLLS13x1**2 - 3*MS1**2*yRRS13x1**2 + 3*MT**2*yRRS13x1**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.S1m13,P.mu__plus__):'((-MS1**2 + MT**2)*(-3*MS1**2*yLLS13x2**2 + 3*MT**2*yLLS13x2**2 - 3*MS1**2*yRRS13x2**2 + 3*MT**2*yRRS13x2**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.S1m13,P.ta__plus__):'((-3*MS1**2*yLLS13x3**2 + 3*MT**2*yLLS13x3**2 + 3*MTA**2*yLLS13x3**2 + 12*MT*MTA*yLLS13x3*yRRS13x3 - 3*MS1**2*yRRS13x3**2 + 3*MT**2*yRRS13x3**2 + 3*MTA**2*yRRS13x3**2)*cmath.sqrt(MS1**4 - 2*MS1**2*MT**2 + MT**4 - 2*MS1**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.W__plus__,P.b):'((MT**2 - MW**2)*((3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2))/(96.*cmath.pi*abs(MT)**3)'})

Decay_ta__minus__ = Decay(name = 'Decay_ta__minus__',
                          particle = P.ta__minus__,
                          partial_widths = {(P.R2p23__star__,P.b):'((-MR2**2 + MTA**2)*(-3*MR2**2*yLRR23x3**2 + 3*MTA**2*yLRR23x3**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.R2p23__star__,P.d):'((-MR2**2 + MTA**2)*(-3*MR2**2*yLRR23x1**2 + 3*MTA**2*yLRR23x1**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.R2p23__star__,P.s):'((-MR2**2 + MTA**2)*(-3*MR2**2*yLRR23x2**2 + 3*MTA**2*yLRR23x2**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.R2p53__star__,P.c):'((-MR2**2 + MTA**2)*(-3*MR2**2*yLRR23x2**2 + 3*MTA**2*yLRR23x2**2 - 3*MR2**2*yRLR22x3**2 + 3*MTA**2*yRLR22x3**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.R2p53__star__,P.t):'((-3*MR2**2*yLRR23x3**2 + 3*MT**2*yLRR23x3**2 + 3*MTA**2*yLRR23x3**2 - 12*MT*MTA*yLRR23x3*yRLR23x3 - 3*MR2**2*yRLR23x3**2 + 3*MT**2*yRLR23x3**2 + 3*MTA**2*yRLR23x3**2)*cmath.sqrt(MR2**4 - 2*MR2**2*MT**2 + MT**4 - 2*MR2**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.R2p53__star__,P.u):'((-MR2**2 + MTA**2)*(-3*MR2**2*yLRR23x1**2 + 3*MTA**2*yLRR23x1**2 - 3*MR2**2*yRLR21x3**2 + 3*MTA**2*yRLR21x3**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.S1m13,P.c__tilde__):'((-MS1**2 + MTA**2)*(-3*MS1**2*yLLS12x3**2 + 3*MTA**2*yLLS12x3**2 - 3*MS1**2*yRRS12x3**2 + 3*MTA**2*yRRS12x3**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.S1m13,P.t__tilde__):'((-3*MS1**2*yLLS13x3**2 + 3*MT**2*yLLS13x3**2 + 3*MTA**2*yLLS13x3**2 + 12*MT*MTA*yLLS13x3*yRRS13x3 - 3*MS1**2*yRRS13x3**2 + 3*MT**2*yRRS13x3**2 + 3*MTA**2*yRRS13x3**2)*cmath.sqrt(MS1**4 - 2*MS1**2*MT**2 + MT**4 - 2*MS1**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.S1m13,P.u__tilde__):'((-MS1**2 + MTA**2)*(-3*MS1**2*yLLS11x3**2 + 3*MTA**2*yLLS11x3**2 - 3*MS1**2*yRRS11x3**2 + 3*MTA**2*yRRS11x3**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.W__minus__,P.vt):'((MTA**2 - MW**2)*((ee**2*MTA**2)/(2.*sw**2) + (ee**2*MTA**4)/(2.*MW**2*sw**2) - (ee**2*MW**2)/sw**2))/(32.*cmath.pi*abs(MTA)**3)'})

Decay_W__plus__ = Decay(name = 'Decay_W__plus__',
                        particle = P.W__plus__,
                        partial_widths = {(P.c,P.s__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.R2p23__star__,P.R2p53):'(((-6*ee**2*MR2**2)/sw**2 + (3*ee**2*MW**2)/(2.*sw**2))*cmath.sqrt(-4*MR2**2*MW**2 + MW**4))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.t,P.b__tilde__):'((-MT**2 + MW**2)*((-3*ee**2*MT**2)/(2.*sw**2) - (3*ee**2*MT**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.u,P.d__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.ve,P.e__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vm,P.mu__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vt,P.ta__plus__):'((-MTA**2 + MW**2)*(-(ee**2*MTA**2)/(2.*sw**2) - (ee**2*MTA**4)/(2.*MW**2*sw**2) + (ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)'})

Decay_Z = Decay(name = 'Decay_Z',
                particle = P.Z,
                partial_widths = {(P.b,P.b__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.c,P.c__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.d,P.d__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.e__minus__,P.e__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.mu__minus__,P.mu__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.R2p23__star__,P.R2p23):'((-14*ee**2*MR2**2 + (7*ee**2*MZ**2)/2. - (3*cw**2*ee**2*MR2**2)/sw**2 + (3*cw**2*ee**2*MZ**2)/(4.*sw**2) - (49*ee**2*MR2**2*sw**2)/(3.*cw**2) + (49*ee**2*MZ**2*sw**2)/(12.*cw**2))*cmath.sqrt(-4*MR2**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.R2p53__star__,P.R2p53):'((14*ee**2*MR2**2 - (7*ee**2*MZ**2)/2. - (3*cw**2*ee**2*MR2**2)/sw**2 + (3*cw**2*ee**2*MZ**2)/(4.*sw**2) - (49*ee**2*MR2**2*sw**2)/(3.*cw**2) + (49*ee**2*MZ**2*sw**2)/(12.*cw**2))*cmath.sqrt(-4*MR2**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.s,P.s__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.t,P.t__tilde__):'((-11*ee**2*MT**2 - ee**2*MZ**2 - (3*cw**2*ee**2*MT**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MT**2*sw**2)/(6.*cw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MT**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((-5*ee**2*MTA**2 - ee**2*MZ**2 - (cw**2*ee**2*MTA**2)/(2.*sw**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MTA**2*sw**2)/(2.*cw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2))*cmath.sqrt(-4*MTA**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.u,P.u__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ve,P.ve__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vm,P.vm__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vt,P.vt__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.W__minus__,P.W__plus__):'(((-12*cw**2*ee**2*MW**2)/sw**2 - (17*cw**2*ee**2*MZ**2)/sw**2 + (4*cw**2*ee**2*MZ**4)/(MW**2*sw**2) + (cw**2*ee**2*MZ**6)/(4.*MW**4*sw**2))*cmath.sqrt(-4*MW**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)'})

