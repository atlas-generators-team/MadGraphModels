# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 11 Jun 2020 16:07:52


from object_library import all_CTparameters, CTParameter

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



invFREps = CTParameter(name = 'invFREps',
                       type = 'complex',
                       value = {-1:'1'},
                       texname = 'invFREps')

FRCTdeltaZxuuLxuG = CTParameter(name = 'FRCTdeltaZxuuLxuG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxuuLxuG')

FRCTdeltaZxuuRxuG = CTParameter(name = 'FRCTdeltaZxuuRxuG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxuuRxuG')

FRCTdeltaZxccLxcG = CTParameter(name = 'FRCTdeltaZxccLxcG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxccLxcG')

FRCTdeltaZxccRxcG = CTParameter(name = 'FRCTdeltaZxccRxcG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxccRxcG')

FRCTdeltaxMTxtG = CTParameter(name = 'FRCTdeltaxMTxtG',
                              type = 'complex',
                              value = {0:'-(G**2*MT)/(3.*cmath.pi**2) + (G**2*MT*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxMTxtG')

FRCTdeltaZxttLxtG = CTParameter(name = 'FRCTdeltaZxttLxtG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxttLxtG')

FRCTdeltaZxttRxtG = CTParameter(name = 'FRCTdeltaZxttRxtG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxttRxtG')

FRCTdeltaZxddLxdG = CTParameter(name = 'FRCTdeltaZxddLxdG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxddLxdG')

FRCTdeltaZxddRxdG = CTParameter(name = 'FRCTdeltaZxddRxdG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxddRxdG')

FRCTdeltaZxssLxsG = CTParameter(name = 'FRCTdeltaZxssLxsG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxssLxsG')

FRCTdeltaZxssRxsG = CTParameter(name = 'FRCTdeltaZxssRxsG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxssRxsG')

FRCTdeltaZxbbLxbG = CTParameter(name = 'FRCTdeltaZxbbLxbG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxbbLxbG')

FRCTdeltaZxbbRxbG = CTParameter(name = 'FRCTdeltaZxbbRxbG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxbbRxbG')

FRCTdeltaxMS1xGLQ1d = CTParameter(name = 'FRCTdeltaxMS1xGLQ1d',
                                  type = 'complex',
                                  value = {0:'(-7*G**2*MS1)/(24.*cmath.pi**2) + (G**2*MS1*reglog(MS1**2/MU_R**2))/(8.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaxMS1xGLQ1d')

FRCTdeltaZxLQ1dLQ1dxGLQ1d = CTParameter(name = 'FRCTdeltaZxLQ1dLQ1dxGLQ1d',
                                        type = 'complex',
                                        value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                        texname = 'FRCTdeltaZxLQ1dLQ1dxGLQ1d')

FRCTdeltaxMS1xGLQ1dd = CTParameter(name = 'FRCTdeltaxMS1xGLQ1dd',
                                   type = 'complex',
                                   value = {0:'(-7*G**2*MS1)/(24.*cmath.pi**2) + (G**2*MS1*reglog(MS1**2/MU_R**2))/(8.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaxMS1xGLQ1dd')

FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd = CTParameter(name = 'FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd',
                                           type = 'complex',
                                           value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                           texname = 'FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd')

FRCTdeltaxMR2xGLQ2uu = CTParameter(name = 'FRCTdeltaxMR2xGLQ2uu',
                                   type = 'complex',
                                   value = {0:'(-7*G**2*MR2)/(24.*cmath.pi**2) + (G**2*MR2*reglog(MR2**2/MU_R**2))/(8.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaxMR2xGLQ2uu')

FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu = CTParameter(name = 'FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu',
                                           type = 'complex',
                                           value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                           texname = 'FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu')

FRCTdeltaxMR2xGLQ2u = CTParameter(name = 'FRCTdeltaxMR2xGLQ2u',
                                  type = 'complex',
                                  value = {0:'(-7*G**2*MR2)/(24.*cmath.pi**2) + (G**2*MR2*reglog(MR2**2/MU_R**2))/(8.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaxMR2xGLQ2u')

FRCTdeltaZxLQ2uLQ2uxGLQ2u = CTParameter(name = 'FRCTdeltaZxLQ2uLQ2uxGLQ2u',
                                        type = 'complex',
                                        value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                        texname = 'FRCTdeltaZxLQ2uLQ2uxGLQ2u')

FRCTdeltaxMR2xGLQ2pu = CTParameter(name = 'FRCTdeltaxMR2xGLQ2pu',
                                   type = 'complex',
                                   value = {0:'(-7*G**2*MR2)/(24.*cmath.pi**2) + (G**2*MR2*reglog(MR2**2/MU_R**2))/(8.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaxMR2xGLQ2pu')

FRCTdeltaZxLQ2puLQ2puxGLQ2pu = CTParameter(name = 'FRCTdeltaZxLQ2puLQ2puxGLQ2pu',
                                           type = 'complex',
                                           value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                           texname = 'FRCTdeltaZxLQ2puLQ2puxGLQ2pu')

FRCTdeltaxMR2xGLQ2d = CTParameter(name = 'FRCTdeltaxMR2xGLQ2d',
                                  type = 'complex',
                                  value = {0:'(-7*G**2*MR2)/(24.*cmath.pi**2) + (G**2*MR2*reglog(MR2**2/MU_R**2))/(8.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaxMR2xGLQ2d')

FRCTdeltaZxLQ2dLQ2dxGLQ2d = CTParameter(name = 'FRCTdeltaZxLQ2dLQ2dxGLQ2d',
                                        type = 'complex',
                                        value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                        texname = 'FRCTdeltaZxLQ2dLQ2dxGLQ2d')

FRCTdeltaxMS3xGLQ3u = CTParameter(name = 'FRCTdeltaxMS3xGLQ3u',
                                  type = 'complex',
                                  value = {0:'(-7*G**2*MS3)/(24.*cmath.pi**2) + (G**2*MS3*reglog(MS3**2/MU_R**2))/(8.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaxMS3xGLQ3u')

FRCTdeltaZxLQ3uLQ3uxGLQ3u = CTParameter(name = 'FRCTdeltaZxLQ3uLQ3uxGLQ3u',
                                        type = 'complex',
                                        value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                        texname = 'FRCTdeltaZxLQ3uLQ3uxGLQ3u')

FRCTdeltaxMS3xGLQ3d = CTParameter(name = 'FRCTdeltaxMS3xGLQ3d',
                                  type = 'complex',
                                  value = {0:'(-7*G**2*MS3)/(24.*cmath.pi**2) + (G**2*MS3*reglog(MS3**2/MU_R**2))/(8.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaxMS3xGLQ3d')

FRCTdeltaZxLQ3dLQ3dxGLQ3d = CTParameter(name = 'FRCTdeltaZxLQ3dLQ3dxGLQ3d',
                                        type = 'complex',
                                        value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                        texname = 'FRCTdeltaZxLQ3dLQ3dxGLQ3d')

FRCTdeltaxMS3xGLQ3dd = CTParameter(name = 'FRCTdeltaxMS3xGLQ3dd',
                                   type = 'complex',
                                   value = {0:'(-7*G**2*MS3)/(24.*cmath.pi**2) + (G**2*MS3*reglog(MS3**2/MU_R**2))/(8.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaxMS3xGLQ3dd')

FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd = CTParameter(name = 'FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd',
                                           type = 'complex',
                                           value = {-1:'-G**2/(6.*cmath.pi**2)'},
                                           texname = 'FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd')

FRCTdeltaZxGGxb = CTParameter(name = 'FRCTdeltaZxGGxb',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxb')

FRCTdeltaZxGGxc = CTParameter(name = 'FRCTdeltaZxGGxc',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxc')

FRCTdeltaZxGGxd = CTParameter(name = 'FRCTdeltaZxGGxd',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxd')

FRCTdeltaZxGGxG = CTParameter(name = 'FRCTdeltaZxGGxG',
                              type = 'complex',
                              value = {-1:'(-19*G**2)/(64.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxG')

FRCTdeltaZxGGxghG = CTParameter(name = 'FRCTdeltaZxGGxghG',
                                type = 'complex',
                                value = {-1:'-G**2/(64.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxGGxghG')

FRCTdeltaZxGGxs = CTParameter(name = 'FRCTdeltaZxGGxs',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxs')

FRCTdeltaZxGGxu = CTParameter(name = 'FRCTdeltaZxGGxu',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxu')

FRCTdeltaZxGGxLQ2d = CTParameter(name = 'FRCTdeltaZxGGxLQ2d',
                                 type = 'complex',
                                 value = {0:'(G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaZxGGxLQ2d')

FRCTdeltaZxGGxLQ2pu = CTParameter(name = 'FRCTdeltaZxGGxLQ2pu',
                                  type = 'complex',
                                  value = {0:'(G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaZxGGxLQ2pu')

FRCTdeltaZxGGxLQ2u = CTParameter(name = 'FRCTdeltaZxGGxLQ2u',
                                 type = 'complex',
                                 value = {0:'(G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaZxGGxLQ2u')

FRCTdeltaZxGGxLQ2uu = CTParameter(name = 'FRCTdeltaZxGGxLQ2uu',
                                  type = 'complex',
                                  value = {0:'(G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaZxGGxLQ2uu')

FRCTdeltaZxGGxLQ1d = CTParameter(name = 'FRCTdeltaZxGGxLQ1d',
                                 type = 'complex',
                                 value = {0:'(G**2*reglog(MS1**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaZxGGxLQ1d')

FRCTdeltaZxGGxLQ1dd = CTParameter(name = 'FRCTdeltaZxGGxLQ1dd',
                                  type = 'complex',
                                  value = {0:'(G**2*reglog(MS1**2/MU_R**2))/(96.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaZxGGxLQ1dd')

FRCTdeltaZxGGxLQ3d = CTParameter(name = 'FRCTdeltaZxGGxLQ3d',
                                 type = 'complex',
                                 value = {0:'(G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaZxGGxLQ3d')

FRCTdeltaZxGGxLQ3dd = CTParameter(name = 'FRCTdeltaZxGGxLQ3dd',
                                  type = 'complex',
                                  value = {0:'(G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                  texname = 'FRCTdeltaZxGGxLQ3dd')

FRCTdeltaZxGGxLQ3u = CTParameter(name = 'FRCTdeltaZxGGxLQ3u',
                                 type = 'complex',
                                 value = {0:'(G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaZxGGxLQ3u')

FRCTdeltaZxGGxt = CTParameter(name = 'FRCTdeltaZxGGxt',
                              type = 'complex',
                              value = {0:'(G**2*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxt')

FRCTdeltaxaSxLQ2d = CTParameter(name = 'FRCTdeltaxaSxLQ2d',
                                type = 'complex',
                                value = {0:'-(aS*G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxaSxLQ2d')

FRCTdeltaxaSxLQ2pu = CTParameter(name = 'FRCTdeltaxaSxLQ2pu',
                                 type = 'complex',
                                 value = {0:'-(aS*G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaxaSxLQ2pu')

FRCTdeltaxaSxLQ2u = CTParameter(name = 'FRCTdeltaxaSxLQ2u',
                                type = 'complex',
                                value = {0:'-(aS*G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxaSxLQ2u')

FRCTdeltaxaSxLQ2uu = CTParameter(name = 'FRCTdeltaxaSxLQ2uu',
                                 type = 'complex',
                                 value = {0:'-(aS*G**2*reglog(MR2**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaxaSxLQ2uu')

FRCTdeltaxaSxLQ1d = CTParameter(name = 'FRCTdeltaxaSxLQ1d',
                                type = 'complex',
                                value = {0:'-(aS*G**2*reglog(MS1**2/MU_R**2))/(96.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxaSxLQ1d')

FRCTdeltaxaSxLQ1dd = CTParameter(name = 'FRCTdeltaxaSxLQ1dd',
                                 type = 'complex',
                                 value = {0:'-(aS*G**2*reglog(MS1**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaxaSxLQ1dd')

FRCTdeltaxaSxLQ3d = CTParameter(name = 'FRCTdeltaxaSxLQ3d',
                                type = 'complex',
                                value = {0:'-(aS*G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxaSxLQ3d')

FRCTdeltaxaSxLQ3dd = CTParameter(name = 'FRCTdeltaxaSxLQ3dd',
                                 type = 'complex',
                                 value = {0:'-(aS*G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                 texname = 'FRCTdeltaxaSxLQ3dd')

FRCTdeltaxaSxLQ3u = CTParameter(name = 'FRCTdeltaxaSxLQ3u',
                                type = 'complex',
                                value = {0:'-(aS*G**2*reglog(MS3**2/MU_R**2))/(96.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxaSxLQ3u')

FRCTdeltaxaSxt = CTParameter(name = 'FRCTdeltaxaSxt',
                             type = 'complex',
                             value = {0:'-(aS*G**2*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                             texname = 'FRCTdeltaxaSxt')

