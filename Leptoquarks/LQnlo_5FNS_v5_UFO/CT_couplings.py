# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 11 Jun 2020 16:07:46


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_1000_1 = Coupling(name = 'R2GC_1000_1',
                       value = '-(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1000_2 = Coupling(name = 'R2GC_1000_2',
                       value = '(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1000_3 = Coupling(name = 'R2GC_1000_3',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1000_4 = Coupling(name = 'R2GC_1000_4',
                       value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1000_5 = Coupling(name = 'R2GC_1000_5',
                       value = '(-5*ee**2*complex(0,1)*G**2)/(648.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1000_6 = Coupling(name = 'R2GC_1000_6',
                       value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1001_7 = Coupling(name = 'R2GC_1001_7',
                       value = '(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1001_8 = Coupling(name = 'R2GC_1001_8',
                       value = '-(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1001_9 = Coupling(name = 'R2GC_1001_9',
                       value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1001_10 = Coupling(name = 'R2GC_1001_10',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1001_11 = Coupling(name = 'R2GC_1001_11',
                        value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1001_12 = Coupling(name = 'R2GC_1001_12',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_13 = Coupling(name = 'R2GC_1002_13',
                        value = '(ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_14 = Coupling(name = 'R2GC_1002_14',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_15 = Coupling(name = 'R2GC_1002_15',
                        value = '-(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_16 = Coupling(name = 'R2GC_1002_16',
                        value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_17 = Coupling(name = 'R2GC_1002_17',
                        value = '(5*ee**2*complex(0,1)*G**2)/(324.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1002_18 = Coupling(name = 'R2GC_1002_18',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_19 = Coupling(name = 'R2GC_1003_19',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_20 = Coupling(name = 'R2GC_1003_20',
                        value = '(ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_21 = Coupling(name = 'R2GC_1003_21',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_22 = Coupling(name = 'R2GC_1003_22',
                        value = '-(ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_23 = Coupling(name = 'R2GC_1003_23',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1003_24 = Coupling(name = 'R2GC_1003_24',
                        value = '(5*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1004_25 = Coupling(name = 'R2GC_1004_25',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1004_26 = Coupling(name = 'R2GC_1004_26',
                        value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1004_27 = Coupling(name = 'R2GC_1004_27',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1005_28 = Coupling(name = 'R2GC_1005_28',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1005_29 = Coupling(name = 'R2GC_1005_29',
                        value = '-(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1005_30 = Coupling(name = 'R2GC_1005_30',
                        value = '(5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_31 = Coupling(name = 'R2GC_1006_31',
                        value = '(-2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_32 = Coupling(name = 'R2GC_1006_32',
                        value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_33 = Coupling(name = 'R2GC_1006_33',
                        value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_34 = Coupling(name = 'R2GC_1006_34',
                        value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_35 = Coupling(name = 'R2GC_1006_35',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(162.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1006_36 = Coupling(name = 'R2GC_1006_36',
                        value = '(5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_37 = Coupling(name = 'R2GC_1007_37',
                        value = '(2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_38 = Coupling(name = 'R2GC_1007_38',
                        value = '-(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_39 = Coupling(name = 'R2GC_1007_39',
                        value = '-(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_40 = Coupling(name = 'R2GC_1007_40',
                        value = '(ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_41 = Coupling(name = 'R2GC_1007_41',
                        value = '(5*ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1007_42 = Coupling(name = 'R2GC_1007_42',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1008_43 = Coupling(name = 'R2GC_1008_43',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1008_44 = Coupling(name = 'R2GC_1008_44',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1008_45 = Coupling(name = 'R2GC_1008_45',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1009_46 = Coupling(name = 'R2GC_1009_46',
                        value = '(ee**2*complex(0,1)*G**2)/(3.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1009_47 = Coupling(name = 'R2GC_1009_47',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1009_48 = Coupling(name = 'R2GC_1009_48',
                        value = '(5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1010_49 = Coupling(name = 'R2GC_1010_49',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1010_50 = Coupling(name = 'R2GC_1010_50',
                        value = '(5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1010_51 = Coupling(name = 'R2GC_1010_51',
                        value = '-(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1010_52 = Coupling(name = 'R2GC_1010_52',
                        value = '(-25*ee**2*complex(0,1)*G**2)/(1296.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1010_53 = Coupling(name = 'R2GC_1010_53',
                        value = '(5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_54 = Coupling(name = 'R2GC_1011_54',
                        value = '(5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_55 = Coupling(name = 'R2GC_1011_55',
                        value = '-(ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_56 = Coupling(name = 'R2GC_1011_56',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_57 = Coupling(name = 'R2GC_1011_57',
                        value = '(ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_58 = Coupling(name = 'R2GC_1011_58',
                        value = '(25*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1011_59 = Coupling(name = 'R2GC_1011_59',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1012_60 = Coupling(name = 'R2GC_1012_60',
                        value = '-(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1012_61 = Coupling(name = 'R2GC_1012_61',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1012_62 = Coupling(name = 'R2GC_1012_62',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1012_63 = Coupling(name = 'R2GC_1012_63',
                        value = '(25*ee**2*complex(0,1)*G**2)/(648.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1012_64 = Coupling(name = 'R2GC_1012_64',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_65 = Coupling(name = 'R2GC_1013_65',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_66 = Coupling(name = 'R2GC_1013_66',
                        value = '(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_67 = Coupling(name = 'R2GC_1013_67',
                        value = '(5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_68 = Coupling(name = 'R2GC_1013_68',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_69 = Coupling(name = 'R2GC_1013_69',
                        value = '(-25*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1013_70 = Coupling(name = 'R2GC_1013_70',
                        value = '(5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_71 = Coupling(name = 'R2GC_1014_71',
                        value = '-(ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_72 = Coupling(name = 'R2GC_1014_72',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_73 = Coupling(name = 'R2GC_1014_73',
                        value = '(7*ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_74 = Coupling(name = 'R2GC_1014_74',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_75 = Coupling(name = 'R2GC_1014_75',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1014_76 = Coupling(name = 'R2GC_1014_76',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (245*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_77 = Coupling(name = 'R2GC_1015_77',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_78 = Coupling(name = 'R2GC_1015_78',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_79 = Coupling(name = 'R2GC_1015_79',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(1152.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_80 = Coupling(name = 'R2GC_1015_80',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_81 = Coupling(name = 'R2GC_1015_81',
                        value = '(5*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1015_82 = Coupling(name = 'R2GC_1015_82',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (245*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1016_83 = Coupling(name = 'R2GC_1016_83',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1016_84 = Coupling(name = 'R2GC_1016_84',
                        value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1016_85 = Coupling(name = 'R2GC_1016_85',
                        value = '(5*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1017_86 = Coupling(name = 'R2GC_1017_86',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1017_87 = Coupling(name = 'R2GC_1017_87',
                        value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1017_88 = Coupling(name = 'R2GC_1017_88',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_89 = Coupling(name = 'R2GC_1018_89',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_90 = Coupling(name = 'R2GC_1018_90',
                        value = '(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_91 = Coupling(name = 'R2GC_1018_91',
                        value = '(5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_92 = Coupling(name = 'R2GC_1018_92',
                        value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_93 = Coupling(name = 'R2GC_1018_93',
                        value = '(-25*ee**2*complex(0,1)*G**2)/(324.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1018_94 = Coupling(name = 'R2GC_1018_94',
                        value = '(5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_95 = Coupling(name = 'R2GC_1019_95',
                        value = '(5*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_96 = Coupling(name = 'R2GC_1019_96',
                        value = '-(ee**2*complex(0,1)*G**2)/(3.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_97 = Coupling(name = 'R2GC_1019_97',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_98 = Coupling(name = 'R2GC_1019_98',
                        value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_99 = Coupling(name = 'R2GC_1019_99',
                        value = '(25*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1019_100 = Coupling(name = 'R2GC_1019_100',
                         value = '(-5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1020_101 = Coupling(name = 'R2GC_1020_101',
                         value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1020_102 = Coupling(name = 'R2GC_1020_102',
                         value = '(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1020_103 = Coupling(name = 'R2GC_1020_103',
                         value = '(-5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1021_104 = Coupling(name = 'R2GC_1021_104',
                         value = '(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1021_105 = Coupling(name = 'R2GC_1021_105',
                         value = '-(ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1021_106 = Coupling(name = 'R2GC_1021_106',
                         value = '(5*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_107 = Coupling(name = 'R2GC_1022_107',
                         value = '-(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_108 = Coupling(name = 'R2GC_1022_108',
                         value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_109 = Coupling(name = 'R2GC_1022_109',
                         value = '(7*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_110 = Coupling(name = 'R2GC_1022_110',
                         value = '(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_111 = Coupling(name = 'R2GC_1022_111',
                         value = '(-5*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1022_112 = Coupling(name = 'R2GC_1022_112',
                         value = '(-5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_113 = Coupling(name = 'R2GC_1023_113',
                         value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_114 = Coupling(name = 'R2GC_1023_114',
                         value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_115 = Coupling(name = 'R2GC_1023_115',
                         value = '(-7*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_116 = Coupling(name = 'R2GC_1023_116',
                         value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_117 = Coupling(name = 'R2GC_1023_117',
                         value = '(5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1023_118 = Coupling(name = 'R2GC_1023_118',
                         value = '(5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1024_119 = Coupling(name = 'R2GC_1024_119',
                         value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1024_120 = Coupling(name = 'R2GC_1024_120',
                         value = '-(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1024_121 = Coupling(name = 'R2GC_1024_121',
                         value = '(5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1025_122 = Coupling(name = 'R2GC_1025_122',
                         value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1025_123 = Coupling(name = 'R2GC_1025_123',
                         value = '(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1025_124 = Coupling(name = 'R2GC_1025_124',
                         value = '(-5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1026_125 = Coupling(name = 'R2GC_1026_125',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1026_126 = Coupling(name = 'R2GC_1026_126',
                         value = '(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1026_127 = Coupling(name = 'R2GC_1026_127',
                         value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1027_128 = Coupling(name = 'R2GC_1027_128',
                         value = '(cw**2*ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1027_129 = Coupling(name = 'R2GC_1027_129',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_1027_130 = Coupling(name = 'R2GC_1027_130',
                         value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

R2GC_341_131 = Coupling(name = 'R2GC_341_131',
                        value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_342_132 = Coupling(name = 'R2GC_342_132',
                        value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_343_133 = Coupling(name = 'R2GC_343_133',
                        value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_344_134 = Coupling(name = 'R2GC_344_134',
                        value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_346_135 = Coupling(name = 'R2GC_346_135',
                        value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_348_136 = Coupling(name = 'R2GC_348_136',
                        value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_348_137 = Coupling(name = 'R2GC_348_137',
                        value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_349_138 = Coupling(name = 'R2GC_349_138',
                        value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_349_139 = Coupling(name = 'R2GC_349_139',
                        value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_350_140 = Coupling(name = 'R2GC_350_140',
                        value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_350_141 = Coupling(name = 'R2GC_350_141',
                        value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_351_142 = Coupling(name = 'R2GC_351_142',
                        value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_351_143 = Coupling(name = 'R2GC_351_143',
                        value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_352_144 = Coupling(name = 'R2GC_352_144',
                        value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_352_145 = Coupling(name = 'R2GC_352_145',
                        value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_353_146 = Coupling(name = 'R2GC_353_146',
                        value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_353_147 = Coupling(name = 'R2GC_353_147',
                        value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_354_148 = Coupling(name = 'R2GC_354_148',
                        value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_413_149 = Coupling(name = 'R2GC_413_149',
                        value = '-G**4/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_413_150 = Coupling(name = 'R2GC_413_150',
                        value = 'G**4/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_414_151 = Coupling(name = 'R2GC_414_151',
                        value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_414_152 = Coupling(name = 'R2GC_414_152',
                        value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_415_153 = Coupling(name = 'R2GC_415_153',
                        value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_415_154 = Coupling(name = 'R2GC_415_154',
                        value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_416_155 = Coupling(name = 'R2GC_416_155',
                        value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_417_156 = Coupling(name = 'R2GC_417_156',
                        value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_417_157 = Coupling(name = 'R2GC_417_157',
                        value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_419_158 = Coupling(name = 'R2GC_419_158',
                        value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_420_159 = Coupling(name = 'R2GC_420_159',
                        value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_422_160 = Coupling(name = 'R2GC_422_160',
                        value = 'G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_422_161 = Coupling(name = 'R2GC_422_161',
                        value = '(11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_423_162 = Coupling(name = 'R2GC_423_162',
                        value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_424_163 = Coupling(name = 'R2GC_424_163',
                        value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_425_164 = Coupling(name = 'R2GC_425_164',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_427_165 = Coupling(name = 'R2GC_427_165',
                        value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_428_166 = Coupling(name = 'R2GC_428_166',
                        value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_428_167 = Coupling(name = 'R2GC_428_167',
                        value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_429_168 = Coupling(name = 'R2GC_429_168',
                        value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_430_169 = Coupling(name = 'R2GC_430_169',
                        value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_431_170 = Coupling(name = 'R2GC_431_170',
                        value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_432_171 = Coupling(name = 'R2GC_432_171',
                        value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_433_172 = Coupling(name = 'R2GC_433_172',
                        value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_435_173 = Coupling(name = 'R2GC_435_173',
                        value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_437_174 = Coupling(name = 'R2GC_437_174',
                        value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_444_175 = Coupling(name = 'R2GC_444_175',
                        value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_445_176 = Coupling(name = 'R2GC_445_176',
                        value = '-(ee*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_446_177 = Coupling(name = 'R2GC_446_177',
                        value = '-(ee**2*complex(0,1)*G**2)/(324.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_447_178 = Coupling(name = 'R2GC_447_178',
                        value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_448_179 = Coupling(name = 'R2GC_448_179',
                        value = '(53*ee*complex(0,1)*G**3)/(864.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_449_180 = Coupling(name = 'R2GC_449_180',
                        value = '(-13*complex(0,1)*G**4)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_449_181 = Coupling(name = 'R2GC_449_181',
                        value = '(13*complex(0,1)*G**4)/(1728.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_450_182 = Coupling(name = 'R2GC_450_182',
                        value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_451_183 = Coupling(name = 'R2GC_451_183',
                        value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_451_184 = Coupling(name = 'R2GC_451_184',
                        value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_452_185 = Coupling(name = 'R2GC_452_185',
                        value = '(complex(0,1)*G**2*MS1**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_453_186 = Coupling(name = 'R2GC_453_186',
                        value = '(ee*complex(0,1)*G**2*sw)/(216.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_454_187 = Coupling(name = 'R2GC_454_187',
                        value = '(ee**2*complex(0,1)*G**2*sw)/(324.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_455_188 = Coupling(name = 'R2GC_455_188',
                        value = '(-53*ee*complex(0,1)*G**3*sw)/(864.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_456_189 = Coupling(name = 'R2GC_456_189',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_458_190 = Coupling(name = 'R2GC_458_190',
                        value = '-(ee*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_459_191 = Coupling(name = 'R2GC_459_191',
                        value = '(-4*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_461_192 = Coupling(name = 'R2GC_461_192',
                        value = '(53*ee*complex(0,1)*G**3)/(216.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_466_193 = Coupling(name = 'R2GC_466_193',
                        value = '(ee*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_467_194 = Coupling(name = 'R2GC_467_194',
                        value = '(4*ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_468_195 = Coupling(name = 'R2GC_468_195',
                        value = '(-53*ee*complex(0,1)*G**3*sw)/(216.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_469_196 = Coupling(name = 'R2GC_469_196',
                        value = '(-4*ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_478_197 = Coupling(name = 'R2GC_478_197',
                        value = '-(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_479_198 = Coupling(name = 'R2GC_479_198',
                        value = '-(cw*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(648.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_480_199 = Coupling(name = 'R2GC_480_199',
                        value = '(53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw) + (53*ee*complex(0,1)*G**3*sw)/(1728.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_481_200 = Coupling(name = 'R2GC_481_200',
                        value = '-(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_483_201 = Coupling(name = 'R2GC_483_201',
                        value = '(ee*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_484_202 = Coupling(name = 'R2GC_484_202',
                        value = '-(ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_486_203 = Coupling(name = 'R2GC_486_203',
                        value = '(-53*ee*complex(0,1)*G**3)/(432.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_490_204 = Coupling(name = 'R2GC_490_204',
                        value = '(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_491_205 = Coupling(name = 'R2GC_491_205',
                        value = '-(cw*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*sw)/(324.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_492_206 = Coupling(name = 'R2GC_492_206',
                        value = '(-53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw) + (53*ee*complex(0,1)*G**3*sw)/(1728.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_493_207 = Coupling(name = 'R2GC_493_207',
                        value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_502_208 = Coupling(name = 'R2GC_502_208',
                        value = '-(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (7*ee*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_503_209 = Coupling(name = 'R2GC_503_209',
                        value = '(cw*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*sw) + (7*ee**2*complex(0,1)*G**2*sw)/(324.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_504_210 = Coupling(name = 'R2GC_504_210',
                        value = '(53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw) + (371*ee*complex(0,1)*G**3*sw)/(1728.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_505_211 = Coupling(name = 'R2GC_505_211',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_506_212 = Coupling(name = 'R2GC_506_212',
                        value = '(complex(0,1)*G**2*MR2**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_510_213 = Coupling(name = 'R2GC_510_213',
                        value = '(5*ee*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_513_214 = Coupling(name = 'R2GC_513_214',
                        value = '(-265*ee*complex(0,1)*G**3)/(864.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_518_215 = Coupling(name = 'R2GC_518_215',
                        value = '(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (7*ee*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_519_216 = Coupling(name = 'R2GC_519_216',
                        value = '(-5*cw*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw) + (35*ee**2*complex(0,1)*G**2*sw)/(648.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_520_217 = Coupling(name = 'R2GC_520_217',
                        value = '(-53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw) + (371*ee*complex(0,1)*G**3*sw)/(1728.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_521_218 = Coupling(name = 'R2GC_521_218',
                        value = '(7*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_542_219 = Coupling(name = 'R2GC_542_219',
                        value = '-(cw*ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(216.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_543_220 = Coupling(name = 'R2GC_543_220',
                        value = '-(cw*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_544_221 = Coupling(name = 'R2GC_544_221',
                        value = '(53*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw) - (53*ee*complex(0,1)*G**3*sw)/(864.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_545_222 = Coupling(name = 'R2GC_545_222',
                        value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_546_223 = Coupling(name = 'R2GC_546_223',
                        value = '(complex(0,1)*G**2*MS3**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_557_224 = Coupling(name = 'R2GC_557_224',
                        value = '(cw*ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(216.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_558_225 = Coupling(name = 'R2GC_558_225',
                        value = '-(cw*ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(162.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_559_226 = Coupling(name = 'R2GC_559_226',
                        value = '(-53*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw) - (53*ee*complex(0,1)*G**3*sw)/(864.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_560_227 = Coupling(name = 'R2GC_560_227',
                        value = '-(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_569_228 = Coupling(name = 'R2GC_569_228',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_572_229 = Coupling(name = 'R2GC_572_229',
                        value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_573_230 = Coupling(name = 'R2GC_573_230',
                        value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_579_231 = Coupling(name = 'R2GC_579_231',
                        value = '(complex(0,1)*G**2*y1LL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_580_232 = Coupling(name = 'R2GC_580_232',
                        value = '(complex(0,1)*G**2*y1LL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_581_233 = Coupling(name = 'R2GC_581_233',
                        value = '(complex(0,1)*G**2*y1LL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_582_234 = Coupling(name = 'R2GC_582_234',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_583_235 = Coupling(name = 'R2GC_583_235',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_584_236 = Coupling(name = 'R2GC_584_236',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_585_237 = Coupling(name = 'R2GC_585_237',
                        value = '-(complex(0,1)*G**2*ty1RR3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_586_238 = Coupling(name = 'R2GC_586_238',
                        value = '-(complex(0,1)*G**2*ty1RR3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_587_239 = Coupling(name = 'R2GC_587_239',
                        value = '-(complex(0,1)*G**2*ty1RR3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_588_240 = Coupling(name = 'R2GC_588_240',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_589_241 = Coupling(name = 'R2GC_589_241',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_590_242 = Coupling(name = 'R2GC_590_242',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_591_243 = Coupling(name = 'R2GC_591_243',
                        value = '-(complex(0,1)*G**2*ty2RL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_592_244 = Coupling(name = 'R2GC_592_244',
                        value = '-(complex(0,1)*G**2*ty2RL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_593_245 = Coupling(name = 'R2GC_593_245',
                        value = '-(complex(0,1)*G**2*ty2RL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_594_246 = Coupling(name = 'R2GC_594_246',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_595_247 = Coupling(name = 'R2GC_595_247',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_596_248 = Coupling(name = 'R2GC_596_248',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_597_249 = Coupling(name = 'R2GC_597_249',
                        value = '(complex(0,1)*G**2*ty2RL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_598_250 = Coupling(name = 'R2GC_598_250',
                        value = '(complex(0,1)*G**2*ty2RL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_599_251 = Coupling(name = 'R2GC_599_251',
                        value = '(complex(0,1)*G**2*ty2RL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_600_252 = Coupling(name = 'R2GC_600_252',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_601_253 = Coupling(name = 'R2GC_601_253',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_602_254 = Coupling(name = 'R2GC_602_254',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_603_255 = Coupling(name = 'R2GC_603_255',
                        value = '-(complex(0,1)*G**2*y2LR3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_604_256 = Coupling(name = 'R2GC_604_256',
                        value = '-(complex(0,1)*G**2*y2LR3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_605_257 = Coupling(name = 'R2GC_605_257',
                        value = '-(complex(0,1)*G**2*y2LR3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_606_258 = Coupling(name = 'R2GC_606_258',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_607_259 = Coupling(name = 'R2GC_607_259',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_608_260 = Coupling(name = 'R2GC_608_260',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_609_261 = Coupling(name = 'R2GC_609_261',
                        value = '(complex(0,1)*G**2*y3LL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_610_262 = Coupling(name = 'R2GC_610_262',
                        value = '(complex(0,1)*G**2*y3LL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_611_263 = Coupling(name = 'R2GC_611_263',
                        value = '(complex(0,1)*G**2*y3LL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_612_264 = Coupling(name = 'R2GC_612_264',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_613_265 = Coupling(name = 'R2GC_613_265',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_614_266 = Coupling(name = 'R2GC_614_266',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_615_267 = Coupling(name = 'R2GC_615_267',
                        value = '(complex(0,1)*G**2*y3LL3x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_616_268 = Coupling(name = 'R2GC_616_268',
                        value = '(complex(0,1)*G**2*y3LL3x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_617_269 = Coupling(name = 'R2GC_617_269',
                        value = '(complex(0,1)*G**2*y3LL3x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_618_270 = Coupling(name = 'R2GC_618_270',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_619_271 = Coupling(name = 'R2GC_619_271',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_620_272 = Coupling(name = 'R2GC_620_272',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL3x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_621_273 = Coupling(name = 'R2GC_621_273',
                        value = '-(complex(0,1)*G**2*y1LL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_622_274 = Coupling(name = 'R2GC_622_274',
                        value = '-(complex(0,1)*G**2*y1LL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_623_275 = Coupling(name = 'R2GC_623_275',
                        value = '-(complex(0,1)*G**2*y1LL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_624_276 = Coupling(name = 'R2GC_624_276',
                        value = '-(complex(0,1)*G**2*y1RR2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_625_277 = Coupling(name = 'R2GC_625_277',
                        value = '-(complex(0,1)*G**2*y1RR2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_626_278 = Coupling(name = 'R2GC_626_278',
                        value = '-(complex(0,1)*G**2*y1RR2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_627_279 = Coupling(name = 'R2GC_627_279',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_628_280 = Coupling(name = 'R2GC_628_280',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_629_281 = Coupling(name = 'R2GC_629_281',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_630_282 = Coupling(name = 'R2GC_630_282',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_631_283 = Coupling(name = 'R2GC_631_283',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_632_284 = Coupling(name = 'R2GC_632_284',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_633_285 = Coupling(name = 'R2GC_633_285',
                        value = '-(complex(0,1)*G**2*y2RL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_634_286 = Coupling(name = 'R2GC_634_286',
                        value = '-(complex(0,1)*G**2*y2RL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_635_287 = Coupling(name = 'R2GC_635_287',
                        value = '-(complex(0,1)*G**2*y2RL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_636_288 = Coupling(name = 'R2GC_636_288',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_637_289 = Coupling(name = 'R2GC_637_289',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_638_290 = Coupling(name = 'R2GC_638_290',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_639_291 = Coupling(name = 'R2GC_639_291',
                        value = '-(complex(0,1)*G**2*y2LR2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_640_292 = Coupling(name = 'R2GC_640_292',
                        value = '-(complex(0,1)*G**2*y2LR2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_641_293 = Coupling(name = 'R2GC_641_293',
                        value = '-(complex(0,1)*G**2*y2LR2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_642_294 = Coupling(name = 'R2GC_642_294',
                        value = '(complex(0,1)*G**2*y2RL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_643_295 = Coupling(name = 'R2GC_643_295',
                        value = '(complex(0,1)*G**2*y2RL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_644_296 = Coupling(name = 'R2GC_644_296',
                        value = '(complex(0,1)*G**2*y2RL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_645_297 = Coupling(name = 'R2GC_645_297',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_646_298 = Coupling(name = 'R2GC_646_298',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_647_299 = Coupling(name = 'R2GC_647_299',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_648_300 = Coupling(name = 'R2GC_648_300',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_649_301 = Coupling(name = 'R2GC_649_301',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_650_302 = Coupling(name = 'R2GC_650_302',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_651_303 = Coupling(name = 'R2GC_651_303',
                        value = '(complex(0,1)*G**2*y3LL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_652_304 = Coupling(name = 'R2GC_652_304',
                        value = '(complex(0,1)*G**2*y3LL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_653_305 = Coupling(name = 'R2GC_653_305',
                        value = '(complex(0,1)*G**2*y3LL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_654_306 = Coupling(name = 'R2GC_654_306',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_655_307 = Coupling(name = 'R2GC_655_307',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_656_308 = Coupling(name = 'R2GC_656_308',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_657_309 = Coupling(name = 'R2GC_657_309',
                        value = '-(complex(0,1)*G**2*y3LL2x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_658_310 = Coupling(name = 'R2GC_658_310',
                        value = '-(complex(0,1)*G**2*y3LL2x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_659_311 = Coupling(name = 'R2GC_659_311',
                        value = '-(complex(0,1)*G**2*y3LL2x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_660_312 = Coupling(name = 'R2GC_660_312',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL2x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_661_313 = Coupling(name = 'R2GC_661_313',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL2x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_662_314 = Coupling(name = 'R2GC_662_314',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL2x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_663_315 = Coupling(name = 'R2GC_663_315',
                        value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_664_316 = Coupling(name = 'R2GC_664_316',
                        value = '(complex(0,1)*G**2*y1LL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_665_317 = Coupling(name = 'R2GC_665_317',
                        value = '(complex(0,1)*G**2*y1LL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_666_318 = Coupling(name = 'R2GC_666_318',
                        value = '(complex(0,1)*G**2*y1LL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_667_319 = Coupling(name = 'R2GC_667_319',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_668_320 = Coupling(name = 'R2GC_668_320',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_669_321 = Coupling(name = 'R2GC_669_321',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_670_322 = Coupling(name = 'R2GC_670_322',
                        value = '-(complex(0,1)*G**2*ty1RR1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_671_323 = Coupling(name = 'R2GC_671_323',
                        value = '-(complex(0,1)*G**2*ty1RR1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_672_324 = Coupling(name = 'R2GC_672_324',
                        value = '-(complex(0,1)*G**2*ty1RR1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_673_325 = Coupling(name = 'R2GC_673_325',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_674_326 = Coupling(name = 'R2GC_674_326',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_675_327 = Coupling(name = 'R2GC_675_327',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_676_328 = Coupling(name = 'R2GC_676_328',
                        value = '-(complex(0,1)*G**2*ty2RL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_677_329 = Coupling(name = 'R2GC_677_329',
                        value = '-(complex(0,1)*G**2*ty2RL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_678_330 = Coupling(name = 'R2GC_678_330',
                        value = '-(complex(0,1)*G**2*ty2RL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_679_331 = Coupling(name = 'R2GC_679_331',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_680_332 = Coupling(name = 'R2GC_680_332',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_681_333 = Coupling(name = 'R2GC_681_333',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_682_334 = Coupling(name = 'R2GC_682_334',
                        value = '(complex(0,1)*G**2*ty2RL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_683_335 = Coupling(name = 'R2GC_683_335',
                        value = '(complex(0,1)*G**2*ty2RL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_684_336 = Coupling(name = 'R2GC_684_336',
                        value = '(complex(0,1)*G**2*ty2RL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_685_337 = Coupling(name = 'R2GC_685_337',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_686_338 = Coupling(name = 'R2GC_686_338',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_687_339 = Coupling(name = 'R2GC_687_339',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_688_340 = Coupling(name = 'R2GC_688_340',
                        value = '-(complex(0,1)*G**2*y2LR1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_689_341 = Coupling(name = 'R2GC_689_341',
                        value = '-(complex(0,1)*G**2*y2LR1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_690_342 = Coupling(name = 'R2GC_690_342',
                        value = '-(complex(0,1)*G**2*y2LR1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_691_343 = Coupling(name = 'R2GC_691_343',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_692_344 = Coupling(name = 'R2GC_692_344',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_693_345 = Coupling(name = 'R2GC_693_345',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2LR1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_694_346 = Coupling(name = 'R2GC_694_346',
                        value = '(complex(0,1)*G**2*y3LL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_695_347 = Coupling(name = 'R2GC_695_347',
                        value = '(complex(0,1)*G**2*y3LL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_696_348 = Coupling(name = 'R2GC_696_348',
                        value = '(complex(0,1)*G**2*y3LL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_697_349 = Coupling(name = 'R2GC_697_349',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_698_350 = Coupling(name = 'R2GC_698_350',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_699_351 = Coupling(name = 'R2GC_699_351',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_700_352 = Coupling(name = 'R2GC_700_352',
                        value = '(complex(0,1)*G**2*y3LL1x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_701_353 = Coupling(name = 'R2GC_701_353',
                        value = '(complex(0,1)*G**2*y3LL1x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_702_354 = Coupling(name = 'R2GC_702_354',
                        value = '(complex(0,1)*G**2*y3LL1x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_703_355 = Coupling(name = 'R2GC_703_355',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_704_356 = Coupling(name = 'R2GC_704_356',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_705_357 = Coupling(name = 'R2GC_705_357',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL1x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_706_358 = Coupling(name = 'R2GC_706_358',
                        value = '(-11*complex(0,1)*G**4)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_706_359 = Coupling(name = 'R2GC_706_359',
                        value = '(11*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_706_360 = Coupling(name = 'R2GC_706_360',
                        value = '(-55*complex(0,1)*G**4)/(3456.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_707_361 = Coupling(name = 'R2GC_707_361',
                        value = '(-5*complex(0,1)*G**4)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_707_362 = Coupling(name = 'R2GC_707_362',
                        value = '(5*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_707_363 = Coupling(name = 'R2GC_707_363',
                        value = '(-25*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_742_364 = Coupling(name = 'R2GC_742_364',
                        value = '(ee**2*complex(0,1)*G**2)/(216.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_745_365 = Coupling(name = 'R2GC_745_365',
                        value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_745_366 = Coupling(name = 'R2GC_745_366',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_747_367 = Coupling(name = 'R2GC_747_367',
                        value = '-(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_748_368 = Coupling(name = 'R2GC_748_368',
                        value = '(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_749_369 = Coupling(name = 'R2GC_749_369',
                        value = '-(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_750_370 = Coupling(name = 'R2GC_750_370',
                        value = '(-3*ee*complex(0,1)*G**3)/(32.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_750_371 = Coupling(name = 'R2GC_750_371',
                        value = '(ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_771_372 = Coupling(name = 'R2GC_771_372',
                        value = '(7*ee**2*complex(0,1)*G**2)/(216.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_778_373 = Coupling(name = 'R2GC_778_373',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_788_374 = Coupling(name = 'R2GC_788_374',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_788_375 = Coupling(name = 'R2GC_788_375',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_789_376 = Coupling(name = 'R2GC_789_376',
                        value = '-(ee**2*complex(0,1)*G**2)/(108.*cw*cmath.pi**2) + (cw*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_790_377 = Coupling(name = 'R2GC_790_377',
                        value = '-(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_791_378 = Coupling(name = 'R2GC_791_378',
                        value = '(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_792_379 = Coupling(name = 'R2GC_792_379',
                        value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_793_380 = Coupling(name = 'R2GC_793_380',
                        value = '(-3*ee*complex(0,1)*G**3)/(32.*cmath.pi**2*sw)',
                        order = {'QCD':3,'QED':1})

R2GC_793_381 = Coupling(name = 'R2GC_793_381',
                        value = '(ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw)',
                        order = {'QCD':3,'QED':1})

R2GC_797_382 = Coupling(name = 'R2GC_797_382',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_798_383 = Coupling(name = 'R2GC_798_383',
                        value = '(ee**2*complex(0,1)*G**2)/(108.*cw*cmath.pi**2) + (cw*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_801_384 = Coupling(name = 'R2GC_801_384',
                        value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_802_385 = Coupling(name = 'R2GC_802_385',
                        value = '(3*ee*complex(0,1)*G**3)/(32.*cmath.pi**2*sw)',
                        order = {'QCD':3,'QED':1})

R2GC_802_386 = Coupling(name = 'R2GC_802_386',
                        value = '-(ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw)',
                        order = {'QCD':3,'QED':1})

R2GC_805_387 = Coupling(name = 'R2GC_805_387',
                        value = '(complex(0,1)*G**2*y1LL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_806_388 = Coupling(name = 'R2GC_806_388',
                        value = '(complex(0,1)*G**2*y1LL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_807_389 = Coupling(name = 'R2GC_807_389',
                        value = '(complex(0,1)*G**2*y1LL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_808_390 = Coupling(name = 'R2GC_808_390',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_809_391 = Coupling(name = 'R2GC_809_391',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_810_392 = Coupling(name = 'R2GC_810_392',
                        value = '(complex(0,1)*G**2*complexconjugate(y1LL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_811_393 = Coupling(name = 'R2GC_811_393',
                        value = '-(complex(0,1)*G**2*ty1RR2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_812_394 = Coupling(name = 'R2GC_812_394',
                        value = '-(complex(0,1)*G**2*ty1RR2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_813_395 = Coupling(name = 'R2GC_813_395',
                        value = '-(complex(0,1)*G**2*ty1RR2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_814_396 = Coupling(name = 'R2GC_814_396',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_815_397 = Coupling(name = 'R2GC_815_397',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_816_398 = Coupling(name = 'R2GC_816_398',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty1RR2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_817_399 = Coupling(name = 'R2GC_817_399',
                        value = '-(complex(0,1)*G**2*ty2RL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_818_400 = Coupling(name = 'R2GC_818_400',
                        value = '-(complex(0,1)*G**2*ty2RL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_819_401 = Coupling(name = 'R2GC_819_401',
                        value = '-(complex(0,1)*G**2*ty2RL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_820_402 = Coupling(name = 'R2GC_820_402',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_821_403 = Coupling(name = 'R2GC_821_403',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_822_404 = Coupling(name = 'R2GC_822_404',
                        value = '-(complex(0,1)*G**2*complexconjugate(ty2RL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_823_405 = Coupling(name = 'R2GC_823_405',
                        value = '(complex(0,1)*G**2*ty2RL2x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_824_406 = Coupling(name = 'R2GC_824_406',
                        value = '(complex(0,1)*G**2*ty2RL2x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_825_407 = Coupling(name = 'R2GC_825_407',
                        value = '(complex(0,1)*G**2*ty2RL2x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_826_408 = Coupling(name = 'R2GC_826_408',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL2x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_827_409 = Coupling(name = 'R2GC_827_409',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL2x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_828_410 = Coupling(name = 'R2GC_828_410',
                        value = '(complex(0,1)*G**2*complexconjugate(ty2RL2x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_841_411 = Coupling(name = 'R2GC_841_411',
                        value = '(complex(0,1)*G**2*y3LL2x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_842_412 = Coupling(name = 'R2GC_842_412',
                        value = '(complex(0,1)*G**2*y3LL2x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_843_413 = Coupling(name = 'R2GC_843_413',
                        value = '(complex(0,1)*G**2*y3LL2x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_844_414 = Coupling(name = 'R2GC_844_414',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_845_415 = Coupling(name = 'R2GC_845_415',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_846_416 = Coupling(name = 'R2GC_846_416',
                        value = '(complex(0,1)*G**2*complexconjugate(y3LL2x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_848_417 = Coupling(name = 'R2GC_848_417',
                        value = '-(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_849_418 = Coupling(name = 'R2GC_849_418',
                        value = '(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_850_419 = Coupling(name = 'R2GC_850_419',
                        value = '-(complex(0,1)*G**2*y1LL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_851_420 = Coupling(name = 'R2GC_851_420',
                        value = '-(complex(0,1)*G**2*y1LL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_852_421 = Coupling(name = 'R2GC_852_421',
                        value = '-(complex(0,1)*G**2*y1LL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_853_422 = Coupling(name = 'R2GC_853_422',
                        value = '-(complex(0,1)*G**2*y1RR3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_854_423 = Coupling(name = 'R2GC_854_423',
                        value = '-(complex(0,1)*G**2*y1RR3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_855_424 = Coupling(name = 'R2GC_855_424',
                        value = '-(complex(0,1)*G**2*y1RR3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_856_425 = Coupling(name = 'R2GC_856_425',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_857_426 = Coupling(name = 'R2GC_857_426',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_858_427 = Coupling(name = 'R2GC_858_427',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_859_428 = Coupling(name = 'R2GC_859_428',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_860_429 = Coupling(name = 'R2GC_860_429',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_861_430 = Coupling(name = 'R2GC_861_430',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_862_431 = Coupling(name = 'R2GC_862_431',
                        value = '-(complex(0,1)*G**2*y2RL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_863_432 = Coupling(name = 'R2GC_863_432',
                        value = '-(complex(0,1)*G**2*y2RL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_864_433 = Coupling(name = 'R2GC_864_433',
                        value = '-(complex(0,1)*G**2*y2RL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_865_434 = Coupling(name = 'R2GC_865_434',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_866_435 = Coupling(name = 'R2GC_866_435',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_867_436 = Coupling(name = 'R2GC_867_436',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_871_437 = Coupling(name = 'R2GC_871_437',
                        value = '(complex(0,1)*G**2*y2RL3x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_872_438 = Coupling(name = 'R2GC_872_438',
                        value = '(complex(0,1)*G**2*y2RL3x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_873_439 = Coupling(name = 'R2GC_873_439',
                        value = '(complex(0,1)*G**2*y2RL3x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_877_440 = Coupling(name = 'R2GC_877_440',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL3x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_878_441 = Coupling(name = 'R2GC_878_441',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL3x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_879_442 = Coupling(name = 'R2GC_879_442',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL3x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_886_443 = Coupling(name = 'R2GC_886_443',
                        value = '-(complex(0,1)*G**2*y3LL3x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_887_444 = Coupling(name = 'R2GC_887_444',
                        value = '-(complex(0,1)*G**2*y3LL3x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_888_445 = Coupling(name = 'R2GC_888_445',
                        value = '-(complex(0,1)*G**2*y3LL3x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_889_446 = Coupling(name = 'R2GC_889_446',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL3x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_890_447 = Coupling(name = 'R2GC_890_447',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL3x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_891_448 = Coupling(name = 'R2GC_891_448',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL3x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_893_449 = Coupling(name = 'R2GC_893_449',
                        value = '-(complex(0,1)*G**2*y1LL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_894_450 = Coupling(name = 'R2GC_894_450',
                        value = '-(complex(0,1)*G**2*y1LL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_895_451 = Coupling(name = 'R2GC_895_451',
                        value = '-(complex(0,1)*G**2*y1LL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_896_452 = Coupling(name = 'R2GC_896_452',
                        value = '-(complex(0,1)*G**2*y1RR1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_897_453 = Coupling(name = 'R2GC_897_453',
                        value = '-(complex(0,1)*G**2*y1RR1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_898_454 = Coupling(name = 'R2GC_898_454',
                        value = '-(complex(0,1)*G**2*y1RR1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_899_455 = Coupling(name = 'R2GC_899_455',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_900_456 = Coupling(name = 'R2GC_900_456',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_901_457 = Coupling(name = 'R2GC_901_457',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1LL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_902_458 = Coupling(name = 'R2GC_902_458',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_903_459 = Coupling(name = 'R2GC_903_459',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_904_460 = Coupling(name = 'R2GC_904_460',
                        value = '-(complex(0,1)*G**2*complexconjugate(y1RR1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_905_461 = Coupling(name = 'R2GC_905_461',
                        value = '-(complex(0,1)*G**2*y2RL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_906_462 = Coupling(name = 'R2GC_906_462',
                        value = '-(complex(0,1)*G**2*y2RL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_907_463 = Coupling(name = 'R2GC_907_463',
                        value = '-(complex(0,1)*G**2*y2RL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_908_464 = Coupling(name = 'R2GC_908_464',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_909_465 = Coupling(name = 'R2GC_909_465',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_910_466 = Coupling(name = 'R2GC_910_466',
                        value = '-(complex(0,1)*G**2*complexconjugate(y2RL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_914_467 = Coupling(name = 'R2GC_914_467',
                        value = '(complex(0,1)*G**2*y2RL1x1)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_915_468 = Coupling(name = 'R2GC_915_468',
                        value = '(complex(0,1)*G**2*y2RL1x2)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_916_469 = Coupling(name = 'R2GC_916_469',
                        value = '(complex(0,1)*G**2*y2RL1x3)/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_920_470 = Coupling(name = 'R2GC_920_470',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL1x1))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_921_471 = Coupling(name = 'R2GC_921_471',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL1x2))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_922_472 = Coupling(name = 'R2GC_922_472',
                        value = '(complex(0,1)*G**2*complexconjugate(y2RL1x3))/(24.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

R2GC_929_473 = Coupling(name = 'R2GC_929_473',
                        value = '-(complex(0,1)*G**2*y3LL1x1)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_930_474 = Coupling(name = 'R2GC_930_474',
                        value = '-(complex(0,1)*G**2*y3LL1x2)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_931_475 = Coupling(name = 'R2GC_931_475',
                        value = '-(complex(0,1)*G**2*y3LL1x3)/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_932_476 = Coupling(name = 'R2GC_932_476',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL1x1))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_933_477 = Coupling(name = 'R2GC_933_477',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL1x2))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_934_478 = Coupling(name = 'R2GC_934_478',
                        value = '-(complex(0,1)*G**2*complexconjugate(y3LL1x3))/(12.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

R2GC_935_479 = Coupling(name = 'R2GC_935_479',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_935_480 = Coupling(name = 'R2GC_935_480',
                        value = '(ee**2*complex(0,1)*G**2)/(648.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_935_481 = Coupling(name = 'R2GC_935_481',
                        value = '(ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_936_482 = Coupling(name = 'R2GC_936_482',
                        value = '(-8*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_936_483 = Coupling(name = 'R2GC_936_483',
                        value = '(-8*ee**2*complex(0,1)*G**2*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_936_484 = Coupling(name = 'R2GC_936_484',
                        value = '(2*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_936_485 = Coupling(name = 'R2GC_936_485',
                        value = '(2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_937_486 = Coupling(name = 'R2GC_937_486',
                        value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_937_487 = Coupling(name = 'R2GC_937_487',
                        value = '(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_938_488 = Coupling(name = 'R2GC_938_488',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_938_489 = Coupling(name = 'R2GC_938_489',
                        value = '(ee**2*complex(0,1)*G**2)/(162.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_938_490 = Coupling(name = 'R2GC_938_490',
                        value = '-(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_939_491 = Coupling(name = 'R2GC_939_491',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_939_492 = Coupling(name = 'R2GC_939_492',
                        value = '(7*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_940_493 = Coupling(name = 'R2GC_940_493',
                        value = '(-25*ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_940_494 = Coupling(name = 'R2GC_940_494',
                        value = '(7*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_940_495 = Coupling(name = 'R2GC_940_495',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_942_496 = Coupling(name = 'R2GC_942_496',
                        value = '(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_942_497 = Coupling(name = 'R2GC_942_497',
                        value = '-(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_943_498 = Coupling(name = 'R2GC_943_498',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_943_499 = Coupling(name = 'R2GC_943_499',
                        value = '(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_944_500 = Coupling(name = 'R2GC_944_500',
                        value = '-(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_945_501 = Coupling(name = 'R2GC_945_501',
                        value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_945_502 = Coupling(name = 'R2GC_945_502',
                        value = '-(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_945_503 = Coupling(name = 'R2GC_945_503',
                        value = '(5*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_946_504 = Coupling(name = 'R2GC_946_504',
                        value = '-(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_946_505 = Coupling(name = 'R2GC_946_505',
                        value = '(ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_946_506 = Coupling(name = 'R2GC_946_506',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_947_507 = Coupling(name = 'R2GC_947_507',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_947_508 = Coupling(name = 'R2GC_947_508',
                        value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_947_509 = Coupling(name = 'R2GC_947_509',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_948_510 = Coupling(name = 'R2GC_948_510',
                        value = '(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_948_511 = Coupling(name = 'R2GC_948_511',
                        value = '-(ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_948_512 = Coupling(name = 'R2GC_948_512',
                        value = '(5*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_949_513 = Coupling(name = 'R2GC_949_513',
                        value = '-(ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_949_514 = Coupling(name = 'R2GC_949_514',
                        value = '(5*ee**2*complex(0,1)*G**2)/(1152.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_950_515 = Coupling(name = 'R2GC_950_515',
                        value = '(ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_950_516 = Coupling(name = 'R2GC_950_516',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_517 = Coupling(name = 'R2GC_955_517',
                        value = '(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_518 = Coupling(name = 'R2GC_955_518',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_519 = Coupling(name = 'R2GC_956_519',
                        value = '(ee**2*complex(0,1)*G**2*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_520 = Coupling(name = 'R2GC_956_520',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_521 = Coupling(name = 'R2GC_956_521',
                        value = '(5*ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_957_522 = Coupling(name = 'R2GC_957_522',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(9.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_957_523 = Coupling(name = 'R2GC_957_523',
                        value = '(ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_957_524 = Coupling(name = 'R2GC_957_524',
                        value = '(-5*ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_525 = Coupling(name = 'R2GC_958_525',
                        value = '(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_526 = Coupling(name = 'R2GC_958_526',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_527 = Coupling(name = 'R2GC_958_527',
                        value = '-(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_528 = Coupling(name = 'R2GC_958_528',
                        value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_529 = Coupling(name = 'R2GC_958_529',
                        value = '(5*ee**2*complex(0,1)*G**2)/(1296.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_958_530 = Coupling(name = 'R2GC_958_530',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_531 = Coupling(name = 'R2GC_959_531',
                        value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_532 = Coupling(name = 'R2GC_959_532',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_533 = Coupling(name = 'R2GC_959_533',
                        value = '(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_534 = Coupling(name = 'R2GC_959_534',
                        value = '-(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_535 = Coupling(name = 'R2GC_959_535',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_959_536 = Coupling(name = 'R2GC_959_536',
                        value = '(5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_960_537 = Coupling(name = 'R2GC_960_537',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_960_538 = Coupling(name = 'R2GC_960_538',
                        value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_960_539 = Coupling(name = 'R2GC_960_539',
                        value = '(5*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_961_540 = Coupling(name = 'R2GC_961_540',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_961_541 = Coupling(name = 'R2GC_961_541',
                        value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_961_542 = Coupling(name = 'R2GC_961_542',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_966_543 = Coupling(name = 'R2GC_966_543',
                        value = '(ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_966_544 = Coupling(name = 'R2GC_966_544',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_966_545 = Coupling(name = 'R2GC_966_545',
                        value = '(5*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_967_546 = Coupling(name = 'R2GC_967_546',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_967_547 = Coupling(name = 'R2GC_967_547',
                        value = '(ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_967_548 = Coupling(name = 'R2GC_967_548',
                        value = '(-5*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_972_549 = Coupling(name = 'R2GC_972_549',
                        value = '-(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_972_550 = Coupling(name = 'R2GC_972_550',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_972_551 = Coupling(name = 'R2GC_972_551',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_973_552 = Coupling(name = 'R2GC_973_552',
                        value = '(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(18.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_973_553 = Coupling(name = 'R2GC_973_553',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_973_554 = Coupling(name = 'R2GC_973_554',
                        value = '(5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_974_555 = Coupling(name = 'R2GC_974_555',
                        value = '(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_974_556 = Coupling(name = 'R2GC_974_556',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_974_557 = Coupling(name = 'R2GC_974_557',
                        value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_975_558 = Coupling(name = 'R2GC_975_558',
                        value = '-(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(18.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_975_559 = Coupling(name = 'R2GC_975_559',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_975_560 = Coupling(name = 'R2GC_975_560',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_976_561 = Coupling(name = 'R2GC_976_561',
                        value = '-(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_976_562 = Coupling(name = 'R2GC_976_562',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_976_563 = Coupling(name = 'R2GC_976_563',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_977_564 = Coupling(name = 'R2GC_977_564',
                        value = '(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(18.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_977_565 = Coupling(name = 'R2GC_977_565',
                        value = '-(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_977_566 = Coupling(name = 'R2GC_977_566',
                        value = '(5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_978_567 = Coupling(name = 'R2GC_978_567',
                        value = '(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_978_568 = Coupling(name = 'R2GC_978_568',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_978_569 = Coupling(name = 'R2GC_978_569',
                        value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) - (35*ee**2*complex(0,1)*G**2*sw**2)/(648.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_979_570 = Coupling(name = 'R2GC_979_570',
                        value = '-(ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(18.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_979_571 = Coupling(name = 'R2GC_979_571',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_979_572 = Coupling(name = 'R2GC_979_572',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (35*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_573 = Coupling(name = 'R2GC_982_573',
                        value = '(4*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_574 = Coupling(name = 'R2GC_982_574',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_575 = Coupling(name = 'R2GC_982_575',
                        value = '-(ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_576 = Coupling(name = 'R2GC_982_576',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_577 = Coupling(name = 'R2GC_982_577',
                        value = '(5*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_982_578 = Coupling(name = 'R2GC_982_578',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_983_579 = Coupling(name = 'R2GC_983_579',
                        value = '(-4*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_983_580 = Coupling(name = 'R2GC_983_580',
                        value = '(ee**2*complex(0,1)*G**2)/(3.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(9.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_983_581 = Coupling(name = 'R2GC_983_581',
                        value = '(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_983_582 = Coupling(name = 'R2GC_983_582',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_983_583 = Coupling(name = 'R2GC_983_583',
                        value = '(5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_984_584 = Coupling(name = 'R2GC_984_584',
                        value = '(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_984_585 = Coupling(name = 'R2GC_984_585',
                        value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_984_586 = Coupling(name = 'R2GC_984_586',
                        value = '(5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(324.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_985_587 = Coupling(name = 'R2GC_985_587',
                        value = '-(ee**2*complex(0,1)*G**2)/(3.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2)/(9.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_985_588 = Coupling(name = 'R2GC_985_588',
                        value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_985_589 = Coupling(name = 'R2GC_985_589',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_988_590 = Coupling(name = 'R2GC_988_590',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_988_591 = Coupling(name = 'R2GC_988_591',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_988_592 = Coupling(name = 'R2GC_988_592',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_989_593 = Coupling(name = 'R2GC_989_593',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_989_594 = Coupling(name = 'R2GC_989_594',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_989_595 = Coupling(name = 'R2GC_989_595',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_990_596 = Coupling(name = 'R2GC_990_596',
                        value = '(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_990_597 = Coupling(name = 'R2GC_990_597',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_991_598 = Coupling(name = 'R2GC_991_598',
                        value = '(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_991_599 = Coupling(name = 'R2GC_991_599',
                        value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_991_600 = Coupling(name = 'R2GC_991_600',
                        value = '(5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_992_601 = Coupling(name = 'R2GC_992_601',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_992_602 = Coupling(name = 'R2GC_992_602',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_992_603 = Coupling(name = 'R2GC_992_603',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(5184.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_993_604 = Coupling(name = 'R2GC_993_604',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_993_605 = Coupling(name = 'R2GC_993_605',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(576.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_993_606 = Coupling(name = 'R2GC_993_606',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_996_607 = Coupling(name = 'R2GC_996_607',
                        value = '-(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_996_608 = Coupling(name = 'R2GC_996_608',
                        value = '(5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(2592.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_997_609 = Coupling(name = 'R2GC_997_609',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_997_610 = Coupling(name = 'R2GC_997_610',
                        value = '(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_997_611 = Coupling(name = 'R2GC_997_611',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1000_1 = Coupling(name = 'UVGC_1000_1',
                       value = '-(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1000_2 = Coupling(name = 'UVGC_1000_2',
                       value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1000_3 = Coupling(name = 'UVGC_1000_3',
                       value = '(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1000_4 = Coupling(name = 'UVGC_1000_4',
                       value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1000_5 = Coupling(name = 'UVGC_1000_5',
                       value = '-(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1000_6 = Coupling(name = 'UVGC_1000_6',
                       value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1001_7 = Coupling(name = 'UVGC_1001_7',
                       value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1001_8 = Coupling(name = 'UVGC_1001_8',
                       value = '-(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1001_9 = Coupling(name = 'UVGC_1001_9',
                       value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

UVGC_1001_10 = Coupling(name = 'UVGC_1001_10',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1001_11 = Coupling(name = 'UVGC_1001_11',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1001_12 = Coupling(name = 'UVGC_1001_12',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_13 = Coupling(name = 'UVGC_1002_13',
                        value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_14 = Coupling(name = 'UVGC_1002_14',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_15 = Coupling(name = 'UVGC_1002_15',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_16 = Coupling(name = 'UVGC_1002_16',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_17 = Coupling(name = 'UVGC_1002_17',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1002_18 = Coupling(name = 'UVGC_1002_18',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_19 = Coupling(name = 'UVGC_1003_19',
                        value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_20 = Coupling(name = 'UVGC_1003_20',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_21 = Coupling(name = 'UVGC_1003_21',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_22 = Coupling(name = 'UVGC_1003_22',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_23 = Coupling(name = 'UVGC_1003_23',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1003_24 = Coupling(name = 'UVGC_1003_24',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1004_25 = Coupling(name = 'UVGC_1004_25',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1004_26 = Coupling(name = 'UVGC_1004_26',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1004_27 = Coupling(name = 'UVGC_1004_27',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1005_28 = Coupling(name = 'UVGC_1005_28',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1005_29 = Coupling(name = 'UVGC_1005_29',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1005_30 = Coupling(name = 'UVGC_1005_30',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1006_31 = Coupling(name = 'UVGC_1006_31',
                        value = '(-4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1006_32 = Coupling(name = 'UVGC_1006_32',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1006_33 = Coupling(name = 'UVGC_1006_33',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1006_34 = Coupling(name = 'UVGC_1006_34',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1006_35 = Coupling(name = 'UVGC_1006_35',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1007_36 = Coupling(name = 'UVGC_1007_36',
                        value = '(4*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1007_37 = Coupling(name = 'UVGC_1007_37',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1007_38 = Coupling(name = 'UVGC_1007_38',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1007_39 = Coupling(name = 'UVGC_1007_39',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1007_40 = Coupling(name = 'UVGC_1007_40',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1008_41 = Coupling(name = 'UVGC_1008_41',
                        value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1008_42 = Coupling(name = 'UVGC_1008_42',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1008_43 = Coupling(name = 'UVGC_1008_43',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1009_44 = Coupling(name = 'UVGC_1009_44',
                        value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1009_45 = Coupling(name = 'UVGC_1009_45',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1009_46 = Coupling(name = 'UVGC_1009_46',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_47 = Coupling(name = 'UVGC_1010_47',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_48 = Coupling(name = 'UVGC_1010_48',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_49 = Coupling(name = 'UVGC_1010_49',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_50 = Coupling(name = 'UVGC_1010_50',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_51 = Coupling(name = 'UVGC_1010_51',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1010_52 = Coupling(name = 'UVGC_1010_52',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_53 = Coupling(name = 'UVGC_1011_53',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_54 = Coupling(name = 'UVGC_1011_54',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_55 = Coupling(name = 'UVGC_1011_55',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_56 = Coupling(name = 'UVGC_1011_56',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_57 = Coupling(name = 'UVGC_1011_57',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1011_58 = Coupling(name = 'UVGC_1011_58',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_59 = Coupling(name = 'UVGC_1012_59',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_60 = Coupling(name = 'UVGC_1012_60',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_61 = Coupling(name = 'UVGC_1012_61',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_62 = Coupling(name = 'UVGC_1012_62',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_63 = Coupling(name = 'UVGC_1012_63',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1012_64 = Coupling(name = 'UVGC_1012_64',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_65 = Coupling(name = 'UVGC_1013_65',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_66 = Coupling(name = 'UVGC_1013_66',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_67 = Coupling(name = 'UVGC_1013_67',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_68 = Coupling(name = 'UVGC_1013_68',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_69 = Coupling(name = 'UVGC_1013_69',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1013_70 = Coupling(name = 'UVGC_1013_70',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_71 = Coupling(name = 'UVGC_1014_71',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_72 = Coupling(name = 'UVGC_1014_72',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_73 = Coupling(name = 'UVGC_1014_73',
                        value = '(3*ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_74 = Coupling(name = 'UVGC_1014_74',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_75 = Coupling(name = 'UVGC_1014_75',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1014_76 = Coupling(name = 'UVGC_1014_76',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_77 = Coupling(name = 'UVGC_1015_77',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_78 = Coupling(name = 'UVGC_1015_78',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_79 = Coupling(name = 'UVGC_1015_79',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_80 = Coupling(name = 'UVGC_1015_80',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_81 = Coupling(name = 'UVGC_1015_81',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1015_82 = Coupling(name = 'UVGC_1015_82',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1016_83 = Coupling(name = 'UVGC_1016_83',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1016_84 = Coupling(name = 'UVGC_1016_84',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1016_85 = Coupling(name = 'UVGC_1016_85',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1017_86 = Coupling(name = 'UVGC_1017_86',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1017_87 = Coupling(name = 'UVGC_1017_87',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1017_88 = Coupling(name = 'UVGC_1017_88',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1018_89 = Coupling(name = 'UVGC_1018_89',
                        value = '(-10*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1018_90 = Coupling(name = 'UVGC_1018_90',
                        value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1018_91 = Coupling(name = 'UVGC_1018_91',
                        value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1018_92 = Coupling(name = 'UVGC_1018_92',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1018_93 = Coupling(name = 'UVGC_1018_93',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1019_94 = Coupling(name = 'UVGC_1019_94',
                        value = '(10*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1019_95 = Coupling(name = 'UVGC_1019_95',
                        value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1019_96 = Coupling(name = 'UVGC_1019_96',
                        value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1019_97 = Coupling(name = 'UVGC_1019_97',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1019_98 = Coupling(name = 'UVGC_1019_98',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1020_99 = Coupling(name = 'UVGC_1020_99',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_1020_100 = Coupling(name = 'UVGC_1020_100',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1020_101 = Coupling(name = 'UVGC_1020_101',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1021_102 = Coupling(name = 'UVGC_1021_102',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1021_103 = Coupling(name = 'UVGC_1021_103',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1021_104 = Coupling(name = 'UVGC_1021_104',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_105 = Coupling(name = 'UVGC_1022_105',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_106 = Coupling(name = 'UVGC_1022_106',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_107 = Coupling(name = 'UVGC_1022_107',
                         value = '(3*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_108 = Coupling(name = 'UVGC_1022_108',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_109 = Coupling(name = 'UVGC_1022_109',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1022_110 = Coupling(name = 'UVGC_1022_110',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1023_111 = Coupling(name = 'UVGC_1023_111',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1023_112 = Coupling(name = 'UVGC_1023_112',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1023_113 = Coupling(name = 'UVGC_1023_113',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1023_114 = Coupling(name = 'UVGC_1023_114',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1023_115 = Coupling(name = 'UVGC_1023_115',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1024_116 = Coupling(name = 'UVGC_1024_116',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1024_117 = Coupling(name = 'UVGC_1024_117',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1024_118 = Coupling(name = 'UVGC_1024_118',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1025_119 = Coupling(name = 'UVGC_1025_119',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1025_120 = Coupling(name = 'UVGC_1025_120',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1025_121 = Coupling(name = 'UVGC_1025_121',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1026_122 = Coupling(name = 'UVGC_1026_122',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1026_123 = Coupling(name = 'UVGC_1026_123',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1026_124 = Coupling(name = 'UVGC_1026_124',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1027_125 = Coupling(name = 'UVGC_1027_125',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1027_126 = Coupling(name = 'UVGC_1027_126',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_1027_127 = Coupling(name = 'UVGC_1027_127',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_355_128 = Coupling(name = 'UVGC_355_128',
                        value = '(11*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_356_129 = Coupling(name = 'UVGC_356_129',
                        value = '-(complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_357_130 = Coupling(name = 'UVGC_357_130',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_358_131 = Coupling(name = 'UVGC_358_131',
                        value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_359_132 = Coupling(name = 'UVGC_359_132',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_360_133 = Coupling(name = 'UVGC_360_133',
                        value = '-(ee*FRCTdeltaZxbbRxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_361_134 = Coupling(name = 'UVGC_361_134',
                        value = 'FRCTdeltaZxbbLxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_362_135 = Coupling(name = 'UVGC_362_135',
                        value = 'FRCTdeltaZxbbRxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_363_136 = Coupling(name = 'UVGC_363_136',
                        value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_364_137 = Coupling(name = 'UVGC_364_137',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_365_138 = Coupling(name = 'UVGC_365_138',
                        value = '(2*ee*FRCTdeltaZxccLxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_366_139 = Coupling(name = 'UVGC_366_139',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_367_140 = Coupling(name = 'UVGC_367_140',
                        value = '(2*ee*FRCTdeltaZxccRxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_368_141 = Coupling(name = 'UVGC_368_141',
                        value = 'FRCTdeltaZxccLxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_369_142 = Coupling(name = 'UVGC_369_142',
                        value = 'FRCTdeltaZxccRxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_370_143 = Coupling(name = 'UVGC_370_143',
                        value = '(-2*ee*FRCTdeltaZxccRxcG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_371_144 = Coupling(name = 'UVGC_371_144',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_372_145 = Coupling(name = 'UVGC_372_145',
                        value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_373_146 = Coupling(name = 'UVGC_373_146',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_374_147 = Coupling(name = 'UVGC_374_147',
                        value = '-(ee*FRCTdeltaZxddRxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_375_148 = Coupling(name = 'UVGC_375_148',
                        value = 'FRCTdeltaZxddLxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_376_149 = Coupling(name = 'UVGC_376_149',
                        value = 'FRCTdeltaZxddRxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_377_150 = Coupling(name = 'UVGC_377_150',
                        value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_378_151 = Coupling(name = 'UVGC_378_151',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_379_152 = Coupling(name = 'UVGC_379_152',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_386_153 = Coupling(name = 'UVGC_386_153',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_387_154 = Coupling(name = 'UVGC_387_154',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_392_155 = Coupling(name = 'UVGC_392_155',
                        value = '-(FRCTdeltaZxssLxsG*complex(0,1))',
                        order = {'QCD':2})

UVGC_393_156 = Coupling(name = 'UVGC_393_156',
                        value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_394_157 = Coupling(name = 'UVGC_394_157',
                        value = '-(FRCTdeltaZxssRxsG*complex(0,1))',
                        order = {'QCD':2})

UVGC_395_158 = Coupling(name = 'UVGC_395_158',
                        value = '-(ee*FRCTdeltaZxssRxsG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_396_159 = Coupling(name = 'UVGC_396_159',
                        value = 'FRCTdeltaZxssLxsG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_397_160 = Coupling(name = 'UVGC_397_160',
                        value = 'FRCTdeltaZxssRxsG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_398_161 = Coupling(name = 'UVGC_398_161',
                        value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_399_162 = Coupling(name = 'UVGC_399_162',
                        value = '-(FRCTdeltaZxttLxtG*complex(0,1))',
                        order = {'QCD':2})

UVGC_400_163 = Coupling(name = 'UVGC_400_163',
                        value = '(2*ee*FRCTdeltaZxttLxtG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_401_164 = Coupling(name = 'UVGC_401_164',
                        value = '-(FRCTdeltaZxttRxtG*complex(0,1))',
                        order = {'QCD':2})

UVGC_402_165 = Coupling(name = 'UVGC_402_165',
                        value = '(2*ee*FRCTdeltaZxttRxtG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_403_166 = Coupling(name = 'UVGC_403_166',
                        value = 'FRCTdeltaZxttLxtG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_404_167 = Coupling(name = 'UVGC_404_167',
                        value = 'FRCTdeltaZxttRxtG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_405_168 = Coupling(name = 'UVGC_405_168',
                        value = '(-2*ee*FRCTdeltaZxttRxtG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_406_169 = Coupling(name = 'UVGC_406_169',
                        value = '-(FRCTdeltaZxuuLxuG*complex(0,1))',
                        order = {'QCD':2})

UVGC_407_170 = Coupling(name = 'UVGC_407_170',
                        value = '(2*ee*FRCTdeltaZxuuLxuG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_408_171 = Coupling(name = 'UVGC_408_171',
                        value = '-(FRCTdeltaZxuuRxuG*complex(0,1))',
                        order = {'QCD':2})

UVGC_409_172 = Coupling(name = 'UVGC_409_172',
                        value = '(2*ee*FRCTdeltaZxuuRxuG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_410_173 = Coupling(name = 'UVGC_410_173',
                        value = 'FRCTdeltaZxuuLxuG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_411_174 = Coupling(name = 'UVGC_411_174',
                        value = 'FRCTdeltaZxuuRxuG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_412_175 = Coupling(name = 'UVGC_412_175',
                        value = '(-2*ee*FRCTdeltaZxuuRxuG*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_413_176 = Coupling(name = 'UVGC_413_176',
                        value = '(3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_413_177 = Coupling(name = 'UVGC_413_177',
                        value = '(-3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_178 = Coupling(name = 'UVGC_414_178',
                        value = '(3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_179 = Coupling(name = 'UVGC_414_179',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_416_180 = Coupling(name = 'UVGC_416_180',
                        value = '-(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_416_181 = Coupling(name = 'UVGC_416_181',
                        value = '(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_417_182 = Coupling(name = 'UVGC_417_182',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_417_183 = Coupling(name = 'UVGC_417_183',
                        value = '(3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_184 = Coupling(name = 'UVGC_418_184',
                        value = '(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_185 = Coupling(name = 'UVGC_418_185',
                        value = '(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_186 = Coupling(name = 'UVGC_418_186',
                        value = '(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_187 = Coupling(name = 'UVGC_418_187',
                        value = '(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_188 = Coupling(name = 'UVGC_418_188',
                        value = '(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_189 = Coupling(name = 'UVGC_418_189',
                        value = '(FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_190 = Coupling(name = 'UVGC_418_190',
                        value = '(FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_191 = Coupling(name = 'UVGC_418_191',
                        value = '(FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_192 = Coupling(name = 'UVGC_418_192',
                        value = '(FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_193 = Coupling(name = 'UVGC_418_193',
                        value = '(FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_194 = Coupling(name = 'UVGC_418_194',
                        value = '(FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_195 = Coupling(name = 'UVGC_418_195',
                        value = '(FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_196 = Coupling(name = 'UVGC_418_196',
                        value = '(FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_197 = Coupling(name = 'UVGC_418_197',
                        value = '(FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_198 = Coupling(name = 'UVGC_418_198',
                        value = '(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_199 = Coupling(name = 'UVGC_418_199',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_418_200 = Coupling(name = 'UVGC_418_200',
                        value = '(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_419_201 = Coupling(name = 'UVGC_419_201',
                        value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_419_202 = Coupling(name = 'UVGC_419_202',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_419_203 = Coupling(name = 'UVGC_419_203',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_419_204 = Coupling(name = 'UVGC_419_204',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (31*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_205 = Coupling(name = 'UVGC_419_205',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_206 = Coupling(name = 'UVGC_419_206',
                        value = '-((FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_207 = Coupling(name = 'UVGC_419_207',
                        value = '-((FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_208 = Coupling(name = 'UVGC_419_208',
                        value = '-((FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_209 = Coupling(name = 'UVGC_419_209',
                        value = '-((FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_210 = Coupling(name = 'UVGC_419_210',
                        value = '-((FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_211 = Coupling(name = 'UVGC_419_211',
                        value = '-((FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_212 = Coupling(name = 'UVGC_419_212',
                        value = '-((FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_213 = Coupling(name = 'UVGC_419_213',
                        value = '-((FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_214 = Coupling(name = 'UVGC_419_214',
                        value = '-((FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_215 = Coupling(name = 'UVGC_419_215',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_419_216 = Coupling(name = 'UVGC_419_216',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_419_217 = Coupling(name = 'UVGC_419_217',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_218 = Coupling(name = 'UVGC_420_218',
                        value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_219 = Coupling(name = 'UVGC_420_219',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_220 = Coupling(name = 'UVGC_420_220',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_221 = Coupling(name = 'UVGC_420_221',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (37*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_222 = Coupling(name = 'UVGC_420_222',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_223 = Coupling(name = 'UVGC_420_223',
                        value = '(FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_224 = Coupling(name = 'UVGC_420_224',
                        value = '(FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_225 = Coupling(name = 'UVGC_420_225',
                        value = '(FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_226 = Coupling(name = 'UVGC_420_226',
                        value = '(FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_227 = Coupling(name = 'UVGC_420_227',
                        value = '(FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_228 = Coupling(name = 'UVGC_420_228',
                        value = '(FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_229 = Coupling(name = 'UVGC_420_229',
                        value = '(FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_230 = Coupling(name = 'UVGC_420_230',
                        value = '(FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_231 = Coupling(name = 'UVGC_420_231',
                        value = '(FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_420_232 = Coupling(name = 'UVGC_420_232',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_233 = Coupling(name = 'UVGC_420_233',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_420_234 = Coupling(name = 'UVGC_420_234',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_421_235 = Coupling(name = 'UVGC_421_235',
                        value = 'FRCTdeltaZxGGxb*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_236 = Coupling(name = 'UVGC_421_236',
                        value = 'FRCTdeltaZxGGxc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_237 = Coupling(name = 'UVGC_421_237',
                        value = 'FRCTdeltaZxGGxd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_238 = Coupling(name = 'UVGC_421_238',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)',
                        order = {'QCD':2})

UVGC_421_239 = Coupling(name = 'UVGC_421_239',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)',
                        order = {'QCD':2})

UVGC_421_240 = Coupling(name = 'UVGC_421_240',
                        value = 'FRCTdeltaZxGGxLQ1d*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_241 = Coupling(name = 'UVGC_421_241',
                        value = 'FRCTdeltaZxGGxLQ1dd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_242 = Coupling(name = 'UVGC_421_242',
                        value = 'FRCTdeltaZxGGxLQ2d*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_243 = Coupling(name = 'UVGC_421_243',
                        value = 'FRCTdeltaZxGGxLQ2pu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_244 = Coupling(name = 'UVGC_421_244',
                        value = 'FRCTdeltaZxGGxLQ2u*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_245 = Coupling(name = 'UVGC_421_245',
                        value = 'FRCTdeltaZxGGxLQ2uu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_246 = Coupling(name = 'UVGC_421_246',
                        value = 'FRCTdeltaZxGGxLQ3d*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_247 = Coupling(name = 'UVGC_421_247',
                        value = 'FRCTdeltaZxGGxLQ3dd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_248 = Coupling(name = 'UVGC_421_248',
                        value = 'FRCTdeltaZxGGxLQ3u*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_249 = Coupling(name = 'UVGC_421_249',
                        value = 'FRCTdeltaZxGGxs*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_250 = Coupling(name = 'UVGC_421_250',
                        value = 'FRCTdeltaZxGGxt*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_421_251 = Coupling(name = 'UVGC_421_251',
                        value = 'FRCTdeltaZxGGxu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_422_252 = Coupling(name = 'UVGC_422_252',
                        value = '(-3*FRCTdeltaZxGGxb*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_253 = Coupling(name = 'UVGC_422_253',
                        value = '(-3*FRCTdeltaZxGGxc*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_254 = Coupling(name = 'UVGC_422_254',
                        value = '(-3*FRCTdeltaZxGGxd*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_255 = Coupling(name = 'UVGC_422_255',
                        value = '(-3*FRCTdeltaZxGGxG*G)/2. - (15*G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_256 = Coupling(name = 'UVGC_422_256',
                        value = '(-3*FRCTdeltaZxGGxghG*G)/2. - (G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_257 = Coupling(name = 'UVGC_422_257',
                        value = '-(FRCTdeltaxaSxLQ1d*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ1d*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_258 = Coupling(name = 'UVGC_422_258',
                        value = '-(FRCTdeltaxaSxLQ1dd*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ1dd*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_259 = Coupling(name = 'UVGC_422_259',
                        value = '-(FRCTdeltaxaSxLQ2d*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ2d*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_260 = Coupling(name = 'UVGC_422_260',
                        value = '-(FRCTdeltaxaSxLQ2pu*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ2pu*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_261 = Coupling(name = 'UVGC_422_261',
                        value = '-(FRCTdeltaxaSxLQ2u*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ2u*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_262 = Coupling(name = 'UVGC_422_262',
                        value = '-(FRCTdeltaxaSxLQ2uu*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ2uu*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_263 = Coupling(name = 'UVGC_422_263',
                        value = '-(FRCTdeltaxaSxLQ3d*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ3d*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_264 = Coupling(name = 'UVGC_422_264',
                        value = '-(FRCTdeltaxaSxLQ3dd*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ3dd*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_265 = Coupling(name = 'UVGC_422_265',
                        value = '-(FRCTdeltaxaSxLQ3u*G)/(2.*aS) - (3*FRCTdeltaZxGGxLQ3u*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_266 = Coupling(name = 'UVGC_422_266',
                        value = '(-3*FRCTdeltaZxGGxs*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_267 = Coupling(name = 'UVGC_422_267',
                        value = '-(FRCTdeltaxaSxt*G)/(2.*aS) - (3*FRCTdeltaZxGGxt*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_422_268 = Coupling(name = 'UVGC_422_268',
                        value = '(-3*FRCTdeltaZxGGxu*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_423_269 = Coupling(name = 'UVGC_423_269',
                        value = '-(complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_423_270 = Coupling(name = 'UVGC_423_270',
                        value = '(complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_424_271 = Coupling(name = 'UVGC_424_271',
                        value = '(5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_272 = Coupling(name = 'UVGC_425_272',
                        value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_273 = Coupling(name = 'UVGC_425_273',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_274 = Coupling(name = 'UVGC_425_274',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_275 = Coupling(name = 'UVGC_425_275',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_276 = Coupling(name = 'UVGC_425_276',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_277 = Coupling(name = 'UVGC_425_277',
                        value = '(FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_278 = Coupling(name = 'UVGC_425_278',
                        value = '(FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_279 = Coupling(name = 'UVGC_425_279',
                        value = '(FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_280 = Coupling(name = 'UVGC_425_280',
                        value = '(FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_281 = Coupling(name = 'UVGC_425_281',
                        value = '(FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_282 = Coupling(name = 'UVGC_425_282',
                        value = '(FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_283 = Coupling(name = 'UVGC_425_283',
                        value = '(FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_284 = Coupling(name = 'UVGC_425_284',
                        value = '(FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_285 = Coupling(name = 'UVGC_425_285',
                        value = '(FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_286 = Coupling(name = 'UVGC_425_286',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_287 = Coupling(name = 'UVGC_425_287',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_425_288 = Coupling(name = 'UVGC_425_288',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_426_289 = Coupling(name = 'UVGC_426_289',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_290 = Coupling(name = 'UVGC_427_290',
                        value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_291 = Coupling(name = 'UVGC_427_291',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_292 = Coupling(name = 'UVGC_427_292',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_293 = Coupling(name = 'UVGC_427_293',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_294 = Coupling(name = 'UVGC_427_294',
                        value = '-((FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_295 = Coupling(name = 'UVGC_427_295',
                        value = '-((FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_296 = Coupling(name = 'UVGC_427_296',
                        value = '-((FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_297 = Coupling(name = 'UVGC_427_297',
                        value = '-((FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_298 = Coupling(name = 'UVGC_427_298',
                        value = '-((FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_299 = Coupling(name = 'UVGC_427_299',
                        value = '-((FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_300 = Coupling(name = 'UVGC_427_300',
                        value = '-((FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_301 = Coupling(name = 'UVGC_427_301',
                        value = '-((FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_302 = Coupling(name = 'UVGC_427_302',
                        value = '-((FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_303 = Coupling(name = 'UVGC_427_303',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_304 = Coupling(name = 'UVGC_427_304',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_427_305 = Coupling(name = 'UVGC_427_305',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_306 = Coupling(name = 'UVGC_428_306',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_307 = Coupling(name = 'UVGC_428_307',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_308 = Coupling(name = 'UVGC_428_308',
                        value = '-((FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_309 = Coupling(name = 'UVGC_428_309',
                        value = '-((FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_310 = Coupling(name = 'UVGC_428_310',
                        value = '-((FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_311 = Coupling(name = 'UVGC_428_311',
                        value = '-((FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_312 = Coupling(name = 'UVGC_428_312',
                        value = '-((FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_313 = Coupling(name = 'UVGC_428_313',
                        value = '-((FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_314 = Coupling(name = 'UVGC_428_314',
                        value = '-((FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_315 = Coupling(name = 'UVGC_428_315',
                        value = '-((FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_316 = Coupling(name = 'UVGC_428_316',
                        value = '-((FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_429_317 = Coupling(name = 'UVGC_429_317',
                        value = '(complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_430_318 = Coupling(name = 'UVGC_430_318',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_431_319 = Coupling(name = 'UVGC_431_319',
                        value = '(-13*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_432_320 = Coupling(name = 'UVGC_432_320',
                        value = '-(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_433_321 = Coupling(name = 'UVGC_433_321',
                        value = '(ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_435_322 = Coupling(name = 'UVGC_435_322',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_437_323 = Coupling(name = 'UVGC_437_323',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_442_324 = Coupling(name = 'UVGC_442_324',
                        value = '-(cw*ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_444_325 = Coupling(name = 'UVGC_444_325',
                        value = 'FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_445_326 = Coupling(name = 'UVGC_445_326',
                        value = '(ee*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_446_327 = Coupling(name = 'UVGC_446_327',
                        value = '(2*ee**2*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_447_328 = Coupling(name = 'UVGC_447_328',
                        value = '-(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_329 = Coupling(name = 'UVGC_447_329',
                        value = '-(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_330 = Coupling(name = 'UVGC_447_330',
                        value = '-(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_331 = Coupling(name = 'UVGC_447_331',
                        value = '-(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_332 = Coupling(name = 'UVGC_447_332',
                        value = '-(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_333 = Coupling(name = 'UVGC_447_333',
                        value = '-(FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_334 = Coupling(name = 'UVGC_447_334',
                        value = '-(FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_335 = Coupling(name = 'UVGC_447_335',
                        value = '-(FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_336 = Coupling(name = 'UVGC_447_336',
                        value = '-(FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_337 = Coupling(name = 'UVGC_447_337',
                        value = '-(FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_338 = Coupling(name = 'UVGC_447_338',
                        value = '-(FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_339 = Coupling(name = 'UVGC_447_339',
                        value = '-(FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_340 = Coupling(name = 'UVGC_447_340',
                        value = '-(FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_341 = Coupling(name = 'UVGC_447_341',
                        value = '-(FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_342 = Coupling(name = 'UVGC_447_342',
                        value = '-(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_343 = Coupling(name = 'UVGC_447_343',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_344 = Coupling(name = 'UVGC_447_344',
                        value = '-(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_447_345 = Coupling(name = 'UVGC_447_345',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_448_346 = Coupling(name = 'UVGC_448_346',
                        value = '-(ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_347 = Coupling(name = 'UVGC_448_347',
                        value = '-(ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_348 = Coupling(name = 'UVGC_448_348',
                        value = '-(ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_349 = Coupling(name = 'UVGC_448_349',
                        value = '-(ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_350 = Coupling(name = 'UVGC_448_350',
                        value = '-(ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_351 = Coupling(name = 'UVGC_448_351',
                        value = '-(ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_352 = Coupling(name = 'UVGC_448_352',
                        value = '-(ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_353 = Coupling(name = 'UVGC_448_353',
                        value = '-(ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_354 = Coupling(name = 'UVGC_448_354',
                        value = '-(ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_355 = Coupling(name = 'UVGC_448_355',
                        value = '-(ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_356 = Coupling(name = 'UVGC_448_356',
                        value = '-(ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_357 = Coupling(name = 'UVGC_448_357',
                        value = '-(ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_358 = Coupling(name = 'UVGC_448_358',
                        value = '-(ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_359 = Coupling(name = 'UVGC_448_359',
                        value = '-(ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_360 = Coupling(name = 'UVGC_448_360',
                        value = '-(ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_361 = Coupling(name = 'UVGC_448_361',
                        value = '-(ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_362 = Coupling(name = 'UVGC_448_362',
                        value = '-(ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_448_363 = Coupling(name = 'UVGC_448_363',
                        value = '(-2*ee*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_449_364 = Coupling(name = 'UVGC_449_364',
                        value = '(-13*complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_449_365 = Coupling(name = 'UVGC_449_365',
                        value = '(13*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_450_366 = Coupling(name = 'UVGC_450_366',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_450_367 = Coupling(name = 'UVGC_450_367',
                        value = '(3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_451_368 = Coupling(name = 'UVGC_451_368',
                        value = 'FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_369 = Coupling(name = 'UVGC_451_369',
                        value = 'FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_370 = Coupling(name = 'UVGC_451_370',
                        value = 'FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_371 = Coupling(name = 'UVGC_451_371',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)*G**2 - (9*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_451_372 = Coupling(name = 'UVGC_451_372',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_373 = Coupling(name = 'UVGC_451_373',
                        value = '(FRCTdeltaxaSxLQ1d*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ1d*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_374 = Coupling(name = 'UVGC_451_374',
                        value = '(FRCTdeltaxaSxLQ1dd*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ1dd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_375 = Coupling(name = 'UVGC_451_375',
                        value = '(FRCTdeltaxaSxLQ2d*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ2d*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_376 = Coupling(name = 'UVGC_451_376',
                        value = '(FRCTdeltaxaSxLQ2pu*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ2pu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_377 = Coupling(name = 'UVGC_451_377',
                        value = '(FRCTdeltaxaSxLQ2u*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ2u*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_378 = Coupling(name = 'UVGC_451_378',
                        value = '(FRCTdeltaxaSxLQ2uu*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ2uu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_379 = Coupling(name = 'UVGC_451_379',
                        value = '(FRCTdeltaxaSxLQ3d*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ3d*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_380 = Coupling(name = 'UVGC_451_380',
                        value = '(FRCTdeltaxaSxLQ3dd*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ3dd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_381 = Coupling(name = 'UVGC_451_381',
                        value = '(FRCTdeltaxaSxLQ3u*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxLQ3u*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_382 = Coupling(name = 'UVGC_451_382',
                        value = 'FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_383 = Coupling(name = 'UVGC_451_383',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_384 = Coupling(name = 'UVGC_451_384',
                        value = 'FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_451_385 = Coupling(name = 'UVGC_451_385',
                        value = 'FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_452_386 = Coupling(name = 'UVGC_452_386',
                        value = '-2*FRCTdeltaxMS1xGLQ1d*complex(0,1)*MS1 - FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*MS1**2 + (complex(0,1)*G**2*invFREps*MS1**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_453_387 = Coupling(name = 'UVGC_453_387',
                        value = '-(ee*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_454_388 = Coupling(name = 'UVGC_454_388',
                        value = '(-2*ee**2*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*sw)/(9.*cw) - (ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_455_389 = Coupling(name = 'UVGC_455_389',
                        value = '(ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_390 = Coupling(name = 'UVGC_455_390',
                        value = '(ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_391 = Coupling(name = 'UVGC_455_391',
                        value = '(ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_392 = Coupling(name = 'UVGC_455_392',
                        value = '(ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_393 = Coupling(name = 'UVGC_455_393',
                        value = '(ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_394 = Coupling(name = 'UVGC_455_394',
                        value = '(ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_395 = Coupling(name = 'UVGC_455_395',
                        value = '(ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_396 = Coupling(name = 'UVGC_455_396',
                        value = '(ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_397 = Coupling(name = 'UVGC_455_397',
                        value = '(ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_398 = Coupling(name = 'UVGC_455_398',
                        value = '(ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_399 = Coupling(name = 'UVGC_455_399',
                        value = '(ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_400 = Coupling(name = 'UVGC_455_400',
                        value = '(ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_401 = Coupling(name = 'UVGC_455_401',
                        value = '(ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_402 = Coupling(name = 'UVGC_455_402',
                        value = '(ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_403 = Coupling(name = 'UVGC_455_403',
                        value = '(ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_404 = Coupling(name = 'UVGC_455_404',
                        value = '(ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_405 = Coupling(name = 'UVGC_455_405',
                        value = '(ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_455_406 = Coupling(name = 'UVGC_455_406',
                        value = '(2*ee*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*G*sw)/(3.*cw) - (ee*complex(0,1)*G**3*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_456_407 = Coupling(name = 'UVGC_456_407',
                        value = '(2*ee**2*FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*sw**2)/(9.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_457_408 = Coupling(name = 'UVGC_457_408',
                        value = 'FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_458_409 = Coupling(name = 'UVGC_458_409',
                        value = '(4*ee*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1))/3. + (2*ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_459_410 = Coupling(name = 'UVGC_459_410',
                        value = '(32*ee**2*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1))/9. + (16*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_460_411 = Coupling(name = 'UVGC_460_411',
                        value = '-(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_461_412 = Coupling(name = 'UVGC_461_412',
                        value = '(-4*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_413 = Coupling(name = 'UVGC_461_413',
                        value = '(-4*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_414 = Coupling(name = 'UVGC_461_414',
                        value = '(-4*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_415 = Coupling(name = 'UVGC_461_415',
                        value = '(-4*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_416 = Coupling(name = 'UVGC_461_416',
                        value = '(-4*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_417 = Coupling(name = 'UVGC_461_417',
                        value = '(-4*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_418 = Coupling(name = 'UVGC_461_418',
                        value = '(-4*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_419 = Coupling(name = 'UVGC_461_419',
                        value = '(-4*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_420 = Coupling(name = 'UVGC_461_420',
                        value = '(-4*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_421 = Coupling(name = 'UVGC_461_421',
                        value = '(-4*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_422 = Coupling(name = 'UVGC_461_422',
                        value = '(-4*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_423 = Coupling(name = 'UVGC_461_423',
                        value = '(-4*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_424 = Coupling(name = 'UVGC_461_424',
                        value = '(-4*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_425 = Coupling(name = 'UVGC_461_425',
                        value = '(-4*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_426 = Coupling(name = 'UVGC_461_426',
                        value = '(-4*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_427 = Coupling(name = 'UVGC_461_427',
                        value = '(-4*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) - (4*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_428 = Coupling(name = 'UVGC_461_428',
                        value = '(-4*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_461_429 = Coupling(name = 'UVGC_461_429',
                        value = '(-8*ee*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_464_430 = Coupling(name = 'UVGC_464_430',
                        value = 'FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_465_431 = Coupling(name = 'UVGC_465_431',
                        value = '-2*FRCTdeltaxMS1xGLQ1d*complex(0,1)*MS1',
                        order = {'QCD':2})

UVGC_465_432 = Coupling(name = 'UVGC_465_432',
                        value = '-(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*MS1**2) + (complex(0,1)*G**2*invFREps*MS1**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_466_433 = Coupling(name = 'UVGC_466_433',
                        value = '(-4*ee*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*sw)/(3.*cw) - (2*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_467_434 = Coupling(name = 'UVGC_467_434',
                        value = '(-32*ee**2*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*sw)/(9.*cw) - (16*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_468_435 = Coupling(name = 'UVGC_468_435',
                        value = '(4*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_436 = Coupling(name = 'UVGC_468_436',
                        value = '(4*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_437 = Coupling(name = 'UVGC_468_437',
                        value = '(4*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_438 = Coupling(name = 'UVGC_468_438',
                        value = '(4*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_439 = Coupling(name = 'UVGC_468_439',
                        value = '(4*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_440 = Coupling(name = 'UVGC_468_440',
                        value = '(4*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_441 = Coupling(name = 'UVGC_468_441',
                        value = '(4*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_442 = Coupling(name = 'UVGC_468_442',
                        value = '(4*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_443 = Coupling(name = 'UVGC_468_443',
                        value = '(4*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_444 = Coupling(name = 'UVGC_468_444',
                        value = '(4*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_445 = Coupling(name = 'UVGC_468_445',
                        value = '(4*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_446 = Coupling(name = 'UVGC_468_446',
                        value = '(4*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_447 = Coupling(name = 'UVGC_468_447',
                        value = '(4*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_448 = Coupling(name = 'UVGC_468_448',
                        value = '(4*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_449 = Coupling(name = 'UVGC_468_449',
                        value = '(4*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_450 = Coupling(name = 'UVGC_468_450',
                        value = '(4*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*cw) + (4*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_451 = Coupling(name = 'UVGC_468_451',
                        value = '(4*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_468_452 = Coupling(name = 'UVGC_468_452',
                        value = '(8*ee*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*G*sw)/(3.*cw) - (ee*complex(0,1)*G**3*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_469_453 = Coupling(name = 'UVGC_469_453',
                        value = '(32*ee**2*FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*sw**2)/(9.*cw**2) + (16*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_470_454 = Coupling(name = 'UVGC_470_454',
                        value = 'FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_471_455 = Coupling(name = 'UVGC_471_455',
                        value = '(ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_472_456 = Coupling(name = 'UVGC_472_456',
                        value = '(2*ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_473_457 = Coupling(name = 'UVGC_473_457',
                        value = '-(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_474_458 = Coupling(name = 'UVGC_474_458',
                        value = '(-2*ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_477_459 = Coupling(name = 'UVGC_477_459',
                        value = 'FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_478_460 = Coupling(name = 'UVGC_478_460',
                        value = '(cw*ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) + (ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_479_461 = Coupling(name = 'UVGC_479_461',
                        value = '(cw*ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(3.*sw) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw) + (ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*sw)/(9.*cw) + (ee**2*complex(0,1)*G**2*invFREps*sw)/(54.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_480_462 = Coupling(name = 'UVGC_480_462',
                        value = '-(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_463 = Coupling(name = 'UVGC_480_463',
                        value = '-(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_464 = Coupling(name = 'UVGC_480_464',
                        value = '-(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_465 = Coupling(name = 'UVGC_480_465',
                        value = '-(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_466 = Coupling(name = 'UVGC_480_466',
                        value = '-(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_467 = Coupling(name = 'UVGC_480_467',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_468 = Coupling(name = 'UVGC_480_468',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_469 = Coupling(name = 'UVGC_480_469',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_470 = Coupling(name = 'UVGC_480_470',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_471 = Coupling(name = 'UVGC_480_471',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_472 = Coupling(name = 'UVGC_480_472',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_473 = Coupling(name = 'UVGC_480_473',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_474 = Coupling(name = 'UVGC_480_474',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_475 = Coupling(name = 'UVGC_480_475',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_476 = Coupling(name = 'UVGC_480_476',
                        value = '-(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_477 = Coupling(name = 'UVGC_480_477',
                        value = '-(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_478 = Coupling(name = 'UVGC_480_478',
                        value = '-(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_480_479 = Coupling(name = 'UVGC_480_479',
                        value = '-((cw*ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G)/sw) + (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw) - (ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G*sw)/(3.*cw) + (ee*complex(0,1)*G**3*invFREps*sw)/(144.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_481_480 = Coupling(name = 'UVGC_481_480',
                        value = '(ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/3. + (ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*sw**2)/(18.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_482_481 = Coupling(name = 'UVGC_482_481',
                        value = 'FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_483_482 = Coupling(name = 'UVGC_483_482',
                        value = '(-2*ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_484_483 = Coupling(name = 'UVGC_484_483',
                        value = '(8*ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_485_484 = Coupling(name = 'UVGC_485_484',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_486_485 = Coupling(name = 'UVGC_486_485',
                        value = '(2*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_486 = Coupling(name = 'UVGC_486_486',
                        value = '(2*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_487 = Coupling(name = 'UVGC_486_487',
                        value = '(2*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_488 = Coupling(name = 'UVGC_486_488',
                        value = '(2*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_489 = Coupling(name = 'UVGC_486_489',
                        value = '(2*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_490 = Coupling(name = 'UVGC_486_490',
                        value = '(2*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_491 = Coupling(name = 'UVGC_486_491',
                        value = '(2*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_492 = Coupling(name = 'UVGC_486_492',
                        value = '(2*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_493 = Coupling(name = 'UVGC_486_493',
                        value = '(2*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_494 = Coupling(name = 'UVGC_486_494',
                        value = '(2*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_495 = Coupling(name = 'UVGC_486_495',
                        value = '(2*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_496 = Coupling(name = 'UVGC_486_496',
                        value = '(2*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_497 = Coupling(name = 'UVGC_486_497',
                        value = '(2*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_498 = Coupling(name = 'UVGC_486_498',
                        value = '(2*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_499 = Coupling(name = 'UVGC_486_499',
                        value = '(2*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_500 = Coupling(name = 'UVGC_486_500',
                        value = '(2*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_501 = Coupling(name = 'UVGC_486_501',
                        value = '(2*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_486_502 = Coupling(name = 'UVGC_486_502',
                        value = '(4*ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_489_503 = Coupling(name = 'UVGC_489_503',
                        value = 'FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_490_504 = Coupling(name = 'UVGC_490_504',
                        value = '-(cw*ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) + (ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_491_505 = Coupling(name = 'UVGC_491_505',
                        value = '(2*cw*ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(3.*sw) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw) - (2*ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*sw)/(9.*cw) - (ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_492_506 = Coupling(name = 'UVGC_492_506',
                        value = '(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_507 = Coupling(name = 'UVGC_492_507',
                        value = '(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_508 = Coupling(name = 'UVGC_492_508',
                        value = '(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_509 = Coupling(name = 'UVGC_492_509',
                        value = '(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_510 = Coupling(name = 'UVGC_492_510',
                        value = '(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_511 = Coupling(name = 'UVGC_492_511',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_512 = Coupling(name = 'UVGC_492_512',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_513 = Coupling(name = 'UVGC_492_513',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_514 = Coupling(name = 'UVGC_492_514',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_515 = Coupling(name = 'UVGC_492_515',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_516 = Coupling(name = 'UVGC_492_516',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_517 = Coupling(name = 'UVGC_492_517',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_518 = Coupling(name = 'UVGC_492_518',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_519 = Coupling(name = 'UVGC_492_519',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_520 = Coupling(name = 'UVGC_492_520',
                        value = '(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_521 = Coupling(name = 'UVGC_492_521',
                        value = '(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(6.*aS*cw) - (ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_522 = Coupling(name = 'UVGC_492_522',
                        value = '(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*sw) - (ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_492_523 = Coupling(name = 'UVGC_492_523',
                        value = '(cw*ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G)/sw - (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw) - (ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G*sw)/(3.*cw) + (ee*complex(0,1)*G**3*invFREps*sw)/(144.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_493_524 = Coupling(name = 'UVGC_493_524',
                        value = '-(ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/3. - (ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*sw**2)/(18.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_494_525 = Coupling(name = 'UVGC_494_525',
                        value = 'FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_495_526 = Coupling(name = 'UVGC_495_526',
                        value = '(-2*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_496_527 = Coupling(name = 'UVGC_496_527',
                        value = '(8*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_497_528 = Coupling(name = 'UVGC_497_528',
                        value = '-(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_498_529 = Coupling(name = 'UVGC_498_529',
                        value = '(4*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_501_530 = Coupling(name = 'UVGC_501_530',
                        value = 'FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_502_531 = Coupling(name = 'UVGC_502_531',
                        value = '(cw*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) + (7*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*sw)/(6.*cw) + (7*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_503_532 = Coupling(name = 'UVGC_503_532',
                        value = '(-2*cw*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(3.*sw) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw) - (14*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*sw)/(9.*cw) - (7*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_504_533 = Coupling(name = 'UVGC_504_533',
                        value = '-(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_534 = Coupling(name = 'UVGC_504_534',
                        value = '-(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_535 = Coupling(name = 'UVGC_504_535',
                        value = '-(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_536 = Coupling(name = 'UVGC_504_536',
                        value = '-(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_537 = Coupling(name = 'UVGC_504_537',
                        value = '-(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_538 = Coupling(name = 'UVGC_504_538',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_539 = Coupling(name = 'UVGC_504_539',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_540 = Coupling(name = 'UVGC_504_540',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_541 = Coupling(name = 'UVGC_504_541',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_542 = Coupling(name = 'UVGC_504_542',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_543 = Coupling(name = 'UVGC_504_543',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_544 = Coupling(name = 'UVGC_504_544',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_545 = Coupling(name = 'UVGC_504_545',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_546 = Coupling(name = 'UVGC_504_546',
                        value = '-(cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_547 = Coupling(name = 'UVGC_504_547',
                        value = '-(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_548 = Coupling(name = 'UVGC_504_548',
                        value = '-(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*sw) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_549 = Coupling(name = 'UVGC_504_549',
                        value = '-(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_504_550 = Coupling(name = 'UVGC_504_550',
                        value = '-((cw*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G)/sw) + (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw) - (7*ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G*sw)/(3.*cw) + (7*ee*complex(0,1)*G**3*invFREps*sw)/(144.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_505_551 = Coupling(name = 'UVGC_505_551',
                        value = '(7*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/3. + (7*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (49*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*sw**2)/(18.*cw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_506_552 = Coupling(name = 'UVGC_506_552',
                        value = '-(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*MR2**2) + (complex(0,1)*G**2*invFREps*MR2**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_506_553 = Coupling(name = 'UVGC_506_553',
                        value = '-2*FRCTdeltaxMR2xGLQ2uu*complex(0,1)*MR2',
                        order = {'QCD':2})

UVGC_507_554 = Coupling(name = 'UVGC_507_554',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*MR2**2) + (complex(0,1)*G**2*invFREps*MR2**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_508_555 = Coupling(name = 'UVGC_508_555',
                        value = '-(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*MR2**2) + (complex(0,1)*G**2*invFREps*MR2**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_509_556 = Coupling(name = 'UVGC_509_556',
                        value = 'FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_510_557 = Coupling(name = 'UVGC_510_557',
                        value = '(-5*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/3. - (5*ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_511_558 = Coupling(name = 'UVGC_511_558',
                        value = '(50*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/9. + (25*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_512_559 = Coupling(name = 'UVGC_512_559',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_513_560 = Coupling(name = 'UVGC_513_560',
                        value = '(5*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_561 = Coupling(name = 'UVGC_513_561',
                        value = '(5*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_562 = Coupling(name = 'UVGC_513_562',
                        value = '(5*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_563 = Coupling(name = 'UVGC_513_563',
                        value = '(5*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_564 = Coupling(name = 'UVGC_513_564',
                        value = '(5*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_565 = Coupling(name = 'UVGC_513_565',
                        value = '(5*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_566 = Coupling(name = 'UVGC_513_566',
                        value = '(5*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_567 = Coupling(name = 'UVGC_513_567',
                        value = '(5*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_568 = Coupling(name = 'UVGC_513_568',
                        value = '(5*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_569 = Coupling(name = 'UVGC_513_569',
                        value = '(5*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_570 = Coupling(name = 'UVGC_513_570',
                        value = '(5*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_571 = Coupling(name = 'UVGC_513_571',
                        value = '(5*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_572 = Coupling(name = 'UVGC_513_572',
                        value = '(5*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_573 = Coupling(name = 'UVGC_513_573',
                        value = '(5*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_574 = Coupling(name = 'UVGC_513_574',
                        value = '(5*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_575 = Coupling(name = 'UVGC_513_575',
                        value = '(5*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) + (5*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_576 = Coupling(name = 'UVGC_513_576',
                        value = '(5*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_513_577 = Coupling(name = 'UVGC_513_577',
                        value = '(10*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G)/3. - (5*ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_516_578 = Coupling(name = 'UVGC_516_578',
                        value = 'FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_517_579 = Coupling(name = 'UVGC_517_579',
                        value = '-2*FRCTdeltaxMR2xGLQ2uu*complex(0,1)*MR2 - FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*MR2**2 + (complex(0,1)*G**2*invFREps*MR2**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_518_580 = Coupling(name = 'UVGC_518_580',
                        value = '-(cw*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) + (7*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*sw)/(6.*cw) + (7*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_519_581 = Coupling(name = 'UVGC_519_581',
                        value = '(5*cw*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(3.*sw) + (5*cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw) - (35*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*sw)/(9.*cw) - (35*ee**2*complex(0,1)*G**2*invFREps*sw)/(54.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_520_582 = Coupling(name = 'UVGC_520_582',
                        value = '(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_583 = Coupling(name = 'UVGC_520_583',
                        value = '(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_584 = Coupling(name = 'UVGC_520_584',
                        value = '(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_585 = Coupling(name = 'UVGC_520_585',
                        value = '(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_586 = Coupling(name = 'UVGC_520_586',
                        value = '(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_587 = Coupling(name = 'UVGC_520_587',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_588 = Coupling(name = 'UVGC_520_588',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_589 = Coupling(name = 'UVGC_520_589',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_590 = Coupling(name = 'UVGC_520_590',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_591 = Coupling(name = 'UVGC_520_591',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_592 = Coupling(name = 'UVGC_520_592',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_593 = Coupling(name = 'UVGC_520_593',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_594 = Coupling(name = 'UVGC_520_594',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_595 = Coupling(name = 'UVGC_520_595',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_596 = Coupling(name = 'UVGC_520_596',
                        value = '(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_597 = Coupling(name = 'UVGC_520_597',
                        value = '(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*sw) + (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(6.*aS*cw) - (7*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_598 = Coupling(name = 'UVGC_520_598',
                        value = '(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*sw) - (7*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(6.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_520_599 = Coupling(name = 'UVGC_520_599',
                        value = '(cw*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G)/sw - (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw) - (7*ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G*sw)/(3.*cw) + (7*ee*complex(0,1)*G**3*invFREps*sw)/(144.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_521_600 = Coupling(name = 'UVGC_521_600',
                        value = '(-7*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/3. - (7*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (49*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*sw**2)/(18.*cw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_522_601 = Coupling(name = 'UVGC_522_601',
                        value = 'FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_523_602 = Coupling(name = 'UVGC_523_602',
                        value = '(ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_524_603 = Coupling(name = 'UVGC_524_603',
                        value = '(2*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_525_604 = Coupling(name = 'UVGC_525_604',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_526_605 = Coupling(name = 'UVGC_526_605',
                        value = '(-2*ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_529_606 = Coupling(name = 'UVGC_529_606',
                        value = 'FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_530_607 = Coupling(name = 'UVGC_530_607',
                        value = '-(ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_531_608 = Coupling(name = 'UVGC_531_608',
                        value = '(-2*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*sw)/(9.*cw) - (ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_532_609 = Coupling(name = 'UVGC_532_609',
                        value = '(2*ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G*sw)/(3.*cw) - (ee*complex(0,1)*G**3*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_533_610 = Coupling(name = 'UVGC_533_610',
                        value = '(2*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*sw**2)/(9.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_534_611 = Coupling(name = 'UVGC_534_611',
                        value = 'FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_535_612 = Coupling(name = 'UVGC_535_612',
                        value = '(4*ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/3. + (2*ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_536_613 = Coupling(name = 'UVGC_536_613',
                        value = '(32*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/9. + (16*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_537_614 = Coupling(name = 'UVGC_537_614',
                        value = '-(FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_538_615 = Coupling(name = 'UVGC_538_615',
                        value = '(-8*ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_541_616 = Coupling(name = 'UVGC_541_616',
                        value = 'FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_542_617 = Coupling(name = 'UVGC_542_617',
                        value = '(cw*ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/sw + (cw*ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw) - (ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_543_618 = Coupling(name = 'UVGC_543_618',
                        value = '(8*cw*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(3.*sw) + (4*cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw) - (8*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*sw)/(9.*cw) - (4*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_544_619 = Coupling(name = 'UVGC_544_619',
                        value = '-((cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_620 = Coupling(name = 'UVGC_544_620',
                        value = '-((cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_621 = Coupling(name = 'UVGC_544_621',
                        value = '-((cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_622 = Coupling(name = 'UVGC_544_622',
                        value = '-((cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_623 = Coupling(name = 'UVGC_544_623',
                        value = '-((cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_624 = Coupling(name = 'UVGC_544_624',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_625 = Coupling(name = 'UVGC_544_625',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_626 = Coupling(name = 'UVGC_544_626',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_627 = Coupling(name = 'UVGC_544_627',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_628 = Coupling(name = 'UVGC_544_628',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_629 = Coupling(name = 'UVGC_544_629',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_630 = Coupling(name = 'UVGC_544_630',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_631 = Coupling(name = 'UVGC_544_631',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_632 = Coupling(name = 'UVGC_544_632',
                        value = '-((cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_633 = Coupling(name = 'UVGC_544_633',
                        value = '-((cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_634 = Coupling(name = 'UVGC_544_634',
                        value = '-((cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw)) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_635 = Coupling(name = 'UVGC_544_635',
                        value = '-((cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/sw) + (ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_544_636 = Coupling(name = 'UVGC_544_636',
                        value = '(-2*cw*ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G)/sw + (cw*ee*complex(0,1)*G**3*invFREps)/(24.*cmath.pi**2*sw) + (2*ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G*sw)/(3.*cw) - (ee*complex(0,1)*G**3*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_545_637 = Coupling(name = 'UVGC_545_637',
                        value = '(-4*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/3. - (2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (2*cw**2*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/sw**2 + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2) + (2*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*sw**2)/(9.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_546_638 = Coupling(name = 'UVGC_546_638',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*MS3**2) + (complex(0,1)*G**2*invFREps*MS3**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_546_639 = Coupling(name = 'UVGC_546_639',
                        value = '-2*FRCTdeltaxMS3xGLQ3u*complex(0,1)*MS3',
                        order = {'QCD':2})

UVGC_547_640 = Coupling(name = 'UVGC_547_640',
                        value = '-(FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*MS3**2) + (complex(0,1)*G**2*invFREps*MS3**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_548_641 = Coupling(name = 'UVGC_548_641',
                        value = 'FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_549_642 = Coupling(name = 'UVGC_549_642',
                        value = '(-2*ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_550_643 = Coupling(name = 'UVGC_550_643',
                        value = '(8*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_551_644 = Coupling(name = 'UVGC_551_644',
                        value = '-(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_552_645 = Coupling(name = 'UVGC_552_645',
                        value = '(4*ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_555_646 = Coupling(name = 'UVGC_555_646',
                        value = 'FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_556_647 = Coupling(name = 'UVGC_556_647',
                        value = '-2*FRCTdeltaxMS3xGLQ3u*complex(0,1)*MS3 - FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*MS3**2 + (complex(0,1)*G**2*invFREps*MS3**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_557_648 = Coupling(name = 'UVGC_557_648',
                        value = '-((cw*ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw) - (ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_558_649 = Coupling(name = 'UVGC_558_649',
                        value = '(4*cw*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(3.*sw) + (2*cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw) + (4*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*sw)/(9.*cw) + (2*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_559_650 = Coupling(name = 'UVGC_559_650',
                        value = '(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_651 = Coupling(name = 'UVGC_559_651',
                        value = '(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_652 = Coupling(name = 'UVGC_559_652',
                        value = '(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_653 = Coupling(name = 'UVGC_559_653',
                        value = '(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_654 = Coupling(name = 'UVGC_559_654',
                        value = '(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_655 = Coupling(name = 'UVGC_559_655',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_656 = Coupling(name = 'UVGC_559_656',
                        value = '(cw*ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_657 = Coupling(name = 'UVGC_559_657',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_658 = Coupling(name = 'UVGC_559_658',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_659 = Coupling(name = 'UVGC_559_659',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_660 = Coupling(name = 'UVGC_559_660',
                        value = '(cw*ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_661 = Coupling(name = 'UVGC_559_661',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_662 = Coupling(name = 'UVGC_559_662',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_663 = Coupling(name = 'UVGC_559_663',
                        value = '(cw*ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_664 = Coupling(name = 'UVGC_559_664',
                        value = '(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_665 = Coupling(name = 'UVGC_559_665',
                        value = '(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw) + (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/sw + (ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*cw) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_666 = Coupling(name = 'UVGC_559_666',
                        value = '(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/sw + (ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*cw)',
                        order = {'QCD':3,'QED':1})

UVGC_559_667 = Coupling(name = 'UVGC_559_667',
                        value = '(2*cw*ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G)/sw - (cw*ee*complex(0,1)*G**3*invFREps)/(24.*cmath.pi**2*sw) + (2*ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G*sw)/(3.*cw) - (ee*complex(0,1)*G**3*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_560_668 = Coupling(name = 'UVGC_560_668',
                        value = '(4*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/3. + (2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (2*cw**2*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/sw**2 + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2) + (2*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*sw**2)/(9.*cw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_564_669 = Coupling(name = 'UVGC_564_669',
                        value = '-(cw*ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_569_670 = Coupling(name = 'UVGC_569_670',
                        value = '-(FRCTdeltaxMTxtG*complex(0,1)) - (FRCTdeltaZxttLxtG*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxtG*complex(0,1)*MT)/2. + (complex(0,1)*G**2*invFREps*MT)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_570_671 = Coupling(name = 'UVGC_570_671',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_572_672 = Coupling(name = 'UVGC_572_672',
                        value = '-(FRCTdeltaZxttLxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) + (complex(0,1)*G**2*invFREps*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMTxtG*complex(0,1)*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_573_673 = Coupling(name = 'UVGC_573_673',
                        value = '-(FRCTdeltaZxttLxtG*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxtG*yt)/(2.*cmath.sqrt(2)) + (G**2*invFREps*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMTxtG*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_577_674 = Coupling(name = 'UVGC_577_674',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_579_675 = Coupling(name = 'UVGC_579_675',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y1LL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_579_676 = Coupling(name = 'UVGC_579_676',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_579_677 = Coupling(name = 'UVGC_579_677',
                        value = '(complex(0,1)*G**2*invFREps*y1LL3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_580_678 = Coupling(name = 'UVGC_580_678',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y1LL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_580_679 = Coupling(name = 'UVGC_580_679',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_580_680 = Coupling(name = 'UVGC_580_680',
                        value = '(complex(0,1)*G**2*invFREps*y1LL3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_581_681 = Coupling(name = 'UVGC_581_681',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y1LL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_581_682 = Coupling(name = 'UVGC_581_682',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_581_683 = Coupling(name = 'UVGC_581_683',
                        value = '(complex(0,1)*G**2*invFREps*y1LL3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_582_684 = Coupling(name = 'UVGC_582_684',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y1LL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_582_685 = Coupling(name = 'UVGC_582_685',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_582_686 = Coupling(name = 'UVGC_582_686',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_583_687 = Coupling(name = 'UVGC_583_687',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y1LL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_583_688 = Coupling(name = 'UVGC_583_688',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_583_689 = Coupling(name = 'UVGC_583_689',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_584_690 = Coupling(name = 'UVGC_584_690',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y1LL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_584_691 = Coupling(name = 'UVGC_584_691',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_584_692 = Coupling(name = 'UVGC_584_692',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_585_693 = Coupling(name = 'UVGC_585_693',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty1RR3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_585_694 = Coupling(name = 'UVGC_585_694',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_585_695 = Coupling(name = 'UVGC_585_695',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_586_696 = Coupling(name = 'UVGC_586_696',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty1RR3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_586_697 = Coupling(name = 'UVGC_586_697',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_586_698 = Coupling(name = 'UVGC_586_698',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_587_699 = Coupling(name = 'UVGC_587_699',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty1RR3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_587_700 = Coupling(name = 'UVGC_587_700',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_587_701 = Coupling(name = 'UVGC_587_701',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_588_702 = Coupling(name = 'UVGC_588_702',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty1RR3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_588_703 = Coupling(name = 'UVGC_588_703',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_588_704 = Coupling(name = 'UVGC_588_704',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_589_705 = Coupling(name = 'UVGC_589_705',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty1RR3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_589_706 = Coupling(name = 'UVGC_589_706',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_589_707 = Coupling(name = 'UVGC_589_707',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_590_708 = Coupling(name = 'UVGC_590_708',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty1RR3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_590_709 = Coupling(name = 'UVGC_590_709',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_590_710 = Coupling(name = 'UVGC_590_710',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_591_711 = Coupling(name = 'UVGC_591_711',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_591_712 = Coupling(name = 'UVGC_591_712',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_591_713 = Coupling(name = 'UVGC_591_713',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_592_714 = Coupling(name = 'UVGC_592_714',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_592_715 = Coupling(name = 'UVGC_592_715',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_592_716 = Coupling(name = 'UVGC_592_716',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_593_717 = Coupling(name = 'UVGC_593_717',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_593_718 = Coupling(name = 'UVGC_593_718',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_593_719 = Coupling(name = 'UVGC_593_719',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_594_720 = Coupling(name = 'UVGC_594_720',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_594_721 = Coupling(name = 'UVGC_594_721',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_594_722 = Coupling(name = 'UVGC_594_722',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_595_723 = Coupling(name = 'UVGC_595_723',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_595_724 = Coupling(name = 'UVGC_595_724',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_595_725 = Coupling(name = 'UVGC_595_725',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_596_726 = Coupling(name = 'UVGC_596_726',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_596_727 = Coupling(name = 'UVGC_596_727',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_596_728 = Coupling(name = 'UVGC_596_728',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_597_729 = Coupling(name = 'UVGC_597_729',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_597_730 = Coupling(name = 'UVGC_597_730',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_597_731 = Coupling(name = 'UVGC_597_731',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_598_732 = Coupling(name = 'UVGC_598_732',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_598_733 = Coupling(name = 'UVGC_598_733',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_598_734 = Coupling(name = 'UVGC_598_734',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_599_735 = Coupling(name = 'UVGC_599_735',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*ty2RL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_599_736 = Coupling(name = 'UVGC_599_736',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_599_737 = Coupling(name = 'UVGC_599_737',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_600_738 = Coupling(name = 'UVGC_600_738',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_600_739 = Coupling(name = 'UVGC_600_739',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_600_740 = Coupling(name = 'UVGC_600_740',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_601_741 = Coupling(name = 'UVGC_601_741',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_601_742 = Coupling(name = 'UVGC_601_742',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_601_743 = Coupling(name = 'UVGC_601_743',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_602_744 = Coupling(name = 'UVGC_602_744',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1)*complexconjugate(ty2RL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_602_745 = Coupling(name = 'UVGC_602_745',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_602_746 = Coupling(name = 'UVGC_602_746',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_603_747 = Coupling(name = 'UVGC_603_747',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*y2LR3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_603_748 = Coupling(name = 'UVGC_603_748',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_603_749 = Coupling(name = 'UVGC_603_749',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_604_750 = Coupling(name = 'UVGC_604_750',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*y2LR3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_604_751 = Coupling(name = 'UVGC_604_751',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_604_752 = Coupling(name = 'UVGC_604_752',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_605_753 = Coupling(name = 'UVGC_605_753',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*y2LR3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_605_754 = Coupling(name = 'UVGC_605_754',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_605_755 = Coupling(name = 'UVGC_605_755',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_606_756 = Coupling(name = 'UVGC_606_756',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y2LR3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_606_757 = Coupling(name = 'UVGC_606_757',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_606_758 = Coupling(name = 'UVGC_606_758',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_607_759 = Coupling(name = 'UVGC_607_759',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y2LR3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_607_760 = Coupling(name = 'UVGC_607_760',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_607_761 = Coupling(name = 'UVGC_607_761',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_608_762 = Coupling(name = 'UVGC_608_762',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y2LR3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_608_763 = Coupling(name = 'UVGC_608_763',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_608_764 = Coupling(name = 'UVGC_608_764',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_609_765 = Coupling(name = 'UVGC_609_765',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_609_766 = Coupling(name = 'UVGC_609_766',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL3x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_609_767 = Coupling(name = 'UVGC_609_767',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_610_768 = Coupling(name = 'UVGC_610_768',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_610_769 = Coupling(name = 'UVGC_610_769',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL3x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_610_770 = Coupling(name = 'UVGC_610_770',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_611_771 = Coupling(name = 'UVGC_611_771',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_611_772 = Coupling(name = 'UVGC_611_772',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL3x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_611_773 = Coupling(name = 'UVGC_611_773',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_612_774 = Coupling(name = 'UVGC_612_774',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_612_775 = Coupling(name = 'UVGC_612_775',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL3x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_612_776 = Coupling(name = 'UVGC_612_776',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_613_777 = Coupling(name = 'UVGC_613_777',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_613_778 = Coupling(name = 'UVGC_613_778',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL3x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_613_779 = Coupling(name = 'UVGC_613_779',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_614_780 = Coupling(name = 'UVGC_614_780',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_614_781 = Coupling(name = 'UVGC_614_781',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL3x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_614_782 = Coupling(name = 'UVGC_614_782',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_615_783 = Coupling(name = 'UVGC_615_783',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x1)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_615_784 = Coupling(name = 'UVGC_615_784',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL3x1)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_615_785 = Coupling(name = 'UVGC_615_785',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_616_786 = Coupling(name = 'UVGC_616_786',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x2)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_616_787 = Coupling(name = 'UVGC_616_787',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL3x2)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_616_788 = Coupling(name = 'UVGC_616_788',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_617_789 = Coupling(name = 'UVGC_617_789',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*y3LL3x3)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_617_790 = Coupling(name = 'UVGC_617_790',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL3x3)/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_617_791 = Coupling(name = 'UVGC_617_791',
                        value = '(complex(0,1)*G**2*invFREps*y3LL3x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_618_792 = Coupling(name = 'UVGC_618_792',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x1))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_618_793 = Coupling(name = 'UVGC_618_793',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL3x1))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_618_794 = Coupling(name = 'UVGC_618_794',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_619_795 = Coupling(name = 'UVGC_619_795',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x2))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_619_796 = Coupling(name = 'UVGC_619_796',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL3x2))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_619_797 = Coupling(name = 'UVGC_619_797',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_620_798 = Coupling(name = 'UVGC_620_798',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*complexconjugate(y3LL3x3))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_620_799 = Coupling(name = 'UVGC_620_799',
                        value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL3x3))/cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_620_800 = Coupling(name = 'UVGC_620_800',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_621_801 = Coupling(name = 'UVGC_621_801',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y1LL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_621_802 = Coupling(name = 'UVGC_621_802',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_621_803 = Coupling(name = 'UVGC_621_803',
                        value = '-(complex(0,1)*G**2*invFREps*y1LL2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_622_804 = Coupling(name = 'UVGC_622_804',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y1LL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_622_805 = Coupling(name = 'UVGC_622_805',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_622_806 = Coupling(name = 'UVGC_622_806',
                        value = '-(complex(0,1)*G**2*invFREps*y1LL2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_623_807 = Coupling(name = 'UVGC_623_807',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y1LL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_623_808 = Coupling(name = 'UVGC_623_808',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_623_809 = Coupling(name = 'UVGC_623_809',
                        value = '-(complex(0,1)*G**2*invFREps*y1LL2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_624_810 = Coupling(name = 'UVGC_624_810',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y1RR2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_624_811 = Coupling(name = 'UVGC_624_811',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_624_812 = Coupling(name = 'UVGC_624_812',
                        value = '-(complex(0,1)*G**2*invFREps*y1RR2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_625_813 = Coupling(name = 'UVGC_625_813',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y1RR2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_625_814 = Coupling(name = 'UVGC_625_814',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_625_815 = Coupling(name = 'UVGC_625_815',
                        value = '-(complex(0,1)*G**2*invFREps*y1RR2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_626_816 = Coupling(name = 'UVGC_626_816',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y1RR2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_626_817 = Coupling(name = 'UVGC_626_817',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_626_818 = Coupling(name = 'UVGC_626_818',
                        value = '-(complex(0,1)*G**2*invFREps*y1RR2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_627_819 = Coupling(name = 'UVGC_627_819',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y1LL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_627_820 = Coupling(name = 'UVGC_627_820',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_627_821 = Coupling(name = 'UVGC_627_821',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_628_822 = Coupling(name = 'UVGC_628_822',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y1LL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_628_823 = Coupling(name = 'UVGC_628_823',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_628_824 = Coupling(name = 'UVGC_628_824',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_629_825 = Coupling(name = 'UVGC_629_825',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y1LL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_629_826 = Coupling(name = 'UVGC_629_826',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_629_827 = Coupling(name = 'UVGC_629_827',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_630_828 = Coupling(name = 'UVGC_630_828',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y1RR2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_630_829 = Coupling(name = 'UVGC_630_829',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_630_830 = Coupling(name = 'UVGC_630_830',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_631_831 = Coupling(name = 'UVGC_631_831',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y1RR2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_631_832 = Coupling(name = 'UVGC_631_832',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_631_833 = Coupling(name = 'UVGC_631_833',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_632_834 = Coupling(name = 'UVGC_632_834',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y1RR2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_632_835 = Coupling(name = 'UVGC_632_835',
                        value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_632_836 = Coupling(name = 'UVGC_632_836',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_633_837 = Coupling(name = 'UVGC_633_837',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_633_838 = Coupling(name = 'UVGC_633_838',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_633_839 = Coupling(name = 'UVGC_633_839',
                        value = '-(complex(0,1)*G**2*invFREps*y2RL2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_634_840 = Coupling(name = 'UVGC_634_840',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_634_841 = Coupling(name = 'UVGC_634_841',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_634_842 = Coupling(name = 'UVGC_634_842',
                        value = '-(complex(0,1)*G**2*invFREps*y2RL2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_635_843 = Coupling(name = 'UVGC_635_843',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_635_844 = Coupling(name = 'UVGC_635_844',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_635_845 = Coupling(name = 'UVGC_635_845',
                        value = '-(complex(0,1)*G**2*invFREps*y2RL2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_636_846 = Coupling(name = 'UVGC_636_846',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_636_847 = Coupling(name = 'UVGC_636_847',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_636_848 = Coupling(name = 'UVGC_636_848',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_637_849 = Coupling(name = 'UVGC_637_849',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_637_850 = Coupling(name = 'UVGC_637_850',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_637_851 = Coupling(name = 'UVGC_637_851',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_638_852 = Coupling(name = 'UVGC_638_852',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_638_853 = Coupling(name = 'UVGC_638_853',
                        value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_638_854 = Coupling(name = 'UVGC_638_854',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_639_855 = Coupling(name = 'UVGC_639_855',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y2LR2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_639_856 = Coupling(name = 'UVGC_639_856',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_639_857 = Coupling(name = 'UVGC_639_857',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_640_858 = Coupling(name = 'UVGC_640_858',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y2LR2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_640_859 = Coupling(name = 'UVGC_640_859',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_640_860 = Coupling(name = 'UVGC_640_860',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_641_861 = Coupling(name = 'UVGC_641_861',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y2LR2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_641_862 = Coupling(name = 'UVGC_641_862',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_641_863 = Coupling(name = 'UVGC_641_863',
                        value = '-(complex(0,1)*G**2*invFREps*y2LR2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_642_864 = Coupling(name = 'UVGC_642_864',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_642_865 = Coupling(name = 'UVGC_642_865',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_642_866 = Coupling(name = 'UVGC_642_866',
                        value = '(complex(0,1)*G**2*invFREps*y2RL2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_643_867 = Coupling(name = 'UVGC_643_867',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_643_868 = Coupling(name = 'UVGC_643_868',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_643_869 = Coupling(name = 'UVGC_643_869',
                        value = '(complex(0,1)*G**2*invFREps*y2RL2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_644_870 = Coupling(name = 'UVGC_644_870',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*y2RL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_644_871 = Coupling(name = 'UVGC_644_871',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_644_872 = Coupling(name = 'UVGC_644_872',
                        value = '(complex(0,1)*G**2*invFREps*y2RL2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_645_873 = Coupling(name = 'UVGC_645_873',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y2LR2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_645_874 = Coupling(name = 'UVGC_645_874',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_645_875 = Coupling(name = 'UVGC_645_875',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_646_876 = Coupling(name = 'UVGC_646_876',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y2LR2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_646_877 = Coupling(name = 'UVGC_646_877',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_646_878 = Coupling(name = 'UVGC_646_878',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_647_879 = Coupling(name = 'UVGC_647_879',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y2LR2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_647_880 = Coupling(name = 'UVGC_647_880',
                        value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_647_881 = Coupling(name = 'UVGC_647_881',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_648_882 = Coupling(name = 'UVGC_648_882',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_648_883 = Coupling(name = 'UVGC_648_883',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_648_884 = Coupling(name = 'UVGC_648_884',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_649_885 = Coupling(name = 'UVGC_649_885',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_649_886 = Coupling(name = 'UVGC_649_886',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_649_887 = Coupling(name = 'UVGC_649_887',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_650_888 = Coupling(name = 'UVGC_650_888',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1)*complexconjugate(y2RL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_650_889 = Coupling(name = 'UVGC_650_889',
                        value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_650_890 = Coupling(name = 'UVGC_650_890',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_651_891 = Coupling(name = 'UVGC_651_891',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_651_892 = Coupling(name = 'UVGC_651_892',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL2x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_651_893 = Coupling(name = 'UVGC_651_893',
                        value = '(complex(0,1)*G**2*invFREps*y3LL2x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_652_894 = Coupling(name = 'UVGC_652_894',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_652_895 = Coupling(name = 'UVGC_652_895',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL2x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_652_896 = Coupling(name = 'UVGC_652_896',
                        value = '(complex(0,1)*G**2*invFREps*y3LL2x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_653_897 = Coupling(name = 'UVGC_653_897',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_653_898 = Coupling(name = 'UVGC_653_898',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL2x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_653_899 = Coupling(name = 'UVGC_653_899',
                        value = '(complex(0,1)*G**2*invFREps*y3LL2x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_654_900 = Coupling(name = 'UVGC_654_900',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_654_901 = Coupling(name = 'UVGC_654_901',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL2x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_654_902 = Coupling(name = 'UVGC_654_902',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_655_903 = Coupling(name = 'UVGC_655_903',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_655_904 = Coupling(name = 'UVGC_655_904',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL2x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_655_905 = Coupling(name = 'UVGC_655_905',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_656_906 = Coupling(name = 'UVGC_656_906',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_656_907 = Coupling(name = 'UVGC_656_907',
                        value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL2x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_656_908 = Coupling(name = 'UVGC_656_908',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_657_909 = Coupling(name = 'UVGC_657_909',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x1)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_657_910 = Coupling(name = 'UVGC_657_910',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL2x1)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_657_911 = Coupling(name = 'UVGC_657_911',
                        value = '-(complex(0,1)*G**2*invFREps*y3LL2x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_658_912 = Coupling(name = 'UVGC_658_912',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x2)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_658_913 = Coupling(name = 'UVGC_658_913',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL2x2)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_658_914 = Coupling(name = 'UVGC_658_914',
                        value = '-(complex(0,1)*G**2*invFREps*y3LL2x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_659_915 = Coupling(name = 'UVGC_659_915',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*y3LL2x3)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_659_916 = Coupling(name = 'UVGC_659_916',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL2x3)/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_659_917 = Coupling(name = 'UVGC_659_917',
                        value = '-(complex(0,1)*G**2*invFREps*y3LL2x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_660_918 = Coupling(name = 'UVGC_660_918',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x1))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_660_919 = Coupling(name = 'UVGC_660_919',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL2x1))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_660_920 = Coupling(name = 'UVGC_660_920',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_661_921 = Coupling(name = 'UVGC_661_921',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x2))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_661_922 = Coupling(name = 'UVGC_661_922',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL2x2))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_661_923 = Coupling(name = 'UVGC_661_923',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_662_924 = Coupling(name = 'UVGC_662_924',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*complexconjugate(y3LL2x3))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_662_925 = Coupling(name = 'UVGC_662_925',
                        value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL2x3))/cmath.sqrt(2)',
                        order = {'NEW':1,'QCD':2})

UVGC_662_926 = Coupling(name = 'UVGC_662_926',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'NEW':1,'QCD':2})

UVGC_663_927 = Coupling(name = 'UVGC_663_927',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_663_928 = Coupling(name = 'UVGC_663_928',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_663_929 = Coupling(name = 'UVGC_663_929',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_664_930 = Coupling(name = 'UVGC_664_930',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y1LL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_664_931 = Coupling(name = 'UVGC_664_931',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_664_932 = Coupling(name = 'UVGC_664_932',
                        value = '(complex(0,1)*G**2*invFREps*y1LL1x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_665_933 = Coupling(name = 'UVGC_665_933',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y1LL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_665_934 = Coupling(name = 'UVGC_665_934',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_665_935 = Coupling(name = 'UVGC_665_935',
                        value = '(complex(0,1)*G**2*invFREps*y1LL1x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_666_936 = Coupling(name = 'UVGC_666_936',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y1LL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_666_937 = Coupling(name = 'UVGC_666_937',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_666_938 = Coupling(name = 'UVGC_666_938',
                        value = '(complex(0,1)*G**2*invFREps*y1LL1x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_667_939 = Coupling(name = 'UVGC_667_939',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y1LL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_667_940 = Coupling(name = 'UVGC_667_940',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_667_941 = Coupling(name = 'UVGC_667_941',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_668_942 = Coupling(name = 'UVGC_668_942',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y1LL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_668_943 = Coupling(name = 'UVGC_668_943',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_668_944 = Coupling(name = 'UVGC_668_944',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_669_945 = Coupling(name = 'UVGC_669_945',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y1LL1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_669_946 = Coupling(name = 'UVGC_669_946',
                        value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_669_947 = Coupling(name = 'UVGC_669_947',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_670_948 = Coupling(name = 'UVGC_670_948',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty1RR1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_670_949 = Coupling(name = 'UVGC_670_949',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_670_950 = Coupling(name = 'UVGC_670_950',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR1x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_671_951 = Coupling(name = 'UVGC_671_951',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty1RR1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_671_952 = Coupling(name = 'UVGC_671_952',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_671_953 = Coupling(name = 'UVGC_671_953',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR1x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_672_954 = Coupling(name = 'UVGC_672_954',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty1RR1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_672_955 = Coupling(name = 'UVGC_672_955',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_672_956 = Coupling(name = 'UVGC_672_956',
                        value = '-(complex(0,1)*G**2*invFREps*ty1RR1x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_673_957 = Coupling(name = 'UVGC_673_957',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty1RR1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_673_958 = Coupling(name = 'UVGC_673_958',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_673_959 = Coupling(name = 'UVGC_673_959',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR1x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_674_960 = Coupling(name = 'UVGC_674_960',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty1RR1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_674_961 = Coupling(name = 'UVGC_674_961',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_674_962 = Coupling(name = 'UVGC_674_962',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR1x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_675_963 = Coupling(name = 'UVGC_675_963',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty1RR1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_675_964 = Coupling(name = 'UVGC_675_964',
                        value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_675_965 = Coupling(name = 'UVGC_675_965',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR1x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_676_966 = Coupling(name = 'UVGC_676_966',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_676_967 = Coupling(name = 'UVGC_676_967',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_676_968 = Coupling(name = 'UVGC_676_968',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL1x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_677_969 = Coupling(name = 'UVGC_677_969',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_677_970 = Coupling(name = 'UVGC_677_970',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_677_971 = Coupling(name = 'UVGC_677_971',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL1x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_678_972 = Coupling(name = 'UVGC_678_972',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_678_973 = Coupling(name = 'UVGC_678_973',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_678_974 = Coupling(name = 'UVGC_678_974',
                        value = '-(complex(0,1)*G**2*invFREps*ty2RL1x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_679_975 = Coupling(name = 'UVGC_679_975',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_679_976 = Coupling(name = 'UVGC_679_976',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_679_977 = Coupling(name = 'UVGC_679_977',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_680_978 = Coupling(name = 'UVGC_680_978',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_680_979 = Coupling(name = 'UVGC_680_979',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_680_980 = Coupling(name = 'UVGC_680_980',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_681_981 = Coupling(name = 'UVGC_681_981',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_681_982 = Coupling(name = 'UVGC_681_982',
                        value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_681_983 = Coupling(name = 'UVGC_681_983',
                        value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x3))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_682_984 = Coupling(name = 'UVGC_682_984',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_682_985 = Coupling(name = 'UVGC_682_985',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL1x1)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_682_986 = Coupling(name = 'UVGC_682_986',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL1x1)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_683_987 = Coupling(name = 'UVGC_683_987',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_683_988 = Coupling(name = 'UVGC_683_988',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL1x2)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_683_989 = Coupling(name = 'UVGC_683_989',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL1x2)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_684_990 = Coupling(name = 'UVGC_684_990',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*ty2RL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_684_991 = Coupling(name = 'UVGC_684_991',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL1x3)/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_684_992 = Coupling(name = 'UVGC_684_992',
                        value = '(complex(0,1)*G**2*invFREps*ty2RL1x3)/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_685_993 = Coupling(name = 'UVGC_685_993',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_685_994 = Coupling(name = 'UVGC_685_994',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL1x1))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_685_995 = Coupling(name = 'UVGC_685_995',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x1))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_686_996 = Coupling(name = 'UVGC_686_996',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_686_997 = Coupling(name = 'UVGC_686_997',
                        value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL1x2))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_686_998 = Coupling(name = 'UVGC_686_998',
                        value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x2))/(12.*cmath.pi**2)',
                        order = {'NEW':1,'QCD':2})

UVGC_687_999 = Coupling(name = 'UVGC_687_999',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1)*complexconjugate(ty2RL1x3))/2.',
                        order = {'NEW':1,'QCD':2})

UVGC_687_1000 = Coupling(name = 'UVGC_687_1000',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_687_1001 = Coupling(name = 'UVGC_687_1001',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_688_1002 = Coupling(name = 'UVGC_688_1002',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*y2LR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_688_1003 = Coupling(name = 'UVGC_688_1003',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_688_1004 = Coupling(name = 'UVGC_688_1004',
                         value = '-(complex(0,1)*G**2*invFREps*y2LR1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_689_1005 = Coupling(name = 'UVGC_689_1005',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*y2LR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_689_1006 = Coupling(name = 'UVGC_689_1006',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_689_1007 = Coupling(name = 'UVGC_689_1007',
                         value = '-(complex(0,1)*G**2*invFREps*y2LR1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_690_1008 = Coupling(name = 'UVGC_690_1008',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*y2LR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_690_1009 = Coupling(name = 'UVGC_690_1009',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_690_1010 = Coupling(name = 'UVGC_690_1010',
                         value = '-(complex(0,1)*G**2*invFREps*y2LR1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_691_1011 = Coupling(name = 'UVGC_691_1011',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y2LR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_691_1012 = Coupling(name = 'UVGC_691_1012',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_691_1013 = Coupling(name = 'UVGC_691_1013',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_692_1014 = Coupling(name = 'UVGC_692_1014',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y2LR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_692_1015 = Coupling(name = 'UVGC_692_1015',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_692_1016 = Coupling(name = 'UVGC_692_1016',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_693_1017 = Coupling(name = 'UVGC_693_1017',
                         value = '(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y2LR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_693_1018 = Coupling(name = 'UVGC_693_1018',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_693_1019 = Coupling(name = 'UVGC_693_1019',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2LR1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_694_1020 = Coupling(name = 'UVGC_694_1020',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_694_1021 = Coupling(name = 'UVGC_694_1021',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_694_1022 = Coupling(name = 'UVGC_694_1022',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_695_1023 = Coupling(name = 'UVGC_695_1023',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_695_1024 = Coupling(name = 'UVGC_695_1024',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_695_1025 = Coupling(name = 'UVGC_695_1025',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_696_1026 = Coupling(name = 'UVGC_696_1026',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_696_1027 = Coupling(name = 'UVGC_696_1027',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*y3LL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_696_1028 = Coupling(name = 'UVGC_696_1028',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_697_1029 = Coupling(name = 'UVGC_697_1029',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_697_1030 = Coupling(name = 'UVGC_697_1030',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_697_1031 = Coupling(name = 'UVGC_697_1031',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_698_1032 = Coupling(name = 'UVGC_698_1032',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_698_1033 = Coupling(name = 'UVGC_698_1033',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_698_1034 = Coupling(name = 'UVGC_698_1034',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_699_1035 = Coupling(name = 'UVGC_699_1035',
                         value = '-(FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_699_1036 = Coupling(name = 'UVGC_699_1036',
                         value = '-(FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*complexconjugate(y3LL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_699_1037 = Coupling(name = 'UVGC_699_1037',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_700_1038 = Coupling(name = 'UVGC_700_1038',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x1)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_700_1039 = Coupling(name = 'UVGC_700_1039',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL1x1)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_700_1040 = Coupling(name = 'UVGC_700_1040',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_701_1041 = Coupling(name = 'UVGC_701_1041',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x2)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_701_1042 = Coupling(name = 'UVGC_701_1042',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL1x2)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_701_1043 = Coupling(name = 'UVGC_701_1043',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_702_1044 = Coupling(name = 'UVGC_702_1044',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*y3LL1x3)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_702_1045 = Coupling(name = 'UVGC_702_1045',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL1x3)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_702_1046 = Coupling(name = 'UVGC_702_1046',
                         value = '(complex(0,1)*G**2*invFREps*y3LL1x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_703_1047 = Coupling(name = 'UVGC_703_1047',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x1))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_703_1048 = Coupling(name = 'UVGC_703_1048',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL1x1))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_703_1049 = Coupling(name = 'UVGC_703_1049',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_704_1050 = Coupling(name = 'UVGC_704_1050',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x2))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_704_1051 = Coupling(name = 'UVGC_704_1051',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL1x2))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_704_1052 = Coupling(name = 'UVGC_704_1052',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_705_1053 = Coupling(name = 'UVGC_705_1053',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*complexconjugate(y3LL1x3))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_705_1054 = Coupling(name = 'UVGC_705_1054',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL1x3))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_705_1055 = Coupling(name = 'UVGC_705_1055',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_706_1056 = Coupling(name = 'UVGC_706_1056',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_706_1057 = Coupling(name = 'UVGC_706_1057',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_706_1058 = Coupling(name = 'UVGC_706_1058',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_707_1059 = Coupling(name = 'UVGC_707_1059',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_707_1060 = Coupling(name = 'UVGC_707_1060',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_707_1061 = Coupling(name = 'UVGC_707_1061',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_742_1062 = Coupling(name = 'UVGC_742_1062',
                         value = '-(ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(6.*cw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cw*cmath.pi**2*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_742_1063 = Coupling(name = 'UVGC_742_1063',
                         value = '-(ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(6.*cw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cw*cmath.pi**2*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_742_1064 = Coupling(name = 'UVGC_742_1064',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cw*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_745_1065 = Coupling(name = 'UVGC_745_1065',
                         value = '(ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_745_1066 = Coupling(name = 'UVGC_745_1066',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_746_1067 = Coupling(name = 'UVGC_746_1067',
                         value = '(ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_747_1068 = Coupling(name = 'UVGC_747_1068',
                         value = '(ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_747_1069 = Coupling(name = 'UVGC_747_1069',
                         value = '(ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_748_1070 = Coupling(name = 'UVGC_748_1070',
                         value = '-(ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_748_1071 = Coupling(name = 'UVGC_748_1071',
                         value = '-(ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_748_1072 = Coupling(name = 'UVGC_748_1072',
                         value = '(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_749_1073 = Coupling(name = 'UVGC_749_1073',
                         value = '(ee**2*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1))/(6.*sw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_749_1074 = Coupling(name = 'UVGC_749_1074',
                         value = '(ee**2*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_749_1075 = Coupling(name = 'UVGC_749_1075',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_750_1076 = Coupling(name = 'UVGC_750_1076',
                         value = '(ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1077 = Coupling(name = 'UVGC_750_1077',
                         value = '(ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1078 = Coupling(name = 'UVGC_750_1078',
                         value = '(ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1079 = Coupling(name = 'UVGC_750_1079',
                         value = '(ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1080 = Coupling(name = 'UVGC_750_1080',
                         value = '(ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1081 = Coupling(name = 'UVGC_750_1081',
                         value = '(ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1082 = Coupling(name = 'UVGC_750_1082',
                         value = '(ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1083 = Coupling(name = 'UVGC_750_1083',
                         value = '(ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1084 = Coupling(name = 'UVGC_750_1084',
                         value = '(ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1085 = Coupling(name = 'UVGC_750_1085',
                         value = '(ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1086 = Coupling(name = 'UVGC_750_1086',
                         value = '(ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1087 = Coupling(name = 'UVGC_750_1087',
                         value = '(ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1088 = Coupling(name = 'UVGC_750_1088',
                         value = '(ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1089 = Coupling(name = 'UVGC_750_1089',
                         value = '(ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1090 = Coupling(name = 'UVGC_750_1090',
                         value = '(ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1091 = Coupling(name = 'UVGC_750_1091',
                         value = '(ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1092 = Coupling(name = 'UVGC_750_1092',
                         value = '(ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1093 = Coupling(name = 'UVGC_750_1093',
                         value = '(ee*FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1094 = Coupling(name = 'UVGC_750_1094',
                         value = '(ee*FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_750_1095 = Coupling(name = 'UVGC_750_1095',
                         value = '(-7*ee*complex(0,1)*G**3*invFREps)/(96.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_771_1096 = Coupling(name = 'UVGC_771_1096',
                         value = '(-7*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(6.*cw*cmath.sqrt(2)) - (7*ee**2*complex(0,1)*G**2*invFREps)/(24.*cw*cmath.pi**2*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_771_1097 = Coupling(name = 'UVGC_771_1097',
                         value = '(-7*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(6.*cw*cmath.sqrt(2)) - (7*ee**2*complex(0,1)*G**2*invFREps)/(24.*cw*cmath.pi**2*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_771_1098 = Coupling(name = 'UVGC_771_1098',
                         value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(36.*cw*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_774_1099 = Coupling(name = 'UVGC_774_1099',
                         value = '(ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_775_1100 = Coupling(name = 'UVGC_775_1100',
                         value = '(ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_776_1101 = Coupling(name = 'UVGC_776_1101',
                         value = '(ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_776_1102 = Coupling(name = 'UVGC_776_1102',
                         value = '(ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_777_1103 = Coupling(name = 'UVGC_777_1103',
                         value = '-(ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_777_1104 = Coupling(name = 'UVGC_777_1104',
                         value = '-(ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_778_1105 = Coupling(name = 'UVGC_778_1105',
                         value = '(7*ee**2*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_778_1106 = Coupling(name = 'UVGC_778_1106',
                         value = '(7*ee**2*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (5*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_778_1107 = Coupling(name = 'UVGC_778_1107',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_779_1108 = Coupling(name = 'UVGC_779_1108',
                         value = '(ee*FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_779_1109 = Coupling(name = 'UVGC_779_1109',
                         value = '(ee*FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_788_1110 = Coupling(name = 'UVGC_788_1110',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_788_1111 = Coupling(name = 'UVGC_788_1111',
                         value = '(ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/sw**2 - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_788_1112 = Coupling(name = 'UVGC_788_1112',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_789_1113 = Coupling(name = 'UVGC_789_1113',
                         value = '(ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(3.*cw) + (ee**2*complex(0,1)*G**2*invFREps)/(12.*cw*cmath.pi**2) - (cw*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_789_1114 = Coupling(name = 'UVGC_789_1114',
                         value = '(ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(3.*cw) + (ee**2*complex(0,1)*G**2*invFREps)/(12.*cw*cmath.pi**2) - (cw*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(2.*sw**2) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_789_1115 = Coupling(name = 'UVGC_789_1115',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cw*cmath.pi**2) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_790_1116 = Coupling(name = 'UVGC_790_1116',
                         value = '(ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_790_1117 = Coupling(name = 'UVGC_790_1117',
                         value = '(ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_790_1118 = Coupling(name = 'UVGC_790_1118',
                         value = '-(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_791_1119 = Coupling(name = 'UVGC_791_1119',
                         value = '-(ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_791_1120 = Coupling(name = 'UVGC_791_1120',
                         value = '-(ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_791_1121 = Coupling(name = 'UVGC_791_1121',
                         value = '(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_792_1122 = Coupling(name = 'UVGC_792_1122',
                         value = '(-5*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(6.*sw) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_792_1123 = Coupling(name = 'UVGC_792_1123',
                         value = '(-5*ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/(6.*sw) - (ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_792_1124 = Coupling(name = 'UVGC_792_1124',
                         value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_793_1125 = Coupling(name = 'UVGC_793_1125',
                         value = '(ee*FRCTdeltaZxGGxb*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1126 = Coupling(name = 'UVGC_793_1126',
                         value = '(ee*FRCTdeltaZxGGxc*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1127 = Coupling(name = 'UVGC_793_1127',
                         value = '(ee*FRCTdeltaZxGGxd*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1128 = Coupling(name = 'UVGC_793_1128',
                         value = '(ee*FRCTdeltaZxGGxG*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1129 = Coupling(name = 'UVGC_793_1129',
                         value = '(ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1130 = Coupling(name = 'UVGC_793_1130',
                         value = '(ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1131 = Coupling(name = 'UVGC_793_1131',
                         value = '(ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1132 = Coupling(name = 'UVGC_793_1132',
                         value = '(ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1133 = Coupling(name = 'UVGC_793_1133',
                         value = '(ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1134 = Coupling(name = 'UVGC_793_1134',
                         value = '(ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1135 = Coupling(name = 'UVGC_793_1135',
                         value = '(ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1136 = Coupling(name = 'UVGC_793_1136',
                         value = '(ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1137 = Coupling(name = 'UVGC_793_1137',
                         value = '(ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1138 = Coupling(name = 'UVGC_793_1138',
                         value = '(ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1139 = Coupling(name = 'UVGC_793_1139',
                         value = '(ee*FRCTdeltaZxGGxs*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1140 = Coupling(name = 'UVGC_793_1140',
                         value = '(ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1141 = Coupling(name = 'UVGC_793_1141',
                         value = '(ee*FRCTdeltaZxGGxu*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_793_1142 = Coupling(name = 'UVGC_793_1142',
                         value = '(ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G)/sw + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_793_1143 = Coupling(name = 'UVGC_793_1143',
                         value = '(ee*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*G)/sw + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_793_1144 = Coupling(name = 'UVGC_793_1144',
                         value = '(-7*ee*complex(0,1)*G**3*invFREps)/(96.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_796_1145 = Coupling(name = 'UVGC_796_1145',
                         value = '(ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/sw**2 - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_797_1146 = Coupling(name = 'UVGC_797_1146',
                         value = '(2*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/sw**2 - (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_798_1147 = Coupling(name = 'UVGC_798_1147',
                         value = '-(ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(3.*cw) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cw*cmath.pi**2) - (cw*ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_798_1148 = Coupling(name = 'UVGC_798_1148',
                         value = '-(ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(3.*cw) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cw*cmath.pi**2) - (cw*ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(2.*sw**2) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_798_1149 = Coupling(name = 'UVGC_798_1149',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cw*cmath.pi**2) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_799_1150 = Coupling(name = 'UVGC_799_1150',
                         value = '(ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_800_1151 = Coupling(name = 'UVGC_800_1151',
                         value = '-(ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_801_1152 = Coupling(name = 'UVGC_801_1152',
                         value = '-(ee**2*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1))/(6.*sw) + (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_801_1153 = Coupling(name = 'UVGC_801_1153',
                         value = '-(ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/(6.*sw) - (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_801_1154 = Coupling(name = 'UVGC_801_1154',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1155 = Coupling(name = 'UVGC_802_1155',
                         value = '-((ee*FRCTdeltaZxGGxb*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1156 = Coupling(name = 'UVGC_802_1156',
                         value = '-((ee*FRCTdeltaZxGGxc*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1157 = Coupling(name = 'UVGC_802_1157',
                         value = '-((ee*FRCTdeltaZxGGxd*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1158 = Coupling(name = 'UVGC_802_1158',
                         value = '-((ee*FRCTdeltaZxGGxG*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1159 = Coupling(name = 'UVGC_802_1159',
                         value = '-((ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1160 = Coupling(name = 'UVGC_802_1160',
                         value = '-((ee*FRCTdeltaxaSxLQ1d*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ1d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1161 = Coupling(name = 'UVGC_802_1161',
                         value = '-((ee*FRCTdeltaxaSxLQ1dd*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ1dd*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1162 = Coupling(name = 'UVGC_802_1162',
                         value = '-((ee*FRCTdeltaxaSxLQ2d*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ2d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1163 = Coupling(name = 'UVGC_802_1163',
                         value = '-((ee*FRCTdeltaxaSxLQ2pu*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ2pu*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1164 = Coupling(name = 'UVGC_802_1164',
                         value = '-((ee*FRCTdeltaxaSxLQ2u*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ2u*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1165 = Coupling(name = 'UVGC_802_1165',
                         value = '-((ee*FRCTdeltaxaSxLQ2uu*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ2uu*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1166 = Coupling(name = 'UVGC_802_1166',
                         value = '-((ee*FRCTdeltaxaSxLQ3d*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ3d*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1167 = Coupling(name = 'UVGC_802_1167',
                         value = '-((ee*FRCTdeltaxaSxLQ3dd*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ3dd*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1168 = Coupling(name = 'UVGC_802_1168',
                         value = '-((ee*FRCTdeltaxaSxLQ3u*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxLQ3u*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1169 = Coupling(name = 'UVGC_802_1169',
                         value = '-((ee*FRCTdeltaZxGGxs*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1170 = Coupling(name = 'UVGC_802_1170',
                         value = '-((ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw)) - (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/sw',
                         order = {'QCD':3,'QED':1})

UVGC_802_1171 = Coupling(name = 'UVGC_802_1171',
                         value = '-((ee*FRCTdeltaZxGGxu*complex(0,1)*G)/sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1172 = Coupling(name = 'UVGC_802_1172',
                         value = '-((ee*FRCTdeltaZxLQ3dLQ3dxGLQ3d*complex(0,1)*G)/sw) - (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1173 = Coupling(name = 'UVGC_802_1173',
                         value = '-((ee*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*G)/sw) - (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_802_1174 = Coupling(name = 'UVGC_802_1174',
                         value = '(7*ee*complex(0,1)*G**3*invFREps)/(96.*cmath.pi**2*sw)',
                         order = {'QCD':3,'QED':1})

UVGC_805_1175 = Coupling(name = 'UVGC_805_1175',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_805_1176 = Coupling(name = 'UVGC_805_1176',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y1LL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_805_1177 = Coupling(name = 'UVGC_805_1177',
                         value = '(complex(0,1)*G**2*invFREps*y1LL2x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_806_1178 = Coupling(name = 'UVGC_806_1178',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_806_1179 = Coupling(name = 'UVGC_806_1179',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y1LL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_806_1180 = Coupling(name = 'UVGC_806_1180',
                         value = '(complex(0,1)*G**2*invFREps*y1LL2x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_807_1181 = Coupling(name = 'UVGC_807_1181',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_807_1182 = Coupling(name = 'UVGC_807_1182',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y1LL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_807_1183 = Coupling(name = 'UVGC_807_1183',
                         value = '(complex(0,1)*G**2*invFREps*y1LL2x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_808_1184 = Coupling(name = 'UVGC_808_1184',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_808_1185 = Coupling(name = 'UVGC_808_1185',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y1LL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_808_1186 = Coupling(name = 'UVGC_808_1186',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_809_1187 = Coupling(name = 'UVGC_809_1187',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_809_1188 = Coupling(name = 'UVGC_809_1188',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y1LL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_809_1189 = Coupling(name = 'UVGC_809_1189',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_810_1190 = Coupling(name = 'UVGC_810_1190',
                         value = '-(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_810_1191 = Coupling(name = 'UVGC_810_1191',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y1LL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_810_1192 = Coupling(name = 'UVGC_810_1192',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y1LL2x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_811_1193 = Coupling(name = 'UVGC_811_1193',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_811_1194 = Coupling(name = 'UVGC_811_1194',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty1RR2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_811_1195 = Coupling(name = 'UVGC_811_1195',
                         value = '-(complex(0,1)*G**2*invFREps*ty1RR2x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_812_1196 = Coupling(name = 'UVGC_812_1196',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_812_1197 = Coupling(name = 'UVGC_812_1197',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty1RR2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_812_1198 = Coupling(name = 'UVGC_812_1198',
                         value = '-(complex(0,1)*G**2*invFREps*ty1RR2x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_813_1199 = Coupling(name = 'UVGC_813_1199',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*ty1RR2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_813_1200 = Coupling(name = 'UVGC_813_1200',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty1RR2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_813_1201 = Coupling(name = 'UVGC_813_1201',
                         value = '-(complex(0,1)*G**2*invFREps*ty1RR2x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_814_1202 = Coupling(name = 'UVGC_814_1202',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_814_1203 = Coupling(name = 'UVGC_814_1203',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty1RR2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_814_1204 = Coupling(name = 'UVGC_814_1204',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR2x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_815_1205 = Coupling(name = 'UVGC_815_1205',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_815_1206 = Coupling(name = 'UVGC_815_1206',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty1RR2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_815_1207 = Coupling(name = 'UVGC_815_1207',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR2x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_816_1208 = Coupling(name = 'UVGC_816_1208',
                         value = '(FRCTdeltaZxLQ1ddLQ1ddxGLQ1dd*complex(0,1)*complexconjugate(ty1RR2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_816_1209 = Coupling(name = 'UVGC_816_1209',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty1RR2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_816_1210 = Coupling(name = 'UVGC_816_1210',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty1RR2x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_817_1211 = Coupling(name = 'UVGC_817_1211',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_817_1212 = Coupling(name = 'UVGC_817_1212',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_817_1213 = Coupling(name = 'UVGC_817_1213',
                         value = '-(complex(0,1)*G**2*invFREps*ty2RL2x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_818_1214 = Coupling(name = 'UVGC_818_1214',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_818_1215 = Coupling(name = 'UVGC_818_1215',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_818_1216 = Coupling(name = 'UVGC_818_1216',
                         value = '-(complex(0,1)*G**2*invFREps*ty2RL2x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_819_1217 = Coupling(name = 'UVGC_819_1217',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*ty2RL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_819_1218 = Coupling(name = 'UVGC_819_1218',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_819_1219 = Coupling(name = 'UVGC_819_1219',
                         value = '-(complex(0,1)*G**2*invFREps*ty2RL2x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_820_1220 = Coupling(name = 'UVGC_820_1220',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_820_1221 = Coupling(name = 'UVGC_820_1221',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_820_1222 = Coupling(name = 'UVGC_820_1222',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_821_1223 = Coupling(name = 'UVGC_821_1223',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_821_1224 = Coupling(name = 'UVGC_821_1224',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_821_1225 = Coupling(name = 'UVGC_821_1225',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_822_1226 = Coupling(name = 'UVGC_822_1226',
                         value = '(FRCTdeltaZxLQ2dLQ2dxGLQ2d*complex(0,1)*complexconjugate(ty2RL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_822_1227 = Coupling(name = 'UVGC_822_1227',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_822_1228 = Coupling(name = 'UVGC_822_1228',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_823_1229 = Coupling(name = 'UVGC_823_1229',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_823_1230 = Coupling(name = 'UVGC_823_1230',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_823_1231 = Coupling(name = 'UVGC_823_1231',
                         value = '(complex(0,1)*G**2*invFREps*ty2RL2x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_824_1232 = Coupling(name = 'UVGC_824_1232',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_824_1233 = Coupling(name = 'UVGC_824_1233',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_824_1234 = Coupling(name = 'UVGC_824_1234',
                         value = '(complex(0,1)*G**2*invFREps*ty2RL2x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_825_1235 = Coupling(name = 'UVGC_825_1235',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*ty2RL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_825_1236 = Coupling(name = 'UVGC_825_1236',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*ty2RL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_825_1237 = Coupling(name = 'UVGC_825_1237',
                         value = '(complex(0,1)*G**2*invFREps*ty2RL2x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_826_1238 = Coupling(name = 'UVGC_826_1238',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_826_1239 = Coupling(name = 'UVGC_826_1239',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_826_1240 = Coupling(name = 'UVGC_826_1240',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_827_1241 = Coupling(name = 'UVGC_827_1241',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_827_1242 = Coupling(name = 'UVGC_827_1242',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_827_1243 = Coupling(name = 'UVGC_827_1243',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_828_1244 = Coupling(name = 'UVGC_828_1244',
                         value = '-(FRCTdeltaZxLQ2puLQ2puxGLQ2pu*complex(0,1)*complexconjugate(ty2RL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_828_1245 = Coupling(name = 'UVGC_828_1245',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1)*complexconjugate(ty2RL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_828_1246 = Coupling(name = 'UVGC_828_1246',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(ty2RL2x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_829_1247 = Coupling(name = 'UVGC_829_1247',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_829_1248 = Coupling(name = 'UVGC_829_1248',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*y2LR2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_830_1249 = Coupling(name = 'UVGC_830_1249',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_830_1250 = Coupling(name = 'UVGC_830_1250',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*y2LR2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_831_1251 = Coupling(name = 'UVGC_831_1251',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2LR2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_831_1252 = Coupling(name = 'UVGC_831_1252',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*y2LR2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_832_1253 = Coupling(name = 'UVGC_832_1253',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_832_1254 = Coupling(name = 'UVGC_832_1254',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y2LR2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_833_1255 = Coupling(name = 'UVGC_833_1255',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_833_1256 = Coupling(name = 'UVGC_833_1256',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y2LR2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_834_1257 = Coupling(name = 'UVGC_834_1257',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2LR2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_834_1258 = Coupling(name = 'UVGC_834_1258',
                         value = '(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y2LR2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_835_1259 = Coupling(name = 'UVGC_835_1259',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_836_1260 = Coupling(name = 'UVGC_836_1260',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_837_1261 = Coupling(name = 'UVGC_837_1261',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_838_1262 = Coupling(name = 'UVGC_838_1262',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_839_1263 = Coupling(name = 'UVGC_839_1263',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_840_1264 = Coupling(name = 'UVGC_840_1264',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_841_1265 = Coupling(name = 'UVGC_841_1265',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL2x1)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_841_1266 = Coupling(name = 'UVGC_841_1266',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x1)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_841_1267 = Coupling(name = 'UVGC_841_1267',
                         value = '(complex(0,1)*G**2*invFREps*y3LL2x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_842_1268 = Coupling(name = 'UVGC_842_1268',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL2x2)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_842_1269 = Coupling(name = 'UVGC_842_1269',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x2)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_842_1270 = Coupling(name = 'UVGC_842_1270',
                         value = '(complex(0,1)*G**2*invFREps*y3LL2x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_843_1271 = Coupling(name = 'UVGC_843_1271',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*y3LL2x3)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_843_1272 = Coupling(name = 'UVGC_843_1272',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*y3LL2x3)/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_843_1273 = Coupling(name = 'UVGC_843_1273',
                         value = '(complex(0,1)*G**2*invFREps*y3LL2x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_844_1274 = Coupling(name = 'UVGC_844_1274',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL2x1))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_844_1275 = Coupling(name = 'UVGC_844_1275',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x1))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_844_1276 = Coupling(name = 'UVGC_844_1276',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_845_1277 = Coupling(name = 'UVGC_845_1277',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL2x2))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_845_1278 = Coupling(name = 'UVGC_845_1278',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x2))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_845_1279 = Coupling(name = 'UVGC_845_1279',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_846_1280 = Coupling(name = 'UVGC_846_1280',
                         value = '-((FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1)*complexconjugate(y3LL2x3))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_846_1281 = Coupling(name = 'UVGC_846_1281',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*complexconjugate(y3LL2x3))/cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_846_1282 = Coupling(name = 'UVGC_846_1282',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y3LL2x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_847_1283 = Coupling(name = 'UVGC_847_1283',
                         value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_847_1284 = Coupling(name = 'UVGC_847_1284',
                         value = '(ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_848_1285 = Coupling(name = 'UVGC_848_1285',
                         value = '(FRCTdeltaZxbbLxbG*yt)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_848_1286 = Coupling(name = 'UVGC_848_1286',
                         value = '(FRCTdeltaZxttRxtG*yt)/2. + (FRCTdeltaxMTxtG*yt)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_848_1287 = Coupling(name = 'UVGC_848_1287',
                         value = '-(G**2*invFREps*yt)/(3.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_849_1288 = Coupling(name = 'UVGC_849_1288',
                         value = '-(FRCTdeltaZxbbLxbG*yt)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_849_1289 = Coupling(name = 'UVGC_849_1289',
                         value = '-(FRCTdeltaZxttRxtG*yt)/2. - (FRCTdeltaxMTxtG*yt)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_849_1290 = Coupling(name = 'UVGC_849_1290',
                         value = '(G**2*invFREps*yt)/(3.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_850_1291 = Coupling(name = 'UVGC_850_1291',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_850_1292 = Coupling(name = 'UVGC_850_1292',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y1LL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_850_1293 = Coupling(name = 'UVGC_850_1293',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL3x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_851_1294 = Coupling(name = 'UVGC_851_1294',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_851_1295 = Coupling(name = 'UVGC_851_1295',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y1LL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_851_1296 = Coupling(name = 'UVGC_851_1296',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL3x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_852_1297 = Coupling(name = 'UVGC_852_1297',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_852_1298 = Coupling(name = 'UVGC_852_1298',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y1LL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_852_1299 = Coupling(name = 'UVGC_852_1299',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL3x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_853_1300 = Coupling(name = 'UVGC_853_1300',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_853_1301 = Coupling(name = 'UVGC_853_1301',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y1RR3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_853_1302 = Coupling(name = 'UVGC_853_1302',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR3x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_854_1303 = Coupling(name = 'UVGC_854_1303',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_854_1304 = Coupling(name = 'UVGC_854_1304',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y1RR3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_854_1305 = Coupling(name = 'UVGC_854_1305',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR3x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_855_1306 = Coupling(name = 'UVGC_855_1306',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_855_1307 = Coupling(name = 'UVGC_855_1307',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y1RR3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_855_1308 = Coupling(name = 'UVGC_855_1308',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR3x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_856_1309 = Coupling(name = 'UVGC_856_1309',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_856_1310 = Coupling(name = 'UVGC_856_1310',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y1LL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_856_1311 = Coupling(name = 'UVGC_856_1311',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_857_1312 = Coupling(name = 'UVGC_857_1312',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_857_1313 = Coupling(name = 'UVGC_857_1313',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y1LL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_857_1314 = Coupling(name = 'UVGC_857_1314',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_858_1315 = Coupling(name = 'UVGC_858_1315',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_858_1316 = Coupling(name = 'UVGC_858_1316',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y1LL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_858_1317 = Coupling(name = 'UVGC_858_1317',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL3x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_859_1318 = Coupling(name = 'UVGC_859_1318',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_859_1319 = Coupling(name = 'UVGC_859_1319',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y1RR3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_859_1320 = Coupling(name = 'UVGC_859_1320',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR3x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_860_1321 = Coupling(name = 'UVGC_860_1321',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_860_1322 = Coupling(name = 'UVGC_860_1322',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y1RR3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_860_1323 = Coupling(name = 'UVGC_860_1323',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR3x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_861_1324 = Coupling(name = 'UVGC_861_1324',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_861_1325 = Coupling(name = 'UVGC_861_1325',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y1RR3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_861_1326 = Coupling(name = 'UVGC_861_1326',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR3x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_862_1327 = Coupling(name = 'UVGC_862_1327',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_862_1328 = Coupling(name = 'UVGC_862_1328',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_862_1329 = Coupling(name = 'UVGC_862_1329',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL3x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_863_1330 = Coupling(name = 'UVGC_863_1330',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_863_1331 = Coupling(name = 'UVGC_863_1331',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_863_1332 = Coupling(name = 'UVGC_863_1332',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL3x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_864_1333 = Coupling(name = 'UVGC_864_1333',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_864_1334 = Coupling(name = 'UVGC_864_1334',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_864_1335 = Coupling(name = 'UVGC_864_1335',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL3x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_865_1336 = Coupling(name = 'UVGC_865_1336',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_865_1337 = Coupling(name = 'UVGC_865_1337',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_865_1338 = Coupling(name = 'UVGC_865_1338',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_866_1339 = Coupling(name = 'UVGC_866_1339',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_866_1340 = Coupling(name = 'UVGC_866_1340',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_866_1341 = Coupling(name = 'UVGC_866_1341',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_867_1342 = Coupling(name = 'UVGC_867_1342',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_867_1343 = Coupling(name = 'UVGC_867_1343',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_867_1344 = Coupling(name = 'UVGC_867_1344',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_868_1345 = Coupling(name = 'UVGC_868_1345',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_868_1346 = Coupling(name = 'UVGC_868_1346',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y2LR3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_869_1347 = Coupling(name = 'UVGC_869_1347',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_869_1348 = Coupling(name = 'UVGC_869_1348',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y2LR3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_870_1349 = Coupling(name = 'UVGC_870_1349',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_870_1350 = Coupling(name = 'UVGC_870_1350',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y2LR3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_871_1351 = Coupling(name = 'UVGC_871_1351',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_871_1352 = Coupling(name = 'UVGC_871_1352',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_871_1353 = Coupling(name = 'UVGC_871_1353',
                         value = '(complex(0,1)*G**2*invFREps*y2RL3x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_872_1354 = Coupling(name = 'UVGC_872_1354',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_872_1355 = Coupling(name = 'UVGC_872_1355',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_872_1356 = Coupling(name = 'UVGC_872_1356',
                         value = '(complex(0,1)*G**2*invFREps*y2RL3x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_873_1357 = Coupling(name = 'UVGC_873_1357',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_873_1358 = Coupling(name = 'UVGC_873_1358',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*y2RL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_873_1359 = Coupling(name = 'UVGC_873_1359',
                         value = '(complex(0,1)*G**2*invFREps*y2RL3x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_874_1360 = Coupling(name = 'UVGC_874_1360',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_874_1361 = Coupling(name = 'UVGC_874_1361',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y2LR3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_875_1362 = Coupling(name = 'UVGC_875_1362',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_875_1363 = Coupling(name = 'UVGC_875_1363',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y2LR3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_876_1364 = Coupling(name = 'UVGC_876_1364',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_876_1365 = Coupling(name = 'UVGC_876_1365',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y2LR3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_877_1366 = Coupling(name = 'UVGC_877_1366',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_877_1367 = Coupling(name = 'UVGC_877_1367',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_877_1368 = Coupling(name = 'UVGC_877_1368',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_878_1369 = Coupling(name = 'UVGC_878_1369',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_878_1370 = Coupling(name = 'UVGC_878_1370',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_878_1371 = Coupling(name = 'UVGC_878_1371',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_879_1372 = Coupling(name = 'UVGC_879_1372',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_879_1373 = Coupling(name = 'UVGC_879_1373',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1)*complexconjugate(y2RL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_879_1374 = Coupling(name = 'UVGC_879_1374',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL3x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_880_1375 = Coupling(name = 'UVGC_880_1375',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_881_1376 = Coupling(name = 'UVGC_881_1376',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_882_1377 = Coupling(name = 'UVGC_882_1377',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_883_1378 = Coupling(name = 'UVGC_883_1378',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_884_1379 = Coupling(name = 'UVGC_884_1379',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_885_1380 = Coupling(name = 'UVGC_885_1380',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_886_1381 = Coupling(name = 'UVGC_886_1381',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL3x1)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_886_1382 = Coupling(name = 'UVGC_886_1382',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x1)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_886_1383 = Coupling(name = 'UVGC_886_1383',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL3x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_887_1384 = Coupling(name = 'UVGC_887_1384',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL3x2)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_887_1385 = Coupling(name = 'UVGC_887_1385',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x2)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_887_1386 = Coupling(name = 'UVGC_887_1386',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL3x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_888_1387 = Coupling(name = 'UVGC_888_1387',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL3x3)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_888_1388 = Coupling(name = 'UVGC_888_1388',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*y3LL3x3)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_888_1389 = Coupling(name = 'UVGC_888_1389',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL3x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_889_1390 = Coupling(name = 'UVGC_889_1390',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL3x1))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_889_1391 = Coupling(name = 'UVGC_889_1391',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x1))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_889_1392 = Coupling(name = 'UVGC_889_1392',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_890_1393 = Coupling(name = 'UVGC_890_1393',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL3x2))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_890_1394 = Coupling(name = 'UVGC_890_1394',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x2))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_890_1395 = Coupling(name = 'UVGC_890_1395',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_891_1396 = Coupling(name = 'UVGC_891_1396',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL3x3))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_891_1397 = Coupling(name = 'UVGC_891_1397',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*complexconjugate(y3LL3x3))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_891_1398 = Coupling(name = 'UVGC_891_1398',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL3x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_892_1399 = Coupling(name = 'UVGC_892_1399',
                         value = '(ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_892_1400 = Coupling(name = 'UVGC_892_1400',
                         value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_893_1401 = Coupling(name = 'UVGC_893_1401',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_893_1402 = Coupling(name = 'UVGC_893_1402',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y1LL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_893_1403 = Coupling(name = 'UVGC_893_1403',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_894_1404 = Coupling(name = 'UVGC_894_1404',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_894_1405 = Coupling(name = 'UVGC_894_1405',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y1LL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_894_1406 = Coupling(name = 'UVGC_894_1406',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_895_1407 = Coupling(name = 'UVGC_895_1407',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1LL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_895_1408 = Coupling(name = 'UVGC_895_1408',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y1LL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_895_1409 = Coupling(name = 'UVGC_895_1409',
                         value = '-(complex(0,1)*G**2*invFREps*y1LL1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_896_1410 = Coupling(name = 'UVGC_896_1410',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_896_1411 = Coupling(name = 'UVGC_896_1411',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y1RR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_896_1412 = Coupling(name = 'UVGC_896_1412',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_897_1413 = Coupling(name = 'UVGC_897_1413',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_897_1414 = Coupling(name = 'UVGC_897_1414',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y1RR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_897_1415 = Coupling(name = 'UVGC_897_1415',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_898_1416 = Coupling(name = 'UVGC_898_1416',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*y1RR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_898_1417 = Coupling(name = 'UVGC_898_1417',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y1RR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_898_1418 = Coupling(name = 'UVGC_898_1418',
                         value = '-(complex(0,1)*G**2*invFREps*y1RR1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_899_1419 = Coupling(name = 'UVGC_899_1419',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_899_1420 = Coupling(name = 'UVGC_899_1420',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y1LL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_899_1421 = Coupling(name = 'UVGC_899_1421',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_900_1422 = Coupling(name = 'UVGC_900_1422',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_900_1423 = Coupling(name = 'UVGC_900_1423',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y1LL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_900_1424 = Coupling(name = 'UVGC_900_1424',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_901_1425 = Coupling(name = 'UVGC_901_1425',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1LL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_901_1426 = Coupling(name = 'UVGC_901_1426',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y1LL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_901_1427 = Coupling(name = 'UVGC_901_1427',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1LL1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_902_1428 = Coupling(name = 'UVGC_902_1428',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_902_1429 = Coupling(name = 'UVGC_902_1429',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y1RR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_902_1430 = Coupling(name = 'UVGC_902_1430',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_903_1431 = Coupling(name = 'UVGC_903_1431',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_903_1432 = Coupling(name = 'UVGC_903_1432',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y1RR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_903_1433 = Coupling(name = 'UVGC_903_1433',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_904_1434 = Coupling(name = 'UVGC_904_1434',
                         value = '(FRCTdeltaZxLQ1dLQ1dxGLQ1d*complex(0,1)*complexconjugate(y1RR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_904_1435 = Coupling(name = 'UVGC_904_1435',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y1RR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_904_1436 = Coupling(name = 'UVGC_904_1436',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y1RR1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_905_1437 = Coupling(name = 'UVGC_905_1437',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_905_1438 = Coupling(name = 'UVGC_905_1438',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_905_1439 = Coupling(name = 'UVGC_905_1439',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_906_1440 = Coupling(name = 'UVGC_906_1440',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_906_1441 = Coupling(name = 'UVGC_906_1441',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_906_1442 = Coupling(name = 'UVGC_906_1442',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_907_1443 = Coupling(name = 'UVGC_907_1443',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*y2RL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_907_1444 = Coupling(name = 'UVGC_907_1444',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_907_1445 = Coupling(name = 'UVGC_907_1445',
                         value = '-(complex(0,1)*G**2*invFREps*y2RL1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_908_1446 = Coupling(name = 'UVGC_908_1446',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_908_1447 = Coupling(name = 'UVGC_908_1447',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_908_1448 = Coupling(name = 'UVGC_908_1448',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_909_1449 = Coupling(name = 'UVGC_909_1449',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_909_1450 = Coupling(name = 'UVGC_909_1450',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_909_1451 = Coupling(name = 'UVGC_909_1451',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_910_1452 = Coupling(name = 'UVGC_910_1452',
                         value = '(FRCTdeltaZxLQ2uLQ2uxGLQ2u*complex(0,1)*complexconjugate(y2RL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_910_1453 = Coupling(name = 'UVGC_910_1453',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_910_1454 = Coupling(name = 'UVGC_910_1454',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_911_1455 = Coupling(name = 'UVGC_911_1455',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_911_1456 = Coupling(name = 'UVGC_911_1456',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y2LR1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_912_1457 = Coupling(name = 'UVGC_912_1457',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_912_1458 = Coupling(name = 'UVGC_912_1458',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y2LR1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_913_1459 = Coupling(name = 'UVGC_913_1459',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2LR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_913_1460 = Coupling(name = 'UVGC_913_1460',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y2LR1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_914_1461 = Coupling(name = 'UVGC_914_1461',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_914_1462 = Coupling(name = 'UVGC_914_1462',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_914_1463 = Coupling(name = 'UVGC_914_1463',
                         value = '(complex(0,1)*G**2*invFREps*y2RL1x1)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_915_1464 = Coupling(name = 'UVGC_915_1464',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_915_1465 = Coupling(name = 'UVGC_915_1465',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_915_1466 = Coupling(name = 'UVGC_915_1466',
                         value = '(complex(0,1)*G**2*invFREps*y2RL1x2)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_916_1467 = Coupling(name = 'UVGC_916_1467',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*y2RL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_916_1468 = Coupling(name = 'UVGC_916_1468',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*y2RL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_916_1469 = Coupling(name = 'UVGC_916_1469',
                         value = '(complex(0,1)*G**2*invFREps*y2RL1x3)/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_917_1470 = Coupling(name = 'UVGC_917_1470',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_917_1471 = Coupling(name = 'UVGC_917_1471',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y2LR1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_918_1472 = Coupling(name = 'UVGC_918_1472',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_918_1473 = Coupling(name = 'UVGC_918_1473',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y2LR1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_919_1474 = Coupling(name = 'UVGC_919_1474',
                         value = '(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2LR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_919_1475 = Coupling(name = 'UVGC_919_1475',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y2LR1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_920_1476 = Coupling(name = 'UVGC_920_1476',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_920_1477 = Coupling(name = 'UVGC_920_1477',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_920_1478 = Coupling(name = 'UVGC_920_1478',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x1))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_921_1479 = Coupling(name = 'UVGC_921_1479',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_921_1480 = Coupling(name = 'UVGC_921_1480',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_921_1481 = Coupling(name = 'UVGC_921_1481',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x2))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_922_1482 = Coupling(name = 'UVGC_922_1482',
                         value = '-(FRCTdeltaZxLQ2uuLQ2uuxGLQ2uu*complex(0,1)*complexconjugate(y2RL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_922_1483 = Coupling(name = 'UVGC_922_1483',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1)*complexconjugate(y2RL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_922_1484 = Coupling(name = 'UVGC_922_1484',
                         value = '(complex(0,1)*G**2*invFREps*complexconjugate(y2RL1x3))/(12.*cmath.pi**2)',
                         order = {'NEW':1,'QCD':2})

UVGC_923_1485 = Coupling(name = 'UVGC_923_1485',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x1)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_924_1486 = Coupling(name = 'UVGC_924_1486',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x2)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_925_1487 = Coupling(name = 'UVGC_925_1487',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x3)/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_926_1488 = Coupling(name = 'UVGC_926_1488',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x1))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_927_1489 = Coupling(name = 'UVGC_927_1489',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x2))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_928_1490 = Coupling(name = 'UVGC_928_1490',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x3))/2.',
                         order = {'NEW':1,'QCD':2})

UVGC_929_1491 = Coupling(name = 'UVGC_929_1491',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL1x1)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_929_1492 = Coupling(name = 'UVGC_929_1492',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x1)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_929_1493 = Coupling(name = 'UVGC_929_1493',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL1x1)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_930_1494 = Coupling(name = 'UVGC_930_1494',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL1x2)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_930_1495 = Coupling(name = 'UVGC_930_1495',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x2)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_930_1496 = Coupling(name = 'UVGC_930_1496',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL1x2)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_931_1497 = Coupling(name = 'UVGC_931_1497',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*y3LL1x3)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_931_1498 = Coupling(name = 'UVGC_931_1498',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*y3LL1x3)/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_931_1499 = Coupling(name = 'UVGC_931_1499',
                         value = '-(complex(0,1)*G**2*invFREps*y3LL1x3)/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_932_1500 = Coupling(name = 'UVGC_932_1500',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL1x1))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_932_1501 = Coupling(name = 'UVGC_932_1501',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x1))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_932_1502 = Coupling(name = 'UVGC_932_1502',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x1))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_933_1503 = Coupling(name = 'UVGC_933_1503',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL1x2))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_933_1504 = Coupling(name = 'UVGC_933_1504',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x2))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_933_1505 = Coupling(name = 'UVGC_933_1505',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x2))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_934_1506 = Coupling(name = 'UVGC_934_1506',
                         value = '(FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1)*complexconjugate(y3LL1x3))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_934_1507 = Coupling(name = 'UVGC_934_1507',
                         value = '(FRCTdeltaZxuuLxuG*complex(0,1)*complexconjugate(y3LL1x3))/cmath.sqrt(2)',
                         order = {'NEW':1,'QCD':2})

UVGC_934_1508 = Coupling(name = 'UVGC_934_1508',
                         value = '-(complex(0,1)*G**2*invFREps*complexconjugate(y3LL1x3))/(6.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'NEW':1,'QCD':2})

UVGC_935_1509 = Coupling(name = 'UVGC_935_1509',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_935_1510 = Coupling(name = 'UVGC_935_1510',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_936_1511 = Coupling(name = 'UVGC_936_1511',
                         value = '(-16*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_936_1512 = Coupling(name = 'UVGC_936_1512',
                         value = '(-16*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_936_1513 = Coupling(name = 'UVGC_936_1513',
                         value = '(4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_936_1514 = Coupling(name = 'UVGC_936_1514',
                         value = '(4*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_937_1515 = Coupling(name = 'UVGC_937_1515',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_937_1516 = Coupling(name = 'UVGC_937_1516',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_938_1517 = Coupling(name = 'UVGC_938_1517',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_938_1518 = Coupling(name = 'UVGC_938_1518',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_939_1519 = Coupling(name = 'UVGC_939_1519',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_939_1520 = Coupling(name = 'UVGC_939_1520',
                         value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_940_1521 = Coupling(name = 'UVGC_940_1521',
                         value = '(-25*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_940_1522 = Coupling(name = 'UVGC_940_1522',
                         value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_940_1523 = Coupling(name = 'UVGC_940_1523',
                         value = '(25*ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_940_1524 = Coupling(name = 'UVGC_940_1524',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (49*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_942_1525 = Coupling(name = 'UVGC_942_1525',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_942_1526 = Coupling(name = 'UVGC_942_1526',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_943_1527 = Coupling(name = 'UVGC_943_1527',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_943_1528 = Coupling(name = 'UVGC_943_1528',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_944_1529 = Coupling(name = 'UVGC_944_1529',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_944_1530 = Coupling(name = 'UVGC_944_1530',
                         value = '-((ee**2*FRCTdeltaZxLQ3ddLQ3ddxGLQ3dd*complex(0,1))/sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_944_1531 = Coupling(name = 'UVGC_944_1531',
                         value = '-((ee**2*FRCTdeltaZxLQ3uLQ3uxGLQ3u*complex(0,1))/sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_944_1532 = Coupling(name = 'UVGC_944_1532',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_945_1533 = Coupling(name = 'UVGC_945_1533',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_945_1534 = Coupling(name = 'UVGC_945_1534',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_945_1535 = Coupling(name = 'UVGC_945_1535',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_946_1536 = Coupling(name = 'UVGC_946_1536',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_946_1537 = Coupling(name = 'UVGC_946_1537',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_946_1538 = Coupling(name = 'UVGC_946_1538',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_947_1539 = Coupling(name = 'UVGC_947_1539',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_947_1540 = Coupling(name = 'UVGC_947_1540',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_947_1541 = Coupling(name = 'UVGC_947_1541',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_948_1542 = Coupling(name = 'UVGC_948_1542',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_948_1543 = Coupling(name = 'UVGC_948_1543',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_948_1544 = Coupling(name = 'UVGC_948_1544',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_949_1545 = Coupling(name = 'UVGC_949_1545',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_949_1546 = Coupling(name = 'UVGC_949_1546',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_950_1547 = Coupling(name = 'UVGC_950_1547',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_956_1548 = Coupling(name = 'UVGC_956_1548',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_956_1549 = Coupling(name = 'UVGC_956_1549',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_956_1550 = Coupling(name = 'UVGC_956_1550',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_957_1551 = Coupling(name = 'UVGC_957_1551',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_957_1552 = Coupling(name = 'UVGC_957_1552',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_957_1553 = Coupling(name = 'UVGC_957_1553',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_958_1554 = Coupling(name = 'UVGC_958_1554',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_958_1555 = Coupling(name = 'UVGC_958_1555',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_958_1556 = Coupling(name = 'UVGC_958_1556',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_958_1557 = Coupling(name = 'UVGC_958_1557',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_958_1558 = Coupling(name = 'UVGC_958_1558',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_959_1559 = Coupling(name = 'UVGC_959_1559',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_959_1560 = Coupling(name = 'UVGC_959_1560',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_959_1561 = Coupling(name = 'UVGC_959_1561',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_959_1562 = Coupling(name = 'UVGC_959_1562',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_959_1563 = Coupling(name = 'UVGC_959_1563',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_960_1564 = Coupling(name = 'UVGC_960_1564',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_960_1565 = Coupling(name = 'UVGC_960_1565',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_960_1566 = Coupling(name = 'UVGC_960_1566',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_961_1567 = Coupling(name = 'UVGC_961_1567',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_961_1568 = Coupling(name = 'UVGC_961_1568',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_961_1569 = Coupling(name = 'UVGC_961_1569',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_966_1570 = Coupling(name = 'UVGC_966_1570',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_966_1571 = Coupling(name = 'UVGC_966_1571',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_967_1572 = Coupling(name = 'UVGC_967_1572',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_967_1573 = Coupling(name = 'UVGC_967_1573',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_972_1574 = Coupling(name = 'UVGC_972_1574',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_972_1575 = Coupling(name = 'UVGC_972_1575',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_973_1576 = Coupling(name = 'UVGC_973_1576',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_973_1577 = Coupling(name = 'UVGC_973_1577',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_974_1578 = Coupling(name = 'UVGC_974_1578',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_974_1579 = Coupling(name = 'UVGC_974_1579',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_975_1580 = Coupling(name = 'UVGC_975_1580',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_975_1581 = Coupling(name = 'UVGC_975_1581',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_976_1582 = Coupling(name = 'UVGC_976_1582',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_976_1583 = Coupling(name = 'UVGC_976_1583',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_977_1584 = Coupling(name = 'UVGC_977_1584',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_977_1585 = Coupling(name = 'UVGC_977_1585',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_978_1586 = Coupling(name = 'UVGC_978_1586',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_978_1587 = Coupling(name = 'UVGC_978_1587',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_979_1588 = Coupling(name = 'UVGC_979_1588',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_979_1589 = Coupling(name = 'UVGC_979_1589',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_982_1590 = Coupling(name = 'UVGC_982_1590',
                         value = '(8*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_982_1591 = Coupling(name = 'UVGC_982_1591',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_982_1592 = Coupling(name = 'UVGC_982_1592',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_982_1593 = Coupling(name = 'UVGC_982_1593',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_983_1594 = Coupling(name = 'UVGC_983_1594',
                         value = '(-8*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_983_1595 = Coupling(name = 'UVGC_983_1595',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_983_1596 = Coupling(name = 'UVGC_983_1596',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_983_1597 = Coupling(name = 'UVGC_983_1597',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_984_1598 = Coupling(name = 'UVGC_984_1598',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_984_1599 = Coupling(name = 'UVGC_984_1599',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_985_1600 = Coupling(name = 'UVGC_985_1600',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_985_1601 = Coupling(name = 'UVGC_985_1601',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_988_1602 = Coupling(name = 'UVGC_988_1602',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_988_1603 = Coupling(name = 'UVGC_988_1603',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_988_1604 = Coupling(name = 'UVGC_988_1604',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_989_1605 = Coupling(name = 'UVGC_989_1605',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_989_1606 = Coupling(name = 'UVGC_989_1606',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_989_1607 = Coupling(name = 'UVGC_989_1607',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_990_1608 = Coupling(name = 'UVGC_990_1608',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_991_1609 = Coupling(name = 'UVGC_991_1609',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_991_1610 = Coupling(name = 'UVGC_991_1610',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_991_1611 = Coupling(name = 'UVGC_991_1611',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_992_1612 = Coupling(name = 'UVGC_992_1612',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_992_1613 = Coupling(name = 'UVGC_992_1613',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_993_1614 = Coupling(name = 'UVGC_993_1614',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_993_1615 = Coupling(name = 'UVGC_993_1615',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_996_1616 = Coupling(name = 'UVGC_996_1616',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_997_1617 = Coupling(name = 'UVGC_997_1617',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_997_1618 = Coupling(name = 'UVGC_997_1618',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_997_1619 = Coupling(name = 'UVGC_997_1619',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cw**2*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

