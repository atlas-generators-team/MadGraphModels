# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 11 Jun 2020 16:07:45



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
Ity1RR1x1 = Parameter(name = 'Ity1RR1x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0121,
                      texname = '\\text{Ity1RR1x1}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 1, 1 ])

Ity1RR1x2 = Parameter(name = 'Ity1RR1x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0124,
                      texname = '\\text{Ity1RR1x2}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 1, 2 ])

Ity1RR1x3 = Parameter(name = 'Ity1RR1x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0127,
                      texname = '\\text{Ity1RR1x3}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 1, 3 ])

Ity1RR2x1 = Parameter(name = 'Ity1RR2x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0122,
                      texname = '\\text{Ity1RR2x1}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 2, 1 ])

Ity1RR2x2 = Parameter(name = 'Ity1RR2x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0125,
                      texname = '\\text{Ity1RR2x2}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 2, 2 ])

Ity1RR2x3 = Parameter(name = 'Ity1RR2x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0128,
                      texname = '\\text{Ity1RR2x3}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 2, 3 ])

Ity1RR3x1 = Parameter(name = 'Ity1RR3x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0123,
                      texname = '\\text{Ity1RR3x1}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 3, 1 ])

Ity1RR3x2 = Parameter(name = 'Ity1RR3x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0126,
                      texname = '\\text{Ity1RR3x2}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 3, 2 ])

Ity1RR3x3 = Parameter(name = 'Ity1RR3x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0129,
                      texname = '\\text{Ity1RR3x3}',
                      lhablock = 'IMLQTY1RR',
                      lhacode = [ 3, 3 ])

Ity2RL1x1 = Parameter(name = 'Ity2RL1x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0151,
                      texname = '\\text{Ity2RL1x1}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 1, 1 ])

Ity2RL1x2 = Parameter(name = 'Ity2RL1x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0154,
                      texname = '\\text{Ity2RL1x2}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 1, 2 ])

Ity2RL1x3 = Parameter(name = 'Ity2RL1x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0157,
                      texname = '\\text{Ity2RL1x3}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 1, 3 ])

Ity2RL2x1 = Parameter(name = 'Ity2RL2x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0152,
                      texname = '\\text{Ity2RL2x1}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 2, 1 ])

Ity2RL2x2 = Parameter(name = 'Ity2RL2x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0155,
                      texname = '\\text{Ity2RL2x2}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 2, 2 ])

Ity2RL2x3 = Parameter(name = 'Ity2RL2x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0158,
                      texname = '\\text{Ity2RL2x3}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 2, 3 ])

Ity2RL3x1 = Parameter(name = 'Ity2RL3x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.0153,
                      texname = '\\text{Ity2RL3x1}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 3, 1 ])

Ity2RL3x2 = Parameter(name = 'Ity2RL3x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.0156,
                      texname = '\\text{Ity2RL3x2}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 3, 2 ])

Ity2RL3x3 = Parameter(name = 'Ity2RL3x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.0159,
                      texname = '\\text{Ity2RL3x3}',
                      lhablock = 'IMLQTY2RL',
                      lhacode = [ 3, 3 ])

Iy1LL1x1 = Parameter(name = 'Iy1LL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0101,
                     texname = '\\text{Iy1LL1x1}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 1, 1 ])

Iy1LL1x2 = Parameter(name = 'Iy1LL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0104,
                     texname = '\\text{Iy1LL1x2}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 1, 2 ])

Iy1LL1x3 = Parameter(name = 'Iy1LL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0107,
                     texname = '\\text{Iy1LL1x3}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 1, 3 ])

Iy1LL2x1 = Parameter(name = 'Iy1LL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0102,
                     texname = '\\text{Iy1LL2x1}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 2, 1 ])

Iy1LL2x2 = Parameter(name = 'Iy1LL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0105,
                     texname = '\\text{Iy1LL2x2}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 2, 2 ])

Iy1LL2x3 = Parameter(name = 'Iy1LL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0108,
                     texname = '\\text{Iy1LL2x3}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 2, 3 ])

Iy1LL3x1 = Parameter(name = 'Iy1LL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0103,
                     texname = '\\text{Iy1LL3x1}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 3, 1 ])

Iy1LL3x2 = Parameter(name = 'Iy1LL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0106,
                     texname = '\\text{Iy1LL3x2}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 3, 2 ])

Iy1LL3x3 = Parameter(name = 'Iy1LL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0109,
                     texname = '\\text{Iy1LL3x3}',
                     lhablock = 'IMLQY1LL',
                     lhacode = [ 3, 3 ])

Iy1RR1x1 = Parameter(name = 'Iy1RR1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0111,
                     texname = '\\text{Iy1RR1x1}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 1, 1 ])

Iy1RR1x2 = Parameter(name = 'Iy1RR1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0114,
                     texname = '\\text{Iy1RR1x2}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 1, 2 ])

Iy1RR1x3 = Parameter(name = 'Iy1RR1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0117,
                     texname = '\\text{Iy1RR1x3}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 1, 3 ])

Iy1RR2x1 = Parameter(name = 'Iy1RR2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0112,
                     texname = '\\text{Iy1RR2x1}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 2, 1 ])

Iy1RR2x2 = Parameter(name = 'Iy1RR2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0115,
                     texname = '\\text{Iy1RR2x2}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 2, 2 ])

Iy1RR2x3 = Parameter(name = 'Iy1RR2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0118,
                     texname = '\\text{Iy1RR2x3}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 2, 3 ])

Iy1RR3x1 = Parameter(name = 'Iy1RR3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0113,
                     texname = '\\text{Iy1RR3x1}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 3, 1 ])

Iy1RR3x2 = Parameter(name = 'Iy1RR3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0116,
                     texname = '\\text{Iy1RR3x2}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 3, 2 ])

Iy1RR3x3 = Parameter(name = 'Iy1RR3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0119,
                     texname = '\\text{Iy1RR3x3}',
                     lhablock = 'IMLQY1RR',
                     lhacode = [ 3, 3 ])

Iy2LR1x1 = Parameter(name = 'Iy2LR1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0131,
                     texname = '\\text{Iy2LR1x1}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 1, 1 ])

Iy2LR1x2 = Parameter(name = 'Iy2LR1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0134,
                     texname = '\\text{Iy2LR1x2}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 1, 2 ])

Iy2LR1x3 = Parameter(name = 'Iy2LR1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0137,
                     texname = '\\text{Iy2LR1x3}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 1, 3 ])

Iy2LR2x1 = Parameter(name = 'Iy2LR2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0132,
                     texname = '\\text{Iy2LR2x1}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 2, 1 ])

Iy2LR2x2 = Parameter(name = 'Iy2LR2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0135,
                     texname = '\\text{Iy2LR2x2}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 2, 2 ])

Iy2LR2x3 = Parameter(name = 'Iy2LR2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0138,
                     texname = '\\text{Iy2LR2x3}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 2, 3 ])

Iy2LR3x1 = Parameter(name = 'Iy2LR3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0133,
                     texname = '\\text{Iy2LR3x1}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 3, 1 ])

Iy2LR3x2 = Parameter(name = 'Iy2LR3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0136,
                     texname = '\\text{Iy2LR3x2}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 3, 2 ])

Iy2LR3x3 = Parameter(name = 'Iy2LR3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0139,
                     texname = '\\text{Iy2LR3x3}',
                     lhablock = 'IMLQY2LR',
                     lhacode = [ 3, 3 ])

Iy2RL1x1 = Parameter(name = 'Iy2RL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0141,
                     texname = '\\text{Iy2RL1x1}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 1, 1 ])

Iy2RL1x2 = Parameter(name = 'Iy2RL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0144,
                     texname = '\\text{Iy2RL1x2}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 1, 2 ])

Iy2RL1x3 = Parameter(name = 'Iy2RL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0147,
                     texname = '\\text{Iy2RL1x3}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 1, 3 ])

Iy2RL2x1 = Parameter(name = 'Iy2RL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0142,
                     texname = '\\text{Iy2RL2x1}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 2, 1 ])

Iy2RL2x2 = Parameter(name = 'Iy2RL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0145,
                     texname = '\\text{Iy2RL2x2}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 2, 2 ])

Iy2RL2x3 = Parameter(name = 'Iy2RL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0148,
                     texname = '\\text{Iy2RL2x3}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 2, 3 ])

Iy2RL3x1 = Parameter(name = 'Iy2RL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0143,
                     texname = '\\text{Iy2RL3x1}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 3, 1 ])

Iy2RL3x2 = Parameter(name = 'Iy2RL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0146,
                     texname = '\\text{Iy2RL3x2}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 3, 2 ])

Iy2RL3x3 = Parameter(name = 'Iy2RL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0149,
                     texname = '\\text{Iy2RL3x3}',
                     lhablock = 'IMLQY2RL',
                     lhacode = [ 3, 3 ])

Iy3LL1x1 = Parameter(name = 'Iy3LL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0161,
                     texname = '\\text{Iy3LL1x1}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 1, 1 ])

Iy3LL1x2 = Parameter(name = 'Iy3LL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0164,
                     texname = '\\text{Iy3LL1x2}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 1, 2 ])

Iy3LL1x3 = Parameter(name = 'Iy3LL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0167,
                     texname = '\\text{Iy3LL1x3}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 1, 3 ])

Iy3LL2x1 = Parameter(name = 'Iy3LL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0162,
                     texname = '\\text{Iy3LL2x1}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 2, 1 ])

Iy3LL2x2 = Parameter(name = 'Iy3LL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0165,
                     texname = '\\text{Iy3LL2x2}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 2, 2 ])

Iy3LL2x3 = Parameter(name = 'Iy3LL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0168,
                     texname = '\\text{Iy3LL2x3}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 2, 3 ])

Iy3LL3x1 = Parameter(name = 'Iy3LL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.0163,
                     texname = '\\text{Iy3LL3x1}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 3, 1 ])

Iy3LL3x2 = Parameter(name = 'Iy3LL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.0166,
                     texname = '\\text{Iy3LL3x2}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 3, 2 ])

Iy3LL3x3 = Parameter(name = 'Iy3LL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.0169,
                     texname = '\\text{Iy3LL3x3}',
                     lhablock = 'IMLQY3LL',
                     lhacode = [ 3, 3 ])

Rty1RR1x1 = Parameter(name = 'Rty1RR1x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.121,
                      texname = '\\text{Rty1RR1x1}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 1, 1 ])

Rty1RR1x2 = Parameter(name = 'Rty1RR1x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.124,
                      texname = '\\text{Rty1RR1x2}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 1, 2 ])

Rty1RR1x3 = Parameter(name = 'Rty1RR1x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.127,
                      texname = '\\text{Rty1RR1x3}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 1, 3 ])

Rty1RR2x1 = Parameter(name = 'Rty1RR2x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.122,
                      texname = '\\text{Rty1RR2x1}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 2, 1 ])

Rty1RR2x2 = Parameter(name = 'Rty1RR2x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.125,
                      texname = '\\text{Rty1RR2x2}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 2, 2 ])

Rty1RR2x3 = Parameter(name = 'Rty1RR2x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.128,
                      texname = '\\text{Rty1RR2x3}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 2, 3 ])

Rty1RR3x1 = Parameter(name = 'Rty1RR3x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.123,
                      texname = '\\text{Rty1RR3x1}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 3, 1 ])

Rty1RR3x2 = Parameter(name = 'Rty1RR3x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.126,
                      texname = '\\text{Rty1RR3x2}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 3, 2 ])

Rty1RR3x3 = Parameter(name = 'Rty1RR3x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.129,
                      texname = '\\text{Rty1RR3x3}',
                      lhablock = 'LQTY1RR',
                      lhacode = [ 3, 3 ])

Rty2RL1x1 = Parameter(name = 'Rty2RL1x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.151,
                      texname = '\\text{Rty2RL1x1}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 1, 1 ])

Rty2RL1x2 = Parameter(name = 'Rty2RL1x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.154,
                      texname = '\\text{Rty2RL1x2}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 1, 2 ])

Rty2RL1x3 = Parameter(name = 'Rty2RL1x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.157,
                      texname = '\\text{Rty2RL1x3}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 1, 3 ])

Rty2RL2x1 = Parameter(name = 'Rty2RL2x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.152,
                      texname = '\\text{Rty2RL2x1}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 2, 1 ])

Rty2RL2x2 = Parameter(name = 'Rty2RL2x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.155,
                      texname = '\\text{Rty2RL2x2}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 2, 2 ])

Rty2RL2x3 = Parameter(name = 'Rty2RL2x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.158,
                      texname = '\\text{Rty2RL2x3}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 2, 3 ])

Rty2RL3x1 = Parameter(name = 'Rty2RL3x1',
                      nature = 'external',
                      type = 'real',
                      value = 0.153,
                      texname = '\\text{Rty2RL3x1}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 3, 1 ])

Rty2RL3x2 = Parameter(name = 'Rty2RL3x2',
                      nature = 'external',
                      type = 'real',
                      value = 0.156,
                      texname = '\\text{Rty2RL3x2}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 3, 2 ])

Rty2RL3x3 = Parameter(name = 'Rty2RL3x3',
                      nature = 'external',
                      type = 'real',
                      value = 0.159,
                      texname = '\\text{Rty2RL3x3}',
                      lhablock = 'LQTY2RL',
                      lhacode = [ 3, 3 ])

Ry1LL1x1 = Parameter(name = 'Ry1LL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.101,
                     texname = '\\text{Ry1LL1x1}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 1, 1 ])

Ry1LL1x2 = Parameter(name = 'Ry1LL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.104,
                     texname = '\\text{Ry1LL1x2}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 1, 2 ])

Ry1LL1x3 = Parameter(name = 'Ry1LL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.107,
                     texname = '\\text{Ry1LL1x3}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 1, 3 ])

Ry1LL2x1 = Parameter(name = 'Ry1LL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.102,
                     texname = '\\text{Ry1LL2x1}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 2, 1 ])

Ry1LL2x2 = Parameter(name = 'Ry1LL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.105,
                     texname = '\\text{Ry1LL2x2}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 2, 2 ])

Ry1LL2x3 = Parameter(name = 'Ry1LL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.108,
                     texname = '\\text{Ry1LL2x3}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 2, 3 ])

Ry1LL3x1 = Parameter(name = 'Ry1LL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.103,
                     texname = '\\text{Ry1LL3x1}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 3, 1 ])

Ry1LL3x2 = Parameter(name = 'Ry1LL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.106,
                     texname = '\\text{Ry1LL3x2}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 3, 2 ])

Ry1LL3x3 = Parameter(name = 'Ry1LL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.109,
                     texname = '\\text{Ry1LL3x3}',
                     lhablock = 'LQY1LL',
                     lhacode = [ 3, 3 ])

Ry1RR1x1 = Parameter(name = 'Ry1RR1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.111,
                     texname = '\\text{Ry1RR1x1}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 1, 1 ])

Ry1RR1x2 = Parameter(name = 'Ry1RR1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.114,
                     texname = '\\text{Ry1RR1x2}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 1, 2 ])

Ry1RR1x3 = Parameter(name = 'Ry1RR1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.117,
                     texname = '\\text{Ry1RR1x3}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 1, 3 ])

Ry1RR2x1 = Parameter(name = 'Ry1RR2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.112,
                     texname = '\\text{Ry1RR2x1}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 2, 1 ])

Ry1RR2x2 = Parameter(name = 'Ry1RR2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.115,
                     texname = '\\text{Ry1RR2x2}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 2, 2 ])

Ry1RR2x3 = Parameter(name = 'Ry1RR2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.118,
                     texname = '\\text{Ry1RR2x3}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 2, 3 ])

Ry1RR3x1 = Parameter(name = 'Ry1RR3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.113,
                     texname = '\\text{Ry1RR3x1}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 3, 1 ])

Ry1RR3x2 = Parameter(name = 'Ry1RR3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.116,
                     texname = '\\text{Ry1RR3x2}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 3, 2 ])

Ry1RR3x3 = Parameter(name = 'Ry1RR3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.119,
                     texname = '\\text{Ry1RR3x3}',
                     lhablock = 'LQY1RR',
                     lhacode = [ 3, 3 ])

Ry2LR1x1 = Parameter(name = 'Ry2LR1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.131,
                     texname = '\\text{Ry2LR1x1}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 1, 1 ])

Ry2LR1x2 = Parameter(name = 'Ry2LR1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.134,
                     texname = '\\text{Ry2LR1x2}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 1, 2 ])

Ry2LR1x3 = Parameter(name = 'Ry2LR1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.137,
                     texname = '\\text{Ry2LR1x3}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 1, 3 ])

Ry2LR2x1 = Parameter(name = 'Ry2LR2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.132,
                     texname = '\\text{Ry2LR2x1}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 2, 1 ])

Ry2LR2x2 = Parameter(name = 'Ry2LR2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.135,
                     texname = '\\text{Ry2LR2x2}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 2, 2 ])

Ry2LR2x3 = Parameter(name = 'Ry2LR2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.138,
                     texname = '\\text{Ry2LR2x3}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 2, 3 ])

Ry2LR3x1 = Parameter(name = 'Ry2LR3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.133,
                     texname = '\\text{Ry2LR3x1}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 3, 1 ])

Ry2LR3x2 = Parameter(name = 'Ry2LR3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.136,
                     texname = '\\text{Ry2LR3x2}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 3, 2 ])

Ry2LR3x3 = Parameter(name = 'Ry2LR3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.139,
                     texname = '\\text{Ry2LR3x3}',
                     lhablock = 'LQY2LR',
                     lhacode = [ 3, 3 ])

Ry2RL1x1 = Parameter(name = 'Ry2RL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.141,
                     texname = '\\text{Ry2RL1x1}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 1, 1 ])

Ry2RL1x2 = Parameter(name = 'Ry2RL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.144,
                     texname = '\\text{Ry2RL1x2}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 1, 2 ])

Ry2RL1x3 = Parameter(name = 'Ry2RL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.147,
                     texname = '\\text{Ry2RL1x3}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 1, 3 ])

Ry2RL2x1 = Parameter(name = 'Ry2RL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.142,
                     texname = '\\text{Ry2RL2x1}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 2, 1 ])

Ry2RL2x2 = Parameter(name = 'Ry2RL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.145,
                     texname = '\\text{Ry2RL2x2}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 2, 2 ])

Ry2RL2x3 = Parameter(name = 'Ry2RL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.148,
                     texname = '\\text{Ry2RL2x3}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 2, 3 ])

Ry2RL3x1 = Parameter(name = 'Ry2RL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.143,
                     texname = '\\text{Ry2RL3x1}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 3, 1 ])

Ry2RL3x2 = Parameter(name = 'Ry2RL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.146,
                     texname = '\\text{Ry2RL3x2}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 3, 2 ])

Ry2RL3x3 = Parameter(name = 'Ry2RL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.149,
                     texname = '\\text{Ry2RL3x3}',
                     lhablock = 'LQY2RL',
                     lhacode = [ 3, 3 ])

Ry3LL1x1 = Parameter(name = 'Ry3LL1x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.161,
                     texname = '\\text{Ry3LL1x1}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 1, 1 ])

Ry3LL1x2 = Parameter(name = 'Ry3LL1x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.164,
                     texname = '\\text{Ry3LL1x2}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 1, 2 ])

Ry3LL1x3 = Parameter(name = 'Ry3LL1x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.167,
                     texname = '\\text{Ry3LL1x3}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 1, 3 ])

Ry3LL2x1 = Parameter(name = 'Ry3LL2x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.162,
                     texname = '\\text{Ry3LL2x1}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 2, 1 ])

Ry3LL2x2 = Parameter(name = 'Ry3LL2x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.165,
                     texname = '\\text{Ry3LL2x2}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 2, 2 ])

Ry3LL2x3 = Parameter(name = 'Ry3LL2x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.168,
                     texname = '\\text{Ry3LL2x3}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 2, 3 ])

Ry3LL3x1 = Parameter(name = 'Ry3LL3x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.163,
                     texname = '\\text{Ry3LL3x1}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 3, 1 ])

Ry3LL3x2 = Parameter(name = 'Ry3LL3x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.166,
                     texname = '\\text{Ry3LL3x2}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 3, 2 ])

Ry3LL3x3 = Parameter(name = 'Ry3LL3x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.169,
                     texname = '\\text{Ry3LL3x3}',
                     lhablock = 'LQY3LL',
                     lhacode = [ 3, 3 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MS1 = Parameter(name = 'MS1',
                nature = 'external',
                type = 'real',
                value = 1000.,
                texname = '\\text{MS1}',
                lhablock = 'MASS',
                lhacode = [ 6000042 ])

MR2 = Parameter(name = 'MR2',
                nature = 'external',
                type = 'real',
                value = 1001.,
                texname = '\\text{MR2}',
                lhablock = 'MASS',
                lhacode = [ 6100242 ])

MS3 = Parameter(name = 'MS3',
                nature = 'external',
                type = 'real',
                value = 1002.,
                texname = '\\text{MS3}',
                lhablock = 'MASS',
                lhacode = [ 6001242 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WLQ1d = Parameter(name = 'WLQ1d',
                  nature = 'external',
                  type = 'real',
                  value = 10.,
                  texname = '\\text{WLQ1d}',
                  lhablock = 'DECAY',
                  lhacode = [ 42 ])

WLQ1dd = Parameter(name = 'WLQ1dd',
                   nature = 'external',
                   type = 'real',
                   value = 10.1,
                   texname = '\\text{WLQ1dd}',
                   lhablock = 'DECAY',
                   lhacode = [ 6000042 ])

WLQ2uu = Parameter(name = 'WLQ2uu',
                   nature = 'external',
                   type = 'real',
                   value = 11.,
                   texname = '\\text{WLQ2uu}',
                   lhablock = 'DECAY',
                   lhacode = [ 6000142 ])

WLQ2u = Parameter(name = 'WLQ2u',
                  nature = 'external',
                  type = 'real',
                  value = 11.1,
                  texname = '\\text{WLQ2u}',
                  lhablock = 'DECAY',
                  lhacode = [ 6000242 ])

WLQ2pu = Parameter(name = 'WLQ2pu',
                   nature = 'external',
                   type = 'real',
                   value = 11.2,
                   texname = '\\text{WLQ2pu}',
                   lhablock = 'DECAY',
                   lhacode = [ 6100142 ])

WLQ2d = Parameter(name = 'WLQ2d',
                  nature = 'external',
                  type = 'real',
                  value = 11.3,
                  texname = '\\text{WLQ2d}',
                  lhablock = 'DECAY',
                  lhacode = [ 6100242 ])

WLQ3u = Parameter(name = 'WLQ3u',
                  nature = 'external',
                  type = 'real',
                  value = 12.1,
                  texname = '\\text{WLQ3u}',
                  lhablock = 'DECAY',
                  lhacode = [ 6001042 ])

WLQ3d = Parameter(name = 'WLQ3d',
                  nature = 'external',
                  type = 'real',
                  value = 12.2,
                  texname = '\\text{WLQ3d}',
                  lhablock = 'DECAY',
                  lhacode = [ 6001142 ])

WLQ3dd = Parameter(name = 'WLQ3dd',
                   nature = 'external',
                   type = 'real',
                   value = 12.3,
                   texname = '\\text{WLQ3dd}',
                   lhablock = 'DECAY',
                   lhacode = [ 6001242 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

ty1RR1x1 = Parameter(name = 'ty1RR1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR1x1 + Rty1RR1x1',
                     texname = '\\text{ty1RR1x1}')

ty1RR1x2 = Parameter(name = 'ty1RR1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR1x2 + Rty1RR1x2',
                     texname = '\\text{ty1RR1x2}')

ty1RR1x3 = Parameter(name = 'ty1RR1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR1x3 + Rty1RR1x3',
                     texname = '\\text{ty1RR1x3}')

ty1RR2x1 = Parameter(name = 'ty1RR2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR2x1 + Rty1RR2x1',
                     texname = '\\text{ty1RR2x1}')

ty1RR2x2 = Parameter(name = 'ty1RR2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR2x2 + Rty1RR2x2',
                     texname = '\\text{ty1RR2x2}')

ty1RR2x3 = Parameter(name = 'ty1RR2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR2x3 + Rty1RR2x3',
                     texname = '\\text{ty1RR2x3}')

ty1RR3x1 = Parameter(name = 'ty1RR3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR3x1 + Rty1RR3x1',
                     texname = '\\text{ty1RR3x1}')

ty1RR3x2 = Parameter(name = 'ty1RR3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR3x2 + Rty1RR3x2',
                     texname = '\\text{ty1RR3x2}')

ty1RR3x3 = Parameter(name = 'ty1RR3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity1RR3x3 + Rty1RR3x3',
                     texname = '\\text{ty1RR3x3}')

ty2RL1x1 = Parameter(name = 'ty2RL1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL1x1 + Rty2RL1x1',
                     texname = '\\text{ty2RL1x1}')

ty2RL1x2 = Parameter(name = 'ty2RL1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL1x2 + Rty2RL1x2',
                     texname = '\\text{ty2RL1x2}')

ty2RL1x3 = Parameter(name = 'ty2RL1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL1x3 + Rty2RL1x3',
                     texname = '\\text{ty2RL1x3}')

ty2RL2x1 = Parameter(name = 'ty2RL2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL2x1 + Rty2RL2x1',
                     texname = '\\text{ty2RL2x1}')

ty2RL2x2 = Parameter(name = 'ty2RL2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL2x2 + Rty2RL2x2',
                     texname = '\\text{ty2RL2x2}')

ty2RL2x3 = Parameter(name = 'ty2RL2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL2x3 + Rty2RL2x3',
                     texname = '\\text{ty2RL2x3}')

ty2RL3x1 = Parameter(name = 'ty2RL3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL3x1 + Rty2RL3x1',
                     texname = '\\text{ty2RL3x1}')

ty2RL3x2 = Parameter(name = 'ty2RL3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL3x2 + Rty2RL3x2',
                     texname = '\\text{ty2RL3x2}')

ty2RL3x3 = Parameter(name = 'ty2RL3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Ity2RL3x3 + Rty2RL3x3',
                     texname = '\\text{ty2RL3x3}')

y1LL1x1 = Parameter(name = 'y1LL1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL1x1 + Ry1LL1x1',
                    texname = '\\text{y1LL1x1}')

y1LL1x2 = Parameter(name = 'y1LL1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL1x2 + Ry1LL1x2',
                    texname = '\\text{y1LL1x2}')

y1LL1x3 = Parameter(name = 'y1LL1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL1x3 + Ry1LL1x3',
                    texname = '\\text{y1LL1x3}')

y1LL2x1 = Parameter(name = 'y1LL2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL2x1 + Ry1LL2x1',
                    texname = '\\text{y1LL2x1}')

y1LL2x2 = Parameter(name = 'y1LL2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL2x2 + Ry1LL2x2',
                    texname = '\\text{y1LL2x2}')

y1LL2x3 = Parameter(name = 'y1LL2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL2x3 + Ry1LL2x3',
                    texname = '\\text{y1LL2x3}')

y1LL3x1 = Parameter(name = 'y1LL3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL3x1 + Ry1LL3x1',
                    texname = '\\text{y1LL3x1}')

y1LL3x2 = Parameter(name = 'y1LL3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL3x2 + Ry1LL3x2',
                    texname = '\\text{y1LL3x2}')

y1LL3x3 = Parameter(name = 'y1LL3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1LL3x3 + Ry1LL3x3',
                    texname = '\\text{y1LL3x3}')

y1RR1x1 = Parameter(name = 'y1RR1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR1x1 + Ry1RR1x1',
                    texname = '\\text{y1RR1x1}')

y1RR1x2 = Parameter(name = 'y1RR1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR1x2 + Ry1RR1x2',
                    texname = '\\text{y1RR1x2}')

y1RR1x3 = Parameter(name = 'y1RR1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR1x3 + Ry1RR1x3',
                    texname = '\\text{y1RR1x3}')

y1RR2x1 = Parameter(name = 'y1RR2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR2x1 + Ry1RR2x1',
                    texname = '\\text{y1RR2x1}')

y1RR2x2 = Parameter(name = 'y1RR2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR2x2 + Ry1RR2x2',
                    texname = '\\text{y1RR2x2}')

y1RR2x3 = Parameter(name = 'y1RR2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR2x3 + Ry1RR2x3',
                    texname = '\\text{y1RR2x3}')

y1RR3x1 = Parameter(name = 'y1RR3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR3x1 + Ry1RR3x1',
                    texname = '\\text{y1RR3x1}')

y1RR3x2 = Parameter(name = 'y1RR3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR3x2 + Ry1RR3x2',
                    texname = '\\text{y1RR3x2}')

y1RR3x3 = Parameter(name = 'y1RR3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy1RR3x3 + Ry1RR3x3',
                    texname = '\\text{y1RR3x3}')

y2LR1x1 = Parameter(name = 'y2LR1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR1x1 + Ry2LR1x1',
                    texname = '\\text{y2LR1x1}')

y2LR1x2 = Parameter(name = 'y2LR1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR1x2 + Ry2LR1x2',
                    texname = '\\text{y2LR1x2}')

y2LR1x3 = Parameter(name = 'y2LR1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR1x3 + Ry2LR1x3',
                    texname = '\\text{y2LR1x3}')

y2LR2x1 = Parameter(name = 'y2LR2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR2x1 + Ry2LR2x1',
                    texname = '\\text{y2LR2x1}')

y2LR2x2 = Parameter(name = 'y2LR2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR2x2 + Ry2LR2x2',
                    texname = '\\text{y2LR2x2}')

y2LR2x3 = Parameter(name = 'y2LR2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR2x3 + Ry2LR2x3',
                    texname = '\\text{y2LR2x3}')

y2LR3x1 = Parameter(name = 'y2LR3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR3x1 + Ry2LR3x1',
                    texname = '\\text{y2LR3x1}')

y2LR3x2 = Parameter(name = 'y2LR3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR3x2 + Ry2LR3x2',
                    texname = '\\text{y2LR3x2}')

y2LR3x3 = Parameter(name = 'y2LR3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2LR3x3 + Ry2LR3x3',
                    texname = '\\text{y2LR3x3}')

y2RL1x1 = Parameter(name = 'y2RL1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL1x1 + Ry2RL1x1',
                    texname = '\\text{y2RL1x1}')

y2RL1x2 = Parameter(name = 'y2RL1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL1x2 + Ry2RL1x2',
                    texname = '\\text{y2RL1x2}')

y2RL1x3 = Parameter(name = 'y2RL1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL1x3 + Ry2RL1x3',
                    texname = '\\text{y2RL1x3}')

y2RL2x1 = Parameter(name = 'y2RL2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL2x1 + Ry2RL2x1',
                    texname = '\\text{y2RL2x1}')

y2RL2x2 = Parameter(name = 'y2RL2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL2x2 + Ry2RL2x2',
                    texname = '\\text{y2RL2x2}')

y2RL2x3 = Parameter(name = 'y2RL2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL2x3 + Ry2RL2x3',
                    texname = '\\text{y2RL2x3}')

y2RL3x1 = Parameter(name = 'y2RL3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL3x1 + Ry2RL3x1',
                    texname = '\\text{y2RL3x1}')

y2RL3x2 = Parameter(name = 'y2RL3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL3x2 + Ry2RL3x2',
                    texname = '\\text{y2RL3x2}')

y2RL3x3 = Parameter(name = 'y2RL3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy2RL3x3 + Ry2RL3x3',
                    texname = '\\text{y2RL3x3}')

y3LL1x1 = Parameter(name = 'y3LL1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL1x1 + Ry3LL1x1',
                    texname = '\\text{y3LL1x1}')

y3LL1x2 = Parameter(name = 'y3LL1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL1x2 + Ry3LL1x2',
                    texname = '\\text{y3LL1x2}')

y3LL1x3 = Parameter(name = 'y3LL1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL1x3 + Ry3LL1x3',
                    texname = '\\text{y3LL1x3}')

y3LL2x1 = Parameter(name = 'y3LL2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL2x1 + Ry3LL2x1',
                    texname = '\\text{y3LL2x1}')

y3LL2x2 = Parameter(name = 'y3LL2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL2x2 + Ry3LL2x2',
                    texname = '\\text{y3LL2x2}')

y3LL2x3 = Parameter(name = 'y3LL2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL2x3 + Ry3LL2x3',
                    texname = '\\text{y3LL2x3}')

y3LL3x1 = Parameter(name = 'y3LL3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL3x1 + Ry3LL3x1',
                    texname = '\\text{y3LL3x1}')

y3LL3x2 = Parameter(name = 'y3LL3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL3x2 + Ry3LL3x2',
                    texname = '\\text{y3LL3x2}')

y3LL3x3 = Parameter(name = 'y3LL3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Iy3LL3x3 + Ry3LL3x3',
                    texname = '\\text{y3LL3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

