# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 11 Jun 2020 16:07:50


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_422_160,(0,0,1):C.R2GC_422_161})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,1,0):C.R2GC_415_153,(2,1,1):C.R2GC_415_154,(0,1,0):C.R2GC_415_153,(0,1,1):C.R2GC_415_154,(4,1,0):C.R2GC_413_149,(4,1,1):C.R2GC_413_150,(3,1,0):C.R2GC_413_149,(3,1,1):C.R2GC_413_150,(8,1,0):C.R2GC_414_151,(8,1,1):C.R2GC_414_152,(7,1,0):C.R2GC_424_163,(7,1,1):C.R2GC_420_159,(6,1,0):C.R2GC_423_162,(6,1,1):C.R2GC_425_164,(5,1,0):C.R2GC_413_149,(5,1,1):C.R2GC_413_150,(1,1,0):C.R2GC_413_149,(1,1,1):C.R2GC_413_150,(11,0,0):C.R2GC_417_156,(11,0,1):C.R2GC_417_157,(10,0,0):C.R2GC_417_156,(10,0,1):C.R2GC_417_157,(9,0,1):C.R2GC_416_155,(2,2,0):C.R2GC_415_153,(2,2,1):C.R2GC_415_154,(0,2,0):C.R2GC_415_153,(0,2,1):C.R2GC_415_154,(6,2,0):C.R2GC_428_166,(6,2,1):C.R2GC_428_167,(4,2,0):C.R2GC_413_149,(4,2,1):C.R2GC_413_150,(3,2,0):C.R2GC_413_149,(3,2,1):C.R2GC_413_150,(8,2,0):C.R2GC_414_151,(8,2,1):C.R2GC_420_159,(7,2,0):C.R2GC_424_163,(7,2,1):C.R2GC_414_152,(5,2,0):C.R2GC_413_149,(5,2,1):C.R2GC_413_150,(1,2,0):C.R2GC_413_149,(1,2,1):C.R2GC_413_150,(2,3,0):C.R2GC_415_153,(2,3,1):C.R2GC_415_154,(0,3,0):C.R2GC_415_153,(0,3,1):C.R2GC_415_154,(4,3,0):C.R2GC_413_149,(4,3,1):C.R2GC_413_150,(3,3,0):C.R2GC_413_149,(3,3,1):C.R2GC_413_150,(8,3,0):C.R2GC_414_151,(8,3,1):C.R2GC_419_158,(6,3,0):C.R2GC_423_162,(7,3,0):C.R2GC_427_165,(7,3,1):C.R2GC_419_158,(5,3,0):C.R2GC_413_149,(5,3,1):C.R2GC_413_150,(1,3,0):C.R2GC_413_149,(1,3,1):C.R2GC_413_150})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.a, P.LQ1d__tilde__, P.LQ1d ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.VSS2 ],
               loop_particles = [ [ [P.g, P.LQ1d] ] ],
               couplings = {(0,0,0):C.R2GC_445_176})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.a, P.a, P.LQ1d__tilde__, P.LQ1d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.VVSS1 ],
               loop_particles = [ [ [P.g, P.LQ1d] ] ],
               couplings = {(0,0,0):C.R2GC_446_177})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.a, P.LQ1dd__tilde__, P.LQ1dd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.VSS2 ],
               loop_particles = [ [ [P.g, P.LQ1dd] ] ],
               couplings = {(0,0,0):C.R2GC_458_190})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.a, P.a, P.LQ1dd__tilde__, P.LQ1dd ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.VVSS1 ],
               loop_particles = [ [ [P.g, P.LQ1dd] ] ],
               couplings = {(0,0,0):C.R2GC_459_191})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.a, P.LQ2d__tilde__, P.LQ2d ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.VSS2 ],
               loop_particles = [ [ [P.g, P.LQ2d] ] ],
               couplings = {(0,0,0):C.R2GC_445_176})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.a, P.a, P.LQ2d__tilde__, P.LQ2d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.VVSS1 ],
               loop_particles = [ [ [P.g, P.LQ2d] ] ],
               couplings = {(0,0,0):C.R2GC_446_177})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.a, P.LQ2pu__tilde__, P.LQ2pu ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.VSS2 ],
               loop_particles = [ [ [P.g, P.LQ2pu] ] ],
               couplings = {(0,0,0):C.R2GC_483_201})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ2pu__tilde__, P.LQ2pu ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_484_202})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.a, P.LQ2u__tilde__, P.LQ2u ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VSS2 ],
                loop_particles = [ [ [P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_483_201})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ2u__tilde__, P.LQ2u ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_484_202})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.a, P.LQ2uu__tilde__, P.LQ2uu ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VSS2 ],
                loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_510_213})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ2uu__tilde__, P.LQ2uu ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_1018_93})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.a, P.LQ3d__tilde__, P.LQ3d ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VSS2 ],
                loop_particles = [ [ [P.g, P.LQ3d] ] ],
                couplings = {(0,0,0):C.R2GC_445_176})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ3d__tilde__, P.LQ3d ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ3d] ] ],
                couplings = {(0,0,0):C.R2GC_446_177})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.a, P.LQ3dd__tilde__, P.LQ3dd ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VSS2 ],
                loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                couplings = {(0,0,0):C.R2GC_458_190})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ3dd__tilde__, P.LQ3dd ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                couplings = {(0,0,0):C.R2GC_459_191})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.a, P.LQ3u__tilde__, P.LQ3u ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VSS2 ],
                loop_particles = [ [ [P.g, P.LQ3u] ] ],
                couplings = {(0,0,0):C.R2GC_483_201})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.a, P.a, P.LQ3u__tilde__, P.LQ3u ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g, P.LQ3u] ] ],
                couplings = {(0,0,0):C.R2GC_484_202})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_848_417})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_573_230})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_572_229})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.t, P.e__minus__, P.LQ1d__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_859_428,(0,1,0):C.R2GC_856_425})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.u, P.e__minus__, P.LQ1d__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_902_458,(0,1,0):C.R2GC_899_455})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.t, P.mu__minus__, P.LQ1d__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_860_429,(0,1,0):C.R2GC_857_426})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.u, P.mu__minus__, P.LQ1d__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_903_459,(0,1,0):C.R2GC_900_456})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.u, P.ta__minus__, P.LQ1d__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_904_460,(0,1,0):C.R2GC_901_457})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.e__minus__, P.c, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_630_282,(0,1,0):C.R2GC_627_279})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.mu__minus__, P.c, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_631_283,(0,1,0):C.R2GC_628_280})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.ta__minus__, P.c, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_632_284,(0,1,0):C.R2GC_629_281})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.ta__minus__, P.t, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_861_430,(0,1,0):C.R2GC_858_427})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.s, P.e__minus__, P.LQ1dd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_814_396})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.s, P.mu__minus__, P.LQ1dd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_815_397})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.e__minus__, P.b, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_588_240})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.mu__minus__, P.b, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_589_241})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.ta__minus__, P.b, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_590_242})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.e__minus__, P.d, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_673_325})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.mu__minus__, P.d, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_674_326})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.ta__minus__, P.d, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                couplings = {(0,0,0):C.R2GC_675_327})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.ta__minus__, P.s, P.LQ1dd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_816_398})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.e__plus__, P.u, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_914_467,(0,1,0):C.R2GC_691_343})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.e__plus__, P.d, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_691_343})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.mu__plus__, P.u, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_915_468,(0,1,0):C.R2GC_692_344})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.mu__plus__, P.d, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_692_344})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.ta__plus__, P.u, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_916_469,(0,1,0):C.R2GC_693_345})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.ta__plus__, P.d, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_693_345})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.e__plus__, P.c, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_642_294,(0,1,0):C.R2GC_645_297})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.e__plus__, P.s, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_645_297})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.mu__plus__, P.c, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_643_295,(0,1,0):C.R2GC_646_298})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.mu__plus__, P.s, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_646_298})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.ta__plus__, P.c, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_644_296,(0,1,0):C.R2GC_647_299})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.ta__plus__, P.s, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_647_299})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.e__plus__, P.t, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_871_437,(0,1,0):C.R2GC_606_258})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.e__plus__, P.b, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_606_258})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.mu__plus__, P.t, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_872_438,(0,1,0):C.R2GC_607_259})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.mu__plus__, P.b, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_607_259})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.ta__plus__, P.t, P.LQ2uu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_873_439,(0,1,0):C.R2GC_608_260})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.ta__plus__, P.b, P.LQ2u__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_608_260})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.c__tilde__, P.e__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_639_291,(0,1,0):C.R2GC_648_300})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.t__tilde__, P.e__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_603_255,(0,1,0):C.R2GC_877_440})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.u__tilde__, P.e__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_688_340,(0,1,0):C.R2GC_920_470})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.c__tilde__, P.mu__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_640_292,(0,1,0):C.R2GC_649_301})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.t__tilde__, P.mu__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_604_256,(0,1,0):C.R2GC_878_441})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.u__tilde__, P.mu__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_689_341,(0,1,0):C.R2GC_921_471})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.c__tilde__, P.ta__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2uu] ] ],
                couplings = {(0,0,0):C.R2GC_641_293,(0,1,0):C.R2GC_650_302})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.t__tilde__, P.ta__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_605_257,(0,1,0):C.R2GC_879_442})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.u__tilde__, P.ta__minus__, P.LQ2uu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2uu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_690_342,(0,1,0):C.R2GC_922_472})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.c__tilde__, P.ve, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_636_288})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.t__tilde__, P.ve, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_865_434})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.u__tilde__, P.ve, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_908_464})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.c__tilde__, P.vm, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_637_289})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.t__tilde__, P.vm, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_866_435})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.u__tilde__, P.vm, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_909_465})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.c__tilde__, P.vt, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                couplings = {(0,0,0):C.R2GC_638_290})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.t__tilde__, P.vt, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_867_436})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.u__tilde__, P.vt, P.LQ2u ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_910_466})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.b__tilde__, P.e__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_600_252})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.d__tilde__, P.e__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_685_337})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.s__tilde__, P.e__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_826_408})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.b__tilde__, P.mu__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_601_253})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.d__tilde__, P.mu__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_686_338})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.s__tilde__, P.mu__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_827_409})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.b__tilde__, P.ta__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_602_254})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.d__tilde__, P.ta__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                couplings = {(0,0,0):C.R2GC_687_339})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.s__tilde__, P.ta__minus__, P.LQ2pu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_828_410})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.b__tilde__, P.ve, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_594_246})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.d__tilde__, P.ve, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_679_331})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.s__tilde__, P.ve, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_820_402})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.b__tilde__, P.vm, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_595_247})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.d__tilde__, P.vm, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_680_332})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.s__tilde__, P.vm, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_821_403})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.b__tilde__, P.vt, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_596_248})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.d__tilde__, P.vt, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                couplings = {(0,0,0):C.R2GC_681_333})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.s__tilde__, P.vt, P.LQ2d ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_822_404})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.ve, P.b, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_582_234})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.vm, P.b, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_583_235})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.vt, P.b, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_584_236})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.ve, P.d, P.LQ1d__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS5 ],
                loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                couplings = {(0,0,0):C.R2GC_667_319})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.vm, P.d, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_668_320})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.vt, P.d, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_669_321})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.ve, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_808_390})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.vm, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_809_391})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.vt, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_810_392})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.e__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_703_355})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.ve, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_697_349})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.mu__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_704_356})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.vm, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_698_350})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.ta__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_705_357})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.vt, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_699_351})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.ve, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_654_306})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.vm, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_655_307})

V_113 = CTVertex(name = 'V_113',
                 type = 'R2',
                 particles = [ P.ta__minus__, P.s, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_846_416})

V_114 = CTVertex(name = 'V_114',
                 type = 'R2',
                 particles = [ P.vt, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_656_308})

V_115 = CTVertex(name = 'V_115',
                 type = 'R2',
                 particles = [ P.e__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_618_270})

V_116 = CTVertex(name = 'V_116',
                 type = 'R2',
                 particles = [ P.ve, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_612_264})

V_117 = CTVertex(name = 'V_117',
                 type = 'R2',
                 particles = [ P.mu__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_619_271})

V_118 = CTVertex(name = 'V_118',
                 type = 'R2',
                 particles = [ P.vm, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_613_265})

V_119 = CTVertex(name = 'V_119',
                 type = 'R2',
                 particles = [ P.ta__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_620_272})

V_120 = CTVertex(name = 'V_120',
                 type = 'R2',
                 particles = [ P.vt, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_614_266})

V_121 = CTVertex(name = 'V_121',
                 type = 'R2',
                 particles = [ P.ve, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_932_476})

V_122 = CTVertex(name = 'V_122',
                 type = 'R2',
                 particles = [ P.vm, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_933_477})

V_123 = CTVertex(name = 'V_123',
                 type = 'R2',
                 particles = [ P.vt, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_934_478})

V_124 = CTVertex(name = 'V_124',
                 type = 'R2',
                 particles = [ P.ve, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_660_312})

V_125 = CTVertex(name = 'V_125',
                 type = 'R2',
                 particles = [ P.e__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_654_306})

V_126 = CTVertex(name = 'V_126',
                 type = 'R2',
                 particles = [ P.vm, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_661_313})

V_127 = CTVertex(name = 'V_127',
                 type = 'R2',
                 particles = [ P.mu__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_655_307})

V_128 = CTVertex(name = 'V_128',
                 type = 'R2',
                 particles = [ P.vt, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_662_314})

V_129 = CTVertex(name = 'V_129',
                 type = 'R2',
                 particles = [ P.ta__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_656_308})

V_130 = CTVertex(name = 'V_130',
                 type = 'R2',
                 particles = [ P.ve, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_889_446})

V_131 = CTVertex(name = 'V_131',
                 type = 'R2',
                 particles = [ P.vm, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_890_447})

V_132 = CTVertex(name = 'V_132',
                 type = 'R2',
                 particles = [ P.vt, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_891_448})

V_133 = CTVertex(name = 'V_133',
                 type = 'R2',
                 particles = [ P.ta__minus__, P.t, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_614_266})

V_134 = CTVertex(name = 'V_134',
                 type = 'R2',
                 particles = [ P.u, P.e__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_697_349})

V_135 = CTVertex(name = 'V_135',
                 type = 'R2',
                 particles = [ P.u, P.mu__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_698_350})

V_136 = CTVertex(name = 'V_136',
                 type = 'R2',
                 particles = [ P.u, P.ta__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_699_351})

V_137 = CTVertex(name = 'V_137',
                 type = 'R2',
                 particles = [ P.s, P.e__minus__, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_844_414})

V_138 = CTVertex(name = 'V_138',
                 type = 'R2',
                 particles = [ P.s, P.mu__minus__, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_845_415})

V_139 = CTVertex(name = 'V_139',
                 type = 'R2',
                 particles = [ P.t, P.e__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_612_264})

V_140 = CTVertex(name = 'V_140',
                 type = 'R2',
                 particles = [ P.t, P.mu__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_613_265})

V_141 = CTVertex(name = 'V_141',
                 type = 'R2',
                 particles = [ P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_142 = CTVertex(name = 'V_142',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_448_179})

V_143 = CTVertex(name = 'V_143',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_144 = CTVertex(name = 'V_144',
                 type = 'R2',
                 particles = [ P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_145 = CTVertex(name = 'V_145',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_461_192})

V_146 = CTVertex(name = 'V_146',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_147 = CTVertex(name = 'V_147',
                 type = 'R2',
                 particles = [ P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_148 = CTVertex(name = 'V_148',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_486_203})

V_149 = CTVertex(name = 'V_149',
                 type = 'R2',
                 particles = [ P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_150 = CTVertex(name = 'V_150',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_513_214})

V_151 = CTVertex(name = 'V_151',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_152 = CTVertex(name = 'V_152',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_153 = CTVertex(name = 'V_153',
                 type = 'R2',
                 particles = [ P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_154 = CTVertex(name = 'V_154',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_448_179})

V_155 = CTVertex(name = 'V_155',
                 type = 'R2',
                 particles = [ P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_156 = CTVertex(name = 'V_156',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_486_203})

V_157 = CTVertex(name = 'V_157',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_158 = CTVertex(name = 'V_158',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_159 = CTVertex(name = 'V_159',
                 type = 'R2',
                 particles = [ P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_160 = CTVertex(name = 'V_160',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_448_179})

V_161 = CTVertex(name = 'V_161',
                 type = 'R2',
                 particles = [ P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_162 = CTVertex(name = 'V_162',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_461_192})

V_163 = CTVertex(name = 'V_163',
                 type = 'R2',
                 particles = [ P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_447_178})

V_164 = CTVertex(name = 'V_164',
                 type = 'R2',
                 particles = [ P.a, P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_486_203})

V_165 = CTVertex(name = 'V_165',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_166 = CTVertex(name = 'V_166',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_167 = CTVertex(name = 'V_167',
                 type = 'R2',
                 particles = [ P.g, P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(2,0,0):C.R2GC_451_183,(2,0,1):C.R2GC_451_184,(1,0,0):C.R2GC_451_183,(1,0,1):C.R2GC_451_184,(0,0,0):C.R2GC_417_157,(0,0,1):C.R2GC_450_182})

V_168 = CTVertex(name = 'V_168',
                 type = 'R2',
                 particles = [ P.e__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_670_322})

V_169 = CTVertex(name = 'V_169',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_671_323})

V_170 = CTVertex(name = 'V_170',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_672_324})

V_171 = CTVertex(name = 'V_171',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.e__plus__, P.LQ1dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_811_393})

V_172 = CTVertex(name = 'V_172',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.mu__plus__, P.LQ1dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_812_394})

V_173 = CTVertex(name = 'V_173',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.s__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_813_395})

V_174 = CTVertex(name = 'V_174',
                 type = 'R2',
                 particles = [ P.e__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_585_237})

V_175 = CTVertex(name = 'V_175',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_586_238})

V_176 = CTVertex(name = 'V_176',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_587_239})

V_177 = CTVertex(name = 'V_177',
                 type = 'R2',
                 particles = [ P.e__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_682_334})

V_178 = CTVertex(name = 'V_178',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_676_328})

V_179 = CTVertex(name = 'V_179',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_683_335})

V_180 = CTVertex(name = 'V_180',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_677_329})

V_181 = CTVertex(name = 'V_181',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_684_336})

V_182 = CTVertex(name = 'V_182',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_678_330})

V_183 = CTVertex(name = 'V_183',
                 type = 'R2',
                 particles = [ P.e__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_823_405})

V_184 = CTVertex(name = 'V_184',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_817_399})

V_185 = CTVertex(name = 'V_185',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_824_406})

V_186 = CTVertex(name = 'V_186',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_818_400})

V_187 = CTVertex(name = 'V_187',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_825_407})

V_188 = CTVertex(name = 'V_188',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_819_401})

V_189 = CTVertex(name = 'V_189',
                 type = 'R2',
                 particles = [ P.e__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_597_249})

V_190 = CTVertex(name = 'V_190',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_591_243})

V_191 = CTVertex(name = 'V_191',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_598_250})

V_192 = CTVertex(name = 'V_192',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_592_244})

V_193 = CTVertex(name = 'V_193',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_599_251})

V_194 = CTVertex(name = 'V_194',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_593_245})

V_195 = CTVertex(name = 'V_195',
                 type = 'R2',
                 particles = [ P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_748_368})

V_196 = CTVertex(name = 'V_196',
                 type = 'R2',
                 particles = [ P.a, P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_749_369})

V_197 = CTVertex(name = 'V_197',
                 type = 'R2',
                 particles = [ P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_748_368})

V_198 = CTVertex(name = 'V_198',
                 type = 'R2',
                 particles = [ P.a, P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_778_373})

V_199 = CTVertex(name = 'V_199',
                 type = 'R2',
                 particles = [ P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_790_377})

V_200 = CTVertex(name = 'V_200',
                 type = 'R2',
                 particles = [ P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_790_377})

V_201 = CTVertex(name = 'V_201',
                 type = 'R2',
                 particles = [ P.a, P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_792_379})

V_202 = CTVertex(name = 'V_202',
                 type = 'R2',
                 particles = [ P.a, P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_801_384})

V_203 = CTVertex(name = 'V_203',
                 type = 'R2',
                 particles = [ P.g, P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_750_370,(0,0,1):C.R2GC_750_371})

V_204 = CTVertex(name = 'V_204',
                 type = 'R2',
                 particles = [ P.g, P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_750_370,(0,0,1):C.R2GC_750_371})

V_205 = CTVertex(name = 'V_205',
                 type = 'R2',
                 particles = [ P.g, P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_793_380,(0,0,1):C.R2GC_793_381})

V_206 = CTVertex(name = 'V_206',
                 type = 'R2',
                 particles = [ P.g, P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d], [P.g, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_802_385,(0,0,1):C.R2GC_802_386})

V_207 = CTVertex(name = 'V_207',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__minus__, P.LQ3dd__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3dd, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_1023_113,(0,0,1):C.R2GC_944_500})

V_208 = CTVertex(name = 'V_208',
                 type = 'R2',
                 particles = [ P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_747_367})

V_209 = CTVertex(name = 'V_209',
                 type = 'R2',
                 particles = [ P.a, P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_749_369})

V_210 = CTVertex(name = 'V_210',
                 type = 'R2',
                 particles = [ P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_747_367})

V_211 = CTVertex(name = 'V_211',
                 type = 'R2',
                 particles = [ P.a, P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_778_373})

V_212 = CTVertex(name = 'V_212',
                 type = 'R2',
                 particles = [ P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_791_378})

V_213 = CTVertex(name = 'V_213',
                 type = 'R2',
                 particles = [ P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_791_378})

V_214 = CTVertex(name = 'V_214',
                 type = 'R2',
                 particles = [ P.a, P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_792_379})

V_215 = CTVertex(name = 'V_215',
                 type = 'R2',
                 particles = [ P.a, P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_801_384})

V_216 = CTVertex(name = 'V_216',
                 type = 'R2',
                 particles = [ P.g, P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_750_370,(0,0,1):C.R2GC_750_371})

V_217 = CTVertex(name = 'V_217',
                 type = 'R2',
                 particles = [ P.g, P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_750_370,(0,0,1):C.R2GC_750_371})

V_218 = CTVertex(name = 'V_218',
                 type = 'R2',
                 particles = [ P.g, P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_793_380,(0,0,1):C.R2GC_793_381})

V_219 = CTVertex(name = 'V_219',
                 type = 'R2',
                 particles = [ P.g, P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d], [P.g, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_802_385,(0,0,1):C.R2GC_802_386})

V_220 = CTVertex(name = 'V_220',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_745_365,(0,0,1):C.R2GC_745_366})

V_221 = CTVertex(name = 'V_221',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,1):C.R2GC_745_365,(0,0,0):C.R2GC_745_366})

V_222 = CTVertex(name = 'V_222',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_745_365,(0,0,1):C.R2GC_745_366})

V_223 = CTVertex(name = 'V_223',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,1):C.R2GC_745_365,(0,0,0):C.R2GC_745_366})

V_224 = CTVertex(name = 'V_224',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3dd], [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_797_382,(0,0,1):C.R2GC_788_375})

V_225 = CTVertex(name = 'V_225',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_788_374,(0,0,1):C.R2GC_788_375})

V_226 = CTVertex(name = 'V_226',
                 type = 'R2',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,1):C.R2GC_788_374,(0,0,0):C.R2GC_788_375})

V_227 = CTVertex(name = 'V_227',
                 type = 'R2',
                 particles = [ P.W__plus__, P.W__plus__, P.LQ3dd, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3dd, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_1023_113,(0,0,1):C.R2GC_944_500})

V_228 = CTVertex(name = 'V_228',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.e__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_893_449,(0,1,0):C.R2GC_896_452})

V_229 = CTVertex(name = 'V_229',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_664_316})

V_230 = CTVertex(name = 'V_230',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.mu__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_894_450,(0,1,0):C.R2GC_897_453})

V_231 = CTVertex(name = 'V_231',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_665_317})

V_232 = CTVertex(name = 'V_232',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.ta__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_895_451,(0,1,0):C.R2GC_898_454})

V_233 = CTVertex(name = 'V_233',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_666_318})

V_234 = CTVertex(name = 'V_234',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_805_387})

V_235 = CTVertex(name = 'V_235',
                 type = 'R2',
                 particles = [ P.e__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_621_273,(0,1,0):C.R2GC_624_276})

V_236 = CTVertex(name = 'V_236',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_806_388})

V_237 = CTVertex(name = 'V_237',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_622_274,(0,1,0):C.R2GC_625_277})

V_238 = CTVertex(name = 'V_238',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_807_389})

V_239 = CTVertex(name = 'V_239',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_623_275,(0,1,0):C.R2GC_626_278})

V_240 = CTVertex(name = 'V_240',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.e__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_850_419,(0,1,0):C.R2GC_853_422})

V_241 = CTVertex(name = 'V_241',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_579_231})

V_242 = CTVertex(name = 'V_242',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.mu__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_851_420,(0,1,0):C.R2GC_854_423})

V_243 = CTVertex(name = 'V_243',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_580_232})

V_244 = CTVertex(name = 'V_244',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_581_233})

V_245 = CTVertex(name = 'V_245',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.t__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_852_421,(0,1,0):C.R2GC_855_424})

V_246 = CTVertex(name = 'V_246',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_688_340})

V_247 = CTVertex(name = 'V_247',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_689_341})

V_248 = CTVertex(name = 'V_248',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_690_342})

V_249 = CTVertex(name = 'V_249',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_639_291})

V_250 = CTVertex(name = 'V_250',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_640_292})

V_251 = CTVertex(name = 'V_251',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_641_293})

V_252 = CTVertex(name = 'V_252',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_603_255})

V_253 = CTVertex(name = 'V_253',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_604_256})

V_254 = CTVertex(name = 'V_254',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_605_257})

V_255 = CTVertex(name = 'V_255',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_905_461})

V_256 = CTVertex(name = 'V_256',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_906_462})

V_257 = CTVertex(name = 'V_257',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_907_463})

V_258 = CTVertex(name = 'V_258',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_633_285})

V_259 = CTVertex(name = 'V_259',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_634_286})

V_260 = CTVertex(name = 'V_260',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_635_287})

V_261 = CTVertex(name = 'V_261',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_862_431})

V_262 = CTVertex(name = 'V_262',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_863_432})

V_263 = CTVertex(name = 'V_263',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_864_433})

V_264 = CTVertex(name = 'V_264',
                 type = 'R2',
                 particles = [ P.e__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_700_352})

V_265 = CTVertex(name = 'V_265',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_694_346})

V_266 = CTVertex(name = 'V_266',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_929_473})

V_267 = CTVertex(name = 'V_267',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.e__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_694_346})

V_268 = CTVertex(name = 'V_268',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_701_353})

V_269 = CTVertex(name = 'V_269',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_695_347})

V_270 = CTVertex(name = 'V_270',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_930_474})

V_271 = CTVertex(name = 'V_271',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.mu__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_695_347})

V_272 = CTVertex(name = 'V_272',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_702_354})

V_273 = CTVertex(name = 'V_273',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_696_348})

V_274 = CTVertex(name = 'V_274',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_931_475})

V_275 = CTVertex(name = 'V_275',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.ta__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_696_348})

V_276 = CTVertex(name = 'V_276',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_651_303})

V_277 = CTVertex(name = 'V_277',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_657_309})

V_278 = CTVertex(name = 'V_278',
                 type = 'R2',
                 particles = [ P.e__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_651_303})

V_279 = CTVertex(name = 'V_279',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.e__plus__, P.LQ3dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_841_411})

V_280 = CTVertex(name = 'V_280',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_652_304})

V_281 = CTVertex(name = 'V_281',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_658_310})

V_282 = CTVertex(name = 'V_282',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_652_304})

V_283 = CTVertex(name = 'V_283',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.mu__plus__, P.LQ3dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_842_412})

V_284 = CTVertex(name = 'V_284',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.s__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_843_413})

V_285 = CTVertex(name = 'V_285',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_653_305})

V_286 = CTVertex(name = 'V_286',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_659_311})

V_287 = CTVertex(name = 'V_287',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_653_305})

V_288 = CTVertex(name = 'V_288',
                 type = 'R2',
                 particles = [ P.e__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_615_267})

V_289 = CTVertex(name = 'V_289',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_609_261})

V_290 = CTVertex(name = 'V_290',
                 type = 'R2',
                 particles = [ P.ve__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_886_443})

V_291 = CTVertex(name = 'V_291',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.e__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_609_261})

V_292 = CTVertex(name = 'V_292',
                 type = 'R2',
                 particles = [ P.mu__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_616_268})

V_293 = CTVertex(name = 'V_293',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_610_262})

V_294 = CTVertex(name = 'V_294',
                 type = 'R2',
                 particles = [ P.vm__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_887_444})

V_295 = CTVertex(name = 'V_295',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.mu__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_610_262})

V_296 = CTVertex(name = 'V_296',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_617_269})

V_297 = CTVertex(name = 'V_297',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_611_263})

V_298 = CTVertex(name = 'V_298',
                 type = 'R2',
                 particles = [ P.vt__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_888_445})

V_299 = CTVertex(name = 'V_299',
                 type = 'R2',
                 particles = [ P.ta__plus__, P.t__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_611_263})

V_300 = CTVertex(name = 'V_300',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_849_418})

V_301 = CTVertex(name = 'V_301',
                 type = 'R2',
                 particles = [ P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_453_186})

V_302 = CTVertex(name = 'V_302',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_454_187})

V_303 = CTVertex(name = 'V_303',
                 type = 'R2',
                 particles = [ P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_466_193})

V_304 = CTVertex(name = 'V_304',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_467_194})

V_305 = CTVertex(name = 'V_305',
                 type = 'R2',
                 particles = [ P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_478_197})

V_306 = CTVertex(name = 'V_306',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_479_198})

V_307 = CTVertex(name = 'V_307',
                 type = 'R2',
                 particles = [ P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_490_204})

V_308 = CTVertex(name = 'V_308',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_491_205})

V_309 = CTVertex(name = 'V_309',
                 type = 'R2',
                 particles = [ P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_502_208})

V_310 = CTVertex(name = 'V_310',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_503_209})

V_311 = CTVertex(name = 'V_311',
                 type = 'R2',
                 particles = [ P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_518_215})

V_312 = CTVertex(name = 'V_312',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_519_216})

V_313 = CTVertex(name = 'V_313',
                 type = 'R2',
                 particles = [ P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_453_186})

V_314 = CTVertex(name = 'V_314',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_454_187})

V_315 = CTVertex(name = 'V_315',
                 type = 'R2',
                 particles = [ P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_542_219})

V_316 = CTVertex(name = 'V_316',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_543_220})

V_317 = CTVertex(name = 'V_317',
                 type = 'R2',
                 particles = [ P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_557_224})

V_318 = CTVertex(name = 'V_318',
                 type = 'R2',
                 particles = [ P.a, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_558_225})

V_319 = CTVertex(name = 'V_319',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_455_188})

V_320 = CTVertex(name = 'V_320',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_468_195})

V_321 = CTVertex(name = 'V_321',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_504_210})

V_322 = CTVertex(name = 'V_322',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_520_217})

V_323 = CTVertex(name = 'V_323',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_480_199})

V_324 = CTVertex(name = 'V_324',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_492_206})

V_325 = CTVertex(name = 'V_325',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_455_188})

V_326 = CTVertex(name = 'V_326',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_544_221})

V_327 = CTVertex(name = 'V_327',
                 type = 'R2',
                 particles = [ P.g, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_559_226})

V_328 = CTVertex(name = 'V_328',
                 type = 'R2',
                 particles = [ P.W__minus__, P.Z, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_742_364})

V_329 = CTVertex(name = 'V_329',
                 type = 'R2',
                 particles = [ P.W__minus__, P.Z, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_771_372})

V_330 = CTVertex(name = 'V_330',
                 type = 'R2',
                 particles = [ P.W__minus__, P.Z, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_789_376})

V_331 = CTVertex(name = 'V_331',
                 type = 'R2',
                 particles = [ P.W__minus__, P.Z, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_798_383})

V_332 = CTVertex(name = 'V_332',
                 type = 'R2',
                 particles = [ P.W__plus__, P.Z, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_742_364})

V_333 = CTVertex(name = 'V_333',
                 type = 'R2',
                 particles = [ P.W__plus__, P.Z, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_771_372})

V_334 = CTVertex(name = 'V_334',
                 type = 'R2',
                 particles = [ P.W__plus__, P.Z, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_789_376})

V_335 = CTVertex(name = 'V_335',
                 type = 'R2',
                 particles = [ P.W__plus__, P.Z, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_798_383})

V_336 = CTVertex(name = 'V_336',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_456_189})

V_337 = CTVertex(name = 'V_337',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_469_196})

V_338 = CTVertex(name = 'V_338',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_481_200})

V_339 = CTVertex(name = 'V_339',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_493_207})

V_340 = CTVertex(name = 'V_340',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_505_211})

V_341 = CTVertex(name = 'V_341',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_521_218})

V_342 = CTVertex(name = 'V_342',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_456_189})

V_343 = CTVertex(name = 'V_343',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_545_222})

V_344 = CTVertex(name = 'V_344',
                 type = 'R2',
                 particles = [ P.Z, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_560_227})

V_345 = CTVertex(name = 'V_345',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_435_173})

V_346 = CTVertex(name = 'V_346',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_435_173})

V_347 = CTVertex(name = 'V_347',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_435_173})

V_348 = CTVertex(name = 'V_348',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_430_169})

V_349 = CTVertex(name = 'V_349',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_430_169})

V_350 = CTVertex(name = 'V_350',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_430_169})

V_351 = CTVertex(name = 'V_351',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV7 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_437_174,(0,1,0):C.R2GC_433_172})

V_352 = CTVertex(name = 'V_352',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV7 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_437_174,(0,1,0):C.R2GC_433_172})

V_353 = CTVertex(name = 'V_353',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV7 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_437_174,(0,1,0):C.R2GC_433_172})

V_354 = CTVertex(name = 'V_354',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_432_171,(0,1,0):C.R2GC_433_172})

V_355 = CTVertex(name = 'V_355',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_432_171,(0,1,0):C.R2GC_433_172})

V_356 = CTVertex(name = 'V_356',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_432_171,(0,1,0):C.R2GC_433_172})

V_357 = CTVertex(name = 'V_357',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_358 = CTVertex(name = 'V_358',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_359 = CTVertex(name = 'V_359',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_360 = CTVertex(name = 'V_360',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_361 = CTVertex(name = 'V_361',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_362 = CTVertex(name = 'V_362',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_431_170})

V_363 = CTVertex(name = 'V_363',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_364 = CTVertex(name = 'V_364',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_365 = CTVertex(name = 'V_365',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_366 = CTVertex(name = 'V_366',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_367 = CTVertex(name = 'V_367',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_368 = CTVertex(name = 'V_368',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_663_315})

V_369 = CTVertex(name = 'V_369',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_429_168})

V_370 = CTVertex(name = 'V_370',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_429_168})

V_371 = CTVertex(name = 'V_371',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_569_228,(0,1,0):C.R2GC_429_168})

V_372 = CTVertex(name = 'V_372',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_429_168})

V_373 = CTVertex(name = 'V_373',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_429_168})

V_374 = CTVertex(name = 'V_374',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_429_168})

V_375 = CTVertex(name = 'V_375',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.R2GC_452_185,(0,1,0):C.R2GC_444_175})

V_376 = CTVertex(name = 'V_376',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.R2GC_452_185,(0,1,0):C.R2GC_444_175})

V_377 = CTVertex(name = 'V_377',
                 type = 'R2',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.R2GC_506_212,(0,1,0):C.R2GC_444_175})

V_378 = CTVertex(name = 'V_378',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.R2GC_506_212,(0,1,0):C.R2GC_444_175})

V_379 = CTVertex(name = 'V_379',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.R2GC_506_212,(0,1,0):C.R2GC_444_175})

V_380 = CTVertex(name = 'V_380',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.R2GC_506_212,(0,1,0):C.R2GC_444_175})

V_381 = CTVertex(name = 'V_381',
                 type = 'R2',
                 particles = [ P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.R2GC_546_223,(0,1,0):C.R2GC_444_175})

V_382 = CTVertex(name = 'V_382',
                 type = 'R2',
                 particles = [ P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.R2GC_546_223,(0,1,0):C.R2GC_444_175})

V_383 = CTVertex(name = 'V_383',
                 type = 'R2',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.R2GC_546_223,(0,1,0):C.R2GC_444_175})

V_384 = CTVertex(name = 'V_384',
                 type = 'R2',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV2, L.VV3 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                 couplings = {(0,2,1):C.R2GC_341_131,(0,0,2):C.R2GC_342_132,(0,1,0):C.R2GC_346_135})

V_385 = CTVertex(name = 'V_385',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVV1 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_349_138,(0,0,1):C.R2GC_349_139})

V_386 = CTVertex(name = 'V_386',
                 type = 'R2',
                 particles = [ P.g, P.g, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_343_133})

V_387 = CTVertex(name = 'V_387',
                 type = 'R2',
                 particles = [ P.a, P.a, P.g, P.g ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1000_3,(0,0,1):C.R2GC_1006_33})

V_388 = CTVertex(name = 'V_388',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.Z ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_350_140,(0,0,1):C.R2GC_350_141})

V_389 = CTVertex(name = 'V_389',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_353_146,(0,0,1):C.R2GC_353_147})

V_390 = CTVertex(name = 'V_390',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_354_148})

V_391 = CTVertex(name = 'V_391',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.g ],
                 color = [ 'd(2,3,4)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_348_136,(0,0,1):C.R2GC_348_137})

V_392 = CTVertex(name = 'V_392',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.Z ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(1,0,0):C.R2GC_352_144,(1,0,1):C.R2GC_352_145,(0,1,0):C.R2GC_351_142,(0,1,1):C.R2GC_351_143})

V_393 = CTVertex(name = 'V_393',
                 type = 'R2',
                 particles = [ P.g, P.g, P.H, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_344_134})

V_394 = CTVertex(name = 'V_394',
                 type = 'R2',
                 particles = [ P.g, P.g, P.G0, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_344_134})

V_395 = CTVertex(name = 'V_395',
                 type = 'R2',
                 particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_344_134})

V_396 = CTVertex(name = 'V_396',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d__tilde__, P.LQ1d, P.LQ1d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d] ], [ [P.g] ], [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1000_1,(1,0,5):C.R2GC_935_479,(1,0,1):C.R2GC_935_480,(1,0,4):C.R2GC_935_481,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1000_1,(0,0,5):C.R2GC_935_479,(0,0,1):C.R2GC_935_480,(0,0,4):C.R2GC_935_481})

V_397 = CTVertex(name = 'V_397',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ1dd] ], [ [P.a, P.g, P.LQ1d, P.LQ1dd] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ1dd] ], [ [P.g, P.LQ1d, P.LQ1dd] ], [ [P.g, P.LQ1d, P.LQ1dd, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ1dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_957_522,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_957_523,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_957_524,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_956_519,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_956_520,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_956_521})

V_398 = CTVertex(name = 'V_398',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd__tilde__, P.LQ1dd, P.LQ1dd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd] ], [ [P.g] ], [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_936_482,(1,0,5):C.R2GC_936_483,(1,0,1):C.R2GC_936_484,(1,0,4):C.R2GC_936_485,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_936_482,(0,0,5):C.R2GC_936_483,(0,0,1):C.R2GC_936_484,(0,0,4):C.R2GC_936_485})

V_399 = CTVertex(name = 'V_399',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ1d, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2uu] ], [ [P.g, P.LQ1d, P.LQ2uu] ], [ [P.g, P.LQ1d, P.LQ2uu, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1011_54,(1,0,8):C.R2GC_1017_86,(1,0,1):C.R2GC_1011_56,(1,0,7):C.R2GC_1017_87,(1,0,2):C.R2GC_1011_58,(1,0,6):C.R2GC_1017_88,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1003_23,(0,0,8):C.R2GC_1016_83,(0,0,1):C.R2GC_1010_50,(0,0,7):C.R2GC_1016_84,(0,0,2):C.R2GC_1010_52,(0,0,6):C.R2GC_1016_85})

V_400 = CTVertex(name = 'V_400',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ1dd, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2uu] ], [ [P.g, P.LQ1dd, P.LQ2uu] ], [ [P.g, P.LQ1dd, P.LQ2uu, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1019_95,(1,0,8):C.R2GC_979_570,(1,0,1):C.R2GC_1019_97,(1,0,7):C.R2GC_979_571,(1,0,2):C.R2GC_1019_99,(1,0,6):C.R2GC_979_572,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1018_89,(0,0,8):C.R2GC_978_567,(0,0,1):C.R2GC_1018_91,(0,0,7):C.R2GC_978_568,(0,0,2):C.R2GC_1018_93,(0,0,6):C.R2GC_978_569})

V_401 = CTVertex(name = 'V_401',
                 type = 'R2',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu__tilde__, P.LQ2uu, P.LQ2uu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_940_493,(1,0,5):C.R2GC_940_494,(1,0,1):C.R2GC_1012_63,(1,0,4):C.R2GC_940_495,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_940_493,(0,0,5):C.R2GC_940_494,(0,0,1):C.R2GC_1012_63,(0,0,4):C.R2GC_940_495})

V_402 = CTVertex(name = 'V_402',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ1d, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2u] ], [ [P.g, P.LQ1d, P.LQ2u] ], [ [P.g, P.LQ1d, P.LQ2u, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_1005_28,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_1005_29,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1005_30,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_1004_25,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_1004_26,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1004_27})

V_403 = CTVertex(name = 'V_403',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ1dd, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2u] ], [ [P.g, P.LQ1dd, P.LQ2u] ], [ [P.g, P.LQ1dd, P.LQ2u, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_977_564,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_977_565,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_977_566,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_976_561,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_976_562,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_976_563})

V_404 = CTVertex(name = 'V_404',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2u, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2u, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1013_65,(1,0,10):C.R2GC_1015_77,(1,0,11):C.R2GC_1015_78,(1,0,1):C.R2GC_1013_67,(1,0,8):C.R2GC_1015_79,(1,0,9):C.R2GC_1015_80,(1,0,2):C.R2GC_1013_69,(1,0,6):C.R2GC_1015_81,(1,0,7):C.R2GC_1015_82,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1007_41,(0,0,10):C.R2GC_1014_71,(0,0,11):C.R2GC_1014_72,(0,0,1):C.R2GC_1012_61,(0,0,8):C.R2GC_1014_73,(0,0,9):C.R2GC_1014_74,(0,0,2):C.R2GC_1012_63,(0,0,6):C.R2GC_1014_75,(0,0,7):C.R2GC_1014_76})

V_405 = CTVertex(name = 'V_405',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u__tilde__, P.LQ2u, P.LQ2u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1006_31,(1,0,5):C.R2GC_939_491,(1,0,1):C.R2GC_938_489,(1,0,4):C.R2GC_939_492,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1006_31,(0,0,5):C.R2GC_939_491,(0,0,1):C.R2GC_938_489,(0,0,4):C.R2GC_939_492})

V_406 = CTVertex(name = 'V_406',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ1d, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ1d, P.LQ2pu] ], [ [P.g, P.LQ1d, P.LQ2pu, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_961_540,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_961_541,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_961_542,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_960_537,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_960_538,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_960_539})

V_407 = CTVertex(name = 'V_407',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ1dd, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2pu] ], [ [P.g, P.LQ1dd, P.LQ2pu] ], [ [P.g, P.LQ1dd, P.LQ2pu, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_975_558,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_975_559,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_975_560,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_974_555,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_974_556,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_974_557})

V_408 = CTVertex(name = 'V_408',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2pu, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2pu, P.LQ2uu] ], [ [P.g, P.LQ2pu, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1013_65,(1,0,8):C.R2GC_1013_66,(1,0,1):C.R2GC_1013_67,(1,0,7):C.R2GC_1013_68,(1,0,2):C.R2GC_1013_69,(1,0,6):C.R2GC_1013_70,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1007_41,(0,0,8):C.R2GC_1012_60,(0,0,1):C.R2GC_1012_61,(0,0,7):C.R2GC_1012_62,(0,0,2):C.R2GC_1012_63,(0,0,6):C.R2GC_1012_64})

V_409 = CTVertex(name = 'V_409',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ2pu, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ2u] ], [ [P.g, P.LQ2pu, P.LQ2u] ], [ [P.g, P.LQ2pu, P.LQ2u, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_1003_20,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_1003_22,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_1003_24,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_1002_14,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_1002_16,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_1002_18})

V_410 = CTVertex(name = 'V_410',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu__tilde__, P.LQ2pu, P.LQ2pu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1006_31,(1,0,5):C.R2GC_938_488,(1,0,1):C.R2GC_938_489,(1,0,4):C.R2GC_938_490,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1006_31,(0,0,5):C.R2GC_938_488,(0,0,1):C.R2GC_938_489,(0,0,4):C.R2GC_938_490})

V_411 = CTVertex(name = 'V_411',
                 type = 'R2',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2u, P.W__plus__], [P.g, P.LQ2d, P.LQ2uu, P.W__plus__], [P.g, P.LQ2pu, P.LQ2u, P.W__plus__], [P.g, P.LQ2pu, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_1014_71,(1,0,1):C.R2GC_950_515,(1,0,0):C.R2GC_950_516,(0,0,2):C.R2GC_1015_77,(0,0,1):C.R2GC_949_513,(0,0,0):C.R2GC_949_514})

V_412 = CTVertex(name = 'V_412',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2u, P.W__plus__], [P.g, P.LQ2d, P.LQ2uu, P.W__plus__], [P.g, P.LQ2pu, P.LQ2u, P.W__plus__], [P.g, P.LQ2pu, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_1014_71,(1,0,1):C.R2GC_950_515,(1,0,0):C.R2GC_950_516,(0,0,2):C.R2GC_1015_77,(0,0,1):C.R2GC_949_513,(0,0,0):C.R2GC_949_514})

V_413 = CTVertex(name = 'V_413',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2d] ], [ [P.a, P.g, P.LQ1d, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2d] ], [ [P.g, P.LQ1d, P.LQ2d] ], [ [P.g, P.LQ1d, P.LQ2d, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_959_531,(1,0,8):C.R2GC_959_532,(1,0,1):C.R2GC_959_533,(1,0,7):C.R2GC_959_534,(1,0,2):C.R2GC_959_535,(1,0,6):C.R2GC_959_536,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_958_525,(0,0,8):C.R2GC_958_526,(0,0,1):C.R2GC_958_527,(0,0,7):C.R2GC_958_528,(0,0,2):C.R2GC_958_529,(0,0,6):C.R2GC_958_530})

V_414 = CTVertex(name = 'V_414',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2d] ], [ [P.a, P.g, P.LQ1dd, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2d] ], [ [P.g, P.LQ1dd, P.LQ2d] ], [ [P.g, P.LQ1dd, P.LQ2d, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_973_552,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_973_553,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_973_554,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_972_549,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_972_550,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_972_551})

V_415 = CTVertex(name = 'V_415',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2d, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2d, P.LQ2uu] ], [ [P.g, P.LQ2d, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1011_54,(1,0,8):C.R2GC_1011_55,(1,0,1):C.R2GC_1011_56,(1,0,7):C.R2GC_1011_57,(1,0,2):C.R2GC_1011_58,(1,0,6):C.R2GC_1011_59,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1003_23,(0,0,8):C.R2GC_1010_49,(0,0,1):C.R2GC_1010_50,(0,0,7):C.R2GC_1010_51,(0,0,2):C.R2GC_1010_52,(0,0,6):C.R2GC_1010_53})

V_416 = CTVertex(name = 'V_416',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ2d, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2u] ], [ [P.g, P.LQ2d, P.LQ2u] ], [ [P.g, P.LQ2d, P.LQ2u, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_1001_8,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_1001_10,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1001_12,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_1000_2,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_1000_4,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1000_6})

V_417 = CTVertex(name = 'V_417',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ2d, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu, P.W__plus__] ], [ [P.g, P.LQ2d, P.LQ2pu, P.Z] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,10):C.R2GC_1015_77,(1,0,11):C.R2GC_993_604,(1,0,1):C.R2GC_1001_9,(1,0,8):C.R2GC_1015_79,(1,0,9):C.R2GC_993_605,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1015_81,(1,0,7):C.R2GC_993_606,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,10):C.R2GC_1014_71,(0,0,11):C.R2GC_992_601,(0,0,1):C.R2GC_1000_3,(0,0,8):C.R2GC_1014_73,(0,0,9):C.R2GC_992_602,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1014_75,(0,0,7):C.R2GC_992_603})

V_418 = CTVertex(name = 'V_418',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d__tilde__, P.LQ2d, P.LQ2d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1000_1,(1,0,5):C.R2GC_937_486,(1,0,1):C.R2GC_935_480,(1,0,4):C.R2GC_937_487,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1000_1,(0,0,5):C.R2GC_937_486,(0,0,1):C.R2GC_935_480,(0,0,4):C.R2GC_937_487})

V_419 = CTVertex(name = 'V_419',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ1d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3u] ], [ [P.g, P.LQ1d, P.LQ3u] ], [ [P.g, P.LQ1d, P.LQ3u, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_1025_122,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_1025_123,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1025_124,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_1024_119,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_1024_120,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1024_121})

V_420 = CTVertex(name = 'V_420',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ1dd, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3u] ], [ [P.g, P.LQ1dd, P.LQ3u] ], [ [P.g, P.LQ1dd, P.LQ3u, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_985_587,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_985_588,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_985_589,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_984_584,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_984_585,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_984_586})

V_421 = CTVertex(name = 'V_421',
                 type = 'R2',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2uu, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3u] ], [ [P.g, P.LQ2uu, P.LQ3u] ], [ [P.g, P.LQ2uu, P.LQ3u, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1013_65,(1,0,8):C.R2GC_1021_104,(1,0,1):C.R2GC_1013_67,(1,0,7):C.R2GC_1021_105,(1,0,2):C.R2GC_1013_69,(1,0,6):C.R2GC_1021_106,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1007_41,(0,0,8):C.R2GC_1020_101,(0,0,1):C.R2GC_1012_61,(0,0,7):C.R2GC_1020_102,(0,0,2):C.R2GC_1012_63,(0,0,6):C.R2GC_1020_103})

V_422 = CTVertex(name = 'V_422',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2u, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3u] ], [ [P.g, P.LQ2u, P.LQ3u] ], [ [P.g, P.LQ2u, P.LQ3u, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_1009_46,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_1009_47,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_1009_48,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_1008_43,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_1008_44,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_1008_45})

V_423 = CTVertex(name = 'V_423',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2pu, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3u] ], [ [P.g, P.LQ2pu, P.LQ3u] ], [ [P.g, P.LQ2pu, P.LQ3u, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_989_593,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_989_594,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_989_595,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_988_590,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_988_591,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_988_592})

V_424 = CTVertex(name = 'V_424',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3u] ], [ [P.g, P.LQ2d, P.LQ3u] ], [ [P.g, P.LQ2d, P.LQ3u, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_991_598,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_991_599,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_991_600,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_937_486,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_990_596,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_990_597})

V_425 = CTVertex(name = 'V_425',
                 type = 'R2',
                 particles = [ P.LQ3u__tilde__, P.LQ3u__tilde__, P.LQ3u, P.LQ3u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1006_31,(1,0,5):C.R2GC_943_498,(1,0,1):C.R2GC_938_489,(1,0,4):C.R2GC_943_499,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1006_31,(0,0,5):C.R2GC_943_498,(0,0,1):C.R2GC_938_489,(0,0,4):C.R2GC_943_499})

V_426 = CTVertex(name = 'V_426',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2uu, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3u, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_948_510,(1,0,1):C.R2GC_948_511,(1,0,0):C.R2GC_948_512,(0,0,2):C.R2GC_947_507,(0,0,1):C.R2GC_947_508,(0,0,0):C.R2GC_947_509})

V_427 = CTVertex(name = 'V_427',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3u, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_948_510,(1,0,1):C.R2GC_948_511,(1,0,0):C.R2GC_948_512,(0,0,2):C.R2GC_947_507,(0,0,1):C.R2GC_947_508,(0,0,0):C.R2GC_947_509})

V_428 = CTVertex(name = 'V_428',
                 type = 'R2',
                 particles = [ P.LQ2u, P.LQ2uu__tilde__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3u, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_948_510,(1,0,1):C.R2GC_948_511,(1,0,0):C.R2GC_948_512,(0,0,2):C.R2GC_947_507,(0,0,1):C.R2GC_947_508,(0,0,0):C.R2GC_947_509})

V_429 = CTVertex(name = 'V_429',
                 type = 'R2',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3u, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_948_510,(1,0,1):C.R2GC_948_511,(1,0,0):C.R2GC_948_512,(0,0,2):C.R2GC_947_507,(0,0,1):C.R2GC_947_508,(0,0,0):C.R2GC_947_509})

V_430 = CTVertex(name = 'V_430',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ1d, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3d] ], [ [P.g, P.LQ1d, P.LQ3d] ], [ [P.g, P.LQ1d, P.LQ3d, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_959_531,(1,0,8):C.R2GC_967_546,(1,0,1):C.R2GC_959_533,(1,0,7):C.R2GC_967_547,(1,0,2):C.R2GC_959_535,(1,0,6):C.R2GC_967_548,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_958_525,(0,0,8):C.R2GC_966_543,(0,0,1):C.R2GC_958_527,(0,0,7):C.R2GC_966_544,(0,0,2):C.R2GC_958_529,(0,0,6):C.R2GC_966_545})

V_431 = CTVertex(name = 'V_431',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ1dd, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3d] ], [ [P.g, P.LQ1dd, P.LQ3d] ], [ [P.g, P.LQ1dd, P.LQ3d, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_957_522,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_957_523,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_957_524,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_956_519,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_956_520,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_956_521})

V_432 = CTVertex(name = 'V_432',
                 type = 'R2',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2uu, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3d] ], [ [P.g, P.LQ2uu, P.LQ3d] ], [ [P.g, P.LQ2uu, P.LQ3d, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1011_54,(1,0,8):C.R2GC_1017_86,(1,0,1):C.R2GC_1011_56,(1,0,7):C.R2GC_1017_87,(1,0,2):C.R2GC_1011_58,(1,0,6):C.R2GC_1017_88,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1003_23,(0,0,8):C.R2GC_1016_83,(0,0,1):C.R2GC_1010_50,(0,0,7):C.R2GC_1016_84,(0,0,2):C.R2GC_1010_52,(0,0,6):C.R2GC_1016_85})

V_433 = CTVertex(name = 'V_433',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2u, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3d] ], [ [P.g, P.LQ2u, P.LQ3d] ], [ [P.g, P.LQ2u, P.LQ3d, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_1005_28,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_1005_29,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1005_30,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_1004_25,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_1004_26,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1004_27})

V_434 = CTVertex(name = 'V_434',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2pu, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3d] ], [ [P.g, P.LQ2pu, P.LQ3d] ], [ [P.g, P.LQ2pu, P.LQ3d, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,8):C.R2GC_961_540,(1,0,1):C.R2GC_1001_9,(1,0,7):C.R2GC_961_541,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_961_542,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,8):C.R2GC_960_537,(0,0,1):C.R2GC_1000_3,(0,0,7):C.R2GC_960_538,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_960_539})

V_435 = CTVertex(name = 'V_435',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2d, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3d] ], [ [P.g, P.LQ2d, P.LQ3d] ], [ [P.g, P.LQ2d, P.LQ3d, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_959_531,(1,0,8):C.R2GC_959_532,(1,0,1):C.R2GC_959_533,(1,0,7):C.R2GC_959_534,(1,0,2):C.R2GC_959_535,(1,0,6):C.R2GC_959_536,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_958_525,(0,0,8):C.R2GC_958_526,(0,0,1):C.R2GC_958_527,(0,0,7):C.R2GC_958_528,(0,0,2):C.R2GC_958_529,(0,0,6):C.R2GC_958_530})

V_436 = CTVertex(name = 'V_436',
                 type = 'R2',
                 particles = [ P.LQ3d__tilde__, P.LQ3d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ3d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3d], [P.g, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3u, P.Z] ], [ [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1001_7,(1,0,10):C.R2GC_1023_113,(1,0,11):C.R2GC_1025_122,(1,0,1):C.R2GC_1001_9,(1,0,8):C.R2GC_1023_115,(1,0,9):C.R2GC_1025_123,(1,0,2):C.R2GC_1001_11,(1,0,6):C.R2GC_1023_117,(1,0,7):C.R2GC_1025_124,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1000_1,(0,0,10):C.R2GC_1022_107,(0,0,11):C.R2GC_1024_119,(0,0,1):C.R2GC_1000_3,(0,0,8):C.R2GC_1022_109,(0,0,9):C.R2GC_1024_120,(0,0,2):C.R2GC_1000_5,(0,0,6):C.R2GC_1022_111,(0,0,7):C.R2GC_1024_121})

V_437 = CTVertex(name = 'V_437',
                 type = 'R2',
                 particles = [ P.LQ3d__tilde__, P.LQ3d__tilde__, P.LQ3d, P.LQ3d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_1000_1,(1,0,5):C.R2GC_935_479,(1,0,1):C.R2GC_935_480,(1,0,4):C.R2GC_935_481,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_1000_1,(0,0,5):C.R2GC_935_479,(0,0,1):C.R2GC_935_480,(0,0,4):C.R2GC_935_481})

V_438 = CTVertex(name = 'V_438',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2uu, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3dd, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_946_504,(1,0,1):C.R2GC_946_505,(1,0,0):C.R2GC_946_506,(0,0,2):C.R2GC_945_501,(0,0,1):C.R2GC_945_502,(0,0,0):C.R2GC_945_503})

V_439 = CTVertex(name = 'V_439',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3dd, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_946_504,(1,0,1):C.R2GC_946_505,(1,0,0):C.R2GC_946_506,(0,0,2):C.R2GC_945_501,(0,0,1):C.R2GC_945_502,(0,0,0):C.R2GC_945_503})

V_440 = CTVertex(name = 'V_440',
                 type = 'R2',
                 particles = [ P.LQ3d__tilde__, P.LQ3d__tilde__, P.LQ3dd, P.LQ3u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__], [P.g, P.LQ3d, P.LQ3u, P.W__plus__], [P.g, P.LQ3dd, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,3):C.R2GC_955_517,(1,0,2):C.R2GC_955_518,(1,0,0):C.R2GC_745_365,(1,0,1):C.R2GC_1023_117,(0,0,3):C.R2GC_955_517,(0,0,2):C.R2GC_955_518,(0,0,0):C.R2GC_745_365,(0,0,1):C.R2GC_1023_117})

V_441 = CTVertex(name = 'V_441',
                 type = 'R2',
                 particles = [ P.LQ2u, P.LQ2uu__tilde__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3dd, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_946_504,(1,0,1):C.R2GC_946_505,(1,0,0):C.R2GC_946_506,(0,0,2):C.R2GC_945_501,(0,0,1):C.R2GC_945_502,(0,0,0):C.R2GC_945_503})

V_442 = CTVertex(name = 'V_442',
                 type = 'R2',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3dd, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.R2GC_946_504,(1,0,1):C.R2GC_946_505,(1,0,0):C.R2GC_946_506,(0,0,2):C.R2GC_945_501,(0,0,1):C.R2GC_945_502,(0,0,0):C.R2GC_945_503})

V_443 = CTVertex(name = 'V_443',
                 type = 'R2',
                 particles = [ P.LQ3d, P.LQ3d, P.LQ3dd__tilde__, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__], [P.g, P.LQ3d, P.LQ3u, P.W__plus__], [P.g, P.LQ3dd, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,3):C.R2GC_955_517,(1,0,2):C.R2GC_955_518,(1,0,0):C.R2GC_745_365,(1,0,1):C.R2GC_1023_117,(0,0,3):C.R2GC_955_517,(0,0,2):C.R2GC_955_518,(0,0,0):C.R2GC_745_365,(0,0,1):C.R2GC_1023_117})

V_444 = CTVertex(name = 'V_444',
                 type = 'R2',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ1d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ1d, P.LQ3dd] ], [ [P.g, P.LQ1d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_1023_114,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_1023_116,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_1023_118,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_1022_108,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_1022_110,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_1022_112})

V_445 = CTVertex(name = 'V_445',
                 type = 'R2',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ1dd, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3dd] ], [ [P.g, P.LQ1dd, P.LQ3dd] ], [ [P.g, P.LQ1dd, P.LQ3dd, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_983_579,(1,0,8):C.R2GC_983_580,(1,0,1):C.R2GC_983_581,(1,0,7):C.R2GC_983_582,(1,0,2):C.R2GC_1018_89,(1,0,6):C.R2GC_983_583,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_982_573,(0,0,8):C.R2GC_982_574,(0,0,1):C.R2GC_982_575,(0,0,7):C.R2GC_982_576,(0,0,2):C.R2GC_982_577,(0,0,6):C.R2GC_982_578})

V_446 = CTVertex(name = 'V_446',
                 type = 'R2',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2uu, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2uu, P.LQ3dd] ], [ [P.g, P.LQ2uu, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1019_95,(1,0,8):C.R2GC_1019_96,(1,0,1):C.R2GC_1019_97,(1,0,7):C.R2GC_1019_98,(1,0,2):C.R2GC_1019_99,(1,0,6):C.R2GC_1019_100,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1018_89,(0,0,8):C.R2GC_1018_90,(0,0,1):C.R2GC_1018_91,(0,0,7):C.R2GC_1018_92,(0,0,2):C.R2GC_1018_93,(0,0,6):C.R2GC_1018_94})

V_447 = CTVertex(name = 'V_447',
                 type = 'R2',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2u, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2u, P.LQ3dd] ], [ [P.g, P.LQ2u, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_1007_38,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_1007_40,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_1007_42,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_1006_32,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_1006_34,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_1006_36})

V_448 = CTVertex(name = 'V_448',
                 type = 'R2',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2pu, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2pu, P.LQ3dd] ], [ [P.g, P.LQ2pu, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_997_609,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_997_610,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_997_611,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_938_488,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_996_607,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_996_608})

V_449 = CTVertex(name = 'V_449',
                 type = 'R2',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2d, P.LQ3dd] ], [ [P.g, P.LQ2d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,8):C.R2GC_989_593,(1,0,1):C.R2GC_1003_21,(1,0,7):C.R2GC_989_594,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_989_595,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,8):C.R2GC_988_590,(0,0,1):C.R2GC_1002_15,(0,0,7):C.R2GC_988_591,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_988_592})

V_450 = CTVertex(name = 'V_450',
                 type = 'R2',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3dd], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ3dd, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3dd], [P.g, P.LQ3u] ], [ [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3dd, P.LQ3u, P.Z] ], [ [P.g, P.LQ3dd, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1007_37,(1,0,8):C.R2GC_1027_128,(1,0,1):C.R2GC_1007_39,(1,0,7):C.R2GC_1027_129,(1,0,2):C.R2GC_1007_41,(1,0,6):C.R2GC_1027_130,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1006_31,(0,0,8):C.R2GC_1026_125,(0,0,1):C.R2GC_1006_33,(0,0,7):C.R2GC_1026_126,(0,0,2):C.R2GC_1006_35,(0,0,6):C.R2GC_1026_127})

V_451 = CTVertex(name = 'V_451',
                 type = 'R2',
                 particles = [ P.LQ3d__tilde__, P.LQ3d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ3d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ3d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ3d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.R2GC_707_361,(1,0,4):C.R2GC_707_362,(1,0,5):C.R2GC_707_363,(1,0,0):C.R2GC_1003_19,(1,0,10):C.R2GC_1023_113,(1,0,11):C.R2GC_1023_114,(1,0,1):C.R2GC_1003_21,(1,0,8):C.R2GC_1023_115,(1,0,9):C.R2GC_1023_116,(1,0,2):C.R2GC_1003_23,(1,0,6):C.R2GC_1023_117,(1,0,7):C.R2GC_1023_118,(0,0,3):C.R2GC_706_358,(0,0,4):C.R2GC_706_359,(0,0,5):C.R2GC_706_360,(0,0,0):C.R2GC_1002_13,(0,0,10):C.R2GC_1022_107,(0,0,11):C.R2GC_1022_108,(0,0,1):C.R2GC_1002_15,(0,0,8):C.R2GC_1022_109,(0,0,9):C.R2GC_1022_110,(0,0,2):C.R2GC_1002_17,(0,0,6):C.R2GC_1022_111,(0,0,7):C.R2GC_1022_112})

V_452 = CTVertex(name = 'V_452',
                 type = 'R2',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd__tilde__, P.LQ3dd, P.LQ3dd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.R2GC_449_180,(1,0,3):C.R2GC_449_181,(1,0,0):C.R2GC_936_482,(1,0,5):C.R2GC_942_496,(1,0,1):C.R2GC_936_484,(1,0,4):C.R2GC_942_497,(0,0,2):C.R2GC_449_180,(0,0,3):C.R2GC_449_181,(0,0,0):C.R2GC_936_482,(0,0,5):C.R2GC_942_496,(0,0,1):C.R2GC_936_484,(0,0,4):C.R2GC_942_497})

V_453 = CTVertex(name = 'V_453',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_422_252,(0,0,1):C.UVGC_422_253,(0,0,2):C.UVGC_422_254,(0,0,3):C.UVGC_422_255,(0,0,4):C.UVGC_422_256,(0,0,5):C.UVGC_422_257,(0,0,6):C.UVGC_422_258,(0,0,7):C.UVGC_422_259,(0,0,8):C.UVGC_422_260,(0,0,9):C.UVGC_422_261,(0,0,10):C.UVGC_422_262,(0,0,11):C.UVGC_422_263,(0,0,12):C.UVGC_422_264,(0,0,13):C.UVGC_422_265,(0,0,14):C.UVGC_422_266,(0,0,15):C.UVGC_422_267,(0,0,16):C.UVGC_422_268})

V_454 = CTVertex(name = 'V_454',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.LQ1d], [P.LQ1dd], [P.LQ2d], [P.LQ2pu], [P.LQ2u], [P.LQ2uu], [P.LQ3d], [P.LQ3dd], [P.LQ3u], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ1d], [P.LQ1dd], [P.LQ2d], [P.LQ2pu], [P.LQ2u], [P.LQ2uu], [P.LQ3d], [P.LQ3dd], [P.LQ3u] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,1,5):C.UVGC_414_179,(2,1,6):C.UVGC_414_178,(0,1,5):C.UVGC_414_179,(0,1,6):C.UVGC_414_178,(4,1,5):C.UVGC_413_176,(4,1,6):C.UVGC_413_177,(3,1,5):C.UVGC_413_176,(3,1,6):C.UVGC_413_177,(8,1,5):C.UVGC_414_178,(8,1,6):C.UVGC_414_179,(7,1,0):C.UVGC_425_272,(7,1,3):C.UVGC_425_273,(7,1,4):C.UVGC_425_274,(7,1,5):C.UVGC_420_221,(7,1,6):C.UVGC_426_289,(7,1,7):C.UVGC_425_277,(7,1,8):C.UVGC_425_278,(7,1,10):C.UVGC_425_279,(7,1,11):C.UVGC_425_280,(7,1,12):C.UVGC_425_281,(7,1,13):C.UVGC_425_282,(7,1,14):C.UVGC_425_283,(7,1,15):C.UVGC_425_284,(7,1,16):C.UVGC_425_285,(7,1,17):C.UVGC_425_286,(7,1,18):C.UVGC_425_287,(7,1,19):C.UVGC_425_288,(6,1,0):C.UVGC_425_272,(6,1,3):C.UVGC_425_273,(6,1,4):C.UVGC_425_274,(6,1,5):C.UVGC_425_275,(6,1,6):C.UVGC_425_276,(6,1,7):C.UVGC_425_277,(6,1,8):C.UVGC_425_278,(6,1,10):C.UVGC_425_279,(6,1,11):C.UVGC_425_280,(6,1,12):C.UVGC_425_281,(6,1,13):C.UVGC_425_282,(6,1,14):C.UVGC_425_283,(6,1,15):C.UVGC_425_284,(6,1,16):C.UVGC_425_285,(6,1,17):C.UVGC_425_286,(6,1,18):C.UVGC_425_287,(6,1,19):C.UVGC_425_288,(5,1,5):C.UVGC_413_176,(5,1,6):C.UVGC_413_177,(1,1,5):C.UVGC_413_176,(1,1,6):C.UVGC_413_177,(11,0,5):C.UVGC_417_182,(11,0,6):C.UVGC_417_183,(10,0,5):C.UVGC_417_182,(10,0,6):C.UVGC_417_183,(9,0,5):C.UVGC_416_180,(9,0,6):C.UVGC_416_181,(2,2,5):C.UVGC_414_179,(2,2,6):C.UVGC_414_178,(0,2,5):C.UVGC_414_179,(0,2,6):C.UVGC_414_178,(6,2,0):C.UVGC_427_290,(6,2,3):C.UVGC_427_291,(6,2,4):C.UVGC_427_292,(6,2,5):C.UVGC_428_306,(6,2,6):C.UVGC_428_307,(6,2,7):C.UVGC_428_308,(6,2,8):C.UVGC_428_309,(6,2,10):C.UVGC_428_310,(6,2,11):C.UVGC_428_311,(6,2,12):C.UVGC_428_312,(6,2,13):C.UVGC_428_313,(6,2,14):C.UVGC_428_314,(6,2,15):C.UVGC_428_315,(6,2,16):C.UVGC_428_316,(6,2,17):C.UVGC_427_303,(6,2,18):C.UVGC_427_304,(6,2,19):C.UVGC_427_305,(4,2,5):C.UVGC_413_176,(4,2,6):C.UVGC_413_177,(3,2,5):C.UVGC_413_176,(3,2,6):C.UVGC_413_177,(8,2,0):C.UVGC_420_218,(8,2,3):C.UVGC_420_219,(8,2,4):C.UVGC_420_220,(8,2,5):C.UVGC_420_221,(8,2,6):C.UVGC_420_222,(8,2,7):C.UVGC_420_223,(8,2,8):C.UVGC_420_224,(8,2,10):C.UVGC_420_225,(8,2,11):C.UVGC_420_226,(8,2,12):C.UVGC_420_227,(8,2,13):C.UVGC_420_228,(8,2,14):C.UVGC_420_229,(8,2,15):C.UVGC_420_230,(8,2,16):C.UVGC_420_231,(8,2,17):C.UVGC_420_232,(8,2,18):C.UVGC_420_233,(8,2,19):C.UVGC_420_234,(7,2,1):C.UVGC_423_269,(7,2,5):C.UVGC_414_178,(7,2,6):C.UVGC_424_271,(5,2,5):C.UVGC_413_176,(5,2,6):C.UVGC_413_177,(1,2,5):C.UVGC_413_176,(1,2,6):C.UVGC_413_177,(2,3,5):C.UVGC_414_179,(2,3,6):C.UVGC_414_178,(0,3,5):C.UVGC_414_179,(0,3,6):C.UVGC_414_178,(4,3,5):C.UVGC_413_176,(4,3,6):C.UVGC_413_177,(3,3,5):C.UVGC_413_176,(3,3,6):C.UVGC_413_177,(8,3,0):C.UVGC_419_201,(8,3,3):C.UVGC_419_202,(8,3,4):C.UVGC_419_203,(8,3,5):C.UVGC_419_204,(8,3,6):C.UVGC_419_205,(8,3,7):C.UVGC_419_206,(8,3,8):C.UVGC_419_207,(8,3,10):C.UVGC_419_208,(8,3,11):C.UVGC_419_209,(8,3,12):C.UVGC_419_210,(8,3,13):C.UVGC_419_211,(8,3,14):C.UVGC_419_212,(8,3,15):C.UVGC_419_213,(8,3,16):C.UVGC_419_214,(8,3,17):C.UVGC_419_215,(8,3,18):C.UVGC_419_216,(8,3,19):C.UVGC_419_217,(6,3,2):C.UVGC_423_269,(6,3,6):C.UVGC_416_180,(6,3,9):C.UVGC_423_270,(7,3,0):C.UVGC_427_290,(7,3,3):C.UVGC_427_291,(7,3,4):C.UVGC_427_292,(7,3,5):C.UVGC_419_204,(7,3,6):C.UVGC_427_293,(7,3,7):C.UVGC_427_294,(7,3,8):C.UVGC_427_295,(7,3,10):C.UVGC_427_296,(7,3,11):C.UVGC_427_297,(7,3,12):C.UVGC_427_298,(7,3,13):C.UVGC_427_299,(7,3,14):C.UVGC_427_300,(7,3,15):C.UVGC_427_301,(7,3,16):C.UVGC_427_302,(7,3,17):C.UVGC_427_303,(7,3,18):C.UVGC_427_304,(7,3,19):C.UVGC_427_305,(5,3,5):C.UVGC_413_176,(5,3,6):C.UVGC_413_177,(1,3,5):C.UVGC_413_176,(1,3,6):C.UVGC_413_177})

V_455 = CTVertex(name = 'V_455',
                 type = 'UV',
                 particles = [ P.a, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_445_326})

V_456 = CTVertex(name = 'V_456',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_446_327})

V_457 = CTVertex(name = 'V_457',
                 type = 'UV',
                 particles = [ P.a, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_458_409})

V_458 = CTVertex(name = 'V_458',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_459_410})

V_459 = CTVertex(name = 'V_459',
                 type = 'UV',
                 particles = [ P.a, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_471_455})

V_460 = CTVertex(name = 'V_460',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_472_456})

V_461 = CTVertex(name = 'V_461',
                 type = 'UV',
                 particles = [ P.a, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_483_482})

V_462 = CTVertex(name = 'V_462',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_484_483})

V_463 = CTVertex(name = 'V_463',
                 type = 'UV',
                 particles = [ P.a, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_495_526})

V_464 = CTVertex(name = 'V_464',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_496_527})

V_465 = CTVertex(name = 'V_465',
                 type = 'UV',
                 particles = [ P.a, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_510_557})

V_466 = CTVertex(name = 'V_466',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_511_558})

V_467 = CTVertex(name = 'V_467',
                 type = 'UV',
                 particles = [ P.a, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_523_602})

V_468 = CTVertex(name = 'V_468',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_524_603})

V_469 = CTVertex(name = 'V_469',
                 type = 'UV',
                 particles = [ P.a, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_535_612})

V_470 = CTVertex(name = 'V_470',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_536_613})

V_471 = CTVertex(name = 'V_471',
                 type = 'UV',
                 particles = [ P.a, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_549_642})

V_472 = CTVertex(name = 'V_472',
                 type = 'UV',
                 particles = [ P.a, P.a, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_550_643})

V_473 = CTVertex(name = 'V_473',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_848_1285,(0,0,2):C.UVGC_848_1286,(0,0,1):C.UVGC_848_1287})

V_474 = CTVertex(name = 'V_474',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_573_673})

V_475 = CTVertex(name = 'V_475',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_572_672})

V_476 = CTVertex(name = 'V_476',
                 type = 'UV',
                 particles = [ P.t, P.e__minus__, P.LQ1d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_859_1318,(0,0,2):C.UVGC_859_1319,(0,0,1):C.UVGC_859_1320,(0,1,0):C.UVGC_856_1309,(0,1,2):C.UVGC_856_1310,(0,1,1):C.UVGC_856_1311})

V_477 = CTVertex(name = 'V_477',
                 type = 'UV',
                 particles = [ P.u, P.e__minus__, P.LQ1d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_902_1428,(0,0,2):C.UVGC_902_1429,(0,0,1):C.UVGC_902_1430,(0,1,0):C.UVGC_899_1419,(0,1,2):C.UVGC_899_1420,(0,1,1):C.UVGC_899_1421})

V_478 = CTVertex(name = 'V_478',
                 type = 'UV',
                 particles = [ P.t, P.mu__minus__, P.LQ1d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_860_1321,(0,0,2):C.UVGC_860_1322,(0,0,1):C.UVGC_860_1323,(0,1,0):C.UVGC_857_1312,(0,1,2):C.UVGC_857_1313,(0,1,1):C.UVGC_857_1314})

V_479 = CTVertex(name = 'V_479',
                 type = 'UV',
                 particles = [ P.u, P.mu__minus__, P.LQ1d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_903_1431,(0,0,2):C.UVGC_903_1432,(0,0,1):C.UVGC_903_1433,(0,1,0):C.UVGC_900_1422,(0,1,2):C.UVGC_900_1423,(0,1,1):C.UVGC_900_1424})

V_480 = CTVertex(name = 'V_480',
                 type = 'UV',
                 particles = [ P.u, P.ta__minus__, P.LQ1d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_904_1434,(0,0,2):C.UVGC_904_1435,(0,0,1):C.UVGC_904_1436,(0,1,0):C.UVGC_901_1425,(0,1,2):C.UVGC_901_1426,(0,1,1):C.UVGC_901_1427})

V_481 = CTVertex(name = 'V_481',
                 type = 'UV',
                 particles = [ P.e__minus__, P.c, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_630_828,(0,0,2):C.UVGC_630_829,(0,0,1):C.UVGC_630_830,(0,1,0):C.UVGC_627_819,(0,1,2):C.UVGC_627_820,(0,1,1):C.UVGC_627_821})

V_482 = CTVertex(name = 'V_482',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.c, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_631_831,(0,0,2):C.UVGC_631_832,(0,0,1):C.UVGC_631_833,(0,1,0):C.UVGC_628_822,(0,1,2):C.UVGC_628_823,(0,1,1):C.UVGC_628_824})

V_483 = CTVertex(name = 'V_483',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.c, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_632_834,(0,0,2):C.UVGC_632_835,(0,0,1):C.UVGC_632_836,(0,1,0):C.UVGC_629_825,(0,1,2):C.UVGC_629_826,(0,1,1):C.UVGC_629_827})

V_484 = CTVertex(name = 'V_484',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.t, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_861_1324,(0,0,2):C.UVGC_861_1325,(0,0,1):C.UVGC_861_1326,(0,1,0):C.UVGC_858_1315,(0,1,2):C.UVGC_858_1316,(0,1,1):C.UVGC_858_1317})

V_485 = CTVertex(name = 'V_485',
                 type = 'UV',
                 particles = [ P.s, P.e__minus__, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_814_1202,(0,0,2):C.UVGC_814_1203,(0,0,1):C.UVGC_814_1204})

V_486 = CTVertex(name = 'V_486',
                 type = 'UV',
                 particles = [ P.s, P.mu__minus__, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_815_1205,(0,0,2):C.UVGC_815_1206,(0,0,1):C.UVGC_815_1207})

V_487 = CTVertex(name = 'V_487',
                 type = 'UV',
                 particles = [ P.e__minus__, P.b, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_588_702,(0,0,2):C.UVGC_588_703,(0,0,1):C.UVGC_588_704})

V_488 = CTVertex(name = 'V_488',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.b, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_589_705,(0,0,2):C.UVGC_589_706,(0,0,1):C.UVGC_589_707})

V_489 = CTVertex(name = 'V_489',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.b, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_590_708,(0,0,2):C.UVGC_590_709,(0,0,1):C.UVGC_590_710})

V_490 = CTVertex(name = 'V_490',
                 type = 'UV',
                 particles = [ P.e__minus__, P.d, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_673_957,(0,0,2):C.UVGC_673_958,(0,0,1):C.UVGC_673_959})

V_491 = CTVertex(name = 'V_491',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.d, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_674_960,(0,0,2):C.UVGC_674_961,(0,0,1):C.UVGC_674_962})

V_492 = CTVertex(name = 'V_492',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.d, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_675_963,(0,0,2):C.UVGC_675_964,(0,0,1):C.UVGC_675_965})

V_493 = CTVertex(name = 'V_493',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.s, P.LQ1dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_816_1208,(0,0,2):C.UVGC_816_1209,(0,0,1):C.UVGC_816_1210})

V_494 = CTVertex(name = 'V_494',
                 type = 'UV',
                 particles = [ P.e__plus__, P.u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_914_1461,(0,0,2):C.UVGC_914_1462,(0,0,1):C.UVGC_914_1463,(0,1,0):C.UVGC_917_1470,(0,1,2):C.UVGC_917_1471,(0,1,1):C.UVGC_691_1013})

V_495 = CTVertex(name = 'V_495',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_691_1011,(0,0,2):C.UVGC_691_1012,(0,0,1):C.UVGC_691_1013})

V_496 = CTVertex(name = 'V_496',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_915_1464,(0,0,2):C.UVGC_915_1465,(0,0,1):C.UVGC_915_1466,(0,1,0):C.UVGC_918_1472,(0,1,2):C.UVGC_918_1473,(0,1,1):C.UVGC_692_1016})

V_497 = CTVertex(name = 'V_497',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.d, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_692_1014,(0,0,2):C.UVGC_692_1015,(0,0,1):C.UVGC_692_1016})

V_498 = CTVertex(name = 'V_498',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_916_1467,(0,0,2):C.UVGC_916_1468,(0,0,1):C.UVGC_916_1469,(0,1,0):C.UVGC_919_1474,(0,1,2):C.UVGC_919_1475,(0,1,1):C.UVGC_693_1019})

V_499 = CTVertex(name = 'V_499',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.d, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_693_1017,(0,0,2):C.UVGC_693_1018,(0,0,1):C.UVGC_693_1019})

V_500 = CTVertex(name = 'V_500',
                 type = 'UV',
                 particles = [ P.e__plus__, P.c, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_642_864,(0,0,2):C.UVGC_642_865,(0,0,1):C.UVGC_642_866,(0,1,0):C.UVGC_645_873,(0,1,2):C.UVGC_645_874,(0,1,1):C.UVGC_645_875})

V_501 = CTVertex(name = 'V_501',
                 type = 'UV',
                 particles = [ P.e__plus__, P.s, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_832_1253,(0,0,2):C.UVGC_832_1254,(0,0,1):C.UVGC_645_875})

V_502 = CTVertex(name = 'V_502',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_643_867,(0,0,2):C.UVGC_643_868,(0,0,1):C.UVGC_643_869,(0,1,0):C.UVGC_646_876,(0,1,2):C.UVGC_646_877,(0,1,1):C.UVGC_646_878})

V_503 = CTVertex(name = 'V_503',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.s, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_833_1255,(0,0,2):C.UVGC_833_1256,(0,0,1):C.UVGC_646_878})

V_504 = CTVertex(name = 'V_504',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.c, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_644_870,(0,0,2):C.UVGC_644_871,(0,0,1):C.UVGC_644_872,(0,1,0):C.UVGC_647_879,(0,1,2):C.UVGC_647_880,(0,1,1):C.UVGC_647_881})

V_505 = CTVertex(name = 'V_505',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.s, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_834_1257,(0,0,2):C.UVGC_834_1258,(0,0,1):C.UVGC_647_881})

V_506 = CTVertex(name = 'V_506',
                 type = 'UV',
                 particles = [ P.e__plus__, P.t, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_871_1351,(0,0,2):C.UVGC_871_1352,(0,0,1):C.UVGC_871_1353,(0,1,0):C.UVGC_874_1360,(0,1,2):C.UVGC_874_1361,(0,1,1):C.UVGC_606_758})

V_507 = CTVertex(name = 'V_507',
                 type = 'UV',
                 particles = [ P.e__plus__, P.b, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_606_756,(0,0,2):C.UVGC_606_757,(0,0,1):C.UVGC_606_758})

V_508 = CTVertex(name = 'V_508',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.t, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_872_1354,(0,0,2):C.UVGC_872_1355,(0,0,1):C.UVGC_872_1356,(0,1,0):C.UVGC_875_1362,(0,1,2):C.UVGC_875_1363,(0,1,1):C.UVGC_607_761})

V_509 = CTVertex(name = 'V_509',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.b, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_607_759,(0,0,2):C.UVGC_607_760,(0,0,1):C.UVGC_607_761})

V_510 = CTVertex(name = 'V_510',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_873_1357,(0,0,2):C.UVGC_873_1358,(0,0,1):C.UVGC_873_1359,(0,1,0):C.UVGC_876_1364,(0,1,2):C.UVGC_876_1365,(0,1,1):C.UVGC_608_764})

V_511 = CTVertex(name = 'V_511',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_608_762,(0,0,2):C.UVGC_608_763,(0,0,1):C.UVGC_608_764})

V_512 = CTVertex(name = 'V_512',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.e__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_639_855,(0,0,2):C.UVGC_639_856,(0,0,1):C.UVGC_639_857,(0,1,0):C.UVGC_648_882,(0,1,2):C.UVGC_648_883,(0,1,1):C.UVGC_648_884})

V_513 = CTVertex(name = 'V_513',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.e__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_868_1345,(0,0,2):C.UVGC_868_1346,(0,0,1):C.UVGC_603_749,(0,1,0):C.UVGC_877_1366,(0,1,2):C.UVGC_877_1367,(0,1,1):C.UVGC_877_1368})

V_514 = CTVertex(name = 'V_514',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_911_1455,(0,0,2):C.UVGC_911_1456,(0,0,1):C.UVGC_688_1004,(0,1,0):C.UVGC_920_1476,(0,1,2):C.UVGC_920_1477,(0,1,1):C.UVGC_920_1478})

V_515 = CTVertex(name = 'V_515',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.mu__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_640_858,(0,0,2):C.UVGC_640_859,(0,0,1):C.UVGC_640_860,(0,1,0):C.UVGC_649_885,(0,1,2):C.UVGC_649_886,(0,1,1):C.UVGC_649_887})

V_516 = CTVertex(name = 'V_516',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.mu__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_869_1347,(0,0,2):C.UVGC_869_1348,(0,0,1):C.UVGC_604_752,(0,1,0):C.UVGC_878_1369,(0,1,2):C.UVGC_878_1370,(0,1,1):C.UVGC_878_1371})

V_517 = CTVertex(name = 'V_517',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.mu__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_912_1457,(0,0,2):C.UVGC_912_1458,(0,0,1):C.UVGC_689_1007,(0,1,0):C.UVGC_921_1479,(0,1,2):C.UVGC_921_1480,(0,1,1):C.UVGC_921_1481})

V_518 = CTVertex(name = 'V_518',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.ta__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_641_861,(0,0,2):C.UVGC_641_862,(0,0,1):C.UVGC_641_863,(0,1,0):C.UVGC_650_888,(0,1,2):C.UVGC_650_889,(0,1,1):C.UVGC_650_890})

V_519 = CTVertex(name = 'V_519',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.ta__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_870_1349,(0,0,2):C.UVGC_870_1350,(0,0,1):C.UVGC_605_755,(0,1,0):C.UVGC_879_1372,(0,1,2):C.UVGC_879_1373,(0,1,1):C.UVGC_879_1374})

V_520 = CTVertex(name = 'V_520',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ta__minus__, P.LQ2uu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_913_1459,(0,0,2):C.UVGC_913_1460,(0,0,1):C.UVGC_690_1010,(0,1,0):C.UVGC_922_1482,(0,1,2):C.UVGC_922_1483,(0,1,1):C.UVGC_922_1484})

V_521 = CTVertex(name = 'V_521',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.ve, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_636_846,(0,0,2):C.UVGC_636_847,(0,0,1):C.UVGC_636_848})

V_522 = CTVertex(name = 'V_522',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.ve, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_865_1336,(0,0,2):C.UVGC_865_1337,(0,0,1):C.UVGC_865_1338})

V_523 = CTVertex(name = 'V_523',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ve, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_908_1446,(0,0,2):C.UVGC_908_1447,(0,0,1):C.UVGC_908_1448})

V_524 = CTVertex(name = 'V_524',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vm, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_637_849,(0,0,2):C.UVGC_637_850,(0,0,1):C.UVGC_637_851})

V_525 = CTVertex(name = 'V_525',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vm, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_866_1339,(0,0,2):C.UVGC_866_1340,(0,0,1):C.UVGC_866_1341})

V_526 = CTVertex(name = 'V_526',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.vm, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_909_1449,(0,0,2):C.UVGC_909_1450,(0,0,1):C.UVGC_909_1451})

V_527 = CTVertex(name = 'V_527',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vt, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_638_852,(0,0,2):C.UVGC_638_853,(0,0,1):C.UVGC_638_854})

V_528 = CTVertex(name = 'V_528',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vt, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_867_1342,(0,0,2):C.UVGC_867_1343,(0,0,1):C.UVGC_867_1344})

V_529 = CTVertex(name = 'V_529',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.vt, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_910_1452,(0,0,2):C.UVGC_910_1453,(0,0,1):C.UVGC_910_1454})

V_530 = CTVertex(name = 'V_530',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.e__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_600_738,(0,0,2):C.UVGC_600_739,(0,0,1):C.UVGC_600_740})

V_531 = CTVertex(name = 'V_531',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.e__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_685_993,(0,0,2):C.UVGC_685_994,(0,0,1):C.UVGC_685_995})

V_532 = CTVertex(name = 'V_532',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.e__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_826_1238,(0,0,2):C.UVGC_826_1239,(0,0,1):C.UVGC_826_1240})

V_533 = CTVertex(name = 'V_533',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.mu__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_601_741,(0,0,2):C.UVGC_601_742,(0,0,1):C.UVGC_601_743})

V_534 = CTVertex(name = 'V_534',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.mu__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_686_996,(0,0,2):C.UVGC_686_997,(0,0,1):C.UVGC_686_998})

V_535 = CTVertex(name = 'V_535',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_827_1241,(0,0,2):C.UVGC_827_1242,(0,0,1):C.UVGC_827_1243})

V_536 = CTVertex(name = 'V_536',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ta__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_602_744,(0,0,2):C.UVGC_602_745,(0,0,1):C.UVGC_602_746})

V_537 = CTVertex(name = 'V_537',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.ta__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_687_999,(0,0,2):C.UVGC_687_1000,(0,0,1):C.UVGC_687_1001})

V_538 = CTVertex(name = 'V_538',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.ta__minus__, P.LQ2pu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_828_1244,(0,0,2):C.UVGC_828_1245,(0,0,1):C.UVGC_828_1246})

V_539 = CTVertex(name = 'V_539',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ve, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_594_720,(0,0,2):C.UVGC_594_721,(0,0,1):C.UVGC_594_722})

V_540 = CTVertex(name = 'V_540',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.ve, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_679_975,(0,0,2):C.UVGC_679_976,(0,0,1):C.UVGC_679_977})

V_541 = CTVertex(name = 'V_541',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.ve, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_820_1220,(0,0,2):C.UVGC_820_1221,(0,0,1):C.UVGC_820_1222})

V_542 = CTVertex(name = 'V_542',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.vm, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_595_723,(0,0,2):C.UVGC_595_724,(0,0,1):C.UVGC_595_725})

V_543 = CTVertex(name = 'V_543',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.vm, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_680_978,(0,0,2):C.UVGC_680_979,(0,0,1):C.UVGC_680_980})

V_544 = CTVertex(name = 'V_544',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.vm, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_821_1223,(0,0,2):C.UVGC_821_1224,(0,0,1):C.UVGC_821_1225})

V_545 = CTVertex(name = 'V_545',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.vt, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_596_726,(0,0,2):C.UVGC_596_727,(0,0,1):C.UVGC_596_728})

V_546 = CTVertex(name = 'V_546',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.vt, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_681_981,(0,0,2):C.UVGC_681_982,(0,0,1):C.UVGC_681_983})

V_547 = CTVertex(name = 'V_547',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.vt, P.LQ2d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_822_1226,(0,0,2):C.UVGC_822_1227,(0,0,1):C.UVGC_822_1228})

V_548 = CTVertex(name = 'V_548',
                 type = 'UV',
                 particles = [ P.ve, P.b, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_582_684,(0,0,2):C.UVGC_582_685,(0,0,1):C.UVGC_582_686})

V_549 = CTVertex(name = 'V_549',
                 type = 'UV',
                 particles = [ P.vm, P.b, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_583_687,(0,0,2):C.UVGC_583_688,(0,0,1):C.UVGC_583_689})

V_550 = CTVertex(name = 'V_550',
                 type = 'UV',
                 particles = [ P.vt, P.b, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_584_690,(0,0,2):C.UVGC_584_691,(0,0,1):C.UVGC_584_692})

V_551 = CTVertex(name = 'V_551',
                 type = 'UV',
                 particles = [ P.ve, P.d, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_667_939,(0,0,2):C.UVGC_667_940,(0,0,1):C.UVGC_667_941})

V_552 = CTVertex(name = 'V_552',
                 type = 'UV',
                 particles = [ P.vm, P.d, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_668_942,(0,0,2):C.UVGC_668_943,(0,0,1):C.UVGC_668_944})

V_553 = CTVertex(name = 'V_553',
                 type = 'UV',
                 particles = [ P.vt, P.d, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_669_945,(0,0,2):C.UVGC_669_946,(0,0,1):C.UVGC_669_947})

V_554 = CTVertex(name = 'V_554',
                 type = 'UV',
                 particles = [ P.ve, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_808_1184,(0,0,2):C.UVGC_808_1185,(0,0,1):C.UVGC_808_1186})

V_555 = CTVertex(name = 'V_555',
                 type = 'UV',
                 particles = [ P.vm, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_809_1187,(0,0,2):C.UVGC_809_1188,(0,0,1):C.UVGC_809_1189})

V_556 = CTVertex(name = 'V_556',
                 type = 'UV',
                 particles = [ P.vt, P.s, P.LQ1d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_810_1190,(0,0,2):C.UVGC_810_1191,(0,0,1):C.UVGC_810_1192})

V_557 = CTVertex(name = 'V_557',
                 type = 'UV',
                 particles = [ P.e__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_703_1047,(0,0,2):C.UVGC_703_1048,(0,0,1):C.UVGC_703_1049})

V_558 = CTVertex(name = 'V_558',
                 type = 'UV',
                 particles = [ P.ve, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_697_1029,(0,0,2):C.UVGC_697_1030,(0,0,1):C.UVGC_697_1031})

V_559 = CTVertex(name = 'V_559',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_704_1050,(0,0,2):C.UVGC_704_1051,(0,0,1):C.UVGC_704_1052})

V_560 = CTVertex(name = 'V_560',
                 type = 'UV',
                 particles = [ P.vm, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_698_1032,(0,0,2):C.UVGC_698_1033,(0,0,1):C.UVGC_698_1034})

V_561 = CTVertex(name = 'V_561',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_705_1053,(0,0,2):C.UVGC_705_1054,(0,0,1):C.UVGC_705_1055})

V_562 = CTVertex(name = 'V_562',
                 type = 'UV',
                 particles = [ P.vt, P.d, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_699_1035,(0,0,2):C.UVGC_699_1036,(0,0,1):C.UVGC_699_1037})

V_563 = CTVertex(name = 'V_563',
                 type = 'UV',
                 particles = [ P.ve, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_654_901,(0,0,2):C.UVGC_838_1262,(0,0,1):C.UVGC_654_902})

V_564 = CTVertex(name = 'V_564',
                 type = 'UV',
                 particles = [ P.vm, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_655_904,(0,0,2):C.UVGC_839_1263,(0,0,1):C.UVGC_655_905})

V_565 = CTVertex(name = 'V_565',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.s, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_846_1280,(0,0,2):C.UVGC_846_1281,(0,0,1):C.UVGC_846_1282})

V_566 = CTVertex(name = 'V_566',
                 type = 'UV',
                 particles = [ P.vt, P.s, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_656_907,(0,0,2):C.UVGC_840_1264,(0,0,1):C.UVGC_656_908})

V_567 = CTVertex(name = 'V_567',
                 type = 'UV',
                 particles = [ P.e__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_618_792,(0,0,2):C.UVGC_618_793,(0,0,1):C.UVGC_618_794})

V_568 = CTVertex(name = 'V_568',
                 type = 'UV',
                 particles = [ P.ve, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_612_774,(0,0,2):C.UVGC_612_775,(0,0,1):C.UVGC_612_776})

V_569 = CTVertex(name = 'V_569',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_619_795,(0,0,2):C.UVGC_619_796,(0,0,1):C.UVGC_619_797})

V_570 = CTVertex(name = 'V_570',
                 type = 'UV',
                 particles = [ P.vm, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_613_777,(0,0,2):C.UVGC_613_778,(0,0,1):C.UVGC_613_779})

V_571 = CTVertex(name = 'V_571',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.b, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_620_798,(0,0,2):C.UVGC_620_799,(0,0,1):C.UVGC_620_800})

V_572 = CTVertex(name = 'V_572',
                 type = 'UV',
                 particles = [ P.vt, P.b, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_614_780,(0,0,2):C.UVGC_614_781,(0,0,1):C.UVGC_614_782})

V_573 = CTVertex(name = 'V_573',
                 type = 'UV',
                 particles = [ P.ve, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_932_1500,(0,0,2):C.UVGC_932_1501,(0,0,1):C.UVGC_932_1502})

V_574 = CTVertex(name = 'V_574',
                 type = 'UV',
                 particles = [ P.vm, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_933_1503,(0,0,2):C.UVGC_933_1504,(0,0,1):C.UVGC_933_1505})

V_575 = CTVertex(name = 'V_575',
                 type = 'UV',
                 particles = [ P.vt, P.u, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_934_1506,(0,0,2):C.UVGC_934_1507,(0,0,1):C.UVGC_934_1508})

V_576 = CTVertex(name = 'V_576',
                 type = 'UV',
                 particles = [ P.ve, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_660_918,(0,0,2):C.UVGC_660_919,(0,0,1):C.UVGC_660_920})

V_577 = CTVertex(name = 'V_577',
                 type = 'UV',
                 particles = [ P.e__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_654_900,(0,0,2):C.UVGC_654_901,(0,0,1):C.UVGC_654_902})

V_578 = CTVertex(name = 'V_578',
                 type = 'UV',
                 particles = [ P.vm, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_661_921,(0,0,2):C.UVGC_661_922,(0,0,1):C.UVGC_661_923})

V_579 = CTVertex(name = 'V_579',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_655_903,(0,0,2):C.UVGC_655_904,(0,0,1):C.UVGC_655_905})

V_580 = CTVertex(name = 'V_580',
                 type = 'UV',
                 particles = [ P.vt, P.c, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_662_924,(0,0,2):C.UVGC_662_925,(0,0,1):C.UVGC_662_926})

V_581 = CTVertex(name = 'V_581',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.c, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_656_906,(0,0,2):C.UVGC_656_907,(0,0,1):C.UVGC_656_908})

V_582 = CTVertex(name = 'V_582',
                 type = 'UV',
                 particles = [ P.ve, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_889_1390,(0,0,2):C.UVGC_889_1391,(0,0,1):C.UVGC_889_1392})

V_583 = CTVertex(name = 'V_583',
                 type = 'UV',
                 particles = [ P.vm, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_890_1393,(0,0,2):C.UVGC_890_1394,(0,0,1):C.UVGC_890_1395})

V_584 = CTVertex(name = 'V_584',
                 type = 'UV',
                 particles = [ P.vt, P.t, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_891_1396,(0,0,2):C.UVGC_891_1397,(0,0,1):C.UVGC_891_1398})

V_585 = CTVertex(name = 'V_585',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.t, P.LQ3d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_614_781,(0,0,2):C.UVGC_885_1380,(0,0,1):C.UVGC_614_782})

V_586 = CTVertex(name = 'V_586',
                 type = 'UV',
                 particles = [ P.u, P.e__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_697_1030,(0,0,2):C.UVGC_926_1488,(0,0,1):C.UVGC_697_1031})

V_587 = CTVertex(name = 'V_587',
                 type = 'UV',
                 particles = [ P.u, P.mu__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_698_1033,(0,0,2):C.UVGC_927_1489,(0,0,1):C.UVGC_698_1034})

V_588 = CTVertex(name = 'V_588',
                 type = 'UV',
                 particles = [ P.u, P.ta__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_699_1036,(0,0,2):C.UVGC_928_1490,(0,0,1):C.UVGC_699_1037})

V_589 = CTVertex(name = 'V_589',
                 type = 'UV',
                 particles = [ P.s, P.e__minus__, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_844_1274,(0,0,2):C.UVGC_844_1275,(0,0,1):C.UVGC_844_1276})

V_590 = CTVertex(name = 'V_590',
                 type = 'UV',
                 particles = [ P.s, P.mu__minus__, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_845_1277,(0,0,2):C.UVGC_845_1278,(0,0,1):C.UVGC_845_1279})

V_591 = CTVertex(name = 'V_591',
                 type = 'UV',
                 particles = [ P.t, P.e__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_612_775,(0,0,2):C.UVGC_883_1378,(0,0,1):C.UVGC_612_776})

V_592 = CTVertex(name = 'V_592',
                 type = 'UV',
                 particles = [ P.t, P.mu__minus__, P.LQ3d__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_613_778,(0,0,2):C.UVGC_884_1379,(0,0,1):C.UVGC_613_779})

V_593 = CTVertex(name = 'V_593',
                 type = 'UV',
                 particles = [ P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_447_345})

V_594 = CTVertex(name = 'V_594',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_448_346,(0,0,1):C.UVGC_448_347,(0,0,2):C.UVGC_448_348,(0,0,3):C.UVGC_448_349,(0,0,4):C.UVGC_448_350,(0,0,6):C.UVGC_448_351,(0,0,7):C.UVGC_448_352,(0,0,8):C.UVGC_448_353,(0,0,9):C.UVGC_448_354,(0,0,10):C.UVGC_448_355,(0,0,11):C.UVGC_448_356,(0,0,12):C.UVGC_448_357,(0,0,13):C.UVGC_448_358,(0,0,14):C.UVGC_448_359,(0,0,15):C.UVGC_448_360,(0,0,16):C.UVGC_448_361,(0,0,17):C.UVGC_448_362,(0,0,5):C.UVGC_448_363})

V_595 = CTVertex(name = 'V_595',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_451_385,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_451_385,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_596 = CTVertex(name = 'V_596',
                 type = 'UV',
                 particles = [ P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_460_411})

V_597 = CTVertex(name = 'V_597',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_461_412,(0,0,1):C.UVGC_461_413,(0,0,2):C.UVGC_461_414,(0,0,3):C.UVGC_461_415,(0,0,4):C.UVGC_461_416,(0,0,6):C.UVGC_461_417,(0,0,7):C.UVGC_461_418,(0,0,8):C.UVGC_461_419,(0,0,9):C.UVGC_461_420,(0,0,10):C.UVGC_461_421,(0,0,11):C.UVGC_461_422,(0,0,12):C.UVGC_461_423,(0,0,13):C.UVGC_461_424,(0,0,14):C.UVGC_461_425,(0,0,15):C.UVGC_461_426,(0,0,16):C.UVGC_461_427,(0,0,17):C.UVGC_461_428,(0,0,5):C.UVGC_461_429})

V_598 = CTVertex(name = 'V_598',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_464_430,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_464_430,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_599 = CTVertex(name = 'V_599',
                 type = 'UV',
                 particles = [ P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_497_528})

V_600 = CTVertex(name = 'V_600',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_486_485,(0,0,1):C.UVGC_486_486,(0,0,2):C.UVGC_486_487,(0,0,3):C.UVGC_486_488,(0,0,4):C.UVGC_486_489,(0,0,6):C.UVGC_486_490,(0,0,7):C.UVGC_486_491,(0,0,8):C.UVGC_486_492,(0,0,9):C.UVGC_486_493,(0,0,10):C.UVGC_486_494,(0,0,11):C.UVGC_486_495,(0,0,12):C.UVGC_486_496,(0,0,13):C.UVGC_486_497,(0,0,14):C.UVGC_486_498,(0,0,15):C.UVGC_486_499,(0,0,16):C.UVGC_486_500,(0,0,17):C.UVGC_486_501,(0,0,5):C.UVGC_498_529})

V_601 = CTVertex(name = 'V_601',
                 type = 'UV',
                 particles = [ P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_512_559})

V_602 = CTVertex(name = 'V_602',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_513_560,(0,0,1):C.UVGC_513_561,(0,0,2):C.UVGC_513_562,(0,0,3):C.UVGC_513_563,(0,0,4):C.UVGC_513_564,(0,0,6):C.UVGC_513_565,(0,0,7):C.UVGC_513_566,(0,0,8):C.UVGC_513_567,(0,0,9):C.UVGC_513_568,(0,0,10):C.UVGC_513_569,(0,0,11):C.UVGC_513_570,(0,0,12):C.UVGC_513_571,(0,0,13):C.UVGC_513_572,(0,0,14):C.UVGC_513_573,(0,0,15):C.UVGC_513_574,(0,0,16):C.UVGC_513_575,(0,0,17):C.UVGC_513_576,(0,0,5):C.UVGC_513_577})

V_603 = CTVertex(name = 'V_603',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_501_530,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_501_530,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_604 = CTVertex(name = 'V_604',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_516_578,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_516_578,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_605 = CTVertex(name = 'V_605',
                 type = 'UV',
                 particles = [ P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_473_457})

V_606 = CTVertex(name = 'V_606',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_448_346,(0,0,1):C.UVGC_448_347,(0,0,2):C.UVGC_448_348,(0,0,3):C.UVGC_448_349,(0,0,4):C.UVGC_448_350,(0,0,6):C.UVGC_448_351,(0,0,7):C.UVGC_448_352,(0,0,8):C.UVGC_448_353,(0,0,9):C.UVGC_448_354,(0,0,10):C.UVGC_448_355,(0,0,11):C.UVGC_448_356,(0,0,12):C.UVGC_448_357,(0,0,13):C.UVGC_448_358,(0,0,14):C.UVGC_448_359,(0,0,15):C.UVGC_448_360,(0,0,16):C.UVGC_448_361,(0,0,17):C.UVGC_448_362,(0,0,5):C.UVGC_474_458})

V_607 = CTVertex(name = 'V_607',
                 type = 'UV',
                 particles = [ P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_485_484})

V_608 = CTVertex(name = 'V_608',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_486_485,(0,0,1):C.UVGC_486_486,(0,0,2):C.UVGC_486_487,(0,0,3):C.UVGC_486_488,(0,0,4):C.UVGC_486_489,(0,0,6):C.UVGC_486_490,(0,0,7):C.UVGC_486_491,(0,0,8):C.UVGC_486_492,(0,0,9):C.UVGC_486_493,(0,0,10):C.UVGC_486_494,(0,0,11):C.UVGC_486_495,(0,0,12):C.UVGC_486_496,(0,0,13):C.UVGC_486_497,(0,0,14):C.UVGC_486_498,(0,0,15):C.UVGC_486_499,(0,0,16):C.UVGC_486_500,(0,0,17):C.UVGC_486_501,(0,0,5):C.UVGC_486_502})

V_609 = CTVertex(name = 'V_609',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_477_459,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_477_459,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_610 = CTVertex(name = 'V_610',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_489_503,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_489_503,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_611 = CTVertex(name = 'V_611',
                 type = 'UV',
                 particles = [ P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_525_604})

V_612 = CTVertex(name = 'V_612',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_448_346,(0,0,1):C.UVGC_448_347,(0,0,2):C.UVGC_448_348,(0,0,3):C.UVGC_448_349,(0,0,4):C.UVGC_448_350,(0,0,6):C.UVGC_448_351,(0,0,7):C.UVGC_448_352,(0,0,8):C.UVGC_448_353,(0,0,9):C.UVGC_448_354,(0,0,10):C.UVGC_448_355,(0,0,11):C.UVGC_448_356,(0,0,12):C.UVGC_448_357,(0,0,13):C.UVGC_448_358,(0,0,14):C.UVGC_448_359,(0,0,15):C.UVGC_448_360,(0,0,16):C.UVGC_448_361,(0,0,17):C.UVGC_448_362,(0,0,5):C.UVGC_526_605})

V_613 = CTVertex(name = 'V_613',
                 type = 'UV',
                 particles = [ P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_537_614})

V_614 = CTVertex(name = 'V_614',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_461_412,(0,0,1):C.UVGC_461_413,(0,0,2):C.UVGC_461_414,(0,0,3):C.UVGC_461_415,(0,0,4):C.UVGC_461_416,(0,0,6):C.UVGC_461_417,(0,0,7):C.UVGC_461_418,(0,0,8):C.UVGC_461_419,(0,0,9):C.UVGC_461_420,(0,0,10):C.UVGC_461_421,(0,0,11):C.UVGC_461_422,(0,0,12):C.UVGC_461_423,(0,0,13):C.UVGC_461_424,(0,0,14):C.UVGC_461_425,(0,0,15):C.UVGC_461_426,(0,0,16):C.UVGC_461_427,(0,0,17):C.UVGC_461_428,(0,0,5):C.UVGC_538_615})

V_615 = CTVertex(name = 'V_615',
                 type = 'UV',
                 particles = [ P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_447_328,(0,0,1):C.UVGC_447_329,(0,0,2):C.UVGC_447_330,(0,0,3):C.UVGC_447_331,(0,0,4):C.UVGC_447_332,(0,0,6):C.UVGC_447_333,(0,0,7):C.UVGC_447_334,(0,0,8):C.UVGC_447_335,(0,0,9):C.UVGC_447_336,(0,0,10):C.UVGC_447_337,(0,0,11):C.UVGC_447_338,(0,0,12):C.UVGC_447_339,(0,0,13):C.UVGC_447_340,(0,0,14):C.UVGC_447_341,(0,0,15):C.UVGC_447_342,(0,0,16):C.UVGC_447_343,(0,0,17):C.UVGC_447_344,(0,0,5):C.UVGC_551_644})

V_616 = CTVertex(name = 'V_616',
                 type = 'UV',
                 particles = [ P.a, P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(2,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_486_485,(0,0,1):C.UVGC_486_486,(0,0,2):C.UVGC_486_487,(0,0,3):C.UVGC_486_488,(0,0,4):C.UVGC_486_489,(0,0,6):C.UVGC_486_490,(0,0,7):C.UVGC_486_491,(0,0,8):C.UVGC_486_492,(0,0,9):C.UVGC_486_493,(0,0,10):C.UVGC_486_494,(0,0,11):C.UVGC_486_495,(0,0,12):C.UVGC_486_496,(0,0,13):C.UVGC_486_497,(0,0,14):C.UVGC_486_498,(0,0,15):C.UVGC_486_499,(0,0,16):C.UVGC_486_500,(0,0,17):C.UVGC_486_501,(0,0,5):C.UVGC_552_645})

V_617 = CTVertex(name = 'V_617',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_529_606,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_529_606,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_618 = CTVertex(name = 'V_618',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_541_616,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_541_616,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_619 = CTVertex(name = 'V_619',
                 type = 'UV',
                 particles = [ P.g, P.g, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(2,0,0):C.UVGC_451_368,(2,0,1):C.UVGC_451_369,(2,0,2):C.UVGC_451_370,(2,0,3):C.UVGC_451_371,(2,0,4):C.UVGC_451_372,(2,0,6):C.UVGC_451_373,(2,0,7):C.UVGC_451_374,(2,0,8):C.UVGC_451_375,(2,0,9):C.UVGC_451_376,(2,0,10):C.UVGC_451_377,(2,0,11):C.UVGC_451_378,(2,0,12):C.UVGC_451_379,(2,0,13):C.UVGC_451_380,(2,0,14):C.UVGC_451_381,(2,0,15):C.UVGC_451_382,(2,0,16):C.UVGC_451_383,(2,0,17):C.UVGC_451_384,(2,0,5):C.UVGC_555_646,(1,0,0):C.UVGC_451_368,(1,0,1):C.UVGC_451_369,(1,0,2):C.UVGC_451_370,(1,0,3):C.UVGC_451_371,(1,0,4):C.UVGC_451_372,(1,0,6):C.UVGC_451_373,(1,0,7):C.UVGC_451_374,(1,0,8):C.UVGC_451_375,(1,0,9):C.UVGC_451_376,(1,0,10):C.UVGC_451_377,(1,0,11):C.UVGC_451_378,(1,0,12):C.UVGC_451_379,(1,0,13):C.UVGC_451_380,(1,0,14):C.UVGC_451_381,(1,0,15):C.UVGC_451_382,(1,0,16):C.UVGC_451_383,(1,0,17):C.UVGC_451_384,(1,0,5):C.UVGC_555_646,(0,0,3):C.UVGC_450_366,(0,0,5):C.UVGC_450_367})

V_620 = CTVertex(name = 'V_620',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_670_948,(0,0,2):C.UVGC_670_949,(0,0,1):C.UVGC_670_950})

V_621 = CTVertex(name = 'V_621',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_671_951,(0,0,2):C.UVGC_671_952,(0,0,1):C.UVGC_671_953})

V_622 = CTVertex(name = 'V_622',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.d__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_672_954,(0,0,2):C.UVGC_672_955,(0,0,1):C.UVGC_672_956})

V_623 = CTVertex(name = 'V_623',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.e__plus__, P.LQ1dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_811_1193,(0,0,2):C.UVGC_811_1194,(0,0,1):C.UVGC_811_1195})

V_624 = CTVertex(name = 'V_624',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__plus__, P.LQ1dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_812_1196,(0,0,2):C.UVGC_812_1197,(0,0,1):C.UVGC_812_1198})

V_625 = CTVertex(name = 'V_625',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.s__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_813_1199,(0,0,2):C.UVGC_813_1200,(0,0,1):C.UVGC_813_1201})

V_626 = CTVertex(name = 'V_626',
                 type = 'UV',
                 particles = [ P.e__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_585_693,(0,0,2):C.UVGC_585_694,(0,0,1):C.UVGC_585_695})

V_627 = CTVertex(name = 'V_627',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_586_696,(0,0,2):C.UVGC_586_697,(0,0,1):C.UVGC_586_698})

V_628 = CTVertex(name = 'V_628',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_587_699,(0,0,2):C.UVGC_587_700,(0,0,1):C.UVGC_587_701})

V_629 = CTVertex(name = 'V_629',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_682_984,(0,0,2):C.UVGC_682_985,(0,0,1):C.UVGC_682_986})

V_630 = CTVertex(name = 'V_630',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_676_966,(0,0,2):C.UVGC_676_967,(0,0,1):C.UVGC_676_968})

V_631 = CTVertex(name = 'V_631',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_683_987,(0,0,2):C.UVGC_683_988,(0,0,1):C.UVGC_683_989})

V_632 = CTVertex(name = 'V_632',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_677_969,(0,0,2):C.UVGC_677_970,(0,0,1):C.UVGC_677_971})

V_633 = CTVertex(name = 'V_633',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_684_990,(0,0,2):C.UVGC_684_991,(0,0,1):C.UVGC_684_992})

V_634 = CTVertex(name = 'V_634',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.d, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_678_972,(0,0,2):C.UVGC_678_973,(0,0,1):C.UVGC_678_974})

V_635 = CTVertex(name = 'V_635',
                 type = 'UV',
                 particles = [ P.e__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_823_1229,(0,0,2):C.UVGC_823_1230,(0,0,1):C.UVGC_823_1231})

V_636 = CTVertex(name = 'V_636',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_817_1211,(0,0,2):C.UVGC_817_1212,(0,0,1):C.UVGC_817_1213})

V_637 = CTVertex(name = 'V_637',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_824_1232,(0,0,2):C.UVGC_824_1233,(0,0,1):C.UVGC_824_1234})

V_638 = CTVertex(name = 'V_638',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_818_1214,(0,0,2):C.UVGC_818_1215,(0,0,1):C.UVGC_818_1216})

V_639 = CTVertex(name = 'V_639',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.s, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_825_1235,(0,0,2):C.UVGC_825_1236,(0,0,1):C.UVGC_825_1237})

V_640 = CTVertex(name = 'V_640',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.s, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_819_1217,(0,0,2):C.UVGC_819_1218,(0,0,1):C.UVGC_819_1219})

V_641 = CTVertex(name = 'V_641',
                 type = 'UV',
                 particles = [ P.e__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_597_729,(0,0,2):C.UVGC_597_730,(0,0,1):C.UVGC_597_731})

V_642 = CTVertex(name = 'V_642',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_591_711,(0,0,2):C.UVGC_591_712,(0,0,1):C.UVGC_591_713})

V_643 = CTVertex(name = 'V_643',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_598_732,(0,0,2):C.UVGC_598_733,(0,0,1):C.UVGC_598_734})

V_644 = CTVertex(name = 'V_644',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_592_714,(0,0,2):C.UVGC_592_715,(0,0,1):C.UVGC_592_716})

V_645 = CTVertex(name = 'V_645',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_599_735,(0,0,2):C.UVGC_599_736,(0,0,1):C.UVGC_599_737})

V_646 = CTVertex(name = 'V_646',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b, P.LQ2d__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2d] ], [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_593_717,(0,0,2):C.UVGC_593_718,(0,0,1):C.UVGC_593_719})

V_647 = CTVertex(name = 'V_647',
                 type = 'UV',
                 particles = [ P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,1,0):C.UVGC_748_1070,(0,1,2):C.UVGC_748_1071,(0,1,1):C.UVGC_748_1072,(0,0,0):C.UVGC_378_151,(0,2,2):C.UVGC_379_152})

V_648 = CTVertex(name = 'V_648',
                 type = 'UV',
                 particles = [ P.a, P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_749_1073,(0,0,2):C.UVGC_749_1074,(0,0,1):C.UVGC_749_1075})

V_649 = CTVertex(name = 'V_649',
                 type = 'UV',
                 particles = [ P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,1,0):C.UVGC_777_1103,(0,1,2):C.UVGC_777_1104,(0,1,1):C.UVGC_748_1072,(0,0,0):C.UVGC_378_151,(0,2,2):C.UVGC_379_152})

V_650 = CTVertex(name = 'V_650',
                 type = 'UV',
                 particles = [ P.a, P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_778_1105,(0,0,2):C.UVGC_778_1106,(0,0,1):C.UVGC_778_1107})

V_651 = CTVertex(name = 'V_651',
                 type = 'UV',
                 particles = [ P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,1,0):C.UVGC_790_1116,(0,1,2):C.UVGC_799_1150,(0,1,1):C.UVGC_790_1118,(0,0,0):C.UVGC_387_154,(0,2,2):C.UVGC_386_153})

V_652 = CTVertex(name = 'V_652',
                 type = 'UV',
                 particles = [ P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,1,0):C.UVGC_790_1116,(0,1,1):C.UVGC_790_1117,(0,1,2):C.UVGC_790_1118,(0,0,0):C.UVGC_387_154,(0,2,1):C.UVGC_386_153})

V_653 = CTVertex(name = 'V_653',
                 type = 'UV',
                 particles = [ P.a, P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_792_1122,(0,0,1):C.UVGC_792_1123,(0,0,2):C.UVGC_792_1124})

V_654 = CTVertex(name = 'V_654',
                 type = 'UV',
                 particles = [ P.a, P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_801_1152,(0,0,2):C.UVGC_801_1153,(0,0,1):C.UVGC_801_1154})

V_655 = CTVertex(name = 'V_655',
                 type = 'UV',
                 particles = [ P.g, P.W__minus__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_750_1076,(0,0,1):C.UVGC_750_1077,(0,0,2):C.UVGC_750_1078,(0,0,3):C.UVGC_750_1079,(0,0,4):C.UVGC_750_1080,(0,0,8):C.UVGC_750_1081,(0,0,9):C.UVGC_750_1082,(0,0,10):C.UVGC_750_1083,(0,0,11):C.UVGC_750_1084,(0,0,12):C.UVGC_750_1085,(0,0,13):C.UVGC_750_1086,(0,0,14):C.UVGC_750_1087,(0,0,15):C.UVGC_750_1088,(0,0,16):C.UVGC_750_1089,(0,0,17):C.UVGC_750_1090,(0,0,18):C.UVGC_750_1091,(0,0,19):C.UVGC_750_1092,(0,0,5):C.UVGC_779_1108,(0,0,7):C.UVGC_779_1109,(0,0,6):C.UVGC_750_1095})

V_656 = CTVertex(name = 'V_656',
                 type = 'UV',
                 particles = [ P.g, P.W__minus__, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_750_1076,(0,0,1):C.UVGC_750_1077,(0,0,2):C.UVGC_750_1078,(0,0,3):C.UVGC_750_1079,(0,0,4):C.UVGC_750_1080,(0,0,8):C.UVGC_750_1081,(0,0,9):C.UVGC_750_1082,(0,0,10):C.UVGC_750_1083,(0,0,11):C.UVGC_750_1084,(0,0,12):C.UVGC_750_1085,(0,0,13):C.UVGC_750_1086,(0,0,14):C.UVGC_750_1087,(0,0,15):C.UVGC_750_1088,(0,0,16):C.UVGC_750_1089,(0,0,17):C.UVGC_750_1090,(0,0,18):C.UVGC_750_1091,(0,0,19):C.UVGC_750_1092,(0,0,5):C.UVGC_750_1093,(0,0,7):C.UVGC_750_1094,(0,0,6):C.UVGC_750_1095})

V_657 = CTVertex(name = 'V_657',
                 type = 'UV',
                 particles = [ P.g, P.W__minus__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_793_1125,(0,0,1):C.UVGC_793_1126,(0,0,2):C.UVGC_793_1127,(0,0,3):C.UVGC_793_1128,(0,0,4):C.UVGC_793_1129,(0,0,8):C.UVGC_793_1130,(0,0,9):C.UVGC_793_1131,(0,0,10):C.UVGC_793_1132,(0,0,11):C.UVGC_793_1133,(0,0,12):C.UVGC_793_1134,(0,0,13):C.UVGC_793_1135,(0,0,14):C.UVGC_793_1136,(0,0,15):C.UVGC_793_1137,(0,0,16):C.UVGC_793_1138,(0,0,17):C.UVGC_793_1139,(0,0,18):C.UVGC_793_1140,(0,0,19):C.UVGC_793_1141,(0,0,5):C.UVGC_793_1142,(0,0,6):C.UVGC_793_1143,(0,0,7):C.UVGC_793_1144})

V_658 = CTVertex(name = 'V_658',
                 type = 'UV',
                 particles = [ P.g, P.W__minus__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_802_1155,(0,0,1):C.UVGC_802_1156,(0,0,2):C.UVGC_802_1157,(0,0,3):C.UVGC_802_1158,(0,0,4):C.UVGC_802_1159,(0,0,8):C.UVGC_802_1160,(0,0,9):C.UVGC_802_1161,(0,0,10):C.UVGC_802_1162,(0,0,11):C.UVGC_802_1163,(0,0,12):C.UVGC_802_1164,(0,0,13):C.UVGC_802_1165,(0,0,14):C.UVGC_802_1166,(0,0,15):C.UVGC_802_1167,(0,0,16):C.UVGC_802_1168,(0,0,17):C.UVGC_802_1169,(0,0,18):C.UVGC_802_1170,(0,0,19):C.UVGC_802_1171,(0,0,5):C.UVGC_802_1172,(0,0,7):C.UVGC_802_1173,(0,0,6):C.UVGC_802_1174})

V_659 = CTVertex(name = 'V_659',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__minus__, P.LQ3dd__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd], [P.g, P.LQ3d, P.LQ3u], [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_944_1529,(0,0,1):C.UVGC_944_1530,(0,0,4):C.UVGC_944_1531,(0,0,2):C.UVGC_1023_111,(0,0,3):C.UVGC_944_1532})

V_660 = CTVertex(name = 'V_660',
                 type = 'UV',
                 particles = [ P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,1,0):C.UVGC_747_1068,(0,1,2):C.UVGC_747_1069,(0,1,1):C.UVGC_663_929,(0,0,0):C.UVGC_379_152,(0,2,2):C.UVGC_378_151})

V_661 = CTVertex(name = 'V_661',
                 type = 'UV',
                 particles = [ P.a, P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_749_1073,(0,0,2):C.UVGC_749_1074,(0,0,1):C.UVGC_749_1075})

V_662 = CTVertex(name = 'V_662',
                 type = 'UV',
                 particles = [ P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,1,0):C.UVGC_776_1101,(0,1,2):C.UVGC_776_1102,(0,1,1):C.UVGC_663_929,(0,0,0):C.UVGC_379_152,(0,2,2):C.UVGC_378_151})

V_663 = CTVertex(name = 'V_663',
                 type = 'UV',
                 particles = [ P.a, P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_778_1105,(0,0,2):C.UVGC_778_1106,(0,0,1):C.UVGC_778_1107})

V_664 = CTVertex(name = 'V_664',
                 type = 'UV',
                 particles = [ P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,1,0):C.UVGC_791_1119,(0,1,1):C.UVGC_791_1120,(0,1,2):C.UVGC_791_1121,(0,0,0):C.UVGC_386_153,(0,2,1):C.UVGC_387_154})

V_665 = CTVertex(name = 'V_665',
                 type = 'UV',
                 particles = [ P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS1, L.VSS2, L.VSS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,1,0):C.UVGC_791_1119,(0,1,2):C.UVGC_800_1151,(0,1,1):C.UVGC_791_1121,(0,0,0):C.UVGC_386_153,(0,2,2):C.UVGC_387_154})

V_666 = CTVertex(name = 'V_666',
                 type = 'UV',
                 particles = [ P.a, P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_792_1122,(0,0,1):C.UVGC_792_1123,(0,0,2):C.UVGC_792_1124})

V_667 = CTVertex(name = 'V_667',
                 type = 'UV',
                 particles = [ P.a, P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_801_1152,(0,0,2):C.UVGC_801_1153,(0,0,1):C.UVGC_801_1154})

V_668 = CTVertex(name = 'V_668',
                 type = 'UV',
                 particles = [ P.g, P.W__plus__, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_750_1076,(0,0,1):C.UVGC_750_1077,(0,0,2):C.UVGC_750_1078,(0,0,3):C.UVGC_750_1079,(0,0,4):C.UVGC_750_1080,(0,0,8):C.UVGC_750_1081,(0,0,9):C.UVGC_750_1082,(0,0,10):C.UVGC_750_1083,(0,0,11):C.UVGC_750_1084,(0,0,12):C.UVGC_750_1085,(0,0,13):C.UVGC_750_1086,(0,0,14):C.UVGC_750_1087,(0,0,15):C.UVGC_750_1088,(0,0,16):C.UVGC_750_1089,(0,0,17):C.UVGC_750_1090,(0,0,18):C.UVGC_750_1091,(0,0,19):C.UVGC_750_1092,(0,0,5):C.UVGC_779_1108,(0,0,7):C.UVGC_779_1109,(0,0,6):C.UVGC_750_1095})

V_669 = CTVertex(name = 'V_669',
                 type = 'UV',
                 particles = [ P.g, P.W__plus__, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_750_1076,(0,0,1):C.UVGC_750_1077,(0,0,2):C.UVGC_750_1078,(0,0,3):C.UVGC_750_1079,(0,0,4):C.UVGC_750_1080,(0,0,8):C.UVGC_750_1081,(0,0,9):C.UVGC_750_1082,(0,0,10):C.UVGC_750_1083,(0,0,11):C.UVGC_750_1084,(0,0,12):C.UVGC_750_1085,(0,0,13):C.UVGC_750_1086,(0,0,14):C.UVGC_750_1087,(0,0,15):C.UVGC_750_1088,(0,0,16):C.UVGC_750_1089,(0,0,17):C.UVGC_750_1090,(0,0,18):C.UVGC_750_1091,(0,0,19):C.UVGC_750_1092,(0,0,5):C.UVGC_750_1093,(0,0,7):C.UVGC_750_1094,(0,0,6):C.UVGC_750_1095})

V_670 = CTVertex(name = 'V_670',
                 type = 'UV',
                 particles = [ P.g, P.W__plus__, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_793_1125,(0,0,1):C.UVGC_793_1126,(0,0,2):C.UVGC_793_1127,(0,0,3):C.UVGC_793_1128,(0,0,4):C.UVGC_793_1129,(0,0,8):C.UVGC_793_1130,(0,0,9):C.UVGC_793_1131,(0,0,10):C.UVGC_793_1132,(0,0,11):C.UVGC_793_1133,(0,0,12):C.UVGC_793_1134,(0,0,13):C.UVGC_793_1135,(0,0,14):C.UVGC_793_1136,(0,0,15):C.UVGC_793_1137,(0,0,16):C.UVGC_793_1138,(0,0,17):C.UVGC_793_1139,(0,0,18):C.UVGC_793_1140,(0,0,19):C.UVGC_793_1141,(0,0,5):C.UVGC_793_1142,(0,0,6):C.UVGC_793_1143,(0,0,7):C.UVGC_793_1144})

V_671 = CTVertex(name = 'V_671',
                 type = 'UV',
                 particles = [ P.g, P.W__plus__, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'T(1,3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_802_1155,(0,0,1):C.UVGC_802_1156,(0,0,2):C.UVGC_802_1157,(0,0,3):C.UVGC_802_1158,(0,0,4):C.UVGC_802_1159,(0,0,8):C.UVGC_802_1160,(0,0,9):C.UVGC_802_1161,(0,0,10):C.UVGC_802_1162,(0,0,11):C.UVGC_802_1163,(0,0,12):C.UVGC_802_1164,(0,0,13):C.UVGC_802_1165,(0,0,14):C.UVGC_802_1166,(0,0,15):C.UVGC_802_1167,(0,0,16):C.UVGC_802_1168,(0,0,17):C.UVGC_802_1169,(0,0,18):C.UVGC_802_1170,(0,0,19):C.UVGC_802_1171,(0,0,5):C.UVGC_802_1172,(0,0,7):C.UVGC_802_1173,(0,0,6):C.UVGC_802_1174})

V_672 = CTVertex(name = 'V_672',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_746_1067,(0,0,2):C.UVGC_1023_111,(0,0,1):C.UVGC_745_1066})

V_673 = CTVertex(name = 'V_673',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_1023_111,(0,0,2):C.UVGC_745_1065,(0,0,1):C.UVGC_745_1066})

V_674 = CTVertex(name = 'V_674',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_775_1100,(0,0,2):C.UVGC_1023_111,(0,0,1):C.UVGC_745_1066})

V_675 = CTVertex(name = 'V_675',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_1023_111,(0,0,2):C.UVGC_774_1099,(0,0,1):C.UVGC_745_1066})

V_676 = CTVertex(name = 'V_676',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd], [P.g, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3dd], [P.g, P.LQ3d, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_797_1146,(0,0,1):C.UVGC_788_1110,(0,0,2):C.UVGC_788_1112})

V_677 = CTVertex(name = 'V_677',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_788_1110,(0,0,1):C.UVGC_788_1111,(0,0,2):C.UVGC_788_1112})

V_678 = CTVertex(name = 'V_678',
                 type = 'UV',
                 particles = [ P.W__minus__, P.W__plus__, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_788_1110,(0,0,2):C.UVGC_796_1145,(0,0,1):C.UVGC_788_1112})

V_679 = CTVertex(name = 'V_679',
                 type = 'UV',
                 particles = [ P.W__plus__, P.W__plus__, P.LQ3dd, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd], [P.g, P.LQ3d, P.LQ3u], [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_944_1529,(0,0,1):C.UVGC_944_1530,(0,0,4):C.UVGC_944_1531,(0,0,2):C.UVGC_1023_111,(0,0,3):C.UVGC_944_1532})

V_680 = CTVertex(name = 'V_680',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_893_1401,(0,0,2):C.UVGC_893_1402,(0,0,1):C.UVGC_893_1403,(0,1,0):C.UVGC_896_1410,(0,1,2):C.UVGC_896_1411,(0,1,1):C.UVGC_896_1412})

V_681 = CTVertex(name = 'V_681',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_664_930,(0,0,2):C.UVGC_664_931,(0,0,1):C.UVGC_664_932})

V_682 = CTVertex(name = 'V_682',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.mu__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_894_1404,(0,0,2):C.UVGC_894_1405,(0,0,1):C.UVGC_894_1406,(0,1,0):C.UVGC_897_1413,(0,1,2):C.UVGC_897_1414,(0,1,1):C.UVGC_897_1415})

V_683 = CTVertex(name = 'V_683',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_665_933,(0,0,2):C.UVGC_665_934,(0,0,1):C.UVGC_665_935})

V_684 = CTVertex(name = 'V_684',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ta__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_895_1407,(0,0,2):C.UVGC_895_1408,(0,0,1):C.UVGC_895_1409,(0,1,0):C.UVGC_898_1416,(0,1,2):C.UVGC_898_1417,(0,1,1):C.UVGC_898_1418})

V_685 = CTVertex(name = 'V_685',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_666_936,(0,0,2):C.UVGC_666_937,(0,0,1):C.UVGC_666_938})

V_686 = CTVertex(name = 'V_686',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_805_1175,(0,0,2):C.UVGC_805_1176,(0,0,1):C.UVGC_805_1177})

V_687 = CTVertex(name = 'V_687',
                 type = 'UV',
                 particles = [ P.e__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_621_801,(0,0,2):C.UVGC_621_802,(0,0,1):C.UVGC_621_803,(0,1,0):C.UVGC_624_810,(0,1,2):C.UVGC_624_811,(0,1,1):C.UVGC_624_812})

V_688 = CTVertex(name = 'V_688',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_806_1178,(0,0,2):C.UVGC_806_1179,(0,0,1):C.UVGC_806_1180})

V_689 = CTVertex(name = 'V_689',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_622_804,(0,0,2):C.UVGC_622_805,(0,0,1):C.UVGC_622_806,(0,1,0):C.UVGC_625_813,(0,1,2):C.UVGC_625_814,(0,1,1):C.UVGC_625_815})

V_690 = CTVertex(name = 'V_690',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.s__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_807_1181,(0,0,2):C.UVGC_807_1182,(0,0,1):C.UVGC_807_1183})

V_691 = CTVertex(name = 'V_691',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.c__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_623_807,(0,0,2):C.UVGC_623_808,(0,0,1):C.UVGC_623_809,(0,1,0):C.UVGC_626_816,(0,1,2):C.UVGC_626_817,(0,1,1):C.UVGC_626_818})

V_692 = CTVertex(name = 'V_692',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.e__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_850_1291,(0,0,2):C.UVGC_850_1292,(0,0,1):C.UVGC_850_1293,(0,1,0):C.UVGC_853_1300,(0,1,2):C.UVGC_853_1301,(0,1,1):C.UVGC_853_1302})

V_693 = CTVertex(name = 'V_693',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_579_675,(0,0,2):C.UVGC_579_676,(0,0,1):C.UVGC_579_677})

V_694 = CTVertex(name = 'V_694',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.mu__plus__, P.LQ1d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_851_1294,(0,0,2):C.UVGC_851_1295,(0,0,1):C.UVGC_851_1296,(0,1,0):C.UVGC_854_1303,(0,1,2):C.UVGC_854_1304,(0,1,1):C.UVGC_854_1305})

V_695 = CTVertex(name = 'V_695',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_580_678,(0,0,2):C.UVGC_580_679,(0,0,1):C.UVGC_580_680})

V_696 = CTVertex(name = 'V_696',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ1d] ], [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_581_681,(0,0,2):C.UVGC_581_682,(0,0,1):C.UVGC_581_683})

V_697 = CTVertex(name = 'V_697',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_852_1297,(0,0,2):C.UVGC_852_1298,(0,0,1):C.UVGC_852_1299,(0,1,0):C.UVGC_855_1306,(0,1,2):C.UVGC_855_1307,(0,1,1):C.UVGC_855_1308})

V_698 = CTVertex(name = 'V_698',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_688_1002,(0,0,2):C.UVGC_688_1003,(0,0,1):C.UVGC_688_1004})

V_699 = CTVertex(name = 'V_699',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_689_1005,(0,0,2):C.UVGC_689_1006,(0,0,1):C.UVGC_689_1007})

V_700 = CTVertex(name = 'V_700',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_690_1008,(0,0,2):C.UVGC_690_1009,(0,0,1):C.UVGC_690_1010})

V_701 = CTVertex(name = 'V_701',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_829_1247,(0,0,2):C.UVGC_829_1248,(0,0,1):C.UVGC_639_857})

V_702 = CTVertex(name = 'V_702',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_830_1249,(0,0,2):C.UVGC_830_1250,(0,0,1):C.UVGC_640_860})

V_703 = CTVertex(name = 'V_703',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_831_1251,(0,0,2):C.UVGC_831_1252,(0,0,1):C.UVGC_641_863})

V_704 = CTVertex(name = 'V_704',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.e__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_603_747,(0,0,2):C.UVGC_603_748,(0,0,1):C.UVGC_603_749})

V_705 = CTVertex(name = 'V_705',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.mu__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_604_750,(0,0,2):C.UVGC_604_751,(0,0,1):C.UVGC_604_752})

V_706 = CTVertex(name = 'V_706',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ta__minus__, P.LQ2u ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_605_753,(0,0,2):C.UVGC_605_754,(0,0,1):C.UVGC_605_755})

V_707 = CTVertex(name = 'V_707',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_905_1437,(0,0,2):C.UVGC_905_1438,(0,0,1):C.UVGC_905_1439})

V_708 = CTVertex(name = 'V_708',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_906_1440,(0,0,2):C.UVGC_906_1441,(0,0,1):C.UVGC_906_1442})

V_709 = CTVertex(name = 'V_709',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.u, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_907_1443,(0,0,2):C.UVGC_907_1444,(0,0,1):C.UVGC_907_1445})

V_710 = CTVertex(name = 'V_710',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_633_837,(0,0,2):C.UVGC_633_838,(0,0,1):C.UVGC_633_839})

V_711 = CTVertex(name = 'V_711',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_634_840,(0,0,2):C.UVGC_634_841,(0,0,1):C.UVGC_634_842})

V_712 = CTVertex(name = 'V_712',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.c, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ2u] ], [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_635_843,(0,0,2):C.UVGC_635_844,(0,0,1):C.UVGC_635_845})

V_713 = CTVertex(name = 'V_713',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_862_1327,(0,0,2):C.UVGC_862_1328,(0,0,1):C.UVGC_862_1329})

V_714 = CTVertex(name = 'V_714',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_863_1330,(0,0,2):C.UVGC_863_1331,(0,0,1):C.UVGC_863_1332})

V_715 = CTVertex(name = 'V_715',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.t, P.LQ2u__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_864_1333,(0,0,2):C.UVGC_864_1334,(0,0,1):C.UVGC_864_1335})

V_716 = CTVertex(name = 'V_716',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_700_1038,(0,0,2):C.UVGC_700_1039,(0,0,1):C.UVGC_700_1040})

V_717 = CTVertex(name = 'V_717',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_694_1020,(0,0,2):C.UVGC_694_1021,(0,0,1):C.UVGC_694_1022})

V_718 = CTVertex(name = 'V_718',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_929_1491,(0,0,2):C.UVGC_929_1492,(0,0,1):C.UVGC_929_1493})

V_719 = CTVertex(name = 'V_719',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_694_1021,(0,0,2):C.UVGC_923_1485,(0,0,1):C.UVGC_694_1022})

V_720 = CTVertex(name = 'V_720',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_701_1041,(0,0,2):C.UVGC_701_1042,(0,0,1):C.UVGC_701_1043})

V_721 = CTVertex(name = 'V_721',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_695_1023,(0,0,2):C.UVGC_695_1024,(0,0,1):C.UVGC_695_1025})

V_722 = CTVertex(name = 'V_722',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_930_1494,(0,0,2):C.UVGC_930_1495,(0,0,1):C.UVGC_930_1496})

V_723 = CTVertex(name = 'V_723',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.mu__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_695_1024,(0,0,2):C.UVGC_924_1486,(0,0,1):C.UVGC_695_1025})

V_724 = CTVertex(name = 'V_724',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_702_1044,(0,0,2):C.UVGC_702_1045,(0,0,1):C.UVGC_702_1046})

V_725 = CTVertex(name = 'V_725',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_696_1026,(0,0,2):C.UVGC_696_1027,(0,0,1):C.UVGC_696_1028})

V_726 = CTVertex(name = 'V_726',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_931_1497,(0,0,2):C.UVGC_931_1498,(0,0,1):C.UVGC_931_1499})

V_727 = CTVertex(name = 'V_727',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ta__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_696_1027,(0,0,2):C.UVGC_925_1487,(0,0,1):C.UVGC_696_1028})

V_728 = CTVertex(name = 'V_728',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_651_892,(0,0,2):C.UVGC_835_1259,(0,0,1):C.UVGC_651_893})

V_729 = CTVertex(name = 'V_729',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_657_909,(0,0,2):C.UVGC_657_910,(0,0,1):C.UVGC_657_911})

V_730 = CTVertex(name = 'V_730',
                 type = 'UV',
                 particles = [ P.e__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_651_891,(0,0,2):C.UVGC_651_892,(0,0,1):C.UVGC_651_893})

V_731 = CTVertex(name = 'V_731',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.e__plus__, P.LQ3dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_841_1265,(0,0,2):C.UVGC_841_1266,(0,0,1):C.UVGC_841_1267})

V_732 = CTVertex(name = 'V_732',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_652_895,(0,0,2):C.UVGC_836_1260,(0,0,1):C.UVGC_652_896})

V_733 = CTVertex(name = 'V_733',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_658_912,(0,0,2):C.UVGC_658_913,(0,0,1):C.UVGC_658_914})

V_734 = CTVertex(name = 'V_734',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_652_894,(0,0,2):C.UVGC_652_895,(0,0,1):C.UVGC_652_896})

V_735 = CTVertex(name = 'V_735',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__plus__, P.LQ3dd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_842_1268,(0,0,2):C.UVGC_842_1269,(0,0,1):C.UVGC_842_1270})

V_736 = CTVertex(name = 'V_736',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.s__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_843_1271,(0,0,2):C.UVGC_843_1272,(0,0,1):C.UVGC_843_1273})

V_737 = CTVertex(name = 'V_737',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.s__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_653_898,(0,0,2):C.UVGC_837_1261,(0,0,1):C.UVGC_653_899})

V_738 = CTVertex(name = 'V_738',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.c__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_659_915,(0,0,2):C.UVGC_659_916,(0,0,1):C.UVGC_659_917})

V_739 = CTVertex(name = 'V_739',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.c__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_653_897,(0,0,2):C.UVGC_653_898,(0,0,1):C.UVGC_653_899})

V_740 = CTVertex(name = 'V_740',
                 type = 'UV',
                 particles = [ P.e__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_615_783,(0,0,2):C.UVGC_615_784,(0,0,1):C.UVGC_615_785})

V_741 = CTVertex(name = 'V_741',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_609_765,(0,0,2):C.UVGC_609_766,(0,0,1):C.UVGC_609_767})

V_742 = CTVertex(name = 'V_742',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_886_1381,(0,0,2):C.UVGC_886_1382,(0,0,1):C.UVGC_886_1383})

V_743 = CTVertex(name = 'V_743',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.e__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_609_766,(0,0,2):C.UVGC_880_1375,(0,0,1):C.UVGC_609_767})

V_744 = CTVertex(name = 'V_744',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_616_786,(0,0,2):C.UVGC_616_787,(0,0,1):C.UVGC_616_788})

V_745 = CTVertex(name = 'V_745',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_610_768,(0,0,2):C.UVGC_610_769,(0,0,1):C.UVGC_610_770})

V_746 = CTVertex(name = 'V_746',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_887_1384,(0,0,2):C.UVGC_887_1385,(0,0,1):C.UVGC_887_1386})

V_747 = CTVertex(name = 'V_747',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.mu__plus__, P.LQ3d ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_610_769,(0,0,2):C.UVGC_881_1376,(0,0,1):C.UVGC_610_770})

V_748 = CTVertex(name = 'V_748',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_617_789,(0,0,2):C.UVGC_617_790,(0,0,1):C.UVGC_617_791})

V_749 = CTVertex(name = 'V_749',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.LQ3d] ], [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_611_771,(0,0,2):C.UVGC_611_772,(0,0,1):C.UVGC_611_773})

V_750 = CTVertex(name = 'V_750',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.t__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_888_1387,(0,0,2):C.UVGC_888_1388,(0,0,1):C.UVGC_888_1389})

V_751 = CTVertex(name = 'V_751',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_611_772,(0,0,2):C.UVGC_882_1377,(0,0,1):C.UVGC_611_773})

V_752 = CTVertex(name = 'V_752',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_849_1288,(0,0,2):C.UVGC_849_1289,(0,0,1):C.UVGC_849_1290})

V_753 = CTVertex(name = 'V_753',
                 type = 'UV',
                 particles = [ P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_453_387})

V_754 = CTVertex(name = 'V_754',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_454_388})

V_755 = CTVertex(name = 'V_755',
                 type = 'UV',
                 particles = [ P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_466_433})

V_756 = CTVertex(name = 'V_756',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_467_434})

V_757 = CTVertex(name = 'V_757',
                 type = 'UV',
                 particles = [ P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_478_460})

V_758 = CTVertex(name = 'V_758',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_479_461})

V_759 = CTVertex(name = 'V_759',
                 type = 'UV',
                 particles = [ P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_490_504})

V_760 = CTVertex(name = 'V_760',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_491_505})

V_761 = CTVertex(name = 'V_761',
                 type = 'UV',
                 particles = [ P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_502_531})

V_762 = CTVertex(name = 'V_762',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_503_532})

V_763 = CTVertex(name = 'V_763',
                 type = 'UV',
                 particles = [ P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_518_580})

V_764 = CTVertex(name = 'V_764',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_519_581})

V_765 = CTVertex(name = 'V_765',
                 type = 'UV',
                 particles = [ P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_530_607})

V_766 = CTVertex(name = 'V_766',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_531_608})

V_767 = CTVertex(name = 'V_767',
                 type = 'UV',
                 particles = [ P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_542_617})

V_768 = CTVertex(name = 'V_768',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_543_618})

V_769 = CTVertex(name = 'V_769',
                 type = 'UV',
                 particles = [ P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VSS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_557_648})

V_770 = CTVertex(name = 'V_770',
                 type = 'UV',
                 particles = [ P.a, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_558_649})

V_771 = CTVertex(name = 'V_771',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_455_389,(0,0,1):C.UVGC_455_390,(0,0,2):C.UVGC_455_391,(0,0,3):C.UVGC_455_392,(0,0,4):C.UVGC_455_393,(0,0,6):C.UVGC_455_394,(0,0,7):C.UVGC_455_395,(0,0,8):C.UVGC_455_396,(0,0,9):C.UVGC_455_397,(0,0,10):C.UVGC_455_398,(0,0,11):C.UVGC_455_399,(0,0,12):C.UVGC_455_400,(0,0,13):C.UVGC_455_401,(0,0,14):C.UVGC_455_402,(0,0,15):C.UVGC_455_403,(0,0,16):C.UVGC_455_404,(0,0,17):C.UVGC_455_405,(0,0,5):C.UVGC_455_406})

V_772 = CTVertex(name = 'V_772',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ1dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_468_435,(0,0,1):C.UVGC_468_436,(0,0,2):C.UVGC_468_437,(0,0,3):C.UVGC_468_438,(0,0,4):C.UVGC_468_439,(0,0,6):C.UVGC_468_440,(0,0,7):C.UVGC_468_441,(0,0,8):C.UVGC_468_442,(0,0,9):C.UVGC_468_443,(0,0,10):C.UVGC_468_444,(0,0,11):C.UVGC_468_445,(0,0,12):C.UVGC_468_446,(0,0,13):C.UVGC_468_447,(0,0,14):C.UVGC_468_448,(0,0,15):C.UVGC_468_449,(0,0,16):C.UVGC_468_450,(0,0,17):C.UVGC_468_451,(0,0,5):C.UVGC_468_452})

V_773 = CTVertex(name = 'V_773',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_504_533,(0,0,1):C.UVGC_504_534,(0,0,2):C.UVGC_504_535,(0,0,3):C.UVGC_504_536,(0,0,4):C.UVGC_504_537,(0,0,6):C.UVGC_504_538,(0,0,7):C.UVGC_504_539,(0,0,8):C.UVGC_504_540,(0,0,9):C.UVGC_504_541,(0,0,10):C.UVGC_504_542,(0,0,11):C.UVGC_504_543,(0,0,12):C.UVGC_504_544,(0,0,13):C.UVGC_504_545,(0,0,14):C.UVGC_504_546,(0,0,15):C.UVGC_504_547,(0,0,16):C.UVGC_504_548,(0,0,17):C.UVGC_504_549,(0,0,5):C.UVGC_504_550})

V_774 = CTVertex(name = 'V_774',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2uu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_520_582,(0,0,1):C.UVGC_520_583,(0,0,2):C.UVGC_520_584,(0,0,3):C.UVGC_520_585,(0,0,4):C.UVGC_520_586,(0,0,6):C.UVGC_520_587,(0,0,7):C.UVGC_520_588,(0,0,8):C.UVGC_520_589,(0,0,9):C.UVGC_520_590,(0,0,10):C.UVGC_520_591,(0,0,11):C.UVGC_520_592,(0,0,12):C.UVGC_520_593,(0,0,13):C.UVGC_520_594,(0,0,14):C.UVGC_520_595,(0,0,15):C.UVGC_520_596,(0,0,16):C.UVGC_520_597,(0,0,17):C.UVGC_520_598,(0,0,5):C.UVGC_520_599})

V_775 = CTVertex(name = 'V_775',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_480_462,(0,0,1):C.UVGC_480_463,(0,0,2):C.UVGC_480_464,(0,0,3):C.UVGC_480_465,(0,0,4):C.UVGC_480_466,(0,0,6):C.UVGC_480_467,(0,0,7):C.UVGC_480_468,(0,0,8):C.UVGC_480_469,(0,0,9):C.UVGC_480_470,(0,0,10):C.UVGC_480_471,(0,0,11):C.UVGC_480_472,(0,0,12):C.UVGC_480_473,(0,0,13):C.UVGC_480_474,(0,0,14):C.UVGC_480_475,(0,0,15):C.UVGC_480_476,(0,0,16):C.UVGC_480_477,(0,0,17):C.UVGC_480_478,(0,0,5):C.UVGC_480_479})

V_776 = CTVertex(name = 'V_776',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ2pu] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_492_506,(0,0,1):C.UVGC_492_507,(0,0,2):C.UVGC_492_508,(0,0,3):C.UVGC_492_509,(0,0,4):C.UVGC_492_510,(0,0,6):C.UVGC_492_511,(0,0,7):C.UVGC_492_512,(0,0,8):C.UVGC_492_513,(0,0,9):C.UVGC_492_514,(0,0,10):C.UVGC_492_515,(0,0,11):C.UVGC_492_516,(0,0,12):C.UVGC_492_517,(0,0,13):C.UVGC_492_518,(0,0,14):C.UVGC_492_519,(0,0,15):C.UVGC_492_520,(0,0,16):C.UVGC_492_521,(0,0,17):C.UVGC_492_522,(0,0,5):C.UVGC_492_523})

V_777 = CTVertex(name = 'V_777',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3d] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_455_389,(0,0,1):C.UVGC_455_390,(0,0,2):C.UVGC_455_391,(0,0,3):C.UVGC_455_392,(0,0,4):C.UVGC_455_393,(0,0,6):C.UVGC_455_394,(0,0,7):C.UVGC_455_395,(0,0,8):C.UVGC_455_396,(0,0,9):C.UVGC_455_397,(0,0,10):C.UVGC_455_398,(0,0,11):C.UVGC_455_399,(0,0,12):C.UVGC_455_400,(0,0,13):C.UVGC_455_401,(0,0,14):C.UVGC_455_402,(0,0,15):C.UVGC_455_403,(0,0,16):C.UVGC_455_404,(0,0,17):C.UVGC_455_405,(0,0,5):C.UVGC_532_609})

V_778 = CTVertex(name = 'V_778',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3dd] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_544_619,(0,0,1):C.UVGC_544_620,(0,0,2):C.UVGC_544_621,(0,0,3):C.UVGC_544_622,(0,0,4):C.UVGC_544_623,(0,0,6):C.UVGC_544_624,(0,0,7):C.UVGC_544_625,(0,0,8):C.UVGC_544_626,(0,0,9):C.UVGC_544_627,(0,0,10):C.UVGC_544_628,(0,0,11):C.UVGC_544_629,(0,0,12):C.UVGC_544_630,(0,0,13):C.UVGC_544_631,(0,0,14):C.UVGC_544_632,(0,0,15):C.UVGC_544_633,(0,0,16):C.UVGC_544_634,(0,0,17):C.UVGC_544_635,(0,0,5):C.UVGC_544_636})

V_779 = CTVertex(name = 'V_779',
                 type = 'UV',
                 particles = [ P.g, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'T(1,4,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.LQ3u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_559_650,(0,0,1):C.UVGC_559_651,(0,0,2):C.UVGC_559_652,(0,0,3):C.UVGC_559_653,(0,0,4):C.UVGC_559_654,(0,0,6):C.UVGC_559_655,(0,0,7):C.UVGC_559_656,(0,0,8):C.UVGC_559_657,(0,0,9):C.UVGC_559_658,(0,0,10):C.UVGC_559_659,(0,0,11):C.UVGC_559_660,(0,0,12):C.UVGC_559_661,(0,0,13):C.UVGC_559_662,(0,0,14):C.UVGC_559_663,(0,0,15):C.UVGC_559_664,(0,0,16):C.UVGC_559_665,(0,0,17):C.UVGC_559_666,(0,0,5):C.UVGC_559_667})

V_780 = CTVertex(name = 'V_780',
                 type = 'UV',
                 particles = [ P.W__minus__, P.Z, P.LQ2d__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_742_1062,(0,0,2):C.UVGC_742_1063,(0,0,1):C.UVGC_742_1064})

V_781 = CTVertex(name = 'V_781',
                 type = 'UV',
                 particles = [ P.W__minus__, P.Z, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_771_1096,(0,0,2):C.UVGC_771_1097,(0,0,1):C.UVGC_771_1098})

V_782 = CTVertex(name = 'V_782',
                 type = 'UV',
                 particles = [ P.W__minus__, P.Z, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_789_1113,(0,0,1):C.UVGC_789_1114,(0,0,2):C.UVGC_789_1115})

V_783 = CTVertex(name = 'V_783',
                 type = 'UV',
                 particles = [ P.W__minus__, P.Z, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_798_1147,(0,0,2):C.UVGC_798_1148,(0,0,1):C.UVGC_798_1149})

V_784 = CTVertex(name = 'V_784',
                 type = 'UV',
                 particles = [ P.W__plus__, P.Z, P.LQ2d, P.LQ2pu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_742_1062,(0,0,2):C.UVGC_742_1063,(0,0,1):C.UVGC_742_1064})

V_785 = CTVertex(name = 'V_785',
                 type = 'UV',
                 particles = [ P.W__plus__, P.Z, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_771_1096,(0,0,2):C.UVGC_771_1097,(0,0,1):C.UVGC_771_1098})

V_786 = CTVertex(name = 'V_786',
                 type = 'UV',
                 particles = [ P.W__plus__, P.Z, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_789_1113,(0,0,1):C.UVGC_789_1114,(0,0,2):C.UVGC_789_1115})

V_787 = CTVertex(name = 'V_787',
                 type = 'UV',
                 particles = [ P.W__plus__, P.Z, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_798_1147,(0,0,2):C.UVGC_798_1148,(0,0,1):C.UVGC_798_1149})

V_788 = CTVertex(name = 'V_788',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_456_407})

V_789 = CTVertex(name = 'V_789',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_469_453})

V_790 = CTVertex(name = 'V_790',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ] ],
                 couplings = {(0,0,0):C.UVGC_481_480})

V_791 = CTVertex(name = 'V_791',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ] ],
                 couplings = {(0,0,0):C.UVGC_493_524})

V_792 = CTVertex(name = 'V_792',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ] ],
                 couplings = {(0,0,0):C.UVGC_505_551})

V_793 = CTVertex(name = 'V_793',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_521_600})

V_794 = CTVertex(name = 'V_794',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ] ],
                 couplings = {(0,0,0):C.UVGC_533_610})

V_795 = CTVertex(name = 'V_795',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ] ],
                 couplings = {(0,0,0):C.UVGC_545_637})

V_796 = CTVertex(name = 'V_796',
                 type = 'UV',
                 particles = [ P.Z, P.Z, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_560_668})

V_797 = CTVertex(name = 'V_797',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_435_322,(0,1,0):C.UVGC_407_170,(0,2,0):C.UVGC_409_172})

V_798 = CTVertex(name = 'V_798',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_435_322,(0,1,0):C.UVGC_365_138,(0,2,0):C.UVGC_367_140})

V_799 = CTVertex(name = 'V_799',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_435_322,(0,1,0):C.UVGC_400_163,(0,2,0):C.UVGC_402_165})

V_800 = CTVertex(name = 'V_800',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_430_318,(0,1,0):C.UVGC_372_145,(0,2,0):C.UVGC_374_147})

V_801 = CTVertex(name = 'V_801',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_430_318,(0,1,0):C.UVGC_393_156,(0,2,0):C.UVGC_395_158})

V_802 = CTVertex(name = 'V_802',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_430_318,(0,1,0):C.UVGC_358_131,(0,2,0):C.UVGC_360_133})

V_803 = CTVertex(name = 'V_803',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV7 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_577_674,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_412_175})

V_804 = CTVertex(name = 'V_804',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV7 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_437_323,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_370_143})

V_805 = CTVertex(name = 'V_805',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV7 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_570_671,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_405_168})

V_806 = CTVertex(name = 'V_806',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_442_324,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_377_150})

V_807 = CTVertex(name = 'V_807',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_564_669,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_398_161})

V_808 = CTVertex(name = 'V_808',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_432_320,(0,2,0):C.UVGC_433_321,(0,1,0):C.UVGC_363_136})

V_809 = CTVertex(name = 'V_809',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,5):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,1):C.UVGC_418_185,(0,3,2):C.UVGC_418_186,(0,3,3):C.UVGC_418_187,(0,3,4):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,5):C.UVGC_410_173,(0,2,5):C.UVGC_411_174})

V_810 = CTVertex(name = 'V_810',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.c, P.g] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,2):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,1):C.UVGC_418_185,(0,3,3):C.UVGC_418_186,(0,3,4):C.UVGC_418_187,(0,3,5):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,2):C.UVGC_368_141,(0,2,2):C.UVGC_369_142})

V_811 = CTVertex(name = 'V_811',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,5):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,1):C.UVGC_418_185,(0,3,2):C.UVGC_418_186,(0,3,3):C.UVGC_418_187,(0,3,4):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,5):C.UVGC_403_166,(0,2,5):C.UVGC_404_167})

V_812 = CTVertex(name = 'V_812',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,3):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,1):C.UVGC_418_185,(0,3,2):C.UVGC_418_186,(0,3,4):C.UVGC_418_187,(0,3,5):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,3):C.UVGC_375_148,(0,2,3):C.UVGC_376_149})

V_813 = CTVertex(name = 'V_813',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,5):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,1):C.UVGC_418_185,(0,3,2):C.UVGC_418_186,(0,3,3):C.UVGC_418_187,(0,3,4):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,5):C.UVGC_396_159,(0,2,5):C.UVGC_397_160})

V_814 = CTVertex(name = 'V_814',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,1):C.UVGC_431_319,(0,3,0):C.UVGC_418_184,(0,3,2):C.UVGC_418_185,(0,3,3):C.UVGC_418_186,(0,3,4):C.UVGC_418_187,(0,3,5):C.UVGC_418_188,(0,3,6):C.UVGC_418_189,(0,3,7):C.UVGC_418_190,(0,3,8):C.UVGC_418_191,(0,3,9):C.UVGC_418_192,(0,3,10):C.UVGC_418_193,(0,3,11):C.UVGC_418_194,(0,3,12):C.UVGC_418_195,(0,3,13):C.UVGC_418_196,(0,3,14):C.UVGC_418_197,(0,3,15):C.UVGC_418_198,(0,3,16):C.UVGC_418_199,(0,3,17):C.UVGC_418_200,(0,1,1):C.UVGC_361_134,(0,2,1):C.UVGC_362_135})

V_815 = CTVertex(name = 'V_815',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_892_1399,(0,0,2):C.UVGC_892_1400,(0,0,1):C.UVGC_663_929})

V_816 = CTVertex(name = 'V_816',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_663_927,(0,0,2):C.UVGC_663_928,(0,0,1):C.UVGC_663_929})

V_817 = CTVertex(name = 'V_817',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_847_1283,(0,0,2):C.UVGC_847_1284,(0,0,1):C.UVGC_663_929})

V_818 = CTVertex(name = 'V_818',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_892_1399,(0,0,2):C.UVGC_892_1400,(0,0,1):C.UVGC_663_929})

V_819 = CTVertex(name = 'V_819',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_663_927,(0,0,2):C.UVGC_663_928,(0,0,1):C.UVGC_663_929})

V_820 = CTVertex(name = 'V_820',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_847_1283,(0,0,2):C.UVGC_847_1284,(0,0,1):C.UVGC_663_929})

V_821 = CTVertex(name = 'V_821',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_429_317,(0,1,0):C.UVGC_406_169,(0,2,0):C.UVGC_408_171})

V_822 = CTVertex(name = 'V_822',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_429_317,(0,1,0):C.UVGC_364_137,(0,2,0):C.UVGC_366_139})

V_823 = CTVertex(name = 'V_823',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_569_670,(0,3,0):C.UVGC_429_317,(0,0,0):C.UVGC_399_162,(0,2,0):C.UVGC_401_164})

V_824 = CTVertex(name = 'V_824',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_429_317,(0,1,0):C.UVGC_371_144,(0,2,0):C.UVGC_373_146})

V_825 = CTVertex(name = 'V_825',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_429_317,(0,1,0):C.UVGC_392_155,(0,2,0):C.UVGC_394_157})

V_826 = CTVertex(name = 'V_826',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_429_317,(0,1,0):C.UVGC_357_130,(0,2,0):C.UVGC_359_132})

V_827 = CTVertex(name = 'V_827',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ] ],
                 couplings = {(0,0,0):C.UVGC_452_386,(0,1,0):C.UVGC_444_325})

V_828 = CTVertex(name = 'V_828',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1dd] ] ],
                 couplings = {(0,0,0):C.UVGC_465_431,(0,0,1):C.UVGC_465_432,(0,1,1):C.UVGC_457_408})

V_829 = CTVertex(name = 'V_829',
                 type = 'UV',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_517_579,(0,1,0):C.UVGC_509_556})

V_830 = CTVertex(name = 'V_830',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_508_555,(0,0,1):C.UVGC_506_553,(0,1,0):C.UVGC_494_525})

V_831 = CTVertex(name = 'V_831',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_507_554,(0,0,1):C.UVGC_506_553,(0,1,0):C.UVGC_482_481})

V_832 = CTVertex(name = 'V_832',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2uu] ] ],
                 couplings = {(0,0,0):C.UVGC_506_552,(0,0,1):C.UVGC_506_553,(0,1,0):C.UVGC_470_454})

V_833 = CTVertex(name = 'V_833',
                 type = 'UV',
                 particles = [ P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_556_647,(0,1,0):C.UVGC_548_641})

V_834 = CTVertex(name = 'V_834',
                 type = 'UV',
                 particles = [ P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_546_638,(0,0,1):C.UVGC_546_639,(0,1,0):C.UVGC_522_601})

V_835 = CTVertex(name = 'V_835',
                 type = 'UV',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3u] ] ],
                 couplings = {(0,0,0):C.UVGC_547_640,(0,0,1):C.UVGC_546_639,(0,1,0):C.UVGC_534_611})

V_836 = CTVertex(name = 'V_836',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV4, L.VV5, L.VV6 ],
                 loop_particles = [ [ [P.b] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.LQ1d] ], [ [P.LQ1dd] ], [ [P.LQ2d] ], [ [P.LQ2pu] ], [ [P.LQ2u] ], [ [P.LQ2uu] ], [ [P.LQ3d] ], [ [P.LQ3dd] ], [ [P.LQ3u] ], [ [P.s] ], [ [P.t] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_421_235,(0,0,1):C.UVGC_421_236,(0,0,2):C.UVGC_421_237,(0,0,3):C.UVGC_421_238,(0,0,4):C.UVGC_421_239,(0,0,5):C.UVGC_421_240,(0,0,6):C.UVGC_421_241,(0,0,7):C.UVGC_421_242,(0,0,8):C.UVGC_421_243,(0,0,9):C.UVGC_421_244,(0,0,10):C.UVGC_421_245,(0,0,11):C.UVGC_421_246,(0,0,12):C.UVGC_421_247,(0,0,13):C.UVGC_421_248,(0,0,14):C.UVGC_421_249,(0,0,15):C.UVGC_421_250,(0,0,16):C.UVGC_421_251,(0,1,3):C.UVGC_355_128,(0,2,4):C.UVGC_356_129})

V_837 = CTVertex(name = 'V_837',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d__tilde__, P.LQ1d, P.LQ1d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d] ], [ [P.g] ], [ [P.g, P.LQ1d] ], [ [P.g, P.LQ1d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1000_1,(1,0,5):C.UVGC_935_1509,(1,0,1):C.UVGC_1000_3,(1,0,4):C.UVGC_935_1510,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1000_1,(0,0,5):C.UVGC_935_1509,(0,0,1):C.UVGC_1000_3,(0,0,4):C.UVGC_935_1510})

V_838 = CTVertex(name = 'V_838',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ1dd__tilde__, P.LQ1dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ1dd] ], [ [P.a, P.g, P.LQ1d, P.LQ1dd] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ1dd] ], [ [P.g, P.LQ1d, P.LQ1dd] ], [ [P.g, P.LQ1d, P.LQ1dd, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ1dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_957_1551,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_957_1552,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_957_1553,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_956_1548,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_956_1549,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_956_1550})

V_839 = CTVertex(name = 'V_839',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd__tilde__, P.LQ1dd, P.LQ1dd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd] ], [ [P.g] ], [ [P.g, P.LQ1dd] ], [ [P.g, P.LQ1dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_936_1511,(1,0,5):C.UVGC_936_1512,(1,0,1):C.UVGC_936_1513,(1,0,4):C.UVGC_936_1514,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_936_1511,(0,0,5):C.UVGC_936_1512,(0,0,1):C.UVGC_936_1513,(0,0,4):C.UVGC_936_1514})

V_840 = CTVertex(name = 'V_840',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ1d, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2uu] ], [ [P.g, P.LQ1d, P.LQ2uu] ], [ [P.g, P.LQ1d, P.LQ2uu, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1011_53,(1,0,8):C.UVGC_1017_86,(1,0,1):C.UVGC_1011_55,(1,0,7):C.UVGC_1017_87,(1,0,2):C.UVGC_1011_57,(1,0,6):C.UVGC_1017_88,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1010_47,(0,0,8):C.UVGC_1016_83,(0,0,1):C.UVGC_1010_49,(0,0,7):C.UVGC_1016_84,(0,0,2):C.UVGC_1010_51,(0,0,6):C.UVGC_1016_85})

V_841 = CTVertex(name = 'V_841',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ1dd, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2uu] ], [ [P.g, P.LQ1dd, P.LQ2uu] ], [ [P.g, P.LQ1dd, P.LQ2uu, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1019_94,(1,0,8):C.UVGC_979_1588,(1,0,1):C.UVGC_1019_96,(1,0,7):C.UVGC_979_1589,(1,0,2):C.UVGC_1011_53,(1,0,6):C.UVGC_1017_86,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1018_89,(0,0,8):C.UVGC_978_1586,(0,0,1):C.UVGC_1018_91,(0,0,7):C.UVGC_978_1587,(0,0,2):C.UVGC_1010_47,(0,0,6):C.UVGC_1016_83})

V_842 = CTVertex(name = 'V_842',
                 type = 'UV',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu__tilde__, P.LQ2uu, P.LQ2uu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2uu] ], [ [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_940_1521,(1,0,5):C.UVGC_940_1522,(1,0,1):C.UVGC_940_1523,(1,0,4):C.UVGC_940_1524,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_940_1521,(0,0,5):C.UVGC_940_1522,(0,0,1):C.UVGC_940_1523,(0,0,4):C.UVGC_940_1524})

V_843 = CTVertex(name = 'V_843',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ1d, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2u] ], [ [P.g, P.LQ1d, P.LQ2u] ], [ [P.g, P.LQ1d, P.LQ2u, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_1005_28,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_1005_29,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1005_30,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_1004_25,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_1004_26,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1004_27})

V_844 = CTVertex(name = 'V_844',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ1dd, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2u] ], [ [P.g, P.LQ1dd, P.LQ2u] ], [ [P.g, P.LQ1dd, P.LQ2u, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_977_1584,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_977_1585,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_1005_28,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_976_1582,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_976_1583,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_1004_25})

V_845 = CTVertex(name = 'V_845',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2u, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu] ], [ [P.g, P.LQ2u, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2u, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1013_65,(1,0,10):C.UVGC_1015_77,(1,0,11):C.UVGC_1015_78,(1,0,1):C.UVGC_1013_67,(1,0,8):C.UVGC_1015_79,(1,0,9):C.UVGC_1015_80,(1,0,2):C.UVGC_1013_69,(1,0,6):C.UVGC_1015_81,(1,0,7):C.UVGC_1015_82,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1012_59,(0,0,10):C.UVGC_1014_71,(0,0,11):C.UVGC_1014_72,(0,0,1):C.UVGC_1012_61,(0,0,8):C.UVGC_1014_73,(0,0,9):C.UVGC_1014_74,(0,0,2):C.UVGC_1012_63,(0,0,6):C.UVGC_1014_75,(0,0,7):C.UVGC_1014_76})

V_846 = CTVertex(name = 'V_846',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u__tilde__, P.LQ2u, P.LQ2u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2u] ], [ [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1006_31,(1,0,5):C.UVGC_939_1519,(1,0,1):C.UVGC_1006_33,(1,0,4):C.UVGC_939_1520,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1006_31,(0,0,5):C.UVGC_939_1519,(0,0,1):C.UVGC_1006_33,(0,0,4):C.UVGC_939_1520})

V_847 = CTVertex(name = 'V_847',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ1d, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ1d, P.LQ2pu] ], [ [P.g, P.LQ1d, P.LQ2pu, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_961_1567,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_961_1568,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_961_1569,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_960_1564,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_960_1565,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_960_1566})

V_848 = CTVertex(name = 'V_848',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ1dd, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2pu] ], [ [P.g, P.LQ1dd, P.LQ2pu] ], [ [P.g, P.LQ1dd, P.LQ2pu, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_975_1580,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_975_1581,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_961_1567,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_974_1578,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_974_1579,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_960_1564})

V_849 = CTVertex(name = 'V_849',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2pu, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2pu, P.LQ2uu] ], [ [P.g, P.LQ2pu, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1013_65,(1,0,8):C.UVGC_1013_66,(1,0,1):C.UVGC_1013_67,(1,0,7):C.UVGC_1013_68,(1,0,2):C.UVGC_1013_69,(1,0,6):C.UVGC_1013_70,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1012_59,(0,0,8):C.UVGC_1012_60,(0,0,1):C.UVGC_1012_61,(0,0,7):C.UVGC_1012_62,(0,0,2):C.UVGC_1012_63,(0,0,6):C.UVGC_1012_64})

V_850 = CTVertex(name = 'V_850',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ2pu, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ2u] ], [ [P.g, P.LQ2pu, P.LQ2u] ], [ [P.g, P.LQ2pu, P.LQ2u, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_1003_20,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_1003_22,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_1003_24,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_1002_14,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_1002_16,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_1002_18})

V_851 = CTVertex(name = 'V_851',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu__tilde__, P.LQ2pu, P.LQ2pu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ2pu] ], [ [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1006_31,(1,0,5):C.UVGC_938_1517,(1,0,1):C.UVGC_1006_33,(1,0,4):C.UVGC_938_1518,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1006_31,(0,0,5):C.UVGC_938_1517,(0,0,1):C.UVGC_1006_33,(0,0,4):C.UVGC_938_1518})

V_852 = CTVertex(name = 'V_852',
                 type = 'UV',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ2u__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2u, P.W__plus__], [P.g, P.LQ2d, P.LQ2uu, P.W__plus__], [P.g, P.LQ2pu, P.LQ2u, P.W__plus__], [P.g, P.LQ2pu, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_1014_71,(1,0,1):C.UVGC_950_1547,(1,0,0):C.UVGC_1015_79,(0,0,2):C.UVGC_1015_77,(0,0,1):C.UVGC_949_1545,(0,0,0):C.UVGC_949_1546})

V_853 = CTVertex(name = 'V_853',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ2u, P.LQ2uu__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ2u, P.W__plus__], [P.g, P.LQ2d, P.LQ2uu, P.W__plus__], [P.g, P.LQ2pu, P.LQ2u, P.W__plus__], [P.g, P.LQ2pu, P.LQ2uu, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_1014_71,(1,0,1):C.UVGC_950_1547,(1,0,0):C.UVGC_1015_79,(0,0,2):C.UVGC_1015_77,(0,0,1):C.UVGC_949_1545,(0,0,0):C.UVGC_949_1546})

V_854 = CTVertex(name = 'V_854',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ2d] ], [ [P.a, P.g, P.LQ1d, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ2d] ], [ [P.g, P.LQ1d, P.LQ2d] ], [ [P.g, P.LQ1d, P.LQ2d, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_23,(1,0,8):C.UVGC_959_1559,(1,0,1):C.UVGC_959_1560,(1,0,7):C.UVGC_959_1561,(1,0,2):C.UVGC_959_1562,(1,0,6):C.UVGC_959_1563,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_17,(0,0,8):C.UVGC_958_1554,(0,0,1):C.UVGC_958_1555,(0,0,7):C.UVGC_958_1556,(0,0,2):C.UVGC_958_1557,(0,0,6):C.UVGC_958_1558})

V_855 = CTVertex(name = 'V_855',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ2d__tilde__, P.LQ2d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ2d] ], [ [P.a, P.g, P.LQ1dd, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ2d] ], [ [P.g, P.LQ1dd, P.LQ2d] ], [ [P.g, P.LQ1dd, P.LQ2d, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_973_1576,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_973_1577,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_959_1559,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_972_1574,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_972_1575,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_958_1554})

V_856 = CTVertex(name = 'V_856',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2uu__tilde__, P.LQ2uu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2uu] ], [ [P.a, P.g, P.LQ2d, P.LQ2uu] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2uu] ], [ [P.g, P.LQ2d, P.LQ2uu] ], [ [P.g, P.LQ2d, P.LQ2uu, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2uu, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1011_53,(1,0,8):C.UVGC_1011_54,(1,0,1):C.UVGC_1011_55,(1,0,7):C.UVGC_1011_56,(1,0,2):C.UVGC_1011_57,(1,0,6):C.UVGC_1011_58,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1010_47,(0,0,8):C.UVGC_1010_48,(0,0,1):C.UVGC_1010_49,(0,0,7):C.UVGC_1010_50,(0,0,2):C.UVGC_1010_51,(0,0,6):C.UVGC_1010_52})

V_857 = CTVertex(name = 'V_857',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2u__tilde__, P.LQ2u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2u] ], [ [P.a, P.g, P.LQ2d, P.LQ2u] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2u] ], [ [P.g, P.LQ2d, P.LQ2u] ], [ [P.g, P.LQ2d, P.LQ2u, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_1001_8,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_1001_10,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1001_12,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_1000_2,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_1000_4,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1000_6})

V_858 = CTVertex(name = 'V_858',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ2pu__tilde__, P.LQ2pu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ2pu] ], [ [P.a, P.g, P.LQ2d, P.LQ2pu] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu] ], [ [P.g, P.LQ2d, P.LQ2pu, P.W__plus__] ], [ [P.g, P.LQ2d, P.LQ2pu, P.Z] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ2pu, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,10):C.UVGC_1015_77,(1,0,11):C.UVGC_1027_127,(1,0,1):C.UVGC_1001_9,(1,0,8):C.UVGC_1015_79,(1,0,9):C.UVGC_993_1614,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1015_81,(1,0,7):C.UVGC_993_1615,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,10):C.UVGC_1014_71,(0,0,11):C.UVGC_1026_124,(0,0,1):C.UVGC_1000_3,(0,0,8):C.UVGC_1014_73,(0,0,9):C.UVGC_992_1612,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1014_75,(0,0,7):C.UVGC_992_1613})

V_859 = CTVertex(name = 'V_859',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d__tilde__, P.LQ2d, P.LQ2d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d] ], [ [P.g] ], [ [P.g, P.LQ2d] ], [ [P.g, P.LQ2d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1000_1,(1,0,5):C.UVGC_937_1515,(1,0,1):C.UVGC_1000_3,(1,0,4):C.UVGC_937_1516,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1000_1,(0,0,5):C.UVGC_937_1515,(0,0,1):C.UVGC_1000_3,(0,0,4):C.UVGC_937_1516})

V_860 = CTVertex(name = 'V_860',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ1d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3u] ], [ [P.g, P.LQ1d, P.LQ3u] ], [ [P.g, P.LQ1d, P.LQ3u, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_1025_119,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_1025_120,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1025_121,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_1024_116,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_1024_117,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1024_118})

V_861 = CTVertex(name = 'V_861',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ1dd, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3u] ], [ [P.g, P.LQ1dd, P.LQ3u] ], [ [P.g, P.LQ1dd, P.LQ3u, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_985_1600,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_985_1601,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_1025_119,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_984_1598,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_984_1599,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_1024_116})

V_862 = CTVertex(name = 'V_862',
                 type = 'UV',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2uu, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3u] ], [ [P.g, P.LQ2uu, P.LQ3u] ], [ [P.g, P.LQ2uu, P.LQ3u, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1013_65,(1,0,8):C.UVGC_1021_102,(1,0,1):C.UVGC_1013_67,(1,0,7):C.UVGC_1021_103,(1,0,2):C.UVGC_1013_69,(1,0,6):C.UVGC_1021_104,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1012_59,(0,0,8):C.UVGC_1020_99,(0,0,1):C.UVGC_1012_61,(0,0,7):C.UVGC_1020_100,(0,0,2):C.UVGC_1012_63,(0,0,6):C.UVGC_1020_101})

V_863 = CTVertex(name = 'V_863',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2u, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3u] ], [ [P.g, P.LQ2u, P.LQ3u] ], [ [P.g, P.LQ2u, P.LQ3u, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_1009_44,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_1009_45,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_1009_46,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_1008_41,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_1008_42,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_1008_43})

V_864 = CTVertex(name = 'V_864',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2pu, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3u] ], [ [P.g, P.LQ2pu, P.LQ3u] ], [ [P.g, P.LQ2pu, P.LQ3u, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_989_1605,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_989_1606,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_989_1607,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_988_1602,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_988_1603,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_988_1604})

V_865 = CTVertex(name = 'V_865',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ2d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3u] ], [ [P.g, P.LQ2d, P.LQ3u] ], [ [P.g, P.LQ2d, P.LQ3u, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_991_1609,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_991_1610,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_991_1611,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_937_1515,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_937_1516,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_990_1608})

V_866 = CTVertex(name = 'V_866',
                 type = 'UV',
                 particles = [ P.LQ3u__tilde__, P.LQ3u__tilde__, P.LQ3u, P.LQ3u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3u] ], [ [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1006_31,(1,0,5):C.UVGC_943_1527,(1,0,1):C.UVGC_1006_33,(1,0,4):C.UVGC_943_1528,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1006_31,(0,0,5):C.UVGC_943_1527,(0,0,1):C.UVGC_1006_33,(0,0,4):C.UVGC_943_1528})

V_867 = CTVertex(name = 'V_867',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2uu, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3u, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_948_1542,(1,0,1):C.UVGC_948_1543,(1,0,0):C.UVGC_948_1544,(0,0,2):C.UVGC_947_1539,(0,0,1):C.UVGC_947_1540,(0,0,0):C.UVGC_947_1541})

V_868 = CTVertex(name = 'V_868',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ3d, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3u, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_948_1542,(1,0,1):C.UVGC_948_1543,(1,0,0):C.UVGC_948_1544,(0,0,2):C.UVGC_947_1539,(0,0,1):C.UVGC_947_1540,(0,0,0):C.UVGC_947_1541})

V_869 = CTVertex(name = 'V_869',
                 type = 'UV',
                 particles = [ P.LQ2u, P.LQ2uu__tilde__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3u, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_948_1542,(1,0,1):C.UVGC_948_1543,(1,0,0):C.UVGC_948_1544,(0,0,2):C.UVGC_947_1539,(0,0,1):C.UVGC_947_1540,(0,0,0):C.UVGC_947_1541})

V_870 = CTVertex(name = 'V_870',
                 type = 'UV',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ3d__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3u, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_948_1542,(1,0,1):C.UVGC_948_1543,(1,0,0):C.UVGC_948_1544,(0,0,2):C.UVGC_947_1539,(0,0,1):C.UVGC_947_1540,(0,0,0):C.UVGC_947_1541})

V_871 = CTVertex(name = 'V_871',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ1d, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3d] ], [ [P.g, P.LQ1d, P.LQ3d] ], [ [P.g, P.LQ1d, P.LQ3d, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_23,(1,0,8):C.UVGC_957_1553,(1,0,1):C.UVGC_959_1560,(1,0,7):C.UVGC_967_1572,(1,0,2):C.UVGC_959_1562,(1,0,6):C.UVGC_967_1573,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_17,(0,0,8):C.UVGC_956_1550,(0,0,1):C.UVGC_958_1555,(0,0,7):C.UVGC_966_1570,(0,0,2):C.UVGC_958_1557,(0,0,6):C.UVGC_966_1571})

V_872 = CTVertex(name = 'V_872',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ1dd, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3d] ], [ [P.g, P.LQ1dd, P.LQ3d] ], [ [P.g, P.LQ1dd, P.LQ3d, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_957_1551,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_957_1552,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_957_1553,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_956_1548,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_956_1549,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_956_1550})

V_873 = CTVertex(name = 'V_873',
                 type = 'UV',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2uu, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3d] ], [ [P.g, P.LQ2uu, P.LQ3d] ], [ [P.g, P.LQ2uu, P.LQ3d, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1011_53,(1,0,8):C.UVGC_1017_86,(1,0,1):C.UVGC_1011_55,(1,0,7):C.UVGC_1017_87,(1,0,2):C.UVGC_1011_57,(1,0,6):C.UVGC_1017_88,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1010_47,(0,0,8):C.UVGC_1016_83,(0,0,1):C.UVGC_1010_49,(0,0,7):C.UVGC_1016_84,(0,0,2):C.UVGC_1010_51,(0,0,6):C.UVGC_1016_85})

V_874 = CTVertex(name = 'V_874',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2u, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3d] ], [ [P.g, P.LQ2u, P.LQ3d] ], [ [P.g, P.LQ2u, P.LQ3d, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_1005_28,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_1005_29,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1005_30,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_1004_25,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_1004_26,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1004_27})

V_875 = CTVertex(name = 'V_875',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2pu, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3d] ], [ [P.g, P.LQ2pu, P.LQ3d] ], [ [P.g, P.LQ2pu, P.LQ3d, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,8):C.UVGC_961_1567,(1,0,1):C.UVGC_1001_9,(1,0,7):C.UVGC_961_1568,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_961_1569,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,8):C.UVGC_960_1564,(0,0,1):C.UVGC_1000_3,(0,0,7):C.UVGC_960_1565,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_960_1566})

V_876 = CTVertex(name = 'V_876',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3d__tilde__, P.LQ3d ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3d] ], [ [P.a, P.g, P.LQ2d, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3d] ], [ [P.g, P.LQ2d, P.LQ3d] ], [ [P.g, P.LQ2d, P.LQ3d, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_23,(1,0,8):C.UVGC_959_1559,(1,0,1):C.UVGC_959_1560,(1,0,7):C.UVGC_959_1561,(1,0,2):C.UVGC_959_1562,(1,0,6):C.UVGC_959_1563,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_17,(0,0,8):C.UVGC_958_1554,(0,0,1):C.UVGC_958_1555,(0,0,7):C.UVGC_958_1556,(0,0,2):C.UVGC_958_1557,(0,0,6):C.UVGC_958_1558})

V_877 = CTVertex(name = 'V_877',
                 type = 'UV',
                 particles = [ P.LQ3d__tilde__, P.LQ3d, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ3d, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3d], [P.g, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u] ], [ [P.g, P.LQ3d, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3u, P.Z] ], [ [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1001_7,(1,0,10):C.UVGC_1023_111,(1,0,11):C.UVGC_1025_119,(1,0,1):C.UVGC_1001_9,(1,0,8):C.UVGC_1014_75,(1,0,9):C.UVGC_1025_120,(1,0,2):C.UVGC_1001_11,(1,0,6):C.UVGC_1023_114,(1,0,7):C.UVGC_1025_121,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1000_1,(0,0,10):C.UVGC_1022_105,(0,0,11):C.UVGC_1024_116,(0,0,1):C.UVGC_1000_3,(0,0,8):C.UVGC_1022_107,(0,0,9):C.UVGC_1024_117,(0,0,2):C.UVGC_1000_5,(0,0,6):C.UVGC_1022_109,(0,0,7):C.UVGC_1024_118})

V_878 = CTVertex(name = 'V_878',
                 type = 'UV',
                 particles = [ P.LQ3d__tilde__, P.LQ3d__tilde__, P.LQ3d, P.LQ3d ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d] ], [ [P.g] ], [ [P.g, P.LQ3d] ], [ [P.g, P.LQ3d, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_1000_1,(1,0,5):C.UVGC_935_1509,(1,0,1):C.UVGC_1000_3,(1,0,4):C.UVGC_935_1510,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_1000_1,(0,0,5):C.UVGC_935_1509,(0,0,1):C.UVGC_1000_3,(0,0,4):C.UVGC_935_1510})

V_879 = CTVertex(name = 'V_879',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2uu, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3dd, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_946_1536,(1,0,1):C.UVGC_946_1537,(1,0,0):C.UVGC_946_1538,(0,0,2):C.UVGC_945_1533,(0,0,1):C.UVGC_945_1534,(0,0,0):C.UVGC_945_1535})

V_880 = CTVertex(name = 'V_880',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2pu, P.LQ3d__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3dd, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_946_1536,(1,0,1):C.UVGC_946_1537,(1,0,0):C.UVGC_946_1538,(0,0,2):C.UVGC_945_1533,(0,0,1):C.UVGC_945_1534,(0,0,0):C.UVGC_945_1535})

V_881 = CTVertex(name = 'V_881',
                 type = 'UV',
                 particles = [ P.LQ3d__tilde__, P.LQ3d__tilde__, P.LQ3dd, P.LQ3u ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__], [P.g, P.LQ3d, P.LQ3u, P.W__plus__], [P.g, P.LQ3dd, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,3):C.UVGC_788_1110,(1,0,2):C.UVGC_1022_109,(1,0,0):C.UVGC_745_1066,(1,0,1):C.UVGC_1023_114,(0,0,3):C.UVGC_788_1110,(0,0,2):C.UVGC_1022_109,(0,0,0):C.UVGC_745_1066,(0,0,1):C.UVGC_1023_114})

V_882 = CTVertex(name = 'V_882',
                 type = 'UV',
                 particles = [ P.LQ2u, P.LQ2uu__tilde__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2u, P.LQ3d, P.W__plus__], [P.g, P.LQ2u, P.LQ3dd, P.W__plus__], [P.g, P.LQ2uu, P.LQ3d, P.W__plus__], [P.g, P.LQ2uu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2u, P.W__plus__], [P.g, P.LQ2uu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_946_1536,(1,0,1):C.UVGC_946_1537,(1,0,0):C.UVGC_946_1538,(0,0,2):C.UVGC_945_1533,(0,0,1):C.UVGC_945_1534,(0,0,0):C.UVGC_945_1535})

V_883 = CTVertex(name = 'V_883',
                 type = 'UV',
                 particles = [ P.LQ2d, P.LQ2pu__tilde__, P.LQ3d, P.LQ3dd__tilde__ ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ2d, P.LQ3d, P.W__plus__], [P.g, P.LQ2d, P.LQ3dd, P.W__plus__], [P.g, P.LQ2pu, P.LQ3d, P.W__plus__], [P.g, P.LQ2pu, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ2d, P.W__plus__], [P.g, P.LQ2pu, P.W__plus__], [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,2):C.UVGC_946_1536,(1,0,1):C.UVGC_946_1537,(1,0,0):C.UVGC_946_1538,(0,0,2):C.UVGC_945_1533,(0,0,1):C.UVGC_945_1534,(0,0,0):C.UVGC_945_1535})

V_884 = CTVertex(name = 'V_884',
                 type = 'UV',
                 particles = [ P.LQ3d, P.LQ3d, P.LQ3dd__tilde__, P.LQ3u__tilde__ ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g, P.LQ3dd, P.W__plus__], [P.g, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__], [P.g, P.LQ3d, P.LQ3u, P.W__plus__], [P.g, P.LQ3dd, P.LQ3u, P.W__plus__] ], [ [P.g, P.LQ3d, P.W__plus__] ], [ [P.g, P.W__plus__] ] ],
                 couplings = {(1,0,3):C.UVGC_788_1110,(1,0,2):C.UVGC_1022_109,(1,0,0):C.UVGC_745_1066,(1,0,1):C.UVGC_1023_114,(0,0,3):C.UVGC_788_1110,(0,0,2):C.UVGC_1022_109,(0,0,0):C.UVGC_745_1066,(0,0,1):C.UVGC_1023_114})

V_885 = CTVertex(name = 'V_885',
                 type = 'UV',
                 particles = [ P.LQ1d__tilde__, P.LQ1d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ1d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ1d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ1d, P.LQ3dd] ], [ [P.g, P.LQ1d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ1d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_1023_112,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_1023_113,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_1023_115,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_1022_106,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_1022_108,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_1022_110})

V_886 = CTVertex(name = 'V_886',
                 type = 'UV',
                 particles = [ P.LQ1dd__tilde__, P.LQ1dd, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ1dd], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ1dd, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ1dd], [P.g, P.LQ3dd] ], [ [P.g, P.LQ1dd, P.LQ3dd] ], [ [P.g, P.LQ1dd, P.LQ3dd, P.Z] ], [ [P.g, P.LQ1dd, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_983_1594,(1,0,8):C.UVGC_983_1595,(1,0,1):C.UVGC_983_1596,(1,0,7):C.UVGC_983_1597,(1,0,2):C.UVGC_1003_19,(1,0,6):C.UVGC_1023_112,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_982_1590,(0,0,8):C.UVGC_982_1591,(0,0,1):C.UVGC_982_1592,(0,0,7):C.UVGC_982_1593,(0,0,2):C.UVGC_1002_13,(0,0,6):C.UVGC_1022_106})

V_887 = CTVertex(name = 'V_887',
                 type = 'UV',
                 particles = [ P.LQ2uu__tilde__, P.LQ2uu, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2uu], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2uu, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2uu], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2uu, P.LQ3dd] ], [ [P.g, P.LQ2uu, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2uu, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1019_94,(1,0,8):C.UVGC_1019_95,(1,0,1):C.UVGC_1019_96,(1,0,7):C.UVGC_1019_97,(1,0,2):C.UVGC_1011_53,(1,0,6):C.UVGC_1019_98,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1018_89,(0,0,8):C.UVGC_1018_90,(0,0,1):C.UVGC_1018_91,(0,0,7):C.UVGC_1018_92,(0,0,2):C.UVGC_1010_47,(0,0,6):C.UVGC_1018_93})

V_888 = CTVertex(name = 'V_888',
                 type = 'UV',
                 particles = [ P.LQ2u__tilde__, P.LQ2u, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2u], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2u, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2u], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2u, P.LQ3dd] ], [ [P.g, P.LQ2u, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2u, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_1007_37,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_1007_39,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_1007_40,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_1006_32,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_1006_34,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_1006_35})

V_889 = CTVertex(name = 'V_889',
                 type = 'UV',
                 particles = [ P.LQ2pu__tilde__, P.LQ2pu, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2pu], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2pu, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2pu], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2pu, P.LQ3dd] ], [ [P.g, P.LQ2pu, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2pu, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_997_1617,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_997_1618,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_997_1619,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_938_1517,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_938_1518,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_996_1616})

V_890 = CTVertex(name = 'V_890',
                 type = 'UV',
                 particles = [ P.LQ2d__tilde__, P.LQ2d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ2d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ2d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ2d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ2d, P.LQ3dd] ], [ [P.g, P.LQ2d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ2d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,8):C.UVGC_989_1605,(1,0,1):C.UVGC_1003_21,(1,0,7):C.UVGC_989_1606,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_989_1607,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,8):C.UVGC_988_1602,(0,0,1):C.UVGC_1002_15,(0,0,7):C.UVGC_988_1603,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_988_1604})

V_891 = CTVertex(name = 'V_891',
                 type = 'UV',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd, P.LQ3u__tilde__, P.LQ3u ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3dd], [P.a, P.g, P.LQ3u] ], [ [P.a, P.g, P.LQ3dd, P.LQ3u] ], [ [P.g] ], [ [P.g, P.LQ3dd], [P.g, P.LQ3u] ], [ [P.g, P.LQ3dd, P.LQ3u] ], [ [P.g, P.LQ3dd, P.LQ3u, P.Z] ], [ [P.g, P.LQ3dd, P.Z], [P.g, P.LQ3u, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1007_36,(1,0,8):C.UVGC_1027_125,(1,0,1):C.UVGC_1007_38,(1,0,7):C.UVGC_1027_126,(1,0,2):C.UVGC_1001_7,(1,0,6):C.UVGC_1027_127,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1006_31,(0,0,8):C.UVGC_1026_122,(0,0,1):C.UVGC_1006_33,(0,0,7):C.UVGC_1026_123,(0,0,2):C.UVGC_1000_1,(0,0,6):C.UVGC_1026_124})

V_892 = CTVertex(name = 'V_892',
                 type = 'UV',
                 particles = [ P.LQ3d__tilde__, P.LQ3d, P.LQ3dd__tilde__, P.LQ3dd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3d], [P.a, P.g, P.LQ3dd] ], [ [P.a, P.g, P.LQ3d, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ3d], [P.g, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd] ], [ [P.g, P.LQ3d, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ3d, P.LQ3dd, P.Z] ], [ [P.g, P.LQ3d, P.W__plus__], [P.g, P.LQ3dd, P.W__plus__] ], [ [P.g, P.LQ3d, P.Z], [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.W__plus__] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,3):C.UVGC_707_1059,(1,0,4):C.UVGC_707_1060,(1,0,5):C.UVGC_707_1061,(1,0,0):C.UVGC_1003_19,(1,0,10):C.UVGC_1023_111,(1,0,11):C.UVGC_1023_112,(1,0,1):C.UVGC_1003_21,(1,0,8):C.UVGC_1014_75,(1,0,9):C.UVGC_1023_113,(1,0,2):C.UVGC_1003_23,(1,0,6):C.UVGC_1023_114,(1,0,7):C.UVGC_1023_115,(0,0,3):C.UVGC_706_1056,(0,0,4):C.UVGC_706_1057,(0,0,5):C.UVGC_706_1058,(0,0,0):C.UVGC_1002_13,(0,0,10):C.UVGC_1022_105,(0,0,11):C.UVGC_1022_106,(0,0,1):C.UVGC_1002_15,(0,0,8):C.UVGC_1022_107,(0,0,9):C.UVGC_1022_108,(0,0,2):C.UVGC_1002_17,(0,0,6):C.UVGC_1022_109,(0,0,7):C.UVGC_1022_110})

V_893 = CTVertex(name = 'V_893',
                 type = 'UV',
                 particles = [ P.LQ3dd__tilde__, P.LQ3dd__tilde__, P.LQ3dd, P.LQ3dd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.a, P.g] ], [ [P.a, P.g, P.LQ3dd] ], [ [P.g] ], [ [P.g, P.LQ3dd] ], [ [P.g, P.LQ3dd, P.Z] ], [ [P.g, P.Z] ] ],
                 couplings = {(1,0,2):C.UVGC_449_364,(1,0,3):C.UVGC_449_365,(1,0,0):C.UVGC_936_1511,(1,0,5):C.UVGC_942_1525,(1,0,1):C.UVGC_936_1513,(1,0,4):C.UVGC_942_1526,(0,0,2):C.UVGC_449_364,(0,0,3):C.UVGC_449_365,(0,0,0):C.UVGC_936_1511,(0,0,5):C.UVGC_942_1525,(0,0,1):C.UVGC_936_1513,(0,0,4):C.UVGC_942_1526})

