Requestor: Ruth Pottgen
Content: Pair production of scalar LQs (all generations)
Paper: http://arxiv.org/abs/1506.07369
JIRA: https://its.cern.ch/jira/browse/AGENE-1229