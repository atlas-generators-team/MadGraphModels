Contents: Scalar leptoquark model combining S1 and R2 multiplets for novel asymmetric pair production mechanism
Requestor: Casey Hampson
Paper: https://arxiv.org/abs/2210.11004
Source: Personal Communication with Ilja Dorsner (dorsner@fesb.hr)
