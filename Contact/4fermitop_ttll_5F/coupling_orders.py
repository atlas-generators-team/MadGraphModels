# This file was automatically created by FeynRules 2.0.28
# Mathematica version: 9.0 for Microsoft Windows (64-bit) (January 25, 2013)
# Date: Sat 13 Feb 2021 16:50:50


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 4,
                   hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

