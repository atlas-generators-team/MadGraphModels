Requestor: Denys Timoshyn
Content: SM + a singlet scalar (sig1), which only couples to top.
Paper: https://arxiv.org/abs/2104.09512
Model Origin: https://feynrules.irmp.ucl.ac.be/wiki/TopHeavyRes
Source: Luc Darmé, Benjamin Fuks, Fabio Maltoni