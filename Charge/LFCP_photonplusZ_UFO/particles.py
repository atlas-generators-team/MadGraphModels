# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.0.1 for Microsoft Windows (64-bit) (September 20, 2016)
# Date: Sun 7 Apr 2019 23:02:46


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

a = Particle(pdg_code = 22,
             name = 'a',
             antiname = 'a',
             spin = 3,
             color = 1,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'a',
             antitexname = 'a',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

Z = Particle(pdg_code = 23,
             name = 'Z',
             antiname = 'Z',
             spin = 3,
             color = 1,
             mass = Param.MZ,
             width = Param.WZ,
             texname = 'Z',
             antitexname = 'Z',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

W__plus__ = Particle(pdg_code = 24,
                     name = 'W+',
                     antiname = 'W-',
                     spin = 3,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'W+',
                     antitexname = 'W-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

W__minus__ = W__plus__.anti()

g = Particle(pdg_code = 21,
             name = 'g',
             antiname = 'g',
             spin = 3,
             color = 8,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'g',
             antitexname = 'g',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

ghA = Particle(pdg_code = 9000001,
               name = 'ghA',
               antiname = 'ghA~',
               spin = -1,
               color = 1,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghA',
               antitexname = 'ghA~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghA__tilde__ = ghA.anti()

ghZ = Particle(pdg_code = 9000002,
               name = 'ghZ',
               antiname = 'ghZ~',
               spin = -1,
               color = 1,
               mass = Param.MZ,
               width = Param.WZ,
               texname = 'ghZ',
               antitexname = 'ghZ~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghZ__tilde__ = ghZ.anti()

ghWp = Particle(pdg_code = 9000003,
                name = 'ghWp',
                antiname = 'ghWp~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWp',
                antitexname = 'ghWp~',
                charge = 1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWp__tilde__ = ghWp.anti()

ghWm = Particle(pdg_code = 9000004,
                name = 'ghWm',
                antiname = 'ghWm~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWm',
                antitexname = 'ghWm~',
                charge = -1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWm__tilde__ = ghWm.anti()

ghG = Particle(pdg_code = 82,
               name = 'ghG',
               antiname = 'ghG~',
               spin = -1,
               color = 8,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghG',
               antitexname = 'ghG~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghG__tilde__ = ghG.anti()

ve = Particle(pdg_code = 12,
              name = 've',
              antiname = 've~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 've',
              antitexname = 've~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

ve__tilde__ = ve.anti()

vm = Particle(pdg_code = 14,
              name = 'vm',
              antiname = 'vm~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vm',
              antitexname = 'vm~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

vm__tilde__ = vm.anti()

vt = Particle(pdg_code = 16,
              name = 'vt',
              antiname = 'vt~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vt',
              antitexname = 'vt~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

vt__tilde__ = vt.anti()

e__minus__ = Particle(pdg_code = 11,
                      name = 'e-',
                      antiname = 'e+',
                      spin = 2,
                      color = 1,
                      mass = Param.ZERO,
                      width = Param.ZERO,
                      texname = 'e-',
                      antitexname = 'e+',
                      charge = -1,
                      GhostNumber = 0,
                      LeptonNumber = 1,
                      Y = 0)

e__plus__ = e__minus__.anti()

mu__minus__ = Particle(pdg_code = 13,
                       name = 'mu-',
                       antiname = 'mu+',
                       spin = 2,
                       color = 1,
                       mass = Param.ZERO,
                       width = Param.ZERO,
                       texname = 'mu-',
                       antitexname = 'mu+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

mu__plus__ = mu__minus__.anti()

ta__minus__ = Particle(pdg_code = 15,
                       name = 'ta-',
                       antiname = 'ta+',
                       spin = 2,
                       color = 1,
                       mass = Param.MTA,
                       width = Param.ZERO,
                       texname = 'ta-',
                       antitexname = 'ta+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

ta__plus__ = ta__minus__.anti()

u = Particle(pdg_code = 2,
             name = 'u',
             antiname = 'u~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'u',
             antitexname = 'u~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

u__tilde__ = u.anti()

c = Particle(pdg_code = 4,
             name = 'c',
             antiname = 'c~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'c',
             antitexname = 'c~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

c__tilde__ = c.anti()

t = Particle(pdg_code = 6,
             name = 't',
             antiname = 't~',
             spin = 2,
             color = 3,
             mass = Param.MT,
             width = Param.WT,
             texname = 't',
             antitexname = 't~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

t__tilde__ = t.anti()

d = Particle(pdg_code = 1,
             name = 'd',
             antiname = 'd~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'd',
             antitexname = 'd~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

d__tilde__ = d.anti()

s = Particle(pdg_code = 3,
             name = 's',
             antiname = 's~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 's',
             antitexname = 's~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

s__tilde__ = s.anti()

b = Particle(pdg_code = 5,
             name = 'b',
             antiname = 'b~',
             spin = 2,
             color = 3,
             mass = Param.MB,
             width = Param.ZERO,
             texname = 'b',
             antitexname = 'b~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

b__tilde__ = b.anti()

H = Particle(pdg_code = 25,
             name = 'H',
             antiname = 'H',
             spin = 1,
             color = 1,
             mass = Param.MH,
             width = Param.WH,
             texname = 'H',
             antitexname = 'H',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

G0 = Particle(pdg_code = 250,
              name = 'G0',
              antiname = 'G0',
              spin = 1,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'G0',
              antitexname = 'G0',
              goldstone = True,
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

G__plus__ = Particle(pdg_code = 251,
                     name = 'G+',
                     antiname = 'G-',
                     spin = 1,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'G+',
                     antitexname = 'G-',
                     goldstone = True,
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

G__minus__ = G__plus__.anti()

fcp1v3 = Particle(pdg_code = 20001030,
                name = 'fcp1v3',
                antiname = 'fcp1v3~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp1v3,
                width = Param.ZERO,
                texname = 'fcp1v3',
                antitexname = 'fcp1v3~',
                charge = 1/3,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp1v3__tilde__ = fcp1v3.anti()

fcp2v3 = Particle(pdg_code = 20002030,
                name = 'fcp2v3',
                antiname = 'fcp2v3~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp2v3,
                width = Param.ZERO,
                texname = 'fcp2v3',
                antitexname = 'fcp2v3~',
                charge = 2/3,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp2v3__tilde__ = fcp2v3.anti()

fcp1v10 = Particle(pdg_code = 20001100,
                name = 'fcp1v10',
                antiname = 'fcp1v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp1v10,
                width = Param.ZERO,
                texname = 'fcp1v10',
                antitexname = 'fcp1v10~',
                charge = 0.1,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp1v10__tilde__ = fcp1v10.anti()

fcp2v10 = Particle(pdg_code = 20002100,
                name = 'fcp2v10',
                antiname = 'fcp2v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp2v10,
                width = Param.ZERO,
                texname = 'fcp2v10',
                antitexname = 'fcp2v10~',
                charge = 0.2,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp2v10__tilde__ = fcp2v10.anti()

fcp3v10 = Particle(pdg_code = 20003100,
                name = 'fcp3v10',
                antiname = 'fcp3v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp3v10,
                width = Param.ZERO,
                texname = 'fcp3v10',
                antitexname = 'fcp3v10~',
                charge = 0.3,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp3v10__tilde__ = fcp3v10.anti()

fcp4v10 = Particle(pdg_code = 20004100,
                name = 'fcp4v10',
                antiname = 'fcp4v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp4v10,
                width = Param.ZERO,
                texname = 'fcp4v10',
                antitexname = 'fcp4v10~',
                charge = 0.4,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp4v10__tilde__ = fcp4v10.anti()

fcp5v10 = Particle(pdg_code = 20005100,
                name = 'fcp5v10',
                antiname = 'fcp5v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp5v10,
                width = Param.ZERO,
                texname = 'fcp5v10',
                antitexname = 'fcp5v10~',
                charge = 0.5,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp5v10__tilde__ = fcp5v10.anti()

fcp6v10 = Particle(pdg_code = 20006100,
                name = 'fcp6v10',
                antiname = 'fcp6v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp6v10,
                width = Param.ZERO,
                texname = 'fcp6v10',
                antitexname = 'fcp6v10~',
                charge = 0.6,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp6v10__tilde__ = fcp6v10.anti()

fcp7v10 = Particle(pdg_code = 20007100,
                name = 'fcp7v10',
                antiname = 'fcp7v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp7v10,
                width = Param.ZERO,
                texname = 'fcp7v10',
                antitexname = 'fcp7v10~',
                charge = 0.7,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp7v10__tilde__ = fcp7v10.anti()

fcp8v10 = Particle(pdg_code = 20008100,
                name = 'fcp8v10',
                antiname = 'fcp8v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp8v10,
                width = Param.ZERO,
                texname = 'fcp8v10',
                antitexname = 'fcp8v10~',
                charge = 0.8,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp8v10__tilde__ = fcp8v10.anti()

fcp9v10 = Particle(pdg_code = 20009100,
                name = 'fcp9v10',
                antiname = 'fcp9v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp9v10,
                width = Param.ZERO,
                texname = 'fcp9v10',
                antitexname = 'fcp9v10~',
                charge = 0.9,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp9v10__tilde__ = fcp9v10.anti()

fcp10v10 = Particle(pdg_code = 20010100,
                name = 'fcp10v10',
                antiname = 'fcp10v10~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp10v10,
                width = Param.ZERO,
                texname = 'fcp10v10',
                antitexname = 'fcp10v10~',
                charge = 1.0,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp10v10__tilde__ = fcp10v10.anti()

fcp3v4 = Particle(pdg_code = 20003040,
                name = 'fcp3v4',
                antiname = 'fcp3v4~',
                spin = 2,
                color = 1,
                mass = Param.Mfcp3v4,
                width = Param.ZERO,
                texname = 'fcp3v4',
                antitexname = 'fcp3v4~',
                charge = 1.0,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

fcp3v4__tilde__ = fcp3v4.anti()
