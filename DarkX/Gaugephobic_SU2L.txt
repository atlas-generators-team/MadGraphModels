Requestor: Jochen Heinrich
Content: Dark pion model with gauge-phobic pi > f fbar / f f' decays and full mixing
Paper 1: https://arxiv.org/abs/1809.10183
Paper 2: https://arxiv.org/abs/1809.10184
Source: https://github.com/bostdiek/HeavyDarkMesons/tree/master/UFO_Files/FromPaper
JIRA: https://its.cern.ch/jira/browse/AGENE-1767
