# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 11.3.0 for Linux x86 (64-bit) (March 7, 2018)
# Date: Wed 24 Mar 2021 10:48:15



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

sTL = Parameter(name = 'sTL',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\sin (\\text{thetaL})',
                lhablock = 'NPBLOCK',
                lhacode = [ 1 ])

sS = Parameter(name = 'sS',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\sin (\\text{thetaS})',
               lhablock = 'NPBLOCK',
               lhacode = [ 2 ])

vD = Parameter(name = 'vD',
               nature = 'external',
               type = 'real',
               value = 10,
               texname = 'v_D',
               lhablock = 'NPBLOCK',
               lhacode = [ 3 ])

Epsi = Parameter(name = 'Epsi',
                 nature = 'external',
                 type = 'real',
                 value = 0.001,
                 texname = '\\epsilon',
                 lhablock = 'NPBLOCK',
                 lhacode = [ 4 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 137.035999074,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.000011663787,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 173,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MAD = Parameter(name = 'MAD',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{MAD}',
                lhablock = 'MASS',
                lhacode = [ 60000 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 173,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MVLT = Parameter(name = 'MVLT',
                 nature = 'external',
                 type = 'real',
                 value = 1000.,
                 texname = '\\text{MVLT}',
                 lhablock = 'MASS',
                 lhacode = [ 60001 ])

MH1 = Parameter(name = 'MH1',
                nature = 'external',
                type = 'real',
                value = 125,
                texname = '\\text{MH1}',
                lhablock = 'MASS',
                lhacode = [ 25 ])

MH2 = Parameter(name = 'MH2',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{MH2}',
                lhablock = 'MASS',
                lhacode = [ 60002 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WAD = Parameter(name = 'WAD',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\text{WAD}',
                lhablock = 'DECAY',
                lhacode = [ 60000 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WVLT = Parameter(name = 'WVLT',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = '\\text{WVLT}',
                 lhablock = 'DECAY',
                 lhacode = [ 60001 ])

WH1 = Parameter(name = 'WH1',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{WH1}',
                lhablock = 'DECAY',
                lhacode = [ 25 ])

WH2 = Parameter(name = 'WH2',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\text{WH2}',
                lhablock = 'DECAY',
                lhacode = [ 60002 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1/Gf)/2**0.25',
                texname = '\\text{vev}')

vevD = Parameter(name = 'vevD',
                 nature = 'internal',
                 type = 'real',
                 value = 'vD',
                 texname = '\\text{vevD}')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

thetaS = Parameter(name = 'thetaS',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.asin(sS)',
                   texname = '\\text{thetaS}')

cTL = Parameter(name = 'cTL',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sTL**2)',
                texname = '\\cos  \\theta _L')

s2TL = Parameter(name = 's2TL',
                 nature = 'internal',
                 type = 'real',
                 value = '2*sTL*cmath.sqrt(1 - sTL**2)',
                 texname = '\\text{s2TL}')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

cS = Parameter(name = 'cS',
               nature = 'internal',
               type = 'real',
               value = 'cmath.cos(thetaS)',
               texname = '\\text{Cos} \\theta _s')

cWSM2 = Parameter(name = 'cWSM2',
                  nature = 'internal',
                  type = 'real',
                  value = '0.5 + 0.5*cmath.sqrt(1 - (2*aEW*cmath.pi*cmath.sqrt(2))/(Gf*MZ**2))',
                  texname = '\\text{cWSM2}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

lamHHD = Parameter(name = 'lamHHD',
                   nature = 'internal',
                   type = 'real',
                   value = '((-MH1**2 + MH2**2)*cmath.sin(2*thetaS))/(2.*vev*vevD)',
                   texname = '\\lambda _{\\text{HHD}}')

lamt = Parameter(name = 'lamt',
                 nature = 'internal',
                 type = 'real',
                 value = '((-MT**2 + MVLT**2)*s2TL)/(vevD*cmath.sqrt(2)*cmath.sqrt(cTL**2*MT**2 + MVLT**2*sTL**2))',
                 texname = '\\text{lamt}')

m2 = Parameter(name = 'm2',
               nature = 'internal',
               type = 'real',
               value = '(MT*MVLT)/cmath.sqrt(cTL**2*MT**2 + MVLT**2*sTL**2)',
               texname = '\\text{m2}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

ytp = Parameter(name = 'ytp',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(cTL**2*MT**2 + MVLT**2*sTL**2)',
                texname = '\\text{ytp}')

cTR = Parameter(name = 'cTR',
                nature = 'internal',
                type = 'real',
                value = '(cTL*m2)/MVLT',
                texname = '\\cos  \\theta _R')

lamH = Parameter(name = 'lamH',
                 nature = 'internal',
                 type = 'real',
                 value = '(cS**2*MH1**2 + MH2**2*sS**2)/(2.*vev**2)',
                 texname = '\\text{lamH}')

lamHD = Parameter(name = 'lamHD',
                  nature = 'internal',
                  type = 'real',
                  value = '(cS**2*MH2**2 + MH1**2*sS**2)/(2.*vevD**2)',
                  texname = '\\text{lamHD}')

sTR = Parameter(name = 'sTR',
                nature = 'internal',
                type = 'real',
                value = '(m2*sTL)/MT',
                texname = '\\sin  \\theta _R')

tWSM = Parameter(name = 'tWSM',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(1 - cWSM2)/cmath.sqrt(cWSM2)',
                 texname = '\\text{tWSM}')

cW = Parameter(name = 'cW',
               nature = 'internal',
               type = 'real',
               value = '-(Epsi**2*tWSM**4)/(2.*(-1 + 2*cWSM2)*(1 - MAD**2/MZ**2)*(1 + tWSM**2)**1.5) + cmath.sqrt(cWSM2)',
               texname = '\\cos (\\text{thetaW})')

gD = Parameter(name = 'gD',
               nature = 'internal',
               type = 'real',
               value = 'MAD/vevD + (Epsi**2*(1 - cWSM2*(1 - MAD**2/MZ**2))*MZ**2*tWSM**2)/(2.*MAD*(1 - MAD**2/MZ**2)*vevD)',
               texname = 'g_D')

muH2 = Parameter(name = 'muH2',
                 nature = 'internal',
                 type = 'real',
                 value = 'lamH*vev**2 + (lamHHD*vevD**2)/2.',
                 texname = '\\mu')

muHD2 = Parameter(name = 'muHD2',
                  nature = 'internal',
                  type = 'real',
                  value = '(lamHHD*vev**2)/2. + lamHD*vevD**2',
                  texname = '\\mu')

sD = Parameter(name = 'sD',
               nature = 'internal',
               type = 'real',
               value = '(Epsi*tWSM)/(1 - MAD**2/MZ**2) + (Epsi**3*(1 - cWSM2*(1 - (2*MAD**2)/MZ**2) - (1.5*MAD**2)/MZ**2)*tWSM**3)/((-1 + 2*cWSM2)*(1 - MAD**2/MZ**2)**3)',
               texname = '\\sin (\\text{thetaD})')

EpsiP = Parameter(name = 'EpsiP',
                  nature = 'internal',
                  type = 'real',
                  value = 'Epsi/cmath.sqrt(1 + Epsi**2/cW**2)',
                  texname = '\\text{EpsiP}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cW',
               texname = 'g_1')

M0Dhat = Parameter(name = 'M0Dhat',
                   nature = 'internal',
                   type = 'real',
                   value = '(0.5*ee*Epsi*vev)/cW**2',
                   texname = '\\text{M0Dhat}')

thetaD = Parameter(name = 'thetaD',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.asin(sD)',
                   texname = '\\text{thetaD}')

thetaW = Parameter(name = 'thetaW',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.acos(cW)',
                   texname = '\\text{thetaW}')

cD = Parameter(name = 'cD',
               nature = 'internal',
               type = 'real',
               value = 'cmath.cos(thetaD)',
               texname = '\\text{cD}')

gD1 = Parameter(name = 'gD1',
                nature = 'internal',
                type = 'real',
                value = 'gD*cmath.sqrt(1 - EpsiP**2/cW**2)',
                texname = 'g_D')

sW = Parameter(name = 'sW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(thetaW)',
               texname = '\\text{sW}')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sW',
               texname = 'g_w')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = '(ee*vev)/(2.*sW)',
               texname = 'M_W')

