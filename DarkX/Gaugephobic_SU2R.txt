Requestor: Jochen Heinrich
Content: Dark pion model with gauge-phobic pi > f fbar / f f' decays and neutral (only) mixing
Paper 1: https://arxiv.org/abs/1809.10183
Paper 2: https://arxiv.org/abs/1809.10184
Source: https://github.com/bostdiek/HeavyDarkMesons/tree/master/UFO_Files/FromPaper
JIRA: https://its.cern.ch/jira/browse/AGENE-1767
Update from: Jochen Heinrich
Note: Modified PDG IDs relative to website version (to comply with PDG ID rules)
Source: https://github.com/bostdiek/HeavyDarkMesons/tree/master/UFO_Files/FromPaper
JIRA: https://its.cern.ch/jira/browse/AGENE-1878
