# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 10.2.0 for Linux x86 (64-bit) (July 28, 2015)
# Date: Mon 21 May 2018 22:02:25


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

DIM6 = CouplingOrder(name = 'DIM6',
                     expansion_order = 99,
                     hierarchy = 1)

FCNC = CouplingOrder(name = 'FCNC',
                     expansion_order = 99,
                     hierarchy = 1)

