# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 13 Jun 2016 10:16:35



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
Lambda = Parameter(name = 'Lambda',
                   nature = 'external',
                   type = 'real',
                   value = 1000,
                   texname = '\\Lambda',
                   lhablock = 'DIM6',
                   lhacode = [ 1 ])

ccc = Parameter(name = 'ccc',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{ccc}',
                lhablock = 'DIM6',
                lhacode = [ 2 ])

ctG = Parameter(name = 'ctG',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = 'C_{\\text{tG}}',
                lhablock = 'DIM6',
                lhacode = [ 3 ])

cft = Parameter(name = 'cft',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'C_{\\text{$\\phi $t}}',
                lhablock = 'DIM6',
                lhacode = [ 4 ])

cfb = Parameter(name = 'cfb',
                nature = 'external',
                type = 'real',
                value = 2.,
                texname = 'C_{\\text{$\\phi $b}}',
                lhablock = 'DIM6',
                lhacode = [ 5 ])

cfQ1 = Parameter(name = 'cfQ1',
                 nature = 'external',
                 type = 'real',
                 value = 3.,
                 texname = '\\text{Subsuperscript}[C,\\text{$\\phi $Q},1]',
                 lhablock = 'DIM6',
                 lhacode = [ 6 ])

cfQ3 = Parameter(name = 'cfQ3',
                 nature = 'external',
                 type = 'real',
                 value = 4.,
                 texname = '\\text{Subsuperscript}[C,\\text{$\\phi $Q},3]',
                 lhablock = 'DIM6',
                 lhacode = [ 7 ])

ctW = Parameter(name = 'ctW',
                nature = 'external',
                type = 'real',
                value = 2.,
                texname = 'C_{\\text{tW}}',
                lhablock = 'DIM6',
                lhacode = [ 8 ])

ctB = Parameter(name = 'ctB',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'C_{\\text{tB}}',
                lhablock = 'DIM6',
                lhacode = [ 9 ])

cff = Parameter(name = 'cff',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = 'C_{\\phi \\phi }',
                lhablock = 'DIM6',
                lhacode = [ 10 ])

cbW = Parameter(name = 'cbW',
                nature = 'external',
                type = 'real',
                value = 2.5,
                texname = 'C_{\\text{bW}}',
                lhablock = 'DIM6',
                lhacode = [ 11 ])

muprime = Parameter(name = 'muprime',
                    nature = 'external',
                    type = 'real',
                    value = 172.5,
                    texname = '\\text{$\\mu $1}',
                    lhablock = 'DIM6',
                    lhacode = [ 12 ])

C4f = Parameter(name = 'C4f',
                nature = 'external',
                type = 'real',
                value = -1.,
                texname = 'C_{4 f}',
                lhablock = 'EFT',
                lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172.5,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172.5,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MWp = Parameter(name = 'MWp',
                nature = 'external',
                type = 'real',
                value = 1.e8,
                texname = '\\text{MWp}',
                lhablock = 'MASS',
                lhacode = [ 34 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               #value = 1.50833649,
               value = 0.0,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WWp = Parameter(name = 'WWp',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{WWp}',
                lhablock = 'DECAY',
                lhacode = [ 34 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

gp = Parameter(name = 'gp',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(-((C4f*MWp**2)/Lambda**2))',
               texname = 'g_p')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

