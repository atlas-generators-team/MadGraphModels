# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 13 Jun 2016 10:16:36


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6, L.VVV7 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_247_117,(0,0,1):C.R2GC_233_102,(0,0,2):C.R2GC_233_103,(0,1,0):C.R2GC_248_118,(0,1,1):C.R2GC_234_104,(0,1,2):C.R2GC_234_105,(0,2,0):C.R2GC_248_118,(0,2,1):C.R2GC_234_104,(0,2,2):C.R2GC_234_105,(0,3,0):C.R2GC_247_117,(0,3,1):C.R2GC_233_102,(0,3,2):C.R2GC_233_103,(0,4,0):C.R2GC_247_117,(0,4,1):C.R2GC_233_102,(0,4,2):C.R2GC_233_103,(0,5,0):C.R2GC_248_118,(0,5,1):C.R2GC_234_104,(0,5,2):C.R2GC_234_105})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(7,0,0):C.R2GC_250_120,(7,0,1):C.R2GC_237_110,(7,0,2):C.R2GC_237_111,(6,0,0):C.R2GC_250_120,(6,0,1):C.R2GC_238_112,(6,0,2):C.R2GC_238_113,(2,0,1):C.R2GC_208_90,(2,0,2):C.R2GC_208_91,(0,0,1):C.R2GC_208_90,(0,0,2):C.R2GC_208_91,(4,0,1):C.R2GC_206_86,(4,0,2):C.R2GC_206_87,(3,0,1):C.R2GC_206_86,(3,0,2):C.R2GC_206_87,(8,0,1):C.R2GC_207_88,(8,0,2):C.R2GC_207_89,(5,0,1):C.R2GC_206_86,(5,0,2):C.R2GC_206_87,(1,0,1):C.R2GC_206_86,(1,0,2):C.R2GC_206_87,(11,0,1):C.R2GC_210_93,(11,0,2):C.R2GC_210_94,(10,0,1):C.R2GC_210_93,(10,0,2):C.R2GC_210_94,(9,0,2):C.R2GC_209_92,(6,1,0):C.R2GC_249_119,(6,1,1):C.R2GC_236_108,(6,1,2):C.R2GC_236_109,(8,1,0):C.R2GC_250_120,(8,1,1):C.R2GC_237_110,(8,1,2):C.R2GC_237_111,(2,1,1):C.R2GC_208_90,(2,1,2):C.R2GC_208_91,(0,1,1):C.R2GC_208_90,(0,1,2):C.R2GC_208_91,(4,1,1):C.R2GC_206_86,(4,1,2):C.R2GC_206_87,(3,1,1):C.R2GC_206_86,(3,1,2):C.R2GC_206_87,(5,1,1):C.R2GC_206_86,(5,1,2):C.R2GC_206_87,(1,1,1):C.R2GC_206_86,(1,1,2):C.R2GC_206_87,(7,1,1):C.R2GC_207_88,(7,1,2):C.R2GC_207_89,(11,1,1):C.R2GC_210_93,(11,1,2):C.R2GC_210_94,(10,1,1):C.R2GC_210_93,(10,1,2):C.R2GC_210_94,(9,1,2):C.R2GC_209_92,(7,2,0):C.R2GC_249_119,(7,2,1):C.R2GC_235_106,(7,2,2):C.R2GC_235_107,(8,2,0):C.R2GC_249_119,(8,2,1):C.R2GC_235_106,(8,2,2):C.R2GC_235_107,(2,2,1):C.R2GC_208_90,(2,2,2):C.R2GC_208_91,(0,2,1):C.R2GC_208_90,(0,2,2):C.R2GC_208_91,(4,2,1):C.R2GC_206_86,(4,2,2):C.R2GC_206_87,(3,2,1):C.R2GC_206_86,(3,2,2):C.R2GC_206_87,(5,2,1):C.R2GC_206_86,(5,2,2):C.R2GC_206_87,(1,2,1):C.R2GC_206_86,(1,2,2):C.R2GC_206_87,(11,2,1):C.R2GC_210_93,(11,2,2):C.R2GC_210_94,(10,2,1):C.R2GC_210_93,(10,2,2):C.R2GC_210_94,(9,2,2):C.R2GC_209_92})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.d__tilde__, P.u, P.Wp__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5 ],
               loop_particles = [ [ [P.d, P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_216_95})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.s__tilde__, P.c, P.Wp__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5 ],
               loop_particles = [ [ [P.c, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_216_95})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.b__tilde__, P.t, P.Wp__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5, L.FFV6, L.FFV7, L.FFV8, L.FFV9 ],
               loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_103_4,(0,0,1):C.R2GC_216_95,(0,3,0):C.R2GC_82_140,(0,1,0):C.R2GC_83_141,(0,4,0):C.R2GC_78_136,(0,2,0):C.R2GC_81_139})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.u__tilde__, P.d, P.Wp__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5 ],
               loop_particles = [ [ [P.d, P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_216_95})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.c__tilde__, P.s, P.Wp__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV5 ],
               loop_particles = [ [ [P.c, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_216_95})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.Wp__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV18, L.FFV19, L.FFV20, L.FFV21, L.FFV5 ],
               loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
               couplings = {(0,4,0):C.R2GC_103_4,(0,4,1):C.R2GC_216_95,(0,2,0):C.R2GC_83_141,(0,0,0):C.R2GC_82_140,(0,3,0):C.R2GC_79_137,(0,1,0):C.R2GC_80_138})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV12, L.FFV18, L.FFV19, L.FFV20, L.FFV21, L.FFV5, L.FFV6, L.FFV7, L.FFV8, L.FFV9 ],
               loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
               couplings = {(0,5,0):C.R2GC_230_100,(0,5,1):C.R2GC_244_115,(0,0,0):C.R2GC_231_101,(0,0,1):C.R2GC_245_116,(0,8,0):C.R2GC_161_48,(0,3,0):C.R2GC_160_47,(0,6,0):C.R2GC_160_47,(0,1,0):C.R2GC_161_48,(0,9,0):C.R2GC_157_44,(0,4,0):C.R2GC_156_43,(0,7,0):C.R2GC_158_45,(0,2,0):C.R2GC_159_46})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV12, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_153_40,(0,1,1):C.R2GC_184_78,(0,0,0):C.R2GC_126_27,(0,0,1):C.R2GC_132_33})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV12, L.FFV18, L.FFV19, L.FFV20, L.FFV21, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
                couplings = {(0,5,0):C.R2GC_229_99,(0,5,1):C.R2GC_217_96,(0,0,0):C.R2GC_228_98,(0,3,0):C.R2GC_110_11,(0,1,0):C.R2GC_109_10,(0,4,0):C.R2GC_263_124,(0,2,0):C.R2GC_261_123})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_134_34})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_134_34})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV24 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_101_2,(0,0,1):C.R2GC_134_34,(0,1,0):C.R2GC_68_126})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_147_38})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_147_38})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV15, L.FFV23 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_71_129,(0,0,0):C.R2GC_102_3,(0,0,1):C.R2GC_147_38})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_217_96})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_217_96})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV10, L.FFV12, L.FFV5, L.FFV7 ],
                loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_229_99,(0,2,1):C.R2GC_217_96,(0,1,0):C.R2GC_228_98,(0,0,0):C.R2GC_109_10,(0,3,0):C.R2GC_111_12})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV17, L.FFV5 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_135_35,(0,0,0):C.R2GC_136_36})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV17, L.FFV5 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_135_35,(0,0,0):C.R2GC_136_36})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_131_32})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_131_32})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_131_32})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_147_38})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_147_38})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_147_38})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_217_96})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_217_96})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV13, L.FFV5 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_139_37,(0,0,0):C.R2GC_136_36})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV13, L.FFV5 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,1,0):C.R2GC_139_37,(0,0,0):C.R2GC_136_36})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV24 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_105_6})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                lorentz = [ L.FFVV1, L.FFVV10, L.FFVV16, L.FFVV17, L.FFVV2, L.FFVV4, L.FFVV5, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,6,0):C.R2GC_74_132,(2,4,0):C.R2GC_254_122,(1,4,0):C.R2GC_253_121,(2,0,0):C.R2GC_76_134,(1,0,0):C.R2GC_77_135,(2,5,0):C.R2GC_75_133,(1,5,0):C.R2GC_75_133,(2,1,0):C.R2GC_77_135,(1,1,0):C.R2GC_76_134,(2,7,0):C.R2GC_253_121,(1,7,0):C.R2GC_254_122,(2,3,0):C.R2GC_77_135,(1,3,0):C.R2GC_76_134,(2,2,0):C.R2GC_253_121,(1,2,0):C.R2GC_254_122})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_113_14})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV18 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_112_13})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV13, L.FFVV14 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_70_128,(0,1,0):C.R2GC_106_7})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV22, L.FFVV23 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_70_128,(0,1,0):C.R2GC_107_8})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_130_31})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_130_31})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF4 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_104_5,(0,1,1):C.R2GC_242_114,(0,0,0):C.R2GC_222_97,(0,0,1):C.R2GC_130_31,(0,2,0):C.R2GC_67_125})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_130_31})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_130_31})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_130_31})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV2, L.VV3, L.VV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,2,1):C.R2GC_128_29,(0,0,2):C.R2GC_129_30,(0,1,0):C.R2GC_176_63})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.g, P.g, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_174_61,(0,0,1):C.R2GC_179_68,(0,0,2):C.R2GC_179_69})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV7 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_69_127})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_202_83,(0,0,0):C.R2GC_202_83,(0,2,0):C.R2GC_201_82})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_204_85,(0,0,0):C.R2GC_204_85,(0,2,0):C.R2GC_203_84})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.a, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV15, L.FFVV16 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_84_142,(0,0,0):C.R2GC_85_143})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.Wp__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV15, L.FFVV16, L.FFVV17 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_171_58,(0,1,0):C.R2GC_173_60,(0,0,0):C.R2GC_168_55})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV15, L.FFVV16, L.FFVV17, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_122_23,(0,5,0):C.R2GC_121_22,(0,4,0):C.R2GC_120_21,(0,3,0):C.R2GC_124_25,(0,2,0):C.R2GC_123_24,(0,1,0):C.R2GC_125_26})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.a, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_88_146,(0,2,0):C.R2GC_86_144,(0,1,0):C.R2GC_87_145})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.Wp__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_170_57,(0,2,0):C.R2GC_172_59,(0,1,0):C.R2GC_169_56})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.W__plus__, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV15, L.FFVV16, L.FFVV17, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_121_22,(0,5,0):C.R2GC_122_23,(0,4,0):C.R2GC_120_21,(0,3,0):C.R2GC_123_24,(0,2,0):C.R2GC_124_25,(0,1,0):C.R2GC_125_26})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Wp__minus__, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV15, L.FFVV16, L.FFVV17, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_98_156,(0,5,0):C.R2GC_96_154,(0,4,0):C.R2GC_95_153,(0,3,0):C.R2GC_100_1,(0,2,0):C.R2GC_99_157,(0,1,0):C.R2GC_97_155})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.g, P.Wp__minus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_92_150,(0,2,0):C.R2GC_93_151,(0,1,0):C.R2GC_89_147})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.g ],
                color = [ 'T(4,2,1)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_73_131,(0,0,0):C.R2GC_73_131,(0,2,0):C.R2GC_72_130})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.Z ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV10, L.FFVV15, L.FFVV16, L.FFVV17, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_164_51,(0,5,0):C.R2GC_163_50,(0,4,0):C.R2GC_166_53,(0,3,0):C.R2GC_165_52,(0,2,0):C.R2GC_167_54,(0,1,0):C.R2GC_162_49})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.g, P.W__plus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV15, L.FFVV16, L.FFVV17 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_116_17,(0,1,0):C.R2GC_115_16,(0,0,0):C.R2GC_119_20})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.g, P.W__minus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_117_18,(0,2,0):C.R2GC_118_19,(0,1,0):C.R2GC_114_15})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.g, P.Wp__plus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV15, L.FFVV16, L.FFVV26 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_90_148,(0,2,0):C.R2GC_91_149,(0,0,0):C.R2GC_94_152})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.g, P.g, P.W__plus__, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_127_28,(0,0,1):C.R2GC_187_81,(0,1,0):C.R2GC_127_28,(0,1,1):C.R2GC_187_81,(0,2,0):C.R2GC_127_28,(0,2,1):C.R2GC_187_81})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.g, P.g, P.Wp__minus__, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_185_79,(0,1,0):C.R2GC_185_79,(0,2,0):C.R2GC_185_79})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_177_64,(0,0,1):C.R2GC_177_65,(0,1,0):C.R2GC_177_64,(0,1,1):C.R2GC_177_65,(0,2,0):C.R2GC_177_64,(0,2,1):C.R2GC_177_65})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_154_41,(0,0,1):C.R2GC_180_70,(0,0,2):C.R2GC_180_71,(0,1,0):C.R2GC_154_41,(0,1,1):C.R2GC_180_70,(0,1,2):C.R2GC_180_71,(0,2,0):C.R2GC_154_41,(0,2,1):C.R2GC_180_70,(0,2,2):C.R2GC_180_71})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_152_39,(0,0,1):C.R2GC_183_76,(0,0,2):C.R2GC_183_77,(0,1,0):C.R2GC_152_39,(0,1,1):C.R2GC_183_76,(0,1,2):C.R2GC_183_77,(0,2,0):C.R2GC_152_39,(0,2,1):C.R2GC_183_76,(0,2,2):C.R2GC_183_77})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_108_9,(0,0,1):C.R2GC_186_80,(0,1,0):C.R2GC_108_9,(0,1,1):C.R2GC_186_80,(0,2,0):C.R2GC_108_9,(0,2,1):C.R2GC_186_80})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_127_28,(0,0,1):C.R2GC_187_81,(0,1,0):C.R2GC_127_28,(0,1,1):C.R2GC_187_81,(0,2,0):C.R2GC_127_28,(0,2,1):C.R2GC_187_81})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_178_66,(0,0,1):C.R2GC_178_67,(0,1,0):C.R2GC_178_66,(0,1,1):C.R2GC_178_67,(0,2,0):C.R2GC_178_66,(0,2,1):C.R2GC_178_67})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_175_62,(1,0,1):C.R2GC_182_74,(1,0,2):C.R2GC_182_75,(0,1,0):C.R2GC_155_42,(0,1,1):C.R2GC_181_72,(0,1,2):C.R2GC_181_73,(0,2,0):C.R2GC_155_42,(0,2,1):C.R2GC_181_72,(0,2,2):C.R2GC_181_73,(0,3,0):C.R2GC_155_42,(0,3,1):C.R2GC_181_72,(0,3,2):C.R2GC_181_73})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6, L.VVV7 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_247_70,(0,0,1):C.UVGC_233_42,(0,0,2):C.UVGC_233_43,(0,0,3):C.UVGC_233_44,(0,0,4):C.UVGC_233_45,(0,1,0):C.UVGC_248_71,(0,1,1):C.UVGC_234_46,(0,1,2):C.UVGC_234_47,(0,1,3):C.UVGC_234_48,(0,1,4):C.UVGC_234_49,(0,2,0):C.UVGC_248_71,(0,2,1):C.UVGC_234_46,(0,2,2):C.UVGC_234_47,(0,2,3):C.UVGC_234_48,(0,2,4):C.UVGC_234_49,(0,3,0):C.UVGC_247_70,(0,3,1):C.UVGC_233_42,(0,3,2):C.UVGC_233_43,(0,3,3):C.UVGC_233_44,(0,3,4):C.UVGC_233_45,(0,4,0):C.UVGC_247_70,(0,4,1):C.UVGC_233_42,(0,4,2):C.UVGC_233_43,(0,4,3):C.UVGC_233_44,(0,4,4):C.UVGC_233_45,(0,5,0):C.UVGC_248_71,(0,5,1):C.UVGC_234_46,(0,5,2):C.UVGC_234_47,(0,5,3):C.UVGC_234_48,(0,5,4):C.UVGC_234_49})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(7,0,0):C.UVGC_250_73,(7,0,1):C.UVGC_237_56,(7,0,2):C.UVGC_237_57,(7,0,3):C.UVGC_237_58,(7,0,4):C.UVGC_237_59,(6,0,0):C.UVGC_250_73,(6,0,1):C.UVGC_237_56,(6,0,2):C.UVGC_238_60,(6,0,3):C.UVGC_238_61,(6,0,4):C.UVGC_237_59,(2,0,2):C.UVGC_207_16,(2,0,3):C.UVGC_207_15,(0,0,2):C.UVGC_207_16,(0,0,3):C.UVGC_207_15,(4,0,2):C.UVGC_206_13,(4,0,3):C.UVGC_206_14,(3,0,2):C.UVGC_206_13,(3,0,3):C.UVGC_206_14,(8,0,2):C.UVGC_207_15,(8,0,3):C.UVGC_207_16,(5,0,2):C.UVGC_206_13,(5,0,3):C.UVGC_206_14,(1,0,2):C.UVGC_206_13,(1,0,3):C.UVGC_206_14,(11,0,2):C.UVGC_210_19,(11,0,3):C.UVGC_210_20,(10,0,2):C.UVGC_210_19,(10,0,3):C.UVGC_210_20,(9,0,2):C.UVGC_209_17,(9,0,3):C.UVGC_209_18,(6,1,0):C.UVGC_249_72,(6,1,1):C.UVGC_235_50,(6,1,2):C.UVGC_236_54,(6,1,3):C.UVGC_236_55,(6,1,4):C.UVGC_235_53,(8,1,0):C.UVGC_250_73,(8,1,1):C.UVGC_237_56,(8,1,2):C.UVGC_237_57,(8,1,3):C.UVGC_237_58,(8,1,4):C.UVGC_237_59,(2,1,2):C.UVGC_207_16,(2,1,3):C.UVGC_207_15,(0,1,2):C.UVGC_207_16,(0,1,3):C.UVGC_207_15,(4,1,2):C.UVGC_206_13,(4,1,3):C.UVGC_206_14,(3,1,2):C.UVGC_206_13,(3,1,3):C.UVGC_206_14,(5,1,2):C.UVGC_206_13,(5,1,3):C.UVGC_206_14,(1,1,2):C.UVGC_206_13,(1,1,3):C.UVGC_206_14,(7,1,2):C.UVGC_207_15,(7,1,3):C.UVGC_207_16,(11,1,2):C.UVGC_210_19,(11,1,3):C.UVGC_210_20,(10,1,2):C.UVGC_210_19,(10,1,3):C.UVGC_210_20,(9,1,2):C.UVGC_209_17,(9,1,3):C.UVGC_209_18,(7,2,0):C.UVGC_249_72,(7,2,1):C.UVGC_235_50,(7,2,2):C.UVGC_235_51,(7,2,3):C.UVGC_235_52,(7,2,4):C.UVGC_235_53,(8,2,0):C.UVGC_249_72,(8,2,1):C.UVGC_235_50,(8,2,2):C.UVGC_235_51,(8,2,3):C.UVGC_235_52,(8,2,4):C.UVGC_235_53,(2,2,2):C.UVGC_207_16,(2,2,3):C.UVGC_207_15,(0,2,2):C.UVGC_207_16,(0,2,3):C.UVGC_207_15,(4,2,2):C.UVGC_206_13,(4,2,3):C.UVGC_206_14,(3,2,2):C.UVGC_206_13,(3,2,3):C.UVGC_206_14,(5,2,2):C.UVGC_206_13,(5,2,3):C.UVGC_206_14,(1,2,2):C.UVGC_206_13,(1,2,3):C.UVGC_206_14,(11,2,2):C.UVGC_210_19,(11,2,3):C.UVGC_210_20,(10,2,2):C.UVGC_210_19,(10,2,3):C.UVGC_210_20,(9,2,2):C.UVGC_209_17,(9,2,3):C.UVGC_209_18})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_216_25,(0,0,1):C.UVGC_216_26})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_216_25,(0,0,1):C.UVGC_216_26})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.Wp__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [],
                couplings = {(0,0,0):C.UVGC_220_29})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_216_25,(0,0,1):C.UVGC_216_26})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_216_25,(0,0,1):C.UVGC_216_26})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.Wp__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [],
                couplings = {(0,0,0):C.UVGC_220_29})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV12, L.FFV2, L.FFV3, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_230_39,(0,3,1):C.UVGC_244_67,(0,0,0):C.UVGC_231_40,(0,0,1):C.UVGC_245_68,(0,2,0):C.UVGC_270_92,(0,1,0):C.UVGC_271_93})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.UVGC_265_87,(0,0,0):C.UVGC_264_86})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV12, L.FFV19, L.FFV21, L.FFV5, L.FFV7, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_229_38,(0,3,1):C.UVGC_217_27,(0,3,3):C.UVGC_243_66,(0,3,2):C.UVGC_217_28,(0,0,0):C.UVGC_228_37,(0,2,0):C.UVGC_262_85,(0,1,0):C.UVGC_261_84,(0,5,0):C.UVGC_260_83,(0,4,0):C.UVGC_259_82})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_193_5})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_193_5})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV4 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_223_32,(0,0,1):C.UVGC_240_63,(0,1,0):C.UVGC_251_74})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_211_21,(0,0,1):C.UVGC_211_22,(0,0,2):C.UVGC_211_23,(0,0,3):C.UVGC_211_24})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_211_21,(0,0,2):C.UVGC_211_22,(0,0,3):C.UVGC_211_23,(0,0,1):C.UVGC_211_24})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_224_33,(0,0,1):C.UVGC_211_21,(0,0,2):C.UVGC_211_22,(0,0,3):C.UVGC_211_23,(0,0,4):C.UVGC_241_64,(0,1,0):C.UVGC_252_75})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_217_27,(0,0,1):C.UVGC_217_28})

V_90 = CTVertex(name = 'V_90',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_217_27,(0,0,1):C.UVGC_217_28})

V_91 = CTVertex(name = 'V_91',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV12, L.FFV25, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_229_38,(0,3,1):C.UVGC_217_27,(0,3,3):C.UVGC_243_66,(0,3,2):C.UVGC_217_28,(0,1,0):C.UVGC_228_37,(0,0,0):C.UVGC_262_85,(0,2,0):C.UVGC_260_83})

V_92 = CTVertex(name = 'V_92',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_191_4})

V_93 = CTVertex(name = 'V_93',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_191_4})

V_94 = CTVertex(name = 'V_94',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14, L.FFV22 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_191_4,(0,1,0):C.UVGC_221_30})

V_95 = CTVertex(name = 'V_95',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_211_21,(0,0,2):C.UVGC_211_22,(0,0,3):C.UVGC_211_23,(0,0,1):C.UVGC_211_24})

V_96 = CTVertex(name = 'V_96',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_211_21,(0,0,1):C.UVGC_211_22,(0,0,2):C.UVGC_211_23,(0,0,3):C.UVGC_211_24})

V_97 = CTVertex(name = 'V_97',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.b, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_211_21,(0,0,2):C.UVGC_211_22,(0,0,3):C.UVGC_211_23,(0,0,1):C.UVGC_211_24})

V_98 = CTVertex(name = 'V_98',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_217_27,(0,0,1):C.UVGC_217_28})

V_99 = CTVertex(name = 'V_99',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_217_27,(0,0,1):C.UVGC_217_28})

V_100 = CTVertex(name = 'V_100',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV19 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_226_35})

V_101 = CTVertex(name = 'V_101',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV19, L.FFVV25 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_227_36,(0,1,0):C.UVGC_189_2})

V_102 = CTVertex(name = 'V_102',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g, P.g ],
                 color = [ 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                 lorentz = [ L.FFVV16, L.FFVV2, L.FFVV9 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(1,1,0):C.UVGC_254_77,(0,1,0):C.UVGC_253_76,(1,2,0):C.UVGC_253_76,(0,2,0):C.UVGC_254_77,(1,0,0):C.UVGC_253_76,(0,0,0):C.UVGC_254_77})

V_103 = CTVertex(name = 'V_103',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV11, L.FFVV20 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_269_91,(0,1,0):C.UVGC_266_88})

V_104 = CTVertex(name = 'V_104',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV11, L.FFVV20 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_267_89,(0,1,0):C.UVGC_268_90})

V_105 = CTVertex(name = 'V_105',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV11, L.FFVV20 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_257_80,(0,1,0):C.UVGC_256_79})

V_106 = CTVertex(name = 'V_106',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV11, L.FFVV20 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_255_78,(0,1,0):C.UVGC_258_81})

V_107 = CTVertex(name = 'V_107',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF6 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_190_3})

V_108 = CTVertex(name = 'V_108',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF6 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_190_3})

V_109 = CTVertex(name = 'V_109',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF3, L.FF5, L.FF7 ],
                 loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,1):C.UVGC_242_65,(0,0,0):C.UVGC_222_31,(0,1,0):C.UVGC_225_34,(0,3,1):C.UVGC_239_62,(0,4,1):C.UVGC_198_6})

V_110 = CTVertex(name = 'V_110',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF6 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_190_3})

V_111 = CTVertex(name = 'V_111',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF6 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_190_3})

V_112 = CTVertex(name = 'V_112',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF6 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_190_3})

V_113 = CTVertex(name = 'V_113',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV5 ],
                 loop_particles = [ [ [] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_246_69,(0,1,3):C.UVGC_232_41,(0,0,1):C.UVGC_205_11,(0,0,2):C.UVGC_205_12})

V_114 = CTVertex(name = 'V_114',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV6 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,0,0):C.UVGC_188_1})

V_115 = CTVertex(name = 'V_115',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV1, L.FFVV2, L.FFVV4 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,1,0):C.UVGC_202_8,(0,0,0):C.UVGC_202_8,(0,2,0):C.UVGC_201_7})

V_116 = CTVertex(name = 'V_116',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFVV1, L.FFVV2, L.FFVV4 ],
                 loop_particles = [ [ [] ] ],
                 couplings = {(0,1,0):C.UVGC_204_10,(0,0,0):C.UVGC_204_10,(0,2,0):C.UVGC_203_9})

