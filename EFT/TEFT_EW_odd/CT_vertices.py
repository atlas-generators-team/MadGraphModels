# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Wed 14 Sep 2016 20:40:23


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_207_81,(0,0,1):C.R2GC_194_69,(0,0,2):C.R2GC_194_70})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(7,1,0):C.R2GC_209_83,(7,1,1):C.R2GC_197_75,(7,1,2):C.R2GC_197_76,(6,1,0):C.R2GC_209_83,(6,1,1):C.R2GC_198_77,(6,1,2):C.R2GC_198_78,(2,1,1):C.R2GC_166_52,(2,1,2):C.R2GC_166_53,(0,1,1):C.R2GC_166_52,(0,1,2):C.R2GC_166_53,(4,1,1):C.R2GC_164_48,(4,1,2):C.R2GC_164_49,(3,1,1):C.R2GC_164_48,(3,1,2):C.R2GC_164_49,(8,1,1):C.R2GC_165_50,(8,1,2):C.R2GC_165_51,(5,1,1):C.R2GC_164_48,(5,1,2):C.R2GC_164_49,(1,1,1):C.R2GC_164_48,(1,1,2):C.R2GC_164_49,(11,0,1):C.R2GC_168_55,(11,0,2):C.R2GC_168_56,(10,0,1):C.R2GC_168_55,(10,0,2):C.R2GC_168_56,(9,0,2):C.R2GC_167_54,(6,2,0):C.R2GC_208_82,(6,2,1):C.R2GC_196_73,(6,2,2):C.R2GC_196_74,(8,2,0):C.R2GC_209_83,(8,2,1):C.R2GC_197_75,(8,2,2):C.R2GC_197_76,(2,2,1):C.R2GC_166_52,(2,2,2):C.R2GC_166_53,(0,2,1):C.R2GC_166_52,(0,2,2):C.R2GC_166_53,(4,2,1):C.R2GC_164_48,(4,2,2):C.R2GC_164_49,(3,2,1):C.R2GC_164_48,(3,2,2):C.R2GC_164_49,(5,2,1):C.R2GC_164_48,(5,2,2):C.R2GC_164_49,(1,2,1):C.R2GC_164_48,(1,2,2):C.R2GC_164_49,(7,2,1):C.R2GC_165_50,(7,2,2):C.R2GC_165_51,(7,3,0):C.R2GC_208_82,(7,3,1):C.R2GC_195_71,(7,3,2):C.R2GC_195_72,(8,3,0):C.R2GC_208_82,(8,3,1):C.R2GC_195_71,(8,3,2):C.R2GC_195_72,(2,3,1):C.R2GC_166_52,(2,3,2):C.R2GC_166_53,(0,3,1):C.R2GC_166_52,(0,3,2):C.R2GC_166_53,(4,3,1):C.R2GC_164_48,(4,3,2):C.R2GC_164_49,(3,3,1):C.R2GC_164_48,(3,3,2):C.R2GC_164_49,(5,3,1):C.R2GC_164_48,(5,3,2):C.R2GC_164_49,(1,3,1):C.R2GC_164_48,(1,3,2):C.R2GC_164_49})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV15, L.FFV17, L.FFV19, L.FFV22, L.FFV27, L.FFV30, L.FFV9 ],
               loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
               couplings = {(0,6,0):C.R2GC_189_66,(0,6,1):C.R2GC_204_80,(0,3,1):C.R2GC_112_13,(0,0,0):C.R2GC_188_65,(0,2,0):C.R2GC_107_8,(0,1,0):C.R2GC_104_5,(0,5,0):C.R2GC_97_111,(0,4,0):C.R2GC_105_6})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV15, L.FFV16, L.FFV9 ],
               loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
               couplings = {(0,2,0):C.R2GC_129_21,(0,2,1):C.R2GC_111_12,(0,1,1):C.R2GC_112_13,(0,0,0):C.R2GC_103_4})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV15, L.FFV29, L.FFV9 ],
               loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
               couplings = {(0,2,0):C.R2GC_187_64,(0,2,1):C.R2GC_177_58,(0,0,0):C.R2GC_191_68,(0,1,0):C.R2GC_96_110})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.u__tilde__, P.u, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_114_14})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.c__tilde__, P.c, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.c, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_114_14})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV20, L.FFV28 ],
               loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_91_105,(0,0,1):C.R2GC_114_14,(0,1,0):C.R2GC_82_96})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.u__tilde__, P.u, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_122_17})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_122_17})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV20, L.FFV26 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_84_98,(0,0,0):C.R2GC_92_106,(0,0,1):C.R2GC_122_17})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_177_58})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_177_58})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV10, L.FFV11, L.FFV12, L.FFV13, L.FFV15, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
                couplings = {(0,5,0):C.R2GC_187_64,(0,5,1):C.R2GC_177_58,(0,4,0):C.R2GC_190_67,(0,2,0):C.R2GC_95_109,(0,0,0):C.R2GC_96_110,(0,3,0):C.R2GC_242_91,(0,1,0):C.R2GC_240_90})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_144_43,(0,0,0):C.R2GC_115_15})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_144_43,(0,0,0):C.R2GC_115_15})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_169_57})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_169_57})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_169_57})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_122_17})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_122_17})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_122_17})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_177_58})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_177_58})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_145_44,(0,0,0):C.R2GC_117_16})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,1,0):C.R2GC_145_44,(0,0,0):C.R2GC_117_16})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV12, L.FFVV13, L.FFVV14, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_186_63,(0,5,0):C.R2GC_183_60,(0,4,0):C.R2GC_161_46,(0,3,0):C.R2GC_184_61,(0,2,0):C.R2GC_185_62,(0,1,0):C.R2GC_162_47})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                lorentz = [ L.FFVV1, L.FFVV10, L.FFVV13, L.FFVV14, L.FFVV3, L.FFVV5, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,4,0):C.R2GC_86_100,(2,4,0):C.R2GC_215_85,(1,4,0):C.R2GC_214_84,(0,0,0):C.R2GC_86_100,(2,0,0):C.R2GC_89_103,(1,0,0):C.R2GC_90_104,(0,5,0):C.R2GC_88_102,(2,5,0):C.R2GC_87_101,(1,5,0):C.R2GC_87_101,(2,1,0):C.R2GC_90_104,(1,1,0):C.R2GC_89_103,(2,6,0):C.R2GC_214_84,(1,6,0):C.R2GC_215_85,(2,3,0):C.R2GC_90_104,(1,3,0):C.R2GC_89_103,(2,2,0):C.R2GC_214_84,(1,2,0):C.R2GC_215_85})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_246_94,(0,2,0):C.R2GC_243_92,(0,1,0):C.R2GC_99_113})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_244_93,(0,0,0):C.R2GC_98_112})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_227_86,(0,2,0):C.R2GC_230_89,(0,1,0):C.R2GC_127_19})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13, L.FFVV14 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_228_87,(0,1,0):C.R2GC_229_88,(0,0,0):C.R2GC_126_18})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_110_11})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_110_11})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF4 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_93_107,(0,1,1):C.R2GC_202_79,(0,0,0):C.R2GC_179_59,(0,0,1):C.R2GC_110_11,(0,2,0):C.R2GC_81_95})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_110_11})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_110_11})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_110_11})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV2, L.VV3, L.VV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,2,1):C.R2GC_108_9,(0,0,2):C.R2GC_109_10,(0,1,0):C.R2GC_136_28})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.g, P.g, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_133_25,(0,0,1):C.R2GC_139_33,(0,0,2):C.R2GC_139_34})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV6 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_83_97})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV6 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_132_24})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV6 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_135_27})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.g ],
                color = [ 'T(4,2,1)' ],
                lorentz = [ L.FFVV7 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_85_99})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.Z ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV15, L.FFVV17 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_102_3,(0,1,0):C.R2GC_106_7})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.g, P.W__plus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV16 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.g, P.W__minus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV11 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_101_2})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_137_29,(0,0,1):C.R2GC_137_30})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_130_22,(0,0,1):C.R2GC_140_35,(0,0,2):C.R2GC_140_36})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_128_20,(0,0,1):C.R2GC_143_41,(0,0,2):C.R2GC_143_42})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_94_108,(0,0,1):C.R2GC_147_45})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_138_31,(0,0,1):C.R2GC_138_32})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_134_26,(1,0,1):C.R2GC_142_39,(1,0,2):C.R2GC_142_40,(0,1,0):C.R2GC_131_23,(0,1,1):C.R2GC_141_37,(0,1,2):C.R2GC_141_38})

V_54 = CTVertex(name = 'V_54',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV2 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_207_64,(0,0,1):C.UVGC_194_40,(0,0,2):C.UVGC_194_41,(0,0,3):C.UVGC_194_42,(0,0,4):C.UVGC_194_43})

V_55 = CTVertex(name = 'V_55',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(7,1,0):C.UVGC_209_66,(7,1,1):C.UVGC_197_50,(7,1,2):C.UVGC_197_51,(7,1,3):C.UVGC_197_52,(7,1,4):C.UVGC_197_53,(6,1,0):C.UVGC_209_66,(6,1,1):C.UVGC_197_50,(6,1,2):C.UVGC_198_54,(6,1,3):C.UVGC_198_55,(6,1,4):C.UVGC_197_53,(2,1,2):C.UVGC_165_13,(2,1,3):C.UVGC_165_12,(0,1,2):C.UVGC_165_13,(0,1,3):C.UVGC_165_12,(4,1,2):C.UVGC_164_10,(4,1,3):C.UVGC_164_11,(3,1,2):C.UVGC_164_10,(3,1,3):C.UVGC_164_11,(8,1,2):C.UVGC_165_12,(8,1,3):C.UVGC_165_13,(5,1,2):C.UVGC_164_10,(5,1,3):C.UVGC_164_11,(1,1,2):C.UVGC_164_10,(1,1,3):C.UVGC_164_11,(11,0,2):C.UVGC_168_16,(11,0,3):C.UVGC_168_17,(10,0,2):C.UVGC_168_16,(10,0,3):C.UVGC_168_17,(9,0,2):C.UVGC_167_14,(9,0,3):C.UVGC_167_15,(6,2,0):C.UVGC_208_65,(6,2,1):C.UVGC_195_44,(6,2,2):C.UVGC_196_48,(6,2,3):C.UVGC_196_49,(6,2,4):C.UVGC_195_47,(8,2,0):C.UVGC_209_66,(8,2,1):C.UVGC_197_50,(8,2,2):C.UVGC_197_51,(8,2,3):C.UVGC_197_52,(8,2,4):C.UVGC_197_53,(2,2,2):C.UVGC_165_13,(2,2,3):C.UVGC_165_12,(0,2,2):C.UVGC_165_13,(0,2,3):C.UVGC_165_12,(4,2,2):C.UVGC_164_10,(4,2,3):C.UVGC_164_11,(3,2,2):C.UVGC_164_10,(3,2,3):C.UVGC_164_11,(5,2,2):C.UVGC_164_10,(5,2,3):C.UVGC_164_11,(1,2,2):C.UVGC_164_10,(1,2,3):C.UVGC_164_11,(7,2,2):C.UVGC_165_12,(7,2,3):C.UVGC_165_13,(7,3,0):C.UVGC_208_65,(7,3,1):C.UVGC_195_44,(7,3,2):C.UVGC_195_45,(7,3,3):C.UVGC_195_46,(7,3,4):C.UVGC_195_47,(8,3,0):C.UVGC_208_65,(8,3,1):C.UVGC_195_44,(8,3,2):C.UVGC_195_45,(8,3,3):C.UVGC_195_46,(8,3,4):C.UVGC_195_47,(2,3,2):C.UVGC_165_13,(2,3,3):C.UVGC_165_12,(0,3,2):C.UVGC_165_13,(0,3,3):C.UVGC_165_12,(4,3,2):C.UVGC_164_10,(4,3,3):C.UVGC_164_11,(3,3,2):C.UVGC_164_10,(3,3,3):C.UVGC_164_11,(5,3,2):C.UVGC_164_10,(5,3,3):C.UVGC_164_11,(1,3,2):C.UVGC_164_10,(1,3,3):C.UVGC_164_11})

V_56 = CTVertex(name = 'V_56',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV15, L.FFV22, L.FFV5, L.FFV8, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,4,0):C.UVGC_189_35,(0,4,1):C.UVGC_204_61,(0,1,1):C.UVGC_205_62,(0,1,0):C.UVGC_192_38,(0,0,0):C.UVGC_188_34,(0,2,0):C.UVGC_247_104,(0,3,0):C.UVGC_248_105})

V_57 = CTVertex(name = 'V_57',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5, L.FFV8 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_231_88,(0,1,0):C.UVGC_235_92})

V_58 = CTVertex(name = 'V_58',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14, L.FFV15, L.FFV31, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_187_33,(0,3,1):C.UVGC_177_23,(0,3,3):C.UVGC_203_60,(0,3,2):C.UVGC_177_24,(0,1,0):C.UVGC_191_37,(0,0,0):C.UVGC_234_91,(0,2,0):C.UVGC_241_98})

V_59 = CTVertex(name = 'V_59',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV18 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_153_4})

V_60 = CTVertex(name = 'V_60',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV18 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_153_4})

V_61 = CTVertex(name = 'V_61',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV5, L.FFV7, L.FFV8 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_180_26,(0,0,1):C.UVGC_200_57,(0,1,0):C.UVGC_212_69,(0,3,0):C.UVGC_218_75,(0,2,0):C.UVGC_149_1})

V_62 = CTVertex(name = 'V_62',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_170_19,(0,0,1):C.UVGC_170_20,(0,0,2):C.UVGC_170_21,(0,0,3):C.UVGC_170_22})

V_63 = CTVertex(name = 'V_63',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_170_19,(0,0,2):C.UVGC_170_20,(0,0,3):C.UVGC_170_21,(0,0,1):C.UVGC_170_22})

V_64 = CTVertex(name = 'V_64',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_181_27,(0,0,1):C.UVGC_170_19,(0,0,2):C.UVGC_170_20,(0,0,3):C.UVGC_170_21,(0,0,4):C.UVGC_201_58,(0,1,0):C.UVGC_213_70})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_177_23,(0,0,1):C.UVGC_177_24})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_177_23,(0,0,1):C.UVGC_177_24})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV13, L.FFV15, L.FFV23, L.FFV24, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,5,0):C.UVGC_187_33,(0,5,1):C.UVGC_177_23,(0,5,3):C.UVGC_203_60,(0,5,2):C.UVGC_177_24,(0,2,0):C.UVGC_190_36,(0,1,0):C.UVGC_242_99,(0,4,0):C.UVGC_232_89,(0,3,0):C.UVGC_233_90,(0,0,0):C.UVGC_240_97})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_169_18,(0,2,0):C.UVGC_151_3,(0,1,0):C.UVGC_151_3})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV15, L.FFV9 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_169_18,(0,2,0):C.UVGC_151_3,(0,1,0):C.UVGC_151_3})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV15, L.FFV2, L.FFV3, L.FFV4, L.FFV6, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_169_18,(0,4,0):C.UVGC_211_68,(0,5,0):C.UVGC_216_73,(0,2,0):C.UVGC_210_67,(0,3,0):C.UVGC_217_74,(0,6,1):C.UVGC_151_3,(0,1,1):C.UVGC_151_3})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_170_19,(0,0,2):C.UVGC_170_20,(0,0,3):C.UVGC_170_21,(0,0,1):C.UVGC_170_22})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_170_19,(0,0,1):C.UVGC_170_20,(0,0,2):C.UVGC_170_21,(0,0,3):C.UVGC_170_22})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.b, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_170_19,(0,0,2):C.UVGC_170_20,(0,0,3):C.UVGC_170_21,(0,0,1):C.UVGC_170_22})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_177_23,(0,0,1):C.UVGC_177_24})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV9 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_177_23,(0,0,1):C.UVGC_177_24})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV1, L.FFVV2, L.FFVV3, L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.UVGC_220_77,(0,3,0):C.UVGC_221_78,(0,0,0):C.UVGC_219_76,(0,1,0):C.UVGC_222_79})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV12, L.FFVV13, L.FFVV14, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_186_32,(0,5,0):C.UVGC_183_29,(0,4,0):C.UVGC_161_6,(0,3,0):C.UVGC_184_30,(0,2,0):C.UVGC_185_31,(0,1,0):C.UVGC_162_7})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                lorentz = [ L.FFVV13, L.FFVV3, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(1,1,0):C.UVGC_215_72,(0,1,0):C.UVGC_214_71,(1,2,0):C.UVGC_214_71,(0,2,0):C.UVGC_215_72,(1,0,0):C.UVGC_214_71,(0,0,0):C.UVGC_215_72})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV13, L.FFVV14, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_246_103,(0,3,0):C.UVGC_243_100,(0,2,0):C.UVGC_237_94,(0,1,0):C.UVGC_238_95})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV13, L.FFVV14, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.UVGC_244_101,(0,0,0):C.UVGC_236_93,(0,3,0):C.UVGC_239_96,(0,2,0):C.UVGC_245_102})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV13, L.FFVV14, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_227_84,(0,3,0):C.UVGC_230_87,(0,2,0):C.UVGC_225_82,(0,1,0):C.UVGC_224_81})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV10, L.FFVV13, L.FFVV14, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.UVGC_228_85,(0,1,0):C.UVGC_229_86,(0,0,0):C.UVGC_226_83,(0,3,0):C.UVGC_223_80})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_150_2})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_150_2})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF2, L.FF3, L.FF5, L.FF7 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,2,1):C.UVGC_202_59,(0,0,0):C.UVGC_179_25,(0,1,0):C.UVGC_182_28,(0,3,1):C.UVGC_199_56,(0,4,1):C.UVGC_158_5})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_150_2})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_150_2})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_150_2})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV5 ],
                loop_particles = [ [ [] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,1,0):C.UVGC_206_63,(0,1,3):C.UVGC_193_39,(0,0,1):C.UVGC_163_8,(0,0,2):C.UVGC_163_9})

