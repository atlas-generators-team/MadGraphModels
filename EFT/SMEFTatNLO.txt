Requestor: Pim Verschuuren
Contents: complete implementation of SM EFT dimension 6 operators in the Warsaw basis compatible with NLO QCD calculations (v1.0)
Webpage: http://feynrules.irmp.ucl.ac.be/wiki/SMEFTatNLO
Source: http://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/SMEFTatNLO/SMEFTatNLO_v1.0.tar.gz
JIRA: https://its.cern.ch/jira/browse/AGENE-1692, https://its.cern.ch/jira/browse/AGENE-1862
