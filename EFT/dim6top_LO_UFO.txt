Requestor: Thomas Peiffer
Contents: Top EFT dim6top UFO model. 
          All the flavour-,B- and L-conserving parameters are assigned the same coupling order: DIM6=1. 
          Similarly, all the flavour-changing parameters are assigned one single coupling order: FCNC=1.
Source: http://feynrules.irmp.ucl.ac.be/wiki/dim6top
Paper: https://arxiv.org/abs/1802.07237
JIRA: https://its.cern.ch/jira/browse/AGENE-1585
