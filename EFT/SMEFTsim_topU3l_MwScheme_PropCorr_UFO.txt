Requestor: Hannes Mildner
Contents: SMEFTsim 3.0: Dimension 6 Standard Model EFT using the Warsaw basis, mW input parameter scheme, flavour universal except for 3rd quark gen !!!with activated propagator correction, see Section 8.3 of paper!!!
Webpage: https://smeftsim.github.io/
Source: https://github.com/SMEFTsim/SMEFTsim/archive/v3.0.2.zip
Paper: https://arxiv.org/abs/2012.11343
JIRA: https://its.cern.ch/jira/browse/AGENE-2002
