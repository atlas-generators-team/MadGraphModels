# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 4 Jul 2016 18:12:49


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_193_68,(0,0,1):C.R2GC_180_55,(0,0,2):C.R2GC_180_56})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(7,1,0):C.R2GC_195_70,(7,1,1):C.R2GC_183_61,(7,1,2):C.R2GC_183_62,(6,1,0):C.R2GC_195_70,(6,1,1):C.R2GC_184_63,(6,1,2):C.R2GC_184_64,(2,1,1):C.R2GC_150_41,(2,1,2):C.R2GC_150_42,(0,1,1):C.R2GC_150_41,(0,1,2):C.R2GC_150_42,(4,1,1):C.R2GC_148_37,(4,1,2):C.R2GC_148_38,(3,1,1):C.R2GC_148_37,(3,1,2):C.R2GC_148_38,(8,1,1):C.R2GC_149_39,(8,1,2):C.R2GC_149_40,(5,1,1):C.R2GC_148_37,(5,1,2):C.R2GC_148_38,(1,1,1):C.R2GC_148_37,(1,1,2):C.R2GC_148_38,(11,0,1):C.R2GC_152_44,(11,0,2):C.R2GC_152_45,(10,0,1):C.R2GC_152_44,(10,0,2):C.R2GC_152_45,(9,0,2):C.R2GC_151_43,(6,2,0):C.R2GC_194_69,(6,2,1):C.R2GC_182_59,(6,2,2):C.R2GC_182_60,(8,2,0):C.R2GC_195_70,(8,2,1):C.R2GC_183_61,(8,2,2):C.R2GC_183_62,(2,2,1):C.R2GC_150_41,(2,2,2):C.R2GC_150_42,(0,2,1):C.R2GC_150_41,(0,2,2):C.R2GC_150_42,(4,2,1):C.R2GC_148_37,(4,2,2):C.R2GC_148_38,(3,2,1):C.R2GC_148_37,(3,2,2):C.R2GC_148_38,(5,2,1):C.R2GC_148_37,(5,2,2):C.R2GC_148_38,(1,2,1):C.R2GC_148_37,(1,2,2):C.R2GC_148_38,(7,2,1):C.R2GC_149_39,(7,2,2):C.R2GC_149_40,(7,3,0):C.R2GC_194_69,(7,3,1):C.R2GC_181_57,(7,3,2):C.R2GC_181_58,(8,3,0):C.R2GC_194_69,(8,3,1):C.R2GC_181_57,(8,3,2):C.R2GC_181_58,(2,3,1):C.R2GC_150_41,(2,3,2):C.R2GC_150_42,(0,3,1):C.R2GC_150_41,(0,3,2):C.R2GC_150_42,(4,3,1):C.R2GC_148_37,(4,3,2):C.R2GC_148_38,(3,3,1):C.R2GC_148_37,(3,3,2):C.R2GC_148_38,(5,3,1):C.R2GC_148_37,(5,3,2):C.R2GC_148_38,(1,3,1):C.R2GC_148_37,(1,3,2):C.R2GC_148_38})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV11, L.FFV13, L.FFV15, L.FFV18, L.FFV23, L.FFV26, L.FFV5 ],
               loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
               couplings = {(0,6,0):C.R2GC_177_54,(0,6,1):C.R2GC_190_66,(0,3,1):C.R2GC_191_67,(0,0,0):C.R2GC_176_53,(0,2,0):C.R2GC_87_108,(0,1,0):C.R2GC_84_105,(0,5,0):C.R2GC_77_98,(0,4,0):C.R2GC_85_106})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV11, L.FFV12, L.FFV5 ],
               loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
               couplings = {(0,2,0):C.R2GC_109_8,(0,2,1):C.R2GC_91_111,(0,1,1):C.R2GC_191_67,(0,0,0):C.R2GC_83_104})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV11, L.FFV25, L.FFV5 ],
               loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
               couplings = {(0,2,0):C.R2GC_175_52,(0,2,1):C.R2GC_161_47,(0,0,0):C.R2GC_174_51,(0,1,0):C.R2GC_76_97})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.u__tilde__, P.u, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_101_2})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.c__tilde__, P.c, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.c, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_101_2})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV16, L.FFV24 ],
               loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_71_92,(0,0,1):C.R2GC_101_2,(0,1,0):C.R2GC_62_83})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.u__tilde__, P.u, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_102_3})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_102_3})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV16, L.FFV22 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_64_85,(0,0,0):C.R2GC_72_93,(0,0,1):C.R2GC_102_3})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_161_47})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_161_47})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV5, L.FFV6, L.FFV7, L.FFV8, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_175_52,(0,1,1):C.R2GC_161_47,(0,0,0):C.R2GC_174_51,(0,4,0):C.R2GC_75_96,(0,2,0):C.R2GC_76_97,(0,5,0):C.R2GC_210_78,(0,3,0):C.R2GC_208_77})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_124_30,(0,0,0):C.R2GC_105_4})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_124_30,(0,0,0):C.R2GC_105_4})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_153_46})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_153_46})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_153_46})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_102_3})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_102_3})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_102_3})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_161_47})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_161_47})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_125_31,(0,0,0):C.R2GC_97_112})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,1,0):C.R2GC_125_31,(0,0,0):C.R2GC_97_112})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV11, L.FFVV12, L.FFVV13, L.FFVV7, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,5,0):C.R2GC_172_49,(0,4,0):C.R2GC_142_34,(0,3,0):C.R2GC_141_33,(0,2,0):C.R2GC_173_50,(0,1,0):C.R2GC_144_36,(0,0,0):C.R2GC_143_35})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                lorentz = [ L.FFVV1, L.FFVV12, L.FFVV13, L.FFVV2, L.FFVV3, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,3,0):C.R2GC_66_87,(2,3,0):C.R2GC_199_72,(1,3,0):C.R2GC_198_71,(0,0,0):C.R2GC_66_87,(2,0,0):C.R2GC_69_90,(1,0,0):C.R2GC_70_91,(0,4,0):C.R2GC_68_89,(2,4,0):C.R2GC_67_88,(1,4,0):C.R2GC_67_88,(2,6,0):C.R2GC_70_91,(1,6,0):C.R2GC_69_90,(2,5,0):C.R2GC_198_71,(1,5,0):C.R2GC_199_72,(2,2,0):C.R2GC_70_91,(1,2,0):C.R2GC_69_90,(2,1,0):C.R2GC_198_71,(1,1,0):C.R2GC_199_72})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV7, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_217_81,(0,1,0):C.R2GC_215_79,(0,0,0):C.R2GC_79_100})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV11, L.FFVV12 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.R2GC_216_80,(0,0,0):C.R2GC_78_99})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV7, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_202_73,(0,1,0):C.R2GC_205_76,(0,0,0):C.R2GC_107_6})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV11, L.FFVV12, L.FFVV13 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,2,0):C.R2GC_204_75,(0,1,0):C.R2GC_203_74,(0,0,0):C.R2GC_106_5})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF4 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_73_94,(0,1,1):C.R2GC_188_65,(0,0,0):C.R2GC_165_48,(0,0,1):C.R2GC_100_1,(0,2,0):C.R2GC_61_82})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_100_1})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV2, L.VV3, L.VV4 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,2,1):C.R2GC_88_109,(0,0,2):C.R2GC_89_110,(0,1,0):C.R2GC_116_15})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.g, P.g, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_113_12,(0,0,1):C.R2GC_119_20,(0,0,2):C.R2GC_119_21})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV5 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_63_84})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV5 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_112_11})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV5 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_115_14})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a, P.g ],
                color = [ 'T(4,2,1)' ],
                lorentz = [ L.FFVV6 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_65_86})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g, P.Z ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV14, L.FFVV16 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_82_103,(0,1,0):C.R2GC_86_107})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.g, P.W__plus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV15 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_80_101})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.g, P.W__minus__ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFVV10 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.R2GC_81_102})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_117_16,(0,0,1):C.R2GC_117_17})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_110_9,(0,0,1):C.R2GC_120_22,(0,0,2):C.R2GC_120_23})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_108_7,(0,0,1):C.R2GC_123_28,(0,0,2):C.R2GC_123_29})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_74_95,(0,0,1):C.R2GC_127_32})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_118_18,(0,0,1):C.R2GC_118_19})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV10 ],
                loop_particles = [ [ [] ], [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_114_13,(1,0,1):C.R2GC_122_26,(1,0,2):C.R2GC_122_27,(0,1,0):C.R2GC_111_10,(0,1,1):C.R2GC_121_24,(0,1,2):C.R2GC_121_25})

V_54 = CTVertex(name = 'V_54',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV2 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_193_69,(0,0,1):C.UVGC_180_45,(0,0,2):C.UVGC_180_46,(0,0,3):C.UVGC_180_47,(0,0,4):C.UVGC_180_48})

V_55 = CTVertex(name = 'V_55',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(7,1,0):C.UVGC_195_71,(7,1,1):C.UVGC_183_55,(7,1,2):C.UVGC_183_56,(7,1,3):C.UVGC_183_57,(7,1,4):C.UVGC_183_58,(6,1,0):C.UVGC_195_71,(6,1,1):C.UVGC_183_55,(6,1,2):C.UVGC_184_59,(6,1,3):C.UVGC_184_60,(6,1,4):C.UVGC_183_58,(2,1,2):C.UVGC_149_16,(2,1,3):C.UVGC_149_15,(0,1,2):C.UVGC_149_16,(0,1,3):C.UVGC_149_15,(4,1,2):C.UVGC_148_13,(4,1,3):C.UVGC_148_14,(3,1,2):C.UVGC_148_13,(3,1,3):C.UVGC_148_14,(8,1,2):C.UVGC_149_15,(8,1,3):C.UVGC_149_16,(5,1,2):C.UVGC_148_13,(5,1,3):C.UVGC_148_14,(1,1,2):C.UVGC_148_13,(1,1,3):C.UVGC_148_14,(11,0,2):C.UVGC_152_19,(11,0,3):C.UVGC_152_20,(10,0,2):C.UVGC_152_19,(10,0,3):C.UVGC_152_20,(9,0,2):C.UVGC_151_17,(9,0,3):C.UVGC_151_18,(6,2,0):C.UVGC_194_70,(6,2,1):C.UVGC_181_49,(6,2,2):C.UVGC_182_53,(6,2,3):C.UVGC_182_54,(6,2,4):C.UVGC_181_52,(8,2,0):C.UVGC_195_71,(8,2,1):C.UVGC_183_55,(8,2,2):C.UVGC_183_56,(8,2,3):C.UVGC_183_57,(8,2,4):C.UVGC_183_58,(2,2,2):C.UVGC_149_16,(2,2,3):C.UVGC_149_15,(0,2,2):C.UVGC_149_16,(0,2,3):C.UVGC_149_15,(4,2,2):C.UVGC_148_13,(4,2,3):C.UVGC_148_14,(3,2,2):C.UVGC_148_13,(3,2,3):C.UVGC_148_14,(5,2,2):C.UVGC_148_13,(5,2,3):C.UVGC_148_14,(1,2,2):C.UVGC_148_13,(1,2,3):C.UVGC_148_14,(7,2,2):C.UVGC_149_15,(7,2,3):C.UVGC_149_16,(7,3,0):C.UVGC_194_70,(7,3,1):C.UVGC_181_49,(7,3,2):C.UVGC_181_50,(7,3,3):C.UVGC_181_51,(7,3,4):C.UVGC_181_52,(8,3,0):C.UVGC_194_70,(8,3,1):C.UVGC_181_49,(8,3,2):C.UVGC_181_50,(8,3,3):C.UVGC_181_51,(8,3,4):C.UVGC_181_52,(2,3,2):C.UVGC_149_16,(2,3,3):C.UVGC_149_15,(0,3,2):C.UVGC_149_16,(0,3,3):C.UVGC_149_15,(4,3,2):C.UVGC_148_13,(4,3,3):C.UVGC_148_14,(3,3,2):C.UVGC_148_13,(3,3,3):C.UVGC_148_14,(5,3,2):C.UVGC_148_13,(5,3,3):C.UVGC_148_14,(1,3,2):C.UVGC_148_13,(1,3,3):C.UVGC_148_14})

V_56 = CTVertex(name = 'V_56',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV18, L.FFV4, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_177_42,(0,3,1):C.UVGC_190_66,(0,1,1):C.UVGC_191_67,(0,1,0):C.UVGC_178_43,(0,0,0):C.UVGC_176_41,(0,2,0):C.UVGC_218_89})

V_57 = CTVertex(name = 'V_57',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_211_84})

V_58 = CTVertex(name = 'V_58',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV10, L.FFV11, L.FFV27, L.FFV5 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_175_40,(0,3,1):C.UVGC_161_26,(0,3,3):C.UVGC_189_65,(0,3,2):C.UVGC_161_27,(0,1,0):C.UVGC_174_39,(0,0,0):C.UVGC_207_81,(0,2,0):C.UVGC_209_83})

V_59 = CTVertex(name = 'V_59',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_133_4})

V_60 = CTVertex(name = 'V_60',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV14 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_133_4})

V_61 = CTVertex(name = 'V_61',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV4 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_166_31,(0,0,1):C.UVGC_186_62,(0,1,0):C.UVGC_196_72})

V_62 = CTVertex(name = 'V_62',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_154_22,(0,0,1):C.UVGC_154_23,(0,0,2):C.UVGC_154_24,(0,0,3):C.UVGC_154_25})

V_63 = CTVertex(name = 'V_63',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_154_22,(0,0,2):C.UVGC_154_23,(0,0,3):C.UVGC_154_24,(0,0,1):C.UVGC_154_25})

V_64 = CTVertex(name = 'V_64',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV4 ],
                loop_particles = [ [ [] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_167_32,(0,0,1):C.UVGC_154_22,(0,0,2):C.UVGC_154_23,(0,0,3):C.UVGC_154_24,(0,0,4):C.UVGC_187_63,(0,1,0):C.UVGC_197_73})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_161_26,(0,0,1):C.UVGC_161_27})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_161_26,(0,0,1):C.UVGC_161_27})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV11, L.FFV19, L.FFV20, L.FFV5, L.FFV7, L.FFV9 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,3,0):C.UVGC_175_40,(0,3,1):C.UVGC_161_26,(0,3,3):C.UVGC_189_65,(0,3,2):C.UVGC_161_27,(0,0,0):C.UVGC_174_39,(0,5,0):C.UVGC_209_83,(0,2,0):C.UVGC_207_81,(0,4,0):C.UVGC_208_82,(0,1,0):C.UVGC_206_80})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_153_21,(0,2,0):C.UVGC_131_3,(0,1,0):C.UVGC_131_3})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV11, L.FFV5 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_153_21,(0,2,0):C.UVGC_131_3,(0,1,0):C.UVGC_131_3})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV11, L.FFV19, L.FFV3, L.FFV5, L.FFV7 ],
                loop_particles = [ [ [] ], [ [P.b, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_153_21,(0,3,0):C.UVGC_164_29,(0,4,1):C.UVGC_131_3,(0,1,1):C.UVGC_131_3,(0,5,0):C.UVGC_163_28,(0,2,0):C.UVGC_163_28})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_154_22,(0,0,2):C.UVGC_154_23,(0,0,3):C.UVGC_154_24,(0,0,1):C.UVGC_154_25})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_154_22,(0,0,1):C.UVGC_154_23,(0,0,2):C.UVGC_154_24,(0,0,3):C.UVGC_154_25})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.b, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,0):C.UVGC_154_22,(0,0,2):C.UVGC_154_23,(0,0,3):C.UVGC_154_24,(0,0,1):C.UVGC_154_25})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_161_26,(0,0,1):C.UVGC_161_27})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV5 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_161_26,(0,0,1):C.UVGC_161_27})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV1, L.FFVV13, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_169_34,(0,2,0):C.UVGC_170_35,(0,1,0):C.UVGC_170_35})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV1, L.FFVV11, L.FFVV12, L.FFVV13, L.FFVV7, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_171_36,(0,6,0):C.UVGC_172_37,(0,5,0):C.UVGC_141_6,(0,4,0):C.UVGC_141_6,(0,3,0):C.UVGC_173_38,(0,2,0):C.UVGC_144_8,(0,1,0):C.UVGC_143_7})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g, P.g ],
                color = [ 'T(3,-1,1)*T(4,2,-1)', 'T(3,2,-1)*T(4,-1,1)' ],
                lorentz = [ L.FFVV12, L.FFVV2, L.FFVV8 ],
                loop_particles = [ [ [] ] ],
                couplings = {(1,1,0):C.UVGC_199_75,(0,1,0):C.UVGC_198_74,(1,2,0):C.UVGC_198_74,(0,2,0):C.UVGC_199_75,(1,0,0):C.UVGC_198_74,(0,0,0):C.UVGC_199_75})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.a, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,3,0):C.UVGC_216_88,(0,2,0):C.UVGC_214_87,(0,1,0):C.UVGC_212_85,(0,0,0):C.UVGC_213_86})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.a, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_216_88,(0,3,0):C.UVGC_213_86,(0,2,0):C.UVGC_212_85,(0,1,0):C.UVGC_214_87})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,3,0):C.UVGC_202_78,(0,2,0):C.UVGC_204_79,(0,1,0):C.UVGC_201_77,(0,0,0):C.UVGC_200_76})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV12, L.FFVV13, L.FFVV8, L.FFVV9 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,1,0):C.UVGC_204_79,(0,0,0):C.UVGC_202_78,(0,3,0):C.UVGC_200_76,(0,2,0):C.UVGC_201_77})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_130_2})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_130_2})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF2, L.FF3, L.FF5, L.FF7 ],
                loop_particles = [ [ [] ], [ [P.g, P.t] ] ],
                couplings = {(0,2,1):C.UVGC_188_64,(0,0,0):C.UVGC_165_30,(0,1,0):C.UVGC_168_33,(0,3,1):C.UVGC_185_61,(0,4,1):C.UVGC_138_5})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_130_2})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_130_2})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF6 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_130_2})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV5 ],
                loop_particles = [ [ [] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,1,0):C.UVGC_192_68,(0,1,3):C.UVGC_179_44,(0,0,1):C.UVGC_147_11,(0,0,2):C.UVGC_147_12})

V_90 = CTVertex(name = 'V_90',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_129_1})

V_91 = CTVertex(name = 'V_91',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_145_9})

V_92 = CTVertex(name = 'V_92',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFVV4 ],
                loop_particles = [ [ [] ] ],
                couplings = {(0,0,0):C.UVGC_146_10})

