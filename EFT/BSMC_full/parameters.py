# This file was automatically created by FeynRules 2.4.61
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Tue 17 Jul 2018 21:06:45



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cll1122 = Parameter(name = 'cll1122',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = 'c_{\\text{ll},1122}',
                    lhablock = 'BCx4f',
                    lhacode = [ 1 ])

cpuu3333 = Parameter(name = 'cpuu3333',
                     nature = 'external',
                     type = 'real',
                     value = 0.1,
                     texname = '\\text{c\'}_{\\text{uu},3333}',
                     lhablock = 'BCx4f',
                     lhacode = [ 2 ])

cll1221 = Parameter(name = 'cll1221',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = 'c_{\\text{ll},1221}',
                    lhablock = 'BCx4f',
                    lhacode = [ 3 ])

Rdad1x1 = Parameter(name = 'Rdad1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad1x1}',
                    lhablock = 'BCxdad',
                    lhacode = [ 1, 1 ])

Rdad1x2 = Parameter(name = 'Rdad1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad1x2}',
                    lhablock = 'BCxdad',
                    lhacode = [ 1, 2 ])

Rdad1x3 = Parameter(name = 'Rdad1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad1x3}',
                    lhablock = 'BCxdad',
                    lhacode = [ 1, 3 ])

Rdad2x1 = Parameter(name = 'Rdad2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad2x1}',
                    lhablock = 'BCxdad',
                    lhacode = [ 2, 1 ])

Rdad2x2 = Parameter(name = 'Rdad2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad2x2}',
                    lhablock = 'BCxdad',
                    lhacode = [ 2, 2 ])

Rdad2x3 = Parameter(name = 'Rdad2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad2x3}',
                    lhablock = 'BCxdad',
                    lhacode = [ 2, 3 ])

Rdad3x1 = Parameter(name = 'Rdad3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad3x1}',
                    lhablock = 'BCxdad',
                    lhacode = [ 3, 1 ])

Rdad3x2 = Parameter(name = 'Rdad3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad3x2}',
                    lhablock = 'BCxdad',
                    lhacode = [ 3, 2 ])

Rdad3x3 = Parameter(name = 'Rdad3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdad3x3}',
                    lhablock = 'BCxdad',
                    lhacode = [ 3, 3 ])

Rdae1x1 = Parameter(name = 'Rdae1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae1x1}',
                    lhablock = 'BCxdae',
                    lhacode = [ 1, 1 ])

Rdae1x2 = Parameter(name = 'Rdae1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae1x2}',
                    lhablock = 'BCxdae',
                    lhacode = [ 1, 2 ])

Rdae1x3 = Parameter(name = 'Rdae1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae1x3}',
                    lhablock = 'BCxdae',
                    lhacode = [ 1, 3 ])

Rdae2x1 = Parameter(name = 'Rdae2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae2x1}',
                    lhablock = 'BCxdae',
                    lhacode = [ 2, 1 ])

Rdae2x2 = Parameter(name = 'Rdae2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae2x2}',
                    lhablock = 'BCxdae',
                    lhacode = [ 2, 2 ])

Rdae2x3 = Parameter(name = 'Rdae2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae2x3}',
                    lhablock = 'BCxdae',
                    lhacode = [ 2, 3 ])

Rdae3x1 = Parameter(name = 'Rdae3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae3x1}',
                    lhablock = 'BCxdae',
                    lhacode = [ 3, 1 ])

Rdae3x2 = Parameter(name = 'Rdae3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae3x2}',
                    lhablock = 'BCxdae',
                    lhacode = [ 3, 2 ])

Rdae3x3 = Parameter(name = 'Rdae3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdae3x3}',
                    lhablock = 'BCxdae',
                    lhacode = [ 3, 3 ])

Rdau1x1 = Parameter(name = 'Rdau1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau1x1}',
                    lhablock = 'BCxdau',
                    lhacode = [ 1, 1 ])

Rdau1x2 = Parameter(name = 'Rdau1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau1x2}',
                    lhablock = 'BCxdau',
                    lhacode = [ 1, 2 ])

Rdau1x3 = Parameter(name = 'Rdau1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau1x3}',
                    lhablock = 'BCxdau',
                    lhacode = [ 1, 3 ])

Rdau2x1 = Parameter(name = 'Rdau2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau2x1}',
                    lhablock = 'BCxdau',
                    lhacode = [ 2, 1 ])

Rdau2x2 = Parameter(name = 'Rdau2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau2x2}',
                    lhablock = 'BCxdau',
                    lhacode = [ 2, 2 ])

Rdau2x3 = Parameter(name = 'Rdau2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau2x3}',
                    lhablock = 'BCxdau',
                    lhacode = [ 2, 3 ])

Rdau3x1 = Parameter(name = 'Rdau3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau3x1}',
                    lhablock = 'BCxdau',
                    lhacode = [ 3, 1 ])

Rdau3x2 = Parameter(name = 'Rdau3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau3x2}',
                    lhablock = 'BCxdau',
                    lhacode = [ 3, 2 ])

Rdau3x3 = Parameter(name = 'Rdau3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdau3x3}',
                    lhablock = 'BCxdau',
                    lhacode = [ 3, 3 ])

Rdgd1x1 = Parameter(name = 'Rdgd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd1x1}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 1, 1 ])

Rdgd1x2 = Parameter(name = 'Rdgd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd1x2}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 1, 2 ])

Rdgd1x3 = Parameter(name = 'Rdgd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd1x3}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 1, 3 ])

Rdgd2x1 = Parameter(name = 'Rdgd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd2x1}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 2, 1 ])

Rdgd2x2 = Parameter(name = 'Rdgd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd2x2}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 2, 2 ])

Rdgd2x3 = Parameter(name = 'Rdgd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd2x3}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 2, 3 ])

Rdgd3x1 = Parameter(name = 'Rdgd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd3x1}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 3, 1 ])

Rdgd3x2 = Parameter(name = 'Rdgd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd3x2}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 3, 2 ])

Rdgd3x3 = Parameter(name = 'Rdgd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgd3x3}',
                    lhablock = 'BCxdgd',
                    lhacode = [ 3, 3 ])

RdGLhwl1x1 = Parameter(name = 'RdGLhwl1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl1x1}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 1, 1 ])

RdGLhwl1x2 = Parameter(name = 'RdGLhwl1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl1x2}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 1, 2 ])

RdGLhwl1x3 = Parameter(name = 'RdGLhwl1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl1x3}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 1, 3 ])

RdGLhwl2x1 = Parameter(name = 'RdGLhwl2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl2x1}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 2, 1 ])

RdGLhwl2x2 = Parameter(name = 'RdGLhwl2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl2x2}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 2, 2 ])

RdGLhwl2x3 = Parameter(name = 'RdGLhwl2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl2x3}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 2, 3 ])

RdGLhwl3x1 = Parameter(name = 'RdGLhwl3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl3x1}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 3, 1 ])

RdGLhwl3x2 = Parameter(name = 'RdGLhwl3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl3x2}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 3, 2 ])

RdGLhwl3x3 = Parameter(name = 'RdGLhwl3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwl3x3}',
                       lhablock = 'BCxdGLhWl',
                       lhacode = [ 3, 3 ])

RdGLhwq1x1 = Parameter(name = 'RdGLhwq1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq1x1}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 1, 1 ])

RdGLhwq1x2 = Parameter(name = 'RdGLhwq1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq1x2}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 1, 2 ])

RdGLhwq1x3 = Parameter(name = 'RdGLhwq1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq1x3}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 1, 3 ])

RdGLhwq2x1 = Parameter(name = 'RdGLhwq2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq2x1}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 2, 1 ])

RdGLhwq2x2 = Parameter(name = 'RdGLhwq2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq2x2}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 2, 2 ])

RdGLhwq2x3 = Parameter(name = 'RdGLhwq2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq2x3}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 2, 3 ])

RdGLhwq3x1 = Parameter(name = 'RdGLhwq3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq3x1}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 3, 1 ])

RdGLhwq3x2 = Parameter(name = 'RdGLhwq3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq3x2}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 3, 2 ])

RdGLhwq3x3 = Parameter(name = 'RdGLhwq3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhwq3x3}',
                       lhablock = 'BCxdGLhWq',
                       lhacode = [ 3, 3 ])

RdGLhzd1x1 = Parameter(name = 'RdGLhzd1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd1x1}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 1, 1 ])

RdGLhzd1x2 = Parameter(name = 'RdGLhzd1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd1x2}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 1, 2 ])

RdGLhzd1x3 = Parameter(name = 'RdGLhzd1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd1x3}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 1, 3 ])

RdGLhzd2x1 = Parameter(name = 'RdGLhzd2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd2x1}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 2, 1 ])

RdGLhzd2x2 = Parameter(name = 'RdGLhzd2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd2x2}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 2, 2 ])

RdGLhzd2x3 = Parameter(name = 'RdGLhzd2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd2x3}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 2, 3 ])

RdGLhzd3x1 = Parameter(name = 'RdGLhzd3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd3x1}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 3, 1 ])

RdGLhzd3x2 = Parameter(name = 'RdGLhzd3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd3x2}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 3, 2 ])

RdGLhzd3x3 = Parameter(name = 'RdGLhzd3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzd3x3}',
                       lhablock = 'BCxdGLhzd',
                       lhacode = [ 3, 3 ])

RdGLhze1x1 = Parameter(name = 'RdGLhze1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze1x1}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 1, 1 ])

RdGLhze1x2 = Parameter(name = 'RdGLhze1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze1x2}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 1, 2 ])

RdGLhze1x3 = Parameter(name = 'RdGLhze1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze1x3}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 1, 3 ])

RdGLhze2x1 = Parameter(name = 'RdGLhze2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze2x1}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 2, 1 ])

RdGLhze2x2 = Parameter(name = 'RdGLhze2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze2x2}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 2, 2 ])

RdGLhze2x3 = Parameter(name = 'RdGLhze2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze2x3}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 2, 3 ])

RdGLhze3x1 = Parameter(name = 'RdGLhze3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze3x1}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 3, 1 ])

RdGLhze3x2 = Parameter(name = 'RdGLhze3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze3x2}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 3, 2 ])

RdGLhze3x3 = Parameter(name = 'RdGLhze3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhze3x3}',
                       lhablock = 'BCxdGLhze',
                       lhacode = [ 3, 3 ])

RdGLhzu1x1 = Parameter(name = 'RdGLhzu1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu1x1}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 1, 1 ])

RdGLhzu1x2 = Parameter(name = 'RdGLhzu1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu1x2}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 1, 2 ])

RdGLhzu1x3 = Parameter(name = 'RdGLhzu1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu1x3}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 1, 3 ])

RdGLhzu2x1 = Parameter(name = 'RdGLhzu2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu2x1}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 2, 1 ])

RdGLhzu2x2 = Parameter(name = 'RdGLhzu2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu2x2}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 2, 2 ])

RdGLhzu2x3 = Parameter(name = 'RdGLhzu2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu2x3}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 2, 3 ])

RdGLhzu3x1 = Parameter(name = 'RdGLhzu3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu3x1}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 3, 1 ])

RdGLhzu3x2 = Parameter(name = 'RdGLhzu3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu3x2}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 3, 2 ])

RdGLhzu3x3 = Parameter(name = 'RdGLhzu3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzu3x3}',
                       lhablock = 'BCxdGLhzu',
                       lhacode = [ 3, 3 ])

RdGLhzv1x1 = Parameter(name = 'RdGLhzv1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv1x1}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 1, 1 ])

RdGLhzv1x2 = Parameter(name = 'RdGLhzv1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv1x2}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 1, 2 ])

RdGLhzv1x3 = Parameter(name = 'RdGLhzv1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv1x3}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 1, 3 ])

RdGLhzv2x1 = Parameter(name = 'RdGLhzv2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv2x1}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 2, 1 ])

RdGLhzv2x2 = Parameter(name = 'RdGLhzv2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv2x2}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 2, 2 ])

RdGLhzv2x3 = Parameter(name = 'RdGLhzv2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv2x3}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 2, 3 ])

RdGLhzv3x1 = Parameter(name = 'RdGLhzv3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv3x1}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 3, 1 ])

RdGLhzv3x2 = Parameter(name = 'RdGLhzv3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv3x2}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 3, 2 ])

RdGLhzv3x3 = Parameter(name = 'RdGLhzv3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGLhzv3x3}',
                       lhablock = 'BCxdGLhzv',
                       lhacode = [ 3, 3 ])

RdGLwl1x1 = Parameter(name = 'RdGLwl1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl1x1}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 1, 1 ])

RdGLwl1x2 = Parameter(name = 'RdGLwl1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl1x2}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 1, 2 ])

RdGLwl1x3 = Parameter(name = 'RdGLwl1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl1x3}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 1, 3 ])

RdGLwl2x1 = Parameter(name = 'RdGLwl2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl2x1}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 2, 1 ])

RdGLwl2x2 = Parameter(name = 'RdGLwl2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl2x2}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 2, 2 ])

RdGLwl2x3 = Parameter(name = 'RdGLwl2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl2x3}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 2, 3 ])

RdGLwl3x1 = Parameter(name = 'RdGLwl3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl3x1}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 3, 1 ])

RdGLwl3x2 = Parameter(name = 'RdGLwl3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl3x2}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 3, 2 ])

RdGLwl3x3 = Parameter(name = 'RdGLwl3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwl3x3}',
                      lhablock = 'BCxdGLWl',
                      lhacode = [ 3, 3 ])

RdGLwq1x1 = Parameter(name = 'RdGLwq1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq1x1}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 1, 1 ])

RdGLwq1x2 = Parameter(name = 'RdGLwq1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq1x2}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 1, 2 ])

RdGLwq1x3 = Parameter(name = 'RdGLwq1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq1x3}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 1, 3 ])

RdGLwq2x1 = Parameter(name = 'RdGLwq2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq2x1}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 2, 1 ])

RdGLwq2x2 = Parameter(name = 'RdGLwq2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq2x2}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 2, 2 ])

RdGLwq2x3 = Parameter(name = 'RdGLwq2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq2x3}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 2, 3 ])

RdGLwq3x1 = Parameter(name = 'RdGLwq3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq3x1}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 3, 1 ])

RdGLwq3x2 = Parameter(name = 'RdGLwq3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq3x2}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 3, 2 ])

RdGLwq3x3 = Parameter(name = 'RdGLwq3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLwq3x3}',
                      lhablock = 'BCxdGLWq',
                      lhacode = [ 3, 3 ])

RdGLzd1x1 = Parameter(name = 'RdGLzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd1x1}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 1, 1 ])

RdGLzd1x2 = Parameter(name = 'RdGLzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd1x2}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 1, 2 ])

RdGLzd1x3 = Parameter(name = 'RdGLzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd1x3}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 1, 3 ])

RdGLzd2x1 = Parameter(name = 'RdGLzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd2x1}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 2, 1 ])

RdGLzd2x2 = Parameter(name = 'RdGLzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd2x2}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 2, 2 ])

RdGLzd2x3 = Parameter(name = 'RdGLzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd2x3}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 2, 3 ])

RdGLzd3x1 = Parameter(name = 'RdGLzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd3x1}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 3, 1 ])

RdGLzd3x2 = Parameter(name = 'RdGLzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd3x2}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 3, 2 ])

RdGLzd3x3 = Parameter(name = 'RdGLzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzd3x3}',
                      lhablock = 'BCxdGLzd',
                      lhacode = [ 3, 3 ])

RdGLze1x1 = Parameter(name = 'RdGLze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze1x1}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 1, 1 ])

RdGLze1x2 = Parameter(name = 'RdGLze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze1x2}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 1, 2 ])

RdGLze1x3 = Parameter(name = 'RdGLze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze1x3}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 1, 3 ])

RdGLze2x1 = Parameter(name = 'RdGLze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze2x1}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 2, 1 ])

RdGLze2x2 = Parameter(name = 'RdGLze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze2x2}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 2, 2 ])

RdGLze2x3 = Parameter(name = 'RdGLze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze2x3}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 2, 3 ])

RdGLze3x1 = Parameter(name = 'RdGLze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze3x1}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 3, 1 ])

RdGLze3x2 = Parameter(name = 'RdGLze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze3x2}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 3, 2 ])

RdGLze3x3 = Parameter(name = 'RdGLze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLze3x3}',
                      lhablock = 'BCxdGLze',
                      lhacode = [ 3, 3 ])

RdGLzu1x1 = Parameter(name = 'RdGLzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu1x1}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 1, 1 ])

RdGLzu1x2 = Parameter(name = 'RdGLzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu1x2}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 1, 2 ])

RdGLzu1x3 = Parameter(name = 'RdGLzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu1x3}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 1, 3 ])

RdGLzu2x1 = Parameter(name = 'RdGLzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu2x1}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 2, 1 ])

RdGLzu2x2 = Parameter(name = 'RdGLzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu2x2}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 2, 2 ])

RdGLzu2x3 = Parameter(name = 'RdGLzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu2x3}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 2, 3 ])

RdGLzu3x1 = Parameter(name = 'RdGLzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu3x1}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 3, 1 ])

RdGLzu3x2 = Parameter(name = 'RdGLzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu3x2}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 3, 2 ])

RdGLzu3x3 = Parameter(name = 'RdGLzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzu3x3}',
                      lhablock = 'BCxdGLzu',
                      lhacode = [ 3, 3 ])

RdGLzv1x1 = Parameter(name = 'RdGLzv1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv1x1}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 1, 1 ])

RdGLzv1x2 = Parameter(name = 'RdGLzv1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv1x2}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 1, 2 ])

RdGLzv1x3 = Parameter(name = 'RdGLzv1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv1x3}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 1, 3 ])

RdGLzv2x1 = Parameter(name = 'RdGLzv2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv2x1}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 2, 1 ])

RdGLzv2x2 = Parameter(name = 'RdGLzv2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv2x2}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 2, 2 ])

RdGLzv2x3 = Parameter(name = 'RdGLzv2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv2x3}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 2, 3 ])

RdGLzv3x1 = Parameter(name = 'RdGLzv3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv3x1}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 3, 1 ])

RdGLzv3x2 = Parameter(name = 'RdGLzv3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv3x2}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 3, 2 ])

RdGLzv3x3 = Parameter(name = 'RdGLzv3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGLzv3x3}',
                      lhablock = 'BCxdGLzv',
                      lhacode = [ 3, 3 ])

RdGRhwq1x1 = Parameter(name = 'RdGRhwq1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq1x1}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 1, 1 ])

RdGRhwq1x2 = Parameter(name = 'RdGRhwq1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq1x2}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 1, 2 ])

RdGRhwq1x3 = Parameter(name = 'RdGRhwq1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq1x3}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 1, 3 ])

RdGRhwq2x1 = Parameter(name = 'RdGRhwq2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq2x1}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 2, 1 ])

RdGRhwq2x2 = Parameter(name = 'RdGRhwq2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq2x2}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 2, 2 ])

RdGRhwq2x3 = Parameter(name = 'RdGRhwq2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq2x3}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 2, 3 ])

RdGRhwq3x1 = Parameter(name = 'RdGRhwq3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq3x1}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 3, 1 ])

RdGRhwq3x2 = Parameter(name = 'RdGRhwq3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq3x2}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 3, 2 ])

RdGRhwq3x3 = Parameter(name = 'RdGRhwq3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhwq3x3}',
                       lhablock = 'BCxdGRhWq',
                       lhacode = [ 3, 3 ])

RdGRhzd1x1 = Parameter(name = 'RdGRhzd1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd1x1}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 1, 1 ])

RdGRhzd1x2 = Parameter(name = 'RdGRhzd1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd1x2}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 1, 2 ])

RdGRhzd1x3 = Parameter(name = 'RdGRhzd1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd1x3}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 1, 3 ])

RdGRhzd2x1 = Parameter(name = 'RdGRhzd2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd2x1}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 2, 1 ])

RdGRhzd2x2 = Parameter(name = 'RdGRhzd2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd2x2}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 2, 2 ])

RdGRhzd2x3 = Parameter(name = 'RdGRhzd2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd2x3}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 2, 3 ])

RdGRhzd3x1 = Parameter(name = 'RdGRhzd3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd3x1}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 3, 1 ])

RdGRhzd3x2 = Parameter(name = 'RdGRhzd3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd3x2}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 3, 2 ])

RdGRhzd3x3 = Parameter(name = 'RdGRhzd3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzd3x3}',
                       lhablock = 'BCxdGRhzd',
                       lhacode = [ 3, 3 ])

RdGRhze1x1 = Parameter(name = 'RdGRhze1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze1x1}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 1, 1 ])

RdGRhze1x2 = Parameter(name = 'RdGRhze1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze1x2}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 1, 2 ])

RdGRhze1x3 = Parameter(name = 'RdGRhze1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze1x3}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 1, 3 ])

RdGRhze2x1 = Parameter(name = 'RdGRhze2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze2x1}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 2, 1 ])

RdGRhze2x2 = Parameter(name = 'RdGRhze2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze2x2}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 2, 2 ])

RdGRhze2x3 = Parameter(name = 'RdGRhze2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze2x3}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 2, 3 ])

RdGRhze3x1 = Parameter(name = 'RdGRhze3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze3x1}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 3, 1 ])

RdGRhze3x2 = Parameter(name = 'RdGRhze3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze3x2}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 3, 2 ])

RdGRhze3x3 = Parameter(name = 'RdGRhze3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhze3x3}',
                       lhablock = 'BCxdGRhze',
                       lhacode = [ 3, 3 ])

RdGRhzu1x1 = Parameter(name = 'RdGRhzu1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu1x1}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 1, 1 ])

RdGRhzu1x2 = Parameter(name = 'RdGRhzu1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu1x2}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 1, 2 ])

RdGRhzu1x3 = Parameter(name = 'RdGRhzu1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu1x3}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 1, 3 ])

RdGRhzu2x1 = Parameter(name = 'RdGRhzu2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu2x1}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 2, 1 ])

RdGRhzu2x2 = Parameter(name = 'RdGRhzu2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu2x2}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 2, 2 ])

RdGRhzu2x3 = Parameter(name = 'RdGRhzu2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu2x3}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 2, 3 ])

RdGRhzu3x1 = Parameter(name = 'RdGRhzu3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu3x1}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 3, 1 ])

RdGRhzu3x2 = Parameter(name = 'RdGRhzu3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu3x2}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 3, 2 ])

RdGRhzu3x3 = Parameter(name = 'RdGRhzu3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.1,
                       texname = '\\text{RdGRhzu3x3}',
                       lhablock = 'BCxdGRhzu',
                       lhacode = [ 3, 3 ])

RdGRwq1x1 = Parameter(name = 'RdGRwq1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq1x1}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 1, 1 ])

RdGRwq1x2 = Parameter(name = 'RdGRwq1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq1x2}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 1, 2 ])

RdGRwq1x3 = Parameter(name = 'RdGRwq1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq1x3}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 1, 3 ])

RdGRwq2x1 = Parameter(name = 'RdGRwq2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq2x1}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 2, 1 ])

RdGRwq2x2 = Parameter(name = 'RdGRwq2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq2x2}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 2, 2 ])

RdGRwq2x3 = Parameter(name = 'RdGRwq2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq2x3}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 2, 3 ])

RdGRwq3x1 = Parameter(name = 'RdGRwq3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq3x1}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 3, 1 ])

RdGRwq3x2 = Parameter(name = 'RdGRwq3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq3x2}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 3, 2 ])

RdGRwq3x3 = Parameter(name = 'RdGRwq3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRwq3x3}',
                      lhablock = 'BCxdGRWq',
                      lhacode = [ 3, 3 ])

RdGRzd1x1 = Parameter(name = 'RdGRzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd1x1}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 1, 1 ])

RdGRzd1x2 = Parameter(name = 'RdGRzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd1x2}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 1, 2 ])

RdGRzd1x3 = Parameter(name = 'RdGRzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd1x3}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 1, 3 ])

RdGRzd2x1 = Parameter(name = 'RdGRzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd2x1}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 2, 1 ])

RdGRzd2x2 = Parameter(name = 'RdGRzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd2x2}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 2, 2 ])

RdGRzd2x3 = Parameter(name = 'RdGRzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd2x3}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 2, 3 ])

RdGRzd3x1 = Parameter(name = 'RdGRzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd3x1}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 3, 1 ])

RdGRzd3x2 = Parameter(name = 'RdGRzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd3x2}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 3, 2 ])

RdGRzd3x3 = Parameter(name = 'RdGRzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzd3x3}',
                      lhablock = 'BCxdGRzd',
                      lhacode = [ 3, 3 ])

RdGRze1x1 = Parameter(name = 'RdGRze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze1x1}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 1, 1 ])

RdGRze1x2 = Parameter(name = 'RdGRze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze1x2}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 1, 2 ])

RdGRze1x3 = Parameter(name = 'RdGRze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze1x3}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 1, 3 ])

RdGRze2x1 = Parameter(name = 'RdGRze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze2x1}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 2, 1 ])

RdGRze2x2 = Parameter(name = 'RdGRze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze2x2}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 2, 2 ])

RdGRze2x3 = Parameter(name = 'RdGRze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze2x3}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 2, 3 ])

RdGRze3x1 = Parameter(name = 'RdGRze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze3x1}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 3, 1 ])

RdGRze3x2 = Parameter(name = 'RdGRze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze3x2}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 3, 2 ])

RdGRze3x3 = Parameter(name = 'RdGRze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRze3x3}',
                      lhablock = 'BCxdGRze',
                      lhacode = [ 3, 3 ])

RdGRzu1x1 = Parameter(name = 'RdGRzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu1x1}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 1, 1 ])

RdGRzu1x2 = Parameter(name = 'RdGRzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu1x2}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 1, 2 ])

RdGRzu1x3 = Parameter(name = 'RdGRzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu1x3}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 1, 3 ])

RdGRzu2x1 = Parameter(name = 'RdGRzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu2x1}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 2, 1 ])

RdGRzu2x2 = Parameter(name = 'RdGRzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu2x2}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 2, 2 ])

RdGRzu2x3 = Parameter(name = 'RdGRzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu2x3}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 2, 3 ])

RdGRzu3x1 = Parameter(name = 'RdGRzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu3x1}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 3, 1 ])

RdGRzu3x2 = Parameter(name = 'RdGRzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu3x2}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 3, 2 ])

RdGRzu3x3 = Parameter(name = 'RdGRzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{RdGRzu3x3}',
                      lhablock = 'BCxdGRzu',
                      lhacode = [ 3, 3 ])

Rdgu1x1 = Parameter(name = 'Rdgu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu1x1}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 1, 1 ])

Rdgu1x2 = Parameter(name = 'Rdgu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu1x2}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 1, 2 ])

Rdgu1x3 = Parameter(name = 'Rdgu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu1x3}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 1, 3 ])

Rdgu2x1 = Parameter(name = 'Rdgu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu2x1}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 2, 1 ])

Rdgu2x2 = Parameter(name = 'Rdgu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu2x2}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 2, 2 ])

Rdgu2x3 = Parameter(name = 'Rdgu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu2x3}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 2, 3 ])

Rdgu3x1 = Parameter(name = 'Rdgu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu3x1}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 3, 1 ])

Rdgu3x2 = Parameter(name = 'Rdgu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu3x2}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 3, 2 ])

Rdgu3x3 = Parameter(name = 'Rdgu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdgu3x3}',
                    lhablock = 'BCxdgu',
                    lhacode = [ 3, 3 ])

Rdhad1x1 = Parameter(name = 'Rdhad1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad1x1}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 1, 1 ])

Rdhad1x2 = Parameter(name = 'Rdhad1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad1x2}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 1, 2 ])

Rdhad1x3 = Parameter(name = 'Rdhad1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad1x3}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 1, 3 ])

Rdhad2x1 = Parameter(name = 'Rdhad2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad2x1}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 2, 1 ])

Rdhad2x2 = Parameter(name = 'Rdhad2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad2x2}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 2, 2 ])

Rdhad2x3 = Parameter(name = 'Rdhad2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad2x3}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 2, 3 ])

Rdhad3x1 = Parameter(name = 'Rdhad3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad3x1}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 3, 1 ])

Rdhad3x2 = Parameter(name = 'Rdhad3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad3x2}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 3, 2 ])

Rdhad3x3 = Parameter(name = 'Rdhad3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhad3x3}',
                     lhablock = 'BCxdhad',
                     lhacode = [ 3, 3 ])

Rdhae1x1 = Parameter(name = 'Rdhae1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae1x1}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 1, 1 ])

Rdhae1x2 = Parameter(name = 'Rdhae1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae1x2}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 1, 2 ])

Rdhae1x3 = Parameter(name = 'Rdhae1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae1x3}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 1, 3 ])

Rdhae2x1 = Parameter(name = 'Rdhae2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae2x1}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 2, 1 ])

Rdhae2x2 = Parameter(name = 'Rdhae2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae2x2}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 2, 2 ])

Rdhae2x3 = Parameter(name = 'Rdhae2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae2x3}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 2, 3 ])

Rdhae3x1 = Parameter(name = 'Rdhae3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae3x1}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 3, 1 ])

Rdhae3x2 = Parameter(name = 'Rdhae3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae3x2}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 3, 2 ])

Rdhae3x3 = Parameter(name = 'Rdhae3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhae3x3}',
                     lhablock = 'BCxdhae',
                     lhacode = [ 3, 3 ])

Rdhau1x1 = Parameter(name = 'Rdhau1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau1x1}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 1, 1 ])

Rdhau1x2 = Parameter(name = 'Rdhau1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau1x2}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 1, 2 ])

Rdhau1x3 = Parameter(name = 'Rdhau1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau1x3}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 1, 3 ])

Rdhau2x1 = Parameter(name = 'Rdhau2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau2x1}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 2, 1 ])

Rdhau2x2 = Parameter(name = 'Rdhau2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau2x2}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 2, 2 ])

Rdhau2x3 = Parameter(name = 'Rdhau2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau2x3}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 2, 3 ])

Rdhau3x1 = Parameter(name = 'Rdhau3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau3x1}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 3, 1 ])

Rdhau3x2 = Parameter(name = 'Rdhau3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau3x2}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 3, 2 ])

Rdhau3x3 = Parameter(name = 'Rdhau3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhau3x3}',
                     lhablock = 'BCxdhau',
                     lhacode = [ 3, 3 ])

Rdhgd1x1 = Parameter(name = 'Rdhgd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd1x1}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 1, 1 ])

Rdhgd1x2 = Parameter(name = 'Rdhgd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd1x2}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 1, 2 ])

Rdhgd1x3 = Parameter(name = 'Rdhgd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd1x3}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 1, 3 ])

Rdhgd2x1 = Parameter(name = 'Rdhgd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd2x1}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 2, 1 ])

Rdhgd2x2 = Parameter(name = 'Rdhgd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd2x2}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 2, 2 ])

Rdhgd2x3 = Parameter(name = 'Rdhgd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd2x3}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 2, 3 ])

Rdhgd3x1 = Parameter(name = 'Rdhgd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd3x1}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 3, 1 ])

Rdhgd3x2 = Parameter(name = 'Rdhgd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd3x2}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 3, 2 ])

Rdhgd3x3 = Parameter(name = 'Rdhgd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgd3x3}',
                     lhablock = 'BCxdhgd',
                     lhacode = [ 3, 3 ])

Rdhgu1x1 = Parameter(name = 'Rdhgu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu1x1}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 1, 1 ])

Rdhgu1x2 = Parameter(name = 'Rdhgu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu1x2}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 1, 2 ])

Rdhgu1x3 = Parameter(name = 'Rdhgu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu1x3}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 1, 3 ])

Rdhgu2x1 = Parameter(name = 'Rdhgu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu2x1}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 2, 1 ])

Rdhgu2x2 = Parameter(name = 'Rdhgu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu2x2}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 2, 2 ])

Rdhgu2x3 = Parameter(name = 'Rdhgu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu2x3}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 2, 3 ])

Rdhgu3x1 = Parameter(name = 'Rdhgu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu3x1}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 3, 1 ])

Rdhgu3x2 = Parameter(name = 'Rdhgu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu3x2}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 3, 2 ])

Rdhgu3x3 = Parameter(name = 'Rdhgu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhgu3x3}',
                     lhablock = 'BCxdhgu',
                     lhacode = [ 3, 3 ])

Rdhwd1x1 = Parameter(name = 'Rdhwd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd1x1}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 1, 1 ])

Rdhwd1x2 = Parameter(name = 'Rdhwd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd1x2}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 1, 2 ])

Rdhwd1x3 = Parameter(name = 'Rdhwd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd1x3}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 1, 3 ])

Rdhwd2x1 = Parameter(name = 'Rdhwd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd2x1}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 2, 1 ])

Rdhwd2x2 = Parameter(name = 'Rdhwd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd2x2}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 2, 2 ])

Rdhwd2x3 = Parameter(name = 'Rdhwd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd2x3}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 2, 3 ])

Rdhwd3x1 = Parameter(name = 'Rdhwd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd3x1}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 3, 1 ])

Rdhwd3x2 = Parameter(name = 'Rdhwd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd3x2}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 3, 2 ])

Rdhwd3x3 = Parameter(name = 'Rdhwd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwd3x3}',
                     lhablock = 'BCxdhwd',
                     lhacode = [ 3, 3 ])

Rdhwl1x1 = Parameter(name = 'Rdhwl1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl1x1}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 1, 1 ])

Rdhwl1x2 = Parameter(name = 'Rdhwl1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl1x2}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 1, 2 ])

Rdhwl1x3 = Parameter(name = 'Rdhwl1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl1x3}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 1, 3 ])

Rdhwl2x1 = Parameter(name = 'Rdhwl2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl2x1}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 2, 1 ])

Rdhwl2x2 = Parameter(name = 'Rdhwl2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl2x2}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 2, 2 ])

Rdhwl2x3 = Parameter(name = 'Rdhwl2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl2x3}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 2, 3 ])

Rdhwl3x1 = Parameter(name = 'Rdhwl3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl3x1}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 3, 1 ])

Rdhwl3x2 = Parameter(name = 'Rdhwl3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl3x2}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 3, 2 ])

Rdhwl3x3 = Parameter(name = 'Rdhwl3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwl3x3}',
                     lhablock = 'BCxdhwl',
                     lhacode = [ 3, 3 ])

Rdhwu1x1 = Parameter(name = 'Rdhwu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu1x1}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 1, 1 ])

Rdhwu1x2 = Parameter(name = 'Rdhwu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu1x2}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 1, 2 ])

Rdhwu1x3 = Parameter(name = 'Rdhwu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu1x3}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 1, 3 ])

Rdhwu2x1 = Parameter(name = 'Rdhwu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu2x1}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 2, 1 ])

Rdhwu2x2 = Parameter(name = 'Rdhwu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu2x2}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 2, 2 ])

Rdhwu2x3 = Parameter(name = 'Rdhwu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu2x3}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 2, 3 ])

Rdhwu3x1 = Parameter(name = 'Rdhwu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu3x1}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 3, 1 ])

Rdhwu3x2 = Parameter(name = 'Rdhwu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu3x2}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 3, 2 ])

Rdhwu3x3 = Parameter(name = 'Rdhwu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhwu3x3}',
                     lhablock = 'BCxdhwu',
                     lhacode = [ 3, 3 ])

Rdhzd1x1 = Parameter(name = 'Rdhzd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd1x1}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 1, 1 ])

Rdhzd1x2 = Parameter(name = 'Rdhzd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd1x2}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 1, 2 ])

Rdhzd1x3 = Parameter(name = 'Rdhzd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd1x3}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 1, 3 ])

Rdhzd2x1 = Parameter(name = 'Rdhzd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd2x1}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 2, 1 ])

Rdhzd2x2 = Parameter(name = 'Rdhzd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd2x2}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 2, 2 ])

Rdhzd2x3 = Parameter(name = 'Rdhzd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd2x3}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 2, 3 ])

Rdhzd3x1 = Parameter(name = 'Rdhzd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd3x1}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 3, 1 ])

Rdhzd3x2 = Parameter(name = 'Rdhzd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd3x2}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 3, 2 ])

Rdhzd3x3 = Parameter(name = 'Rdhzd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzd3x3}',
                     lhablock = 'BCxdhzd',
                     lhacode = [ 3, 3 ])

Rdhze1x1 = Parameter(name = 'Rdhze1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze1x1}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 1, 1 ])

Rdhze1x2 = Parameter(name = 'Rdhze1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze1x2}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 1, 2 ])

Rdhze1x3 = Parameter(name = 'Rdhze1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze1x3}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 1, 3 ])

Rdhze2x1 = Parameter(name = 'Rdhze2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze2x1}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 2, 1 ])

Rdhze2x2 = Parameter(name = 'Rdhze2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze2x2}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 2, 2 ])

Rdhze2x3 = Parameter(name = 'Rdhze2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze2x3}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 2, 3 ])

Rdhze3x1 = Parameter(name = 'Rdhze3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze3x1}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 3, 1 ])

Rdhze3x2 = Parameter(name = 'Rdhze3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze3x2}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 3, 2 ])

Rdhze3x3 = Parameter(name = 'Rdhze3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhze3x3}',
                     lhablock = 'BCxdhze',
                     lhacode = [ 3, 3 ])

Rdhzu1x1 = Parameter(name = 'Rdhzu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu1x1}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 1, 1 ])

Rdhzu1x2 = Parameter(name = 'Rdhzu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu1x2}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 1, 2 ])

Rdhzu1x3 = Parameter(name = 'Rdhzu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu1x3}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 1, 3 ])

Rdhzu2x1 = Parameter(name = 'Rdhzu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu2x1}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 2, 1 ])

Rdhzu2x2 = Parameter(name = 'Rdhzu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu2x2}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 2, 2 ])

Rdhzu2x3 = Parameter(name = 'Rdhzu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu2x3}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 2, 3 ])

Rdhzu3x1 = Parameter(name = 'Rdhzu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu3x1}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 3, 1 ])

Rdhzu3x2 = Parameter(name = 'Rdhzu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu3x2}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 3, 2 ])

Rdhzu3x3 = Parameter(name = 'Rdhzu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rdhzu3x3}',
                     lhablock = 'BCxdhzu',
                     lhacode = [ 3, 3 ])

Rdwd1x1 = Parameter(name = 'Rdwd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd1x1}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 1, 1 ])

Rdwd1x2 = Parameter(name = 'Rdwd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd1x2}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 1, 2 ])

Rdwd1x3 = Parameter(name = 'Rdwd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd1x3}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 1, 3 ])

Rdwd2x1 = Parameter(name = 'Rdwd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd2x1}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 2, 1 ])

Rdwd2x2 = Parameter(name = 'Rdwd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd2x2}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 2, 2 ])

Rdwd2x3 = Parameter(name = 'Rdwd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd2x3}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 2, 3 ])

Rdwd3x1 = Parameter(name = 'Rdwd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd3x1}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 3, 1 ])

Rdwd3x2 = Parameter(name = 'Rdwd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd3x2}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 3, 2 ])

Rdwd3x3 = Parameter(name = 'Rdwd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwd3x3}',
                    lhablock = 'BCxdwd',
                    lhacode = [ 3, 3 ])

Rdwl1x1 = Parameter(name = 'Rdwl1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl1x1}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 1, 1 ])

Rdwl1x2 = Parameter(name = 'Rdwl1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl1x2}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 1, 2 ])

Rdwl1x3 = Parameter(name = 'Rdwl1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl1x3}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 1, 3 ])

Rdwl2x1 = Parameter(name = 'Rdwl2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl2x1}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 2, 1 ])

Rdwl2x2 = Parameter(name = 'Rdwl2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl2x2}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 2, 2 ])

Rdwl2x3 = Parameter(name = 'Rdwl2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl2x3}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 2, 3 ])

Rdwl3x1 = Parameter(name = 'Rdwl3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl3x1}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 3, 1 ])

Rdwl3x2 = Parameter(name = 'Rdwl3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl3x2}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 3, 2 ])

Rdwl3x3 = Parameter(name = 'Rdwl3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwl3x3}',
                    lhablock = 'BCxdwl',
                    lhacode = [ 3, 3 ])

Rdwu1x1 = Parameter(name = 'Rdwu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu1x1}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 1, 1 ])

Rdwu1x2 = Parameter(name = 'Rdwu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu1x2}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 1, 2 ])

Rdwu1x3 = Parameter(name = 'Rdwu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu1x3}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 1, 3 ])

Rdwu2x1 = Parameter(name = 'Rdwu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu2x1}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 2, 1 ])

Rdwu2x2 = Parameter(name = 'Rdwu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu2x2}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 2, 2 ])

Rdwu2x3 = Parameter(name = 'Rdwu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu2x3}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 2, 3 ])

Rdwu3x1 = Parameter(name = 'Rdwu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu3x1}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 3, 1 ])

Rdwu3x2 = Parameter(name = 'Rdwu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu3x2}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 3, 2 ])

Rdwu3x3 = Parameter(name = 'Rdwu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdwu3x3}',
                    lhablock = 'BCxdwu',
                    lhacode = [ 3, 3 ])

dYd1x1 = Parameter(name = 'dYd1x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd1x1}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 1, 1 ])

dYd1x2 = Parameter(name = 'dYd1x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd1x2}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 1, 2 ])

dYd1x3 = Parameter(name = 'dYd1x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd1x3}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 1, 3 ])

dYd2x1 = Parameter(name = 'dYd2x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd2x1}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 2, 1 ])

dYd2x2 = Parameter(name = 'dYd2x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd2x2}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 2, 2 ])

dYd2x3 = Parameter(name = 'dYd2x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd2x3}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 2, 3 ])

dYd3x1 = Parameter(name = 'dYd3x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd3x1}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 3, 1 ])

dYd3x2 = Parameter(name = 'dYd3x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd3x2}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 3, 2 ])

dYd3x3 = Parameter(name = 'dYd3x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYd3x3}',
                   lhablock = 'BCxdYd',
                   lhacode = [ 3, 3 ])

dYe1x1 = Parameter(name = 'dYe1x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe1x1}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 1, 1 ])

dYe1x2 = Parameter(name = 'dYe1x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe1x2}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 1, 2 ])

dYe1x3 = Parameter(name = 'dYe1x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe1x3}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 1, 3 ])

dYe2x1 = Parameter(name = 'dYe2x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe2x1}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 2, 1 ])

dYe2x2 = Parameter(name = 'dYe2x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe2x2}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 2, 2 ])

dYe2x3 = Parameter(name = 'dYe2x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe2x3}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 2, 3 ])

dYe3x1 = Parameter(name = 'dYe3x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe3x1}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 3, 1 ])

dYe3x2 = Parameter(name = 'dYe3x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe3x2}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 3, 2 ])

dYe3x3 = Parameter(name = 'dYe3x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYe3x3}',
                   lhablock = 'BCxdYe',
                   lhacode = [ 3, 3 ])

dYu1x1 = Parameter(name = 'dYu1x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu1x1}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 1, 1 ])

dYu1x2 = Parameter(name = 'dYu1x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu1x2}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 1, 2 ])

dYu1x3 = Parameter(name = 'dYu1x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu1x3}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 1, 3 ])

dYu2x1 = Parameter(name = 'dYu2x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu2x1}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 2, 1 ])

dYu2x2 = Parameter(name = 'dYu2x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu2x2}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 2, 2 ])

dYu2x3 = Parameter(name = 'dYu2x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu2x3}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 2, 3 ])

dYu3x1 = Parameter(name = 'dYu3x1',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu3x1}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 3, 1 ])

dYu3x2 = Parameter(name = 'dYu3x2',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu3x2}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 3, 2 ])

dYu3x3 = Parameter(name = 'dYu3x3',
                   nature = 'external',
                   type = 'complex',
                   value = 0.1,
                   texname = '\\text{dYu3x3}',
                   lhablock = 'BCxdYu',
                   lhacode = [ 3, 3 ])

Rdzd1x1 = Parameter(name = 'Rdzd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd1x1}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 1, 1 ])

Rdzd1x2 = Parameter(name = 'Rdzd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd1x2}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 1, 2 ])

Rdzd1x3 = Parameter(name = 'Rdzd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd1x3}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 1, 3 ])

Rdzd2x1 = Parameter(name = 'Rdzd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd2x1}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 2, 1 ])

Rdzd2x2 = Parameter(name = 'Rdzd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd2x2}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 2, 2 ])

Rdzd2x3 = Parameter(name = 'Rdzd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd2x3}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 2, 3 ])

Rdzd3x1 = Parameter(name = 'Rdzd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd3x1}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 3, 1 ])

Rdzd3x2 = Parameter(name = 'Rdzd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd3x2}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 3, 2 ])

Rdzd3x3 = Parameter(name = 'Rdzd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzd3x3}',
                    lhablock = 'BCxdzd',
                    lhacode = [ 3, 3 ])

Rdze1x1 = Parameter(name = 'Rdze1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze1x1}',
                    lhablock = 'BCxdze',
                    lhacode = [ 1, 1 ])

Rdze1x2 = Parameter(name = 'Rdze1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze1x2}',
                    lhablock = 'BCxdze',
                    lhacode = [ 1, 2 ])

Rdze1x3 = Parameter(name = 'Rdze1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze1x3}',
                    lhablock = 'BCxdze',
                    lhacode = [ 1, 3 ])

Rdze2x1 = Parameter(name = 'Rdze2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze2x1}',
                    lhablock = 'BCxdze',
                    lhacode = [ 2, 1 ])

Rdze2x2 = Parameter(name = 'Rdze2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze2x2}',
                    lhablock = 'BCxdze',
                    lhacode = [ 2, 2 ])

Rdze2x3 = Parameter(name = 'Rdze2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze2x3}',
                    lhablock = 'BCxdze',
                    lhacode = [ 2, 3 ])

Rdze3x1 = Parameter(name = 'Rdze3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze3x1}',
                    lhablock = 'BCxdze',
                    lhacode = [ 3, 1 ])

Rdze3x2 = Parameter(name = 'Rdze3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze3x2}',
                    lhablock = 'BCxdze',
                    lhacode = [ 3, 2 ])

Rdze3x3 = Parameter(name = 'Rdze3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdze3x3}',
                    lhablock = 'BCxdze',
                    lhacode = [ 3, 3 ])

Rdzu1x1 = Parameter(name = 'Rdzu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu1x1}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 1, 1 ])

Rdzu1x2 = Parameter(name = 'Rdzu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu1x2}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 1, 2 ])

Rdzu1x3 = Parameter(name = 'Rdzu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu1x3}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 1, 3 ])

Rdzu2x1 = Parameter(name = 'Rdzu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu2x1}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 2, 1 ])

Rdzu2x2 = Parameter(name = 'Rdzu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu2x2}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 2, 2 ])

Rdzu2x3 = Parameter(name = 'Rdzu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu2x3}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 2, 3 ])

Rdzu3x1 = Parameter(name = 'Rdzu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu3x1}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 3, 1 ])

Rdzu3x2 = Parameter(name = 'Rdzu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu3x2}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 3, 2 ])

Rdzu3x3 = Parameter(name = 'Rdzu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.1,
                    texname = '\\text{Rdzu3x3}',
                    lhablock = 'BCxdzu',
                    lhacode = [ 3, 3 ])

dCw = Parameter(name = 'dCw',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\text{$\\delta $c}_w',
                lhablock = 'BCxh',
                lhacode = [ 1 ])

dCz = Parameter(name = 'dCz',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\text{$\\delta $c}_z',
                lhablock = 'BCxh',
                lhacode = [ 2 ])

Cww = Parameter(name = 'Cww',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{ww}}',
                lhablock = 'BCxh',
                lhacode = [ 3 ])

Cgg = Parameter(name = 'Cgg',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{gg}}',
                lhablock = 'BCxh',
                lhacode = [ 4 ])

Caa = Parameter(name = 'Caa',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{aa}}',
                lhablock = 'BCxh',
                lhacode = [ 5 ])

Cza = Parameter(name = 'Cza',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{za}}',
                lhablock = 'BCxh',
                lhacode = [ 6 ])

Czz = Parameter(name = 'Czz',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{zz}}',
                lhablock = 'BCxh',
                lhacode = [ 7 ])

Cwbx = Parameter(name = 'Cwbx',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = 'c_{w,\\square \\text{}}',
                 lhablock = 'BCxh',
                 lhacode = [ 8 ])

Czbx = Parameter(name = 'Czbx',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = 'c_{z,\\square \\text{}}',
                 lhablock = 'BCxh',
                 lhacode = [ 9 ])

Cabx = Parameter(name = 'Cabx',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = 'c_{a,\\square \\text{}}',
                 lhablock = 'BCxh',
                 lhacode = [ 10 ])

tCww = Parameter(name = 'tCww',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{ww}}',
                 lhablock = 'BCxh',
                 lhacode = [ 11 ])

tCgg = Parameter(name = 'tCgg',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{gg}}',
                 lhablock = 'BCxh',
                 lhacode = [ 12 ])

tCaa = Parameter(name = 'tCaa',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{aa}}',
                 lhablock = 'BCxh',
                 lhacode = [ 13 ])

tCza = Parameter(name = 'tCza',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{za}}',
                 lhablock = 'BCxh',
                 lhacode = [ 14 ])

tCzz = Parameter(name = 'tCzz',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{zz}}',
                 lhablock = 'BCxh',
                 lhacode = [ 15 ])

dCw2 = Parameter(name = 'dCw2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[\\text{$\\delta $c},w,\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 1 ])

dCz2 = Parameter(name = 'dCz2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[\\text{$\\delta $c},z,\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 2 ])

Cww2 = Parameter(name = 'Cww2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[c,\\text{ww},\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 3 ])

Cgg2 = Parameter(name = 'Cgg2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[c,\\text{gg},\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 4 ])

Caa2 = Parameter(name = 'Caa2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[c,\\text{aa},\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 5 ])

Cza2 = Parameter(name = 'Cza2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[c,\\text{za},\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 6 ])

Czz2 = Parameter(name = 'Czz2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{Subsuperscript}[c,\\text{zz},\\text{(2)}]',
                 lhablock = 'BCxhh',
                 lhacode = [ 7 ])

Cwbx2 = Parameter(name = 'Cwbx2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}[c,w,\\square \\text{},\\text{(2)}]',
                  lhablock = 'BCxhh',
                  lhacode = [ 8 ])

Czbx2 = Parameter(name = 'Czbx2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}[c,z,\\square \\text{},\\text{(2)}]',
                  lhablock = 'BCxhh',
                  lhacode = [ 9 ])

Cabx2 = Parameter(name = 'Cabx2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}[c,a,\\square \\text{},\\text{(2)}]',
                  lhablock = 'BCxhh',
                  lhacode = [ 10 ])

tCww2 = Parameter(name = 'tCww2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}\\left[\\tilde{c},\\text{ww},\\text{(2)}\\right]',
                  lhablock = 'BCxhh',
                  lhacode = [ 11 ])

tCgg2 = Parameter(name = 'tCgg2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}\\left[\\tilde{c},\\text{gg},\\text{(2)}\\right]',
                  lhablock = 'BCxhh',
                  lhacode = [ 12 ])

tCaa2 = Parameter(name = 'tCaa2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}\\left[\\tilde{c},\\text{aa},\\text{(2)}\\right]',
                  lhablock = 'BCxhh',
                  lhacode = [ 13 ])

tCza2 = Parameter(name = 'tCza2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}\\left[\\tilde{c},\\text{za},\\text{(2)}\\right]',
                  lhablock = 'BCxhh',
                  lhacode = [ 14 ])

tCzz2 = Parameter(name = 'tCzz2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{Subsuperscript}\\left[\\tilde{c},\\text{zz},\\text{(2)}\\right]',
                  lhablock = 'BCxhh',
                  lhacode = [ 15 ])

dL3 = Parameter(name = 'dL3',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\delta \\lambda _3',
                lhablock = 'BCxhself',
                lhacode = [ 1 ])

dL4 = Parameter(name = 'dL4',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\delta \\lambda _4',
                lhablock = 'BCxhself',
                lhacode = [ 2 ])

dM = Parameter(name = 'dM',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{$\\delta $m}',
               lhablock = 'BCxmass',
               lhacode = [ 1 ])

dGw4 = Parameter(name = 'dGw4',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{$\\delta $g}_{\\text{w4}}',
                 lhablock = 'BCxqgc',
                 lhacode = [ 1 ])

dGw2z2 = Parameter(name = 'dGw2z2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{$\\delta $g}_{\\text{w2z2}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 2 ])

dGw2za = Parameter(name = 'dGw2za',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{$\\delta $g}_{\\text{w2za}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 3 ])

Lw4 = Parameter(name = 'Lw4',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\lambda _{\\text{w4}}',
                lhablock = 'BCxqgc',
                lhacode = [ 4 ])

Lw2z2 = Parameter(name = 'Lw2z2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\lambda _{\\text{w2z2}}',
                  lhablock = 'BCxqgc',
                  lhacode = [ 5 ])

Lw2a2 = Parameter(name = 'Lw2a2',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\lambda _{\\text{w2a2}}',
                  lhablock = 'BCxqgc',
                  lhacode = [ 6 ])

Lw2az = Parameter(name = 'Lw2az',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\lambda _{\\text{w2az}}',
                  lhablock = 'BCxqgc',
                  lhacode = [ 7 ])

Lw2za = Parameter(name = 'Lw2za',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\lambda _{\\text{w2za}}',
                  lhablock = 'BCxqgc',
                  lhacode = [ 8 ])

C4g = Parameter(name = 'C4g',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{4G}}',
                lhablock = 'BCxqgc',
                lhacode = [ 9 ])

tLw4 = Parameter(name = 'tLw4',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{\\lambda }_{\\text{w4}}',
                 lhablock = 'BCxqgc',
                 lhacode = [ 10 ])

tLw2z2 = Parameter(name = 'tLw2z2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\tilde{\\lambda }_{\\text{w2z2}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 11 ])

tLw2a2 = Parameter(name = 'tLw2a2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\tilde{\\lambda }_{\\text{w2a2}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 12 ])

tLw2az = Parameter(name = 'tLw2az',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\tilde{\\lambda }_{\\text{w2az}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 13 ])

tLw2za = Parameter(name = 'tLw2za',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\tilde{\\lambda }_{\\text{w2za}}',
                   lhablock = 'BCxqgc',
                   lhacode = [ 14 ])

tC4g = Parameter(name = 'tC4g',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{c}_{\\text{4G}}',
                 lhablock = 'BCxqgc',
                 lhacode = [ 15 ])

Sd1x1 = Parameter(name = 'Sd1x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd1x1}',
                  lhablock = 'BCxSd',
                  lhacode = [ 1, 1 ])

Sd1x2 = Parameter(name = 'Sd1x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd1x2}',
                  lhablock = 'BCxSd',
                  lhacode = [ 1, 2 ])

Sd1x3 = Parameter(name = 'Sd1x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd1x3}',
                  lhablock = 'BCxSd',
                  lhacode = [ 1, 3 ])

Sd2x1 = Parameter(name = 'Sd2x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd2x1}',
                  lhablock = 'BCxSd',
                  lhacode = [ 2, 1 ])

Sd2x2 = Parameter(name = 'Sd2x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd2x2}',
                  lhablock = 'BCxSd',
                  lhacode = [ 2, 2 ])

Sd2x3 = Parameter(name = 'Sd2x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd2x3}',
                  lhablock = 'BCxSd',
                  lhacode = [ 2, 3 ])

Sd3x1 = Parameter(name = 'Sd3x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd3x1}',
                  lhablock = 'BCxSd',
                  lhacode = [ 3, 1 ])

Sd3x2 = Parameter(name = 'Sd3x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd3x2}',
                  lhablock = 'BCxSd',
                  lhacode = [ 3, 2 ])

Sd3x3 = Parameter(name = 'Sd3x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Sd3x3}',
                  lhablock = 'BCxSd',
                  lhacode = [ 3, 3 ])

Se1x1 = Parameter(name = 'Se1x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se1x1}',
                  lhablock = 'BCxSe',
                  lhacode = [ 1, 1 ])

Se1x2 = Parameter(name = 'Se1x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se1x2}',
                  lhablock = 'BCxSe',
                  lhacode = [ 1, 2 ])

Se1x3 = Parameter(name = 'Se1x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se1x3}',
                  lhablock = 'BCxSe',
                  lhacode = [ 1, 3 ])

Se2x1 = Parameter(name = 'Se2x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se2x1}',
                  lhablock = 'BCxSe',
                  lhacode = [ 2, 1 ])

Se2x2 = Parameter(name = 'Se2x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se2x2}',
                  lhablock = 'BCxSe',
                  lhacode = [ 2, 2 ])

Se2x3 = Parameter(name = 'Se2x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se2x3}',
                  lhablock = 'BCxSe',
                  lhacode = [ 2, 3 ])

Se3x1 = Parameter(name = 'Se3x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se3x1}',
                  lhablock = 'BCxSe',
                  lhacode = [ 3, 1 ])

Se3x2 = Parameter(name = 'Se3x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se3x2}',
                  lhablock = 'BCxSe',
                  lhacode = [ 3, 2 ])

Se3x3 = Parameter(name = 'Se3x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Se3x3}',
                  lhablock = 'BCxSe',
                  lhacode = [ 3, 3 ])

Su1x1 = Parameter(name = 'Su1x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su1x1}',
                  lhablock = 'BCxSu',
                  lhacode = [ 1, 1 ])

Su1x2 = Parameter(name = 'Su1x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su1x2}',
                  lhablock = 'BCxSu',
                  lhacode = [ 1, 2 ])

Su1x3 = Parameter(name = 'Su1x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su1x3}',
                  lhablock = 'BCxSu',
                  lhacode = [ 1, 3 ])

Su2x1 = Parameter(name = 'Su2x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su2x1}',
                  lhablock = 'BCxSu',
                  lhacode = [ 2, 1 ])

Su2x2 = Parameter(name = 'Su2x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su2x2}',
                  lhablock = 'BCxSu',
                  lhacode = [ 2, 2 ])

Su2x3 = Parameter(name = 'Su2x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su2x3}',
                  lhablock = 'BCxSu',
                  lhacode = [ 2, 3 ])

Su3x1 = Parameter(name = 'Su3x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su3x1}',
                  lhablock = 'BCxSu',
                  lhacode = [ 3, 1 ])

Su3x2 = Parameter(name = 'Su3x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su3x2}',
                  lhablock = 'BCxSu',
                  lhacode = [ 3, 2 ])

Su3x3 = Parameter(name = 'Su3x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0.707107,
                  texname = '\\text{Su3x3}',
                  lhablock = 'BCxSu',
                  lhacode = [ 3, 3 ])

Rtdad1x1 = Parameter(name = 'Rtdad1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad1x1}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 1, 1 ])

Rtdad1x2 = Parameter(name = 'Rtdad1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad1x2}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 1, 2 ])

Rtdad1x3 = Parameter(name = 'Rtdad1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad1x3}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 1, 3 ])

Rtdad2x1 = Parameter(name = 'Rtdad2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad2x1}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 2, 1 ])

Rtdad2x2 = Parameter(name = 'Rtdad2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad2x2}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 2, 2 ])

Rtdad2x3 = Parameter(name = 'Rtdad2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad2x3}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 2, 3 ])

Rtdad3x1 = Parameter(name = 'Rtdad3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad3x1}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 3, 1 ])

Rtdad3x2 = Parameter(name = 'Rtdad3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad3x2}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 3, 2 ])

Rtdad3x3 = Parameter(name = 'Rtdad3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdad3x3}',
                     lhablock = 'BCxtdad',
                     lhacode = [ 3, 3 ])

Rtdae1x1 = Parameter(name = 'Rtdae1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae1x1}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 1, 1 ])

Rtdae1x2 = Parameter(name = 'Rtdae1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae1x2}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 1, 2 ])

Rtdae1x3 = Parameter(name = 'Rtdae1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae1x3}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 1, 3 ])

Rtdae2x1 = Parameter(name = 'Rtdae2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae2x1}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 2, 1 ])

Rtdae2x2 = Parameter(name = 'Rtdae2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae2x2}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 2, 2 ])

Rtdae2x3 = Parameter(name = 'Rtdae2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae2x3}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 2, 3 ])

Rtdae3x1 = Parameter(name = 'Rtdae3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae3x1}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 3, 1 ])

Rtdae3x2 = Parameter(name = 'Rtdae3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae3x2}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 3, 2 ])

Rtdae3x3 = Parameter(name = 'Rtdae3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdae3x3}',
                     lhablock = 'BCxtdae',
                     lhacode = [ 3, 3 ])

Rtdau1x1 = Parameter(name = 'Rtdau1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau1x1}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 1, 1 ])

Rtdau1x2 = Parameter(name = 'Rtdau1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau1x2}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 1, 2 ])

Rtdau1x3 = Parameter(name = 'Rtdau1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau1x3}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 1, 3 ])

Rtdau2x1 = Parameter(name = 'Rtdau2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau2x1}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 2, 1 ])

Rtdau2x2 = Parameter(name = 'Rtdau2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau2x2}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 2, 2 ])

Rtdau2x3 = Parameter(name = 'Rtdau2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau2x3}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 2, 3 ])

Rtdau3x1 = Parameter(name = 'Rtdau3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau3x1}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 3, 1 ])

Rtdau3x2 = Parameter(name = 'Rtdau3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau3x2}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 3, 2 ])

Rtdau3x3 = Parameter(name = 'Rtdau3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdau3x3}',
                     lhablock = 'BCxtdau',
                     lhacode = [ 3, 3 ])

Rtdgd1x1 = Parameter(name = 'Rtdgd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd1x1}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 1, 1 ])

Rtdgd1x2 = Parameter(name = 'Rtdgd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd1x2}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 1, 2 ])

Rtdgd1x3 = Parameter(name = 'Rtdgd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd1x3}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 1, 3 ])

Rtdgd2x1 = Parameter(name = 'Rtdgd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd2x1}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 2, 1 ])

Rtdgd2x2 = Parameter(name = 'Rtdgd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd2x2}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 2, 2 ])

Rtdgd2x3 = Parameter(name = 'Rtdgd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd2x3}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 2, 3 ])

Rtdgd3x1 = Parameter(name = 'Rtdgd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd3x1}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 3, 1 ])

Rtdgd3x2 = Parameter(name = 'Rtdgd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd3x2}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 3, 2 ])

Rtdgd3x3 = Parameter(name = 'Rtdgd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgd3x3}',
                     lhablock = 'BCxtdgd',
                     lhacode = [ 3, 3 ])

Rtdgu1x1 = Parameter(name = 'Rtdgu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu1x1}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 1, 1 ])

Rtdgu1x2 = Parameter(name = 'Rtdgu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu1x2}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 1, 2 ])

Rtdgu1x3 = Parameter(name = 'Rtdgu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu1x3}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 1, 3 ])

Rtdgu2x1 = Parameter(name = 'Rtdgu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu2x1}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 2, 1 ])

Rtdgu2x2 = Parameter(name = 'Rtdgu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu2x2}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 2, 2 ])

Rtdgu2x3 = Parameter(name = 'Rtdgu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu2x3}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 2, 3 ])

Rtdgu3x1 = Parameter(name = 'Rtdgu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu3x1}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 3, 1 ])

Rtdgu3x2 = Parameter(name = 'Rtdgu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu3x2}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 3, 2 ])

Rtdgu3x3 = Parameter(name = 'Rtdgu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdgu3x3}',
                     lhablock = 'BCxtdgu',
                     lhacode = [ 3, 3 ])

Rtdhad1x1 = Parameter(name = 'Rtdhad1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad1x1}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 1, 1 ])

Rtdhad1x2 = Parameter(name = 'Rtdhad1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad1x2}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 1, 2 ])

Rtdhad1x3 = Parameter(name = 'Rtdhad1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad1x3}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 1, 3 ])

Rtdhad2x1 = Parameter(name = 'Rtdhad2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad2x1}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 2, 1 ])

Rtdhad2x2 = Parameter(name = 'Rtdhad2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad2x2}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 2, 2 ])

Rtdhad2x3 = Parameter(name = 'Rtdhad2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad2x3}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 2, 3 ])

Rtdhad3x1 = Parameter(name = 'Rtdhad3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad3x1}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 3, 1 ])

Rtdhad3x2 = Parameter(name = 'Rtdhad3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad3x2}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 3, 2 ])

Rtdhad3x3 = Parameter(name = 'Rtdhad3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhad3x3}',
                      lhablock = 'BCxtdhad',
                      lhacode = [ 3, 3 ])

Rtdhae1x1 = Parameter(name = 'Rtdhae1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae1x1}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 1, 1 ])

Rtdhae1x2 = Parameter(name = 'Rtdhae1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae1x2}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 1, 2 ])

Rtdhae1x3 = Parameter(name = 'Rtdhae1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae1x3}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 1, 3 ])

Rtdhae2x1 = Parameter(name = 'Rtdhae2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae2x1}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 2, 1 ])

Rtdhae2x2 = Parameter(name = 'Rtdhae2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae2x2}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 2, 2 ])

Rtdhae2x3 = Parameter(name = 'Rtdhae2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae2x3}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 2, 3 ])

Rtdhae3x1 = Parameter(name = 'Rtdhae3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae3x1}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 3, 1 ])

Rtdhae3x2 = Parameter(name = 'Rtdhae3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae3x2}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 3, 2 ])

Rtdhae3x3 = Parameter(name = 'Rtdhae3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhae3x3}',
                      lhablock = 'BCxtdhae',
                      lhacode = [ 3, 3 ])

Rtdhau1x1 = Parameter(name = 'Rtdhau1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau1x1}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 1, 1 ])

Rtdhau1x2 = Parameter(name = 'Rtdhau1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau1x2}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 1, 2 ])

Rtdhau1x3 = Parameter(name = 'Rtdhau1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau1x3}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 1, 3 ])

Rtdhau2x1 = Parameter(name = 'Rtdhau2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau2x1}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 2, 1 ])

Rtdhau2x2 = Parameter(name = 'Rtdhau2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau2x2}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 2, 2 ])

Rtdhau2x3 = Parameter(name = 'Rtdhau2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau2x3}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 2, 3 ])

Rtdhau3x1 = Parameter(name = 'Rtdhau3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau3x1}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 3, 1 ])

Rtdhau3x2 = Parameter(name = 'Rtdhau3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau3x2}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 3, 2 ])

Rtdhau3x3 = Parameter(name = 'Rtdhau3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhau3x3}',
                      lhablock = 'BCxtdhau',
                      lhacode = [ 3, 3 ])

Rtdhgd1x1 = Parameter(name = 'Rtdhgd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd1x1}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 1, 1 ])

Rtdhgd1x2 = Parameter(name = 'Rtdhgd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd1x2}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 1, 2 ])

Rtdhgd1x3 = Parameter(name = 'Rtdhgd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd1x3}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 1, 3 ])

Rtdhgd2x1 = Parameter(name = 'Rtdhgd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd2x1}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 2, 1 ])

Rtdhgd2x2 = Parameter(name = 'Rtdhgd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd2x2}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 2, 2 ])

Rtdhgd2x3 = Parameter(name = 'Rtdhgd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd2x3}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 2, 3 ])

Rtdhgd3x1 = Parameter(name = 'Rtdhgd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd3x1}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 3, 1 ])

Rtdhgd3x2 = Parameter(name = 'Rtdhgd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd3x2}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 3, 2 ])

Rtdhgd3x3 = Parameter(name = 'Rtdhgd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgd3x3}',
                      lhablock = 'BCxtdhgd',
                      lhacode = [ 3, 3 ])

Rtdhgu1x1 = Parameter(name = 'Rtdhgu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu1x1}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 1, 1 ])

Rtdhgu1x2 = Parameter(name = 'Rtdhgu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu1x2}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 1, 2 ])

Rtdhgu1x3 = Parameter(name = 'Rtdhgu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu1x3}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 1, 3 ])

Rtdhgu2x1 = Parameter(name = 'Rtdhgu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu2x1}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 2, 1 ])

Rtdhgu2x2 = Parameter(name = 'Rtdhgu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu2x2}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 2, 2 ])

Rtdhgu2x3 = Parameter(name = 'Rtdhgu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu2x3}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 2, 3 ])

Rtdhgu3x1 = Parameter(name = 'Rtdhgu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu3x1}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 3, 1 ])

Rtdhgu3x2 = Parameter(name = 'Rtdhgu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu3x2}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 3, 2 ])

Rtdhgu3x3 = Parameter(name = 'Rtdhgu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhgu3x3}',
                      lhablock = 'BCxtdhgu',
                      lhacode = [ 3, 3 ])

Rtdhzd1x1 = Parameter(name = 'Rtdhzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd1x1}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 1, 1 ])

Rtdhzd1x2 = Parameter(name = 'Rtdhzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd1x2}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 1, 2 ])

Rtdhzd1x3 = Parameter(name = 'Rtdhzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd1x3}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 1, 3 ])

Rtdhzd2x1 = Parameter(name = 'Rtdhzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd2x1}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 2, 1 ])

Rtdhzd2x2 = Parameter(name = 'Rtdhzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd2x2}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 2, 2 ])

Rtdhzd2x3 = Parameter(name = 'Rtdhzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd2x3}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 2, 3 ])

Rtdhzd3x1 = Parameter(name = 'Rtdhzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd3x1}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 3, 1 ])

Rtdhzd3x2 = Parameter(name = 'Rtdhzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd3x2}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 3, 2 ])

Rtdhzd3x3 = Parameter(name = 'Rtdhzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzd3x3}',
                      lhablock = 'BCxtdhzd',
                      lhacode = [ 3, 3 ])

Rtdhze1x1 = Parameter(name = 'Rtdhze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze1x1}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 1, 1 ])

Rtdhze1x2 = Parameter(name = 'Rtdhze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze1x2}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 1, 2 ])

Rtdhze1x3 = Parameter(name = 'Rtdhze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze1x3}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 1, 3 ])

Rtdhze2x1 = Parameter(name = 'Rtdhze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze2x1}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 2, 1 ])

Rtdhze2x2 = Parameter(name = 'Rtdhze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze2x2}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 2, 2 ])

Rtdhze2x3 = Parameter(name = 'Rtdhze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze2x3}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 2, 3 ])

Rtdhze3x1 = Parameter(name = 'Rtdhze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze3x1}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 3, 1 ])

Rtdhze3x2 = Parameter(name = 'Rtdhze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze3x2}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 3, 2 ])

Rtdhze3x3 = Parameter(name = 'Rtdhze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhze3x3}',
                      lhablock = 'BCxtdhze',
                      lhacode = [ 3, 3 ])

Rtdhzu1x1 = Parameter(name = 'Rtdhzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu1x1}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 1, 1 ])

Rtdhzu1x2 = Parameter(name = 'Rtdhzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu1x2}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 1, 2 ])

Rtdhzu1x3 = Parameter(name = 'Rtdhzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu1x3}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 1, 3 ])

Rtdhzu2x1 = Parameter(name = 'Rtdhzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu2x1}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 2, 1 ])

Rtdhzu2x2 = Parameter(name = 'Rtdhzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu2x2}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 2, 2 ])

Rtdhzu2x3 = Parameter(name = 'Rtdhzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu2x3}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 2, 3 ])

Rtdhzu3x1 = Parameter(name = 'Rtdhzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu3x1}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 3, 1 ])

Rtdhzu3x2 = Parameter(name = 'Rtdhzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu3x2}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 3, 2 ])

Rtdhzu3x3 = Parameter(name = 'Rtdhzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.1,
                      texname = '\\text{Rtdhzu3x3}',
                      lhablock = 'BCxtdhzu',
                      lhacode = [ 3, 3 ])

Rtdzd1x1 = Parameter(name = 'Rtdzd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd1x1}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 1, 1 ])

Rtdzd1x2 = Parameter(name = 'Rtdzd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd1x2}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 1, 2 ])

Rtdzd1x3 = Parameter(name = 'Rtdzd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd1x3}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 1, 3 ])

Rtdzd2x1 = Parameter(name = 'Rtdzd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd2x1}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 2, 1 ])

Rtdzd2x2 = Parameter(name = 'Rtdzd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd2x2}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 2, 2 ])

Rtdzd2x3 = Parameter(name = 'Rtdzd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd2x3}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 2, 3 ])

Rtdzd3x1 = Parameter(name = 'Rtdzd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd3x1}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 3, 1 ])

Rtdzd3x2 = Parameter(name = 'Rtdzd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd3x2}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 3, 2 ])

Rtdzd3x3 = Parameter(name = 'Rtdzd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzd3x3}',
                     lhablock = 'BCxtdzd',
                     lhacode = [ 3, 3 ])

Rtdze1x1 = Parameter(name = 'Rtdze1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze1x1}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 1, 1 ])

Rtdze1x2 = Parameter(name = 'Rtdze1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze1x2}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 1, 2 ])

Rtdze1x3 = Parameter(name = 'Rtdze1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze1x3}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 1, 3 ])

Rtdze2x1 = Parameter(name = 'Rtdze2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze2x1}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 2, 1 ])

Rtdze2x2 = Parameter(name = 'Rtdze2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze2x2}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 2, 2 ])

Rtdze2x3 = Parameter(name = 'Rtdze2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze2x3}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 2, 3 ])

Rtdze3x1 = Parameter(name = 'Rtdze3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze3x1}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 3, 1 ])

Rtdze3x2 = Parameter(name = 'Rtdze3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze3x2}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 3, 2 ])

Rtdze3x3 = Parameter(name = 'Rtdze3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdze3x3}',
                     lhablock = 'BCxtdze',
                     lhacode = [ 3, 3 ])

Rtdzu1x1 = Parameter(name = 'Rtdzu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu1x1}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 1, 1 ])

Rtdzu1x2 = Parameter(name = 'Rtdzu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu1x2}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 1, 2 ])

Rtdzu1x3 = Parameter(name = 'Rtdzu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu1x3}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 1, 3 ])

Rtdzu2x1 = Parameter(name = 'Rtdzu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu2x1}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 2, 1 ])

Rtdzu2x2 = Parameter(name = 'Rtdzu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu2x2}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 2, 2 ])

Rtdzu2x3 = Parameter(name = 'Rtdzu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu2x3}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 2, 3 ])

Rtdzu3x1 = Parameter(name = 'Rtdzu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu3x1}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 3, 1 ])

Rtdzu3x2 = Parameter(name = 'Rtdzu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu3x2}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 3, 2 ])

Rtdzu3x3 = Parameter(name = 'Rtdzu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{Rtdzu3x3}',
                     lhablock = 'BCxtdzu',
                     lhacode = [ 3, 3 ])

dKa = Parameter(name = 'dKa',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\delta \\kappa _a',
                lhablock = 'BCxtgc',
                lhacode = [ 1 ])

dKz = Parameter(name = 'dKz',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\delta \\kappa _z',
                lhablock = 'BCxtgc',
                lhacode = [ 2 ])

dG1z = Parameter(name = 'dG1z',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\text{$\\delta $g}_{\\text{1z}}',
                 lhablock = 'BCxtgc',
                 lhacode = [ 3 ])

La = Parameter(name = 'La',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\lambda _a',
               lhablock = 'BCxtgc',
               lhacode = [ 4 ])

Lz = Parameter(name = 'Lz',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\lambda _z',
               lhablock = 'BCxtgc',
               lhacode = [ 5 ])

C3g = Parameter(name = 'C3g',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'c_{\\text{3G}}',
                lhablock = 'BCxtgc',
                lhacode = [ 6 ])

tKa = Parameter(name = 'tKa',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\tilde{\\kappa }_a',
                lhablock = 'BCxtgc',
                lhacode = [ 7 ])

tKz = Parameter(name = 'tKz',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\tilde{\\kappa }_z',
                lhablock = 'BCxtgc',
                lhacode = [ 8 ])

tLa = Parameter(name = 'tLa',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\tilde{\\lambda }_a',
                lhablock = 'BCxtgc',
                lhacode = [ 9 ])

tLz = Parameter(name = 'tLz',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\tilde{\\lambda }_z',
                lhablock = 'BCxtgc',
                lhacode = [ 10 ])

tC3g = Parameter(name = 'tC3g',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\tilde{\\text{c}}_{\\text{3g}}',
                 lhablock = 'BCxtgc',
                 lhacode = [ 11 ])

RdYd21x1 = Parameter(name = 'RdYd21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd21x1}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 1, 1 ])

RdYd21x2 = Parameter(name = 'RdYd21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd21x2}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 1, 2 ])

RdYd21x3 = Parameter(name = 'RdYd21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd21x3}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 1, 3 ])

RdYd22x1 = Parameter(name = 'RdYd22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd22x1}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 2, 1 ])

RdYd22x2 = Parameter(name = 'RdYd22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd22x2}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 2, 2 ])

RdYd22x3 = Parameter(name = 'RdYd22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd22x3}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 2, 3 ])

RdYd23x1 = Parameter(name = 'RdYd23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd23x1}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 3, 1 ])

RdYd23x2 = Parameter(name = 'RdYd23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd23x2}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 3, 2 ])

RdYd23x3 = Parameter(name = 'RdYd23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYd23x3}',
                     lhablock = 'BCxY2d',
                     lhacode = [ 3, 3 ])

RdYe21x1 = Parameter(name = 'RdYe21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe21x1}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 1, 1 ])

RdYe21x2 = Parameter(name = 'RdYe21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe21x2}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 1, 2 ])

RdYe21x3 = Parameter(name = 'RdYe21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe21x3}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 1, 3 ])

RdYe22x1 = Parameter(name = 'RdYe22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe22x1}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 2, 1 ])

RdYe22x2 = Parameter(name = 'RdYe22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe22x2}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 2, 2 ])

RdYe22x3 = Parameter(name = 'RdYe22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe22x3}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 2, 3 ])

RdYe23x1 = Parameter(name = 'RdYe23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe23x1}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 3, 1 ])

RdYe23x2 = Parameter(name = 'RdYe23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe23x2}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 3, 2 ])

RdYe23x3 = Parameter(name = 'RdYe23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYe23x3}',
                     lhablock = 'BCxY2e',
                     lhacode = [ 3, 3 ])

RdYu21x1 = Parameter(name = 'RdYu21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu21x1}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 1, 1 ])

RdYu21x2 = Parameter(name = 'RdYu21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu21x2}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 1, 2 ])

RdYu21x3 = Parameter(name = 'RdYu21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu21x3}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 1, 3 ])

RdYu22x1 = Parameter(name = 'RdYu22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu22x1}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 2, 1 ])

RdYu22x2 = Parameter(name = 'RdYu22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu22x2}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 2, 2 ])

RdYu22x3 = Parameter(name = 'RdYu22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu22x3}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 2, 3 ])

RdYu23x1 = Parameter(name = 'RdYu23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu23x1}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 3, 1 ])

RdYu23x2 = Parameter(name = 'RdYu23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu23x2}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 3, 2 ])

RdYu23x3 = Parameter(name = 'RdYu23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.1,
                     texname = '\\text{RdYu23x3}',
                     lhablock = 'BCxY2u',
                     lhacode = [ 3, 3 ])

Idad1x1 = Parameter(name = 'Idad1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad1x1}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 1, 1 ])

Idad1x2 = Parameter(name = 'Idad1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad1x2}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 1, 2 ])

Idad1x3 = Parameter(name = 'Idad1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad1x3}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 1, 3 ])

Idad2x1 = Parameter(name = 'Idad2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad2x1}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 2, 1 ])

Idad2x2 = Parameter(name = 'Idad2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad2x2}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 2, 2 ])

Idad2x3 = Parameter(name = 'Idad2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad2x3}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 2, 3 ])

Idad3x1 = Parameter(name = 'Idad3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad3x1}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 3, 1 ])

Idad3x2 = Parameter(name = 'Idad3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad3x2}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 3, 2 ])

Idad3x3 = Parameter(name = 'Idad3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idad3x3}',
                    lhablock = 'IMBCxdad',
                    lhacode = [ 3, 3 ])

Idae1x1 = Parameter(name = 'Idae1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae1x1}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 1, 1 ])

Idae1x2 = Parameter(name = 'Idae1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae1x2}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 1, 2 ])

Idae1x3 = Parameter(name = 'Idae1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae1x3}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 1, 3 ])

Idae2x1 = Parameter(name = 'Idae2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae2x1}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 2, 1 ])

Idae2x2 = Parameter(name = 'Idae2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae2x2}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 2, 2 ])

Idae2x3 = Parameter(name = 'Idae2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae2x3}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 2, 3 ])

Idae3x1 = Parameter(name = 'Idae3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae3x1}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 3, 1 ])

Idae3x2 = Parameter(name = 'Idae3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae3x2}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 3, 2 ])

Idae3x3 = Parameter(name = 'Idae3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idae3x3}',
                    lhablock = 'IMBCxdae',
                    lhacode = [ 3, 3 ])

Idau1x1 = Parameter(name = 'Idau1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau1x1}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 1, 1 ])

Idau1x2 = Parameter(name = 'Idau1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau1x2}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 1, 2 ])

Idau1x3 = Parameter(name = 'Idau1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau1x3}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 1, 3 ])

Idau2x1 = Parameter(name = 'Idau2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau2x1}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 2, 1 ])

Idau2x2 = Parameter(name = 'Idau2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau2x2}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 2, 2 ])

Idau2x3 = Parameter(name = 'Idau2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau2x3}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 2, 3 ])

Idau3x1 = Parameter(name = 'Idau3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau3x1}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 3, 1 ])

Idau3x2 = Parameter(name = 'Idau3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau3x2}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 3, 2 ])

Idau3x3 = Parameter(name = 'Idau3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idau3x3}',
                    lhablock = 'IMBCxdau',
                    lhacode = [ 3, 3 ])

Idgd1x1 = Parameter(name = 'Idgd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd1x1}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 1, 1 ])

Idgd1x2 = Parameter(name = 'Idgd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd1x2}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 1, 2 ])

Idgd1x3 = Parameter(name = 'Idgd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd1x3}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 1, 3 ])

Idgd2x1 = Parameter(name = 'Idgd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd2x1}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 2, 1 ])

Idgd2x2 = Parameter(name = 'Idgd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd2x2}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 2, 2 ])

Idgd2x3 = Parameter(name = 'Idgd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd2x3}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 2, 3 ])

Idgd3x1 = Parameter(name = 'Idgd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd3x1}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 3, 1 ])

Idgd3x2 = Parameter(name = 'Idgd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd3x2}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 3, 2 ])

Idgd3x3 = Parameter(name = 'Idgd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgd3x3}',
                    lhablock = 'IMBCxdgd',
                    lhacode = [ 3, 3 ])

IdGLhwl1x1 = Parameter(name = 'IdGLhwl1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl1x1}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 1, 1 ])

IdGLhwl1x2 = Parameter(name = 'IdGLhwl1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl1x2}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 1, 2 ])

IdGLhwl1x3 = Parameter(name = 'IdGLhwl1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl1x3}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 1, 3 ])

IdGLhwl2x1 = Parameter(name = 'IdGLhwl2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl2x1}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 2, 1 ])

IdGLhwl2x2 = Parameter(name = 'IdGLhwl2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl2x2}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 2, 2 ])

IdGLhwl2x3 = Parameter(name = 'IdGLhwl2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl2x3}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 2, 3 ])

IdGLhwl3x1 = Parameter(name = 'IdGLhwl3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl3x1}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 3, 1 ])

IdGLhwl3x2 = Parameter(name = 'IdGLhwl3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl3x2}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 3, 2 ])

IdGLhwl3x3 = Parameter(name = 'IdGLhwl3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwl3x3}',
                       lhablock = 'IMBCxdGLhWl',
                       lhacode = [ 3, 3 ])

IdGLhwq1x1 = Parameter(name = 'IdGLhwq1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq1x1}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 1, 1 ])

IdGLhwq1x2 = Parameter(name = 'IdGLhwq1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq1x2}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 1, 2 ])

IdGLhwq1x3 = Parameter(name = 'IdGLhwq1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq1x3}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 1, 3 ])

IdGLhwq2x1 = Parameter(name = 'IdGLhwq2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq2x1}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 2, 1 ])

IdGLhwq2x2 = Parameter(name = 'IdGLhwq2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq2x2}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 2, 2 ])

IdGLhwq2x3 = Parameter(name = 'IdGLhwq2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq2x3}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 2, 3 ])

IdGLhwq3x1 = Parameter(name = 'IdGLhwq3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq3x1}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 3, 1 ])

IdGLhwq3x2 = Parameter(name = 'IdGLhwq3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq3x2}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 3, 2 ])

IdGLhwq3x3 = Parameter(name = 'IdGLhwq3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhwq3x3}',
                       lhablock = 'IMBCxdGLhWq',
                       lhacode = [ 3, 3 ])

IdGLhzd1x1 = Parameter(name = 'IdGLhzd1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd1x1}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 1, 1 ])

IdGLhzd1x2 = Parameter(name = 'IdGLhzd1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd1x2}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 1, 2 ])

IdGLhzd1x3 = Parameter(name = 'IdGLhzd1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd1x3}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 1, 3 ])

IdGLhzd2x1 = Parameter(name = 'IdGLhzd2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd2x1}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 2, 1 ])

IdGLhzd2x2 = Parameter(name = 'IdGLhzd2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd2x2}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 2, 2 ])

IdGLhzd2x3 = Parameter(name = 'IdGLhzd2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd2x3}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 2, 3 ])

IdGLhzd3x1 = Parameter(name = 'IdGLhzd3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd3x1}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 3, 1 ])

IdGLhzd3x2 = Parameter(name = 'IdGLhzd3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd3x2}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 3, 2 ])

IdGLhzd3x3 = Parameter(name = 'IdGLhzd3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzd3x3}',
                       lhablock = 'IMBCxdGLhzd',
                       lhacode = [ 3, 3 ])

IdGLhze1x1 = Parameter(name = 'IdGLhze1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze1x1}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 1, 1 ])

IdGLhze1x2 = Parameter(name = 'IdGLhze1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze1x2}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 1, 2 ])

IdGLhze1x3 = Parameter(name = 'IdGLhze1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze1x3}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 1, 3 ])

IdGLhze2x1 = Parameter(name = 'IdGLhze2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze2x1}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 2, 1 ])

IdGLhze2x2 = Parameter(name = 'IdGLhze2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze2x2}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 2, 2 ])

IdGLhze2x3 = Parameter(name = 'IdGLhze2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze2x3}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 2, 3 ])

IdGLhze3x1 = Parameter(name = 'IdGLhze3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze3x1}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 3, 1 ])

IdGLhze3x2 = Parameter(name = 'IdGLhze3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze3x2}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 3, 2 ])

IdGLhze3x3 = Parameter(name = 'IdGLhze3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhze3x3}',
                       lhablock = 'IMBCxdGLhze',
                       lhacode = [ 3, 3 ])

IdGLhzu1x1 = Parameter(name = 'IdGLhzu1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu1x1}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 1, 1 ])

IdGLhzu1x2 = Parameter(name = 'IdGLhzu1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu1x2}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 1, 2 ])

IdGLhzu1x3 = Parameter(name = 'IdGLhzu1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu1x3}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 1, 3 ])

IdGLhzu2x1 = Parameter(name = 'IdGLhzu2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu2x1}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 2, 1 ])

IdGLhzu2x2 = Parameter(name = 'IdGLhzu2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu2x2}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 2, 2 ])

IdGLhzu2x3 = Parameter(name = 'IdGLhzu2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu2x3}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 2, 3 ])

IdGLhzu3x1 = Parameter(name = 'IdGLhzu3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu3x1}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 3, 1 ])

IdGLhzu3x2 = Parameter(name = 'IdGLhzu3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu3x2}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 3, 2 ])

IdGLhzu3x3 = Parameter(name = 'IdGLhzu3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzu3x3}',
                       lhablock = 'IMBCxdGLhzu',
                       lhacode = [ 3, 3 ])

IdGLhzv1x1 = Parameter(name = 'IdGLhzv1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv1x1}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 1, 1 ])

IdGLhzv1x2 = Parameter(name = 'IdGLhzv1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv1x2}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 1, 2 ])

IdGLhzv1x3 = Parameter(name = 'IdGLhzv1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv1x3}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 1, 3 ])

IdGLhzv2x1 = Parameter(name = 'IdGLhzv2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv2x1}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 2, 1 ])

IdGLhzv2x2 = Parameter(name = 'IdGLhzv2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv2x2}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 2, 2 ])

IdGLhzv2x3 = Parameter(name = 'IdGLhzv2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv2x3}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 2, 3 ])

IdGLhzv3x1 = Parameter(name = 'IdGLhzv3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv3x1}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 3, 1 ])

IdGLhzv3x2 = Parameter(name = 'IdGLhzv3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv3x2}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 3, 2 ])

IdGLhzv3x3 = Parameter(name = 'IdGLhzv3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGLhzv3x3}',
                       lhablock = 'IMBCxdGLhzv',
                       lhacode = [ 3, 3 ])

IdGLwl1x1 = Parameter(name = 'IdGLwl1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl1x1}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 1, 1 ])

IdGLwl1x2 = Parameter(name = 'IdGLwl1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl1x2}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 1, 2 ])

IdGLwl1x3 = Parameter(name = 'IdGLwl1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl1x3}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 1, 3 ])

IdGLwl2x1 = Parameter(name = 'IdGLwl2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl2x1}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 2, 1 ])

IdGLwl2x2 = Parameter(name = 'IdGLwl2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl2x2}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 2, 2 ])

IdGLwl2x3 = Parameter(name = 'IdGLwl2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl2x3}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 2, 3 ])

IdGLwl3x1 = Parameter(name = 'IdGLwl3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl3x1}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 3, 1 ])

IdGLwl3x2 = Parameter(name = 'IdGLwl3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl3x2}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 3, 2 ])

IdGLwl3x3 = Parameter(name = 'IdGLwl3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwl3x3}',
                      lhablock = 'IMBCxdGLWl',
                      lhacode = [ 3, 3 ])

IdGLwq1x1 = Parameter(name = 'IdGLwq1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq1x1}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 1, 1 ])

IdGLwq1x2 = Parameter(name = 'IdGLwq1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq1x2}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 1, 2 ])

IdGLwq1x3 = Parameter(name = 'IdGLwq1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq1x3}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 1, 3 ])

IdGLwq2x1 = Parameter(name = 'IdGLwq2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq2x1}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 2, 1 ])

IdGLwq2x2 = Parameter(name = 'IdGLwq2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq2x2}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 2, 2 ])

IdGLwq2x3 = Parameter(name = 'IdGLwq2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq2x3}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 2, 3 ])

IdGLwq3x1 = Parameter(name = 'IdGLwq3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq3x1}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 3, 1 ])

IdGLwq3x2 = Parameter(name = 'IdGLwq3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq3x2}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 3, 2 ])

IdGLwq3x3 = Parameter(name = 'IdGLwq3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLwq3x3}',
                      lhablock = 'IMBCxdGLWq',
                      lhacode = [ 3, 3 ])

IdGLzd1x1 = Parameter(name = 'IdGLzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd1x1}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 1, 1 ])

IdGLzd1x2 = Parameter(name = 'IdGLzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd1x2}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 1, 2 ])

IdGLzd1x3 = Parameter(name = 'IdGLzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd1x3}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 1, 3 ])

IdGLzd2x1 = Parameter(name = 'IdGLzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd2x1}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 2, 1 ])

IdGLzd2x2 = Parameter(name = 'IdGLzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd2x2}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 2, 2 ])

IdGLzd2x3 = Parameter(name = 'IdGLzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd2x3}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 2, 3 ])

IdGLzd3x1 = Parameter(name = 'IdGLzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd3x1}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 3, 1 ])

IdGLzd3x2 = Parameter(name = 'IdGLzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd3x2}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 3, 2 ])

IdGLzd3x3 = Parameter(name = 'IdGLzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzd3x3}',
                      lhablock = 'IMBCxdGLzd',
                      lhacode = [ 3, 3 ])

IdGLze1x1 = Parameter(name = 'IdGLze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze1x1}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 1, 1 ])

IdGLze1x2 = Parameter(name = 'IdGLze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze1x2}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 1, 2 ])

IdGLze1x3 = Parameter(name = 'IdGLze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze1x3}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 1, 3 ])

IdGLze2x1 = Parameter(name = 'IdGLze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze2x1}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 2, 1 ])

IdGLze2x2 = Parameter(name = 'IdGLze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze2x2}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 2, 2 ])

IdGLze2x3 = Parameter(name = 'IdGLze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze2x3}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 2, 3 ])

IdGLze3x1 = Parameter(name = 'IdGLze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze3x1}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 3, 1 ])

IdGLze3x2 = Parameter(name = 'IdGLze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze3x2}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 3, 2 ])

IdGLze3x3 = Parameter(name = 'IdGLze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLze3x3}',
                      lhablock = 'IMBCxdGLze',
                      lhacode = [ 3, 3 ])

IdGLzu1x1 = Parameter(name = 'IdGLzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu1x1}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 1, 1 ])

IdGLzu1x2 = Parameter(name = 'IdGLzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu1x2}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 1, 2 ])

IdGLzu1x3 = Parameter(name = 'IdGLzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu1x3}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 1, 3 ])

IdGLzu2x1 = Parameter(name = 'IdGLzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu2x1}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 2, 1 ])

IdGLzu2x2 = Parameter(name = 'IdGLzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu2x2}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 2, 2 ])

IdGLzu2x3 = Parameter(name = 'IdGLzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu2x3}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 2, 3 ])

IdGLzu3x1 = Parameter(name = 'IdGLzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu3x1}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 3, 1 ])

IdGLzu3x2 = Parameter(name = 'IdGLzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu3x2}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 3, 2 ])

IdGLzu3x3 = Parameter(name = 'IdGLzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzu3x3}',
                      lhablock = 'IMBCxdGLzu',
                      lhacode = [ 3, 3 ])

IdGLzv1x1 = Parameter(name = 'IdGLzv1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv1x1}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 1, 1 ])

IdGLzv1x2 = Parameter(name = 'IdGLzv1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv1x2}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 1, 2 ])

IdGLzv1x3 = Parameter(name = 'IdGLzv1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv1x3}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 1, 3 ])

IdGLzv2x1 = Parameter(name = 'IdGLzv2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv2x1}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 2, 1 ])

IdGLzv2x2 = Parameter(name = 'IdGLzv2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv2x2}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 2, 2 ])

IdGLzv2x3 = Parameter(name = 'IdGLzv2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv2x3}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 2, 3 ])

IdGLzv3x1 = Parameter(name = 'IdGLzv3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv3x1}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 3, 1 ])

IdGLzv3x2 = Parameter(name = 'IdGLzv3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv3x2}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 3, 2 ])

IdGLzv3x3 = Parameter(name = 'IdGLzv3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGLzv3x3}',
                      lhablock = 'IMBCxdGLzv',
                      lhacode = [ 3, 3 ])

IdGRhwq1x1 = Parameter(name = 'IdGRhwq1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq1x1}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 1, 1 ])

IdGRhwq1x2 = Parameter(name = 'IdGRhwq1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq1x2}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 1, 2 ])

IdGRhwq1x3 = Parameter(name = 'IdGRhwq1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq1x3}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 1, 3 ])

IdGRhwq2x1 = Parameter(name = 'IdGRhwq2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq2x1}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 2, 1 ])

IdGRhwq2x2 = Parameter(name = 'IdGRhwq2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq2x2}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 2, 2 ])

IdGRhwq2x3 = Parameter(name = 'IdGRhwq2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq2x3}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 2, 3 ])

IdGRhwq3x1 = Parameter(name = 'IdGRhwq3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq3x1}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 3, 1 ])

IdGRhwq3x2 = Parameter(name = 'IdGRhwq3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq3x2}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 3, 2 ])

IdGRhwq3x3 = Parameter(name = 'IdGRhwq3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhwq3x3}',
                       lhablock = 'IMBCxdGRhWq',
                       lhacode = [ 3, 3 ])

IdGRhzd1x1 = Parameter(name = 'IdGRhzd1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd1x1}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 1, 1 ])

IdGRhzd1x2 = Parameter(name = 'IdGRhzd1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd1x2}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 1, 2 ])

IdGRhzd1x3 = Parameter(name = 'IdGRhzd1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd1x3}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 1, 3 ])

IdGRhzd2x1 = Parameter(name = 'IdGRhzd2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd2x1}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 2, 1 ])

IdGRhzd2x2 = Parameter(name = 'IdGRhzd2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd2x2}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 2, 2 ])

IdGRhzd2x3 = Parameter(name = 'IdGRhzd2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd2x3}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 2, 3 ])

IdGRhzd3x1 = Parameter(name = 'IdGRhzd3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd3x1}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 3, 1 ])

IdGRhzd3x2 = Parameter(name = 'IdGRhzd3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd3x2}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 3, 2 ])

IdGRhzd3x3 = Parameter(name = 'IdGRhzd3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzd3x3}',
                       lhablock = 'IMBCxdGRhzd',
                       lhacode = [ 3, 3 ])

IdGRhze1x1 = Parameter(name = 'IdGRhze1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze1x1}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 1, 1 ])

IdGRhze1x2 = Parameter(name = 'IdGRhze1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze1x2}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 1, 2 ])

IdGRhze1x3 = Parameter(name = 'IdGRhze1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze1x3}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 1, 3 ])

IdGRhze2x1 = Parameter(name = 'IdGRhze2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze2x1}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 2, 1 ])

IdGRhze2x2 = Parameter(name = 'IdGRhze2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze2x2}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 2, 2 ])

IdGRhze2x3 = Parameter(name = 'IdGRhze2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze2x3}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 2, 3 ])

IdGRhze3x1 = Parameter(name = 'IdGRhze3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze3x1}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 3, 1 ])

IdGRhze3x2 = Parameter(name = 'IdGRhze3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze3x2}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 3, 2 ])

IdGRhze3x3 = Parameter(name = 'IdGRhze3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhze3x3}',
                       lhablock = 'IMBCxdGRhze',
                       lhacode = [ 3, 3 ])

IdGRhzu1x1 = Parameter(name = 'IdGRhzu1x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu1x1}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 1, 1 ])

IdGRhzu1x2 = Parameter(name = 'IdGRhzu1x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu1x2}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 1, 2 ])

IdGRhzu1x3 = Parameter(name = 'IdGRhzu1x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu1x3}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 1, 3 ])

IdGRhzu2x1 = Parameter(name = 'IdGRhzu2x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu2x1}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 2, 1 ])

IdGRhzu2x2 = Parameter(name = 'IdGRhzu2x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu2x2}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 2, 2 ])

IdGRhzu2x3 = Parameter(name = 'IdGRhzu2x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu2x3}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 2, 3 ])

IdGRhzu3x1 = Parameter(name = 'IdGRhzu3x1',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu3x1}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 3, 1 ])

IdGRhzu3x2 = Parameter(name = 'IdGRhzu3x2',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu3x2}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 3, 2 ])

IdGRhzu3x3 = Parameter(name = 'IdGRhzu3x3',
                       nature = 'external',
                       type = 'complex',
                       value = 0.,
                       texname = '\\text{IdGRhzu3x3}',
                       lhablock = 'IMBCxdGRhzu',
                       lhacode = [ 3, 3 ])

IdGRwq1x1 = Parameter(name = 'IdGRwq1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq1x1}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 1, 1 ])

IdGRwq1x2 = Parameter(name = 'IdGRwq1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq1x2}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 1, 2 ])

IdGRwq1x3 = Parameter(name = 'IdGRwq1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq1x3}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 1, 3 ])

IdGRwq2x1 = Parameter(name = 'IdGRwq2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq2x1}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 2, 1 ])

IdGRwq2x2 = Parameter(name = 'IdGRwq2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq2x2}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 2, 2 ])

IdGRwq2x3 = Parameter(name = 'IdGRwq2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq2x3}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 2, 3 ])

IdGRwq3x1 = Parameter(name = 'IdGRwq3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq3x1}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 3, 1 ])

IdGRwq3x2 = Parameter(name = 'IdGRwq3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq3x2}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 3, 2 ])

IdGRwq3x3 = Parameter(name = 'IdGRwq3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRwq3x3}',
                      lhablock = 'IMBCxdGRWq',
                      lhacode = [ 3, 3 ])

IdGRzd1x1 = Parameter(name = 'IdGRzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd1x1}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 1, 1 ])

IdGRzd1x2 = Parameter(name = 'IdGRzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd1x2}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 1, 2 ])

IdGRzd1x3 = Parameter(name = 'IdGRzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd1x3}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 1, 3 ])

IdGRzd2x1 = Parameter(name = 'IdGRzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd2x1}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 2, 1 ])

IdGRzd2x2 = Parameter(name = 'IdGRzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd2x2}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 2, 2 ])

IdGRzd2x3 = Parameter(name = 'IdGRzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd2x3}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 2, 3 ])

IdGRzd3x1 = Parameter(name = 'IdGRzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd3x1}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 3, 1 ])

IdGRzd3x2 = Parameter(name = 'IdGRzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd3x2}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 3, 2 ])

IdGRzd3x3 = Parameter(name = 'IdGRzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzd3x3}',
                      lhablock = 'IMBCxdGRzd',
                      lhacode = [ 3, 3 ])

IdGRze1x1 = Parameter(name = 'IdGRze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze1x1}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 1, 1 ])

IdGRze1x2 = Parameter(name = 'IdGRze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze1x2}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 1, 2 ])

IdGRze1x3 = Parameter(name = 'IdGRze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze1x3}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 1, 3 ])

IdGRze2x1 = Parameter(name = 'IdGRze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze2x1}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 2, 1 ])

IdGRze2x2 = Parameter(name = 'IdGRze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze2x2}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 2, 2 ])

IdGRze2x3 = Parameter(name = 'IdGRze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze2x3}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 2, 3 ])

IdGRze3x1 = Parameter(name = 'IdGRze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze3x1}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 3, 1 ])

IdGRze3x2 = Parameter(name = 'IdGRze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze3x2}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 3, 2 ])

IdGRze3x3 = Parameter(name = 'IdGRze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRze3x3}',
                      lhablock = 'IMBCxdGRze',
                      lhacode = [ 3, 3 ])

IdGRzu1x1 = Parameter(name = 'IdGRzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu1x1}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 1, 1 ])

IdGRzu1x2 = Parameter(name = 'IdGRzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu1x2}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 1, 2 ])

IdGRzu1x3 = Parameter(name = 'IdGRzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu1x3}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 1, 3 ])

IdGRzu2x1 = Parameter(name = 'IdGRzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu2x1}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 2, 1 ])

IdGRzu2x2 = Parameter(name = 'IdGRzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu2x2}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 2, 2 ])

IdGRzu2x3 = Parameter(name = 'IdGRzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu2x3}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 2, 3 ])

IdGRzu3x1 = Parameter(name = 'IdGRzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu3x1}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 3, 1 ])

IdGRzu3x2 = Parameter(name = 'IdGRzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu3x2}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 3, 2 ])

IdGRzu3x3 = Parameter(name = 'IdGRzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{IdGRzu3x3}',
                      lhablock = 'IMBCxdGRzu',
                      lhacode = [ 3, 3 ])

Idgu1x1 = Parameter(name = 'Idgu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu1x1}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 1, 1 ])

Idgu1x2 = Parameter(name = 'Idgu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu1x2}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 1, 2 ])

Idgu1x3 = Parameter(name = 'Idgu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu1x3}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 1, 3 ])

Idgu2x1 = Parameter(name = 'Idgu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu2x1}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 2, 1 ])

Idgu2x2 = Parameter(name = 'Idgu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu2x2}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 2, 2 ])

Idgu2x3 = Parameter(name = 'Idgu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu2x3}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 2, 3 ])

Idgu3x1 = Parameter(name = 'Idgu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu3x1}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 3, 1 ])

Idgu3x2 = Parameter(name = 'Idgu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu3x2}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 3, 2 ])

Idgu3x3 = Parameter(name = 'Idgu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idgu3x3}',
                    lhablock = 'IMBCxdgu',
                    lhacode = [ 3, 3 ])

Idhad1x1 = Parameter(name = 'Idhad1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad1x1}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 1, 1 ])

Idhad1x2 = Parameter(name = 'Idhad1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad1x2}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 1, 2 ])

Idhad1x3 = Parameter(name = 'Idhad1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad1x3}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 1, 3 ])

Idhad2x1 = Parameter(name = 'Idhad2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad2x1}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 2, 1 ])

Idhad2x2 = Parameter(name = 'Idhad2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad2x2}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 2, 2 ])

Idhad2x3 = Parameter(name = 'Idhad2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad2x3}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 2, 3 ])

Idhad3x1 = Parameter(name = 'Idhad3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad3x1}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 3, 1 ])

Idhad3x2 = Parameter(name = 'Idhad3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad3x2}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 3, 2 ])

Idhad3x3 = Parameter(name = 'Idhad3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhad3x3}',
                     lhablock = 'IMBCxdhad',
                     lhacode = [ 3, 3 ])

Idhae1x1 = Parameter(name = 'Idhae1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae1x1}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 1, 1 ])

Idhae1x2 = Parameter(name = 'Idhae1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae1x2}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 1, 2 ])

Idhae1x3 = Parameter(name = 'Idhae1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae1x3}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 1, 3 ])

Idhae2x1 = Parameter(name = 'Idhae2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae2x1}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 2, 1 ])

Idhae2x2 = Parameter(name = 'Idhae2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae2x2}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 2, 2 ])

Idhae2x3 = Parameter(name = 'Idhae2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae2x3}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 2, 3 ])

Idhae3x1 = Parameter(name = 'Idhae3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae3x1}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 3, 1 ])

Idhae3x2 = Parameter(name = 'Idhae3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae3x2}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 3, 2 ])

Idhae3x3 = Parameter(name = 'Idhae3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhae3x3}',
                     lhablock = 'IMBCxdhae',
                     lhacode = [ 3, 3 ])

Idhau1x1 = Parameter(name = 'Idhau1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau1x1}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 1, 1 ])

Idhau1x2 = Parameter(name = 'Idhau1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau1x2}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 1, 2 ])

Idhau1x3 = Parameter(name = 'Idhau1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau1x3}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 1, 3 ])

Idhau2x1 = Parameter(name = 'Idhau2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau2x1}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 2, 1 ])

Idhau2x2 = Parameter(name = 'Idhau2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau2x2}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 2, 2 ])

Idhau2x3 = Parameter(name = 'Idhau2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau2x3}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 2, 3 ])

Idhau3x1 = Parameter(name = 'Idhau3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau3x1}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 3, 1 ])

Idhau3x2 = Parameter(name = 'Idhau3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau3x2}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 3, 2 ])

Idhau3x3 = Parameter(name = 'Idhau3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhau3x3}',
                     lhablock = 'IMBCxdhau',
                     lhacode = [ 3, 3 ])

Idhgd1x1 = Parameter(name = 'Idhgd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd1x1}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 1, 1 ])

Idhgd1x2 = Parameter(name = 'Idhgd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd1x2}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 1, 2 ])

Idhgd1x3 = Parameter(name = 'Idhgd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd1x3}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 1, 3 ])

Idhgd2x1 = Parameter(name = 'Idhgd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd2x1}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 2, 1 ])

Idhgd2x2 = Parameter(name = 'Idhgd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd2x2}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 2, 2 ])

Idhgd2x3 = Parameter(name = 'Idhgd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd2x3}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 2, 3 ])

Idhgd3x1 = Parameter(name = 'Idhgd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd3x1}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 3, 1 ])

Idhgd3x2 = Parameter(name = 'Idhgd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd3x2}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 3, 2 ])

Idhgd3x3 = Parameter(name = 'Idhgd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgd3x3}',
                     lhablock = 'IMBCxdhgd',
                     lhacode = [ 3, 3 ])

Idhgu1x1 = Parameter(name = 'Idhgu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu1x1}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 1, 1 ])

Idhgu1x2 = Parameter(name = 'Idhgu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu1x2}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 1, 2 ])

Idhgu1x3 = Parameter(name = 'Idhgu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu1x3}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 1, 3 ])

Idhgu2x1 = Parameter(name = 'Idhgu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu2x1}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 2, 1 ])

Idhgu2x2 = Parameter(name = 'Idhgu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu2x2}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 2, 2 ])

Idhgu2x3 = Parameter(name = 'Idhgu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu2x3}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 2, 3 ])

Idhgu3x1 = Parameter(name = 'Idhgu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu3x1}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 3, 1 ])

Idhgu3x2 = Parameter(name = 'Idhgu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu3x2}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 3, 2 ])

Idhgu3x3 = Parameter(name = 'Idhgu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhgu3x3}',
                     lhablock = 'IMBCxdhgu',
                     lhacode = [ 3, 3 ])

Idhwd1x1 = Parameter(name = 'Idhwd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd1x1}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 1, 1 ])

Idhwd1x2 = Parameter(name = 'Idhwd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd1x2}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 1, 2 ])

Idhwd1x3 = Parameter(name = 'Idhwd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd1x3}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 1, 3 ])

Idhwd2x1 = Parameter(name = 'Idhwd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd2x1}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 2, 1 ])

Idhwd2x2 = Parameter(name = 'Idhwd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd2x2}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 2, 2 ])

Idhwd2x3 = Parameter(name = 'Idhwd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd2x3}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 2, 3 ])

Idhwd3x1 = Parameter(name = 'Idhwd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd3x1}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 3, 1 ])

Idhwd3x2 = Parameter(name = 'Idhwd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd3x2}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 3, 2 ])

Idhwd3x3 = Parameter(name = 'Idhwd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwd3x3}',
                     lhablock = 'IMBCxdhwd',
                     lhacode = [ 3, 3 ])

Idhwl1x1 = Parameter(name = 'Idhwl1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl1x1}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 1, 1 ])

Idhwl1x2 = Parameter(name = 'Idhwl1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl1x2}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 1, 2 ])

Idhwl1x3 = Parameter(name = 'Idhwl1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl1x3}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 1, 3 ])

Idhwl2x1 = Parameter(name = 'Idhwl2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl2x1}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 2, 1 ])

Idhwl2x2 = Parameter(name = 'Idhwl2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl2x2}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 2, 2 ])

Idhwl2x3 = Parameter(name = 'Idhwl2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl2x3}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 2, 3 ])

Idhwl3x1 = Parameter(name = 'Idhwl3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl3x1}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 3, 1 ])

Idhwl3x2 = Parameter(name = 'Idhwl3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl3x2}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 3, 2 ])

Idhwl3x3 = Parameter(name = 'Idhwl3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwl3x3}',
                     lhablock = 'IMBCxdhwl',
                     lhacode = [ 3, 3 ])

Idhwu1x1 = Parameter(name = 'Idhwu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu1x1}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 1, 1 ])

Idhwu1x2 = Parameter(name = 'Idhwu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu1x2}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 1, 2 ])

Idhwu1x3 = Parameter(name = 'Idhwu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu1x3}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 1, 3 ])

Idhwu2x1 = Parameter(name = 'Idhwu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu2x1}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 2, 1 ])

Idhwu2x2 = Parameter(name = 'Idhwu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu2x2}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 2, 2 ])

Idhwu2x3 = Parameter(name = 'Idhwu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu2x3}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 2, 3 ])

Idhwu3x1 = Parameter(name = 'Idhwu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu3x1}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 3, 1 ])

Idhwu3x2 = Parameter(name = 'Idhwu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu3x2}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 3, 2 ])

Idhwu3x3 = Parameter(name = 'Idhwu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhwu3x3}',
                     lhablock = 'IMBCxdhwu',
                     lhacode = [ 3, 3 ])

Idhzd1x1 = Parameter(name = 'Idhzd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd1x1}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 1, 1 ])

Idhzd1x2 = Parameter(name = 'Idhzd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd1x2}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 1, 2 ])

Idhzd1x3 = Parameter(name = 'Idhzd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd1x3}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 1, 3 ])

Idhzd2x1 = Parameter(name = 'Idhzd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd2x1}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 2, 1 ])

Idhzd2x2 = Parameter(name = 'Idhzd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd2x2}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 2, 2 ])

Idhzd2x3 = Parameter(name = 'Idhzd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd2x3}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 2, 3 ])

Idhzd3x1 = Parameter(name = 'Idhzd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd3x1}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 3, 1 ])

Idhzd3x2 = Parameter(name = 'Idhzd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd3x2}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 3, 2 ])

Idhzd3x3 = Parameter(name = 'Idhzd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzd3x3}',
                     lhablock = 'IMBCxdhzd',
                     lhacode = [ 3, 3 ])

Idhze1x1 = Parameter(name = 'Idhze1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze1x1}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 1, 1 ])

Idhze1x2 = Parameter(name = 'Idhze1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze1x2}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 1, 2 ])

Idhze1x3 = Parameter(name = 'Idhze1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze1x3}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 1, 3 ])

Idhze2x1 = Parameter(name = 'Idhze2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze2x1}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 2, 1 ])

Idhze2x2 = Parameter(name = 'Idhze2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze2x2}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 2, 2 ])

Idhze2x3 = Parameter(name = 'Idhze2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze2x3}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 2, 3 ])

Idhze3x1 = Parameter(name = 'Idhze3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze3x1}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 3, 1 ])

Idhze3x2 = Parameter(name = 'Idhze3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze3x2}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 3, 2 ])

Idhze3x3 = Parameter(name = 'Idhze3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhze3x3}',
                     lhablock = 'IMBCxdhze',
                     lhacode = [ 3, 3 ])

Idhzu1x1 = Parameter(name = 'Idhzu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu1x1}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 1, 1 ])

Idhzu1x2 = Parameter(name = 'Idhzu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu1x2}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 1, 2 ])

Idhzu1x3 = Parameter(name = 'Idhzu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu1x3}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 1, 3 ])

Idhzu2x1 = Parameter(name = 'Idhzu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu2x1}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 2, 1 ])

Idhzu2x2 = Parameter(name = 'Idhzu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu2x2}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 2, 2 ])

Idhzu2x3 = Parameter(name = 'Idhzu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu2x3}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 2, 3 ])

Idhzu3x1 = Parameter(name = 'Idhzu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu3x1}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 3, 1 ])

Idhzu3x2 = Parameter(name = 'Idhzu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu3x2}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 3, 2 ])

Idhzu3x3 = Parameter(name = 'Idhzu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Idhzu3x3}',
                     lhablock = 'IMBCxdhzu',
                     lhacode = [ 3, 3 ])

Idwd1x1 = Parameter(name = 'Idwd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd1x1}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 1, 1 ])

Idwd1x2 = Parameter(name = 'Idwd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd1x2}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 1, 2 ])

Idwd1x3 = Parameter(name = 'Idwd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd1x3}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 1, 3 ])

Idwd2x1 = Parameter(name = 'Idwd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd2x1}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 2, 1 ])

Idwd2x2 = Parameter(name = 'Idwd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd2x2}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 2, 2 ])

Idwd2x3 = Parameter(name = 'Idwd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd2x3}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 2, 3 ])

Idwd3x1 = Parameter(name = 'Idwd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd3x1}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 3, 1 ])

Idwd3x2 = Parameter(name = 'Idwd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd3x2}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 3, 2 ])

Idwd3x3 = Parameter(name = 'Idwd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwd3x3}',
                    lhablock = 'IMBCxdwd',
                    lhacode = [ 3, 3 ])

Idwl1x1 = Parameter(name = 'Idwl1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl1x1}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 1, 1 ])

Idwl1x2 = Parameter(name = 'Idwl1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl1x2}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 1, 2 ])

Idwl1x3 = Parameter(name = 'Idwl1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl1x3}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 1, 3 ])

Idwl2x1 = Parameter(name = 'Idwl2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl2x1}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 2, 1 ])

Idwl2x2 = Parameter(name = 'Idwl2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl2x2}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 2, 2 ])

Idwl2x3 = Parameter(name = 'Idwl2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl2x3}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 2, 3 ])

Idwl3x1 = Parameter(name = 'Idwl3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl3x1}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 3, 1 ])

Idwl3x2 = Parameter(name = 'Idwl3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl3x2}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 3, 2 ])

Idwl3x3 = Parameter(name = 'Idwl3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwl3x3}',
                    lhablock = 'IMBCxdwl',
                    lhacode = [ 3, 3 ])

Idwu1x1 = Parameter(name = 'Idwu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu1x1}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 1, 1 ])

Idwu1x2 = Parameter(name = 'Idwu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu1x2}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 1, 2 ])

Idwu1x3 = Parameter(name = 'Idwu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu1x3}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 1, 3 ])

Idwu2x1 = Parameter(name = 'Idwu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu2x1}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 2, 1 ])

Idwu2x2 = Parameter(name = 'Idwu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu2x2}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 2, 2 ])

Idwu2x3 = Parameter(name = 'Idwu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu2x3}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 2, 3 ])

Idwu3x1 = Parameter(name = 'Idwu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu3x1}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 3, 1 ])

Idwu3x2 = Parameter(name = 'Idwu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu3x2}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 3, 2 ])

Idwu3x3 = Parameter(name = 'Idwu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idwu3x3}',
                    lhablock = 'IMBCxdwu',
                    lhacode = [ 3, 3 ])

Idzd1x1 = Parameter(name = 'Idzd1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd1x1}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 1, 1 ])

Idzd1x2 = Parameter(name = 'Idzd1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd1x2}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 1, 2 ])

Idzd1x3 = Parameter(name = 'Idzd1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd1x3}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 1, 3 ])

Idzd2x1 = Parameter(name = 'Idzd2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd2x1}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 2, 1 ])

Idzd2x2 = Parameter(name = 'Idzd2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd2x2}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 2, 2 ])

Idzd2x3 = Parameter(name = 'Idzd2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd2x3}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 2, 3 ])

Idzd3x1 = Parameter(name = 'Idzd3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd3x1}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 3, 1 ])

Idzd3x2 = Parameter(name = 'Idzd3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd3x2}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 3, 2 ])

Idzd3x3 = Parameter(name = 'Idzd3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzd3x3}',
                    lhablock = 'IMBCxdzd',
                    lhacode = [ 3, 3 ])

Idze1x1 = Parameter(name = 'Idze1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze1x1}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 1, 1 ])

Idze1x2 = Parameter(name = 'Idze1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze1x2}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 1, 2 ])

Idze1x3 = Parameter(name = 'Idze1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze1x3}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 1, 3 ])

Idze2x1 = Parameter(name = 'Idze2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze2x1}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 2, 1 ])

Idze2x2 = Parameter(name = 'Idze2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze2x2}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 2, 2 ])

Idze2x3 = Parameter(name = 'Idze2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze2x3}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 2, 3 ])

Idze3x1 = Parameter(name = 'Idze3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze3x1}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 3, 1 ])

Idze3x2 = Parameter(name = 'Idze3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze3x2}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 3, 2 ])

Idze3x3 = Parameter(name = 'Idze3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idze3x3}',
                    lhablock = 'IMBCxdze',
                    lhacode = [ 3, 3 ])

Idzu1x1 = Parameter(name = 'Idzu1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu1x1}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 1, 1 ])

Idzu1x2 = Parameter(name = 'Idzu1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu1x2}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 1, 2 ])

Idzu1x3 = Parameter(name = 'Idzu1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu1x3}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 1, 3 ])

Idzu2x1 = Parameter(name = 'Idzu2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu2x1}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 2, 1 ])

Idzu2x2 = Parameter(name = 'Idzu2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu2x2}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 2, 2 ])

Idzu2x3 = Parameter(name = 'Idzu2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu2x3}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 2, 3 ])

Idzu3x1 = Parameter(name = 'Idzu3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu3x1}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 3, 1 ])

Idzu3x2 = Parameter(name = 'Idzu3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu3x2}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 3, 2 ])

Idzu3x3 = Parameter(name = 'Idzu3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0.,
                    texname = '\\text{Idzu3x3}',
                    lhablock = 'IMBCxdzu',
                    lhacode = [ 3, 3 ])

Itdad1x1 = Parameter(name = 'Itdad1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad1x1}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 1, 1 ])

Itdad1x2 = Parameter(name = 'Itdad1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad1x2}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 1, 2 ])

Itdad1x3 = Parameter(name = 'Itdad1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad1x3}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 1, 3 ])

Itdad2x1 = Parameter(name = 'Itdad2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad2x1}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 2, 1 ])

Itdad2x2 = Parameter(name = 'Itdad2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad2x2}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 2, 2 ])

Itdad2x3 = Parameter(name = 'Itdad2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad2x3}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 2, 3 ])

Itdad3x1 = Parameter(name = 'Itdad3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad3x1}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 3, 1 ])

Itdad3x2 = Parameter(name = 'Itdad3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad3x2}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 3, 2 ])

Itdad3x3 = Parameter(name = 'Itdad3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdad3x3}',
                     lhablock = 'IMBCxtdad',
                     lhacode = [ 3, 3 ])

Itdae1x1 = Parameter(name = 'Itdae1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae1x1}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 1, 1 ])

Itdae1x2 = Parameter(name = 'Itdae1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae1x2}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 1, 2 ])

Itdae1x3 = Parameter(name = 'Itdae1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae1x3}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 1, 3 ])

Itdae2x1 = Parameter(name = 'Itdae2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae2x1}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 2, 1 ])

Itdae2x2 = Parameter(name = 'Itdae2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae2x2}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 2, 2 ])

Itdae2x3 = Parameter(name = 'Itdae2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae2x3}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 2, 3 ])

Itdae3x1 = Parameter(name = 'Itdae3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae3x1}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 3, 1 ])

Itdae3x2 = Parameter(name = 'Itdae3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae3x2}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 3, 2 ])

Itdae3x3 = Parameter(name = 'Itdae3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdae3x3}',
                     lhablock = 'IMBCxtdae',
                     lhacode = [ 3, 3 ])

Itdau1x1 = Parameter(name = 'Itdau1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau1x1}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 1, 1 ])

Itdau1x2 = Parameter(name = 'Itdau1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau1x2}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 1, 2 ])

Itdau1x3 = Parameter(name = 'Itdau1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau1x3}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 1, 3 ])

Itdau2x1 = Parameter(name = 'Itdau2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau2x1}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 2, 1 ])

Itdau2x2 = Parameter(name = 'Itdau2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau2x2}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 2, 2 ])

Itdau2x3 = Parameter(name = 'Itdau2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau2x3}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 2, 3 ])

Itdau3x1 = Parameter(name = 'Itdau3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau3x1}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 3, 1 ])

Itdau3x2 = Parameter(name = 'Itdau3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau3x2}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 3, 2 ])

Itdau3x3 = Parameter(name = 'Itdau3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdau3x3}',
                     lhablock = 'IMBCxtdau',
                     lhacode = [ 3, 3 ])

Itdgd1x1 = Parameter(name = 'Itdgd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd1x1}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 1, 1 ])

Itdgd1x2 = Parameter(name = 'Itdgd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd1x2}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 1, 2 ])

Itdgd1x3 = Parameter(name = 'Itdgd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd1x3}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 1, 3 ])

Itdgd2x1 = Parameter(name = 'Itdgd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd2x1}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 2, 1 ])

Itdgd2x2 = Parameter(name = 'Itdgd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd2x2}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 2, 2 ])

Itdgd2x3 = Parameter(name = 'Itdgd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd2x3}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 2, 3 ])

Itdgd3x1 = Parameter(name = 'Itdgd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd3x1}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 3, 1 ])

Itdgd3x2 = Parameter(name = 'Itdgd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd3x2}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 3, 2 ])

Itdgd3x3 = Parameter(name = 'Itdgd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgd3x3}',
                     lhablock = 'IMBCxtdgd',
                     lhacode = [ 3, 3 ])

Itdgu1x1 = Parameter(name = 'Itdgu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu1x1}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 1, 1 ])

Itdgu1x2 = Parameter(name = 'Itdgu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu1x2}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 1, 2 ])

Itdgu1x3 = Parameter(name = 'Itdgu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu1x3}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 1, 3 ])

Itdgu2x1 = Parameter(name = 'Itdgu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu2x1}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 2, 1 ])

Itdgu2x2 = Parameter(name = 'Itdgu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu2x2}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 2, 2 ])

Itdgu2x3 = Parameter(name = 'Itdgu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu2x3}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 2, 3 ])

Itdgu3x1 = Parameter(name = 'Itdgu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu3x1}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 3, 1 ])

Itdgu3x2 = Parameter(name = 'Itdgu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu3x2}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 3, 2 ])

Itdgu3x3 = Parameter(name = 'Itdgu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdgu3x3}',
                     lhablock = 'IMBCxtdgu',
                     lhacode = [ 3, 3 ])

Itdhad1x1 = Parameter(name = 'Itdhad1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad1x1}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 1, 1 ])

Itdhad1x2 = Parameter(name = 'Itdhad1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad1x2}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 1, 2 ])

Itdhad1x3 = Parameter(name = 'Itdhad1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad1x3}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 1, 3 ])

Itdhad2x1 = Parameter(name = 'Itdhad2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad2x1}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 2, 1 ])

Itdhad2x2 = Parameter(name = 'Itdhad2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad2x2}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 2, 2 ])

Itdhad2x3 = Parameter(name = 'Itdhad2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad2x3}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 2, 3 ])

Itdhad3x1 = Parameter(name = 'Itdhad3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad3x1}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 3, 1 ])

Itdhad3x2 = Parameter(name = 'Itdhad3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad3x2}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 3, 2 ])

Itdhad3x3 = Parameter(name = 'Itdhad3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhad3x3}',
                      lhablock = 'IMBCxtdhad',
                      lhacode = [ 3, 3 ])

Itdhae1x1 = Parameter(name = 'Itdhae1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae1x1}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 1, 1 ])

Itdhae1x2 = Parameter(name = 'Itdhae1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae1x2}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 1, 2 ])

Itdhae1x3 = Parameter(name = 'Itdhae1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae1x3}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 1, 3 ])

Itdhae2x1 = Parameter(name = 'Itdhae2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae2x1}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 2, 1 ])

Itdhae2x2 = Parameter(name = 'Itdhae2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae2x2}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 2, 2 ])

Itdhae2x3 = Parameter(name = 'Itdhae2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae2x3}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 2, 3 ])

Itdhae3x1 = Parameter(name = 'Itdhae3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae3x1}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 3, 1 ])

Itdhae3x2 = Parameter(name = 'Itdhae3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae3x2}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 3, 2 ])

Itdhae3x3 = Parameter(name = 'Itdhae3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhae3x3}',
                      lhablock = 'IMBCxtdhae',
                      lhacode = [ 3, 3 ])

Itdhau1x1 = Parameter(name = 'Itdhau1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau1x1}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 1, 1 ])

Itdhau1x2 = Parameter(name = 'Itdhau1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau1x2}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 1, 2 ])

Itdhau1x3 = Parameter(name = 'Itdhau1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau1x3}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 1, 3 ])

Itdhau2x1 = Parameter(name = 'Itdhau2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau2x1}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 2, 1 ])

Itdhau2x2 = Parameter(name = 'Itdhau2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau2x2}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 2, 2 ])

Itdhau2x3 = Parameter(name = 'Itdhau2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau2x3}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 2, 3 ])

Itdhau3x1 = Parameter(name = 'Itdhau3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau3x1}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 3, 1 ])

Itdhau3x2 = Parameter(name = 'Itdhau3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau3x2}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 3, 2 ])

Itdhau3x3 = Parameter(name = 'Itdhau3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhau3x3}',
                      lhablock = 'IMBCxtdhau',
                      lhacode = [ 3, 3 ])

Itdhgd1x1 = Parameter(name = 'Itdhgd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd1x1}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 1, 1 ])

Itdhgd1x2 = Parameter(name = 'Itdhgd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd1x2}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 1, 2 ])

Itdhgd1x3 = Parameter(name = 'Itdhgd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd1x3}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 1, 3 ])

Itdhgd2x1 = Parameter(name = 'Itdhgd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd2x1}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 2, 1 ])

Itdhgd2x2 = Parameter(name = 'Itdhgd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd2x2}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 2, 2 ])

Itdhgd2x3 = Parameter(name = 'Itdhgd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd2x3}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 2, 3 ])

Itdhgd3x1 = Parameter(name = 'Itdhgd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd3x1}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 3, 1 ])

Itdhgd3x2 = Parameter(name = 'Itdhgd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd3x2}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 3, 2 ])

Itdhgd3x3 = Parameter(name = 'Itdhgd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgd3x3}',
                      lhablock = 'IMBCxtdhgd',
                      lhacode = [ 3, 3 ])

Itdhgu1x1 = Parameter(name = 'Itdhgu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu1x1}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 1, 1 ])

Itdhgu1x2 = Parameter(name = 'Itdhgu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu1x2}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 1, 2 ])

Itdhgu1x3 = Parameter(name = 'Itdhgu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu1x3}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 1, 3 ])

Itdhgu2x1 = Parameter(name = 'Itdhgu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu2x1}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 2, 1 ])

Itdhgu2x2 = Parameter(name = 'Itdhgu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu2x2}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 2, 2 ])

Itdhgu2x3 = Parameter(name = 'Itdhgu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu2x3}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 2, 3 ])

Itdhgu3x1 = Parameter(name = 'Itdhgu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu3x1}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 3, 1 ])

Itdhgu3x2 = Parameter(name = 'Itdhgu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu3x2}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 3, 2 ])

Itdhgu3x3 = Parameter(name = 'Itdhgu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhgu3x3}',
                      lhablock = 'IMBCxtdhgu',
                      lhacode = [ 3, 3 ])

Itdhzd1x1 = Parameter(name = 'Itdhzd1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd1x1}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 1, 1 ])

Itdhzd1x2 = Parameter(name = 'Itdhzd1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd1x2}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 1, 2 ])

Itdhzd1x3 = Parameter(name = 'Itdhzd1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd1x3}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 1, 3 ])

Itdhzd2x1 = Parameter(name = 'Itdhzd2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd2x1}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 2, 1 ])

Itdhzd2x2 = Parameter(name = 'Itdhzd2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd2x2}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 2, 2 ])

Itdhzd2x3 = Parameter(name = 'Itdhzd2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd2x3}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 2, 3 ])

Itdhzd3x1 = Parameter(name = 'Itdhzd3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd3x1}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 3, 1 ])

Itdhzd3x2 = Parameter(name = 'Itdhzd3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd3x2}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 3, 2 ])

Itdhzd3x3 = Parameter(name = 'Itdhzd3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzd3x3}',
                      lhablock = 'IMBCxtdhzd',
                      lhacode = [ 3, 3 ])

Itdhze1x1 = Parameter(name = 'Itdhze1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze1x1}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 1, 1 ])

Itdhze1x2 = Parameter(name = 'Itdhze1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze1x2}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 1, 2 ])

Itdhze1x3 = Parameter(name = 'Itdhze1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze1x3}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 1, 3 ])

Itdhze2x1 = Parameter(name = 'Itdhze2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze2x1}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 2, 1 ])

Itdhze2x2 = Parameter(name = 'Itdhze2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze2x2}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 2, 2 ])

Itdhze2x3 = Parameter(name = 'Itdhze2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze2x3}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 2, 3 ])

Itdhze3x1 = Parameter(name = 'Itdhze3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze3x1}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 3, 1 ])

Itdhze3x2 = Parameter(name = 'Itdhze3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze3x2}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 3, 2 ])

Itdhze3x3 = Parameter(name = 'Itdhze3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhze3x3}',
                      lhablock = 'IMBCxtdhze',
                      lhacode = [ 3, 3 ])

Itdhzu1x1 = Parameter(name = 'Itdhzu1x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu1x1}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 1, 1 ])

Itdhzu1x2 = Parameter(name = 'Itdhzu1x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu1x2}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 1, 2 ])

Itdhzu1x3 = Parameter(name = 'Itdhzu1x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu1x3}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 1, 3 ])

Itdhzu2x1 = Parameter(name = 'Itdhzu2x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu2x1}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 2, 1 ])

Itdhzu2x2 = Parameter(name = 'Itdhzu2x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu2x2}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 2, 2 ])

Itdhzu2x3 = Parameter(name = 'Itdhzu2x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu2x3}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 2, 3 ])

Itdhzu3x1 = Parameter(name = 'Itdhzu3x1',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu3x1}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 3, 1 ])

Itdhzu3x2 = Parameter(name = 'Itdhzu3x2',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu3x2}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 3, 2 ])

Itdhzu3x3 = Parameter(name = 'Itdhzu3x3',
                      nature = 'external',
                      type = 'complex',
                      value = 0.,
                      texname = '\\text{Itdhzu3x3}',
                      lhablock = 'IMBCxtdhzu',
                      lhacode = [ 3, 3 ])

Itdzd1x1 = Parameter(name = 'Itdzd1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd1x1}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 1, 1 ])

Itdzd1x2 = Parameter(name = 'Itdzd1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd1x2}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 1, 2 ])

Itdzd1x3 = Parameter(name = 'Itdzd1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd1x3}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 1, 3 ])

Itdzd2x1 = Parameter(name = 'Itdzd2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd2x1}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 2, 1 ])

Itdzd2x2 = Parameter(name = 'Itdzd2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd2x2}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 2, 2 ])

Itdzd2x3 = Parameter(name = 'Itdzd2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd2x3}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 2, 3 ])

Itdzd3x1 = Parameter(name = 'Itdzd3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd3x1}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 3, 1 ])

Itdzd3x2 = Parameter(name = 'Itdzd3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd3x2}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 3, 2 ])

Itdzd3x3 = Parameter(name = 'Itdzd3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzd3x3}',
                     lhablock = 'IMBCxtdzd',
                     lhacode = [ 3, 3 ])

Itdze1x1 = Parameter(name = 'Itdze1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze1x1}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 1, 1 ])

Itdze1x2 = Parameter(name = 'Itdze1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze1x2}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 1, 2 ])

Itdze1x3 = Parameter(name = 'Itdze1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze1x3}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 1, 3 ])

Itdze2x1 = Parameter(name = 'Itdze2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze2x1}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 2, 1 ])

Itdze2x2 = Parameter(name = 'Itdze2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze2x2}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 2, 2 ])

Itdze2x3 = Parameter(name = 'Itdze2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze2x3}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 2, 3 ])

Itdze3x1 = Parameter(name = 'Itdze3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze3x1}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 3, 1 ])

Itdze3x2 = Parameter(name = 'Itdze3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze3x2}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 3, 2 ])

Itdze3x3 = Parameter(name = 'Itdze3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdze3x3}',
                     lhablock = 'IMBCxtdze',
                     lhacode = [ 3, 3 ])

Itdzu1x1 = Parameter(name = 'Itdzu1x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu1x1}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 1, 1 ])

Itdzu1x2 = Parameter(name = 'Itdzu1x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu1x2}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 1, 2 ])

Itdzu1x3 = Parameter(name = 'Itdzu1x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu1x3}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 1, 3 ])

Itdzu2x1 = Parameter(name = 'Itdzu2x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu2x1}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 2, 1 ])

Itdzu2x2 = Parameter(name = 'Itdzu2x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu2x2}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 2, 2 ])

Itdzu2x3 = Parameter(name = 'Itdzu2x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu2x3}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 2, 3 ])

Itdzu3x1 = Parameter(name = 'Itdzu3x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu3x1}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 3, 1 ])

Itdzu3x2 = Parameter(name = 'Itdzu3x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu3x2}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 3, 2 ])

Itdzu3x3 = Parameter(name = 'Itdzu3x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{Itdzu3x3}',
                     lhablock = 'IMBCxtdzu',
                     lhacode = [ 3, 3 ])

IdYd21x1 = Parameter(name = 'IdYd21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd21x1}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 1, 1 ])

IdYd21x2 = Parameter(name = 'IdYd21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd21x2}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 1, 2 ])

IdYd21x3 = Parameter(name = 'IdYd21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd21x3}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 1, 3 ])

IdYd22x1 = Parameter(name = 'IdYd22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd22x1}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 2, 1 ])

IdYd22x2 = Parameter(name = 'IdYd22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd22x2}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 2, 2 ])

IdYd22x3 = Parameter(name = 'IdYd22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd22x3}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 2, 3 ])

IdYd23x1 = Parameter(name = 'IdYd23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd23x1}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 3, 1 ])

IdYd23x2 = Parameter(name = 'IdYd23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd23x2}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 3, 2 ])

IdYd23x3 = Parameter(name = 'IdYd23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYd23x3}',
                     lhablock = 'IMBCxY2d',
                     lhacode = [ 3, 3 ])

IdYe21x1 = Parameter(name = 'IdYe21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe21x1}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 1, 1 ])

IdYe21x2 = Parameter(name = 'IdYe21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe21x2}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 1, 2 ])

IdYe21x3 = Parameter(name = 'IdYe21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe21x3}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 1, 3 ])

IdYe22x1 = Parameter(name = 'IdYe22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe22x1}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 2, 1 ])

IdYe22x2 = Parameter(name = 'IdYe22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe22x2}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 2, 2 ])

IdYe22x3 = Parameter(name = 'IdYe22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe22x3}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 2, 3 ])

IdYe23x1 = Parameter(name = 'IdYe23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe23x1}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 3, 1 ])

IdYe23x2 = Parameter(name = 'IdYe23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe23x2}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 3, 2 ])

IdYe23x3 = Parameter(name = 'IdYe23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYe23x3}',
                     lhablock = 'IMBCxY2e',
                     lhacode = [ 3, 3 ])

IdYu21x1 = Parameter(name = 'IdYu21x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu21x1}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 1, 1 ])

IdYu21x2 = Parameter(name = 'IdYu21x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu21x2}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 1, 2 ])

IdYu21x3 = Parameter(name = 'IdYu21x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu21x3}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 1, 3 ])

IdYu22x1 = Parameter(name = 'IdYu22x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu22x1}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 2, 1 ])

IdYu22x2 = Parameter(name = 'IdYu22x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu22x2}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 2, 2 ])

IdYu22x3 = Parameter(name = 'IdYu22x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu22x3}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 2, 3 ])

IdYu23x1 = Parameter(name = 'IdYu23x1',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu23x1}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 3, 1 ])

IdYu23x2 = Parameter(name = 'IdYu23x2',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu23x2}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 3, 2 ])

IdYu23x3 = Parameter(name = 'IdYu23x3',
                     nature = 'external',
                     type = 'complex',
                     value = 0.,
                     texname = '\\text{IdYu23x3}',
                     lhablock = 'IMBCxY2u',
                     lhacode = [ 3, 3 ])

ICKM1x1 = Parameter(name = 'ICKM1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM1x1}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 1, 1 ])

ICKM1x2 = Parameter(name = 'ICKM1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM1x2}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 1, 2 ])

ICKM1x3 = Parameter(name = 'ICKM1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM1x3}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 1, 3 ])

ICKM2x1 = Parameter(name = 'ICKM2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM2x1}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 2, 1 ])

ICKM2x2 = Parameter(name = 'ICKM2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM2x2}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 2, 2 ])

ICKM2x3 = Parameter(name = 'ICKM2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM2x3}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 2, 3 ])

ICKM3x1 = Parameter(name = 'ICKM3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM3x1}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 3, 1 ])

ICKM3x2 = Parameter(name = 'ICKM3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM3x2}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 3, 2 ])

ICKM3x3 = Parameter(name = 'ICKM3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{ICKM3x3}',
                    lhablock = 'IMVCKM',
                    lhacode = [ 3, 3 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

RCKM1x1 = Parameter(name = 'RCKM1x1',
                    nature = 'external',
                    type = 'complex',
                    value = 1,
                    texname = '\\text{RCKM1x1}',
                    lhablock = 'VCKM',
                    lhacode = [ 1, 1 ])

RCKM1x2 = Parameter(name = 'RCKM1x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM1x2}',
                    lhablock = 'VCKM',
                    lhacode = [ 1, 2 ])

RCKM1x3 = Parameter(name = 'RCKM1x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM1x3}',
                    lhablock = 'VCKM',
                    lhacode = [ 1, 3 ])

RCKM2x1 = Parameter(name = 'RCKM2x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM2x1}',
                    lhablock = 'VCKM',
                    lhacode = [ 2, 1 ])

RCKM2x2 = Parameter(name = 'RCKM2x2',
                    nature = 'external',
                    type = 'complex',
                    value = 1,
                    texname = '\\text{RCKM2x2}',
                    lhablock = 'VCKM',
                    lhacode = [ 2, 2 ])

RCKM2x3 = Parameter(name = 'RCKM2x3',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM2x3}',
                    lhablock = 'VCKM',
                    lhacode = [ 2, 3 ])

RCKM3x1 = Parameter(name = 'RCKM3x1',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM3x1}',
                    lhablock = 'VCKM',
                    lhacode = [ 3, 1 ])

RCKM3x2 = Parameter(name = 'RCKM3x2',
                    nature = 'external',
                    type = 'complex',
                    value = 0,
                    texname = '\\text{RCKM3x2}',
                    lhablock = 'VCKM',
                    lhacode = [ 3, 2 ])

RCKM3x3 = Parameter(name = 'RCKM3x3',
                    nature = 'external',
                    type = 'complex',
                    value = 1,
                    texname = '\\text{RCKM3x3}',
                    lhablock = 'VCKM',
                    lhacode = [ 3, 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 79.82436,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Cd1x1 = Parameter(name = 'Cd1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd1x1**2)',
                  texname = '\\text{Cd1x1}')

Cd1x2 = Parameter(name = 'Cd1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd1x2**2)',
                  texname = '\\text{Cd1x2}')

Cd1x3 = Parameter(name = 'Cd1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd1x3**2)',
                  texname = '\\text{Cd1x3}')

Cd2x1 = Parameter(name = 'Cd2x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd2x1**2)',
                  texname = '\\text{Cd2x1}')

Cd2x2 = Parameter(name = 'Cd2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd2x2**2)',
                  texname = '\\text{Cd2x2}')

Cd2x3 = Parameter(name = 'Cd2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd2x3**2)',
                  texname = '\\text{Cd2x3}')

Cd3x1 = Parameter(name = 'Cd3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd3x1**2)',
                  texname = '\\text{Cd3x1}')

Cd3x2 = Parameter(name = 'Cd3x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd3x2**2)',
                  texname = '\\text{Cd3x2}')

Cd3x3 = Parameter(name = 'Cd3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Sd3x3**2)',
                  texname = '\\text{Cd3x3}')

Ce1x1 = Parameter(name = 'Ce1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se1x1**2)',
                  texname = '\\text{Ce1x1}')

Ce1x2 = Parameter(name = 'Ce1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se1x2**2)',
                  texname = '\\text{Ce1x2}')

Ce1x3 = Parameter(name = 'Ce1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se1x3**2)',
                  texname = '\\text{Ce1x3}')

Ce2x1 = Parameter(name = 'Ce2x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se2x1**2)',
                  texname = '\\text{Ce2x1}')

Ce2x2 = Parameter(name = 'Ce2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se2x2**2)',
                  texname = '\\text{Ce2x2}')

Ce2x3 = Parameter(name = 'Ce2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se2x3**2)',
                  texname = '\\text{Ce2x3}')

Ce3x1 = Parameter(name = 'Ce3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se3x1**2)',
                  texname = '\\text{Ce3x1}')

Ce3x2 = Parameter(name = 'Ce3x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se3x2**2)',
                  texname = '\\text{Ce3x2}')

Ce3x3 = Parameter(name = 'Ce3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Se3x3**2)',
                  texname = '\\text{Ce3x3}')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM1x1 + RCKM1x1',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM1x2 + RCKM1x2',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM1x3 + RCKM1x3',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM2x1 + RCKM2x1',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM2x2 + RCKM2x2',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM2x3 + RCKM2x3',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM3x1 + RCKM3x1',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM3x2 + RCKM3x2',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*ICKM3x3 + RCKM3x3',
                   texname = '\\text{CKM3x3}')

Cu1x1 = Parameter(name = 'Cu1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su1x1**2)',
                  texname = '\\text{Cu1x1}')

Cu1x2 = Parameter(name = 'Cu1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su1x2**2)',
                  texname = '\\text{Cu1x2}')

Cu1x3 = Parameter(name = 'Cu1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su1x3**2)',
                  texname = '\\text{Cu1x3}')

Cu2x1 = Parameter(name = 'Cu2x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su2x1**2)',
                  texname = '\\text{Cu2x1}')

Cu2x2 = Parameter(name = 'Cu2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su2x2**2)',
                  texname = '\\text{Cu2x2}')

Cu2x3 = Parameter(name = 'Cu2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su2x3**2)',
                  texname = '\\text{Cu2x3}')

Cu3x1 = Parameter(name = 'Cu3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su3x1**2)',
                  texname = '\\text{Cu3x1}')

Cu3x2 = Parameter(name = 'Cu3x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su3x2**2)',
                  texname = '\\text{Cu3x2}')

Cu3x3 = Parameter(name = 'Cu3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - Su3x3**2)',
                  texname = '\\text{Cu3x3}')

gHaa = Parameter(name = 'gHaa',
                 nature = 'internal',
                 type = 'real',
                 value = '-47/(72.*cmath.pi**2)',
                 texname = 'g_{\\text{Haa}}')

gHgg = Parameter(name = 'gHgg',
                 nature = 'internal',
                 type = 'real',
                 value = '1/(12.*cmath.pi**2)',
                 texname = 'g_{\\text{Hgg}}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

dad1x1 = Parameter(name = 'dad1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad1x1 + Rdad1x1',
                   texname = '\\text{dad1x1}')

dad1x2 = Parameter(name = 'dad1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad1x2 + Rdad1x2',
                   texname = '\\text{dad1x2}')

dad1x3 = Parameter(name = 'dad1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad1x3 + Rdad1x3',
                   texname = '\\text{dad1x3}')

dad2x1 = Parameter(name = 'dad2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad2x1 + Rdad2x1',
                   texname = '\\text{dad2x1}')

dad2x2 = Parameter(name = 'dad2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad2x2 + Rdad2x2',
                   texname = '\\text{dad2x2}')

dad2x3 = Parameter(name = 'dad2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad2x3 + Rdad2x3',
                   texname = '\\text{dad2x3}')

dad3x1 = Parameter(name = 'dad3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad3x1 + Rdad3x1',
                   texname = '\\text{dad3x1}')

dad3x2 = Parameter(name = 'dad3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad3x2 + Rdad3x2',
                   texname = '\\text{dad3x2}')

dad3x3 = Parameter(name = 'dad3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idad3x3 + Rdad3x3',
                   texname = '\\text{dad3x3}')

dae1x1 = Parameter(name = 'dae1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae1x1 + Rdae1x1',
                   texname = '\\text{dae1x1}')

dae1x2 = Parameter(name = 'dae1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae1x2 + Rdae1x2',
                   texname = '\\text{dae1x2}')

dae1x3 = Parameter(name = 'dae1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae1x3 + Rdae1x3',
                   texname = '\\text{dae1x3}')

dae2x1 = Parameter(name = 'dae2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae2x1 + Rdae2x1',
                   texname = '\\text{dae2x1}')

dae2x2 = Parameter(name = 'dae2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae2x2 + Rdae2x2',
                   texname = '\\text{dae2x2}')

dae2x3 = Parameter(name = 'dae2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae2x3 + Rdae2x3',
                   texname = '\\text{dae2x3}')

dae3x1 = Parameter(name = 'dae3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae3x1 + Rdae3x1',
                   texname = '\\text{dae3x1}')

dae3x2 = Parameter(name = 'dae3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae3x2 + Rdae3x2',
                   texname = '\\text{dae3x2}')

dae3x3 = Parameter(name = 'dae3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idae3x3 + Rdae3x3',
                   texname = '\\text{dae3x3}')

dau1x1 = Parameter(name = 'dau1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau1x1 + Rdau1x1',
                   texname = '\\text{dau1x1}')

dau1x2 = Parameter(name = 'dau1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau1x2 + Rdau1x2',
                   texname = '\\text{dau1x2}')

dau1x3 = Parameter(name = 'dau1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau1x3 + Rdau1x3',
                   texname = '\\text{dau1x3}')

dau2x1 = Parameter(name = 'dau2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau2x1 + Rdau2x1',
                   texname = '\\text{dau2x1}')

dau2x2 = Parameter(name = 'dau2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau2x2 + Rdau2x2',
                   texname = '\\text{dau2x2}')

dau2x3 = Parameter(name = 'dau2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau2x3 + Rdau2x3',
                   texname = '\\text{dau2x3}')

dau3x1 = Parameter(name = 'dau3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau3x1 + Rdau3x1',
                   texname = '\\text{dau3x1}')

dau3x2 = Parameter(name = 'dau3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau3x2 + Rdau3x2',
                   texname = '\\text{dau3x2}')

dau3x3 = Parameter(name = 'dau3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idau3x3 + Rdau3x3',
                   texname = '\\text{dau3x3}')

dgd1x1 = Parameter(name = 'dgd1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd1x1 + Rdgd1x1',
                   texname = '\\text{dgd1x1}')

dgd1x2 = Parameter(name = 'dgd1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd1x2 + Rdgd1x2',
                   texname = '\\text{dgd1x2}')

dgd1x3 = Parameter(name = 'dgd1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd1x3 + Rdgd1x3',
                   texname = '\\text{dgd1x3}')

dgd2x1 = Parameter(name = 'dgd2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd2x1 + Rdgd2x1',
                   texname = '\\text{dgd2x1}')

dgd2x2 = Parameter(name = 'dgd2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd2x2 + Rdgd2x2',
                   texname = '\\text{dgd2x2}')

dgd2x3 = Parameter(name = 'dgd2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd2x3 + Rdgd2x3',
                   texname = '\\text{dgd2x3}')

dgd3x1 = Parameter(name = 'dgd3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd3x1 + Rdgd3x1',
                   texname = '\\text{dgd3x1}')

dgd3x2 = Parameter(name = 'dgd3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd3x2 + Rdgd3x2',
                   texname = '\\text{dgd3x2}')

dgd3x3 = Parameter(name = 'dgd3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgd3x3 + Rdgd3x3',
                   texname = '\\text{dgd3x3}')

dGLhwl1x1 = Parameter(name = 'dGLhwl1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl1x1 + RdGLhwl1x1',
                      texname = '\\text{dGLhwl1x1}')

dGLhwl1x2 = Parameter(name = 'dGLhwl1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl1x2 + RdGLhwl1x2',
                      texname = '\\text{dGLhwl1x2}')

dGLhwl1x3 = Parameter(name = 'dGLhwl1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl1x3 + RdGLhwl1x3',
                      texname = '\\text{dGLhwl1x3}')

dGLhwl2x1 = Parameter(name = 'dGLhwl2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl2x1 + RdGLhwl2x1',
                      texname = '\\text{dGLhwl2x1}')

dGLhwl2x2 = Parameter(name = 'dGLhwl2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl2x2 + RdGLhwl2x2',
                      texname = '\\text{dGLhwl2x2}')

dGLhwl2x3 = Parameter(name = 'dGLhwl2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl2x3 + RdGLhwl2x3',
                      texname = '\\text{dGLhwl2x3}')

dGLhwl3x1 = Parameter(name = 'dGLhwl3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl3x1 + RdGLhwl3x1',
                      texname = '\\text{dGLhwl3x1}')

dGLhwl3x2 = Parameter(name = 'dGLhwl3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl3x2 + RdGLhwl3x2',
                      texname = '\\text{dGLhwl3x2}')

dGLhwl3x3 = Parameter(name = 'dGLhwl3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwl3x3 + RdGLhwl3x3',
                      texname = '\\text{dGLhwl3x3}')

dGLhwq1x1 = Parameter(name = 'dGLhwq1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq1x1 + RdGLhwq1x1',
                      texname = '\\text{dGLhwq1x1}')

dGLhwq1x2 = Parameter(name = 'dGLhwq1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq1x2 + RdGLhwq1x2',
                      texname = '\\text{dGLhwq1x2}')

dGLhwq1x3 = Parameter(name = 'dGLhwq1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq1x3 + RdGLhwq1x3',
                      texname = '\\text{dGLhwq1x3}')

dGLhwq2x1 = Parameter(name = 'dGLhwq2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq2x1 + RdGLhwq2x1',
                      texname = '\\text{dGLhwq2x1}')

dGLhwq2x2 = Parameter(name = 'dGLhwq2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq2x2 + RdGLhwq2x2',
                      texname = '\\text{dGLhwq2x2}')

dGLhwq2x3 = Parameter(name = 'dGLhwq2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq2x3 + RdGLhwq2x3',
                      texname = '\\text{dGLhwq2x3}')

dGLhwq3x1 = Parameter(name = 'dGLhwq3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq3x1 + RdGLhwq3x1',
                      texname = '\\text{dGLhwq3x1}')

dGLhwq3x2 = Parameter(name = 'dGLhwq3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq3x2 + RdGLhwq3x2',
                      texname = '\\text{dGLhwq3x2}')

dGLhwq3x3 = Parameter(name = 'dGLhwq3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhwq3x3 + RdGLhwq3x3',
                      texname = '\\text{dGLhwq3x3}')

dGLhzd1x1 = Parameter(name = 'dGLhzd1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd1x1 + RdGLhzd1x1',
                      texname = '\\text{dGLhzd1x1}')

dGLhzd1x2 = Parameter(name = 'dGLhzd1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd1x2 + RdGLhzd1x2',
                      texname = '\\text{dGLhzd1x2}')

dGLhzd1x3 = Parameter(name = 'dGLhzd1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd1x3 + RdGLhzd1x3',
                      texname = '\\text{dGLhzd1x3}')

dGLhzd2x1 = Parameter(name = 'dGLhzd2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd2x1 + RdGLhzd2x1',
                      texname = '\\text{dGLhzd2x1}')

dGLhzd2x2 = Parameter(name = 'dGLhzd2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd2x2 + RdGLhzd2x2',
                      texname = '\\text{dGLhzd2x2}')

dGLhzd2x3 = Parameter(name = 'dGLhzd2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd2x3 + RdGLhzd2x3',
                      texname = '\\text{dGLhzd2x3}')

dGLhzd3x1 = Parameter(name = 'dGLhzd3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd3x1 + RdGLhzd3x1',
                      texname = '\\text{dGLhzd3x1}')

dGLhzd3x2 = Parameter(name = 'dGLhzd3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd3x2 + RdGLhzd3x2',
                      texname = '\\text{dGLhzd3x2}')

dGLhzd3x3 = Parameter(name = 'dGLhzd3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzd3x3 + RdGLhzd3x3',
                      texname = '\\text{dGLhzd3x3}')

dGLhze1x1 = Parameter(name = 'dGLhze1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze1x1 + RdGLhze1x1',
                      texname = '\\text{dGLhze1x1}')

dGLhze1x2 = Parameter(name = 'dGLhze1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze1x2 + RdGLhze1x2',
                      texname = '\\text{dGLhze1x2}')

dGLhze1x3 = Parameter(name = 'dGLhze1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze1x3 + RdGLhze1x3',
                      texname = '\\text{dGLhze1x3}')

dGLhze2x1 = Parameter(name = 'dGLhze2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze2x1 + RdGLhze2x1',
                      texname = '\\text{dGLhze2x1}')

dGLhze2x2 = Parameter(name = 'dGLhze2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze2x2 + RdGLhze2x2',
                      texname = '\\text{dGLhze2x2}')

dGLhze2x3 = Parameter(name = 'dGLhze2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze2x3 + RdGLhze2x3',
                      texname = '\\text{dGLhze2x3}')

dGLhze3x1 = Parameter(name = 'dGLhze3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze3x1 + RdGLhze3x1',
                      texname = '\\text{dGLhze3x1}')

dGLhze3x2 = Parameter(name = 'dGLhze3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze3x2 + RdGLhze3x2',
                      texname = '\\text{dGLhze3x2}')

dGLhze3x3 = Parameter(name = 'dGLhze3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhze3x3 + RdGLhze3x3',
                      texname = '\\text{dGLhze3x3}')

dGLhzu1x1 = Parameter(name = 'dGLhzu1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu1x1 + RdGLhzu1x1',
                      texname = '\\text{dGLhzu1x1}')

dGLhzu1x2 = Parameter(name = 'dGLhzu1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu1x2 + RdGLhzu1x2',
                      texname = '\\text{dGLhzu1x2}')

dGLhzu1x3 = Parameter(name = 'dGLhzu1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu1x3 + RdGLhzu1x3',
                      texname = '\\text{dGLhzu1x3}')

dGLhzu2x1 = Parameter(name = 'dGLhzu2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu2x1 + RdGLhzu2x1',
                      texname = '\\text{dGLhzu2x1}')

dGLhzu2x2 = Parameter(name = 'dGLhzu2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu2x2 + RdGLhzu2x2',
                      texname = '\\text{dGLhzu2x2}')

dGLhzu2x3 = Parameter(name = 'dGLhzu2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu2x3 + RdGLhzu2x3',
                      texname = '\\text{dGLhzu2x3}')

dGLhzu3x1 = Parameter(name = 'dGLhzu3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu3x1 + RdGLhzu3x1',
                      texname = '\\text{dGLhzu3x1}')

dGLhzu3x2 = Parameter(name = 'dGLhzu3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu3x2 + RdGLhzu3x2',
                      texname = '\\text{dGLhzu3x2}')

dGLhzu3x3 = Parameter(name = 'dGLhzu3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzu3x3 + RdGLhzu3x3',
                      texname = '\\text{dGLhzu3x3}')

dGLhzv1x1 = Parameter(name = 'dGLhzv1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv1x1 + RdGLhzv1x1',
                      texname = '\\text{dGLhzv1x1}')

dGLhzv1x2 = Parameter(name = 'dGLhzv1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv1x2 + RdGLhzv1x2',
                      texname = '\\text{dGLhzv1x2}')

dGLhzv1x3 = Parameter(name = 'dGLhzv1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv1x3 + RdGLhzv1x3',
                      texname = '\\text{dGLhzv1x3}')

dGLhzv2x1 = Parameter(name = 'dGLhzv2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv2x1 + RdGLhzv2x1',
                      texname = '\\text{dGLhzv2x1}')

dGLhzv2x2 = Parameter(name = 'dGLhzv2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv2x2 + RdGLhzv2x2',
                      texname = '\\text{dGLhzv2x2}')

dGLhzv2x3 = Parameter(name = 'dGLhzv2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv2x3 + RdGLhzv2x3',
                      texname = '\\text{dGLhzv2x3}')

dGLhzv3x1 = Parameter(name = 'dGLhzv3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv3x1 + RdGLhzv3x1',
                      texname = '\\text{dGLhzv3x1}')

dGLhzv3x2 = Parameter(name = 'dGLhzv3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv3x2 + RdGLhzv3x2',
                      texname = '\\text{dGLhzv3x2}')

dGLhzv3x3 = Parameter(name = 'dGLhzv3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGLhzv3x3 + RdGLhzv3x3',
                      texname = '\\text{dGLhzv3x3}')

dGLwl1x1 = Parameter(name = 'dGLwl1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl1x1 + RdGLwl1x1',
                     texname = '\\text{dGLwl1x1}')

dGLwl1x2 = Parameter(name = 'dGLwl1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl1x2 + RdGLwl1x2',
                     texname = '\\text{dGLwl1x2}')

dGLwl1x3 = Parameter(name = 'dGLwl1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl1x3 + RdGLwl1x3',
                     texname = '\\text{dGLwl1x3}')

dGLwl2x1 = Parameter(name = 'dGLwl2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl2x1 + RdGLwl2x1',
                     texname = '\\text{dGLwl2x1}')

dGLwl2x2 = Parameter(name = 'dGLwl2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl2x2 + RdGLwl2x2',
                     texname = '\\text{dGLwl2x2}')

dGLwl2x3 = Parameter(name = 'dGLwl2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl2x3 + RdGLwl2x3',
                     texname = '\\text{dGLwl2x3}')

dGLwl3x1 = Parameter(name = 'dGLwl3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl3x1 + RdGLwl3x1',
                     texname = '\\text{dGLwl3x1}')

dGLwl3x2 = Parameter(name = 'dGLwl3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl3x2 + RdGLwl3x2',
                     texname = '\\text{dGLwl3x2}')

dGLwl3x3 = Parameter(name = 'dGLwl3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwl3x3 + RdGLwl3x3',
                     texname = '\\text{dGLwl3x3}')

dGLwq1x1 = Parameter(name = 'dGLwq1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq1x1 + RdGLwq1x1',
                     texname = '\\text{dGLwq1x1}')

dGLwq1x2 = Parameter(name = 'dGLwq1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq1x2 + RdGLwq1x2',
                     texname = '\\text{dGLwq1x2}')

dGLwq1x3 = Parameter(name = 'dGLwq1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq1x3 + RdGLwq1x3',
                     texname = '\\text{dGLwq1x3}')

dGLwq2x1 = Parameter(name = 'dGLwq2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq2x1 + RdGLwq2x1',
                     texname = '\\text{dGLwq2x1}')

dGLwq2x2 = Parameter(name = 'dGLwq2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq2x2 + RdGLwq2x2',
                     texname = '\\text{dGLwq2x2}')

dGLwq2x3 = Parameter(name = 'dGLwq2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq2x3 + RdGLwq2x3',
                     texname = '\\text{dGLwq2x3}')

dGLwq3x1 = Parameter(name = 'dGLwq3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq3x1 + RdGLwq3x1',
                     texname = '\\text{dGLwq3x1}')

dGLwq3x2 = Parameter(name = 'dGLwq3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq3x2 + RdGLwq3x2',
                     texname = '\\text{dGLwq3x2}')

dGLwq3x3 = Parameter(name = 'dGLwq3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLwq3x3 + RdGLwq3x3',
                     texname = '\\text{dGLwq3x3}')

dGLzd1x1 = Parameter(name = 'dGLzd1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd1x1 + RdGLzd1x1',
                     texname = '\\text{dGLzd1x1}')

dGLzd1x2 = Parameter(name = 'dGLzd1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd1x2 + RdGLzd1x2',
                     texname = '\\text{dGLzd1x2}')

dGLzd1x3 = Parameter(name = 'dGLzd1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd1x3 + RdGLzd1x3',
                     texname = '\\text{dGLzd1x3}')

dGLzd2x1 = Parameter(name = 'dGLzd2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd2x1 + RdGLzd2x1',
                     texname = '\\text{dGLzd2x1}')

dGLzd2x2 = Parameter(name = 'dGLzd2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd2x2 + RdGLzd2x2',
                     texname = '\\text{dGLzd2x2}')

dGLzd2x3 = Parameter(name = 'dGLzd2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd2x3 + RdGLzd2x3',
                     texname = '\\text{dGLzd2x3}')

dGLzd3x1 = Parameter(name = 'dGLzd3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd3x1 + RdGLzd3x1',
                     texname = '\\text{dGLzd3x1}')

dGLzd3x2 = Parameter(name = 'dGLzd3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd3x2 + RdGLzd3x2',
                     texname = '\\text{dGLzd3x2}')

dGLzd3x3 = Parameter(name = 'dGLzd3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzd3x3 + RdGLzd3x3',
                     texname = '\\text{dGLzd3x3}')

dGLze1x1 = Parameter(name = 'dGLze1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze1x1 + RdGLze1x1',
                     texname = '\\text{dGLze1x1}')

dGLze1x2 = Parameter(name = 'dGLze1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze1x2 + RdGLze1x2',
                     texname = '\\text{dGLze1x2}')

dGLze1x3 = Parameter(name = 'dGLze1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze1x3 + RdGLze1x3',
                     texname = '\\text{dGLze1x3}')

dGLze2x1 = Parameter(name = 'dGLze2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze2x1 + RdGLze2x1',
                     texname = '\\text{dGLze2x1}')

dGLze2x2 = Parameter(name = 'dGLze2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze2x2 + RdGLze2x2',
                     texname = '\\text{dGLze2x2}')

dGLze2x3 = Parameter(name = 'dGLze2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze2x3 + RdGLze2x3',
                     texname = '\\text{dGLze2x3}')

dGLze3x1 = Parameter(name = 'dGLze3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze3x1 + RdGLze3x1',
                     texname = '\\text{dGLze3x1}')

dGLze3x2 = Parameter(name = 'dGLze3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze3x2 + RdGLze3x2',
                     texname = '\\text{dGLze3x2}')

dGLze3x3 = Parameter(name = 'dGLze3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLze3x3 + RdGLze3x3',
                     texname = '\\text{dGLze3x3}')

dGLzu1x1 = Parameter(name = 'dGLzu1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu1x1 + RdGLzu1x1',
                     texname = '\\text{dGLzu1x1}')

dGLzu1x2 = Parameter(name = 'dGLzu1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu1x2 + RdGLzu1x2',
                     texname = '\\text{dGLzu1x2}')

dGLzu1x3 = Parameter(name = 'dGLzu1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu1x3 + RdGLzu1x3',
                     texname = '\\text{dGLzu1x3}')

dGLzu2x1 = Parameter(name = 'dGLzu2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu2x1 + RdGLzu2x1',
                     texname = '\\text{dGLzu2x1}')

dGLzu2x2 = Parameter(name = 'dGLzu2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu2x2 + RdGLzu2x2',
                     texname = '\\text{dGLzu2x2}')

dGLzu2x3 = Parameter(name = 'dGLzu2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu2x3 + RdGLzu2x3',
                     texname = '\\text{dGLzu2x3}')

dGLzu3x1 = Parameter(name = 'dGLzu3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu3x1 + RdGLzu3x1',
                     texname = '\\text{dGLzu3x1}')

dGLzu3x2 = Parameter(name = 'dGLzu3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu3x2 + RdGLzu3x2',
                     texname = '\\text{dGLzu3x2}')

dGLzu3x3 = Parameter(name = 'dGLzu3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzu3x3 + RdGLzu3x3',
                     texname = '\\text{dGLzu3x3}')

dGLzv1x1 = Parameter(name = 'dGLzv1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv1x1 + RdGLzv1x1',
                     texname = '\\text{dGLzv1x1}')

dGLzv1x2 = Parameter(name = 'dGLzv1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv1x2 + RdGLzv1x2',
                     texname = '\\text{dGLzv1x2}')

dGLzv1x3 = Parameter(name = 'dGLzv1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv1x3 + RdGLzv1x3',
                     texname = '\\text{dGLzv1x3}')

dGLzv2x1 = Parameter(name = 'dGLzv2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv2x1 + RdGLzv2x1',
                     texname = '\\text{dGLzv2x1}')

dGLzv2x2 = Parameter(name = 'dGLzv2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv2x2 + RdGLzv2x2',
                     texname = '\\text{dGLzv2x2}')

dGLzv2x3 = Parameter(name = 'dGLzv2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv2x3 + RdGLzv2x3',
                     texname = '\\text{dGLzv2x3}')

dGLzv3x1 = Parameter(name = 'dGLzv3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv3x1 + RdGLzv3x1',
                     texname = '\\text{dGLzv3x1}')

dGLzv3x2 = Parameter(name = 'dGLzv3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv3x2 + RdGLzv3x2',
                     texname = '\\text{dGLzv3x2}')

dGLzv3x3 = Parameter(name = 'dGLzv3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGLzv3x3 + RdGLzv3x3',
                     texname = '\\text{dGLzv3x3}')

dGRhwq1x1 = Parameter(name = 'dGRhwq1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq1x1 + RdGRhwq1x1',
                      texname = '\\text{dGRhwq1x1}')

dGRhwq1x2 = Parameter(name = 'dGRhwq1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq1x2 + RdGRhwq1x2',
                      texname = '\\text{dGRhwq1x2}')

dGRhwq1x3 = Parameter(name = 'dGRhwq1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq1x3 + RdGRhwq1x3',
                      texname = '\\text{dGRhwq1x3}')

dGRhwq2x1 = Parameter(name = 'dGRhwq2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq2x1 + RdGRhwq2x1',
                      texname = '\\text{dGRhwq2x1}')

dGRhwq2x2 = Parameter(name = 'dGRhwq2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq2x2 + RdGRhwq2x2',
                      texname = '\\text{dGRhwq2x2}')

dGRhwq2x3 = Parameter(name = 'dGRhwq2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq2x3 + RdGRhwq2x3',
                      texname = '\\text{dGRhwq2x3}')

dGRhwq3x1 = Parameter(name = 'dGRhwq3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq3x1 + RdGRhwq3x1',
                      texname = '\\text{dGRhwq3x1}')

dGRhwq3x2 = Parameter(name = 'dGRhwq3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq3x2 + RdGRhwq3x2',
                      texname = '\\text{dGRhwq3x2}')

dGRhwq3x3 = Parameter(name = 'dGRhwq3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhwq3x3 + RdGRhwq3x3',
                      texname = '\\text{dGRhwq3x3}')

dGRhzd1x1 = Parameter(name = 'dGRhzd1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd1x1 + RdGRhzd1x1',
                      texname = '\\text{dGRhzd1x1}')

dGRhzd1x2 = Parameter(name = 'dGRhzd1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd1x2 + RdGRhzd1x2',
                      texname = '\\text{dGRhzd1x2}')

dGRhzd1x3 = Parameter(name = 'dGRhzd1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd1x3 + RdGRhzd1x3',
                      texname = '\\text{dGRhzd1x3}')

dGRhzd2x1 = Parameter(name = 'dGRhzd2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd2x1 + RdGRhzd2x1',
                      texname = '\\text{dGRhzd2x1}')

dGRhzd2x2 = Parameter(name = 'dGRhzd2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd2x2 + RdGRhzd2x2',
                      texname = '\\text{dGRhzd2x2}')

dGRhzd2x3 = Parameter(name = 'dGRhzd2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd2x3 + RdGRhzd2x3',
                      texname = '\\text{dGRhzd2x3}')

dGRhzd3x1 = Parameter(name = 'dGRhzd3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd3x1 + RdGRhzd3x1',
                      texname = '\\text{dGRhzd3x1}')

dGRhzd3x2 = Parameter(name = 'dGRhzd3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd3x2 + RdGRhzd3x2',
                      texname = '\\text{dGRhzd3x2}')

dGRhzd3x3 = Parameter(name = 'dGRhzd3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzd3x3 + RdGRhzd3x3',
                      texname = '\\text{dGRhzd3x3}')

dGRhze1x1 = Parameter(name = 'dGRhze1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze1x1 + RdGRhze1x1',
                      texname = '\\text{dGRhze1x1}')

dGRhze1x2 = Parameter(name = 'dGRhze1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze1x2 + RdGRhze1x2',
                      texname = '\\text{dGRhze1x2}')

dGRhze1x3 = Parameter(name = 'dGRhze1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze1x3 + RdGRhze1x3',
                      texname = '\\text{dGRhze1x3}')

dGRhze2x1 = Parameter(name = 'dGRhze2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze2x1 + RdGRhze2x1',
                      texname = '\\text{dGRhze2x1}')

dGRhze2x2 = Parameter(name = 'dGRhze2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze2x2 + RdGRhze2x2',
                      texname = '\\text{dGRhze2x2}')

dGRhze2x3 = Parameter(name = 'dGRhze2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze2x3 + RdGRhze2x3',
                      texname = '\\text{dGRhze2x3}')

dGRhze3x1 = Parameter(name = 'dGRhze3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze3x1 + RdGRhze3x1',
                      texname = '\\text{dGRhze3x1}')

dGRhze3x2 = Parameter(name = 'dGRhze3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze3x2 + RdGRhze3x2',
                      texname = '\\text{dGRhze3x2}')

dGRhze3x3 = Parameter(name = 'dGRhze3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhze3x3 + RdGRhze3x3',
                      texname = '\\text{dGRhze3x3}')

dGRhzu1x1 = Parameter(name = 'dGRhzu1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu1x1 + RdGRhzu1x1',
                      texname = '\\text{dGRhzu1x1}')

dGRhzu1x2 = Parameter(name = 'dGRhzu1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu1x2 + RdGRhzu1x2',
                      texname = '\\text{dGRhzu1x2}')

dGRhzu1x3 = Parameter(name = 'dGRhzu1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu1x3 + RdGRhzu1x3',
                      texname = '\\text{dGRhzu1x3}')

dGRhzu2x1 = Parameter(name = 'dGRhzu2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu2x1 + RdGRhzu2x1',
                      texname = '\\text{dGRhzu2x1}')

dGRhzu2x2 = Parameter(name = 'dGRhzu2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu2x2 + RdGRhzu2x2',
                      texname = '\\text{dGRhzu2x2}')

dGRhzu2x3 = Parameter(name = 'dGRhzu2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu2x3 + RdGRhzu2x3',
                      texname = '\\text{dGRhzu2x3}')

dGRhzu3x1 = Parameter(name = 'dGRhzu3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu3x1 + RdGRhzu3x1',
                      texname = '\\text{dGRhzu3x1}')

dGRhzu3x2 = Parameter(name = 'dGRhzu3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu3x2 + RdGRhzu3x2',
                      texname = '\\text{dGRhzu3x2}')

dGRhzu3x3 = Parameter(name = 'dGRhzu3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'complex(0,1)*IdGRhzu3x3 + RdGRhzu3x3',
                      texname = '\\text{dGRhzu3x3}')

dGRwq1x1 = Parameter(name = 'dGRwq1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq1x1 + RdGRwq1x1',
                     texname = '\\text{dGRwq1x1}')

dGRwq1x2 = Parameter(name = 'dGRwq1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq1x2 + RdGRwq1x2',
                     texname = '\\text{dGRwq1x2}')

dGRwq1x3 = Parameter(name = 'dGRwq1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq1x3 + RdGRwq1x3',
                     texname = '\\text{dGRwq1x3}')

dGRwq2x1 = Parameter(name = 'dGRwq2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq2x1 + RdGRwq2x1',
                     texname = '\\text{dGRwq2x1}')

dGRwq2x2 = Parameter(name = 'dGRwq2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq2x2 + RdGRwq2x2',
                     texname = '\\text{dGRwq2x2}')

dGRwq2x3 = Parameter(name = 'dGRwq2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq2x3 + RdGRwq2x3',
                     texname = '\\text{dGRwq2x3}')

dGRwq3x1 = Parameter(name = 'dGRwq3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq3x1 + RdGRwq3x1',
                     texname = '\\text{dGRwq3x1}')

dGRwq3x2 = Parameter(name = 'dGRwq3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq3x2 + RdGRwq3x2',
                     texname = '\\text{dGRwq3x2}')

dGRwq3x3 = Parameter(name = 'dGRwq3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRwq3x3 + RdGRwq3x3',
                     texname = '\\text{dGRwq3x3}')

dGRzd1x1 = Parameter(name = 'dGRzd1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd1x1 + RdGRzd1x1',
                     texname = '\\text{dGRzd1x1}')

dGRzd1x2 = Parameter(name = 'dGRzd1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd1x2 + RdGRzd1x2',
                     texname = '\\text{dGRzd1x2}')

dGRzd1x3 = Parameter(name = 'dGRzd1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd1x3 + RdGRzd1x3',
                     texname = '\\text{dGRzd1x3}')

dGRzd2x1 = Parameter(name = 'dGRzd2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd2x1 + RdGRzd2x1',
                     texname = '\\text{dGRzd2x1}')

dGRzd2x2 = Parameter(name = 'dGRzd2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd2x2 + RdGRzd2x2',
                     texname = '\\text{dGRzd2x2}')

dGRzd2x3 = Parameter(name = 'dGRzd2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd2x3 + RdGRzd2x3',
                     texname = '\\text{dGRzd2x3}')

dGRzd3x1 = Parameter(name = 'dGRzd3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd3x1 + RdGRzd3x1',
                     texname = '\\text{dGRzd3x1}')

dGRzd3x2 = Parameter(name = 'dGRzd3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd3x2 + RdGRzd3x2',
                     texname = '\\text{dGRzd3x2}')

dGRzd3x3 = Parameter(name = 'dGRzd3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzd3x3 + RdGRzd3x3',
                     texname = '\\text{dGRzd3x3}')

dGRze1x1 = Parameter(name = 'dGRze1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze1x1 + RdGRze1x1',
                     texname = '\\text{dGRze1x1}')

dGRze1x2 = Parameter(name = 'dGRze1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze1x2 + RdGRze1x2',
                     texname = '\\text{dGRze1x2}')

dGRze1x3 = Parameter(name = 'dGRze1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze1x3 + RdGRze1x3',
                     texname = '\\text{dGRze1x3}')

dGRze2x1 = Parameter(name = 'dGRze2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze2x1 + RdGRze2x1',
                     texname = '\\text{dGRze2x1}')

dGRze2x2 = Parameter(name = 'dGRze2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze2x2 + RdGRze2x2',
                     texname = '\\text{dGRze2x2}')

dGRze2x3 = Parameter(name = 'dGRze2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze2x3 + RdGRze2x3',
                     texname = '\\text{dGRze2x3}')

dGRze3x1 = Parameter(name = 'dGRze3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze3x1 + RdGRze3x1',
                     texname = '\\text{dGRze3x1}')

dGRze3x2 = Parameter(name = 'dGRze3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze3x2 + RdGRze3x2',
                     texname = '\\text{dGRze3x2}')

dGRze3x3 = Parameter(name = 'dGRze3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRze3x3 + RdGRze3x3',
                     texname = '\\text{dGRze3x3}')

dGRzu1x1 = Parameter(name = 'dGRzu1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu1x1 + RdGRzu1x1',
                     texname = '\\text{dGRzu1x1}')

dGRzu1x2 = Parameter(name = 'dGRzu1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu1x2 + RdGRzu1x2',
                     texname = '\\text{dGRzu1x2}')

dGRzu1x3 = Parameter(name = 'dGRzu1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu1x3 + RdGRzu1x3',
                     texname = '\\text{dGRzu1x3}')

dGRzu2x1 = Parameter(name = 'dGRzu2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu2x1 + RdGRzu2x1',
                     texname = '\\text{dGRzu2x1}')

dGRzu2x2 = Parameter(name = 'dGRzu2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu2x2 + RdGRzu2x2',
                     texname = '\\text{dGRzu2x2}')

dGRzu2x3 = Parameter(name = 'dGRzu2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu2x3 + RdGRzu2x3',
                     texname = '\\text{dGRzu2x3}')

dGRzu3x1 = Parameter(name = 'dGRzu3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu3x1 + RdGRzu3x1',
                     texname = '\\text{dGRzu3x1}')

dGRzu3x2 = Parameter(name = 'dGRzu3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu3x2 + RdGRzu3x2',
                     texname = '\\text{dGRzu3x2}')

dGRzu3x3 = Parameter(name = 'dGRzu3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*IdGRzu3x3 + RdGRzu3x3',
                     texname = '\\text{dGRzu3x3}')

dgu1x1 = Parameter(name = 'dgu1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu1x1 + Rdgu1x1',
                   texname = '\\text{dgu1x1}')

dgu1x2 = Parameter(name = 'dgu1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu1x2 + Rdgu1x2',
                   texname = '\\text{dgu1x2}')

dgu1x3 = Parameter(name = 'dgu1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu1x3 + Rdgu1x3',
                   texname = '\\text{dgu1x3}')

dgu2x1 = Parameter(name = 'dgu2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu2x1 + Rdgu2x1',
                   texname = '\\text{dgu2x1}')

dgu2x2 = Parameter(name = 'dgu2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu2x2 + Rdgu2x2',
                   texname = '\\text{dgu2x2}')

dgu2x3 = Parameter(name = 'dgu2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu2x3 + Rdgu2x3',
                   texname = '\\text{dgu2x3}')

dgu3x1 = Parameter(name = 'dgu3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu3x1 + Rdgu3x1',
                   texname = '\\text{dgu3x1}')

dgu3x2 = Parameter(name = 'dgu3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu3x2 + Rdgu3x2',
                   texname = '\\text{dgu3x2}')

dgu3x3 = Parameter(name = 'dgu3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idgu3x3 + Rdgu3x3',
                   texname = '\\text{dgu3x3}')

dhad1x1 = Parameter(name = 'dhad1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad1x1 + Rdhad1x1',
                    texname = '\\text{dhad1x1}')

dhad1x2 = Parameter(name = 'dhad1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad1x2 + Rdhad1x2',
                    texname = '\\text{dhad1x2}')

dhad1x3 = Parameter(name = 'dhad1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad1x3 + Rdhad1x3',
                    texname = '\\text{dhad1x3}')

dhad2x1 = Parameter(name = 'dhad2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad2x1 + Rdhad2x1',
                    texname = '\\text{dhad2x1}')

dhad2x2 = Parameter(name = 'dhad2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad2x2 + Rdhad2x2',
                    texname = '\\text{dhad2x2}')

dhad2x3 = Parameter(name = 'dhad2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad2x3 + Rdhad2x3',
                    texname = '\\text{dhad2x3}')

dhad3x1 = Parameter(name = 'dhad3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad3x1 + Rdhad3x1',
                    texname = '\\text{dhad3x1}')

dhad3x2 = Parameter(name = 'dhad3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad3x2 + Rdhad3x2',
                    texname = '\\text{dhad3x2}')

dhad3x3 = Parameter(name = 'dhad3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhad3x3 + Rdhad3x3',
                    texname = '\\text{dhad3x3}')

dhae1x1 = Parameter(name = 'dhae1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae1x1 + Rdhae1x1',
                    texname = '\\text{dhae1x1}')

dhae1x2 = Parameter(name = 'dhae1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae1x2 + Rdhae1x2',
                    texname = '\\text{dhae1x2}')

dhae1x3 = Parameter(name = 'dhae1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae1x3 + Rdhae1x3',
                    texname = '\\text{dhae1x3}')

dhae2x1 = Parameter(name = 'dhae2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae2x1 + Rdhae2x1',
                    texname = '\\text{dhae2x1}')

dhae2x2 = Parameter(name = 'dhae2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae2x2 + Rdhae2x2',
                    texname = '\\text{dhae2x2}')

dhae2x3 = Parameter(name = 'dhae2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae2x3 + Rdhae2x3',
                    texname = '\\text{dhae2x3}')

dhae3x1 = Parameter(name = 'dhae3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae3x1 + Rdhae3x1',
                    texname = '\\text{dhae3x1}')

dhae3x2 = Parameter(name = 'dhae3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae3x2 + Rdhae3x2',
                    texname = '\\text{dhae3x2}')

dhae3x3 = Parameter(name = 'dhae3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhae3x3 + Rdhae3x3',
                    texname = '\\text{dhae3x3}')

dhau1x1 = Parameter(name = 'dhau1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau1x1 + Rdhau1x1',
                    texname = '\\text{dhau1x1}')

dhau1x2 = Parameter(name = 'dhau1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau1x2 + Rdhau1x2',
                    texname = '\\text{dhau1x2}')

dhau1x3 = Parameter(name = 'dhau1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau1x3 + Rdhau1x3',
                    texname = '\\text{dhau1x3}')

dhau2x1 = Parameter(name = 'dhau2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau2x1 + Rdhau2x1',
                    texname = '\\text{dhau2x1}')

dhau2x2 = Parameter(name = 'dhau2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau2x2 + Rdhau2x2',
                    texname = '\\text{dhau2x2}')

dhau2x3 = Parameter(name = 'dhau2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau2x3 + Rdhau2x3',
                    texname = '\\text{dhau2x3}')

dhau3x1 = Parameter(name = 'dhau3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau3x1 + Rdhau3x1',
                    texname = '\\text{dhau3x1}')

dhau3x2 = Parameter(name = 'dhau3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau3x2 + Rdhau3x2',
                    texname = '\\text{dhau3x2}')

dhau3x3 = Parameter(name = 'dhau3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhau3x3 + Rdhau3x3',
                    texname = '\\text{dhau3x3}')

dhgd1x1 = Parameter(name = 'dhgd1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd1x1 + Rdhgd1x1',
                    texname = '\\text{dhgd1x1}')

dhgd1x2 = Parameter(name = 'dhgd1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd1x2 + Rdhgd1x2',
                    texname = '\\text{dhgd1x2}')

dhgd1x3 = Parameter(name = 'dhgd1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd1x3 + Rdhgd1x3',
                    texname = '\\text{dhgd1x3}')

dhgd2x1 = Parameter(name = 'dhgd2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd2x1 + Rdhgd2x1',
                    texname = '\\text{dhgd2x1}')

dhgd2x2 = Parameter(name = 'dhgd2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd2x2 + Rdhgd2x2',
                    texname = '\\text{dhgd2x2}')

dhgd2x3 = Parameter(name = 'dhgd2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd2x3 + Rdhgd2x3',
                    texname = '\\text{dhgd2x3}')

dhgd3x1 = Parameter(name = 'dhgd3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd3x1 + Rdhgd3x1',
                    texname = '\\text{dhgd3x1}')

dhgd3x2 = Parameter(name = 'dhgd3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd3x2 + Rdhgd3x2',
                    texname = '\\text{dhgd3x2}')

dhgd3x3 = Parameter(name = 'dhgd3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgd3x3 + Rdhgd3x3',
                    texname = '\\text{dhgd3x3}')

dhgu1x1 = Parameter(name = 'dhgu1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu1x1 + Rdhgu1x1',
                    texname = '\\text{dhgu1x1}')

dhgu1x2 = Parameter(name = 'dhgu1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu1x2 + Rdhgu1x2',
                    texname = '\\text{dhgu1x2}')

dhgu1x3 = Parameter(name = 'dhgu1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu1x3 + Rdhgu1x3',
                    texname = '\\text{dhgu1x3}')

dhgu2x1 = Parameter(name = 'dhgu2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu2x1 + Rdhgu2x1',
                    texname = '\\text{dhgu2x1}')

dhgu2x2 = Parameter(name = 'dhgu2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu2x2 + Rdhgu2x2',
                    texname = '\\text{dhgu2x2}')

dhgu2x3 = Parameter(name = 'dhgu2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu2x3 + Rdhgu2x3',
                    texname = '\\text{dhgu2x3}')

dhgu3x1 = Parameter(name = 'dhgu3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu3x1 + Rdhgu3x1',
                    texname = '\\text{dhgu3x1}')

dhgu3x2 = Parameter(name = 'dhgu3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu3x2 + Rdhgu3x2',
                    texname = '\\text{dhgu3x2}')

dhgu3x3 = Parameter(name = 'dhgu3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhgu3x3 + Rdhgu3x3',
                    texname = '\\text{dhgu3x3}')

dhwd1x1 = Parameter(name = 'dhwd1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd1x1 + Rdhwd1x1',
                    texname = '\\text{dhwd1x1}')

dhwd1x2 = Parameter(name = 'dhwd1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd1x2 + Rdhwd1x2',
                    texname = '\\text{dhwd1x2}')

dhwd1x3 = Parameter(name = 'dhwd1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd1x3 + Rdhwd1x3',
                    texname = '\\text{dhwd1x3}')

dhwd2x1 = Parameter(name = 'dhwd2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd2x1 + Rdhwd2x1',
                    texname = '\\text{dhwd2x1}')

dhwd2x2 = Parameter(name = 'dhwd2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd2x2 + Rdhwd2x2',
                    texname = '\\text{dhwd2x2}')

dhwd2x3 = Parameter(name = 'dhwd2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd2x3 + Rdhwd2x3',
                    texname = '\\text{dhwd2x3}')

dhwd3x1 = Parameter(name = 'dhwd3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd3x1 + Rdhwd3x1',
                    texname = '\\text{dhwd3x1}')

dhwd3x2 = Parameter(name = 'dhwd3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd3x2 + Rdhwd3x2',
                    texname = '\\text{dhwd3x2}')

dhwd3x3 = Parameter(name = 'dhwd3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwd3x3 + Rdhwd3x3',
                    texname = '\\text{dhwd3x3}')

dhwl1x1 = Parameter(name = 'dhwl1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl1x1 + Rdhwl1x1',
                    texname = '\\text{dhwl1x1}')

dhwl1x2 = Parameter(name = 'dhwl1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl1x2 + Rdhwl1x2',
                    texname = '\\text{dhwl1x2}')

dhwl1x3 = Parameter(name = 'dhwl1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl1x3 + Rdhwl1x3',
                    texname = '\\text{dhwl1x3}')

dhwl2x1 = Parameter(name = 'dhwl2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl2x1 + Rdhwl2x1',
                    texname = '\\text{dhwl2x1}')

dhwl2x2 = Parameter(name = 'dhwl2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl2x2 + Rdhwl2x2',
                    texname = '\\text{dhwl2x2}')

dhwl2x3 = Parameter(name = 'dhwl2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl2x3 + Rdhwl2x3',
                    texname = '\\text{dhwl2x3}')

dhwl3x1 = Parameter(name = 'dhwl3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl3x1 + Rdhwl3x1',
                    texname = '\\text{dhwl3x1}')

dhwl3x2 = Parameter(name = 'dhwl3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl3x2 + Rdhwl3x2',
                    texname = '\\text{dhwl3x2}')

dhwl3x3 = Parameter(name = 'dhwl3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwl3x3 + Rdhwl3x3',
                    texname = '\\text{dhwl3x3}')

dhwu1x1 = Parameter(name = 'dhwu1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu1x1 + Rdhwu1x1',
                    texname = '\\text{dhwu1x1}')

dhwu1x2 = Parameter(name = 'dhwu1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu1x2 + Rdhwu1x2',
                    texname = '\\text{dhwu1x2}')

dhwu1x3 = Parameter(name = 'dhwu1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu1x3 + Rdhwu1x3',
                    texname = '\\text{dhwu1x3}')

dhwu2x1 = Parameter(name = 'dhwu2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu2x1 + Rdhwu2x1',
                    texname = '\\text{dhwu2x1}')

dhwu2x2 = Parameter(name = 'dhwu2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu2x2 + Rdhwu2x2',
                    texname = '\\text{dhwu2x2}')

dhwu2x3 = Parameter(name = 'dhwu2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu2x3 + Rdhwu2x3',
                    texname = '\\text{dhwu2x3}')

dhwu3x1 = Parameter(name = 'dhwu3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu3x1 + Rdhwu3x1',
                    texname = '\\text{dhwu3x1}')

dhwu3x2 = Parameter(name = 'dhwu3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu3x2 + Rdhwu3x2',
                    texname = '\\text{dhwu3x2}')

dhwu3x3 = Parameter(name = 'dhwu3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhwu3x3 + Rdhwu3x3',
                    texname = '\\text{dhwu3x3}')

dhzd1x1 = Parameter(name = 'dhzd1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd1x1 + Rdhzd1x1',
                    texname = '\\text{dhzd1x1}')

dhzd1x2 = Parameter(name = 'dhzd1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd1x2 + Rdhzd1x2',
                    texname = '\\text{dhzd1x2}')

dhzd1x3 = Parameter(name = 'dhzd1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd1x3 + Rdhzd1x3',
                    texname = '\\text{dhzd1x3}')

dhzd2x1 = Parameter(name = 'dhzd2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd2x1 + Rdhzd2x1',
                    texname = '\\text{dhzd2x1}')

dhzd2x2 = Parameter(name = 'dhzd2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd2x2 + Rdhzd2x2',
                    texname = '\\text{dhzd2x2}')

dhzd2x3 = Parameter(name = 'dhzd2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd2x3 + Rdhzd2x3',
                    texname = '\\text{dhzd2x3}')

dhzd3x1 = Parameter(name = 'dhzd3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd3x1 + Rdhzd3x1',
                    texname = '\\text{dhzd3x1}')

dhzd3x2 = Parameter(name = 'dhzd3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd3x2 + Rdhzd3x2',
                    texname = '\\text{dhzd3x2}')

dhzd3x3 = Parameter(name = 'dhzd3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzd3x3 + Rdhzd3x3',
                    texname = '\\text{dhzd3x3}')

dhze1x1 = Parameter(name = 'dhze1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze1x1 + Rdhze1x1',
                    texname = '\\text{dhze1x1}')

dhze1x2 = Parameter(name = 'dhze1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze1x2 + Rdhze1x2',
                    texname = '\\text{dhze1x2}')

dhze1x3 = Parameter(name = 'dhze1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze1x3 + Rdhze1x3',
                    texname = '\\text{dhze1x3}')

dhze2x1 = Parameter(name = 'dhze2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze2x1 + Rdhze2x1',
                    texname = '\\text{dhze2x1}')

dhze2x2 = Parameter(name = 'dhze2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze2x2 + Rdhze2x2',
                    texname = '\\text{dhze2x2}')

dhze2x3 = Parameter(name = 'dhze2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze2x3 + Rdhze2x3',
                    texname = '\\text{dhze2x3}')

dhze3x1 = Parameter(name = 'dhze3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze3x1 + Rdhze3x1',
                    texname = '\\text{dhze3x1}')

dhze3x2 = Parameter(name = 'dhze3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze3x2 + Rdhze3x2',
                    texname = '\\text{dhze3x2}')

dhze3x3 = Parameter(name = 'dhze3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhze3x3 + Rdhze3x3',
                    texname = '\\text{dhze3x3}')

dhzu1x1 = Parameter(name = 'dhzu1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu1x1 + Rdhzu1x1',
                    texname = '\\text{dhzu1x1}')

dhzu1x2 = Parameter(name = 'dhzu1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu1x2 + Rdhzu1x2',
                    texname = '\\text{dhzu1x2}')

dhzu1x3 = Parameter(name = 'dhzu1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu1x3 + Rdhzu1x3',
                    texname = '\\text{dhzu1x3}')

dhzu2x1 = Parameter(name = 'dhzu2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu2x1 + Rdhzu2x1',
                    texname = '\\text{dhzu2x1}')

dhzu2x2 = Parameter(name = 'dhzu2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu2x2 + Rdhzu2x2',
                    texname = '\\text{dhzu2x2}')

dhzu2x3 = Parameter(name = 'dhzu2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu2x3 + Rdhzu2x3',
                    texname = '\\text{dhzu2x3}')

dhzu3x1 = Parameter(name = 'dhzu3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu3x1 + Rdhzu3x1',
                    texname = '\\text{dhzu3x1}')

dhzu3x2 = Parameter(name = 'dhzu3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu3x2 + Rdhzu3x2',
                    texname = '\\text{dhzu3x2}')

dhzu3x3 = Parameter(name = 'dhzu3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Idhzu3x3 + Rdhzu3x3',
                    texname = '\\text{dhzu3x3}')

dwd1x1 = Parameter(name = 'dwd1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd1x1 + Rdwd1x1',
                   texname = '\\text{dwd1x1}')

dwd1x2 = Parameter(name = 'dwd1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd1x2 + Rdwd1x2',
                   texname = '\\text{dwd1x2}')

dwd1x3 = Parameter(name = 'dwd1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd1x3 + Rdwd1x3',
                   texname = '\\text{dwd1x3}')

dwd2x1 = Parameter(name = 'dwd2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd2x1 + Rdwd2x1',
                   texname = '\\text{dwd2x1}')

dwd2x2 = Parameter(name = 'dwd2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd2x2 + Rdwd2x2',
                   texname = '\\text{dwd2x2}')

dwd2x3 = Parameter(name = 'dwd2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd2x3 + Rdwd2x3',
                   texname = '\\text{dwd2x3}')

dwd3x1 = Parameter(name = 'dwd3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd3x1 + Rdwd3x1',
                   texname = '\\text{dwd3x1}')

dwd3x2 = Parameter(name = 'dwd3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd3x2 + Rdwd3x2',
                   texname = '\\text{dwd3x2}')

dwd3x3 = Parameter(name = 'dwd3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwd3x3 + Rdwd3x3',
                   texname = '\\text{dwd3x3}')

dwl1x1 = Parameter(name = 'dwl1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl1x1 + Rdwl1x1',
                   texname = '\\text{dwl1x1}')

dwl1x2 = Parameter(name = 'dwl1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl1x2 + Rdwl1x2',
                   texname = '\\text{dwl1x2}')

dwl1x3 = Parameter(name = 'dwl1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl1x3 + Rdwl1x3',
                   texname = '\\text{dwl1x3}')

dwl2x1 = Parameter(name = 'dwl2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl2x1 + Rdwl2x1',
                   texname = '\\text{dwl2x1}')

dwl2x2 = Parameter(name = 'dwl2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl2x2 + Rdwl2x2',
                   texname = '\\text{dwl2x2}')

dwl2x3 = Parameter(name = 'dwl2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl2x3 + Rdwl2x3',
                   texname = '\\text{dwl2x3}')

dwl3x1 = Parameter(name = 'dwl3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl3x1 + Rdwl3x1',
                   texname = '\\text{dwl3x1}')

dwl3x2 = Parameter(name = 'dwl3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl3x2 + Rdwl3x2',
                   texname = '\\text{dwl3x2}')

dwl3x3 = Parameter(name = 'dwl3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwl3x3 + Rdwl3x3',
                   texname = '\\text{dwl3x3}')

dwu1x1 = Parameter(name = 'dwu1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu1x1 + Rdwu1x1',
                   texname = '\\text{dwu1x1}')

dwu1x2 = Parameter(name = 'dwu1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu1x2 + Rdwu1x2',
                   texname = '\\text{dwu1x2}')

dwu1x3 = Parameter(name = 'dwu1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu1x3 + Rdwu1x3',
                   texname = '\\text{dwu1x3}')

dwu2x1 = Parameter(name = 'dwu2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu2x1 + Rdwu2x1',
                   texname = '\\text{dwu2x1}')

dwu2x2 = Parameter(name = 'dwu2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu2x2 + Rdwu2x2',
                   texname = '\\text{dwu2x2}')

dwu2x3 = Parameter(name = 'dwu2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu2x3 + Rdwu2x3',
                   texname = '\\text{dwu2x3}')

dwu3x1 = Parameter(name = 'dwu3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu3x1 + Rdwu3x1',
                   texname = '\\text{dwu3x1}')

dwu3x2 = Parameter(name = 'dwu3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu3x2 + Rdwu3x2',
                   texname = '\\text{dwu3x2}')

dwu3x3 = Parameter(name = 'dwu3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idwu3x3 + Rdwu3x3',
                   texname = '\\text{dwu3x3}')

dYd21x1 = Parameter(name = 'dYd21x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd21x1 + RdYd21x1',
                    texname = '\\text{dYd21x1}')

dYd21x2 = Parameter(name = 'dYd21x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd21x2 + RdYd21x2',
                    texname = '\\text{dYd21x2}')

dYd21x3 = Parameter(name = 'dYd21x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd21x3 + RdYd21x3',
                    texname = '\\text{dYd21x3}')

dYd22x1 = Parameter(name = 'dYd22x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd22x1 + RdYd22x1',
                    texname = '\\text{dYd22x1}')

dYd22x2 = Parameter(name = 'dYd22x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd22x2 + RdYd22x2',
                    texname = '\\text{dYd22x2}')

dYd22x3 = Parameter(name = 'dYd22x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd22x3 + RdYd22x3',
                    texname = '\\text{dYd22x3}')

dYd23x1 = Parameter(name = 'dYd23x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd23x1 + RdYd23x1',
                    texname = '\\text{dYd23x1}')

dYd23x2 = Parameter(name = 'dYd23x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd23x2 + RdYd23x2',
                    texname = '\\text{dYd23x2}')

dYd23x3 = Parameter(name = 'dYd23x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYd23x3 + RdYd23x3',
                    texname = '\\text{dYd23x3}')

dYe21x1 = Parameter(name = 'dYe21x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe21x1 + RdYe21x1',
                    texname = '\\text{dYe21x1}')

dYe21x2 = Parameter(name = 'dYe21x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe21x2 + RdYe21x2',
                    texname = '\\text{dYe21x2}')

dYe21x3 = Parameter(name = 'dYe21x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe21x3 + RdYe21x3',
                    texname = '\\text{dYe21x3}')

dYe22x1 = Parameter(name = 'dYe22x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe22x1 + RdYe22x1',
                    texname = '\\text{dYe22x1}')

dYe22x2 = Parameter(name = 'dYe22x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe22x2 + RdYe22x2',
                    texname = '\\text{dYe22x2}')

dYe22x3 = Parameter(name = 'dYe22x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe22x3 + RdYe22x3',
                    texname = '\\text{dYe22x3}')

dYe23x1 = Parameter(name = 'dYe23x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe23x1 + RdYe23x1',
                    texname = '\\text{dYe23x1}')

dYe23x2 = Parameter(name = 'dYe23x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe23x2 + RdYe23x2',
                    texname = '\\text{dYe23x2}')

dYe23x3 = Parameter(name = 'dYe23x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYe23x3 + RdYe23x3',
                    texname = '\\text{dYe23x3}')

dYu21x1 = Parameter(name = 'dYu21x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu21x1 + RdYu21x1',
                    texname = '\\text{dYu21x1}')

dYu21x2 = Parameter(name = 'dYu21x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu21x2 + RdYu21x2',
                    texname = '\\text{dYu21x2}')

dYu21x3 = Parameter(name = 'dYu21x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu21x3 + RdYu21x3',
                    texname = '\\text{dYu21x3}')

dYu22x1 = Parameter(name = 'dYu22x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu22x1 + RdYu22x1',
                    texname = '\\text{dYu22x1}')

dYu22x2 = Parameter(name = 'dYu22x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu22x2 + RdYu22x2',
                    texname = '\\text{dYu22x2}')

dYu22x3 = Parameter(name = 'dYu22x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu22x3 + RdYu22x3',
                    texname = '\\text{dYu22x3}')

dYu23x1 = Parameter(name = 'dYu23x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu23x1 + RdYu23x1',
                    texname = '\\text{dYu23x1}')

dYu23x2 = Parameter(name = 'dYu23x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu23x2 + RdYu23x2',
                    texname = '\\text{dYu23x2}')

dYu23x3 = Parameter(name = 'dYu23x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*IdYu23x3 + RdYu23x3',
                    texname = '\\text{dYu23x3}')

dzd1x1 = Parameter(name = 'dzd1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd1x1 + Rdzd1x1',
                   texname = '\\text{dzd1x1}')

dzd1x2 = Parameter(name = 'dzd1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd1x2 + Rdzd1x2',
                   texname = '\\text{dzd1x2}')

dzd1x3 = Parameter(name = 'dzd1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd1x3 + Rdzd1x3',
                   texname = '\\text{dzd1x3}')

dzd2x1 = Parameter(name = 'dzd2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd2x1 + Rdzd2x1',
                   texname = '\\text{dzd2x1}')

dzd2x2 = Parameter(name = 'dzd2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd2x2 + Rdzd2x2',
                   texname = '\\text{dzd2x2}')

dzd2x3 = Parameter(name = 'dzd2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd2x3 + Rdzd2x3',
                   texname = '\\text{dzd2x3}')

dzd3x1 = Parameter(name = 'dzd3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd3x1 + Rdzd3x1',
                   texname = '\\text{dzd3x1}')

dzd3x2 = Parameter(name = 'dzd3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd3x2 + Rdzd3x2',
                   texname = '\\text{dzd3x2}')

dzd3x3 = Parameter(name = 'dzd3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzd3x3 + Rdzd3x3',
                   texname = '\\text{dzd3x3}')

dze1x1 = Parameter(name = 'dze1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze1x1 + Rdze1x1',
                   texname = '\\text{dze1x1}')

dze1x2 = Parameter(name = 'dze1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze1x2 + Rdze1x2',
                   texname = '\\text{dze1x2}')

dze1x3 = Parameter(name = 'dze1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze1x3 + Rdze1x3',
                   texname = '\\text{dze1x3}')

dze2x1 = Parameter(name = 'dze2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze2x1 + Rdze2x1',
                   texname = '\\text{dze2x1}')

dze2x2 = Parameter(name = 'dze2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze2x2 + Rdze2x2',
                   texname = '\\text{dze2x2}')

dze2x3 = Parameter(name = 'dze2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze2x3 + Rdze2x3',
                   texname = '\\text{dze2x3}')

dze3x1 = Parameter(name = 'dze3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze3x1 + Rdze3x1',
                   texname = '\\text{dze3x1}')

dze3x2 = Parameter(name = 'dze3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze3x2 + Rdze3x2',
                   texname = '\\text{dze3x2}')

dze3x3 = Parameter(name = 'dze3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idze3x3 + Rdze3x3',
                   texname = '\\text{dze3x3}')

dzu1x1 = Parameter(name = 'dzu1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu1x1 + Rdzu1x1',
                   texname = '\\text{dzu1x1}')

dzu1x2 = Parameter(name = 'dzu1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu1x2 + Rdzu1x2',
                   texname = '\\text{dzu1x2}')

dzu1x3 = Parameter(name = 'dzu1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu1x3 + Rdzu1x3',
                   texname = '\\text{dzu1x3}')

dzu2x1 = Parameter(name = 'dzu2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu2x1 + Rdzu2x1',
                   texname = '\\text{dzu2x1}')

dzu2x2 = Parameter(name = 'dzu2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu2x2 + Rdzu2x2',
                   texname = '\\text{dzu2x2}')

dzu2x3 = Parameter(name = 'dzu2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu2x3 + Rdzu2x3',
                   texname = '\\text{dzu2x3}')

dzu3x1 = Parameter(name = 'dzu3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu3x1 + Rdzu3x1',
                   texname = '\\text{dzu3x1}')

dzu3x2 = Parameter(name = 'dzu3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu3x2 + Rdzu3x2',
                   texname = '\\text{dzu3x2}')

dzu3x3 = Parameter(name = 'dzu3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complex(0,1)*Idzu3x3 + Rdzu3x3',
                   texname = '\\text{dzu3x3}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

tdad1x1 = Parameter(name = 'tdad1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad1x1 + Rtdad1x1',
                    texname = '\\text{tdad1x1}')

tdad1x2 = Parameter(name = 'tdad1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad1x2 + Rtdad1x2',
                    texname = '\\text{tdad1x2}')

tdad1x3 = Parameter(name = 'tdad1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad1x3 + Rtdad1x3',
                    texname = '\\text{tdad1x3}')

tdad2x1 = Parameter(name = 'tdad2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad2x1 + Rtdad2x1',
                    texname = '\\text{tdad2x1}')

tdad2x2 = Parameter(name = 'tdad2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad2x2 + Rtdad2x2',
                    texname = '\\text{tdad2x2}')

tdad2x3 = Parameter(name = 'tdad2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad2x3 + Rtdad2x3',
                    texname = '\\text{tdad2x3}')

tdad3x1 = Parameter(name = 'tdad3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad3x1 + Rtdad3x1',
                    texname = '\\text{tdad3x1}')

tdad3x2 = Parameter(name = 'tdad3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad3x2 + Rtdad3x2',
                    texname = '\\text{tdad3x2}')

tdad3x3 = Parameter(name = 'tdad3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdad3x3 + Rtdad3x3',
                    texname = '\\text{tdad3x3}')

tdae1x1 = Parameter(name = 'tdae1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae1x1 + Rtdae1x1',
                    texname = '\\text{tdae1x1}')

tdae1x2 = Parameter(name = 'tdae1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae1x2 + Rtdae1x2',
                    texname = '\\text{tdae1x2}')

tdae1x3 = Parameter(name = 'tdae1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae1x3 + Rtdae1x3',
                    texname = '\\text{tdae1x3}')

tdae2x1 = Parameter(name = 'tdae2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae2x1 + Rtdae2x1',
                    texname = '\\text{tdae2x1}')

tdae2x2 = Parameter(name = 'tdae2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae2x2 + Rtdae2x2',
                    texname = '\\text{tdae2x2}')

tdae2x3 = Parameter(name = 'tdae2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae2x3 + Rtdae2x3',
                    texname = '\\text{tdae2x3}')

tdae3x1 = Parameter(name = 'tdae3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae3x1 + Rtdae3x1',
                    texname = '\\text{tdae3x1}')

tdae3x2 = Parameter(name = 'tdae3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae3x2 + Rtdae3x2',
                    texname = '\\text{tdae3x2}')

tdae3x3 = Parameter(name = 'tdae3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdae3x3 + Rtdae3x3',
                    texname = '\\text{tdae3x3}')

tdau1x1 = Parameter(name = 'tdau1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau1x1 + Rtdau1x1',
                    texname = '\\text{tdau1x1}')

tdau1x2 = Parameter(name = 'tdau1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau1x2 + Rtdau1x2',
                    texname = '\\text{tdau1x2}')

tdau1x3 = Parameter(name = 'tdau1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau1x3 + Rtdau1x3',
                    texname = '\\text{tdau1x3}')

tdau2x1 = Parameter(name = 'tdau2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau2x1 + Rtdau2x1',
                    texname = '\\text{tdau2x1}')

tdau2x2 = Parameter(name = 'tdau2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau2x2 + Rtdau2x2',
                    texname = '\\text{tdau2x2}')

tdau2x3 = Parameter(name = 'tdau2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau2x3 + Rtdau2x3',
                    texname = '\\text{tdau2x3}')

tdau3x1 = Parameter(name = 'tdau3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau3x1 + Rtdau3x1',
                    texname = '\\text{tdau3x1}')

tdau3x2 = Parameter(name = 'tdau3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau3x2 + Rtdau3x2',
                    texname = '\\text{tdau3x2}')

tdau3x3 = Parameter(name = 'tdau3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdau3x3 + Rtdau3x3',
                    texname = '\\text{tdau3x3}')

tdgd1x1 = Parameter(name = 'tdgd1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd1x1 + Rtdgd1x1',
                    texname = '\\text{tdgd1x1}')

tdgd1x2 = Parameter(name = 'tdgd1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd1x2 + Rtdgd1x2',
                    texname = '\\text{tdgd1x2}')

tdgd1x3 = Parameter(name = 'tdgd1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd1x3 + Rtdgd1x3',
                    texname = '\\text{tdgd1x3}')

tdgd2x1 = Parameter(name = 'tdgd2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd2x1 + Rtdgd2x1',
                    texname = '\\text{tdgd2x1}')

tdgd2x2 = Parameter(name = 'tdgd2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd2x2 + Rtdgd2x2',
                    texname = '\\text{tdgd2x2}')

tdgd2x3 = Parameter(name = 'tdgd2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd2x3 + Rtdgd2x3',
                    texname = '\\text{tdgd2x3}')

tdgd3x1 = Parameter(name = 'tdgd3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd3x1 + Rtdgd3x1',
                    texname = '\\text{tdgd3x1}')

tdgd3x2 = Parameter(name = 'tdgd3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd3x2 + Rtdgd3x2',
                    texname = '\\text{tdgd3x2}')

tdgd3x3 = Parameter(name = 'tdgd3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgd3x3 + Rtdgd3x3',
                    texname = '\\text{tdgd3x3}')

tdgu1x1 = Parameter(name = 'tdgu1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu1x1 + Rtdgu1x1',
                    texname = '\\text{tdgu1x1}')

tdgu1x2 = Parameter(name = 'tdgu1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu1x2 + Rtdgu1x2',
                    texname = '\\text{tdgu1x2}')

tdgu1x3 = Parameter(name = 'tdgu1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu1x3 + Rtdgu1x3',
                    texname = '\\text{tdgu1x3}')

tdgu2x1 = Parameter(name = 'tdgu2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu2x1 + Rtdgu2x1',
                    texname = '\\text{tdgu2x1}')

tdgu2x2 = Parameter(name = 'tdgu2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu2x2 + Rtdgu2x2',
                    texname = '\\text{tdgu2x2}')

tdgu2x3 = Parameter(name = 'tdgu2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu2x3 + Rtdgu2x3',
                    texname = '\\text{tdgu2x3}')

tdgu3x1 = Parameter(name = 'tdgu3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu3x1 + Rtdgu3x1',
                    texname = '\\text{tdgu3x1}')

tdgu3x2 = Parameter(name = 'tdgu3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu3x2 + Rtdgu3x2',
                    texname = '\\text{tdgu3x2}')

tdgu3x3 = Parameter(name = 'tdgu3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdgu3x3 + Rtdgu3x3',
                    texname = '\\text{tdgu3x3}')

tdhad1x1 = Parameter(name = 'tdhad1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad1x1 + Rtdhad1x1',
                     texname = '\\text{tdhad1x1}')

tdhad1x2 = Parameter(name = 'tdhad1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad1x2 + Rtdhad1x2',
                     texname = '\\text{tdhad1x2}')

tdhad1x3 = Parameter(name = 'tdhad1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad1x3 + Rtdhad1x3',
                     texname = '\\text{tdhad1x3}')

tdhad2x1 = Parameter(name = 'tdhad2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad2x1 + Rtdhad2x1',
                     texname = '\\text{tdhad2x1}')

tdhad2x2 = Parameter(name = 'tdhad2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad2x2 + Rtdhad2x2',
                     texname = '\\text{tdhad2x2}')

tdhad2x3 = Parameter(name = 'tdhad2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad2x3 + Rtdhad2x3',
                     texname = '\\text{tdhad2x3}')

tdhad3x1 = Parameter(name = 'tdhad3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad3x1 + Rtdhad3x1',
                     texname = '\\text{tdhad3x1}')

tdhad3x2 = Parameter(name = 'tdhad3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad3x2 + Rtdhad3x2',
                     texname = '\\text{tdhad3x2}')

tdhad3x3 = Parameter(name = 'tdhad3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhad3x3 + Rtdhad3x3',
                     texname = '\\text{tdhad3x3}')

tdhae1x1 = Parameter(name = 'tdhae1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae1x1 + Rtdhae1x1',
                     texname = '\\text{tdhae1x1}')

tdhae1x2 = Parameter(name = 'tdhae1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae1x2 + Rtdhae1x2',
                     texname = '\\text{tdhae1x2}')

tdhae1x3 = Parameter(name = 'tdhae1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae1x3 + Rtdhae1x3',
                     texname = '\\text{tdhae1x3}')

tdhae2x1 = Parameter(name = 'tdhae2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae2x1 + Rtdhae2x1',
                     texname = '\\text{tdhae2x1}')

tdhae2x2 = Parameter(name = 'tdhae2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae2x2 + Rtdhae2x2',
                     texname = '\\text{tdhae2x2}')

tdhae2x3 = Parameter(name = 'tdhae2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae2x3 + Rtdhae2x3',
                     texname = '\\text{tdhae2x3}')

tdhae3x1 = Parameter(name = 'tdhae3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae3x1 + Rtdhae3x1',
                     texname = '\\text{tdhae3x1}')

tdhae3x2 = Parameter(name = 'tdhae3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae3x2 + Rtdhae3x2',
                     texname = '\\text{tdhae3x2}')

tdhae3x3 = Parameter(name = 'tdhae3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhae3x3 + Rtdhae3x3',
                     texname = '\\text{tdhae3x3}')

tdhau1x1 = Parameter(name = 'tdhau1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau1x1 + Rtdhau1x1',
                     texname = '\\text{tdhau1x1}')

tdhau1x2 = Parameter(name = 'tdhau1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau1x2 + Rtdhau1x2',
                     texname = '\\text{tdhau1x2}')

tdhau1x3 = Parameter(name = 'tdhau1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau1x3 + Rtdhau1x3',
                     texname = '\\text{tdhau1x3}')

tdhau2x1 = Parameter(name = 'tdhau2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau2x1 + Rtdhau2x1',
                     texname = '\\text{tdhau2x1}')

tdhau2x2 = Parameter(name = 'tdhau2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau2x2 + Rtdhau2x2',
                     texname = '\\text{tdhau2x2}')

tdhau2x3 = Parameter(name = 'tdhau2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau2x3 + Rtdhau2x3',
                     texname = '\\text{tdhau2x3}')

tdhau3x1 = Parameter(name = 'tdhau3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau3x1 + Rtdhau3x1',
                     texname = '\\text{tdhau3x1}')

tdhau3x2 = Parameter(name = 'tdhau3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau3x2 + Rtdhau3x2',
                     texname = '\\text{tdhau3x2}')

tdhau3x3 = Parameter(name = 'tdhau3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhau3x3 + Rtdhau3x3',
                     texname = '\\text{tdhau3x3}')

tdhgd1x1 = Parameter(name = 'tdhgd1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd1x1 + Rtdhgd1x1',
                     texname = '\\text{tdhgd1x1}')

tdhgd1x2 = Parameter(name = 'tdhgd1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd1x2 + Rtdhgd1x2',
                     texname = '\\text{tdhgd1x2}')

tdhgd1x3 = Parameter(name = 'tdhgd1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd1x3 + Rtdhgd1x3',
                     texname = '\\text{tdhgd1x3}')

tdhgd2x1 = Parameter(name = 'tdhgd2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd2x1 + Rtdhgd2x1',
                     texname = '\\text{tdhgd2x1}')

tdhgd2x2 = Parameter(name = 'tdhgd2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd2x2 + Rtdhgd2x2',
                     texname = '\\text{tdhgd2x2}')

tdhgd2x3 = Parameter(name = 'tdhgd2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd2x3 + Rtdhgd2x3',
                     texname = '\\text{tdhgd2x3}')

tdhgd3x1 = Parameter(name = 'tdhgd3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd3x1 + Rtdhgd3x1',
                     texname = '\\text{tdhgd3x1}')

tdhgd3x2 = Parameter(name = 'tdhgd3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd3x2 + Rtdhgd3x2',
                     texname = '\\text{tdhgd3x2}')

tdhgd3x3 = Parameter(name = 'tdhgd3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgd3x3 + Rtdhgd3x3',
                     texname = '\\text{tdhgd3x3}')

tdhgu1x1 = Parameter(name = 'tdhgu1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu1x1 + Rtdhgu1x1',
                     texname = '\\text{tdhgu1x1}')

tdhgu1x2 = Parameter(name = 'tdhgu1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu1x2 + Rtdhgu1x2',
                     texname = '\\text{tdhgu1x2}')

tdhgu1x3 = Parameter(name = 'tdhgu1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu1x3 + Rtdhgu1x3',
                     texname = '\\text{tdhgu1x3}')

tdhgu2x1 = Parameter(name = 'tdhgu2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu2x1 + Rtdhgu2x1',
                     texname = '\\text{tdhgu2x1}')

tdhgu2x2 = Parameter(name = 'tdhgu2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu2x2 + Rtdhgu2x2',
                     texname = '\\text{tdhgu2x2}')

tdhgu2x3 = Parameter(name = 'tdhgu2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu2x3 + Rtdhgu2x3',
                     texname = '\\text{tdhgu2x3}')

tdhgu3x1 = Parameter(name = 'tdhgu3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu3x1 + Rtdhgu3x1',
                     texname = '\\text{tdhgu3x1}')

tdhgu3x2 = Parameter(name = 'tdhgu3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu3x2 + Rtdhgu3x2',
                     texname = '\\text{tdhgu3x2}')

tdhgu3x3 = Parameter(name = 'tdhgu3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhgu3x3 + Rtdhgu3x3',
                     texname = '\\text{tdhgu3x3}')

tdhzd1x1 = Parameter(name = 'tdhzd1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd1x1 + Rtdhzd1x1',
                     texname = '\\text{tdhzd1x1}')

tdhzd1x2 = Parameter(name = 'tdhzd1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd1x2 + Rtdhzd1x2',
                     texname = '\\text{tdhzd1x2}')

tdhzd1x3 = Parameter(name = 'tdhzd1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd1x3 + Rtdhzd1x3',
                     texname = '\\text{tdhzd1x3}')

tdhzd2x1 = Parameter(name = 'tdhzd2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd2x1 + Rtdhzd2x1',
                     texname = '\\text{tdhzd2x1}')

tdhzd2x2 = Parameter(name = 'tdhzd2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd2x2 + Rtdhzd2x2',
                     texname = '\\text{tdhzd2x2}')

tdhzd2x3 = Parameter(name = 'tdhzd2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd2x3 + Rtdhzd2x3',
                     texname = '\\text{tdhzd2x3}')

tdhzd3x1 = Parameter(name = 'tdhzd3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd3x1 + Rtdhzd3x1',
                     texname = '\\text{tdhzd3x1}')

tdhzd3x2 = Parameter(name = 'tdhzd3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd3x2 + Rtdhzd3x2',
                     texname = '\\text{tdhzd3x2}')

tdhzd3x3 = Parameter(name = 'tdhzd3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzd3x3 + Rtdhzd3x3',
                     texname = '\\text{tdhzd3x3}')

tdhze1x1 = Parameter(name = 'tdhze1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze1x1 + Rtdhze1x1',
                     texname = '\\text{tdhze1x1}')

tdhze1x2 = Parameter(name = 'tdhze1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze1x2 + Rtdhze1x2',
                     texname = '\\text{tdhze1x2}')

tdhze1x3 = Parameter(name = 'tdhze1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze1x3 + Rtdhze1x3',
                     texname = '\\text{tdhze1x3}')

tdhze2x1 = Parameter(name = 'tdhze2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze2x1 + Rtdhze2x1',
                     texname = '\\text{tdhze2x1}')

tdhze2x2 = Parameter(name = 'tdhze2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze2x2 + Rtdhze2x2',
                     texname = '\\text{tdhze2x2}')

tdhze2x3 = Parameter(name = 'tdhze2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze2x3 + Rtdhze2x3',
                     texname = '\\text{tdhze2x3}')

tdhze3x1 = Parameter(name = 'tdhze3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze3x1 + Rtdhze3x1',
                     texname = '\\text{tdhze3x1}')

tdhze3x2 = Parameter(name = 'tdhze3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze3x2 + Rtdhze3x2',
                     texname = '\\text{tdhze3x2}')

tdhze3x3 = Parameter(name = 'tdhze3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhze3x3 + Rtdhze3x3',
                     texname = '\\text{tdhze3x3}')

tdhzu1x1 = Parameter(name = 'tdhzu1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu1x1 + Rtdhzu1x1',
                     texname = '\\text{tdhzu1x1}')

tdhzu1x2 = Parameter(name = 'tdhzu1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu1x2 + Rtdhzu1x2',
                     texname = '\\text{tdhzu1x2}')

tdhzu1x3 = Parameter(name = 'tdhzu1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu1x3 + Rtdhzu1x3',
                     texname = '\\text{tdhzu1x3}')

tdhzu2x1 = Parameter(name = 'tdhzu2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu2x1 + Rtdhzu2x1',
                     texname = '\\text{tdhzu2x1}')

tdhzu2x2 = Parameter(name = 'tdhzu2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu2x2 + Rtdhzu2x2',
                     texname = '\\text{tdhzu2x2}')

tdhzu2x3 = Parameter(name = 'tdhzu2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu2x3 + Rtdhzu2x3',
                     texname = '\\text{tdhzu2x3}')

tdhzu3x1 = Parameter(name = 'tdhzu3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu3x1 + Rtdhzu3x1',
                     texname = '\\text{tdhzu3x1}')

tdhzu3x2 = Parameter(name = 'tdhzu3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu3x2 + Rtdhzu3x2',
                     texname = '\\text{tdhzu3x2}')

tdhzu3x3 = Parameter(name = 'tdhzu3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complex(0,1)*Itdhzu3x3 + Rtdhzu3x3',
                     texname = '\\text{tdhzu3x3}')

tdzd1x1 = Parameter(name = 'tdzd1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd1x1 + Rtdzd1x1',
                    texname = '\\text{tdzd1x1}')

tdzd1x2 = Parameter(name = 'tdzd1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd1x2 + Rtdzd1x2',
                    texname = '\\text{tdzd1x2}')

tdzd1x3 = Parameter(name = 'tdzd1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd1x3 + Rtdzd1x3',
                    texname = '\\text{tdzd1x3}')

tdzd2x1 = Parameter(name = 'tdzd2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd2x1 + Rtdzd2x1',
                    texname = '\\text{tdzd2x1}')

tdzd2x2 = Parameter(name = 'tdzd2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd2x2 + Rtdzd2x2',
                    texname = '\\text{tdzd2x2}')

tdzd2x3 = Parameter(name = 'tdzd2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd2x3 + Rtdzd2x3',
                    texname = '\\text{tdzd2x3}')

tdzd3x1 = Parameter(name = 'tdzd3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd3x1 + Rtdzd3x1',
                    texname = '\\text{tdzd3x1}')

tdzd3x2 = Parameter(name = 'tdzd3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd3x2 + Rtdzd3x2',
                    texname = '\\text{tdzd3x2}')

tdzd3x3 = Parameter(name = 'tdzd3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzd3x3 + Rtdzd3x3',
                    texname = '\\text{tdzd3x3}')

tdze1x1 = Parameter(name = 'tdze1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze1x1 + Rtdze1x1',
                    texname = '\\text{tdze1x1}')

tdze1x2 = Parameter(name = 'tdze1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze1x2 + Rtdze1x2',
                    texname = '\\text{tdze1x2}')

tdze1x3 = Parameter(name = 'tdze1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze1x3 + Rtdze1x3',
                    texname = '\\text{tdze1x3}')

tdze2x1 = Parameter(name = 'tdze2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze2x1 + Rtdze2x1',
                    texname = '\\text{tdze2x1}')

tdze2x2 = Parameter(name = 'tdze2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze2x2 + Rtdze2x2',
                    texname = '\\text{tdze2x2}')

tdze2x3 = Parameter(name = 'tdze2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze2x3 + Rtdze2x3',
                    texname = '\\text{tdze2x3}')

tdze3x1 = Parameter(name = 'tdze3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze3x1 + Rtdze3x1',
                    texname = '\\text{tdze3x1}')

tdze3x2 = Parameter(name = 'tdze3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze3x2 + Rtdze3x2',
                    texname = '\\text{tdze3x2}')

tdze3x3 = Parameter(name = 'tdze3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdze3x3 + Rtdze3x3',
                    texname = '\\text{tdze3x3}')

tdzu1x1 = Parameter(name = 'tdzu1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu1x1 + Rtdzu1x1',
                    texname = '\\text{tdzu1x1}')

tdzu1x2 = Parameter(name = 'tdzu1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu1x2 + Rtdzu1x2',
                    texname = '\\text{tdzu1x2}')

tdzu1x3 = Parameter(name = 'tdzu1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu1x3 + Rtdzu1x3',
                    texname = '\\text{tdzu1x3}')

tdzu2x1 = Parameter(name = 'tdzu2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu2x1 + Rtdzu2x1',
                    texname = '\\text{tdzu2x1}')

tdzu2x2 = Parameter(name = 'tdzu2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu2x2 + Rtdzu2x2',
                    texname = '\\text{tdzu2x2}')

tdzu2x3 = Parameter(name = 'tdzu2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu2x3 + Rtdzu2x3',
                    texname = '\\text{tdzu2x3}')

tdzu3x1 = Parameter(name = 'tdzu3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu3x1 + Rtdzu3x1',
                    texname = '\\text{tdzu3x1}')

tdzu3x2 = Parameter(name = 'tdzu3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu3x2 + Rtdzu3x2',
                    texname = '\\text{tdzu3x2}')

tdzu3x3 = Parameter(name = 'tdzu3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*Itdzu3x3 + Rtdzu3x3',
                    texname = '\\text{tdzu3x3}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

gHza = Parameter(name = 'gHza',
                 nature = 'internal',
                 type = 'real',
                 value = '-((-13 + 94*cw**2)*cmath.sqrt(ee**2*Gf*MZ**2))/(36.*2**0.75*ee**2*cmath.pi**2)',
                 texname = 'g_{\\text{Hza}}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

