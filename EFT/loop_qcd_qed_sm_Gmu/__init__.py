import object_library 
import particles
import couplings
import CT_couplings
import lorentz
import parameters
import CT_parameters
import vertices
import CT_vertices
import write_param_card
import coupling_orders
import function_library

gauge = [1]

all_particles = particles.all_particles
all_vertices = vertices.all_vertices
all_CTvertices = CT_vertices.all_CTvertices
all_couplings = couplings.all_couplings
all_lorentz = lorentz.all_lorentz
all_parameters = parameters.all_parameters
all_CTparameters = CT_parameters.all_CTparameters
all_functions = function_library.all_functions
all_orders = coupling_orders.all_orders

__author__ = "Huasheng Shao"
__version__ = "1.0"
__email__ = "huasheng.shao@cern.ch"

# Checking that MadGraph version >= 2.6.2

# Initializing value so that failing to find the version will not alter behaviour
mg_version = "2.6.2"

import os
f = open(os.environ["MADPATH"]+"/VERSION")
for l in f:
  if l.find("version =") == 0:
    mg_version = l.replace("\n","").split(" = ")[1]
    break

from distutils.version import StrictVersion
if StrictVersion(mg_version) < StrictVersion('2.6.2'):
  import logging
  error_msg = "Detected version " + mg_version + ". Importing this model from PYTHONPATH won't work with MG5_aMCNLO < 2.6.2"
  logging.getLogger('fatalerror').error(error_msg)
