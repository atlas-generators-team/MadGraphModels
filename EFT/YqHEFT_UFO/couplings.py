# This file was automatically created by FeynRules 2.3.48
# Mathematica version: 12.3.1 for Mac OS X x86 (64-bit) (July 7, 2021)
# Date: Wed 24 Nov 2021 17:07:08


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-0.5*ee**2/cw',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '(ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = 'ee**2/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = 'I1a11',
                 order = {'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'I1a22',
                 order = {'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'I1a33',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = 'I2a11/2.',
                 order = {'1':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'I2a22/2.',
                 order = {'1':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'I2a33/2.',
                 order = {'1':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-I3a11',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-I3a22',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-I3a33',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-0.5*I4a11',
                 order = {'1':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-0.5*I4a22',
                 order = {'1':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-0.5*I4a33',
                 order = {'1':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'I5a11',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'I5a22',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'I5a33',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'I6a11/2.',
                 order = {'1':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'I6a22/2.',
                 order = {'1':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'I6a33/2.',
                 order = {'1':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '-I7a11',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-I7a22',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '-I7a33',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-0.5*I8a11',
                 order = {'1':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-0.5*I8a22',
                 order = {'1':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-0.5*I8a33',
                 order = {'1':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-2*complex(0,1)*lam',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '-4*complex(0,1)*lam',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '-0.5*ee/sw',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-0.5*(ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-0.5*ee**2/sw',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '-0.5*(ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = 'ee**2/(2.*sw)',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-0.5*(cw*ee)/sw - (ee*sw)/(2.*cw)',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-0.5*(cw*ee*complex(0,1))/sw + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(cw*ee**2*complex(0,1))/sw - (ee**2*complex(0,1)*sw)/cw',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = 'I2a11/vev**2',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = '(2*I2a11)/vev**2',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = 'I2a22/vev**2',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '(2*I2a22)/vev**2',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = 'I2a33/vev**2',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '(2*I2a33)/vev**2',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '(-2*I4a11)/vev**2',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(I4a11/vev**2)',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '(-2*I4a22)/vev**2',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(I4a22/vev**2)',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '(-2*I4a33)/vev**2',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(I4a33/vev**2)',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = 'I6a11/vev**2',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '(2*I6a11)/vev**2',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = 'I6a22/vev**2',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(2*I6a22)/vev**2',
                 order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = 'I6a33/vev**2',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '(2*I6a33)/vev**2',
                 order = {'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '(-2*I8a11)/vev**2',
                 order = {'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(I8a11/vev**2)',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '(-2*I8a22)/vev**2',
                 order = {'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = '-(I8a22/vev**2)',
                 order = {'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = '(-2*I8a33)/vev**2',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '-(I8a33/vev**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = 'I2a11/vev',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = 'I2a22/vev',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = 'I2a33/vev',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-(I4a11/vev)',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(I4a22/vev)',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-(I4a33/vev)',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = 'I6a11/vev',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = 'I6a22/vev',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = 'I6a33/vev',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(I8a11/vev)',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-(I8a22/vev)',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-(I8a33/vev)',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '-0.5*(ee**2*vev)/cw',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '-2*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-0.25*(ee**2*vev)/sw**2',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-0.25*(ee**2*complex(0,1)*vev)/sw**2',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-0.5*(ee**2*vev)/sw',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '-0.25*(ee**2*vev)/cw - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '-0.25*(ee**2*vev)/cw + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-0.5*(ee**2*complex(0,1)*vev) - (cw**2*ee**2*complex(0,1)*vev)/(4.*sw**2) - (ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(yb/cmath.sqrt(2))',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(yc/cmath.sqrt(2))',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '-((complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = 'yc/cmath.sqrt(2)',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(ydo/cmath.sqrt(2))',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '-((complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-ye',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = 'ye',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '-(ye/cmath.sqrt(2))',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '-((complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = 'ye/cmath.sqrt(2)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '-ym',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = 'ym',
                  order = {'QED':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '-(ym/cmath.sqrt(2))',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-((complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = 'ym/cmath.sqrt(2)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-(ys/cmath.sqrt(2))',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-((complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(yt/cmath.sqrt(2))',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-ytau',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = 'ytau',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = 'ytau/cmath.sqrt(2)',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-0.5*YukBottom/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_144 = Coupling(name = 'GC_144',
                  value = '(-3*complex(0,1)*YukBottom)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_145 = Coupling(name = 'GC_145',
                  value = '(-3*YukBottom)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(YukBottom/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '-((complex(0,1)*YukBottom)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '(-3*complex(0,1)*YukBottom)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_149 = Coupling(name = 'GC_149',
                  value = 'YukBottom/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(3*YukBottom)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-(YukBottom/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-((complex(0,1)*YukBottom)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(-3*complex(0,1)*YukBottom)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_154 = Coupling(name = 'GC_154',
                  value = 'YukBottom/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-0.5*YukCharm/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_156 = Coupling(name = 'GC_156',
                  value = '(-3*complex(0,1)*YukCharm)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_157 = Coupling(name = 'GC_157',
                  value = 'YukCharm/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_158 = Coupling(name = 'GC_158',
                  value = '(-3*YukCharm)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '-(YukCharm/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '-((complex(0,1)*YukCharm)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '(-3*complex(0,1)*YukCharm)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_162 = Coupling(name = 'GC_162',
                  value = 'YukCharm/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(3*YukCharm)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-((complex(0,1)*YukCharm)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_165 = Coupling(name = 'GC_165',
                  value = '(-3*complex(0,1)*YukCharm)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_166 = Coupling(name = 'GC_166',
                  value = 'YukCharm/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_167 = Coupling(name = 'GC_167',
                  value = '-0.5*YukDown/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_168 = Coupling(name = 'GC_168',
                  value = '(-3*complex(0,1)*YukDown)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_169 = Coupling(name = 'GC_169',
                  value = '(-3*YukDown)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(YukDown/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '-((complex(0,1)*YukDown)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '(-3*complex(0,1)*YukDown)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_173 = Coupling(name = 'GC_173',
                  value = 'YukDown/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '(3*YukDown)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-(YukDown/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-((complex(0,1)*YukDown)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(-3*complex(0,1)*YukDown)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_178 = Coupling(name = 'GC_178',
                  value = 'YukDown/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-0.5*YukStrange/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_180 = Coupling(name = 'GC_180',
                  value = '(-3*complex(0,1)*YukStrange)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_181 = Coupling(name = 'GC_181',
                  value = '(-3*YukStrange)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '-(YukStrange/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((complex(0,1)*YukStrange)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(-3*complex(0,1)*YukStrange)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_185 = Coupling(name = 'GC_185',
                  value = 'YukStrange/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(3*YukStrange)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-(YukStrange/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-((complex(0,1)*YukStrange)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_189 = Coupling(name = 'GC_189',
                  value = '(-3*complex(0,1)*YukStrange)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_190 = Coupling(name = 'GC_190',
                  value = 'YukStrange/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-0.5*YukTop/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_192 = Coupling(name = 'GC_192',
                  value = '(-3*complex(0,1)*YukTop)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_193 = Coupling(name = 'GC_193',
                  value = 'YukTop/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_194 = Coupling(name = 'GC_194',
                  value = '(-3*YukTop)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(YukTop/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-((complex(0,1)*YukTop)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '(-3*complex(0,1)*YukTop)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_198 = Coupling(name = 'GC_198',
                  value = 'YukTop/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(3*YukTop)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '-((complex(0,1)*YukTop)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_201 = Coupling(name = 'GC_201',
                  value = '(-3*complex(0,1)*YukTop)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_202 = Coupling(name = 'GC_202',
                  value = 'YukTop/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_203 = Coupling(name = 'GC_203',
                  value = '-0.5*YukUp/cmath.sqrt(2)',
                  order = {'NP':1,'QED':-2})

GC_204 = Coupling(name = 'GC_204',
                  value = '(-3*complex(0,1)*YukUp)/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_205 = Coupling(name = 'GC_205',
                  value = 'YukUp/(2.*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-2})

GC_206 = Coupling(name = 'GC_206',
                  value = '(-3*YukUp)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(YukUp/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '-((complex(0,1)*YukUp)/(vev**2*cmath.sqrt(2)))',
                  order = {'NP':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '(-3*complex(0,1)*YukUp)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_210 = Coupling(name = 'GC_210',
                  value = 'YukUp/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '(3*YukUp)/(vev**2*cmath.sqrt(2))',
                  order = {'NP':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '-((complex(0,1)*YukUp)/(vev*cmath.sqrt(2)))',
                  order = {'NP':1,'QED':-1})

GC_213 = Coupling(name = 'GC_213',
                  value = '(-3*complex(0,1)*YukUp)/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_214 = Coupling(name = 'GC_214',
                  value = 'YukUp/(vev*cmath.sqrt(2))',
                  order = {'NP':1,'QED':-1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-(yup/cmath.sqrt(2))',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-((complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = 'yup/cmath.sqrt(2)',
                  order = {'QED':1})

