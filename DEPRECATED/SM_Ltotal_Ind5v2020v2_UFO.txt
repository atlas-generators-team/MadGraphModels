Requestor: Evgeny Soldatov
Content: Anomalous quartic electroweak gauge-boson interactions with all operators
Paper: https://arxiv.org/abs/1604.03555
Webpage: http://feynrules.irmp.ucl.ac.be/wiki/AnomalousGaugeCoupling#no1
Source: http://feynrules.irmp.ucl.ac.be/attachment/wiki/AnomalousGaugeCoupling/April2020.tgz
JIRA: https://its.cern.ch/jira/browse/AGENE-1863
Description: DEPRECATED! Superseded by QAll_5_Aug21v2 which adds T3 and T4. Fixes a bug in LM5 w.r.t. SM_LSMT_Ind5_UFOv2.
