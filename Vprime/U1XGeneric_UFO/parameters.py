# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (November 20, 2012)
# Date: Fri 14 Jan 2022 00:06:59



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
xH = Parameter(name = 'xH',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{xH}',
               lhablock = 'BLINPUTS',
               lhacode = [ 1 ])

xPhi = Parameter(name = 'xPhi',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{xPhi}',
                 lhablock = 'BLINPUTS',
                 lhacode = [ 2 ])

g1p = Parameter(name = 'g1p',
                nature = 'external',
                type = 'real',
                value = 0.2,
                texname = '\\text{g1p}',
                lhablock = 'BLINPUTS',
                lhacode = [ 3 ])

Sa = Parameter(name = 'Sa',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{Sa}',
               lhablock = 'BLINPUTS',
               lhacode = [ 4 ])

cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

VeN1 = Parameter(name = 'VeN1',
                 nature = 'external',
                 type = 'real',
                 value = 0.001,
                 texname = 'V_{\\text{eN1}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 1 ])

VeN2 = Parameter(name = 'VeN2',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = 'V_{\\text{eN2}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 2 ])

VeN3 = Parameter(name = 'VeN3',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = 'V_{\\text{eN3}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 3 ])

VmuN1 = Parameter(name = 'VmuN1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{muN1}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 4 ])

VmuN2 = Parameter(name = 'VmuN2',
                  nature = 'external',
                  type = 'real',
                  value = 0.001,
                  texname = 'V_{\\text{muN2}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 5 ])

VmuN3 = Parameter(name = 'VmuN3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{muN3}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 6 ])

VtaN1 = Parameter(name = 'VtaN1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{taN1}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 7 ])

VtaN2 = Parameter(name = 'VtaN2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{taN2}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 8 ])

VtaN3 = Parameter(name = 'VtaN3',
                  nature = 'external',
                  type = 'real',
                  value = 0.001,
                  texname = 'V_{\\text{taN3}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 9 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

VV1x1 = Parameter(name = 'VV1x1',
                  nature = 'external',
                  type = 'complex',
                  value = 1,
                  texname = '\\text{VV1x1}',
                  lhablock = 'VV',
                  lhacode = [ 1, 1 ])

VV1x2 = Parameter(name = 'VV1x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV1x2}',
                  lhablock = 'VV',
                  lhacode = [ 1, 2 ])

VV1x3 = Parameter(name = 'VV1x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV1x3}',
                  lhablock = 'VV',
                  lhacode = [ 1, 3 ])

VV2x1 = Parameter(name = 'VV2x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV2x1}',
                  lhablock = 'VV',
                  lhacode = [ 2, 1 ])

VV2x2 = Parameter(name = 'VV2x2',
                  nature = 'external',
                  type = 'complex',
                  value = 1,
                  texname = '\\text{VV2x2}',
                  lhablock = 'VV',
                  lhacode = [ 2, 2 ])

VV2x3 = Parameter(name = 'VV2x3',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV2x3}',
                  lhablock = 'VV',
                  lhacode = [ 2, 3 ])

VV3x1 = Parameter(name = 'VV3x1',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV3x1}',
                  lhablock = 'VV',
                  lhacode = [ 3, 1 ])

VV3x2 = Parameter(name = 'VV3x2',
                  nature = 'external',
                  type = 'complex',
                  value = 0,
                  texname = '\\text{VV3x2}',
                  lhablock = 'VV',
                  lhacode = [ 3, 2 ])

VV3x3 = Parameter(name = 'VV3x3',
                  nature = 'external',
                  type = 'complex',
                  value = 1,
                  texname = '\\text{VV3x3}',
                  lhablock = 'VV',
                  lhacode = [ 3, 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.379,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MZp = Parameter(name = 'MZp',
                nature = 'external',
                type = 'real',
                value = 3000,
                texname = '\\text{MZp}',
                lhablock = 'MASS',
                lhacode = [ 9900032 ])

MN1 = Parameter(name = 'MN1',
                nature = 'external',
                type = 'real',
                value = 200.,
                texname = '\\text{MN1}',
                lhablock = 'MASS',
                lhacode = [ 9910012 ])

MN2 = Parameter(name = 'MN2',
                nature = 'external',
                type = 'real',
                value = 400.,
                texname = '\\text{MN2}',
                lhablock = 'MASS',
                lhacode = [ 9910014 ])

MN3 = Parameter(name = 'MN3',
                nature = 'external',
                type = 'real',
                value = 600.,
                texname = '\\text{MN3}',
                lhablock = 'MASS',
                lhacode = [ 9910016 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH1 = Parameter(name = 'MH1',
                nature = 'external',
                type = 'real',
                value = 125.18,
                texname = '\\text{MH1}',
                lhablock = 'MASS',
                lhacode = [ 25 ])

MH2 = Parameter(name = 'MH2',
                nature = 'external',
                type = 'real',
                value = 450,
                texname = '\\text{MH2}',
                lhablock = 'MASS',
                lhacode = [ 9900035 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WZp = Parameter(name = 'WZp',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WZp}',
                lhablock = 'DECAY',
                lhacode = [ 9900032 ])

WN1 = Parameter(name = 'WN1',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WN1}',
                lhablock = 'DECAY',
                lhacode = [ 9910012 ])

WN2 = Parameter(name = 'WN2',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WN2}',
                lhablock = 'DECAY',
                lhacode = [ 9910014 ])

WN3 = Parameter(name = 'WN3',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WN3}',
                lhablock = 'DECAY',
                lhacode = [ 9910016 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH1 = Parameter(name = 'WH1',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{WH1}',
                lhablock = 'DECAY',
                lhacode = [ 25 ])

WH2 = Parameter(name = 'WH2',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WH2}',
                lhablock = 'DECAY',
                lhacode = [ 9900035 ])

g1pp = Parameter(name = 'g1pp',
                 nature = 'internal',
                 type = 'real',
                 value = 'g1p*xPhi',
                 texname = '\\text{g1pp}')

gt = Parameter(name = 'gt',
               nature = 'internal',
               type = 'real',
               value = 'g1p*xH',
               texname = '\\text{gt}')

Ca = Parameter(name = 'Ca',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - Sa**2)',
               texname = '\\text{Ca}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '0.231',
                texname = '\\text{sw2}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

Sm1x1 = Parameter(name = 'Sm1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'VeN1',
                  texname = '\\text{Sm1x1}')

Sm1x2 = Parameter(name = 'Sm1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'VeN2',
                  texname = '\\text{Sm1x2}')

Sm1x3 = Parameter(name = 'Sm1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'VeN3',
                  texname = '\\text{Sm1x3}')

Sm2x1 = Parameter(name = 'Sm2x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'VmuN1',
                  texname = '\\text{Sm2x1}')

Sm2x2 = Parameter(name = 'Sm2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'VmuN2',
                  texname = '\\text{Sm2x2}')

Sm2x3 = Parameter(name = 'Sm2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'VmuN3',
                  texname = '\\text{Sm2x3}')

Sm3x1 = Parameter(name = 'Sm3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'VtaN1',
                  texname = '\\text{Sm3x1}')

Sm3x2 = Parameter(name = 'Sm3x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'VtaN2',
                  texname = '\\text{Sm3x2}')

Sm3x3 = Parameter(name = 'Sm3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'VtaN3',
                  texname = '\\text{Sm3x3}')

U11 = Parameter(name = 'U11',
                nature = 'internal',
                type = 'complex',
                value = '1',
                texname = '\\text{U11}')

U12 = Parameter(name = 'U12',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U12}')

U13 = Parameter(name = 'U13',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U13}')

U21 = Parameter(name = 'U21',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U21}')

U22 = Parameter(name = 'U22',
                nature = 'internal',
                type = 'complex',
                value = '1',
                texname = '\\text{U22}')

U23 = Parameter(name = 'U23',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U23}')

U31 = Parameter(name = 'U31',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U31}')

U32 = Parameter(name = 'U32',
                nature = 'internal',
                type = 'complex',
                value = '0',
                texname = '\\text{U32}')

U33 = Parameter(name = 'U33',
                nature = 'internal',
                type = 'complex',
                value = '1',
                texname = '\\text{U33}')

Tm1x1 = Parameter(name = 'Tm1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-VeN1',
                  texname = '\\text{Tm1x1}')

Tm1x2 = Parameter(name = 'Tm1x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-VmuN1',
                  texname = '\\text{Tm1x2}')

Tm1x3 = Parameter(name = 'Tm1x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-VtaN1',
                  texname = '\\text{Tm1x3}')

Tm2x1 = Parameter(name = 'Tm2x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-VeN2',
                  texname = '\\text{Tm2x1}')

Tm2x2 = Parameter(name = 'Tm2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-VmuN2',
                  texname = '\\text{Tm2x2}')

Tm2x3 = Parameter(name = 'Tm2x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-VtaN2',
                  texname = '\\text{Tm2x3}')

Tm3x1 = Parameter(name = 'Tm3x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-VeN3',
                  texname = '\\text{Tm3x1}')

Tm3x2 = Parameter(name = 'Tm3x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-VmuN3',
                  texname = '\\text{Tm3x2}')

Tm3x3 = Parameter(name = 'Tm3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-VtaN3',
                  texname = '\\text{Tm3x3}')

ynd1x2 = Parameter(name = 'ynd1x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd1x2}')

ynd1x3 = Parameter(name = 'ynd1x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd1x3}')

ynd2x1 = Parameter(name = 'ynd2x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd2x1}')

ynd2x3 = Parameter(name = 'ynd2x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd2x3}')

ynd3x1 = Parameter(name = 'ynd3x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd3x1}')

ynd3x2 = Parameter(name = 'ynd3x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{ynd3x2}')

ynm1x2 = Parameter(name = 'ynm1x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm1x2}')

ynm1x3 = Parameter(name = 'ynm1x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm1x3}')

ynm2x1 = Parameter(name = 'ynm2x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm2x1}')

ynm2x3 = Parameter(name = 'ynm2x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm2x3}')

ynm3x1 = Parameter(name = 'ynm3x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm3x1}')

ynm3x2 = Parameter(name = 'ynm3x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0.',
                   texname = '\\text{ynm3x2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

Sp2num = Parameter(name = 'Sp2num',
                   nature = 'internal',
                   type = 'real',
                   value = '2*gt*cmath.sqrt(ee**2/cw**2 + ee**2/sw**2)',
                   texname = '\\text{Sp2num}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam1 = Parameter(name = 'lam1',
                 nature = 'internal',
                 type = 'real',
                 value = '(Ca**2*MH1**2)/(2.*vev**2) + (MH2**2*Sa**2)/(2.*vev**2)',
                 texname = '\\text{lam1}')

x = Parameter(name = 'x',
              nature = 'internal',
              type = 'real',
              value = '(MZp*cmath.sqrt(1 - (gt**2*vev**2)/(4*MZp**2 - (g1**2 + gw**2)*vev**2)))/(2.*g1pp)',
              texname = 'x')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ynd1x1 = Parameter(name = 'ynd1x1',
                   nature = 'internal',
                   type = 'real',
                   value = '(MN1*VeN1*cmath.sqrt(2))/vev',
                   texname = '\\text{ynd1x1}')

ynd2x2 = Parameter(name = 'ynd2x2',
                   nature = 'internal',
                   type = 'real',
                   value = '(MN2*VmuN2*cmath.sqrt(2))/vev',
                   texname = '\\text{ynd2x2}')

ynd3x3 = Parameter(name = 'ynd3x3',
                   nature = 'internal',
                   type = 'real',
                   value = '(MN3*VtaN3*cmath.sqrt(2))/vev',
                   texname = '\\text{ynd3x3}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

C2gNum = Parameter(name = 'C2gNum',
                   nature = 'internal',
                   type = 'real',
                   value = 'ee**2/cw**2 + gt**2 + ee**2/sw**2 - (16*g1pp**2*x**2)/vev**2',
                   texname = '\\text{C2gNum}')

Cn = Parameter(name = 'Cn',
               nature = 'internal',
               type = 'real',
               value = 'ee**2/cw**2 + gt**2 + ee**2/sw**2 + (16*g1pp**2*x**2)/vev**2',
               texname = '\\text{Cn}')

Cp2num = Parameter(name = 'Cp2num',
                   nature = 'internal',
                   type = 'real',
                   value = '-(ee**2/cw**2) + gt**2 - ee**2/sw**2 + (16*g1pp**2*x**2)/vev**2',
                   texname = '\\text{Cp2num}')

Dn = Parameter(name = 'Dn',
               nature = 'internal',
               type = 'real',
               value = '64*g1pp**2*(ee**2/cw**2 + ee**2/sw**2)*vev**2*x**2',
               texname = '\\text{Dn}')

S2gNum = Parameter(name = 'S2gNum',
                   nature = 'internal',
                   type = 'real',
                   value = '(8*g1pp*gt*x)/vev',
                   texname = '\\text{S2gNum}')

lam2 = Parameter(name = 'lam2',
                 nature = 'internal',
                 type = 'real',
                 value = '(Ca**2*MH2**2)/(2.*x**2) + (MH1**2*Sa**2)/(2.*x**2)',
                 texname = '\\text{lam2}')

lam3 = Parameter(name = 'lam3',
                 nature = 'internal',
                 type = 'real',
                 value = '(Ca*(-MH1**2 + MH2**2)*Sa)/(vev*x)',
                 texname = '\\text{lam3}')

ynm1x1 = Parameter(name = 'ynm1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN1/(x*cmath.sqrt(2))',
                   texname = '\\text{ynm1x1}')

ynm2x2 = Parameter(name = 'ynm2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN2/(x*cmath.sqrt(2))',
                   texname = '\\text{ynm2x2}')

ynm3x3 = Parameter(name = 'ynm3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN3/(x*cmath.sqrt(2))',
                   texname = '\\text{ynm3x3}')

mu2H1 = Parameter(name = 'mu2H1',
                  nature = 'internal',
                  type = 'real',
                  value = 'lam1*vev**2 + (lam3*x**2)/2.',
                  texname = 'm^2')

mu2H2 = Parameter(name = 'mu2H2',
                  nature = 'internal',
                  type = 'real',
                  value = '(lam3*vev**2)/2. + lam2*x**2',
                  texname = '\\mu ^2')

sg = Parameter(name = 'sg',
               nature = 'internal',
               type = 'real',
               value = '-cmath.sin(cmath.asin(S2gNum/cmath.sqrt(C2gNum**2 + S2gNum**2))/2.)',
               texname = '\\text{sg}')

Sp = Parameter(name = 'Sp',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(cmath.asin(Sp2num/cmath.sqrt(Cp2num**2 + Sp2num**2))/2.)',
               texname = '\\text{Sp}')

cg = Parameter(name = 'cg',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sg**2)',
               texname = '\\text{cg}')

Cp = Parameter(name = 'Cp',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - Sp**2)',
               texname = '\\text{Cp}')

I1a11 = Parameter(name = 'I1a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1**2 + Sm2x1**2 + Sm3x1**2',
                  texname = '\\text{I1a11}')

I1a12 = Parameter(name = 'I1a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x2 + Sm2x1*Sm2x2 + Sm3x1*Sm3x2',
                  texname = '\\text{I1a12}')

I1a13 = Parameter(name = 'I1a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x3 + Sm2x1*Sm2x3 + Sm3x1*Sm3x3',
                  texname = '\\text{I1a13}')

I1a21 = Parameter(name = 'I1a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x2 + Sm2x1*Sm2x2 + Sm3x1*Sm3x2',
                  texname = '\\text{I1a21}')

I1a22 = Parameter(name = 'I1a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2**2 + Sm2x2**2 + Sm3x2**2',
                  texname = '\\text{I1a22}')

I1a23 = Parameter(name = 'I1a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*Sm1x3 + Sm2x2*Sm2x3 + Sm3x2*Sm3x3',
                  texname = '\\text{I1a23}')

I1a31 = Parameter(name = 'I1a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x3 + Sm2x1*Sm2x3 + Sm3x1*Sm3x3',
                  texname = '\\text{I1a31}')

I1a32 = Parameter(name = 'I1a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*Sm1x3 + Sm2x2*Sm2x3 + Sm3x2*Sm3x3',
                  texname = '\\text{I1a32}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3**2 + Sm2x3**2 + Sm3x3**2',
                  texname = '\\text{I1a33}')

I10a11 = Parameter(name = 'I10a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1**2 + Tm2x1**2 + Tm3x1**2',
                   texname = '\\text{I10a11}')

I10a12 = Parameter(name = 'I10a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2 + Tm2x1*Tm2x2 + Tm3x1*Tm3x2',
                   texname = '\\text{I10a12}')

I10a13 = Parameter(name = 'I10a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3 + Tm2x1*Tm2x3 + Tm3x1*Tm3x3',
                   texname = '\\text{I10a13}')

I10a21 = Parameter(name = 'I10a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2 + Tm2x1*Tm2x2 + Tm3x1*Tm3x2',
                   texname = '\\text{I10a21}')

I10a22 = Parameter(name = 'I10a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2**2 + Tm2x2**2 + Tm3x2**2',
                   texname = '\\text{I10a22}')

I10a23 = Parameter(name = 'I10a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3 + Tm2x2*Tm2x3 + Tm3x2*Tm3x3',
                   texname = '\\text{I10a23}')

I10a31 = Parameter(name = 'I10a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3 + Tm2x1*Tm2x3 + Tm3x1*Tm3x3',
                   texname = '\\text{I10a31}')

I10a32 = Parameter(name = 'I10a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3 + Tm2x2*Tm2x3 + Tm3x2*Tm3x3',
                   texname = '\\text{I10a32}')

I10a33 = Parameter(name = 'I10a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3**2 + Tm2x3**2 + Tm3x3**2',
                   texname = '\\text{I10a33}')

I11a11 = Parameter(name = 'I11a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U11) + U21*complexconjugate(U21) + U31*complexconjugate(U31)',
                   texname = '\\text{I11a11}')

I11a12 = Parameter(name = 'I11a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U12) + U21*complexconjugate(U22) + U31*complexconjugate(U32)',
                   texname = '\\text{I11a12}')

I11a13 = Parameter(name = 'I11a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U13) + U21*complexconjugate(U23) + U31*complexconjugate(U33)',
                   texname = '\\text{I11a13}')

I11a21 = Parameter(name = 'I11a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U11) + U22*complexconjugate(U21) + U32*complexconjugate(U31)',
                   texname = '\\text{I11a21}')

I11a22 = Parameter(name = 'I11a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U12) + U22*complexconjugate(U22) + U32*complexconjugate(U32)',
                   texname = '\\text{I11a22}')

I11a23 = Parameter(name = 'I11a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U13) + U22*complexconjugate(U23) + U32*complexconjugate(U33)',
                   texname = '\\text{I11a23}')

I11a31 = Parameter(name = 'I11a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U11) + U23*complexconjugate(U21) + U33*complexconjugate(U31)',
                   texname = '\\text{I11a31}')

I11a32 = Parameter(name = 'I11a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U12) + U23*complexconjugate(U22) + U33*complexconjugate(U32)',
                   texname = '\\text{I11a32}')

I11a33 = Parameter(name = 'I11a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U13) + U23*complexconjugate(U23) + U33*complexconjugate(U33)',
                   texname = '\\text{I11a33}')

I12a11 = Parameter(name = 'I12a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U11) + U21*complexconjugate(U21) + U31*complexconjugate(U31)',
                   texname = '\\text{I12a11}')

I12a12 = Parameter(name = 'I12a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U11) + U22*complexconjugate(U21) + U32*complexconjugate(U31)',
                   texname = '\\text{I12a12}')

I12a13 = Parameter(name = 'I12a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U11) + U23*complexconjugate(U21) + U33*complexconjugate(U31)',
                   texname = '\\text{I12a13}')

I12a21 = Parameter(name = 'I12a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U12) + U21*complexconjugate(U22) + U31*complexconjugate(U32)',
                   texname = '\\text{I12a21}')

I12a22 = Parameter(name = 'I12a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U12) + U22*complexconjugate(U22) + U32*complexconjugate(U32)',
                   texname = '\\text{I12a22}')

I12a23 = Parameter(name = 'I12a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U12) + U23*complexconjugate(U22) + U33*complexconjugate(U32)',
                   texname = '\\text{I12a23}')

I12a31 = Parameter(name = 'I12a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*complexconjugate(U13) + U21*complexconjugate(U23) + U31*complexconjugate(U33)',
                   texname = '\\text{I12a31}')

I12a32 = Parameter(name = 'I12a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*complexconjugate(U13) + U22*complexconjugate(U23) + U32*complexconjugate(U33)',
                   texname = '\\text{I12a32}')

I12a33 = Parameter(name = 'I12a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*complexconjugate(U13) + U23*complexconjugate(U23) + U33*complexconjugate(U33)',
                   texname = '\\text{I12a33}')

I13a11 = Parameter(name = 'I13a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ydo*complexconjugate(CKM1x1)',
                   texname = '\\text{I13a11}')

I13a12 = Parameter(name = 'I13a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ydo*complexconjugate(CKM2x1)',
                   texname = '\\text{I13a12}')

I13a13 = Parameter(name = 'I13a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ydo*complexconjugate(CKM3x1)',
                   texname = '\\text{I13a13}')

I13a21 = Parameter(name = 'I13a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ys*complexconjugate(CKM1x2)',
                   texname = '\\text{I13a21}')

I13a22 = Parameter(name = 'I13a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ys*complexconjugate(CKM2x2)',
                   texname = '\\text{I13a22}')

I13a23 = Parameter(name = 'I13a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ys*complexconjugate(CKM3x2)',
                   texname = '\\text{I13a23}')

I13a31 = Parameter(name = 'I13a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yb*complexconjugate(CKM1x3)',
                   texname = '\\text{I13a31}')

I13a32 = Parameter(name = 'I13a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yb*complexconjugate(CKM2x3)',
                   texname = '\\text{I13a32}')

I13a33 = Parameter(name = 'I13a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yb*complexconjugate(CKM3x3)',
                   texname = '\\text{I13a33}')

I14a11 = Parameter(name = 'I14a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yup*complexconjugate(CKM1x1)',
                   texname = '\\text{I14a11}')

I14a12 = Parameter(name = 'I14a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yc*complexconjugate(CKM2x1)',
                   texname = '\\text{I14a12}')

I14a13 = Parameter(name = 'I14a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yt*complexconjugate(CKM3x1)',
                   texname = '\\text{I14a13}')

I14a21 = Parameter(name = 'I14a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yup*complexconjugate(CKM1x2)',
                   texname = '\\text{I14a21}')

I14a22 = Parameter(name = 'I14a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yc*complexconjugate(CKM2x2)',
                   texname = '\\text{I14a22}')

I14a23 = Parameter(name = 'I14a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yt*complexconjugate(CKM3x2)',
                   texname = '\\text{I14a23}')

I14a31 = Parameter(name = 'I14a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yup*complexconjugate(CKM1x3)',
                   texname = '\\text{I14a31}')

I14a32 = Parameter(name = 'I14a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yc*complexconjugate(CKM2x3)',
                   texname = '\\text{I14a32}')

I14a33 = Parameter(name = 'I14a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yt*complexconjugate(CKM3x3)',
                   texname = '\\text{I14a33}')

I15a11 = Parameter(name = 'I15a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x1*yup',
                   texname = '\\text{I15a11}')

I15a12 = Parameter(name = 'I15a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x2*yup',
                   texname = '\\text{I15a12}')

I15a13 = Parameter(name = 'I15a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x3*yup',
                   texname = '\\text{I15a13}')

I15a21 = Parameter(name = 'I15a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x1*yc',
                   texname = '\\text{I15a21}')

I15a22 = Parameter(name = 'I15a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x2*yc',
                   texname = '\\text{I15a22}')

I15a23 = Parameter(name = 'I15a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x3*yc',
                   texname = '\\text{I15a23}')

I15a31 = Parameter(name = 'I15a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x1*yt',
                   texname = '\\text{I15a31}')

I15a32 = Parameter(name = 'I15a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x2*yt',
                   texname = '\\text{I15a32}')

I15a33 = Parameter(name = 'I15a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x3*yt',
                   texname = '\\text{I15a33}')

I16a11 = Parameter(name = 'I16a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x1*ydo',
                   texname = '\\text{I16a11}')

I16a12 = Parameter(name = 'I16a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x2*ys',
                   texname = '\\text{I16a12}')

I16a13 = Parameter(name = 'I16a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM1x3*yb',
                   texname = '\\text{I16a13}')

I16a21 = Parameter(name = 'I16a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x1*ydo',
                   texname = '\\text{I16a21}')

I16a22 = Parameter(name = 'I16a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x2*ys',
                   texname = '\\text{I16a22}')

I16a23 = Parameter(name = 'I16a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM2x3*yb',
                   texname = '\\text{I16a23}')

I16a31 = Parameter(name = 'I16a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x1*ydo',
                   texname = '\\text{I16a31}')

I16a32 = Parameter(name = 'I16a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x2*ys',
                   texname = '\\text{I16a32}')

I16a33 = Parameter(name = 'I16a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKM3x3*yb',
                   texname = '\\text{I16a33}')

I17a11 = Parameter(name = 'I17a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ye',
                   texname = '\\text{I17a11}')

I17a12 = Parameter(name = 'I17a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ye',
                   texname = '\\text{I17a12}')

I17a13 = Parameter(name = 'I17a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ye',
                   texname = '\\text{I17a13}')

I17a21 = Parameter(name = 'I17a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x1*ym',
                   texname = '\\text{I17a21}')

I17a22 = Parameter(name = 'I17a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x2*ym',
                   texname = '\\text{I17a22}')

I17a23 = Parameter(name = 'I17a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x3*ym',
                   texname = '\\text{I17a23}')

I17a31 = Parameter(name = 'I17a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x1*ytau',
                   texname = '\\text{I17a31}')

I17a32 = Parameter(name = 'I17a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x2*ytau',
                   texname = '\\text{I17a32}')

I17a33 = Parameter(name = 'I17a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x3*ytau',
                   texname = '\\text{I17a33}')

I18a11 = Parameter(name = 'I18a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(VV1x1) + ynd1x2*complexconjugate(VV2x1) + ynd1x3*complexconjugate(VV3x1)',
                   texname = '\\text{I18a11}')

I18a12 = Parameter(name = 'I18a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(VV1x2) + ynd1x2*complexconjugate(VV2x2) + ynd1x3*complexconjugate(VV3x2)',
                   texname = '\\text{I18a12}')

I18a13 = Parameter(name = 'I18a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(VV1x3) + ynd1x2*complexconjugate(VV2x3) + ynd1x3*complexconjugate(VV3x3)',
                   texname = '\\text{I18a13}')

I18a21 = Parameter(name = 'I18a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd2x1*complexconjugate(VV1x1) + ynd2x2*complexconjugate(VV2x1) + ynd2x3*complexconjugate(VV3x1)',
                   texname = '\\text{I18a21}')

I18a22 = Parameter(name = 'I18a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd2x1*complexconjugate(VV1x2) + ynd2x2*complexconjugate(VV2x2) + ynd2x3*complexconjugate(VV3x2)',
                   texname = '\\text{I18a22}')

I18a23 = Parameter(name = 'I18a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd2x1*complexconjugate(VV1x3) + ynd2x2*complexconjugate(VV2x3) + ynd2x3*complexconjugate(VV3x3)',
                   texname = '\\text{I18a23}')

I18a31 = Parameter(name = 'I18a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd3x1*complexconjugate(VV1x1) + ynd3x2*complexconjugate(VV2x1) + ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I18a31}')

I18a32 = Parameter(name = 'I18a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd3x1*complexconjugate(VV1x2) + ynd3x2*complexconjugate(VV2x2) + ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I18a32}')

I18a33 = Parameter(name = 'I18a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd3x1*complexconjugate(VV1x3) + ynd3x2*complexconjugate(VV2x3) + ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I18a33}')

I19a11 = Parameter(name = 'I19a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*ye',
                   texname = '\\text{I19a11}')

I19a12 = Parameter(name = 'I19a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*ye',
                   texname = '\\text{I19a12}')

I19a13 = Parameter(name = 'I19a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*ye',
                   texname = '\\text{I19a13}')

I19a21 = Parameter(name = 'I19a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U21*ym',
                   texname = '\\text{I19a21}')

I19a22 = Parameter(name = 'I19a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U22*ym',
                   texname = '\\text{I19a22}')

I19a23 = Parameter(name = 'I19a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U23*ym',
                   texname = '\\text{I19a23}')

I19a31 = Parameter(name = 'I19a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U31*ytau',
                   texname = '\\text{I19a31}')

I19a32 = Parameter(name = 'I19a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U32*ytau',
                   texname = '\\text{I19a32}')

I19a33 = Parameter(name = 'I19a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U33*ytau',
                   texname = '\\text{I19a33}')

I2a11 = Parameter(name = 'I2a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1**2 + Sm2x1**2 + Sm3x1**2',
                  texname = '\\text{I2a11}')

I2a12 = Parameter(name = 'I2a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x2 + Sm2x1*Sm2x2 + Sm3x1*Sm3x2',
                  texname = '\\text{I2a12}')

I2a13 = Parameter(name = 'I2a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x3 + Sm2x1*Sm2x3 + Sm3x1*Sm3x3',
                  texname = '\\text{I2a13}')

I2a21 = Parameter(name = 'I2a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x2 + Sm2x1*Sm2x2 + Sm3x1*Sm3x2',
                  texname = '\\text{I2a21}')

I2a22 = Parameter(name = 'I2a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2**2 + Sm2x2**2 + Sm3x2**2',
                  texname = '\\text{I2a22}')

I2a23 = Parameter(name = 'I2a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*Sm1x3 + Sm2x2*Sm2x3 + Sm3x2*Sm3x3',
                  texname = '\\text{I2a23}')

I2a31 = Parameter(name = 'I2a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*Sm1x3 + Sm2x1*Sm2x3 + Sm3x1*Sm3x3',
                  texname = '\\text{I2a31}')

I2a32 = Parameter(name = 'I2a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*Sm1x3 + Sm2x2*Sm2x3 + Sm3x2*Sm3x3',
                  texname = '\\text{I2a32}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3**2 + Sm2x3**2 + Sm3x3**2',
                  texname = '\\text{I2a33}')

I20a11 = Parameter(name = 'I20a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1 + Tm2x1*ynd1x2 + Tm3x1*ynd1x3',
                   texname = '\\text{I20a11}')

I20a12 = Parameter(name = 'I20a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1 + Tm2x2*ynd1x2 + Tm3x2*ynd1x3',
                   texname = '\\text{I20a12}')

I20a13 = Parameter(name = 'I20a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1 + Tm2x3*ynd1x2 + Tm3x3*ynd1x3',
                   texname = '\\text{I20a13}')

I20a21 = Parameter(name = 'I20a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd2x1 + Tm2x1*ynd2x2 + Tm3x1*ynd2x3',
                   texname = '\\text{I20a21}')

I20a22 = Parameter(name = 'I20a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd2x1 + Tm2x2*ynd2x2 + Tm3x2*ynd2x3',
                   texname = '\\text{I20a22}')

I20a23 = Parameter(name = 'I20a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd2x1 + Tm2x3*ynd2x2 + Tm3x3*ynd2x3',
                   texname = '\\text{I20a23}')

I20a31 = Parameter(name = 'I20a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd3x1 + Tm2x1*ynd3x2 + Tm3x1*ynd3x3',
                   texname = '\\text{I20a31}')

I20a32 = Parameter(name = 'I20a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd3x1 + Tm2x2*ynd3x2 + Tm3x2*ynd3x3',
                   texname = '\\text{I20a32}')

I20a33 = Parameter(name = 'I20a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd3x1 + Tm2x3*ynd3x2 + Tm3x3*ynd3x3',
                   texname = '\\text{I20a33}')

I21a11 = Parameter(name = 'I21a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*ynd1x1 + VV2x1*ynd1x2 + VV3x1*ynd1x3',
                   texname = '\\text{I21a11}')

I21a12 = Parameter(name = 'I21a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*ynd2x1 + VV2x1*ynd2x2 + VV3x1*ynd2x3',
                   texname = '\\text{I21a12}')

I21a13 = Parameter(name = 'I21a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*ynd3x1 + VV2x1*ynd3x2 + VV3x1*ynd3x3',
                   texname = '\\text{I21a13}')

I21a21 = Parameter(name = 'I21a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*ynd1x1 + VV2x2*ynd1x2 + VV3x2*ynd1x3',
                   texname = '\\text{I21a21}')

I21a22 = Parameter(name = 'I21a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*ynd2x1 + VV2x2*ynd2x2 + VV3x2*ynd2x3',
                   texname = '\\text{I21a22}')

I21a23 = Parameter(name = 'I21a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*ynd3x1 + VV2x2*ynd3x2 + VV3x2*ynd3x3',
                   texname = '\\text{I21a23}')

I21a31 = Parameter(name = 'I21a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x3*ynd1x1 + VV2x3*ynd1x2 + VV3x3*ynd1x3',
                   texname = '\\text{I21a31}')

I21a32 = Parameter(name = 'I21a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x3*ynd2x1 + VV2x3*ynd2x2 + VV3x3*ynd2x3',
                   texname = '\\text{I21a32}')

I21a33 = Parameter(name = 'I21a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x3*ynd3x1 + VV2x3*ynd3x2 + VV3x3*ynd3x3',
                   texname = '\\text{I21a33}')

I22a11 = Parameter(name = 'I22a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ye',
                   texname = '\\text{I22a11}')

I22a12 = Parameter(name = 'I22a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x1*ym',
                   texname = '\\text{I22a12}')

I22a13 = Parameter(name = 'I22a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x1*ytau',
                   texname = '\\text{I22a13}')

I22a21 = Parameter(name = 'I22a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ye',
                   texname = '\\text{I22a21}')

I22a22 = Parameter(name = 'I22a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x2*ym',
                   texname = '\\text{I22a22}')

I22a23 = Parameter(name = 'I22a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x2*ytau',
                   texname = '\\text{I22a23}')

I22a31 = Parameter(name = 'I22a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ye',
                   texname = '\\text{I22a31}')

I22a32 = Parameter(name = 'I22a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm2x3*ym',
                   texname = '\\text{I22a32}')

I22a33 = Parameter(name = 'I22a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm3x3*ytau',
                   texname = '\\text{I22a33}')

I23a11 = Parameter(name = 'I23a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1 + Tm2x1*ynd1x2 + Tm3x1*ynd1x3',
                   texname = '\\text{I23a11}')

I23a12 = Parameter(name = 'I23a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd2x1 + Tm2x1*ynd2x2 + Tm3x1*ynd2x3',
                   texname = '\\text{I23a12}')

I23a13 = Parameter(name = 'I23a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd3x1 + Tm2x1*ynd3x2 + Tm3x1*ynd3x3',
                   texname = '\\text{I23a13}')

I23a21 = Parameter(name = 'I23a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1 + Tm2x2*ynd1x2 + Tm3x2*ynd1x3',
                   texname = '\\text{I23a21}')

I23a22 = Parameter(name = 'I23a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd2x1 + Tm2x2*ynd2x2 + Tm3x2*ynd2x3',
                   texname = '\\text{I23a22}')

I23a23 = Parameter(name = 'I23a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd3x1 + Tm2x2*ynd3x2 + Tm3x2*ynd3x3',
                   texname = '\\text{I23a23}')

I23a31 = Parameter(name = 'I23a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1 + Tm2x3*ynd1x2 + Tm3x3*ynd1x3',
                   texname = '\\text{I23a31}')

I23a32 = Parameter(name = 'I23a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd2x1 + Tm2x3*ynd2x2 + Tm3x3*ynd2x3',
                   texname = '\\text{I23a32}')

I23a33 = Parameter(name = 'I23a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd3x1 + Tm2x3*ynd3x2 + Tm3x3*ynd3x3',
                   texname = '\\text{I23a33}')

I24a11 = Parameter(name = 'I24a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ye*complexconjugate(U11)',
                   texname = '\\text{I24a11}')

I24a12 = Parameter(name = 'I24a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ym*complexconjugate(U21)',
                   texname = '\\text{I24a12}')

I24a13 = Parameter(name = 'I24a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ytau*complexconjugate(U31)',
                   texname = '\\text{I24a13}')

I24a21 = Parameter(name = 'I24a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ye*complexconjugate(U12)',
                   texname = '\\text{I24a21}')

I24a22 = Parameter(name = 'I24a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ym*complexconjugate(U22)',
                   texname = '\\text{I24a22}')

I24a23 = Parameter(name = 'I24a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ytau*complexconjugate(U32)',
                   texname = '\\text{I24a23}')

I24a31 = Parameter(name = 'I24a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ye*complexconjugate(U13)',
                   texname = '\\text{I24a31}')

I24a32 = Parameter(name = 'I24a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ym*complexconjugate(U23)',
                   texname = '\\text{I24a32}')

I24a33 = Parameter(name = 'I24a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ytau*complexconjugate(U33)',
                   texname = '\\text{I24a33}')

I25a11 = Parameter(name = 'I25a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x1*ynd1x1 + Sm1x1*Tm2x1*ynd1x2 + Sm1x1*Tm3x1*ynd1x3 + Sm2x1*Tm1x1*ynd2x1 + Sm2x1*Tm2x1*ynd2x2 + Sm2x1*Tm3x1*ynd2x3 + Sm3x1*Tm1x1*ynd3x1 + Sm3x1*Tm2x1*ynd3x2 + Sm3x1*Tm3x1*ynd3x3',
                   texname = '\\text{I25a11}')

I25a12 = Parameter(name = 'I25a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x2*ynd1x1 + Sm1x1*Tm2x2*ynd1x2 + Sm1x1*Tm3x2*ynd1x3 + Sm2x1*Tm1x2*ynd2x1 + Sm2x1*Tm2x2*ynd2x2 + Sm2x1*Tm3x2*ynd2x3 + Sm3x1*Tm1x2*ynd3x1 + Sm3x1*Tm2x2*ynd3x2 + Sm3x1*Tm3x2*ynd3x3',
                   texname = '\\text{I25a12}')

I25a13 = Parameter(name = 'I25a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x3*ynd1x1 + Sm1x1*Tm2x3*ynd1x2 + Sm1x1*Tm3x3*ynd1x3 + Sm2x1*Tm1x3*ynd2x1 + Sm2x1*Tm2x3*ynd2x2 + Sm2x1*Tm3x3*ynd2x3 + Sm3x1*Tm1x3*ynd3x1 + Sm3x1*Tm2x3*ynd3x2 + Sm3x1*Tm3x3*ynd3x3',
                   texname = '\\text{I25a13}')

I25a21 = Parameter(name = 'I25a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x1*ynd1x1 + Sm1x2*Tm2x1*ynd1x2 + Sm1x2*Tm3x1*ynd1x3 + Sm2x2*Tm1x1*ynd2x1 + Sm2x2*Tm2x1*ynd2x2 + Sm2x2*Tm3x1*ynd2x3 + Sm3x2*Tm1x1*ynd3x1 + Sm3x2*Tm2x1*ynd3x2 + Sm3x2*Tm3x1*ynd3x3',
                   texname = '\\text{I25a21}')

I25a22 = Parameter(name = 'I25a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x2*ynd1x1 + Sm1x2*Tm2x2*ynd1x2 + Sm1x2*Tm3x2*ynd1x3 + Sm2x2*Tm1x2*ynd2x1 + Sm2x2*Tm2x2*ynd2x2 + Sm2x2*Tm3x2*ynd2x3 + Sm3x2*Tm1x2*ynd3x1 + Sm3x2*Tm2x2*ynd3x2 + Sm3x2*Tm3x2*ynd3x3',
                   texname = '\\text{I25a22}')

I25a23 = Parameter(name = 'I25a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x3*ynd1x1 + Sm1x2*Tm2x3*ynd1x2 + Sm1x2*Tm3x3*ynd1x3 + Sm2x2*Tm1x3*ynd2x1 + Sm2x2*Tm2x3*ynd2x2 + Sm2x2*Tm3x3*ynd2x3 + Sm3x2*Tm1x3*ynd3x1 + Sm3x2*Tm2x3*ynd3x2 + Sm3x2*Tm3x3*ynd3x3',
                   texname = '\\text{I25a23}')

I25a31 = Parameter(name = 'I25a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x1*ynd1x1 + Sm1x3*Tm2x1*ynd1x2 + Sm1x3*Tm3x1*ynd1x3 + Sm2x3*Tm1x1*ynd2x1 + Sm2x3*Tm2x1*ynd2x2 + Sm2x3*Tm3x1*ynd2x3 + Sm3x3*Tm1x1*ynd3x1 + Sm3x3*Tm2x1*ynd3x2 + Sm3x3*Tm3x1*ynd3x3',
                   texname = '\\text{I25a31}')

I25a32 = Parameter(name = 'I25a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x2*ynd1x1 + Sm1x3*Tm2x2*ynd1x2 + Sm1x3*Tm3x2*ynd1x3 + Sm2x3*Tm1x2*ynd2x1 + Sm2x3*Tm2x2*ynd2x2 + Sm2x3*Tm3x2*ynd2x3 + Sm3x3*Tm1x2*ynd3x1 + Sm3x3*Tm2x2*ynd3x2 + Sm3x3*Tm3x2*ynd3x3',
                   texname = '\\text{I25a32}')

I25a33 = Parameter(name = 'I25a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x3*ynd1x1 + Sm1x3*Tm2x3*ynd1x2 + Sm1x3*Tm3x3*ynd1x3 + Sm2x3*Tm1x3*ynd2x1 + Sm2x3*Tm2x3*ynd2x2 + Sm2x3*Tm3x3*ynd2x3 + Sm3x3*Tm1x3*ynd3x1 + Sm3x3*Tm2x3*ynd3x2 + Sm3x3*Tm3x3*ynd3x3',
                   texname = '\\text{I25a33}')

I26a11 = Parameter(name = 'I26a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*VV1x1*ynd1x1 + U11*VV2x1*ynd1x2 + U11*VV3x1*ynd1x3 + U21*VV1x1*ynd2x1 + U21*VV2x1*ynd2x2 + U21*VV3x1*ynd2x3 + U31*VV1x1*ynd3x1 + U31*VV2x1*ynd3x2 + U31*VV3x1*ynd3x3',
                   texname = '\\text{I26a11}')

I26a12 = Parameter(name = 'I26a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*VV1x1*ynd1x1 + U12*VV2x1*ynd1x2 + U12*VV3x1*ynd1x3 + U22*VV1x1*ynd2x1 + U22*VV2x1*ynd2x2 + U22*VV3x1*ynd2x3 + U32*VV1x1*ynd3x1 + U32*VV2x1*ynd3x2 + U32*VV3x1*ynd3x3',
                   texname = '\\text{I26a12}')

I26a13 = Parameter(name = 'I26a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*VV1x1*ynd1x1 + U13*VV2x1*ynd1x2 + U13*VV3x1*ynd1x3 + U23*VV1x1*ynd2x1 + U23*VV2x1*ynd2x2 + U23*VV3x1*ynd2x3 + U33*VV1x1*ynd3x1 + U33*VV2x1*ynd3x2 + U33*VV3x1*ynd3x3',
                   texname = '\\text{I26a13}')

I26a21 = Parameter(name = 'I26a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*VV1x2*ynd1x1 + U11*VV2x2*ynd1x2 + U11*VV3x2*ynd1x3 + U21*VV1x2*ynd2x1 + U21*VV2x2*ynd2x2 + U21*VV3x2*ynd2x3 + U31*VV1x2*ynd3x1 + U31*VV2x2*ynd3x2 + U31*VV3x2*ynd3x3',
                   texname = '\\text{I26a21}')

I26a22 = Parameter(name = 'I26a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*VV1x2*ynd1x1 + U12*VV2x2*ynd1x2 + U12*VV3x2*ynd1x3 + U22*VV1x2*ynd2x1 + U22*VV2x2*ynd2x2 + U22*VV3x2*ynd2x3 + U32*VV1x2*ynd3x1 + U32*VV2x2*ynd3x2 + U32*VV3x2*ynd3x3',
                   texname = '\\text{I26a22}')

I26a23 = Parameter(name = 'I26a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*VV1x2*ynd1x1 + U13*VV2x2*ynd1x2 + U13*VV3x2*ynd1x3 + U23*VV1x2*ynd2x1 + U23*VV2x2*ynd2x2 + U23*VV3x2*ynd2x3 + U33*VV1x2*ynd3x1 + U33*VV2x2*ynd3x2 + U33*VV3x2*ynd3x3',
                   texname = '\\text{I26a23}')

I26a31 = Parameter(name = 'I26a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U11*VV1x3*ynd1x1 + U11*VV2x3*ynd1x2 + U11*VV3x3*ynd1x3 + U21*VV1x3*ynd2x1 + U21*VV2x3*ynd2x2 + U21*VV3x3*ynd2x3 + U31*VV1x3*ynd3x1 + U31*VV2x3*ynd3x2 + U31*VV3x3*ynd3x3',
                   texname = '\\text{I26a31}')

I26a32 = Parameter(name = 'I26a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U12*VV1x3*ynd1x1 + U12*VV2x3*ynd1x2 + U12*VV3x3*ynd1x3 + U22*VV1x3*ynd2x1 + U22*VV2x3*ynd2x2 + U22*VV3x3*ynd2x3 + U32*VV1x3*ynd3x1 + U32*VV2x3*ynd3x2 + U32*VV3x3*ynd3x3',
                   texname = '\\text{I26a32}')

I26a33 = Parameter(name = 'I26a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'U13*VV1x3*ynd1x1 + U13*VV2x3*ynd1x2 + U13*VV3x3*ynd1x3 + U23*VV1x3*ynd2x1 + U23*VV2x3*ynd2x2 + U23*VV3x3*ynd2x3 + U33*VV1x3*ynd3x1 + U33*VV2x3*ynd3x2 + U33*VV3x3*ynd3x3',
                   texname = '\\text{I26a33}')

I27a11 = Parameter(name = 'I27a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x1*ynd1x1 + Sm1x1*Tm2x1*ynd1x2 + Sm1x1*Tm3x1*ynd1x3 + Sm2x1*Tm1x1*ynd2x1 + Sm2x1*Tm2x1*ynd2x2 + Sm2x1*Tm3x1*ynd2x3 + Sm3x1*Tm1x1*ynd3x1 + Sm3x1*Tm2x1*ynd3x2 + Sm3x1*Tm3x1*ynd3x3',
                   texname = '\\text{I27a11}')

I27a12 = Parameter(name = 'I27a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x2*ynd1x1 + Sm1x1*Tm2x2*ynd1x2 + Sm1x1*Tm3x2*ynd1x3 + Sm2x1*Tm1x2*ynd2x1 + Sm2x1*Tm2x2*ynd2x2 + Sm2x1*Tm3x2*ynd2x3 + Sm3x1*Tm1x2*ynd3x1 + Sm3x1*Tm2x2*ynd3x2 + Sm3x1*Tm3x2*ynd3x3',
                   texname = '\\text{I27a12}')

I27a13 = Parameter(name = 'I27a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*Tm1x3*ynd1x1 + Sm1x1*Tm2x3*ynd1x2 + Sm1x1*Tm3x3*ynd1x3 + Sm2x1*Tm1x3*ynd2x1 + Sm2x1*Tm2x3*ynd2x2 + Sm2x1*Tm3x3*ynd2x3 + Sm3x1*Tm1x3*ynd3x1 + Sm3x1*Tm2x3*ynd3x2 + Sm3x1*Tm3x3*ynd3x3',
                   texname = '\\text{I27a13}')

I27a21 = Parameter(name = 'I27a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x1*ynd1x1 + Sm1x2*Tm2x1*ynd1x2 + Sm1x2*Tm3x1*ynd1x3 + Sm2x2*Tm1x1*ynd2x1 + Sm2x2*Tm2x1*ynd2x2 + Sm2x2*Tm3x1*ynd2x3 + Sm3x2*Tm1x1*ynd3x1 + Sm3x2*Tm2x1*ynd3x2 + Sm3x2*Tm3x1*ynd3x3',
                   texname = '\\text{I27a21}')

I27a22 = Parameter(name = 'I27a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x2*ynd1x1 + Sm1x2*Tm2x2*ynd1x2 + Sm1x2*Tm3x2*ynd1x3 + Sm2x2*Tm1x2*ynd2x1 + Sm2x2*Tm2x2*ynd2x2 + Sm2x2*Tm3x2*ynd2x3 + Sm3x2*Tm1x2*ynd3x1 + Sm3x2*Tm2x2*ynd3x2 + Sm3x2*Tm3x2*ynd3x3',
                   texname = '\\text{I27a22}')

I27a23 = Parameter(name = 'I27a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*Tm1x3*ynd1x1 + Sm1x2*Tm2x3*ynd1x2 + Sm1x2*Tm3x3*ynd1x3 + Sm2x2*Tm1x3*ynd2x1 + Sm2x2*Tm2x3*ynd2x2 + Sm2x2*Tm3x3*ynd2x3 + Sm3x2*Tm1x3*ynd3x1 + Sm3x2*Tm2x3*ynd3x2 + Sm3x2*Tm3x3*ynd3x3',
                   texname = '\\text{I27a23}')

I27a31 = Parameter(name = 'I27a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x1*ynd1x1 + Sm1x3*Tm2x1*ynd1x2 + Sm1x3*Tm3x1*ynd1x3 + Sm2x3*Tm1x1*ynd2x1 + Sm2x3*Tm2x1*ynd2x2 + Sm2x3*Tm3x1*ynd2x3 + Sm3x3*Tm1x1*ynd3x1 + Sm3x3*Tm2x1*ynd3x2 + Sm3x3*Tm3x1*ynd3x3',
                   texname = '\\text{I27a31}')

I27a32 = Parameter(name = 'I27a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x2*ynd1x1 + Sm1x3*Tm2x2*ynd1x2 + Sm1x3*Tm3x2*ynd1x3 + Sm2x3*Tm1x2*ynd2x1 + Sm2x3*Tm2x2*ynd2x2 + Sm2x3*Tm3x2*ynd2x3 + Sm3x3*Tm1x2*ynd3x1 + Sm3x3*Tm2x2*ynd3x2 + Sm3x3*Tm3x2*ynd3x3',
                   texname = '\\text{I27a32}')

I27a33 = Parameter(name = 'I27a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*Tm1x3*ynd1x1 + Sm1x3*Tm2x3*ynd1x2 + Sm1x3*Tm3x3*ynd1x3 + Sm2x3*Tm1x3*ynd2x1 + Sm2x3*Tm2x3*ynd2x2 + Sm2x3*Tm3x3*ynd2x3 + Sm3x3*Tm1x3*ynd3x1 + Sm3x3*Tm2x3*ynd3x2 + Sm3x3*Tm3x3*ynd3x3',
                   texname = '\\text{I27a33}')

I28a11 = Parameter(name = 'I28a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U11)*complexconjugate(VV1x1) + ynd2x1*complexconjugate(U21)*complexconjugate(VV1x1) + ynd3x1*complexconjugate(U31)*complexconjugate(VV1x1) + ynd1x2*complexconjugate(U11)*complexconjugate(VV2x1) + ynd2x2*complexconjugate(U21)*complexconjugate(VV2x1) + ynd3x2*complexconjugate(U31)*complexconjugate(VV2x1) + ynd1x3*complexconjugate(U11)*complexconjugate(VV3x1) + ynd2x3*complexconjugate(U21)*complexconjugate(VV3x1) + ynd3x3*complexconjugate(U31)*complexconjugate(VV3x1)',
                   texname = '\\text{I28a11}')

I28a12 = Parameter(name = 'I28a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U12)*complexconjugate(VV1x1) + ynd2x1*complexconjugate(U22)*complexconjugate(VV1x1) + ynd3x1*complexconjugate(U32)*complexconjugate(VV1x1) + ynd1x2*complexconjugate(U12)*complexconjugate(VV2x1) + ynd2x2*complexconjugate(U22)*complexconjugate(VV2x1) + ynd3x2*complexconjugate(U32)*complexconjugate(VV2x1) + ynd1x3*complexconjugate(U12)*complexconjugate(VV3x1) + ynd2x3*complexconjugate(U22)*complexconjugate(VV3x1) + ynd3x3*complexconjugate(U32)*complexconjugate(VV3x1)',
                   texname = '\\text{I28a12}')

I28a13 = Parameter(name = 'I28a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U13)*complexconjugate(VV1x1) + ynd2x1*complexconjugate(U23)*complexconjugate(VV1x1) + ynd3x1*complexconjugate(U33)*complexconjugate(VV1x1) + ynd1x2*complexconjugate(U13)*complexconjugate(VV2x1) + ynd2x2*complexconjugate(U23)*complexconjugate(VV2x1) + ynd3x2*complexconjugate(U33)*complexconjugate(VV2x1) + ynd1x3*complexconjugate(U13)*complexconjugate(VV3x1) + ynd2x3*complexconjugate(U23)*complexconjugate(VV3x1) + ynd3x3*complexconjugate(U33)*complexconjugate(VV3x1)',
                   texname = '\\text{I28a13}')

I28a21 = Parameter(name = 'I28a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U11)*complexconjugate(VV1x2) + ynd2x1*complexconjugate(U21)*complexconjugate(VV1x2) + ynd3x1*complexconjugate(U31)*complexconjugate(VV1x2) + ynd1x2*complexconjugate(U11)*complexconjugate(VV2x2) + ynd2x2*complexconjugate(U21)*complexconjugate(VV2x2) + ynd3x2*complexconjugate(U31)*complexconjugate(VV2x2) + ynd1x3*complexconjugate(U11)*complexconjugate(VV3x2) + ynd2x3*complexconjugate(U21)*complexconjugate(VV3x2) + ynd3x3*complexconjugate(U31)*complexconjugate(VV3x2)',
                   texname = '\\text{I28a21}')

I28a22 = Parameter(name = 'I28a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U12)*complexconjugate(VV1x2) + ynd2x1*complexconjugate(U22)*complexconjugate(VV1x2) + ynd3x1*complexconjugate(U32)*complexconjugate(VV1x2) + ynd1x2*complexconjugate(U12)*complexconjugate(VV2x2) + ynd2x2*complexconjugate(U22)*complexconjugate(VV2x2) + ynd3x2*complexconjugate(U32)*complexconjugate(VV2x2) + ynd1x3*complexconjugate(U12)*complexconjugate(VV3x2) + ynd2x3*complexconjugate(U22)*complexconjugate(VV3x2) + ynd3x3*complexconjugate(U32)*complexconjugate(VV3x2)',
                   texname = '\\text{I28a22}')

I28a23 = Parameter(name = 'I28a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U13)*complexconjugate(VV1x2) + ynd2x1*complexconjugate(U23)*complexconjugate(VV1x2) + ynd3x1*complexconjugate(U33)*complexconjugate(VV1x2) + ynd1x2*complexconjugate(U13)*complexconjugate(VV2x2) + ynd2x2*complexconjugate(U23)*complexconjugate(VV2x2) + ynd3x2*complexconjugate(U33)*complexconjugate(VV2x2) + ynd1x3*complexconjugate(U13)*complexconjugate(VV3x2) + ynd2x3*complexconjugate(U23)*complexconjugate(VV3x2) + ynd3x3*complexconjugate(U33)*complexconjugate(VV3x2)',
                   texname = '\\text{I28a23}')

I28a31 = Parameter(name = 'I28a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U11)*complexconjugate(VV1x3) + ynd2x1*complexconjugate(U21)*complexconjugate(VV1x3) + ynd3x1*complexconjugate(U31)*complexconjugate(VV1x3) + ynd1x2*complexconjugate(U11)*complexconjugate(VV2x3) + ynd2x2*complexconjugate(U21)*complexconjugate(VV2x3) + ynd3x2*complexconjugate(U31)*complexconjugate(VV2x3) + ynd1x3*complexconjugate(U11)*complexconjugate(VV3x3) + ynd2x3*complexconjugate(U21)*complexconjugate(VV3x3) + ynd3x3*complexconjugate(U31)*complexconjugate(VV3x3)',
                   texname = '\\text{I28a31}')

I28a32 = Parameter(name = 'I28a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U12)*complexconjugate(VV1x3) + ynd2x1*complexconjugate(U22)*complexconjugate(VV1x3) + ynd3x1*complexconjugate(U32)*complexconjugate(VV1x3) + ynd1x2*complexconjugate(U12)*complexconjugate(VV2x3) + ynd2x2*complexconjugate(U22)*complexconjugate(VV2x3) + ynd3x2*complexconjugate(U32)*complexconjugate(VV2x3) + ynd1x3*complexconjugate(U12)*complexconjugate(VV3x3) + ynd2x3*complexconjugate(U22)*complexconjugate(VV3x3) + ynd3x3*complexconjugate(U32)*complexconjugate(VV3x3)',
                   texname = '\\text{I28a32}')

I28a33 = Parameter(name = 'I28a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynd1x1*complexconjugate(U13)*complexconjugate(VV1x3) + ynd2x1*complexconjugate(U23)*complexconjugate(VV1x3) + ynd3x1*complexconjugate(U33)*complexconjugate(VV1x3) + ynd1x2*complexconjugate(U13)*complexconjugate(VV2x3) + ynd2x2*complexconjugate(U23)*complexconjugate(VV2x3) + ynd3x2*complexconjugate(U33)*complexconjugate(VV2x3) + ynd1x3*complexconjugate(U13)*complexconjugate(VV3x3) + ynd2x3*complexconjugate(U23)*complexconjugate(VV3x3) + ynd3x3*complexconjugate(U33)*complexconjugate(VV3x3)',
                   texname = '\\text{I28a33}')

I29a11 = Parameter(name = 'I29a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x1) + Tm2x1*ynm2x1*complexconjugate(VV1x1) + Tm3x1*ynm3x1*complexconjugate(VV1x1) + Tm1x1*ynm1x2*complexconjugate(VV2x1) + Tm2x1*ynm2x2*complexconjugate(VV2x1) + Tm3x1*ynm3x2*complexconjugate(VV2x1) + Tm1x1*ynm1x3*complexconjugate(VV3x1) + Tm2x1*ynm2x3*complexconjugate(VV3x1) + Tm3x1*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I29a11}')

I29a12 = Parameter(name = 'I29a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x1) + Tm2x2*ynm2x1*complexconjugate(VV1x1) + Tm3x2*ynm3x1*complexconjugate(VV1x1) + Tm1x2*ynm1x2*complexconjugate(VV2x1) + Tm2x2*ynm2x2*complexconjugate(VV2x1) + Tm3x2*ynm3x2*complexconjugate(VV2x1) + Tm1x2*ynm1x3*complexconjugate(VV3x1) + Tm2x2*ynm2x3*complexconjugate(VV3x1) + Tm3x2*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I29a12}')

I29a13 = Parameter(name = 'I29a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x1) + Tm2x3*ynm2x1*complexconjugate(VV1x1) + Tm3x3*ynm3x1*complexconjugate(VV1x1) + Tm1x3*ynm1x2*complexconjugate(VV2x1) + Tm2x3*ynm2x2*complexconjugate(VV2x1) + Tm3x3*ynm3x2*complexconjugate(VV2x1) + Tm1x3*ynm1x3*complexconjugate(VV3x1) + Tm2x3*ynm2x3*complexconjugate(VV3x1) + Tm3x3*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I29a13}')

I29a21 = Parameter(name = 'I29a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x2) + Tm2x1*ynm2x1*complexconjugate(VV1x2) + Tm3x1*ynm3x1*complexconjugate(VV1x2) + Tm1x1*ynm1x2*complexconjugate(VV2x2) + Tm2x1*ynm2x2*complexconjugate(VV2x2) + Tm3x1*ynm3x2*complexconjugate(VV2x2) + Tm1x1*ynm1x3*complexconjugate(VV3x2) + Tm2x1*ynm2x3*complexconjugate(VV3x2) + Tm3x1*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I29a21}')

I29a22 = Parameter(name = 'I29a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x2) + Tm2x2*ynm2x1*complexconjugate(VV1x2) + Tm3x2*ynm3x1*complexconjugate(VV1x2) + Tm1x2*ynm1x2*complexconjugate(VV2x2) + Tm2x2*ynm2x2*complexconjugate(VV2x2) + Tm3x2*ynm3x2*complexconjugate(VV2x2) + Tm1x2*ynm1x3*complexconjugate(VV3x2) + Tm2x2*ynm2x3*complexconjugate(VV3x2) + Tm3x2*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I29a22}')

I29a23 = Parameter(name = 'I29a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x2) + Tm2x3*ynm2x1*complexconjugate(VV1x2) + Tm3x3*ynm3x1*complexconjugate(VV1x2) + Tm1x3*ynm1x2*complexconjugate(VV2x2) + Tm2x3*ynm2x2*complexconjugate(VV2x2) + Tm3x3*ynm3x2*complexconjugate(VV2x2) + Tm1x3*ynm1x3*complexconjugate(VV3x2) + Tm2x3*ynm2x3*complexconjugate(VV3x2) + Tm3x3*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I29a23}')

I29a31 = Parameter(name = 'I29a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x3) + Tm2x1*ynm2x1*complexconjugate(VV1x3) + Tm3x1*ynm3x1*complexconjugate(VV1x3) + Tm1x1*ynm1x2*complexconjugate(VV2x3) + Tm2x1*ynm2x2*complexconjugate(VV2x3) + Tm3x1*ynm3x2*complexconjugate(VV2x3) + Tm1x1*ynm1x3*complexconjugate(VV3x3) + Tm2x1*ynm2x3*complexconjugate(VV3x3) + Tm3x1*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I29a31}')

I29a32 = Parameter(name = 'I29a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x3) + Tm2x2*ynm2x1*complexconjugate(VV1x3) + Tm3x2*ynm3x1*complexconjugate(VV1x3) + Tm1x2*ynm1x2*complexconjugate(VV2x3) + Tm2x2*ynm2x2*complexconjugate(VV2x3) + Tm3x2*ynm3x2*complexconjugate(VV2x3) + Tm1x2*ynm1x3*complexconjugate(VV3x3) + Tm2x2*ynm2x3*complexconjugate(VV3x3) + Tm3x2*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I29a32}')

I29a33 = Parameter(name = 'I29a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x3) + Tm2x3*ynm2x1*complexconjugate(VV1x3) + Tm3x3*ynm3x1*complexconjugate(VV1x3) + Tm1x3*ynm1x2*complexconjugate(VV2x3) + Tm2x3*ynm2x2*complexconjugate(VV2x3) + Tm3x3*ynm3x2*complexconjugate(VV2x3) + Tm1x3*ynm1x3*complexconjugate(VV3x3) + Tm2x3*ynm2x3*complexconjugate(VV3x3) + Tm3x3*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I29a33}')

I3a11 = Parameter(name = 'I3a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x1) + VV2x1*complexconjugate(VV2x1) + VV3x1*complexconjugate(VV3x1)',
                  texname = '\\text{I3a11}')

I3a12 = Parameter(name = 'I3a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x2) + VV2x1*complexconjugate(VV2x2) + VV3x1*complexconjugate(VV3x2)',
                  texname = '\\text{I3a12}')

I3a13 = Parameter(name = 'I3a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x3) + VV2x1*complexconjugate(VV2x3) + VV3x1*complexconjugate(VV3x3)',
                  texname = '\\text{I3a13}')

I3a21 = Parameter(name = 'I3a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x1) + VV2x2*complexconjugate(VV2x1) + VV3x2*complexconjugate(VV3x1)',
                  texname = '\\text{I3a21}')

I3a22 = Parameter(name = 'I3a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x2) + VV2x2*complexconjugate(VV2x2) + VV3x2*complexconjugate(VV3x2)',
                  texname = '\\text{I3a22}')

I3a23 = Parameter(name = 'I3a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x3) + VV2x2*complexconjugate(VV2x3) + VV3x2*complexconjugate(VV3x3)',
                  texname = '\\text{I3a23}')

I3a31 = Parameter(name = 'I3a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x1) + VV2x3*complexconjugate(VV2x1) + VV3x3*complexconjugate(VV3x1)',
                  texname = '\\text{I3a31}')

I3a32 = Parameter(name = 'I3a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x2) + VV2x3*complexconjugate(VV2x2) + VV3x3*complexconjugate(VV3x2)',
                  texname = '\\text{I3a32}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x3) + VV2x3*complexconjugate(VV2x3) + VV3x3*complexconjugate(VV3x3)',
                  texname = '\\text{I3a33}')

I30a11 = Parameter(name = 'I30a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x1*ynm1x1 + Tm1x1*VV2x1*ynm1x2 + Tm1x1*VV3x1*ynm1x3 + Tm2x1*VV1x1*ynm2x1 + Tm2x1*VV2x1*ynm2x2 + Tm2x1*VV3x1*ynm2x3 + Tm3x1*VV1x1*ynm3x1 + Tm3x1*VV2x1*ynm3x2 + Tm3x1*VV3x1*ynm3x3',
                   texname = '\\text{I30a11}')

I30a12 = Parameter(name = 'I30a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x1*ynm1x1 + Tm1x2*VV2x1*ynm1x2 + Tm1x2*VV3x1*ynm1x3 + Tm2x2*VV1x1*ynm2x1 + Tm2x2*VV2x1*ynm2x2 + Tm2x2*VV3x1*ynm2x3 + Tm3x2*VV1x1*ynm3x1 + Tm3x2*VV2x1*ynm3x2 + Tm3x2*VV3x1*ynm3x3',
                   texname = '\\text{I30a12}')

I30a13 = Parameter(name = 'I30a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x1*ynm1x1 + Tm1x3*VV2x1*ynm1x2 + Tm1x3*VV3x1*ynm1x3 + Tm2x3*VV1x1*ynm2x1 + Tm2x3*VV2x1*ynm2x2 + Tm2x3*VV3x1*ynm2x3 + Tm3x3*VV1x1*ynm3x1 + Tm3x3*VV2x1*ynm3x2 + Tm3x3*VV3x1*ynm3x3',
                   texname = '\\text{I30a13}')

I30a21 = Parameter(name = 'I30a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x2*ynm1x1 + Tm1x1*VV2x2*ynm1x2 + Tm1x1*VV3x2*ynm1x3 + Tm2x1*VV1x2*ynm2x1 + Tm2x1*VV2x2*ynm2x2 + Tm2x1*VV3x2*ynm2x3 + Tm3x1*VV1x2*ynm3x1 + Tm3x1*VV2x2*ynm3x2 + Tm3x1*VV3x2*ynm3x3',
                   texname = '\\text{I30a21}')

I30a22 = Parameter(name = 'I30a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x2*ynm1x1 + Tm1x2*VV2x2*ynm1x2 + Tm1x2*VV3x2*ynm1x3 + Tm2x2*VV1x2*ynm2x1 + Tm2x2*VV2x2*ynm2x2 + Tm2x2*VV3x2*ynm2x3 + Tm3x2*VV1x2*ynm3x1 + Tm3x2*VV2x2*ynm3x2 + Tm3x2*VV3x2*ynm3x3',
                   texname = '\\text{I30a22}')

I30a23 = Parameter(name = 'I30a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x2*ynm1x1 + Tm1x3*VV2x2*ynm1x2 + Tm1x3*VV3x2*ynm1x3 + Tm2x3*VV1x2*ynm2x1 + Tm2x3*VV2x2*ynm2x2 + Tm2x3*VV3x2*ynm2x3 + Tm3x3*VV1x2*ynm3x1 + Tm3x3*VV2x2*ynm3x2 + Tm3x3*VV3x2*ynm3x3',
                   texname = '\\text{I30a23}')

I30a31 = Parameter(name = 'I30a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x3*ynm1x1 + Tm1x1*VV2x3*ynm1x2 + Tm1x1*VV3x3*ynm1x3 + Tm2x1*VV1x3*ynm2x1 + Tm2x1*VV2x3*ynm2x2 + Tm2x1*VV3x3*ynm2x3 + Tm3x1*VV1x3*ynm3x1 + Tm3x1*VV2x3*ynm3x2 + Tm3x1*VV3x3*ynm3x3',
                   texname = '\\text{I30a31}')

I30a32 = Parameter(name = 'I30a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x3*ynm1x1 + Tm1x2*VV2x3*ynm1x2 + Tm1x2*VV3x3*ynm1x3 + Tm2x2*VV1x3*ynm2x1 + Tm2x2*VV2x3*ynm2x2 + Tm2x2*VV3x3*ynm2x3 + Tm3x2*VV1x3*ynm3x1 + Tm3x2*VV2x3*ynm3x2 + Tm3x2*VV3x3*ynm3x3',
                   texname = '\\text{I30a32}')

I30a33 = Parameter(name = 'I30a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x3*ynm1x1 + Tm1x3*VV2x3*ynm1x2 + Tm1x3*VV3x3*ynm1x3 + Tm2x3*VV1x3*ynm2x1 + Tm2x3*VV2x3*ynm2x2 + Tm2x3*VV3x3*ynm2x3 + Tm3x3*VV1x3*ynm3x1 + Tm3x3*VV2x3*ynm3x2 + Tm3x3*VV3x3*ynm3x3',
                   texname = '\\text{I30a33}')

I31a11 = Parameter(name = 'I31a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x1) + Tm2x1*ynm1x2*complexconjugate(VV1x1) + Tm3x1*ynm1x3*complexconjugate(VV1x1) + Tm1x1*ynm2x1*complexconjugate(VV2x1) + Tm2x1*ynm2x2*complexconjugate(VV2x1) + Tm3x1*ynm2x3*complexconjugate(VV2x1) + Tm1x1*ynm3x1*complexconjugate(VV3x1) + Tm2x1*ynm3x2*complexconjugate(VV3x1) + Tm3x1*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I31a11}')

I31a12 = Parameter(name = 'I31a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x1) + Tm2x2*ynm1x2*complexconjugate(VV1x1) + Tm3x2*ynm1x3*complexconjugate(VV1x1) + Tm1x2*ynm2x1*complexconjugate(VV2x1) + Tm2x2*ynm2x2*complexconjugate(VV2x1) + Tm3x2*ynm2x3*complexconjugate(VV2x1) + Tm1x2*ynm3x1*complexconjugate(VV3x1) + Tm2x2*ynm3x2*complexconjugate(VV3x1) + Tm3x2*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I31a12}')

I31a13 = Parameter(name = 'I31a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x1) + Tm2x3*ynm1x2*complexconjugate(VV1x1) + Tm3x3*ynm1x3*complexconjugate(VV1x1) + Tm1x3*ynm2x1*complexconjugate(VV2x1) + Tm2x3*ynm2x2*complexconjugate(VV2x1) + Tm3x3*ynm2x3*complexconjugate(VV2x1) + Tm1x3*ynm3x1*complexconjugate(VV3x1) + Tm2x3*ynm3x2*complexconjugate(VV3x1) + Tm3x3*ynm3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I31a13}')

I31a21 = Parameter(name = 'I31a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x2) + Tm2x1*ynm1x2*complexconjugate(VV1x2) + Tm3x1*ynm1x3*complexconjugate(VV1x2) + Tm1x1*ynm2x1*complexconjugate(VV2x2) + Tm2x1*ynm2x2*complexconjugate(VV2x2) + Tm3x1*ynm2x3*complexconjugate(VV2x2) + Tm1x1*ynm3x1*complexconjugate(VV3x2) + Tm2x1*ynm3x2*complexconjugate(VV3x2) + Tm3x1*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I31a21}')

I31a22 = Parameter(name = 'I31a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x2) + Tm2x2*ynm1x2*complexconjugate(VV1x2) + Tm3x2*ynm1x3*complexconjugate(VV1x2) + Tm1x2*ynm2x1*complexconjugate(VV2x2) + Tm2x2*ynm2x2*complexconjugate(VV2x2) + Tm3x2*ynm2x3*complexconjugate(VV2x2) + Tm1x2*ynm3x1*complexconjugate(VV3x2) + Tm2x2*ynm3x2*complexconjugate(VV3x2) + Tm3x2*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I31a22}')

I31a23 = Parameter(name = 'I31a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x2) + Tm2x3*ynm1x2*complexconjugate(VV1x2) + Tm3x3*ynm1x3*complexconjugate(VV1x2) + Tm1x3*ynm2x1*complexconjugate(VV2x2) + Tm2x3*ynm2x2*complexconjugate(VV2x2) + Tm3x3*ynm2x3*complexconjugate(VV2x2) + Tm1x3*ynm3x1*complexconjugate(VV3x2) + Tm2x3*ynm3x2*complexconjugate(VV3x2) + Tm3x3*ynm3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I31a23}')

I31a31 = Parameter(name = 'I31a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynm1x1*complexconjugate(VV1x3) + Tm2x1*ynm1x2*complexconjugate(VV1x3) + Tm3x1*ynm1x3*complexconjugate(VV1x3) + Tm1x1*ynm2x1*complexconjugate(VV2x3) + Tm2x1*ynm2x2*complexconjugate(VV2x3) + Tm3x1*ynm2x3*complexconjugate(VV2x3) + Tm1x1*ynm3x1*complexconjugate(VV3x3) + Tm2x1*ynm3x2*complexconjugate(VV3x3) + Tm3x1*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I31a31}')

I31a32 = Parameter(name = 'I31a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynm1x1*complexconjugate(VV1x3) + Tm2x2*ynm1x2*complexconjugate(VV1x3) + Tm3x2*ynm1x3*complexconjugate(VV1x3) + Tm1x2*ynm2x1*complexconjugate(VV2x3) + Tm2x2*ynm2x2*complexconjugate(VV2x3) + Tm3x2*ynm2x3*complexconjugate(VV2x3) + Tm1x2*ynm3x1*complexconjugate(VV3x3) + Tm2x2*ynm3x2*complexconjugate(VV3x3) + Tm3x2*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I31a32}')

I31a33 = Parameter(name = 'I31a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynm1x1*complexconjugate(VV1x3) + Tm2x3*ynm1x2*complexconjugate(VV1x3) + Tm3x3*ynm1x3*complexconjugate(VV1x3) + Tm1x3*ynm2x1*complexconjugate(VV2x3) + Tm2x3*ynm2x2*complexconjugate(VV2x3) + Tm3x3*ynm2x3*complexconjugate(VV2x3) + Tm1x3*ynm3x1*complexconjugate(VV3x3) + Tm2x3*ynm3x2*complexconjugate(VV3x3) + Tm3x3*ynm3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I31a33}')

I32a11 = Parameter(name = 'I32a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x1*ynm1x1 + Tm2x1*VV1x1*ynm1x2 + Tm3x1*VV1x1*ynm1x3 + Tm1x1*VV2x1*ynm2x1 + Tm2x1*VV2x1*ynm2x2 + Tm3x1*VV2x1*ynm2x3 + Tm1x1*VV3x1*ynm3x1 + Tm2x1*VV3x1*ynm3x2 + Tm3x1*VV3x1*ynm3x3',
                   texname = '\\text{I32a11}')

I32a12 = Parameter(name = 'I32a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x1*ynm1x1 + Tm2x2*VV1x1*ynm1x2 + Tm3x2*VV1x1*ynm1x3 + Tm1x2*VV2x1*ynm2x1 + Tm2x2*VV2x1*ynm2x2 + Tm3x2*VV2x1*ynm2x3 + Tm1x2*VV3x1*ynm3x1 + Tm2x2*VV3x1*ynm3x2 + Tm3x2*VV3x1*ynm3x3',
                   texname = '\\text{I32a12}')

I32a13 = Parameter(name = 'I32a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x1*ynm1x1 + Tm2x3*VV1x1*ynm1x2 + Tm3x3*VV1x1*ynm1x3 + Tm1x3*VV2x1*ynm2x1 + Tm2x3*VV2x1*ynm2x2 + Tm3x3*VV2x1*ynm2x3 + Tm1x3*VV3x1*ynm3x1 + Tm2x3*VV3x1*ynm3x2 + Tm3x3*VV3x1*ynm3x3',
                   texname = '\\text{I32a13}')

I32a21 = Parameter(name = 'I32a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x2*ynm1x1 + Tm2x1*VV1x2*ynm1x2 + Tm3x1*VV1x2*ynm1x3 + Tm1x1*VV2x2*ynm2x1 + Tm2x1*VV2x2*ynm2x2 + Tm3x1*VV2x2*ynm2x3 + Tm1x1*VV3x2*ynm3x1 + Tm2x1*VV3x2*ynm3x2 + Tm3x1*VV3x2*ynm3x3',
                   texname = '\\text{I32a21}')

I32a22 = Parameter(name = 'I32a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x2*ynm1x1 + Tm2x2*VV1x2*ynm1x2 + Tm3x2*VV1x2*ynm1x3 + Tm1x2*VV2x2*ynm2x1 + Tm2x2*VV2x2*ynm2x2 + Tm3x2*VV2x2*ynm2x3 + Tm1x2*VV3x2*ynm3x1 + Tm2x2*VV3x2*ynm3x2 + Tm3x2*VV3x2*ynm3x3',
                   texname = '\\text{I32a22}')

I32a23 = Parameter(name = 'I32a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x2*ynm1x1 + Tm2x3*VV1x2*ynm1x2 + Tm3x3*VV1x2*ynm1x3 + Tm1x3*VV2x2*ynm2x1 + Tm2x3*VV2x2*ynm2x2 + Tm3x3*VV2x2*ynm2x3 + Tm1x3*VV3x2*ynm3x1 + Tm2x3*VV3x2*ynm3x2 + Tm3x3*VV3x2*ynm3x3',
                   texname = '\\text{I32a23}')

I32a31 = Parameter(name = 'I32a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*VV1x3*ynm1x1 + Tm2x1*VV1x3*ynm1x2 + Tm3x1*VV1x3*ynm1x3 + Tm1x1*VV2x3*ynm2x1 + Tm2x1*VV2x3*ynm2x2 + Tm3x1*VV2x3*ynm2x3 + Tm1x1*VV3x3*ynm3x1 + Tm2x1*VV3x3*ynm3x2 + Tm3x1*VV3x3*ynm3x3',
                   texname = '\\text{I32a31}')

I32a32 = Parameter(name = 'I32a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*VV1x3*ynm1x1 + Tm2x2*VV1x3*ynm1x2 + Tm3x2*VV1x3*ynm1x3 + Tm1x2*VV2x3*ynm2x1 + Tm2x2*VV2x3*ynm2x2 + Tm3x2*VV2x3*ynm2x3 + Tm1x2*VV3x3*ynm3x1 + Tm2x2*VV3x3*ynm3x2 + Tm3x2*VV3x3*ynm3x3',
                   texname = '\\text{I32a32}')

I32a33 = Parameter(name = 'I32a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*VV1x3*ynm1x1 + Tm2x3*VV1x3*ynm1x2 + Tm3x3*VV1x3*ynm1x3 + Tm1x3*VV2x3*ynm2x1 + Tm2x3*VV2x3*ynm2x2 + Tm3x3*VV2x3*ynm2x3 + Tm1x3*VV3x3*ynm3x1 + Tm2x3*VV3x3*ynm3x2 + Tm3x3*VV3x3*ynm3x3',
                   texname = '\\text{I32a33}')

I33a11 = Parameter(name = 'I33a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U11*ynd1x1 + Tm2x1*U11*ynd1x2 + Tm3x1*U11*ynd1x3 + Tm1x1*U21*ynd2x1 + Tm2x1*U21*ynd2x2 + Tm3x1*U21*ynd2x3 + Tm1x1*U31*ynd3x1 + Tm2x1*U31*ynd3x2 + Tm3x1*U31*ynd3x3',
                   texname = '\\text{I33a11}')

I33a12 = Parameter(name = 'I33a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U11*ynd1x1 + Tm2x2*U11*ynd1x2 + Tm3x2*U11*ynd1x3 + Tm1x2*U21*ynd2x1 + Tm2x2*U21*ynd2x2 + Tm3x2*U21*ynd2x3 + Tm1x2*U31*ynd3x1 + Tm2x2*U31*ynd3x2 + Tm3x2*U31*ynd3x3',
                   texname = '\\text{I33a12}')

I33a13 = Parameter(name = 'I33a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U11*ynd1x1 + Tm2x3*U11*ynd1x2 + Tm3x3*U11*ynd1x3 + Tm1x3*U21*ynd2x1 + Tm2x3*U21*ynd2x2 + Tm3x3*U21*ynd2x3 + Tm1x3*U31*ynd3x1 + Tm2x3*U31*ynd3x2 + Tm3x3*U31*ynd3x3',
                   texname = '\\text{I33a13}')

I33a21 = Parameter(name = 'I33a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U12*ynd1x1 + Tm2x1*U12*ynd1x2 + Tm3x1*U12*ynd1x3 + Tm1x1*U22*ynd2x1 + Tm2x1*U22*ynd2x2 + Tm3x1*U22*ynd2x3 + Tm1x1*U32*ynd3x1 + Tm2x1*U32*ynd3x2 + Tm3x1*U32*ynd3x3',
                   texname = '\\text{I33a21}')

I33a22 = Parameter(name = 'I33a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U12*ynd1x1 + Tm2x2*U12*ynd1x2 + Tm3x2*U12*ynd1x3 + Tm1x2*U22*ynd2x1 + Tm2x2*U22*ynd2x2 + Tm3x2*U22*ynd2x3 + Tm1x2*U32*ynd3x1 + Tm2x2*U32*ynd3x2 + Tm3x2*U32*ynd3x3',
                   texname = '\\text{I33a22}')

I33a23 = Parameter(name = 'I33a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U12*ynd1x1 + Tm2x3*U12*ynd1x2 + Tm3x3*U12*ynd1x3 + Tm1x3*U22*ynd2x1 + Tm2x3*U22*ynd2x2 + Tm3x3*U22*ynd2x3 + Tm1x3*U32*ynd3x1 + Tm2x3*U32*ynd3x2 + Tm3x3*U32*ynd3x3',
                   texname = '\\text{I33a23}')

I33a31 = Parameter(name = 'I33a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U13*ynd1x1 + Tm2x1*U13*ynd1x2 + Tm3x1*U13*ynd1x3 + Tm1x1*U23*ynd2x1 + Tm2x1*U23*ynd2x2 + Tm3x1*U23*ynd2x3 + Tm1x1*U33*ynd3x1 + Tm2x1*U33*ynd3x2 + Tm3x1*U33*ynd3x3',
                   texname = '\\text{I33a31}')

I33a32 = Parameter(name = 'I33a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U13*ynd1x1 + Tm2x2*U13*ynd1x2 + Tm3x2*U13*ynd1x3 + Tm1x2*U23*ynd2x1 + Tm2x2*U23*ynd2x2 + Tm3x2*U23*ynd2x3 + Tm1x2*U33*ynd3x1 + Tm2x2*U33*ynd3x2 + Tm3x2*U33*ynd3x3',
                   texname = '\\text{I33a32}')

I33a33 = Parameter(name = 'I33a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U13*ynd1x1 + Tm2x3*U13*ynd1x2 + Tm3x3*U13*ynd1x3 + Tm1x3*U23*ynd2x1 + Tm2x3*U23*ynd2x2 + Tm3x3*U23*ynd2x3 + Tm1x3*U33*ynd3x1 + Tm2x3*U33*ynd3x2 + Tm3x3*U33*ynd3x3',
                   texname = '\\text{I33a33}')

I34a11 = Parameter(name = 'I34a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U11*ynd1x1 + Tm2x1*U11*ynd1x2 + Tm3x1*U11*ynd1x3 + Tm1x1*U21*ynd2x1 + Tm2x1*U21*ynd2x2 + Tm3x1*U21*ynd2x3 + Tm1x1*U31*ynd3x1 + Tm2x1*U31*ynd3x2 + Tm3x1*U31*ynd3x3',
                   texname = '\\text{I34a11}')

I34a12 = Parameter(name = 'I34a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U12*ynd1x1 + Tm2x1*U12*ynd1x2 + Tm3x1*U12*ynd1x3 + Tm1x1*U22*ynd2x1 + Tm2x1*U22*ynd2x2 + Tm3x1*U22*ynd2x3 + Tm1x1*U32*ynd3x1 + Tm2x1*U32*ynd3x2 + Tm3x1*U32*ynd3x3',
                   texname = '\\text{I34a12}')

I34a13 = Parameter(name = 'I34a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*U13*ynd1x1 + Tm2x1*U13*ynd1x2 + Tm3x1*U13*ynd1x3 + Tm1x1*U23*ynd2x1 + Tm2x1*U23*ynd2x2 + Tm3x1*U23*ynd2x3 + Tm1x1*U33*ynd3x1 + Tm2x1*U33*ynd3x2 + Tm3x1*U33*ynd3x3',
                   texname = '\\text{I34a13}')

I34a21 = Parameter(name = 'I34a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U11*ynd1x1 + Tm2x2*U11*ynd1x2 + Tm3x2*U11*ynd1x3 + Tm1x2*U21*ynd2x1 + Tm2x2*U21*ynd2x2 + Tm3x2*U21*ynd2x3 + Tm1x2*U31*ynd3x1 + Tm2x2*U31*ynd3x2 + Tm3x2*U31*ynd3x3',
                   texname = '\\text{I34a21}')

I34a22 = Parameter(name = 'I34a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U12*ynd1x1 + Tm2x2*U12*ynd1x2 + Tm3x2*U12*ynd1x3 + Tm1x2*U22*ynd2x1 + Tm2x2*U22*ynd2x2 + Tm3x2*U22*ynd2x3 + Tm1x2*U32*ynd3x1 + Tm2x2*U32*ynd3x2 + Tm3x2*U32*ynd3x3',
                   texname = '\\text{I34a22}')

I34a23 = Parameter(name = 'I34a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*U13*ynd1x1 + Tm2x2*U13*ynd1x2 + Tm3x2*U13*ynd1x3 + Tm1x2*U23*ynd2x1 + Tm2x2*U23*ynd2x2 + Tm3x2*U23*ynd2x3 + Tm1x2*U33*ynd3x1 + Tm2x2*U33*ynd3x2 + Tm3x2*U33*ynd3x3',
                   texname = '\\text{I34a23}')

I34a31 = Parameter(name = 'I34a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U11*ynd1x1 + Tm2x3*U11*ynd1x2 + Tm3x3*U11*ynd1x3 + Tm1x3*U21*ynd2x1 + Tm2x3*U21*ynd2x2 + Tm3x3*U21*ynd2x3 + Tm1x3*U31*ynd3x1 + Tm2x3*U31*ynd3x2 + Tm3x3*U31*ynd3x3',
                   texname = '\\text{I34a31}')

I34a32 = Parameter(name = 'I34a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U12*ynd1x1 + Tm2x3*U12*ynd1x2 + Tm3x3*U12*ynd1x3 + Tm1x3*U22*ynd2x1 + Tm2x3*U22*ynd2x2 + Tm3x3*U22*ynd2x3 + Tm1x3*U32*ynd3x1 + Tm2x3*U32*ynd3x2 + Tm3x3*U32*ynd3x3',
                   texname = '\\text{I34a32}')

I34a33 = Parameter(name = 'I34a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*U13*ynd1x1 + Tm2x3*U13*ynd1x2 + Tm3x3*U13*ynd1x3 + Tm1x3*U23*ynd2x1 + Tm2x3*U23*ynd2x2 + Tm3x3*U23*ynd2x3 + Tm1x3*U33*ynd3x1 + Tm2x3*U33*ynd3x2 + Tm3x3*U33*ynd3x3',
                   texname = '\\text{I34a33}')

I35a11 = Parameter(name = 'I35a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U11) + Tm2x1*ynd1x2*complexconjugate(U11) + Tm3x1*ynd1x3*complexconjugate(U11) + Tm1x1*ynd2x1*complexconjugate(U21) + Tm2x1*ynd2x2*complexconjugate(U21) + Tm3x1*ynd2x3*complexconjugate(U21) + Tm1x1*ynd3x1*complexconjugate(U31) + Tm2x1*ynd3x2*complexconjugate(U31) + Tm3x1*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I35a11}')

I35a12 = Parameter(name = 'I35a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U11) + Tm2x2*ynd1x2*complexconjugate(U11) + Tm3x2*ynd1x3*complexconjugate(U11) + Tm1x2*ynd2x1*complexconjugate(U21) + Tm2x2*ynd2x2*complexconjugate(U21) + Tm3x2*ynd2x3*complexconjugate(U21) + Tm1x2*ynd3x1*complexconjugate(U31) + Tm2x2*ynd3x2*complexconjugate(U31) + Tm3x2*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I35a12}')

I35a13 = Parameter(name = 'I35a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U11) + Tm2x3*ynd1x2*complexconjugate(U11) + Tm3x3*ynd1x3*complexconjugate(U11) + Tm1x3*ynd2x1*complexconjugate(U21) + Tm2x3*ynd2x2*complexconjugate(U21) + Tm3x3*ynd2x3*complexconjugate(U21) + Tm1x3*ynd3x1*complexconjugate(U31) + Tm2x3*ynd3x2*complexconjugate(U31) + Tm3x3*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I35a13}')

I35a21 = Parameter(name = 'I35a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U12) + Tm2x1*ynd1x2*complexconjugate(U12) + Tm3x1*ynd1x3*complexconjugate(U12) + Tm1x1*ynd2x1*complexconjugate(U22) + Tm2x1*ynd2x2*complexconjugate(U22) + Tm3x1*ynd2x3*complexconjugate(U22) + Tm1x1*ynd3x1*complexconjugate(U32) + Tm2x1*ynd3x2*complexconjugate(U32) + Tm3x1*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I35a21}')

I35a22 = Parameter(name = 'I35a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U12) + Tm2x2*ynd1x2*complexconjugate(U12) + Tm3x2*ynd1x3*complexconjugate(U12) + Tm1x2*ynd2x1*complexconjugate(U22) + Tm2x2*ynd2x2*complexconjugate(U22) + Tm3x2*ynd2x3*complexconjugate(U22) + Tm1x2*ynd3x1*complexconjugate(U32) + Tm2x2*ynd3x2*complexconjugate(U32) + Tm3x2*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I35a22}')

I35a23 = Parameter(name = 'I35a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U12) + Tm2x3*ynd1x2*complexconjugate(U12) + Tm3x3*ynd1x3*complexconjugate(U12) + Tm1x3*ynd2x1*complexconjugate(U22) + Tm2x3*ynd2x2*complexconjugate(U22) + Tm3x3*ynd2x3*complexconjugate(U22) + Tm1x3*ynd3x1*complexconjugate(U32) + Tm2x3*ynd3x2*complexconjugate(U32) + Tm3x3*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I35a23}')

I35a31 = Parameter(name = 'I35a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U13) + Tm2x1*ynd1x2*complexconjugate(U13) + Tm3x1*ynd1x3*complexconjugate(U13) + Tm1x1*ynd2x1*complexconjugate(U23) + Tm2x1*ynd2x2*complexconjugate(U23) + Tm3x1*ynd2x3*complexconjugate(U23) + Tm1x1*ynd3x1*complexconjugate(U33) + Tm2x1*ynd3x2*complexconjugate(U33) + Tm3x1*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I35a31}')

I35a32 = Parameter(name = 'I35a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U13) + Tm2x2*ynd1x2*complexconjugate(U13) + Tm3x2*ynd1x3*complexconjugate(U13) + Tm1x2*ynd2x1*complexconjugate(U23) + Tm2x2*ynd2x2*complexconjugate(U23) + Tm3x2*ynd2x3*complexconjugate(U23) + Tm1x2*ynd3x1*complexconjugate(U33) + Tm2x2*ynd3x2*complexconjugate(U33) + Tm3x2*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I35a32}')

I35a33 = Parameter(name = 'I35a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U13) + Tm2x3*ynd1x2*complexconjugate(U13) + Tm3x3*ynd1x3*complexconjugate(U13) + Tm1x3*ynd2x1*complexconjugate(U23) + Tm2x3*ynd2x2*complexconjugate(U23) + Tm3x3*ynd2x3*complexconjugate(U23) + Tm1x3*ynd3x1*complexconjugate(U33) + Tm2x3*ynd3x2*complexconjugate(U33) + Tm3x3*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I35a33}')

I36a11 = Parameter(name = 'I36a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U11) + Tm2x1*ynd1x2*complexconjugate(U11) + Tm3x1*ynd1x3*complexconjugate(U11) + Tm1x1*ynd2x1*complexconjugate(U21) + Tm2x1*ynd2x2*complexconjugate(U21) + Tm3x1*ynd2x3*complexconjugate(U21) + Tm1x1*ynd3x1*complexconjugate(U31) + Tm2x1*ynd3x2*complexconjugate(U31) + Tm3x1*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I36a11}')

I36a12 = Parameter(name = 'I36a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U12) + Tm2x1*ynd1x2*complexconjugate(U12) + Tm3x1*ynd1x3*complexconjugate(U12) + Tm1x1*ynd2x1*complexconjugate(U22) + Tm2x1*ynd2x2*complexconjugate(U22) + Tm3x1*ynd2x3*complexconjugate(U22) + Tm1x1*ynd3x1*complexconjugate(U32) + Tm2x1*ynd3x2*complexconjugate(U32) + Tm3x1*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I36a12}')

I36a13 = Parameter(name = 'I36a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*ynd1x1*complexconjugate(U13) + Tm2x1*ynd1x2*complexconjugate(U13) + Tm3x1*ynd1x3*complexconjugate(U13) + Tm1x1*ynd2x1*complexconjugate(U23) + Tm2x1*ynd2x2*complexconjugate(U23) + Tm3x1*ynd2x3*complexconjugate(U23) + Tm1x1*ynd3x1*complexconjugate(U33) + Tm2x1*ynd3x2*complexconjugate(U33) + Tm3x1*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I36a13}')

I36a21 = Parameter(name = 'I36a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U11) + Tm2x2*ynd1x2*complexconjugate(U11) + Tm3x2*ynd1x3*complexconjugate(U11) + Tm1x2*ynd2x1*complexconjugate(U21) + Tm2x2*ynd2x2*complexconjugate(U21) + Tm3x2*ynd2x3*complexconjugate(U21) + Tm1x2*ynd3x1*complexconjugate(U31) + Tm2x2*ynd3x2*complexconjugate(U31) + Tm3x2*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I36a21}')

I36a22 = Parameter(name = 'I36a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U12) + Tm2x2*ynd1x2*complexconjugate(U12) + Tm3x2*ynd1x3*complexconjugate(U12) + Tm1x2*ynd2x1*complexconjugate(U22) + Tm2x2*ynd2x2*complexconjugate(U22) + Tm3x2*ynd2x3*complexconjugate(U22) + Tm1x2*ynd3x1*complexconjugate(U32) + Tm2x2*ynd3x2*complexconjugate(U32) + Tm3x2*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I36a22}')

I36a23 = Parameter(name = 'I36a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*ynd1x1*complexconjugate(U13) + Tm2x2*ynd1x2*complexconjugate(U13) + Tm3x2*ynd1x3*complexconjugate(U13) + Tm1x2*ynd2x1*complexconjugate(U23) + Tm2x2*ynd2x2*complexconjugate(U23) + Tm3x2*ynd2x3*complexconjugate(U23) + Tm1x2*ynd3x1*complexconjugate(U33) + Tm2x2*ynd3x2*complexconjugate(U33) + Tm3x2*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I36a23}')

I36a31 = Parameter(name = 'I36a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U11) + Tm2x3*ynd1x2*complexconjugate(U11) + Tm3x3*ynd1x3*complexconjugate(U11) + Tm1x3*ynd2x1*complexconjugate(U21) + Tm2x3*ynd2x2*complexconjugate(U21) + Tm3x3*ynd2x3*complexconjugate(U21) + Tm1x3*ynd3x1*complexconjugate(U31) + Tm2x3*ynd3x2*complexconjugate(U31) + Tm3x3*ynd3x3*complexconjugate(U31)',
                   texname = '\\text{I36a31}')

I36a32 = Parameter(name = 'I36a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U12) + Tm2x3*ynd1x2*complexconjugate(U12) + Tm3x3*ynd1x3*complexconjugate(U12) + Tm1x3*ynd2x1*complexconjugate(U22) + Tm2x3*ynd2x2*complexconjugate(U22) + Tm3x3*ynd2x3*complexconjugate(U22) + Tm1x3*ynd3x1*complexconjugate(U32) + Tm2x3*ynd3x2*complexconjugate(U32) + Tm3x3*ynd3x3*complexconjugate(U32)',
                   texname = '\\text{I36a32}')

I36a33 = Parameter(name = 'I36a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3*ynd1x1*complexconjugate(U13) + Tm2x3*ynd1x2*complexconjugate(U13) + Tm3x3*ynd1x3*complexconjugate(U13) + Tm1x3*ynd2x1*complexconjugate(U23) + Tm2x3*ynd2x2*complexconjugate(U23) + Tm3x3*ynd2x3*complexconjugate(U23) + Tm1x3*ynd3x1*complexconjugate(U33) + Tm2x3*ynd3x2*complexconjugate(U33) + Tm3x3*ynd3x3*complexconjugate(U33)',
                   texname = '\\text{I36a33}')

I37a11 = Parameter(name = 'I37a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1**2*ynm1x1 + Tm1x1*Tm2x1*ynm1x2 + Tm1x1*Tm3x1*ynm1x3 + Tm1x1*Tm2x1*ynm2x1 + Tm2x1**2*ynm2x2 + Tm2x1*Tm3x1*ynm2x3 + Tm1x1*Tm3x1*ynm3x1 + Tm2x1*Tm3x1*ynm3x2 + Tm3x1**2*ynm3x3',
                   texname = '\\text{I37a11}')

I37a12 = Parameter(name = 'I37a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x2*Tm2x1*ynm1x2 + Tm1x2*Tm3x1*ynm1x3 + Tm1x1*Tm2x2*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x2*Tm3x1*ynm2x3 + Tm1x1*Tm3x2*ynm3x1 + Tm2x1*Tm3x2*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I37a12}')

I37a13 = Parameter(name = 'I37a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x3*Tm2x1*ynm1x2 + Tm1x3*Tm3x1*ynm1x3 + Tm1x1*Tm2x3*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x3*Tm3x1*ynm2x3 + Tm1x1*Tm3x3*ynm3x1 + Tm2x1*Tm3x3*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I37a13}')

I37a21 = Parameter(name = 'I37a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x1*Tm2x2*ynm1x2 + Tm1x1*Tm3x2*ynm1x3 + Tm1x2*Tm2x1*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x1*Tm3x2*ynm2x3 + Tm1x2*Tm3x1*ynm3x1 + Tm2x2*Tm3x1*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I37a21}')

I37a22 = Parameter(name = 'I37a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2**2*ynm1x1 + Tm1x2*Tm2x2*ynm1x2 + Tm1x2*Tm3x2*ynm1x3 + Tm1x2*Tm2x2*ynm2x1 + Tm2x2**2*ynm2x2 + Tm2x2*Tm3x2*ynm2x3 + Tm1x2*Tm3x2*ynm3x1 + Tm2x2*Tm3x2*ynm3x2 + Tm3x2**2*ynm3x3',
                   texname = '\\text{I37a22}')

I37a23 = Parameter(name = 'I37a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x3*Tm2x2*ynm1x2 + Tm1x3*Tm3x2*ynm1x3 + Tm1x2*Tm2x3*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x3*Tm3x2*ynm2x3 + Tm1x2*Tm3x3*ynm3x1 + Tm2x2*Tm3x3*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I37a23}')

I37a31 = Parameter(name = 'I37a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x1*Tm2x3*ynm1x2 + Tm1x1*Tm3x3*ynm1x3 + Tm1x3*Tm2x1*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x1*Tm3x3*ynm2x3 + Tm1x3*Tm3x1*ynm3x1 + Tm2x3*Tm3x1*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I37a31}')

I37a32 = Parameter(name = 'I37a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x2*Tm2x3*ynm1x2 + Tm1x2*Tm3x3*ynm1x3 + Tm1x3*Tm2x2*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x2*Tm3x3*ynm2x3 + Tm1x3*Tm3x2*ynm3x1 + Tm2x3*Tm3x2*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I37a32}')

I37a33 = Parameter(name = 'I37a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3**2*ynm1x1 + Tm1x3*Tm2x3*ynm1x2 + Tm1x3*Tm3x3*ynm1x3 + Tm1x3*Tm2x3*ynm2x1 + Tm2x3**2*ynm2x2 + Tm2x3*Tm3x3*ynm2x3 + Tm1x3*Tm3x3*ynm3x1 + Tm2x3*Tm3x3*ynm3x2 + Tm3x3**2*ynm3x3',
                   texname = '\\text{I37a33}')

I38a11 = Parameter(name = 'I38a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1**2*ynm1x1 + Tm1x1*Tm2x1*ynm1x2 + Tm1x1*Tm3x1*ynm1x3 + Tm1x1*Tm2x1*ynm2x1 + Tm2x1**2*ynm2x2 + Tm2x1*Tm3x1*ynm2x3 + Tm1x1*Tm3x1*ynm3x1 + Tm2x1*Tm3x1*ynm3x2 + Tm3x1**2*ynm3x3',
                   texname = '\\text{I38a11}')

I38a12 = Parameter(name = 'I38a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x1*Tm2x2*ynm1x2 + Tm1x1*Tm3x2*ynm1x3 + Tm1x2*Tm2x1*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x1*Tm3x2*ynm2x3 + Tm1x2*Tm3x1*ynm3x1 + Tm2x2*Tm3x1*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I38a12}')

I38a13 = Parameter(name = 'I38a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x1*Tm2x3*ynm1x2 + Tm1x1*Tm3x3*ynm1x3 + Tm1x3*Tm2x1*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x1*Tm3x3*ynm2x3 + Tm1x3*Tm3x1*ynm3x1 + Tm2x3*Tm3x1*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I38a13}')

I38a21 = Parameter(name = 'I38a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x2*Tm2x1*ynm1x2 + Tm1x2*Tm3x1*ynm1x3 + Tm1x1*Tm2x2*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x2*Tm3x1*ynm2x3 + Tm1x1*Tm3x2*ynm3x1 + Tm2x1*Tm3x2*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I38a21}')

I38a22 = Parameter(name = 'I38a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2**2*ynm1x1 + Tm1x2*Tm2x2*ynm1x2 + Tm1x2*Tm3x2*ynm1x3 + Tm1x2*Tm2x2*ynm2x1 + Tm2x2**2*ynm2x2 + Tm2x2*Tm3x2*ynm2x3 + Tm1x2*Tm3x2*ynm3x1 + Tm2x2*Tm3x2*ynm3x2 + Tm3x2**2*ynm3x3',
                   texname = '\\text{I38a22}')

I38a23 = Parameter(name = 'I38a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x2*Tm2x3*ynm1x2 + Tm1x2*Tm3x3*ynm1x3 + Tm1x3*Tm2x2*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x2*Tm3x3*ynm2x3 + Tm1x3*Tm3x2*ynm3x1 + Tm2x3*Tm3x2*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I38a23}')

I38a31 = Parameter(name = 'I38a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x3*Tm2x1*ynm1x2 + Tm1x3*Tm3x1*ynm1x3 + Tm1x1*Tm2x3*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x3*Tm3x1*ynm2x3 + Tm1x1*Tm3x3*ynm3x1 + Tm2x1*Tm3x3*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I38a31}')

I38a32 = Parameter(name = 'I38a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x3*Tm2x2*ynm1x2 + Tm1x3*Tm3x2*ynm1x3 + Tm1x2*Tm2x3*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x3*Tm3x2*ynm2x3 + Tm1x2*Tm3x3*ynm3x1 + Tm2x2*Tm3x3*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I38a32}')

I38a33 = Parameter(name = 'I38a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3**2*ynm1x1 + Tm1x3*Tm2x3*ynm1x2 + Tm1x3*Tm3x3*ynm1x3 + Tm1x3*Tm2x3*ynm2x1 + Tm2x3**2*ynm2x2 + Tm2x3*Tm3x3*ynm2x3 + Tm1x3*Tm3x3*ynm3x1 + Tm2x3*Tm3x3*ynm3x2 + Tm3x3**2*ynm3x3',
                   texname = '\\text{I38a33}')

I39a11 = Parameter(name = 'I39a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1**2*ynm1x1 + Tm1x1*Tm2x1*ynm1x2 + Tm1x1*Tm3x1*ynm1x3 + Tm1x1*Tm2x1*ynm2x1 + Tm2x1**2*ynm2x2 + Tm2x1*Tm3x1*ynm2x3 + Tm1x1*Tm3x1*ynm3x1 + Tm2x1*Tm3x1*ynm3x2 + Tm3x1**2*ynm3x3',
                   texname = '\\text{I39a11}')

I39a12 = Parameter(name = 'I39a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x2*Tm2x1*ynm1x2 + Tm1x2*Tm3x1*ynm1x3 + Tm1x1*Tm2x2*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x2*Tm3x1*ynm2x3 + Tm1x1*Tm3x2*ynm3x1 + Tm2x1*Tm3x2*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I39a12}')

I39a13 = Parameter(name = 'I39a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x3*Tm2x1*ynm1x2 + Tm1x3*Tm3x1*ynm1x3 + Tm1x1*Tm2x3*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x3*Tm3x1*ynm2x3 + Tm1x1*Tm3x3*ynm3x1 + Tm2x1*Tm3x3*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I39a13}')

I39a21 = Parameter(name = 'I39a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x1*Tm2x2*ynm1x2 + Tm1x1*Tm3x2*ynm1x3 + Tm1x2*Tm2x1*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x1*Tm3x2*ynm2x3 + Tm1x2*Tm3x1*ynm3x1 + Tm2x2*Tm3x1*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I39a21}')

I39a22 = Parameter(name = 'I39a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2**2*ynm1x1 + Tm1x2*Tm2x2*ynm1x2 + Tm1x2*Tm3x2*ynm1x3 + Tm1x2*Tm2x2*ynm2x1 + Tm2x2**2*ynm2x2 + Tm2x2*Tm3x2*ynm2x3 + Tm1x2*Tm3x2*ynm3x1 + Tm2x2*Tm3x2*ynm3x2 + Tm3x2**2*ynm3x3',
                   texname = '\\text{I39a22}')

I39a23 = Parameter(name = 'I39a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x3*Tm2x2*ynm1x2 + Tm1x3*Tm3x2*ynm1x3 + Tm1x2*Tm2x3*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x3*Tm3x2*ynm2x3 + Tm1x2*Tm3x3*ynm3x1 + Tm2x2*Tm3x3*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I39a23}')

I39a31 = Parameter(name = 'I39a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x1*Tm2x3*ynm1x2 + Tm1x1*Tm3x3*ynm1x3 + Tm1x3*Tm2x1*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x1*Tm3x3*ynm2x3 + Tm1x3*Tm3x1*ynm3x1 + Tm2x3*Tm3x1*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I39a31}')

I39a32 = Parameter(name = 'I39a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x2*Tm2x3*ynm1x2 + Tm1x2*Tm3x3*ynm1x3 + Tm1x3*Tm2x2*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x2*Tm3x3*ynm2x3 + Tm1x3*Tm3x2*ynm3x1 + Tm2x3*Tm3x2*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I39a32}')

I39a33 = Parameter(name = 'I39a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3**2*ynm1x1 + Tm1x3*Tm2x3*ynm1x2 + Tm1x3*Tm3x3*ynm1x3 + Tm1x3*Tm2x3*ynm2x1 + Tm2x3**2*ynm2x2 + Tm2x3*Tm3x3*ynm2x3 + Tm1x3*Tm3x3*ynm3x1 + Tm2x3*Tm3x3*ynm3x2 + Tm3x3**2*ynm3x3',
                   texname = '\\text{I39a33}')

I4a11 = Parameter(name = 'I4a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x1) + VV2x1*complexconjugate(VV2x1) + VV3x1*complexconjugate(VV3x1)',
                  texname = '\\text{I4a11}')

I4a12 = Parameter(name = 'I4a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x1) + VV2x2*complexconjugate(VV2x1) + VV3x2*complexconjugate(VV3x1)',
                  texname = '\\text{I4a12}')

I4a13 = Parameter(name = 'I4a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x1) + VV2x3*complexconjugate(VV2x1) + VV3x3*complexconjugate(VV3x1)',
                  texname = '\\text{I4a13}')

I4a21 = Parameter(name = 'I4a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x2) + VV2x1*complexconjugate(VV2x2) + VV3x1*complexconjugate(VV3x2)',
                  texname = '\\text{I4a21}')

I4a22 = Parameter(name = 'I4a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x2) + VV2x2*complexconjugate(VV2x2) + VV3x2*complexconjugate(VV3x2)',
                  texname = '\\text{I4a22}')

I4a23 = Parameter(name = 'I4a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x2) + VV2x3*complexconjugate(VV2x2) + VV3x3*complexconjugate(VV3x2)',
                  texname = '\\text{I4a23}')

I4a31 = Parameter(name = 'I4a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x1*complexconjugate(VV1x3) + VV2x1*complexconjugate(VV2x3) + VV3x1*complexconjugate(VV3x3)',
                  texname = '\\text{I4a31}')

I4a32 = Parameter(name = 'I4a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x2*complexconjugate(VV1x3) + VV2x2*complexconjugate(VV2x3) + VV3x2*complexconjugate(VV3x3)',
                  texname = '\\text{I4a32}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'VV1x3*complexconjugate(VV1x3) + VV2x3*complexconjugate(VV2x3) + VV3x3*complexconjugate(VV3x3)',
                  texname = '\\text{I4a33}')

I40a11 = Parameter(name = 'I40a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1**2*ynm1x1 + Tm1x1*Tm2x1*ynm1x2 + Tm1x1*Tm3x1*ynm1x3 + Tm1x1*Tm2x1*ynm2x1 + Tm2x1**2*ynm2x2 + Tm2x1*Tm3x1*ynm2x3 + Tm1x1*Tm3x1*ynm3x1 + Tm2x1*Tm3x1*ynm3x2 + Tm3x1**2*ynm3x3',
                   texname = '\\text{I40a11}')

I40a12 = Parameter(name = 'I40a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x1*Tm2x2*ynm1x2 + Tm1x1*Tm3x2*ynm1x3 + Tm1x2*Tm2x1*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x1*Tm3x2*ynm2x3 + Tm1x2*Tm3x1*ynm3x1 + Tm2x2*Tm3x1*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I40a12}')

I40a13 = Parameter(name = 'I40a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x1*Tm2x3*ynm1x2 + Tm1x1*Tm3x3*ynm1x3 + Tm1x3*Tm2x1*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x1*Tm3x3*ynm2x3 + Tm1x3*Tm3x1*ynm3x1 + Tm2x3*Tm3x1*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I40a13}')

I40a21 = Parameter(name = 'I40a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x2*ynm1x1 + Tm1x2*Tm2x1*ynm1x2 + Tm1x2*Tm3x1*ynm1x3 + Tm1x1*Tm2x2*ynm2x1 + Tm2x1*Tm2x2*ynm2x2 + Tm2x2*Tm3x1*ynm2x3 + Tm1x1*Tm3x2*ynm3x1 + Tm2x1*Tm3x2*ynm3x2 + Tm3x1*Tm3x2*ynm3x3',
                   texname = '\\text{I40a21}')

I40a22 = Parameter(name = 'I40a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2**2*ynm1x1 + Tm1x2*Tm2x2*ynm1x2 + Tm1x2*Tm3x2*ynm1x3 + Tm1x2*Tm2x2*ynm2x1 + Tm2x2**2*ynm2x2 + Tm2x2*Tm3x2*ynm2x3 + Tm1x2*Tm3x2*ynm3x1 + Tm2x2*Tm3x2*ynm3x2 + Tm3x2**2*ynm3x3',
                   texname = '\\text{I40a22}')

I40a23 = Parameter(name = 'I40a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x2*Tm2x3*ynm1x2 + Tm1x2*Tm3x3*ynm1x3 + Tm1x3*Tm2x2*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x2*Tm3x3*ynm2x3 + Tm1x3*Tm3x2*ynm3x1 + Tm2x3*Tm3x2*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I40a23}')

I40a31 = Parameter(name = 'I40a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x1*Tm1x3*ynm1x1 + Tm1x3*Tm2x1*ynm1x2 + Tm1x3*Tm3x1*ynm1x3 + Tm1x1*Tm2x3*ynm2x1 + Tm2x1*Tm2x3*ynm2x2 + Tm2x3*Tm3x1*ynm2x3 + Tm1x1*Tm3x3*ynm3x1 + Tm2x1*Tm3x3*ynm3x2 + Tm3x1*Tm3x3*ynm3x3',
                   texname = '\\text{I40a31}')

I40a32 = Parameter(name = 'I40a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x2*Tm1x3*ynm1x1 + Tm1x3*Tm2x2*ynm1x2 + Tm1x3*Tm3x2*ynm1x3 + Tm1x2*Tm2x3*ynm2x1 + Tm2x2*Tm2x3*ynm2x2 + Tm2x3*Tm3x2*ynm2x3 + Tm1x2*Tm3x3*ynm3x1 + Tm2x2*Tm3x3*ynm3x2 + Tm3x2*Tm3x3*ynm3x3',
                   texname = '\\text{I40a32}')

I40a33 = Parameter(name = 'I40a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Tm1x3**2*ynm1x1 + Tm1x3*Tm2x3*ynm1x2 + Tm1x3*Tm3x3*ynm1x3 + Tm1x3*Tm2x3*ynm2x1 + Tm2x3**2*ynm2x2 + Tm2x3*Tm3x3*ynm2x3 + Tm1x3*Tm3x3*ynm3x1 + Tm2x3*Tm3x3*ynm3x2 + Tm3x3**2*ynm3x3',
                   texname = '\\text{I40a33}')

I41a11 = Parameter(name = 'I41a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x1*ynd1x1 + Sm1x1*VV2x1*ynd1x2 + Sm1x1*VV3x1*ynd1x3 + Sm2x1*VV1x1*ynd2x1 + Sm2x1*VV2x1*ynd2x2 + Sm2x1*VV3x1*ynd2x3 + Sm3x1*VV1x1*ynd3x1 + Sm3x1*VV2x1*ynd3x2 + Sm3x1*VV3x1*ynd3x3',
                   texname = '\\text{I41a11}')

I41a12 = Parameter(name = 'I41a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x1*ynd1x1 + Sm1x2*VV2x1*ynd1x2 + Sm1x2*VV3x1*ynd1x3 + Sm2x2*VV1x1*ynd2x1 + Sm2x2*VV2x1*ynd2x2 + Sm2x2*VV3x1*ynd2x3 + Sm3x2*VV1x1*ynd3x1 + Sm3x2*VV2x1*ynd3x2 + Sm3x2*VV3x1*ynd3x3',
                   texname = '\\text{I41a12}')

I41a13 = Parameter(name = 'I41a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x1*ynd1x1 + Sm1x3*VV2x1*ynd1x2 + Sm1x3*VV3x1*ynd1x3 + Sm2x3*VV1x1*ynd2x1 + Sm2x3*VV2x1*ynd2x2 + Sm2x3*VV3x1*ynd2x3 + Sm3x3*VV1x1*ynd3x1 + Sm3x3*VV2x1*ynd3x2 + Sm3x3*VV3x1*ynd3x3',
                   texname = '\\text{I41a13}')

I41a21 = Parameter(name = 'I41a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x2*ynd1x1 + Sm1x1*VV2x2*ynd1x2 + Sm1x1*VV3x2*ynd1x3 + Sm2x1*VV1x2*ynd2x1 + Sm2x1*VV2x2*ynd2x2 + Sm2x1*VV3x2*ynd2x3 + Sm3x1*VV1x2*ynd3x1 + Sm3x1*VV2x2*ynd3x2 + Sm3x1*VV3x2*ynd3x3',
                   texname = '\\text{I41a21}')

I41a22 = Parameter(name = 'I41a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x2*ynd1x1 + Sm1x2*VV2x2*ynd1x2 + Sm1x2*VV3x2*ynd1x3 + Sm2x2*VV1x2*ynd2x1 + Sm2x2*VV2x2*ynd2x2 + Sm2x2*VV3x2*ynd2x3 + Sm3x2*VV1x2*ynd3x1 + Sm3x2*VV2x2*ynd3x2 + Sm3x2*VV3x2*ynd3x3',
                   texname = '\\text{I41a22}')

I41a23 = Parameter(name = 'I41a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x2*ynd1x1 + Sm1x3*VV2x2*ynd1x2 + Sm1x3*VV3x2*ynd1x3 + Sm2x3*VV1x2*ynd2x1 + Sm2x3*VV2x2*ynd2x2 + Sm2x3*VV3x2*ynd2x3 + Sm3x3*VV1x2*ynd3x1 + Sm3x3*VV2x2*ynd3x2 + Sm3x3*VV3x2*ynd3x3',
                   texname = '\\text{I41a23}')

I41a31 = Parameter(name = 'I41a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x3*ynd1x1 + Sm1x1*VV2x3*ynd1x2 + Sm1x1*VV3x3*ynd1x3 + Sm2x1*VV1x3*ynd2x1 + Sm2x1*VV2x3*ynd2x2 + Sm2x1*VV3x3*ynd2x3 + Sm3x1*VV1x3*ynd3x1 + Sm3x1*VV2x3*ynd3x2 + Sm3x1*VV3x3*ynd3x3',
                   texname = '\\text{I41a31}')

I41a32 = Parameter(name = 'I41a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x3*ynd1x1 + Sm1x2*VV2x3*ynd1x2 + Sm1x2*VV3x3*ynd1x3 + Sm2x2*VV1x3*ynd2x1 + Sm2x2*VV2x3*ynd2x2 + Sm2x2*VV3x3*ynd2x3 + Sm3x2*VV1x3*ynd3x1 + Sm3x2*VV2x3*ynd3x2 + Sm3x2*VV3x3*ynd3x3',
                   texname = '\\text{I41a32}')

I41a33 = Parameter(name = 'I41a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x3*ynd1x1 + Sm1x3*VV2x3*ynd1x2 + Sm1x3*VV3x3*ynd1x3 + Sm2x3*VV1x3*ynd2x1 + Sm2x3*VV2x3*ynd2x2 + Sm2x3*VV3x3*ynd2x3 + Sm3x3*VV1x3*ynd3x1 + Sm3x3*VV2x3*ynd3x2 + Sm3x3*VV3x3*ynd3x3',
                   texname = '\\text{I41a33}')

I42a11 = Parameter(name = 'I42a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x1*ynd1x1 + Sm1x1*VV2x1*ynd1x2 + Sm1x1*VV3x1*ynd1x3 + Sm2x1*VV1x1*ynd2x1 + Sm2x1*VV2x1*ynd2x2 + Sm2x1*VV3x1*ynd2x3 + Sm3x1*VV1x1*ynd3x1 + Sm3x1*VV2x1*ynd3x2 + Sm3x1*VV3x1*ynd3x3',
                   texname = '\\text{I42a11}')

I42a12 = Parameter(name = 'I42a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x2*ynd1x1 + Sm1x1*VV2x2*ynd1x2 + Sm1x1*VV3x2*ynd1x3 + Sm2x1*VV1x2*ynd2x1 + Sm2x1*VV2x2*ynd2x2 + Sm2x1*VV3x2*ynd2x3 + Sm3x1*VV1x2*ynd3x1 + Sm3x1*VV2x2*ynd3x2 + Sm3x1*VV3x2*ynd3x3',
                   texname = '\\text{I42a12}')

I42a13 = Parameter(name = 'I42a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*VV1x3*ynd1x1 + Sm1x1*VV2x3*ynd1x2 + Sm1x1*VV3x3*ynd1x3 + Sm2x1*VV1x3*ynd2x1 + Sm2x1*VV2x3*ynd2x2 + Sm2x1*VV3x3*ynd2x3 + Sm3x1*VV1x3*ynd3x1 + Sm3x1*VV2x3*ynd3x2 + Sm3x1*VV3x3*ynd3x3',
                   texname = '\\text{I42a13}')

I42a21 = Parameter(name = 'I42a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x1*ynd1x1 + Sm1x2*VV2x1*ynd1x2 + Sm1x2*VV3x1*ynd1x3 + Sm2x2*VV1x1*ynd2x1 + Sm2x2*VV2x1*ynd2x2 + Sm2x2*VV3x1*ynd2x3 + Sm3x2*VV1x1*ynd3x1 + Sm3x2*VV2x1*ynd3x2 + Sm3x2*VV3x1*ynd3x3',
                   texname = '\\text{I42a21}')

I42a22 = Parameter(name = 'I42a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x2*ynd1x1 + Sm1x2*VV2x2*ynd1x2 + Sm1x2*VV3x2*ynd1x3 + Sm2x2*VV1x2*ynd2x1 + Sm2x2*VV2x2*ynd2x2 + Sm2x2*VV3x2*ynd2x3 + Sm3x2*VV1x2*ynd3x1 + Sm3x2*VV2x2*ynd3x2 + Sm3x2*VV3x2*ynd3x3',
                   texname = '\\text{I42a22}')

I42a23 = Parameter(name = 'I42a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*VV1x3*ynd1x1 + Sm1x2*VV2x3*ynd1x2 + Sm1x2*VV3x3*ynd1x3 + Sm2x2*VV1x3*ynd2x1 + Sm2x2*VV2x3*ynd2x2 + Sm2x2*VV3x3*ynd2x3 + Sm3x2*VV1x3*ynd3x1 + Sm3x2*VV2x3*ynd3x2 + Sm3x2*VV3x3*ynd3x3',
                   texname = '\\text{I42a23}')

I42a31 = Parameter(name = 'I42a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x1*ynd1x1 + Sm1x3*VV2x1*ynd1x2 + Sm1x3*VV3x1*ynd1x3 + Sm2x3*VV1x1*ynd2x1 + Sm2x3*VV2x1*ynd2x2 + Sm2x3*VV3x1*ynd2x3 + Sm3x3*VV1x1*ynd3x1 + Sm3x3*VV2x1*ynd3x2 + Sm3x3*VV3x1*ynd3x3',
                   texname = '\\text{I42a31}')

I42a32 = Parameter(name = 'I42a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x2*ynd1x1 + Sm1x3*VV2x2*ynd1x2 + Sm1x3*VV3x2*ynd1x3 + Sm2x3*VV1x2*ynd2x1 + Sm2x3*VV2x2*ynd2x2 + Sm2x3*VV3x2*ynd2x3 + Sm3x3*VV1x2*ynd3x1 + Sm3x3*VV2x2*ynd3x2 + Sm3x3*VV3x2*ynd3x3',
                   texname = '\\text{I42a32}')

I42a33 = Parameter(name = 'I42a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*VV1x3*ynd1x1 + Sm1x3*VV2x3*ynd1x2 + Sm1x3*VV3x3*ynd1x3 + Sm2x3*VV1x3*ynd2x1 + Sm2x3*VV2x3*ynd2x2 + Sm2x3*VV3x3*ynd2x3 + Sm3x3*VV1x3*ynd3x1 + Sm3x3*VV2x3*ynd3x2 + Sm3x3*VV3x3*ynd3x3',
                   texname = '\\text{I42a33}')

I43a11 = Parameter(name = 'I43a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x1) + Sm2x1*ynd2x1*complexconjugate(VV1x1) + Sm3x1*ynd3x1*complexconjugate(VV1x1) + Sm1x1*ynd1x2*complexconjugate(VV2x1) + Sm2x1*ynd2x2*complexconjugate(VV2x1) + Sm3x1*ynd3x2*complexconjugate(VV2x1) + Sm1x1*ynd1x3*complexconjugate(VV3x1) + Sm2x1*ynd2x3*complexconjugate(VV3x1) + Sm3x1*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I43a11}')

I43a12 = Parameter(name = 'I43a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x1) + Sm2x2*ynd2x1*complexconjugate(VV1x1) + Sm3x2*ynd3x1*complexconjugate(VV1x1) + Sm1x2*ynd1x2*complexconjugate(VV2x1) + Sm2x2*ynd2x2*complexconjugate(VV2x1) + Sm3x2*ynd3x2*complexconjugate(VV2x1) + Sm1x2*ynd1x3*complexconjugate(VV3x1) + Sm2x2*ynd2x3*complexconjugate(VV3x1) + Sm3x2*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I43a12}')

I43a13 = Parameter(name = 'I43a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x1) + Sm2x3*ynd2x1*complexconjugate(VV1x1) + Sm3x3*ynd3x1*complexconjugate(VV1x1) + Sm1x3*ynd1x2*complexconjugate(VV2x1) + Sm2x3*ynd2x2*complexconjugate(VV2x1) + Sm3x3*ynd3x2*complexconjugate(VV2x1) + Sm1x3*ynd1x3*complexconjugate(VV3x1) + Sm2x3*ynd2x3*complexconjugate(VV3x1) + Sm3x3*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I43a13}')

I43a21 = Parameter(name = 'I43a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x2) + Sm2x1*ynd2x1*complexconjugate(VV1x2) + Sm3x1*ynd3x1*complexconjugate(VV1x2) + Sm1x1*ynd1x2*complexconjugate(VV2x2) + Sm2x1*ynd2x2*complexconjugate(VV2x2) + Sm3x1*ynd3x2*complexconjugate(VV2x2) + Sm1x1*ynd1x3*complexconjugate(VV3x2) + Sm2x1*ynd2x3*complexconjugate(VV3x2) + Sm3x1*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I43a21}')

I43a22 = Parameter(name = 'I43a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x2) + Sm2x2*ynd2x1*complexconjugate(VV1x2) + Sm3x2*ynd3x1*complexconjugate(VV1x2) + Sm1x2*ynd1x2*complexconjugate(VV2x2) + Sm2x2*ynd2x2*complexconjugate(VV2x2) + Sm3x2*ynd3x2*complexconjugate(VV2x2) + Sm1x2*ynd1x3*complexconjugate(VV3x2) + Sm2x2*ynd2x3*complexconjugate(VV3x2) + Sm3x2*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I43a22}')

I43a23 = Parameter(name = 'I43a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x2) + Sm2x3*ynd2x1*complexconjugate(VV1x2) + Sm3x3*ynd3x1*complexconjugate(VV1x2) + Sm1x3*ynd1x2*complexconjugate(VV2x2) + Sm2x3*ynd2x2*complexconjugate(VV2x2) + Sm3x3*ynd3x2*complexconjugate(VV2x2) + Sm1x3*ynd1x3*complexconjugate(VV3x2) + Sm2x3*ynd2x3*complexconjugate(VV3x2) + Sm3x3*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I43a23}')

I43a31 = Parameter(name = 'I43a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x3) + Sm2x1*ynd2x1*complexconjugate(VV1x3) + Sm3x1*ynd3x1*complexconjugate(VV1x3) + Sm1x1*ynd1x2*complexconjugate(VV2x3) + Sm2x1*ynd2x2*complexconjugate(VV2x3) + Sm3x1*ynd3x2*complexconjugate(VV2x3) + Sm1x1*ynd1x3*complexconjugate(VV3x3) + Sm2x1*ynd2x3*complexconjugate(VV3x3) + Sm3x1*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I43a31}')

I43a32 = Parameter(name = 'I43a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x3) + Sm2x2*ynd2x1*complexconjugate(VV1x3) + Sm3x2*ynd3x1*complexconjugate(VV1x3) + Sm1x2*ynd1x2*complexconjugate(VV2x3) + Sm2x2*ynd2x2*complexconjugate(VV2x3) + Sm3x2*ynd3x2*complexconjugate(VV2x3) + Sm1x2*ynd1x3*complexconjugate(VV3x3) + Sm2x2*ynd2x3*complexconjugate(VV3x3) + Sm3x2*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I43a32}')

I43a33 = Parameter(name = 'I43a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x3) + Sm2x3*ynd2x1*complexconjugate(VV1x3) + Sm3x3*ynd3x1*complexconjugate(VV1x3) + Sm1x3*ynd1x2*complexconjugate(VV2x3) + Sm2x3*ynd2x2*complexconjugate(VV2x3) + Sm3x3*ynd3x2*complexconjugate(VV2x3) + Sm1x3*ynd1x3*complexconjugate(VV3x3) + Sm2x3*ynd2x3*complexconjugate(VV3x3) + Sm3x3*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I43a33}')

I44a11 = Parameter(name = 'I44a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x1) + Sm2x1*ynd2x1*complexconjugate(VV1x1) + Sm3x1*ynd3x1*complexconjugate(VV1x1) + Sm1x1*ynd1x2*complexconjugate(VV2x1) + Sm2x1*ynd2x2*complexconjugate(VV2x1) + Sm3x1*ynd3x2*complexconjugate(VV2x1) + Sm1x1*ynd1x3*complexconjugate(VV3x1) + Sm2x1*ynd2x3*complexconjugate(VV3x1) + Sm3x1*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I44a11}')

I44a12 = Parameter(name = 'I44a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x2) + Sm2x1*ynd2x1*complexconjugate(VV1x2) + Sm3x1*ynd3x1*complexconjugate(VV1x2) + Sm1x1*ynd1x2*complexconjugate(VV2x2) + Sm2x1*ynd2x2*complexconjugate(VV2x2) + Sm3x1*ynd3x2*complexconjugate(VV2x2) + Sm1x1*ynd1x3*complexconjugate(VV3x2) + Sm2x1*ynd2x3*complexconjugate(VV3x2) + Sm3x1*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I44a12}')

I44a13 = Parameter(name = 'I44a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x1*ynd1x1*complexconjugate(VV1x3) + Sm2x1*ynd2x1*complexconjugate(VV1x3) + Sm3x1*ynd3x1*complexconjugate(VV1x3) + Sm1x1*ynd1x2*complexconjugate(VV2x3) + Sm2x1*ynd2x2*complexconjugate(VV2x3) + Sm3x1*ynd3x2*complexconjugate(VV2x3) + Sm1x1*ynd1x3*complexconjugate(VV3x3) + Sm2x1*ynd2x3*complexconjugate(VV3x3) + Sm3x1*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I44a13}')

I44a21 = Parameter(name = 'I44a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x1) + Sm2x2*ynd2x1*complexconjugate(VV1x1) + Sm3x2*ynd3x1*complexconjugate(VV1x1) + Sm1x2*ynd1x2*complexconjugate(VV2x1) + Sm2x2*ynd2x2*complexconjugate(VV2x1) + Sm3x2*ynd3x2*complexconjugate(VV2x1) + Sm1x2*ynd1x3*complexconjugate(VV3x1) + Sm2x2*ynd2x3*complexconjugate(VV3x1) + Sm3x2*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I44a21}')

I44a22 = Parameter(name = 'I44a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x2) + Sm2x2*ynd2x1*complexconjugate(VV1x2) + Sm3x2*ynd3x1*complexconjugate(VV1x2) + Sm1x2*ynd1x2*complexconjugate(VV2x2) + Sm2x2*ynd2x2*complexconjugate(VV2x2) + Sm3x2*ynd3x2*complexconjugate(VV2x2) + Sm1x2*ynd1x3*complexconjugate(VV3x2) + Sm2x2*ynd2x3*complexconjugate(VV3x2) + Sm3x2*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I44a22}')

I44a23 = Parameter(name = 'I44a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x2*ynd1x1*complexconjugate(VV1x3) + Sm2x2*ynd2x1*complexconjugate(VV1x3) + Sm3x2*ynd3x1*complexconjugate(VV1x3) + Sm1x2*ynd1x2*complexconjugate(VV2x3) + Sm2x2*ynd2x2*complexconjugate(VV2x3) + Sm3x2*ynd3x2*complexconjugate(VV2x3) + Sm1x2*ynd1x3*complexconjugate(VV3x3) + Sm2x2*ynd2x3*complexconjugate(VV3x3) + Sm3x2*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I44a23}')

I44a31 = Parameter(name = 'I44a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x1) + Sm2x3*ynd2x1*complexconjugate(VV1x1) + Sm3x3*ynd3x1*complexconjugate(VV1x1) + Sm1x3*ynd1x2*complexconjugate(VV2x1) + Sm2x3*ynd2x2*complexconjugate(VV2x1) + Sm3x3*ynd3x2*complexconjugate(VV2x1) + Sm1x3*ynd1x3*complexconjugate(VV3x1) + Sm2x3*ynd2x3*complexconjugate(VV3x1) + Sm3x3*ynd3x3*complexconjugate(VV3x1)',
                   texname = '\\text{I44a31}')

I44a32 = Parameter(name = 'I44a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x2) + Sm2x3*ynd2x1*complexconjugate(VV1x2) + Sm3x3*ynd3x1*complexconjugate(VV1x2) + Sm1x3*ynd1x2*complexconjugate(VV2x2) + Sm2x3*ynd2x2*complexconjugate(VV2x2) + Sm3x3*ynd3x2*complexconjugate(VV2x2) + Sm1x3*ynd1x3*complexconjugate(VV3x2) + Sm2x3*ynd2x3*complexconjugate(VV3x2) + Sm3x3*ynd3x3*complexconjugate(VV3x2)',
                   texname = '\\text{I44a32}')

I44a33 = Parameter(name = 'I44a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Sm1x3*ynd1x1*complexconjugate(VV1x3) + Sm2x3*ynd2x1*complexconjugate(VV1x3) + Sm3x3*ynd3x1*complexconjugate(VV1x3) + Sm1x3*ynd1x2*complexconjugate(VV2x3) + Sm2x3*ynd2x2*complexconjugate(VV2x3) + Sm3x3*ynd3x2*complexconjugate(VV2x3) + Sm1x3*ynd1x3*complexconjugate(VV3x3) + Sm2x3*ynd2x3*complexconjugate(VV3x3) + Sm3x3*ynd3x3*complexconjugate(VV3x3)',
                   texname = '\\text{I44a33}')

I45a11 = Parameter(name = 'I45a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)**2 + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x1) + ynm2x2*complexconjugate(VV2x1)**2 + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x1) + ynm3x3*complexconjugate(VV3x1)**2',
                   texname = '\\text{I45a11}')

I45a12 = Parameter(name = 'I45a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x2) + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x2) + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x2)',
                   texname = '\\text{I45a12}')

I45a13 = Parameter(name = 'I45a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x3) + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x3) + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x3)',
                   texname = '\\text{I45a13}')

I45a21 = Parameter(name = 'I45a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x1) + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x1) + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x2)',
                   texname = '\\text{I45a21}')

I45a22 = Parameter(name = 'I45a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)**2 + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x2)**2 + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x2)**2',
                   texname = '\\text{I45a22}')

I45a23 = Parameter(name = 'I45a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)*complexconjugate(VV1x3) + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x2)*complexconjugate(VV2x3) + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x2)*complexconjugate(VV3x3)',
                   texname = '\\text{I45a23}')

I45a31 = Parameter(name = 'I45a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x1) + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x1) + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x3)',
                   texname = '\\text{I45a31}')

I45a32 = Parameter(name = 'I45a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)*complexconjugate(VV1x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x2) + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x2)*complexconjugate(VV2x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x2) + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x2)*complexconjugate(VV3x3)',
                   texname = '\\text{I45a32}')

I45a33 = Parameter(name = 'I45a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x3)**2 + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x3)**2 + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x3)**2',
                   texname = '\\text{I45a33}')

I46a11 = Parameter(name = 'I46a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)**2 + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x1) + ynm2x2*complexconjugate(VV2x1)**2 + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x1) + ynm3x3*complexconjugate(VV3x1)**2',
                   texname = '\\text{I46a11}')

I46a12 = Parameter(name = 'I46a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x1) + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x1) + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x2)',
                   texname = '\\text{I46a12}')

I46a13 = Parameter(name = 'I46a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x1) + ynm1x2*complexconjugate(VV1x1)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x1) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x1) + ynm1x3*complexconjugate(VV1x1)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x1)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x3)',
                   texname = '\\text{I46a13}')

I46a21 = Parameter(name = 'I46a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x2) + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x2) + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x2)',
                   texname = '\\text{I46a21}')

I46a22 = Parameter(name = 'I46a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)**2 + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x2) + ynm2x2*complexconjugate(VV2x2)**2 + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x2) + ynm3x3*complexconjugate(VV3x2)**2',
                   texname = '\\text{I46a22}')

I46a23 = Parameter(name = 'I46a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)*complexconjugate(VV1x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x2) + ynm1x2*complexconjugate(VV1x2)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x2)*complexconjugate(VV2x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x2) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x2) + ynm1x3*complexconjugate(VV1x2)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x2)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x2)*complexconjugate(VV3x3)',
                   texname = '\\text{I46a23}')

I46a31 = Parameter(name = 'I46a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x1)*complexconjugate(VV1x3) + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x1) + ynm2x1*complexconjugate(VV1x1)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x1)*complexconjugate(VV2x3) + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x1) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x1) + ynm3x1*complexconjugate(VV1x1)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x1)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x1)*complexconjugate(VV3x3)',
                   texname = '\\text{I46a31}')

I46a32 = Parameter(name = 'I46a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x2)*complexconjugate(VV1x3) + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x2) + ynm2x1*complexconjugate(VV1x2)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x2)*complexconjugate(VV2x3) + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x2) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x2) + ynm3x1*complexconjugate(VV1x2)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x2)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x2)*complexconjugate(VV3x3)',
                   texname = '\\text{I46a32}')

I46a33 = Parameter(name = 'I46a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'ynm1x1*complexconjugate(VV1x3)**2 + ynm1x2*complexconjugate(VV1x3)*complexconjugate(VV2x3) + ynm2x1*complexconjugate(VV1x3)*complexconjugate(VV2x3) + ynm2x2*complexconjugate(VV2x3)**2 + ynm1x3*complexconjugate(VV1x3)*complexconjugate(VV3x3) + ynm3x1*complexconjugate(VV1x3)*complexconjugate(VV3x3) + ynm2x3*complexconjugate(VV2x3)*complexconjugate(VV3x3) + ynm3x2*complexconjugate(VV2x3)*complexconjugate(VV3x3) + ynm3x3*complexconjugate(VV3x3)**2',
                   texname = '\\text{I46a33}')

I47a11 = Parameter(name = 'I47a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1**2*ynm1x1 + VV1x1*VV2x1*ynm1x2 + VV1x1*VV3x1*ynm1x3 + VV1x1*VV2x1*ynm2x1 + VV2x1**2*ynm2x2 + VV2x1*VV3x1*ynm2x3 + VV1x1*VV3x1*ynm3x1 + VV2x1*VV3x1*ynm3x2 + VV3x1**2*ynm3x3',
                   texname = '\\text{I47a11}')

I47a12 = Parameter(name = 'I47a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x2*ynm1x1 + VV1x2*VV2x1*ynm1x2 + VV1x2*VV3x1*ynm1x3 + VV1x1*VV2x2*ynm2x1 + VV2x1*VV2x2*ynm2x2 + VV2x2*VV3x1*ynm2x3 + VV1x1*VV3x2*ynm3x1 + VV2x1*VV3x2*ynm3x2 + VV3x1*VV3x2*ynm3x3',
                   texname = '\\text{I47a12}')

I47a13 = Parameter(name = 'I47a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x3*ynm1x1 + VV1x3*VV2x1*ynm1x2 + VV1x3*VV3x1*ynm1x3 + VV1x1*VV2x3*ynm2x1 + VV2x1*VV2x3*ynm2x2 + VV2x3*VV3x1*ynm2x3 + VV1x1*VV3x3*ynm3x1 + VV2x1*VV3x3*ynm3x2 + VV3x1*VV3x3*ynm3x3',
                   texname = '\\text{I47a13}')

I47a21 = Parameter(name = 'I47a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x2*ynm1x1 + VV1x1*VV2x2*ynm1x2 + VV1x1*VV3x2*ynm1x3 + VV1x2*VV2x1*ynm2x1 + VV2x1*VV2x2*ynm2x2 + VV2x1*VV3x2*ynm2x3 + VV1x2*VV3x1*ynm3x1 + VV2x2*VV3x1*ynm3x2 + VV3x1*VV3x2*ynm3x3',
                   texname = '\\text{I47a21}')

I47a22 = Parameter(name = 'I47a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2**2*ynm1x1 + VV1x2*VV2x2*ynm1x2 + VV1x2*VV3x2*ynm1x3 + VV1x2*VV2x2*ynm2x1 + VV2x2**2*ynm2x2 + VV2x2*VV3x2*ynm2x3 + VV1x2*VV3x2*ynm3x1 + VV2x2*VV3x2*ynm3x2 + VV3x2**2*ynm3x3',
                   texname = '\\text{I47a22}')

I47a23 = Parameter(name = 'I47a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*VV1x3*ynm1x1 + VV1x3*VV2x2*ynm1x2 + VV1x3*VV3x2*ynm1x3 + VV1x2*VV2x3*ynm2x1 + VV2x2*VV2x3*ynm2x2 + VV2x3*VV3x2*ynm2x3 + VV1x2*VV3x3*ynm3x1 + VV2x2*VV3x3*ynm3x2 + VV3x2*VV3x3*ynm3x3',
                   texname = '\\text{I47a23}')

I47a31 = Parameter(name = 'I47a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x3*ynm1x1 + VV1x1*VV2x3*ynm1x2 + VV1x1*VV3x3*ynm1x3 + VV1x3*VV2x1*ynm2x1 + VV2x1*VV2x3*ynm2x2 + VV2x1*VV3x3*ynm2x3 + VV1x3*VV3x1*ynm3x1 + VV2x3*VV3x1*ynm3x2 + VV3x1*VV3x3*ynm3x3',
                   texname = '\\text{I47a31}')

I47a32 = Parameter(name = 'I47a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*VV1x3*ynm1x1 + VV1x2*VV2x3*ynm1x2 + VV1x2*VV3x3*ynm1x3 + VV1x3*VV2x2*ynm2x1 + VV2x2*VV2x3*ynm2x2 + VV2x2*VV3x3*ynm2x3 + VV1x3*VV3x2*ynm3x1 + VV2x3*VV3x2*ynm3x2 + VV3x2*VV3x3*ynm3x3',
                   texname = '\\text{I47a32}')

I47a33 = Parameter(name = 'I47a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x3**2*ynm1x1 + VV1x3*VV2x3*ynm1x2 + VV1x3*VV3x3*ynm1x3 + VV1x3*VV2x3*ynm2x1 + VV2x3**2*ynm2x2 + VV2x3*VV3x3*ynm2x3 + VV1x3*VV3x3*ynm3x1 + VV2x3*VV3x3*ynm3x2 + VV3x3**2*ynm3x3',
                   texname = '\\text{I47a33}')

I48a11 = Parameter(name = 'I48a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1**2*ynm1x1 + VV1x1*VV2x1*ynm1x2 + VV1x1*VV3x1*ynm1x3 + VV1x1*VV2x1*ynm2x1 + VV2x1**2*ynm2x2 + VV2x1*VV3x1*ynm2x3 + VV1x1*VV3x1*ynm3x1 + VV2x1*VV3x1*ynm3x2 + VV3x1**2*ynm3x3',
                   texname = '\\text{I48a11}')

I48a12 = Parameter(name = 'I48a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x2*ynm1x1 + VV1x1*VV2x2*ynm1x2 + VV1x1*VV3x2*ynm1x3 + VV1x2*VV2x1*ynm2x1 + VV2x1*VV2x2*ynm2x2 + VV2x1*VV3x2*ynm2x3 + VV1x2*VV3x1*ynm3x1 + VV2x2*VV3x1*ynm3x2 + VV3x1*VV3x2*ynm3x3',
                   texname = '\\text{I48a12}')

I48a13 = Parameter(name = 'I48a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x3*ynm1x1 + VV1x1*VV2x3*ynm1x2 + VV1x1*VV3x3*ynm1x3 + VV1x3*VV2x1*ynm2x1 + VV2x1*VV2x3*ynm2x2 + VV2x1*VV3x3*ynm2x3 + VV1x3*VV3x1*ynm3x1 + VV2x3*VV3x1*ynm3x2 + VV3x1*VV3x3*ynm3x3',
                   texname = '\\text{I48a13}')

I48a21 = Parameter(name = 'I48a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x2*ynm1x1 + VV1x2*VV2x1*ynm1x2 + VV1x2*VV3x1*ynm1x3 + VV1x1*VV2x2*ynm2x1 + VV2x1*VV2x2*ynm2x2 + VV2x2*VV3x1*ynm2x3 + VV1x1*VV3x2*ynm3x1 + VV2x1*VV3x2*ynm3x2 + VV3x1*VV3x2*ynm3x3',
                   texname = '\\text{I48a21}')

I48a22 = Parameter(name = 'I48a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2**2*ynm1x1 + VV1x2*VV2x2*ynm1x2 + VV1x2*VV3x2*ynm1x3 + VV1x2*VV2x2*ynm2x1 + VV2x2**2*ynm2x2 + VV2x2*VV3x2*ynm2x3 + VV1x2*VV3x2*ynm3x1 + VV2x2*VV3x2*ynm3x2 + VV3x2**2*ynm3x3',
                   texname = '\\text{I48a22}')

I48a23 = Parameter(name = 'I48a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*VV1x3*ynm1x1 + VV1x2*VV2x3*ynm1x2 + VV1x2*VV3x3*ynm1x3 + VV1x3*VV2x2*ynm2x1 + VV2x2*VV2x3*ynm2x2 + VV2x2*VV3x3*ynm2x3 + VV1x3*VV3x2*ynm3x1 + VV2x3*VV3x2*ynm3x2 + VV3x2*VV3x3*ynm3x3',
                   texname = '\\text{I48a23}')

I48a31 = Parameter(name = 'I48a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x1*VV1x3*ynm1x1 + VV1x3*VV2x1*ynm1x2 + VV1x3*VV3x1*ynm1x3 + VV1x1*VV2x3*ynm2x1 + VV2x1*VV2x3*ynm2x2 + VV2x3*VV3x1*ynm2x3 + VV1x1*VV3x3*ynm3x1 + VV2x1*VV3x3*ynm3x2 + VV3x1*VV3x3*ynm3x3',
                   texname = '\\text{I48a31}')

I48a32 = Parameter(name = 'I48a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x2*VV1x3*ynm1x1 + VV1x3*VV2x2*ynm1x2 + VV1x3*VV3x2*ynm1x3 + VV1x2*VV2x3*ynm2x1 + VV2x2*VV2x3*ynm2x2 + VV2x3*VV3x2*ynm2x3 + VV1x2*VV3x3*ynm3x1 + VV2x2*VV3x3*ynm3x2 + VV3x2*VV3x3*ynm3x3',
                   texname = '\\text{I48a32}')

I48a33 = Parameter(name = 'I48a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'VV1x3**2*ynm1x1 + VV1x3*VV2x3*ynm1x2 + VV1x3*VV3x3*ynm1x3 + VV1x3*VV2x3*ynm2x1 + VV2x3**2*ynm2x2 + VV2x3*VV3x3*ynm2x3 + VV1x3*VV3x3*ynm3x1 + VV2x3*VV3x3*ynm3x2 + VV3x3**2*ynm3x3',
                   texname = '\\text{I48a33}')

I5a11 = Parameter(name = 'I5a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*complexconjugate(VV1x1) + Tm2x1*complexconjugate(VV2x1) + Tm3x1*complexconjugate(VV3x1)',
                  texname = '\\text{I5a11}')

I5a12 = Parameter(name = 'I5a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*complexconjugate(VV1x1) + Tm2x2*complexconjugate(VV2x1) + Tm3x2*complexconjugate(VV3x1)',
                  texname = '\\text{I5a12}')

I5a13 = Parameter(name = 'I5a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*complexconjugate(VV1x1) + Tm2x3*complexconjugate(VV2x1) + Tm3x3*complexconjugate(VV3x1)',
                  texname = '\\text{I5a13}')

I5a21 = Parameter(name = 'I5a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*complexconjugate(VV1x2) + Tm2x1*complexconjugate(VV2x2) + Tm3x1*complexconjugate(VV3x2)',
                  texname = '\\text{I5a21}')

I5a22 = Parameter(name = 'I5a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*complexconjugate(VV1x2) + Tm2x2*complexconjugate(VV2x2) + Tm3x2*complexconjugate(VV3x2)',
                  texname = '\\text{I5a22}')

I5a23 = Parameter(name = 'I5a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*complexconjugate(VV1x2) + Tm2x3*complexconjugate(VV2x2) + Tm3x3*complexconjugate(VV3x2)',
                  texname = '\\text{I5a23}')

I5a31 = Parameter(name = 'I5a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*complexconjugate(VV1x3) + Tm2x1*complexconjugate(VV2x3) + Tm3x1*complexconjugate(VV3x3)',
                  texname = '\\text{I5a31}')

I5a32 = Parameter(name = 'I5a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*complexconjugate(VV1x3) + Tm2x2*complexconjugate(VV2x3) + Tm3x2*complexconjugate(VV3x3)',
                  texname = '\\text{I5a32}')

I5a33 = Parameter(name = 'I5a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*complexconjugate(VV1x3) + Tm2x3*complexconjugate(VV2x3) + Tm3x3*complexconjugate(VV3x3)',
                  texname = '\\text{I5a33}')

I6a11 = Parameter(name = 'I6a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*U11 + Sm2x1*U21 + Sm3x1*U31',
                  texname = '\\text{I6a11}')

I6a12 = Parameter(name = 'I6a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*U12 + Sm2x1*U22 + Sm3x1*U32',
                  texname = '\\text{I6a12}')

I6a13 = Parameter(name = 'I6a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*U13 + Sm2x1*U23 + Sm3x1*U33',
                  texname = '\\text{I6a13}')

I6a21 = Parameter(name = 'I6a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*U11 + Sm2x2*U21 + Sm3x2*U31',
                  texname = '\\text{I6a21}')

I6a22 = Parameter(name = 'I6a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*U12 + Sm2x2*U22 + Sm3x2*U32',
                  texname = '\\text{I6a22}')

I6a23 = Parameter(name = 'I6a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*U13 + Sm2x2*U23 + Sm3x2*U33',
                  texname = '\\text{I6a23}')

I6a31 = Parameter(name = 'I6a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*U11 + Sm2x3*U21 + Sm3x3*U31',
                  texname = '\\text{I6a31}')

I6a32 = Parameter(name = 'I6a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*U12 + Sm2x3*U22 + Sm3x3*U32',
                  texname = '\\text{I6a32}')

I6a33 = Parameter(name = 'I6a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*U13 + Sm2x3*U23 + Sm3x3*U33',
                  texname = '\\text{I6a33}')

I7a11 = Parameter(name = 'I7a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*complexconjugate(U11) + Sm2x1*complexconjugate(U21) + Sm3x1*complexconjugate(U31)',
                  texname = '\\text{I7a11}')

I7a12 = Parameter(name = 'I7a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*complexconjugate(U11) + Sm2x2*complexconjugate(U21) + Sm3x2*complexconjugate(U31)',
                  texname = '\\text{I7a12}')

I7a13 = Parameter(name = 'I7a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*complexconjugate(U11) + Sm2x3*complexconjugate(U21) + Sm3x3*complexconjugate(U31)',
                  texname = '\\text{I7a13}')

I7a21 = Parameter(name = 'I7a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*complexconjugate(U12) + Sm2x1*complexconjugate(U22) + Sm3x1*complexconjugate(U32)',
                  texname = '\\text{I7a21}')

I7a22 = Parameter(name = 'I7a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*complexconjugate(U12) + Sm2x2*complexconjugate(U22) + Sm3x2*complexconjugate(U32)',
                  texname = '\\text{I7a22}')

I7a23 = Parameter(name = 'I7a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*complexconjugate(U12) + Sm2x3*complexconjugate(U22) + Sm3x3*complexconjugate(U32)',
                  texname = '\\text{I7a23}')

I7a31 = Parameter(name = 'I7a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x1*complexconjugate(U13) + Sm2x1*complexconjugate(U23) + Sm3x1*complexconjugate(U33)',
                  texname = '\\text{I7a31}')

I7a32 = Parameter(name = 'I7a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x2*complexconjugate(U13) + Sm2x2*complexconjugate(U23) + Sm3x2*complexconjugate(U33)',
                  texname = '\\text{I7a32}')

I7a33 = Parameter(name = 'I7a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Sm1x3*complexconjugate(U13) + Sm2x3*complexconjugate(U23) + Sm3x3*complexconjugate(U33)',
                  texname = '\\text{I7a33}')

I8a11 = Parameter(name = 'I8a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*VV1x1 + Tm2x1*VV2x1 + Tm3x1*VV3x1',
                  texname = '\\text{I8a11}')

I8a12 = Parameter(name = 'I8a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*VV1x2 + Tm2x1*VV2x2 + Tm3x1*VV3x2',
                  texname = '\\text{I8a12}')

I8a13 = Parameter(name = 'I8a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*VV1x3 + Tm2x1*VV2x3 + Tm3x1*VV3x3',
                  texname = '\\text{I8a13}')

I8a21 = Parameter(name = 'I8a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*VV1x1 + Tm2x2*VV2x1 + Tm3x2*VV3x1',
                  texname = '\\text{I8a21}')

I8a22 = Parameter(name = 'I8a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*VV1x2 + Tm2x2*VV2x2 + Tm3x2*VV3x2',
                  texname = '\\text{I8a22}')

I8a23 = Parameter(name = 'I8a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*VV1x3 + Tm2x2*VV2x3 + Tm3x2*VV3x3',
                  texname = '\\text{I8a23}')

I8a31 = Parameter(name = 'I8a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*VV1x1 + Tm2x3*VV2x1 + Tm3x3*VV3x1',
                  texname = '\\text{I8a31}')

I8a32 = Parameter(name = 'I8a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*VV1x2 + Tm2x3*VV2x2 + Tm3x3*VV3x2',
                  texname = '\\text{I8a32}')

I8a33 = Parameter(name = 'I8a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3*VV1x3 + Tm2x3*VV2x3 + Tm3x3*VV3x3',
                  texname = '\\text{I8a33}')

I9a11 = Parameter(name = 'I9a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1**2 + Tm2x1**2 + Tm3x1**2',
                  texname = '\\text{I9a11}')

I9a12 = Parameter(name = 'I9a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*Tm1x2 + Tm2x1*Tm2x2 + Tm3x1*Tm3x2',
                  texname = '\\text{I9a12}')

I9a13 = Parameter(name = 'I9a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*Tm1x3 + Tm2x1*Tm2x3 + Tm3x1*Tm3x3',
                  texname = '\\text{I9a13}')

I9a21 = Parameter(name = 'I9a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*Tm1x2 + Tm2x1*Tm2x2 + Tm3x1*Tm3x2',
                  texname = '\\text{I9a21}')

I9a22 = Parameter(name = 'I9a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2**2 + Tm2x2**2 + Tm3x2**2',
                  texname = '\\text{I9a22}')

I9a23 = Parameter(name = 'I9a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*Tm1x3 + Tm2x2*Tm2x3 + Tm3x2*Tm3x3',
                  texname = '\\text{I9a23}')

I9a31 = Parameter(name = 'I9a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x1*Tm1x3 + Tm2x1*Tm2x3 + Tm3x1*Tm3x3',
                  texname = '\\text{I9a31}')

I9a32 = Parameter(name = 'I9a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x2*Tm1x3 + Tm2x2*Tm2x3 + Tm3x2*Tm3x3',
                  texname = '\\text{I9a32}')

I9a33 = Parameter(name = 'I9a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Tm1x3**2 + Tm2x3**2 + Tm3x3**2',
                  texname = '\\text{I9a33}')

