Requestor: Hector de la Torre 
Contents:  This model is an extension of the SM, including additional interaction of fermions to a W' boson following an effective Lagrangian. W' only couples to 3rd generation quarks. NLO capable. Modified from model already in database (WEff_UFO)
Authors: Reinhard Schwienhorst and Jason Gombas. Original LO model by Benjamin Fuks and Julien Donini
Paper: https://arxiv.org/abs/hep-ph/0207290 (Lagrangian)
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/Wprime  (Original LO model)
