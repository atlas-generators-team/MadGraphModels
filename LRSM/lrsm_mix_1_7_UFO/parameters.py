# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Tue 28 Nov 2023 12:50:07



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
s12 = Parameter(name = 's12',
                nature = 'external',
                type = 'real',
                value = 0.221,
                texname = 's_{12}',
                lhablock = 'CKMBLOCK',
                lhacode = [ 1 ])

s23 = Parameter(name = 's23',
                nature = 'external',
                type = 'real',
                value = 0.041,
                texname = 's_{23}',
                lhablock = 'CKMBLOCK',
                lhacode = [ 2 ])

s13 = Parameter(name = 's13',
                nature = 'external',
                type = 'real',
                value = 0.0035,
                texname = 's_{13}',
                lhablock = 'CKMBLOCK',
                lhacode = [ 3 ])

lambda2 = Parameter(name = 'lambda2',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\lambda _2',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 2 ])

lambda3 = Parameter(name = 'lambda3',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\lambda _3',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 3 ])

lambda4 = Parameter(name = 'lambda4',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\lambda _4',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 4 ])

rho4 = Parameter(name = 'rho4',
                 nature = 'external',
                 type = 'real',
                 value = 0,
                 texname = '\\rho _4',
                 lhablock = 'HIGGSBLOCK',
                 lhacode = [ 8 ])

alpha2 = Parameter(name = 'alpha2',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\alpha _2',
                   lhablock = 'HIGGSBLOCK',
                   lhacode = [ 10 ])

VKe = Parameter(name = 'VKe',
                nature = 'external',
                type = 'real',
                value = 0,
                texname = '\\text{KLRmix}_e',
                lhablock = 'KLRBLOCK',
                lhacode = [ 1 ])

VKmu = Parameter(name = 'VKmu',
                 nature = 'external',
                 type = 'real',
                 value = 0,
                 texname = '\\text{KLRmix}_{\\mu }',
                 lhablock = 'KLRBLOCK',
                 lhacode = [ 2 ])

VKta = Parameter(name = 'VKta',
                 nature = 'external',
                 type = 'real',
                 value = 0,
                 texname = '\\text{KLRmix}_{\\text{ta}}',
                 lhablock = 'KLRBLOCK',
                 lhacode = [ 3 ])

Wl11 = Parameter(name = 'Wl11',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{Wl}_{11}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 1 ])

Wl22 = Parameter(name = 'Wl22',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{Wl}_{22}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 2 ])

Wl33 = Parameter(name = 'Wl33',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{Wl}_{33}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 3 ])

WU11 = Parameter(name = 'WU11',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WU}_{11}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 4 ])

WU22 = Parameter(name = 'WU22',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WU}_{22}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 5 ])

WU33 = Parameter(name = 'WU33',
                 nature = 'external',
                 type = 'real',
                 value = -1,
                 texname = '\\text{WU}_{33}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 6 ])

WD11 = Parameter(name = 'WD11',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WD}_{11}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 7 ])

WD22 = Parameter(name = 'WD22',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WD}_{22}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 8 ])

WD33 = Parameter(name = 'WD33',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WD}_{33}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 9 ])

sth1 = Parameter(name = 'sth1',
                 nature = 'external',
                 type = 'real',
                 value = 0.05,
                 texname = 's_{\\theta _1}',
                 lhablock = 'MIXINGBLOCK',
                 lhacode = [ 10 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

sw2 = Parameter(name = 'sw2',
                nature = 'external',
                type = 'real',
                value = 0.23126,
                texname = '\\text{sw2}',
                lhablock = 'SMINPUTS',
                lhacode = [ 4 ])

tanb = Parameter(name = 'tanb',
                 nature = 'external',
                 type = 'real',
                 value = 0.15,
                 texname = '\\text{tanb}',
                 lhablock = 'VEVS',
                 lhacode = [ 1 ])

vL = Parameter(name = 'vL',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{vL}',
               lhablock = 'VEVS',
               lhacode = [ 2 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.385,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MW2 = Parameter(name = 'MW2',
                nature = 'external',
                type = 'real',
                value = 3000.,
                texname = '\\text{MW2}',
                lhablock = 'MASS',
                lhacode = [ 34 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.0005110000000000001,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

Mmu = Parameter(name = 'Mmu',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{Mmu}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

Mta = Parameter(name = 'Mta',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{Mta}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.0025499999999999997,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

Mv1 = Parameter(name = 'Mv1',
                nature = 'external',
                type = 'real',
                value = 1.e-10,
                texname = '\\text{Mv1}',
                lhablock = 'MASS',
                lhacode = [ 12 ])

Mv2 = Parameter(name = 'Mv2',
                nature = 'external',
                type = 'real',
                value = 1.e-10,
                texname = '\\text{Mv2}',
                lhablock = 'MASS',
                lhacode = [ 14 ])

Mv3 = Parameter(name = 'Mv3',
                nature = 'external',
                type = 'real',
                value = 1.e-10,
                texname = '\\text{Mv3}',
                lhablock = 'MASS',
                lhacode = [ 16 ])

MN1 = Parameter(name = 'MN1',
                nature = 'external',
                type = 'real',
                value = 2000,
                texname = '\\text{MN1}',
                lhablock = 'MASS',
                lhacode = [ 9900012 ])

MN2 = Parameter(name = 'MN2',
                nature = 'external',
                type = 'real',
                value = 20,
                texname = '\\text{MN2}',
                lhablock = 'MASS',
                lhacode = [ 9900014 ])

MN3 = Parameter(name = 'MN3',
                nature = 'external',
                type = 'real',
                value = 2000,
                texname = '\\text{MN3}',
                lhablock = 'MASS',
                lhacode = [ 9900016 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125.5,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MH01 = Parameter(name = 'MH01',
                 nature = 'external',
                 type = 'real',
                 value = 16000,
                 texname = '\\text{MH01}',
                 lhablock = 'MASS',
                 lhacode = [ 35 ])

MH02 = Parameter(name = 'MH02',
                 nature = 'external',
                 type = 'real',
                 value = 60,
                 texname = '\\text{MH02}',
                 lhablock = 'MASS',
                 lhacode = [ 9900043 ])

MH03 = Parameter(name = 'MH03',
                 nature = 'external',
                 type = 'real',
                 value = 6000,
                 texname = '\\text{MH03}',
                 lhablock = 'MASS',
                 lhacode = [ 44 ])

MHPPR = Parameter(name = 'MHPPR',
                  nature = 'external',
                  type = 'real',
                  value = 1500,
                  texname = '\\text{MHPPR}',
                  lhablock = 'MASS',
                  lhacode = [ 62 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WW2 = Parameter(name = 'WW2',
                nature = 'external',
                type = 'real',
                value = 101.6764,
                texname = '\\text{WW2}',
                lhablock = 'DECAY',
                lhacode = [ 34 ])

WZ2 = Parameter(name = 'WZ2',
                nature = 'external',
                type = 'real',
                value = 80,
                texname = '\\text{WZ2}',
                lhablock = 'DECAY',
                lhacode = [ 32 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WN1 = Parameter(name = 'WN1',
                nature = 'external',
                type = 'real',
                value = 6.583e-8,
                texname = '\\text{WN1}',
                lhablock = 'DECAY',
                lhacode = [ 9900012 ])

WN2 = Parameter(name = 'WN2',
                nature = 'external',
                type = 'real',
                value = 6.583e-8,
                texname = '\\text{WN2}',
                lhablock = 'DECAY',
                lhacode = [ 9900014 ])

WN3 = Parameter(name = 'WN3',
                nature = 'external',
                type = 'real',
                value = 6.583e-8,
                texname = '\\text{WN3}',
                lhablock = 'DECAY',
                lhacode = [ 9900016 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.0057,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WH01 = Parameter(name = 'WH01',
                 nature = 'external',
                 type = 'real',
                 value = 0.00575308848,
                 texname = '\\text{WH01}',
                 lhablock = 'DECAY',
                 lhacode = [ 35 ])

WH02 = Parameter(name = 'WH02',
                 nature = 'external',
                 type = 'real',
                 value = 0.00575308848,
                 texname = '\\text{WH02}',
                 lhablock = 'DECAY',
                 lhacode = [ 9900043 ])

WH03 = Parameter(name = 'WH03',
                 nature = 'external',
                 type = 'real',
                 value = 0.00575308848,
                 texname = '\\text{WH03}',
                 lhablock = 'DECAY',
                 lhacode = [ 44 ])

WHP1 = Parameter(name = 'WHP1',
                 nature = 'external',
                 type = 'real',
                 value = 0.006,
                 texname = '\\text{WHP1}',
                 lhablock = 'DECAY',
                 lhacode = [ 37 ])

WHP2 = Parameter(name = 'WHP2',
                 nature = 'external',
                 type = 'real',
                 value = 0.006,
                 texname = '\\text{WHP2}',
                 lhablock = 'DECAY',
                 lhacode = [ 38 ])

WHPPL = Parameter(name = 'WHPPL',
                  nature = 'external',
                  type = 'real',
                  value = 0.007,
                  texname = '\\text{WHPPL}',
                  lhablock = 'DECAY',
                  lhacode = [ 61 ])

WHPPR = Parameter(name = 'WHPPR',
                  nature = 'external',
                  type = 'real',
                  value = 0.007,
                  texname = '\\text{WHPPR}',
                  lhablock = 'DECAY',
                  lhacode = [ 62 ])

WA01 = Parameter(name = 'WA01',
                 nature = 'external',
                 type = 'real',
                 value = 0.006,
                 texname = '\\text{WA01}',
                 lhablock = 'DECAY',
                 lhacode = [ 36 ])

WA02 = Parameter(name = 'WA02',
                 nature = 'external',
                 type = 'real',
                 value = 0.006,
                 texname = '\\text{WA02}',
                 lhablock = 'DECAY',
                 lhacode = [ 45 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '1/(2**0.25*cmath.sqrt(Gf))',
                texname = '\\text{Vev}')

cth1 = Parameter(name = 'cth1',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(1 - sth1**2)',
                 texname = 'c_{\\theta _1}')

MA02 = Parameter(name = 'MA02',
                 nature = 'internal',
                 type = 'real',
                 value = 'MH03',
                 texname = 'M_{\\text{A02}}')

yML1x1 = Parameter(name = 'yML1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'Me',
                   texname = '\\text{yML1x1}')

yML2x2 = Parameter(name = 'yML2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'Mmu',
                   texname = '\\text{yML2x2}')

yML3x3 = Parameter(name = 'yML3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'Mta',
                   texname = '\\text{yML3x3}')

yNL1x1 = Parameter(name = 'yNL1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'Mv1',
                   texname = '\\text{yNL1x1}')

yNL2x2 = Parameter(name = 'yNL2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'Mv2',
                   texname = '\\text{yNL2x2}')

yNL3x3 = Parameter(name = 'yNL3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'Mv3',
                   texname = '\\text{yNL3x3}')

yNL4x4 = Parameter(name = 'yNL4x4',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN1',
                   texname = '\\text{yNL4x4}')

yNL5x5 = Parameter(name = 'yNL5x5',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN2',
                   texname = '\\text{yNL5x5}')

yNL6x6 = Parameter(name = 'yNL6x6',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN3',
                   texname = '\\text{yNL6x6}')

yMU1x1 = Parameter(name = 'yMU1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'MU',
                   texname = '\\text{yMU1x1}')

yMU2x2 = Parameter(name = 'yMU2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'MC',
                   texname = '\\text{yMU2x2}')

yMU3x3 = Parameter(name = 'yMU3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'MT',
                   texname = '\\text{yMU3x3}')

yDO1x1 = Parameter(name = 'yDO1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'MD',
                   texname = '\\text{yDO1x1}')

yDO2x2 = Parameter(name = 'yDO2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'MS',
                   texname = '\\text{yDO2x2}')

yDO3x3 = Parameter(name = 'yDO3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'MB',
                   texname = '\\text{yDO3x3}')

CKML1x1 = Parameter(name = 'CKML1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'cmath.sqrt(1 - s12**2)*cmath.sqrt(1 - s13**2)',
                    texname = '\\text{CKML1x1}')

CKML1x2 = Parameter(name = 'CKML1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 's12*cmath.sqrt(1 - s13**2)',
                    texname = '\\text{CKML1x2}')

CKML1x3 = Parameter(name = 'CKML1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 's13',
                    texname = '\\text{CKML1x3}')

CKML2x1 = Parameter(name = 'CKML2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(s13*s23*cmath.sqrt(1 - s12**2)) - s12*cmath.sqrt(1 - s23**2)',
                    texname = '\\text{CKML2x1}')

CKML2x2 = Parameter(name = 'CKML2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(s12*s13*s23) + cmath.sqrt(1 - s12**2)*cmath.sqrt(1 - s23**2)',
                    texname = '\\text{CKML2x2}')

CKML2x3 = Parameter(name = 'CKML2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 's23*cmath.sqrt(1 - s13**2)',
                    texname = '\\text{CKML2x3}')

CKML3x1 = Parameter(name = 'CKML3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 's12*s23 - s13*cmath.sqrt(1 - s12**2)*cmath.sqrt(1 - s23**2)',
                    texname = '\\text{CKML3x1}')

CKML3x2 = Parameter(name = 'CKML3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(s23*cmath.sqrt(1 - s12**2)) - s12*s13*cmath.sqrt(1 - s23**2)',
                    texname = '\\text{CKML3x2}')

CKML3x3 = Parameter(name = 'CKML3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'cmath.sqrt(1 - s13**2)*cmath.sqrt(1 - s23**2)',
                    texname = '\\text{CKML3x3}')

Wl1x1 = Parameter(name = 'Wl1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'Wl11',
                  texname = '\\text{Wl1x1}')

Wl2x2 = Parameter(name = 'Wl2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'Wl22',
                  texname = '\\text{Wl2x2}')

Wl3x3 = Parameter(name = 'Wl3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'Wl33',
                  texname = '\\text{Wl3x3}')

WU1x1 = Parameter(name = 'WU1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'WU11',
                  texname = '\\text{WU1x1}')

WU2x2 = Parameter(name = 'WU2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'WU22',
                  texname = '\\text{WU2x2}')

WU3x3 = Parameter(name = 'WU3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'WU33',
                  texname = '\\text{WU3x3}')

WD1x1 = Parameter(name = 'WD1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'WD11',
                  texname = '\\text{WD1x1}')

WD2x2 = Parameter(name = 'WD2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'WD22',
                  texname = '\\text{WD2x2}')

WD3x3 = Parameter(name = 'WD3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'WD33',
                  texname = '\\text{WD3x3}')

KL1x1 = Parameter(name = 'KL1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KL1x1}')

KL2x2 = Parameter(name = 'KL2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KL2x2}')

KL3x3 = Parameter(name = 'KL3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KL3x3}')

KL4x1 = Parameter(name = 'KL4x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'VKe',
                  texname = '\\text{KL4x1}')

KL5x2 = Parameter(name = 'KL5x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'VKmu',
                  texname = '\\text{KL5x2}')

KL6x3 = Parameter(name = 'KL6x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'VKta',
                  texname = '\\text{KL6x3}')

KR1x1 = Parameter(name = 'KR1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-VKe',
                  texname = '\\text{KR1x1}')

KR2x2 = Parameter(name = 'KR2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-VKmu',
                  texname = '\\text{KR2x2}')

KR3x3 = Parameter(name = 'KR3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-VKta',
                  texname = '\\text{KR3x3}')

KR4x1 = Parameter(name = 'KR4x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR4x1}')

KR5x2 = Parameter(name = 'KR5x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR5x2}')

KR6x3 = Parameter(name = 'KR6x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR6x3}')

CKMR1x1 = Parameter(name = 'CKMR1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x1*WD1x1*WU1x1',
                    texname = '\\text{CKMR1x1}')

CKMR1x2 = Parameter(name = 'CKMR1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x2*WD2x2*WU1x1',
                    texname = '\\text{CKMR1x2}')

CKMR1x3 = Parameter(name = 'CKMR1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x3*WD3x3*WU1x1',
                    texname = '\\text{CKMR1x3}')

CKMR2x1 = Parameter(name = 'CKMR2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x1*WD1x1*WU2x2',
                    texname = '\\text{CKMR2x1}')

CKMR2x2 = Parameter(name = 'CKMR2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x2*WD2x2*WU2x2',
                    texname = '\\text{CKMR2x2}')

CKMR2x3 = Parameter(name = 'CKMR2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x3*WD3x3*WU2x2',
                    texname = '\\text{CKMR2x3}')

CKMR3x1 = Parameter(name = 'CKMR3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x1*WD1x1*WU3x3',
                    texname = '\\text{CKMR3x1}')

CKMR3x2 = Parameter(name = 'CKMR3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x2*WD2x2*WU3x3',
                    texname = '\\text{CKMR3x2}')

CKMR3x3 = Parameter(name = 'CKMR3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x3*WD3x3*WU3x3',
                    texname = '\\text{CKMR3x3}')

MZ2 = Parameter(name = 'MZ2',
                nature = 'internal',
                type = 'real',
                value = '(cw*MW2*cmath.sqrt(2))/cmath.sqrt(1 - 2*sw**2)',
                texname = 'M_{\\text{Z2}}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

GD = Parameter(name = 'GD',
               nature = 'internal',
               type = 'real',
               value = '-0.5*(gs**2*MT**4*(MH02**2/MT**2 + (-4 + MH02**2/MT**2)*cmath.atan(1/cmath.sqrt(-1 + (4*MT**2)/MH02**2))**2))/(cmath.pi**2*MH02**4*vev)',
               texname = 'G_D')

GH = Parameter(name = 'GH',
               nature = 'internal',
               type = 'real',
               value = '-0.5*(gs**2*MT**4*(MH**2/MT**2 + (-4 + MH**2/MT**2)*cmath.atan(1/cmath.sqrt(-1 + (4*MT**2)/MH**2))**2))/(cmath.pi**2*MH**4*vev)',
               texname = 'G_H')

k1 = Parameter(name = 'k1',
               nature = 'internal',
               type = 'real',
               value = 'vev/cmath.sqrt(1 + tanb**2)',
               texname = '\\text{k1}')

k2 = Parameter(name = 'k2',
               nature = 'internal',
               type = 'real',
               value = '(tanb*vev)/cmath.sqrt(1 + tanb**2)',
               texname = '\\text{k2}')

eps = Parameter(name = 'eps',
                nature = 'internal',
                type = 'real',
                value = '(2*k1*k2)/vev**2',
                texname = '\\text{eps}')

AD = Parameter(name = 'AD',
               nature = 'internal',
               type = 'real',
               value = '(47*ee**2*(1 - (2*MH02**4)/(987.*MT**4) - (14*MH02**2)/(705.*MT**2) + (213*MH02**12)/(2.634632e7*MW**12) + (5*MH02**10)/(119756.*MW**10) + (41*MH02**8)/(180950.*MW**8) + (87*MH02**6)/(65800.*MW**6) + (57*MH02**4)/(6580.*MW**4) + (33*MH02**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
               texname = 'A_D')

AH = Parameter(name = 'AH',
               nature = 'internal',
               type = 'real',
               value = '(47*ee**2*(1 - (2*MH**4)/(987.*MT**4) - (14*MH**2)/(705.*MT**2) + (213*MH**12)/(2.634632e7*MW**12) + (5*MH**10)/(119756.*MW**10) + (41*MH**8)/(180950.*MW**8) + (87*MH**6)/(65800.*MW**6) + (57*MH**4)/(6580.*MW**4) + (33*MH**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
               texname = 'A_H')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cmath.sqrt(1 - 2*sw**2)',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

lambda1 = Parameter(name = 'lambda1',
                    nature = 'internal',
                    type = 'real',
                    value = '(gw**2*(MH**2 + MH02**2 - (-MH**2 + MH02**2)*(cth1**2 - sth1**2)))/(16.*MW**2)',
                    texname = '\\lambda _1')

rho1 = Parameter(name = 'rho1',
                 nature = 'internal',
                 type = 'real',
                 value = '(gw**2*(MH**2 + MH02**2 + (-MH**2 + MH02**2)*(cth1**2 - sth1**2)))/(8.*MW2**2)',
                 texname = '\\rho _1')

vR = Parameter(name = 'vR',
               nature = 'internal',
               type = 'real',
               value = '(MW2*cmath.sqrt(2))/gw',
               texname = 'v_R')

alpha3 = Parameter(name = 'alpha3',
                   nature = 'internal',
                   type = 'real',
                   value = '(2*MH01**2*(1 - tanb**2))/((1 + tanb**2)*vR**2)',
                   texname = '\\alpha _3')

rho3 = Parameter(name = 'rho3',
                 nature = 'internal',
                 type = 'real',
                 value = '2*rho1 + (2*MH03**2)/vR**2',
                 texname = '\\rho _3')

sphi = Parameter(name = 'sphi',
                 nature = 'internal',
                 type = 'real',
                 value = '-0.25*((1 - 2*sw**2)**1.5*vev**2)/(cw**4*vR**2)',
                 texname = 's_{\\phi }')

sxi = Parameter(name = 'sxi',
                nature = 'internal',
                type = 'real',
                value = '-((k1*k2)/vR**2)',
                texname = 's_{\\xi }')

alpha1 = Parameter(name = 'alpha1',
                   nature = 'internal',
                   type = 'real',
                   value = '-((alpha3*tanb**2)/(1 + tanb**2)) + (cth1*gw**2*(-MH**2 + MH02**2)*sth1)/(2.*MW*MW2*cmath.sqrt(2))',
                   texname = '\\alpha _1')

cphi = Parameter(name = 'cphi',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(1 - sphi**2)',
                 texname = 'c_{\\phi }')

cxi = Parameter(name = 'cxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sxi**2)',
                texname = 'c_{\\xi }')

MA01 = Parameter(name = 'MA01',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(-2*(2*lambda2 - lambda3)*vev**2 + (alpha3*vR**2)/(2.*cmath.sqrt(1 - eps**2)))',
                 texname = 'M_{\\text{A01}}')

MHP1 = Parameter(name = 'MHP1',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(MH03**2 + (alpha3*(1 - tanb**2)*vev**2)/(4.*(1 + tanb**2)))',
                 texname = 'M_{\\text{HP1}}')

MHP2 = Parameter(name = 'MHP2',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(alpha3*(vR**2/cmath.sqrt(1 - eps**2) + (vev**2*cmath.sqrt(1 - eps**2))/2.))/cmath.sqrt(2)',
                 texname = 'M_{\\text{HP2}}')

MHPPL = Parameter(name = 'MHPPL',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(MH03**2 + (alpha3*(1 - tanb**2)*vev**2)/(2.*(1 + tanb**2)))',
                  texname = 'M_{\\text{HPPL}}')

rho2 = Parameter(name = 'rho2',
                 nature = 'internal',
                 type = 'real',
                 value = '(MHPPR**2 - (alpha3*(1 - tanb**2)*vev**2)/(2.*(1 + tanb**2)))/(2.*vR**2)',
                 texname = '\\rho _2')

