# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Fri 20 Sep 2024 20:29:30



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
CKMlam = Parameter(name = 'CKMlam',
                   nature = 'external',
                   type = 'real',
                   value = 0.22453,
                   texname = '\\lambda _{\\text{CKM}}',
                   lhablock = 'CKMBLOCK',
                   lhacode = [ 1 ])

CKMA = Parameter(name = 'CKMA',
                 nature = 'external',
                 type = 'real',
                 value = 0.836,
                 texname = 'A_{\\text{CKM}}',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 2 ])

CKMrho = Parameter(name = 'CKMrho',
                   nature = 'external',
                   type = 'real',
                   value = 0.122,
                   texname = '\\rho _{\\text{CKM}}',
                   lhablock = 'CKMBLOCK',
                   lhacode = [ 3 ])

CKMeta = Parameter(name = 'CKMeta',
                   nature = 'external',
                   type = 'real',
                   value = 0.355,
                   texname = '\\eta _{\\text{CKM}}',
                   lhablock = 'CKMBLOCK',
                   lhacode = [ 4 ])

CKMRs12 = Parameter(name = 'CKMRs12',
                    nature = 'external',
                    type = 'real',
                    value = 0.22453,
                    texname = '\\text{sR}_{12}',
                    lhablock = 'CKMBLOCK',
                    lhacode = [ 5 ])

CKMRs13 = Parameter(name = 'CKMRs13',
                    nature = 'external',
                    type = 'real',
                    value = 0.00364283532491525,
                    texname = '\\text{sR}_{13}',
                    lhablock = 'CKMBLOCK',
                    lhacode = [ 6 ])

CKMRs23 = Parameter(name = 'CKMRs23',
                    nature = 'external',
                    type = 'real',
                    value = 0.0421458706724,
                    texname = '\\text{sR}_{23}',
                    lhablock = 'CKMBLOCK',
                    lhacode = [ 7 ])

CKMRdel = Parameter(name = 'CKMRdel',
                    nature = 'external',
                    type = 'real',
                    value = 1.240409691862808,
                    texname = '\\delta _R',
                    lhablock = 'CKMBLOCK',
                    lhacode = [ 8 ])

tb = Parameter(name = 'tb',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{tb}',
               lhablock = 'LRSMINPUTS',
               lhacode = [ 1 ])

alp = Parameter(name = 'alp',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = '\\alpha',
                lhablock = 'LRSMINPUTS',
                lhacode = [ 2 ])

thetamix = Parameter(name = 'thetamix',
                     nature = 'external',
                     type = 'real',
                     value = 0.01,
                     texname = '\\theta',
                     lhablock = 'LRSMINPUTS',
                     lhacode = [ 3 ])

etamix = Parameter(name = 'etamix',
                   nature = 'external',
                   type = 'real',
                   value = 0.00001,
                   texname = '\\eta',
                   lhablock = 'LRSMINPUTS',
                   lhacode = [ 4 ])

phimix = Parameter(name = 'phimix',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\phi',
                   lhablock = 'LRSMINPUTS',
                   lhacode = [ 5 ])

r4 = Parameter(name = 'r4',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\rho _4',
               lhablock = 'LRSMINPUTS',
               lhacode = [ 6 ])

zetaLR = Parameter(name = 'zetaLR',
                   nature = 'external',
                   type = 'real',
                   value = 1,
                   texname = '\\zeta _{\\text{LR}}',
                   lhablock = 'LRSMINPUTS',
                   lhacode = [ 7 ])

PMNSLs12 = Parameter(name = 'PMNSLs12',
                     nature = 'external',
                     type = 'real',
                     value = 0.5540758070878027,
                     texname = '\\text{PMNSLs12}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 1 ])

PMNSLs23 = Parameter(name = 'PMNSLs23',
                     nature = 'external',
                     type = 'real',
                     value = 0.7563068160475614,
                     texname = '\\text{PMNSLs23}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 2 ])

PMNSLs13 = Parameter(name = 'PMNSLs13',
                     nature = 'external',
                     type = 'real',
                     value = 0.14842506526863986,
                     texname = '\\text{PMNSLs13}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 3 ])

PMNSLdel = Parameter(name = 'PMNSLdel',
                     nature = 'external',
                     type = 'real',
                     value = 3.436902363027234,
                     texname = '\\text{PMNSLdel}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 4 ])

PMNSRs12 = Parameter(name = 'PMNSRs12',
                     nature = 'external',
                     type = 'real',
                     value = 0.5540758070878027,
                     texname = '\\text{PMNSRs12}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 11 ])

PMNSRs23 = Parameter(name = 'PMNSRs23',
                     nature = 'external',
                     type = 'real',
                     value = 0.7563068160475614,
                     texname = '\\text{PMNSRs23}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 12 ])

PMNSRs13 = Parameter(name = 'PMNSRs13',
                     nature = 'external',
                     type = 'real',
                     value = 0.14842506526863986,
                     texname = '\\text{PMNSRs13}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 13 ])

PMNSRdel = Parameter(name = 'PMNSRdel',
                     nature = 'external',
                     type = 'real',
                     value = 3.436902363027234,
                     texname = '\\text{PMNSRdel}',
                     lhablock = 'PMNSBLOCK',
                     lhacode = [ 14 ])

PMNSLphi1 = Parameter(name = 'PMNSLphi1',
                      nature = 'external',
                      type = 'real',
                      value = 0,
                      texname = '\\text{PMNSLphi1}',
                      lhablock = 'PMNSBLOCK',
                      lhacode = [ 15 ])

PMNSLphi2 = Parameter(name = 'PMNSLphi2',
                      nature = 'external',
                      type = 'real',
                      value = 0,
                      texname = '\\text{PMNSLphi2}',
                      lhablock = 'PMNSBLOCK',
                      lhacode = [ 16 ])

PMNSRphi1 = Parameter(name = 'PMNSRphi1',
                      nature = 'external',
                      type = 'real',
                      value = 0,
                      texname = '\\text{PMNSRphi1}',
                      lhablock = 'PMNSBLOCK',
                      lhacode = [ 17 ])

PMNSRphi2 = Parameter(name = 'PMNSRphi2',
                      nature = 'external',
                      type = 'real',
                      value = 0,
                      texname = '\\text{PMNSRphi2}',
                      lhablock = 'PMNSBLOCK',
                      lhacode = [ 18 ])

PMNSRphi3 = Parameter(name = 'PMNSRphi3',
                      nature = 'external',
                      type = 'real',
                      value = 0,
                      texname = '\\text{PMNSRphi3}',
                      lhablock = 'PMNSBLOCK',
                      lhacode = [ 19 ])

NUH = Parameter(name = 'NUH',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{NUH}',
                lhablock = 'PMNSBLOCK',
                lhacode = [ 23 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 2 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.4,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MWR = Parameter(name = 'MWR',
                nature = 'external',
                type = 'real',
                value = 6000,
                texname = '\\text{MWR}',
                lhablock = 'MASS',
                lhacode = [ 34 ])

mDRpp = Parameter(name = 'mDRpp',
                  nature = 'external',
                  type = 'real',
                  value = 500,
                  texname = '\\text{mDRpp}',
                  lhablock = 'MASS',
                  lhacode = [ 9000001 ])

mDL0 = Parameter(name = 'mDL0',
                 nature = 'external',
                 type = 'real',
                 value = 500,
                 texname = '\\text{mDL0}',
                 lhablock = 'MASS',
                 lhacode = [ 9000004 ])

mh = Parameter(name = 'mh',
               nature = 'external',
               type = 'real',
               value = 125.1,
               texname = '\\text{mh}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

mDD = Parameter(name = 'mDD',
                nature = 'external',
                type = 'real',
                value = 500,
                texname = '\\text{mDD}',
                lhablock = 'MASS',
                lhacode = [ 45 ])

mHH = Parameter(name = 'mHH',
                nature = 'external',
                type = 'real',
                value = 20000,
                texname = '\\text{mHH}',
                lhablock = 'MASS',
                lhacode = [ 35 ])

mAA = Parameter(name = 'mAA',
                nature = 'external',
                type = 'real',
                value = 20010,
                texname = '\\text{mAA}',
                lhablock = 'MASS',
                lhacode = [ 55 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MN1 = Parameter(name = 'MN1',
                nature = 'external',
                type = 'real',
                value = 100,
                texname = '\\text{MN1}',
                lhablock = 'MASS',
                lhacode = [ 60 ])

MN2 = Parameter(name = 'MN2',
                nature = 'external',
                type = 'real',
                value = 2000,
                texname = '\\text{MN2}',
                lhablock = 'MASS',
                lhacode = [ 62 ])

MN3 = Parameter(name = 'MN3',
                nature = 'external',
                type = 'real',
                value = 10000,
                texname = '\\text{MN3}',
                lhablock = 'MASS',
                lhacode = [ 64 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WZR = Parameter(name = 'WZR',
                nature = 'external',
                type = 'real',
                value = 333.94,
                texname = '\\text{WZR}',
                lhablock = 'DECAY',
                lhacode = [ 32 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WWR = Parameter(name = 'WWR',
                nature = 'external',
                type = 'real',
                value = 190.53,
                texname = '\\text{WWR}',
                lhablock = 'DECAY',
                lhacode = [ 34 ])

WHp = Parameter(name = 'WHp',
                nature = 'external',
                type = 'real',
                value = 2012.38,
                texname = '\\text{WHp}',
                lhablock = 'DECAY',
                lhacode = [ 37 ])

WDRpp = Parameter(name = 'WDRpp',
                  nature = 'external',
                  type = 'real',
                  value = 25.37,
                  texname = '\\text{WDRpp}',
                  lhablock = 'DECAY',
                  lhacode = [ 9000001 ])

WDLpp = Parameter(name = 'WDLpp',
                  nature = 'external',
                  type = 'real',
                  value = 32.39,
                  texname = '\\text{WDLpp}',
                  lhablock = 'DECAY',
                  lhacode = [ 9000002 ])

WDLp = Parameter(name = 'WDLp',
                 nature = 'external',
                 type = 'real',
                 value = 28.88,
                 texname = '\\text{WDLp}',
                 lhablock = 'DECAY',
                 lhacode = [ 9000003 ])

WDL0 = Parameter(name = 'WDL0',
                 nature = 'external',
                 type = 'real',
                 value = 52.09,
                 texname = '\\text{WDL0}',
                 lhablock = 'DECAY',
                 lhacode = [ 9000004 ])

WchiL0 = Parameter(name = 'WchiL0',
                   nature = 'external',
                   type = 'real',
                   value = 52.09,
                   texname = '\\text{WchiL0}',
                   lhablock = 'DECAY',
                   lhacode = [ 9000005 ])

Wh = Parameter(name = 'Wh',
               nature = 'external',
               type = 'real',
               value = 0.004115,
               texname = '\\text{Wh}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WDD = Parameter(name = 'WDD',
                nature = 'external',
                type = 'real',
                value = 120.3,
                texname = '\\text{WDD}',
                lhablock = 'DECAY',
                lhacode = [ 45 ])

WHH = Parameter(name = 'WHH',
                nature = 'external',
                type = 'real',
                value = 2667.9,
                texname = '\\text{WHH}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

WAA = Parameter(name = 'WAA',
                nature = 'external',
                type = 'real',
                value = 2019.2,
                texname = '\\text{WAA}',
                lhablock = 'DECAY',
                lhacode = [ 55 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WN1 = Parameter(name = 'WN1',
                nature = 'external',
                type = 'real',
                value = 3.41e-10,
                texname = '\\text{WN1}',
                lhablock = 'DECAY',
                lhacode = [ 60 ])

WN2 = Parameter(name = 'WN2',
                nature = 'external',
                type = 'real',
                value = 0.00054,
                texname = '\\text{WN2}',
                lhablock = 'DECAY',
                lhacode = [ 62 ])

WN3 = Parameter(name = 'WN3',
                nature = 'external',
                type = 'real',
                value = 85.92,
                texname = '\\text{WN3}',
                lhablock = 'DECAY',
                lhacode = [ 64 ])

beta = Parameter(name = 'beta',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.atan(tb)',
                 texname = '\\beta')

CKMLs12 = Parameter(name = 'CKMLs12',
                    nature = 'internal',
                    type = 'real',
                    value = 'CKMlam',
                    texname = '\\text{CKMLs12}')

CKMLs13d = Parameter(name = 'CKMLs13d',
                     nature = 'internal',
                     type = 'complex',
                     value = '(CKMA*CKMlam**3*(CKMrho + CKMeta*complex(0,1))*cmath.sqrt(1 - CKMA**2*CKMlam**4))/((1 - CKMA**2*CKMlam**4*(CKMrho + CKMeta*complex(0,1)))*cmath.sqrt(1 - CKMlam**2))',
                     texname = '\\text{CKMLs13d}')

CKMLs23 = Parameter(name = 'CKMLs23',
                    nature = 'internal',
                    type = 'real',
                    value = 'CKMA*CKMlam**2',
                    texname = '\\text{CKMLs23}')

CKMRc12 = Parameter(name = 'CKMRc12',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - CKMRs12**2)',
                    texname = '\\text{CKMRc12}')

CKMRc13 = Parameter(name = 'CKMRc13',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - CKMRs13**2)',
                    texname = '\\text{CKMRc13}')

CKMRc23 = Parameter(name = 'CKMRc23',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - CKMRs23**2)',
                    texname = '\\text{CKMRc23}')

CKMRs13d = Parameter(name = 'CKMRs13d',
                     nature = 'internal',
                     type = 'complex',
                     value = 'CKMRs13*cmath.exp(CKMRdel*complex(0,1))',
                     texname = '\\text{CKMRs13d}')

epD = Parameter(name = 'epD',
                nature = 'internal',
                type = 'real',
                value = '1',
                texname = '\\epsilon _D')

epr = Parameter(name = 'epr',
                nature = 'internal',
                type = 'real',
                value = '0',
                texname = '\\text{epr}')

mChiL0 = Parameter(name = 'mChiL0',
                   nature = 'internal',
                   type = 'real',
                   value = 'mDL0',
                   texname = '\\text{mChiL0}')

PMNSLc12 = Parameter(name = 'PMNSLc12',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSLs12**2)',
                     texname = '\\text{PMNSLc12}')

PMNSLc13 = Parameter(name = 'PMNSLc13',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSLs13**2)',
                     texname = '\\text{PMNSLc13}')

PMNSLc23 = Parameter(name = 'PMNSLc23',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSLs23**2)',
                     texname = '\\text{PMNSLc23}')

PMNSLs13d = Parameter(name = 'PMNSLs13d',
                      nature = 'internal',
                      type = 'complex',
                      value = 'PMNSLs13*cmath.exp(complex(0,1)*PMNSLdel)',
                      texname = '\\text{PMNSLs13d}')

PMNSRc12 = Parameter(name = 'PMNSRc12',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSRs12**2)',
                     texname = '\\text{PMNSRc12}')

PMNSRc13 = Parameter(name = 'PMNSRc13',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSRs13**2)',
                     texname = '\\text{PMNSRc13}')

PMNSRc23 = Parameter(name = 'PMNSRc23',
                     nature = 'internal',
                     type = 'real',
                     value = 'cmath.sqrt(1 - PMNSRs23**2)',
                     texname = '\\text{PMNSRc23}')

PMNSRs13d = Parameter(name = 'PMNSRs13d',
                      nature = 'internal',
                      type = 'complex',
                      value = 'PMNSRs13*cmath.exp(complex(0,1)*PMNSRdel)',
                      texname = '\\text{PMNSRs13d}')

thetaw = Parameter(name = 'thetaw',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.acos(MW/MZ)',
                   texname = '\\theta _w')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(1/aEWM1)*cmath.sqrt(cmath.pi)',
               texname = 'e')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

vL = Parameter(name = 'vL',
               nature = 'internal',
               type = 'real',
               value = '0',
               texname = 'v_L')

c2b = Parameter(name = 'c2b',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(2*beta)',
                texname = '\\text{c}_{\\text{2$\\beta $}}')

c2w = Parameter(name = 'c2w',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(2*thetaw)',
                texname = '\\text{c}_{2 w}')

cb = Parameter(name = 'cb',
               nature = 'internal',
               type = 'real',
               value = 'cmath.cos(beta)',
               texname = '\\text{c}_{\\beta }')

CKML1x3 = Parameter(name = 'CKML1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complexconjugate(CKMLs13d)',
                    texname = '\\text{CKML1x3}')

CKMLc12 = Parameter(name = 'CKMLc12',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - CKMLs12**2)',
                    texname = '\\text{CKMLc12}')

CKMLc13 = Parameter(name = 'CKMLc13',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - abs(CKMLs13d)**2)',
                    texname = '\\text{CKMLc13}')

CKMLc23 = Parameter(name = 'CKMLc23',
                    nature = 'internal',
                    type = 'real',
                    value = 'cmath.sqrt(1 - CKMLs23**2)',
                    texname = '\\text{CKMLc23}')

CKMR1x1 = Parameter(name = 'CKMR1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMRc12*CKMRc13',
                    texname = '\\text{CKMR1x1}')

CKMR1x2 = Parameter(name = 'CKMR1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMRc13*CKMRs12',
                    texname = '\\text{CKMR1x2}')

CKMR1x3 = Parameter(name = 'CKMR1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complexconjugate(CKMRs13d)',
                    texname = '\\text{CKMR1x3}')

CKMR2x1 = Parameter(name = 'CKMR2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMRc23*CKMRs12) - CKMRc12*CKMRs13d*CKMRs23',
                    texname = '\\text{CKMR2x1}')

CKMR2x2 = Parameter(name = 'CKMR2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMRc12*CKMRc23 - CKMRs12*CKMRs13d*CKMRs23',
                    texname = '\\text{CKMR2x2}')

CKMR2x3 = Parameter(name = 'CKMR2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMRc13*CKMRs23',
                    texname = '\\text{CKMR2x3}')

CKMR3x1 = Parameter(name = 'CKMR3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMRc12*CKMRc23*CKMRs13d) + CKMRs12*CKMRs23',
                    texname = '\\text{CKMR3x1}')

CKMR3x2 = Parameter(name = 'CKMR3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMRc23*CKMRs12*CKMRs13d) - CKMRc12*CKMRs23',
                    texname = '\\text{CKMR3x2}')

CKMR3x3 = Parameter(name = 'CKMR3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMRc13*CKMRc23',
                    texname = '\\text{CKMR3x3}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.cos(thetaw)',
               texname = '\\text{c}_w')

PMNSL1x1 = Parameter(name = 'PMNSL1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSLc12*PMNSLc13',
                     texname = '\\text{PMNSL1x1}')

PMNSL1x2 = Parameter(name = 'PMNSL1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSLc13*PMNSLs12*cmath.exp(complex(0,1)*PMNSLphi1)',
                     texname = '\\text{PMNSL1x2}')

PMNSL1x3 = Parameter(name = 'PMNSL1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complexconjugate(PMNSLs13d)*cmath.exp(complex(0,1)*PMNSLphi2)',
                     texname = '\\text{PMNSL1x3}')

PMNSL2x1 = Parameter(name = 'PMNSL2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = '-(PMNSLc23*PMNSLs12) - PMNSLc12*PMNSLs13d*PMNSLs23',
                     texname = '\\text{PMNSL2x1}')

PMNSL2x2 = Parameter(name = 'PMNSL2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = '(PMNSLc12*PMNSLc23 - PMNSLs12*PMNSLs13d*PMNSLs23)*cmath.exp(complex(0,1)*PMNSLphi1)',
                     texname = '\\text{PMNSL2x2}')

PMNSL2x3 = Parameter(name = 'PMNSL2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSLc13*PMNSLs23*cmath.exp(complex(0,1)*PMNSLphi2)',
                     texname = '\\text{PMNSL2x3}')

PMNSL3x1 = Parameter(name = 'PMNSL3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = '-(PMNSLc12*PMNSLc23*PMNSLs13d) + PMNSLs12*PMNSLs23',
                     texname = '\\text{PMNSL3x1}')

PMNSL3x2 = Parameter(name = 'PMNSL3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = '(-(PMNSLc23*PMNSLs12*PMNSLs13d) - PMNSLc12*PMNSLs23)*cmath.exp(complex(0,1)*PMNSLphi1)',
                     texname = '\\text{PMNSL3x2}')

PMNSL3x3 = Parameter(name = 'PMNSL3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSLc13*PMNSLc23*cmath.exp(complex(0,1)*PMNSLphi2)',
                     texname = '\\text{PMNSL3x3}')

PMNSR1x1 = Parameter(name = 'PMNSR1x1',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSRc12*PMNSRc13*cmath.exp(complex(0,1)*PMNSRphi1)',
                     texname = '\\text{PMNSR1x1}')

PMNSR1x2 = Parameter(name = 'PMNSR1x2',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSRc13*PMNSRs12*cmath.exp(complex(0,1)*PMNSRphi2)',
                     texname = '\\text{PMNSR1x2}')

PMNSR1x3 = Parameter(name = 'PMNSR1x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'complexconjugate(PMNSRs13d)*cmath.exp(complex(0,1)*PMNSRphi3)',
                     texname = '\\text{PMNSR1x3}')

PMNSR2x1 = Parameter(name = 'PMNSR2x1',
                     nature = 'internal',
                     type = 'complex',
                     value = '(-(PMNSRc23*PMNSRs12) - PMNSRc12*PMNSRs13d*PMNSRs23)*cmath.exp(complex(0,1)*PMNSRphi1)',
                     texname = '\\text{PMNSR2x1}')

PMNSR2x2 = Parameter(name = 'PMNSR2x2',
                     nature = 'internal',
                     type = 'complex',
                     value = '(PMNSRc12*PMNSRc23 - PMNSRs12*PMNSRs13d*PMNSRs23)*cmath.exp(complex(0,1)*PMNSRphi2)',
                     texname = '\\text{PMNSR2x2}')

PMNSR2x3 = Parameter(name = 'PMNSR2x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSRc13*PMNSRs23*cmath.exp(complex(0,1)*PMNSRphi3)',
                     texname = '\\text{PMNSR2x3}')

PMNSR3x1 = Parameter(name = 'PMNSR3x1',
                     nature = 'internal',
                     type = 'complex',
                     value = '(-(PMNSRc12*PMNSRc23*PMNSRs13d) + PMNSRs12*PMNSRs23)*cmath.exp(complex(0,1)*PMNSRphi1)',
                     texname = '\\text{PMNSR3x1}')

PMNSR3x2 = Parameter(name = 'PMNSR3x2',
                     nature = 'internal',
                     type = 'complex',
                     value = '(-(PMNSRc23*PMNSRs12*PMNSRs13d) - PMNSRc12*PMNSRs23)*cmath.exp(complex(0,1)*PMNSRphi2)',
                     texname = '\\text{PMNSR3x2}')

PMNSR3x3 = Parameter(name = 'PMNSR3x3',
                     nature = 'internal',
                     type = 'complex',
                     value = 'PMNSRc13*PMNSRc23*cmath.exp(complex(0,1)*PMNSRphi3)',
                     texname = '\\text{PMNSR3x3}')

s2b = Parameter(name = 's2b',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sin(2*beta)',
                texname = '\\text{s}_{\\text{2$\\beta $}}')

sb = Parameter(name = 'sb',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(beta)',
               texname = '\\text{s}_{\\beta }')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(thetaw)',
               texname = '\\text{s}_w')

CKML1x1 = Parameter(name = 'CKML1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMLc12*CKMLc13',
                    texname = '\\text{CKML1x1}')

CKML1x2 = Parameter(name = 'CKML1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMLc13*CKMLs12',
                    texname = '\\text{CKML1x2}')

CKML2x1 = Parameter(name = 'CKML2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMLc23*CKMLs12) - CKMLc12*CKMLs13d*CKMLs23',
                    texname = '\\text{CKML2x1}')

CKML2x2 = Parameter(name = 'CKML2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMLc12*CKMLc23 - CKMLs12*CKMLs13d*CKMLs23',
                    texname = '\\text{CKML2x2}')

CKML2x3 = Parameter(name = 'CKML2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMLc13*CKMLs23',
                    texname = '\\text{CKML2x3}')

CKML3x1 = Parameter(name = 'CKML3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMLc12*CKMLc23*CKMLs13d) + CKMLs12*CKMLs23',
                    texname = '\\text{CKML3x1}')

CKML3x2 = Parameter(name = 'CKML3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '-(CKMLc23*CKMLs12*CKMLs13d) - CKMLc12*CKMLs23',
                    texname = '\\text{CKML3x2}')

CKML3x3 = Parameter(name = 'CKML3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKMLc13*CKMLc23',
                    texname = '\\text{CKML3x3}')

tw = Parameter(name = 'tw',
               nature = 'internal',
               type = 'real',
               value = 'sw/cw',
               texname = '\\text{t}_w')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = '\\text{gw}')

rr = Parameter(name = 'rr',
               nature = 'internal',
               type = 'real',
               value = 'tw/cmath.sqrt(-tw**2 + zetaLR**2)',
               texname = '\\text{rr}')

gL = Parameter(name = 'gL',
               nature = 'internal',
               type = 'real',
               value = 'gw',
               texname = 'g_L')

gR = Parameter(name = 'gR',
               nature = 'internal',
               type = 'real',
               value = 'gw*zetaLR',
               texname = 'g_R')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(MW*cmath.sqrt(2))/gw',
                texname = '\\text{v}')

vR = Parameter(name = 'vR',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MWR**2 - MW**2*zetaLR**2)/(gw*zetaLR)',
               texname = '\\text{v}_{\\text{R}}')

ep = Parameter(name = 'ep',
               nature = 'internal',
               type = 'real',
               value = 'vev/vR',
               texname = '\\epsilon')

Y = Parameter(name = 'Y',
              nature = 'internal',
              type = 'real',
              value = '(mDD**2 + (-mDD**2 + mHH**2)*cmath.sin(etamix)**2)/((1 + thetamix**2)*vR**2)',
              texname = '\\text{Y}')

gBL = Parameter(name = 'gBL',
                nature = 'internal',
                type = 'real',
                value = 'gw*rr*zetaLR',
                texname = '\\text{gBL}')

v1 = Parameter(name = 'v1',
               nature = 'internal',
               type = 'real',
               value = 'cb*vev',
               texname = '\\text{v1}')

v2 = Parameter(name = 'v2',
               nature = 'internal',
               type = 'real',
               value = 'sb*vev',
               texname = '\\text{v2}')

X = Parameter(name = 'X',
              nature = 'internal',
              type = 'real',
              value = 'mAA**2/vR**2',
              texname = '\\text{X}')

Yd3x3 = Parameter(name = 'Yd3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'MB/vev',
                  texname = '\\text{Yd3x3}')

yDelta1x1 = Parameter(name = 'yDelta1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR1x1**2 + MN2*PMNSR1x2**2 + MN3*PMNSR1x3**2)/vR',
                      texname = '\\text{yDelta1x1}')

yDelta1x2 = Parameter(name = 'yDelta1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR1x1*PMNSR2x1 + MN2*PMNSR1x2*PMNSR2x2 + MN3*PMNSR1x3*PMNSR2x3)/vR',
                      texname = '\\text{yDelta1x2}')

yDelta1x3 = Parameter(name = 'yDelta1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR1x1*PMNSR3x1 + MN2*PMNSR1x2*PMNSR3x2 + MN3*PMNSR1x3*PMNSR3x3)/vR',
                      texname = '\\text{yDelta1x3}')

yDelta2x1 = Parameter(name = 'yDelta2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR1x1*PMNSR2x1 + MN2*PMNSR1x2*PMNSR2x2 + MN3*PMNSR1x3*PMNSR2x3)/vR',
                      texname = '\\text{yDelta2x1}')

yDelta2x2 = Parameter(name = 'yDelta2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR2x1**2 + MN2*PMNSR2x2**2 + MN3*PMNSR2x3**2)/vR',
                      texname = '\\text{yDelta2x2}')

yDelta2x3 = Parameter(name = 'yDelta2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR2x1*PMNSR3x1 + MN2*PMNSR2x2*PMNSR3x2 + MN3*PMNSR2x3*PMNSR3x3)/vR',
                      texname = '\\text{yDelta2x3}')

yDelta3x1 = Parameter(name = 'yDelta3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR1x1*PMNSR3x1 + MN2*PMNSR1x2*PMNSR3x2 + MN3*PMNSR1x3*PMNSR3x3)/vR',
                      texname = '\\text{yDelta3x1}')

yDelta3x2 = Parameter(name = 'yDelta3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR2x1*PMNSR3x1 + MN2*PMNSR2x2*PMNSR3x2 + MN3*PMNSR2x3*PMNSR3x3)/vR',
                      texname = '\\text{yDelta3x2}')

yDelta3x3 = Parameter(name = 'yDelta3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = '(MN1*PMNSR3x1**2 + MN2*PMNSR3x2**2 + MN3*PMNSR3x3**2)/vR',
                      texname = '\\text{yDelta3x3}')

Ylep3x3 = Parameter(name = 'Ylep3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'MTA/vev',
                    texname = '\\text{Ylep3x3}')

Yu3x3 = Parameter(name = 'Yu3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'MT/vev',
                  texname = '\\text{Yu3x3}')

eta2 = Parameter(name = 'eta2',
                 nature = 'internal',
                 type = 'real',
                 value = '((-mDD**2 + mHH**2)*(1 + tb**2)**2*cmath.sin(2*etamix))/(4.*ep*mAA**2*cmath.sqrt(1 + tb**4 - 2*tb**2*cmath.cos(2*alp)))',
                 texname = '\\text{eta2}')

mDLp = Parameter(name = 'mDLp',
                 nature = 'internal',
                 type = 'real',
                 value = 'mDL0 + (ep**2*(-1 + tb**2)**2*vR**2*X)/(4.*mDL0*(1 + tb**2)**2)',
                 texname = '\\text{mDeltaLp}')

mDLpp = Parameter(name = 'mDLpp',
                  nature = 'internal',
                  type = 'real',
                  value = 'mDL0 + (ep**2*(-1 + tb**2)**2*vR**2*X)/(2.*mDL0*(1 + tb**2)**2)',
                  texname = '\\text{mDeltaLpp}')

mHp = Parameter(name = 'mHp',
                nature = 'internal',
                type = 'real',
                value = '(vR*cmath.sqrt(X*(4 + ep**2 + ep**2*cmath.cos(4*beta))))/2.',
                texname = '\\text{mHp}')

MZR = Parameter(name = 'MZR',
                nature = 'internal',
                type = 'real',
                value = '(ep**2*MWR)/(4.*MW**3*zetaLR**2*(1/(-MZ**2 + MW**2*(1 + zetaLR**2)))**1.5*cmath.sqrt(2)) + MW*MWR*zetaLR**2*cmath.sqrt(2)*cmath.sqrt(1/(-MZ**2 + MW**2*(1 + zetaLR**2)))',
                texname = '\\text{MZR}')

xi = Parameter(name = 'xi',
               nature = 'internal',
               type = 'real',
               value = 'cmath.asin((cb*ep**2*sb)/zetaLR)',
               texname = '\\xi')

a3 = Parameter(name = 'a3',
               nature = 'internal',
               type = 'real',
               value = '-(((-1 + tb**2)*X)/(1 + tb**2))',
               texname = '\\text{a3}')

l2 = Parameter(name = 'l2',
               nature = 'internal',
               type = 'real',
               value = '-0.0625*((1 + tb**2)**2*(mAA**2 - mHH**2 + (-mDD**2 + mHH**2)*cmath.sin(etamix)**2))/(ep**2*vR**2*(1 + tb**4 - 2*tb**2*cmath.cos(2*alp)))',
               texname = '\\text{l2}')

r1 = Parameter(name = 'r1',
               nature = 'internal',
               type = 'real',
               value = 'Y/4.',
               texname = '\\text{r1}')

r2 = Parameter(name = 'r2',
               nature = 'internal',
               type = 'real',
               value = '(mDRpp**2/vR**2 - (ep**2*(-1 + tb**2)**2*X)/(1 + tb**2)**2)/4.',
               texname = '\\text{r2}')

r3 = Parameter(name = 'r3',
               nature = 'internal',
               type = 'real',
               value = 'mDL0**2/vR**2 + Y/2. - (4*ep**2*tb**2*X*cmath.sin(alp)**2)/(1 + tb**2)**2',
               texname = '\\text{r3}')

cxi = Parameter(name = 'cxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(xi)',
                texname = '\\text{c}_{\\xi }')

sxi = Parameter(name = 'sxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sin(xi)',
                texname = '\\text{s}_{\\xi }')

a2 = Parameter(name = 'a2',
               nature = 'internal',
               type = 'real',
               value = '(X*(eta2 - tb*cmath.cos(alp))*cmath.sqrt(1 + ((1.e-15 + tb)**2*cmath.sin(alp)**2)/(eta2 - (1.e-15 + tb)*cmath.cos(alp))**2))/(2.*(1 + tb**2))',
               texname = '\\text{a2}')

d2 = Parameter(name = 'd2',
               nature = 'internal',
               type = 'real',
               value = 'cmath.atan(((1.e-15 + tb)*cmath.sin(alp))/(eta2 - (1.e-15 + tb)*cmath.cos(alp)))',
               texname = '\\text{d2}')

l3 = Parameter(name = 'l3',
               nature = 'internal',
               type = 'real',
               value = '2*l2',
               texname = '\\text{l3}')

l4 = Parameter(name = 'l4',
               nature = 'internal',
               type = 'real',
               value = '(eta2*mAA**2*thetamix)/(2.*ep*(1 + tb**2)*vR**2) + ((mAA**2*phimix*(1 + tb**2))/(4.*vR**2*(-1 + tb**2*cmath.cos(2*alp))) + (tb*(1 + tb**2)*cmath.cos(alp)*(2*mAA**2 - mDD**2 - mHH**2 + (mDD**2 - mHH**2)*cmath.cos(2*etamix)))/(4.*vR**2*(1 + tb**4 - 2*tb**2*cmath.cos(2*alp))))/ep**2',
               texname = '\\text{l4}')

mu2 = Parameter(name = 'mu2',
                nature = 'internal',
                type = 'real',
                value = '-0.5*((-4*l3*v1**3*v2 + 4*l3*v1*v2**3 - a3*v1*v2*vR**2 - 2*l4*v1**4*cmath.cos(alp) + 2*l4*v2**4*cmath.cos(alp) - 8*l2*v1**3*v2*cmath.cos(2*alp) + 8*l2*v1*v2**3*cmath.cos(2*alp) - 2*a2*v1**2*vR**2*cmath.cos(alp + d2) + 2*a2*v2**2*vR**2*cmath.cos(alp + d2))*sec(alp))/(v1**2 - v2**2)',
                texname = '\\text{mu2}')

ON3x1 = Parameter(name = 'ON3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))',
                  texname = '\\text{ON3x1}')

ON3x2 = Parameter(name = 'ON3x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sin(etamix)*(cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2) - (tb**2*cmath.sin(2*alp)*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON3x2}')

ON3x3 = Parameter(name = 'ON3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(etamix)*(cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2) - (tb**2*cmath.sin(2*alp)*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON3x3}')

ON3x4 = Parameter(name = 'ON3x4',
                  nature = 'internal',
                  type = 'real',
                  value = '-((tb**2*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin(2*alp))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2))) - (cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)',
                  texname = '\\text{ON3x4}')

ON4x1 = Parameter(name = 'ON4x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))',
                  texname = '\\text{ON4x1}')

ON4x2 = Parameter(name = 'ON4x2',
                  nature = 'internal',
                  type = 'real',
                  value = '(tb**2*cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))*cmath.sin(2*alp)*cmath.sin(etamix))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2))',
                  texname = '\\text{ON4x2}')

ON4x3 = Parameter(name = 'ON4x3',
                  nature = 'internal',
                  type = 'real',
                  value = '(tb**2*cmath.cos(etamix)*cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))*cmath.sin(2*alp))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2))',
                  texname = '\\text{ON4x3}')

ON4x4 = Parameter(name = 'ON4x4',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)',
                  texname = '\\text{ON4x4}')

l1 = Parameter(name = 'l1',
               nature = 'internal',
               type = 'real',
               value = 'mh**2/(4.*ep**2*vR**2) + (thetamix**2*Y)/(4.*ep**2) - (4*l4*tb*cmath.cos(alp))/(1 + tb**2) - (16*l2*tb**2*cmath.cos(alp)**2)/(1 + tb**2)**2',
               texname = '\\text{l1}')

a1 = Parameter(name = 'a1',
               nature = 'internal',
               type = 'real',
               value = '(thetamix*Y)/(2.*ep) + (tb*X*(tb + tb**3 - 2*eta2*cmath.cos(alp)))/(1 + tb**2)**2 + ep**2*((-2*l1*thetamix)/ep - (8*l4*tb*thetamix*cmath.cos(alp))/(ep*(1 + tb**2)) - (32*l2*tb**2*thetamix*cmath.cos(alp)**2)/(ep*(1 + tb**2)**2))',
               texname = '\\text{a1}')

mu1 = Parameter(name = 'mu1',
                nature = 'internal',
                type = 'real',
                value = '-((-2*l1*v1**4 + 2*l1*v2**4 - a1*v1**2*vR**2 + a1*v2**2*vR**2 + a3*v2**2*vR**2 - 4*l4*v1**3*v2*cmath.cos(alp) + 4*l4*v1*v2**3*cmath.cos(alp))/(v1**2 - v2**2))',
                texname = '\\text{mu1}')

mu3 = Parameter(name = 'mu3',
                nature = 'internal',
                type = 'real',
                value = 'a1*v1**2 + a1*v2**2 + a3*v2**2 + 2*r1*vR**2 + 4*a2*v1*v2*cmath.cos(alp + d2)',
                texname = '\\text{mu3}')

ON1x1 = Parameter(name = 'ON1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))',
                  texname = '\\text{ON1x1}')

ON1x2 = Parameter(name = 'ON1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(etamix)*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp))) + cmath.sin(etamix)*(-((cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)) - (tb**2*cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin(2*alp)*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON1x2}')

ON1x3 = Parameter(name = 'ON1x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.sin(etamix)*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))) + cmath.cos(etamix)*(-((cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)) - (tb**2*cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin(2*alp)*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON1x3}')

ON1x4 = Parameter(name = 'ON1x4',
                  nature = 'internal',
                  type = 'real',
                  value = '(tb**2*cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin(2*alp)*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)) - (cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)',
                  texname = '\\text{ON1x4}')

ON2x1 = Parameter(name = 'ON2x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.cos((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X))*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp))))',
                  texname = '\\text{ON2x1}')

ON2x2 = Parameter(name = 'ON2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(etamix)*cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp))) + cmath.sin(etamix)*((cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2) + (tb**2*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin(2*alp)*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON2x2}')

ON2x3 = Parameter(name = 'ON2x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin(etamix)) + cmath.cos(etamix)*((cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2) + (tb**2*cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin(2*alp)*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)))',
                  texname = '\\text{ON2x3}')

ON2x4 = Parameter(name = 'ON2x4',
                  nature = 'internal',
                  type = 'real',
                  value = '-((tb**2*cmath.sin(2*alp)*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X)))/((-1 + tb**2*cmath.cos(2*alp))*cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2))) + (cmath.cos((2*ep*eta2*thetamix*(-1 + tb**2*cmath.cos(2*alp)))/(1 + tb**2)**2 - (4*ep**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*(-1 + tb**2*cmath.cos(2*alp)))/((1 + tb**2)**2*X))*cmath.sin((2*ep*((1 + tb**2)*(-a1 + tb**2*(-a1 + X)) - 2*eta2*tb*X*cmath.cos(alp)))/(4*ep**2*l1 + 8*ep**2*l1*tb**2 + 32*ep**2*l2*tb**2 + 4*ep**2*l1*tb**4 - Y - 2*tb**2*Y - tb**4*Y + 16*ep**2*l4*tb*(1 + tb**2)*cmath.cos(alp) + 32*ep**2*l2*tb**2*cmath.cos(2*alp)))*cmath.sin((2*ep*eta2*tb**2*thetamix*cmath.sin(2*alp))/(1 + tb**2)**2 - (4*ep**2*tb**2*(l4 + l4*tb**2 + 8*l2*tb*cmath.cos(alp))*cmath.sin(2*alp))/((1 + tb**2)**2*X)))/cmath.sqrt(1 + (tb**4*cmath.sin(2*alp)**2)/(-1 + tb**2*cmath.cos(2*alp))**2)',
                  texname = '\\text{ON2x4}')

I20a31 = Parameter(name = 'I20a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL3x1*complexconjugate(Ylep3x3)',
                   texname = '\\text{I20a31}')

I20a32 = Parameter(name = 'I20a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL3x2*complexconjugate(Ylep3x3)',
                   texname = '\\text{I20a32}')

I20a33 = Parameter(name = 'I20a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL3x3*complexconjugate(Ylep3x3)',
                   texname = '\\text{I20a33}')

I23a11 = Parameter(name = 'I23a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1**2*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1**2*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a11}')

I23a12 = Parameter(name = 'I23a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a12}')

I23a13 = Parameter(name = 'I23a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a13}')

I23a21 = Parameter(name = 'I23a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a21}')

I23a22 = Parameter(name = 'I23a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2**2*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x2**2*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x2**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a22}')

I23a23 = Parameter(name = 'I23a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x2*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x2*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a23}')

I23a31 = Parameter(name = 'I23a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a31}')

I23a32 = Parameter(name = 'I23a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x2*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x2*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a32}')

I23a33 = Parameter(name = 'I23a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3**2*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x3**2*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x3**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I23a33}')

I24a11 = Parameter(name = 'I24a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1**2*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1**2*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a11}')

I24a12 = Parameter(name = 'I24a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a12}')

I24a13 = Parameter(name = 'I24a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x1*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x1*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x1*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x1*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x1*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a13}')

I24a21 = Parameter(name = 'I24a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a21}')

I24a22 = Parameter(name = 'I24a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2**2*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x2**2*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x2**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a22}')

I24a23 = Parameter(name = 'I24a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x2*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x2*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR2x2*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x2*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x2*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x2*complexconjugate(yDelta3x2) + PMNSR3x2*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a23}')

I24a31 = Parameter(name = 'I24a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x1*complexconjugate(yDelta1x3) + PMNSR1x1*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x1*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x1*complexconjugate(yDelta2x3) + PMNSR1x1*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x1*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x1*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a31}')

I24a32 = Parameter(name = 'I24a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x2*complexconjugate(yDelta1x3) + PMNSR1x2*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x2*PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x2*complexconjugate(yDelta2x3) + PMNSR1x2*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x2*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x2*PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a32}')

I24a33 = Parameter(name = 'I24a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3**2*complexconjugate(yDelta1x1) + PMNSR1x3*PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR1x3*PMNSR3x3*complexconjugate(yDelta1x3) + PMNSR1x3*PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR2x3**2*complexconjugate(yDelta2x2) + PMNSR2x3*PMNSR3x3*complexconjugate(yDelta2x3) + PMNSR1x3*PMNSR3x3*complexconjugate(yDelta3x1) + PMNSR2x3*PMNSR3x3*complexconjugate(yDelta3x2) + PMNSR3x3**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I24a33}')

I29a11 = Parameter(name = 'I29a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)**2 + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x2*complexconjugate(PMNSR2x1)**2 + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x3*complexconjugate(PMNSR3x1)**2',
                   texname = '\\text{I29a11}')

I29a12 = Parameter(name = 'I29a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I29a12}')

I29a13 = Parameter(name = 'I29a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I29a13}')

I29a21 = Parameter(name = 'I29a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I29a21}')

I29a22 = Parameter(name = 'I29a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)**2 + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x2)**2 + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x2)**2',
                   texname = '\\text{I29a22}')

I29a23 = Parameter(name = 'I29a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I29a23}')

I29a31 = Parameter(name = 'I29a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I29a31}')

I29a32 = Parameter(name = 'I29a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I29a32}')

I29a33 = Parameter(name = 'I29a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3)**2 + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x3)**2 + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x3)**2',
                   texname = '\\text{I29a33}')

I30a11 = Parameter(name = 'I30a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)**2 + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x2*complexconjugate(PMNSR2x1)**2 + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x3*complexconjugate(PMNSR3x1)**2',
                   texname = '\\text{I30a11}')

I30a12 = Parameter(name = 'I30a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I30a12}')

I30a13 = Parameter(name = 'I30a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I30a13}')

I30a21 = Parameter(name = 'I30a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I30a21}')

I30a22 = Parameter(name = 'I30a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)**2 + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x2)**2 + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x2)**2',
                   texname = '\\text{I30a22}')

I30a23 = Parameter(name = 'I30a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I30a23}')

I30a31 = Parameter(name = 'I30a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I30a31}')

I30a32 = Parameter(name = 'I30a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I30a32}')

I30a33 = Parameter(name = 'I30a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3)**2 + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x3)**2 + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x3)**2',
                   texname = '\\text{I30a33}')

I31a11 = Parameter(name = 'I31a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)**2 + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x2*complexconjugate(PMNSR2x1)**2 + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x3*complexconjugate(PMNSR3x1)**2',
                   texname = '\\text{I31a11}')

I31a12 = Parameter(name = 'I31a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I31a12}')

I31a13 = Parameter(name = 'I31a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I31a13}')

I31a21 = Parameter(name = 'I31a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I31a21}')

I31a22 = Parameter(name = 'I31a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)**2 + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x2)**2 + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x2)**2',
                   texname = '\\text{I31a22}')

I31a23 = Parameter(name = 'I31a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I31a23}')

I31a31 = Parameter(name = 'I31a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I31a31}')

I31a32 = Parameter(name = 'I31a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I31a32}')

I31a33 = Parameter(name = 'I31a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3)**2 + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x3)**2 + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x3)**2',
                   texname = '\\text{I31a33}')

I32a11 = Parameter(name = 'I32a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)**2 + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x1) + yDelta2x2*complexconjugate(PMNSR2x1)**2 + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x1) + yDelta3x3*complexconjugate(PMNSR3x1)**2',
                   texname = '\\text{I32a11}')

I32a12 = Parameter(name = 'I32a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I32a12}')

I32a13 = Parameter(name = 'I32a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta2x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta3x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I32a13}')

I32a21 = Parameter(name = 'I32a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I32a21}')

I32a22 = Parameter(name = 'I32a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)**2 + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x2) + yDelta2x2*complexconjugate(PMNSR2x2)**2 + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x2) + yDelta3x3*complexconjugate(PMNSR3x2)**2',
                   texname = '\\text{I32a22}')

I32a23 = Parameter(name = 'I32a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta2x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta3x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I32a23}')

I32a31 = Parameter(name = 'I32a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x1) + yDelta1x2*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x1) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x1) + yDelta1x3*complexconjugate(PMNSR1x1)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x1)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x1)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I32a31}')

I32a32 = Parameter(name = 'I32a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x2) + yDelta1x2*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x2) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x2) + yDelta1x3*complexconjugate(PMNSR1x2)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x2)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x2)*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I32a32}')

I32a33 = Parameter(name = 'I32a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3)**2 + yDelta1x2*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR2x3) + yDelta2x2*complexconjugate(PMNSR2x3)**2 + yDelta1x3*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta3x1*complexconjugate(PMNSR1x3)*complexconjugate(PMNSR3x3) + yDelta2x3*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x2*complexconjugate(PMNSR2x3)*complexconjugate(PMNSR3x3) + yDelta3x3*complexconjugate(PMNSR3x3)**2',
                   texname = '\\text{I32a33}')

I37a11 = Parameter(name = 'I37a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta1x1) + PMNSR2x1*complexconjugate(yDelta2x1) + PMNSR3x1*complexconjugate(yDelta3x1)',
                   texname = '\\text{I37a11}')

I37a12 = Parameter(name = 'I37a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR2x2*complexconjugate(yDelta2x1) + PMNSR3x2*complexconjugate(yDelta3x1)',
                   texname = '\\text{I37a12}')

I37a13 = Parameter(name = 'I37a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR2x3*complexconjugate(yDelta2x1) + PMNSR3x3*complexconjugate(yDelta3x1)',
                   texname = '\\text{I37a13}')

I37a21 = Parameter(name = 'I37a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta1x2) + PMNSR2x1*complexconjugate(yDelta2x2) + PMNSR3x1*complexconjugate(yDelta3x2)',
                   texname = '\\text{I37a21}')

I37a22 = Parameter(name = 'I37a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta1x2) + PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR3x2*complexconjugate(yDelta3x2)',
                   texname = '\\text{I37a22}')

I37a23 = Parameter(name = 'I37a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta1x2) + PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR3x3*complexconjugate(yDelta3x2)',
                   texname = '\\text{I37a23}')

I37a31 = Parameter(name = 'I37a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta1x3) + PMNSR2x1*complexconjugate(yDelta2x3) + PMNSR3x1*complexconjugate(yDelta3x3)',
                   texname = '\\text{I37a31}')

I37a32 = Parameter(name = 'I37a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta1x3) + PMNSR2x2*complexconjugate(yDelta2x3) + PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I37a32}')

I37a33 = Parameter(name = 'I37a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta1x3) + PMNSR2x3*complexconjugate(yDelta2x3) + PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I37a33}')

I38a11 = Parameter(name = 'I38a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta1x1) + PMNSR2x1*complexconjugate(yDelta1x2) + PMNSR3x1*complexconjugate(yDelta1x3)',
                   texname = '\\text{I38a11}')

I38a12 = Parameter(name = 'I38a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta1x1) + PMNSR2x2*complexconjugate(yDelta1x2) + PMNSR3x2*complexconjugate(yDelta1x3)',
                   texname = '\\text{I38a12}')

I38a13 = Parameter(name = 'I38a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta1x1) + PMNSR2x3*complexconjugate(yDelta1x2) + PMNSR3x3*complexconjugate(yDelta1x3)',
                   texname = '\\text{I38a13}')

I38a21 = Parameter(name = 'I38a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta2x1) + PMNSR2x1*complexconjugate(yDelta2x2) + PMNSR3x1*complexconjugate(yDelta2x3)',
                   texname = '\\text{I38a21}')

I38a22 = Parameter(name = 'I38a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta2x1) + PMNSR2x2*complexconjugate(yDelta2x2) + PMNSR3x2*complexconjugate(yDelta2x3)',
                   texname = '\\text{I38a22}')

I38a23 = Parameter(name = 'I38a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta2x1) + PMNSR2x3*complexconjugate(yDelta2x2) + PMNSR3x3*complexconjugate(yDelta2x3)',
                   texname = '\\text{I38a23}')

I38a31 = Parameter(name = 'I38a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x1*complexconjugate(yDelta3x1) + PMNSR2x1*complexconjugate(yDelta3x2) + PMNSR3x1*complexconjugate(yDelta3x3)',
                   texname = '\\text{I38a31}')

I38a32 = Parameter(name = 'I38a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x2*complexconjugate(yDelta3x1) + PMNSR2x2*complexconjugate(yDelta3x2) + PMNSR3x2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I38a32}')

I38a33 = Parameter(name = 'I38a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR1x3*complexconjugate(yDelta3x1) + PMNSR2x3*complexconjugate(yDelta3x2) + PMNSR3x3*complexconjugate(yDelta3x3)',
                   texname = '\\text{I38a33}')

I4a11 = Parameter(name = 'I4a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x1*PMNSR3x1*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a11}')

I4a12 = Parameter(name = 'I4a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x1*PMNSR3x2*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a12}')

I4a13 = Parameter(name = 'I4a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x1*PMNSR3x3*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a13}')

I4a21 = Parameter(name = 'I4a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x2*PMNSR3x1*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a21}')

I4a22 = Parameter(name = 'I4a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x2*PMNSR3x2*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a22}')

I4a23 = Parameter(name = 'I4a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x2*PMNSR3x3*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a23}')

I4a31 = Parameter(name = 'I4a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x3*PMNSR3x1*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a31}')

I4a32 = Parameter(name = 'I4a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x3*PMNSR3x2*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a32}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'PMNSL3x3*PMNSR3x3*complexconjugate(Ylep3x3)',
                  texname = '\\text{I4a33}')

I42a31 = Parameter(name = 'I42a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I42a31}')

I42a32 = Parameter(name = 'I42a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I42a32}')

I42a33 = Parameter(name = 'I42a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I42a33}')

I43a13 = Parameter(name = 'I43a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR3x1*complexconjugate(Ylep3x3)',
                   texname = '\\text{I43a13}')

I43a23 = Parameter(name = 'I43a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR3x2*complexconjugate(Ylep3x3)',
                   texname = '\\text{I43a23}')

I43a33 = Parameter(name = 'I43a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSR3x3*complexconjugate(Ylep3x3)',
                   texname = '\\text{I43a33}')

I45a11 = Parameter(name = 'I45a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1) + yDelta2x1*complexconjugate(PMNSR2x1) + yDelta3x1*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I45a11}')

I45a12 = Parameter(name = 'I45a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x2*complexconjugate(PMNSR1x1) + yDelta2x2*complexconjugate(PMNSR2x1) + yDelta3x2*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I45a12}')

I45a13 = Parameter(name = 'I45a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x3*complexconjugate(PMNSR1x1) + yDelta2x3*complexconjugate(PMNSR2x1) + yDelta3x3*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I45a13}')

I45a21 = Parameter(name = 'I45a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2) + yDelta2x1*complexconjugate(PMNSR2x2) + yDelta3x1*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I45a21}')

I45a22 = Parameter(name = 'I45a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x2*complexconjugate(PMNSR1x2) + yDelta2x2*complexconjugate(PMNSR2x2) + yDelta3x2*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I45a22}')

I45a23 = Parameter(name = 'I45a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x3*complexconjugate(PMNSR1x2) + yDelta2x3*complexconjugate(PMNSR2x2) + yDelta3x3*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I45a23}')

I45a31 = Parameter(name = 'I45a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3) + yDelta2x1*complexconjugate(PMNSR2x3) + yDelta3x1*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I45a31}')

I45a32 = Parameter(name = 'I45a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x2*complexconjugate(PMNSR1x3) + yDelta2x2*complexconjugate(PMNSR2x3) + yDelta3x2*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I45a32}')

I45a33 = Parameter(name = 'I45a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x3*complexconjugate(PMNSR1x3) + yDelta2x3*complexconjugate(PMNSR2x3) + yDelta3x3*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I45a33}')

I46a11 = Parameter(name = 'I46a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x1) + yDelta1x2*complexconjugate(PMNSR2x1) + yDelta1x3*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I46a11}')

I46a12 = Parameter(name = 'I46a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta2x1*complexconjugate(PMNSR1x1) + yDelta2x2*complexconjugate(PMNSR2x1) + yDelta2x3*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I46a12}')

I46a13 = Parameter(name = 'I46a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta3x1*complexconjugate(PMNSR1x1) + yDelta3x2*complexconjugate(PMNSR2x1) + yDelta3x3*complexconjugate(PMNSR3x1)',
                   texname = '\\text{I46a13}')

I46a21 = Parameter(name = 'I46a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x2) + yDelta1x2*complexconjugate(PMNSR2x2) + yDelta1x3*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I46a21}')

I46a22 = Parameter(name = 'I46a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta2x1*complexconjugate(PMNSR1x2) + yDelta2x2*complexconjugate(PMNSR2x2) + yDelta2x3*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I46a22}')

I46a23 = Parameter(name = 'I46a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta3x1*complexconjugate(PMNSR1x2) + yDelta3x2*complexconjugate(PMNSR2x2) + yDelta3x3*complexconjugate(PMNSR3x2)',
                   texname = '\\text{I46a23}')

I46a31 = Parameter(name = 'I46a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta1x1*complexconjugate(PMNSR1x3) + yDelta1x2*complexconjugate(PMNSR2x3) + yDelta1x3*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I46a31}')

I46a32 = Parameter(name = 'I46a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta2x1*complexconjugate(PMNSR1x3) + yDelta2x2*complexconjugate(PMNSR2x3) + yDelta2x3*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I46a32}')

I46a33 = Parameter(name = 'I46a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'yDelta3x1*complexconjugate(PMNSR1x3) + yDelta3x2*complexconjugate(PMNSR2x3) + yDelta3x3*complexconjugate(PMNSR3x3)',
                   texname = '\\text{I46a33}')

I54a13 = Parameter(name = 'I54a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSL3x1)',
                   texname = '\\text{I54a13}')

I54a23 = Parameter(name = 'I54a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSL3x2)',
                   texname = '\\text{I54a23}')

I54a33 = Parameter(name = 'I54a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Ylep3x3*complexconjugate(PMNSL3x3)',
                   texname = '\\text{I54a33}')

I59a11 = Parameter(name = 'I59a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a11}')

I59a12 = Parameter(name = 'I59a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a12}')

I59a13 = Parameter(name = 'I59a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a13}')

I59a21 = Parameter(name = 'I59a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a21}')

I59a22 = Parameter(name = 'I59a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a22}')

I59a23 = Parameter(name = 'I59a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a23}')

I59a31 = Parameter(name = 'I59a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a31}')

I59a32 = Parameter(name = 'I59a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a32}')

I59a33 = Parameter(name = 'I59a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x3)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x3)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I59a33}')

I60a11 = Parameter(name = 'I60a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a11}')

I60a12 = Parameter(name = 'I60a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a12}')

I60a13 = Parameter(name = 'I60a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a13}')

I60a21 = Parameter(name = 'I60a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a21}')

I60a22 = Parameter(name = 'I60a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a22}')

I60a23 = Parameter(name = 'I60a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a23}')

I60a31 = Parameter(name = 'I60a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a31}')

I60a32 = Parameter(name = 'I60a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a32}')

I60a33 = Parameter(name = 'I60a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)**2*complexconjugate(yDelta1x1) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x3)**2*complexconjugate(yDelta2x2) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL1x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x3)**2*complexconjugate(yDelta3x3)',
                   texname = '\\text{I60a33}')

I61a11 = Parameter(name = 'I61a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1**2*yDelta1x1 + PMNSL1x1*PMNSL2x1*yDelta1x2 + PMNSL1x1*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x1*yDelta2x1 + PMNSL2x1**2*yDelta2x2 + PMNSL2x1*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x1*yDelta3x1 + PMNSL2x1*PMNSL3x1*yDelta3x2 + PMNSL3x1**2*yDelta3x3',
                   texname = '\\text{I61a11}')

I61a12 = Parameter(name = 'I61a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x2*yDelta1x1 + PMNSL1x2*PMNSL2x1*yDelta1x2 + PMNSL1x2*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x2*yDelta2x1 + PMNSL2x1*PMNSL2x2*yDelta2x2 + PMNSL2x2*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x2*yDelta3x1 + PMNSL2x1*PMNSL3x2*yDelta3x2 + PMNSL3x1*PMNSL3x2*yDelta3x3',
                   texname = '\\text{I61a12}')

I61a13 = Parameter(name = 'I61a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x3*yDelta1x1 + PMNSL1x3*PMNSL2x1*yDelta1x2 + PMNSL1x3*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x3*yDelta2x1 + PMNSL2x1*PMNSL2x3*yDelta2x2 + PMNSL2x3*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x3*yDelta3x1 + PMNSL2x1*PMNSL3x3*yDelta3x2 + PMNSL3x1*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I61a13}')

I61a21 = Parameter(name = 'I61a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x2*yDelta1x1 + PMNSL1x1*PMNSL2x2*yDelta1x2 + PMNSL1x1*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x1*yDelta2x1 + PMNSL2x1*PMNSL2x2*yDelta2x2 + PMNSL2x1*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x1*yDelta3x1 + PMNSL2x2*PMNSL3x1*yDelta3x2 + PMNSL3x1*PMNSL3x2*yDelta3x3',
                   texname = '\\text{I61a21}')

I61a22 = Parameter(name = 'I61a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2**2*yDelta1x1 + PMNSL1x2*PMNSL2x2*yDelta1x2 + PMNSL1x2*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x2*yDelta2x1 + PMNSL2x2**2*yDelta2x2 + PMNSL2x2*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x2*yDelta3x1 + PMNSL2x2*PMNSL3x2*yDelta3x2 + PMNSL3x2**2*yDelta3x3',
                   texname = '\\text{I61a22}')

I61a23 = Parameter(name = 'I61a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*PMNSL1x3*yDelta1x1 + PMNSL1x3*PMNSL2x2*yDelta1x2 + PMNSL1x3*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x3*yDelta2x1 + PMNSL2x2*PMNSL2x3*yDelta2x2 + PMNSL2x3*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x3*yDelta3x1 + PMNSL2x2*PMNSL3x3*yDelta3x2 + PMNSL3x2*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I61a23}')

I61a31 = Parameter(name = 'I61a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x3*yDelta1x1 + PMNSL1x1*PMNSL2x3*yDelta1x2 + PMNSL1x1*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x1*yDelta2x1 + PMNSL2x1*PMNSL2x3*yDelta2x2 + PMNSL2x1*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x1*yDelta3x1 + PMNSL2x3*PMNSL3x1*yDelta3x2 + PMNSL3x1*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I61a31}')

I61a32 = Parameter(name = 'I61a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*PMNSL1x3*yDelta1x1 + PMNSL1x2*PMNSL2x3*yDelta1x2 + PMNSL1x2*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x2*yDelta2x1 + PMNSL2x2*PMNSL2x3*yDelta2x2 + PMNSL2x2*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x2*yDelta3x1 + PMNSL2x3*PMNSL3x2*yDelta3x2 + PMNSL3x2*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I61a32}')

I61a33 = Parameter(name = 'I61a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3**2*yDelta1x1 + PMNSL1x3*PMNSL2x3*yDelta1x2 + PMNSL1x3*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x3*yDelta2x1 + PMNSL2x3**2*yDelta2x2 + PMNSL2x3*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x3*yDelta3x1 + PMNSL2x3*PMNSL3x3*yDelta3x2 + PMNSL3x3**2*yDelta3x3',
                   texname = '\\text{I61a33}')

I62a11 = Parameter(name = 'I62a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1**2*yDelta1x1 + PMNSL1x1*PMNSL2x1*yDelta1x2 + PMNSL1x1*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x1*yDelta2x1 + PMNSL2x1**2*yDelta2x2 + PMNSL2x1*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x1*yDelta3x1 + PMNSL2x1*PMNSL3x1*yDelta3x2 + PMNSL3x1**2*yDelta3x3',
                   texname = '\\text{I62a11}')

I62a12 = Parameter(name = 'I62a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x2*yDelta1x1 + PMNSL1x1*PMNSL2x2*yDelta1x2 + PMNSL1x1*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x1*yDelta2x1 + PMNSL2x1*PMNSL2x2*yDelta2x2 + PMNSL2x1*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x1*yDelta3x1 + PMNSL2x2*PMNSL3x1*yDelta3x2 + PMNSL3x1*PMNSL3x2*yDelta3x3',
                   texname = '\\text{I62a12}')

I62a13 = Parameter(name = 'I62a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x3*yDelta1x1 + PMNSL1x1*PMNSL2x3*yDelta1x2 + PMNSL1x1*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x1*yDelta2x1 + PMNSL2x1*PMNSL2x3*yDelta2x2 + PMNSL2x1*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x1*yDelta3x1 + PMNSL2x3*PMNSL3x1*yDelta3x2 + PMNSL3x1*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I62a13}')

I62a21 = Parameter(name = 'I62a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x2*yDelta1x1 + PMNSL1x2*PMNSL2x1*yDelta1x2 + PMNSL1x2*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x2*yDelta2x1 + PMNSL2x1*PMNSL2x2*yDelta2x2 + PMNSL2x2*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x2*yDelta3x1 + PMNSL2x1*PMNSL3x2*yDelta3x2 + PMNSL3x1*PMNSL3x2*yDelta3x3',
                   texname = '\\text{I62a21}')

I62a22 = Parameter(name = 'I62a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2**2*yDelta1x1 + PMNSL1x2*PMNSL2x2*yDelta1x2 + PMNSL1x2*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x2*yDelta2x1 + PMNSL2x2**2*yDelta2x2 + PMNSL2x2*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x2*yDelta3x1 + PMNSL2x2*PMNSL3x2*yDelta3x2 + PMNSL3x2**2*yDelta3x3',
                   texname = '\\text{I62a22}')

I62a23 = Parameter(name = 'I62a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*PMNSL1x3*yDelta1x1 + PMNSL1x2*PMNSL2x3*yDelta1x2 + PMNSL1x2*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x2*yDelta2x1 + PMNSL2x2*PMNSL2x3*yDelta2x2 + PMNSL2x2*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x2*yDelta3x1 + PMNSL2x3*PMNSL3x2*yDelta3x2 + PMNSL3x2*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I62a23}')

I62a31 = Parameter(name = 'I62a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*PMNSL1x3*yDelta1x1 + PMNSL1x3*PMNSL2x1*yDelta1x2 + PMNSL1x3*PMNSL3x1*yDelta1x3 + PMNSL1x1*PMNSL2x3*yDelta2x1 + PMNSL2x1*PMNSL2x3*yDelta2x2 + PMNSL2x3*PMNSL3x1*yDelta2x3 + PMNSL1x1*PMNSL3x3*yDelta3x1 + PMNSL2x1*PMNSL3x3*yDelta3x2 + PMNSL3x1*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I62a31}')

I62a32 = Parameter(name = 'I62a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*PMNSL1x3*yDelta1x1 + PMNSL1x3*PMNSL2x2*yDelta1x2 + PMNSL1x3*PMNSL3x2*yDelta1x3 + PMNSL1x2*PMNSL2x3*yDelta2x1 + PMNSL2x2*PMNSL2x3*yDelta2x2 + PMNSL2x3*PMNSL3x2*yDelta2x3 + PMNSL1x2*PMNSL3x3*yDelta3x1 + PMNSL2x2*PMNSL3x3*yDelta3x2 + PMNSL3x2*PMNSL3x3*yDelta3x3',
                   texname = '\\text{I62a32}')

I62a33 = Parameter(name = 'I62a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3**2*yDelta1x1 + PMNSL1x3*PMNSL2x3*yDelta1x2 + PMNSL1x3*PMNSL3x3*yDelta1x3 + PMNSL1x3*PMNSL2x3*yDelta2x1 + PMNSL2x3**2*yDelta2x2 + PMNSL2x3*PMNSL3x3*yDelta2x3 + PMNSL1x3*PMNSL3x3*yDelta3x1 + PMNSL2x3*PMNSL3x3*yDelta3x2 + PMNSL3x3**2*yDelta3x3',
                   texname = '\\text{I62a33}')

I63a11 = Parameter(name = 'I63a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x1)',
                   texname = '\\text{I63a11}')

I63a12 = Parameter(name = 'I63a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x1)',
                   texname = '\\text{I63a12}')

I63a13 = Parameter(name = 'I63a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x1)',
                   texname = '\\text{I63a13}')

I63a21 = Parameter(name = 'I63a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x2)',
                   texname = '\\text{I63a21}')

I63a22 = Parameter(name = 'I63a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x2)',
                   texname = '\\text{I63a22}')

I63a23 = Parameter(name = 'I63a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x2)',
                   texname = '\\text{I63a23}')

I63a31 = Parameter(name = 'I63a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I63a31}')

I63a32 = Parameter(name = 'I63a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I63a32}')

I63a33 = Parameter(name = 'I63a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x3) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x3) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I63a33}')

I64a11 = Parameter(name = 'I64a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta1x3)',
                   texname = '\\text{I64a11}')

I64a12 = Parameter(name = 'I64a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta1x3)',
                   texname = '\\text{I64a12}')

I64a13 = Parameter(name = 'I64a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta1x1) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta1x2) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta1x3)',
                   texname = '\\text{I64a13}')

I64a21 = Parameter(name = 'I64a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta2x3)',
                   texname = '\\text{I64a21}')

I64a22 = Parameter(name = 'I64a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta2x3)',
                   texname = '\\text{I64a22}')

I64a23 = Parameter(name = 'I64a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta2x1) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta2x2) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta2x3)',
                   texname = '\\text{I64a23}')

I64a31 = Parameter(name = 'I64a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x1)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x1)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x1)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I64a31}')

I64a32 = Parameter(name = 'I64a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x2)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x2)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x2)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I64a32}')

I64a33 = Parameter(name = 'I64a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'complexconjugate(PMNSL1x3)*complexconjugate(yDelta3x1) + complexconjugate(PMNSL2x3)*complexconjugate(yDelta3x2) + complexconjugate(PMNSL3x3)*complexconjugate(yDelta3x3)',
                   texname = '\\text{I64a33}')

I67a11 = Parameter(name = 'I67a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x1*Yu3x3*complexconjugate(CKMR3x1)',
                   texname = '\\text{I67a11}')

I67a12 = Parameter(name = 'I67a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x2*Yu3x3*complexconjugate(CKMR3x1)',
                   texname = '\\text{I67a12}')

I67a13 = Parameter(name = 'I67a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yu3x3*complexconjugate(CKMR3x1)',
                   texname = '\\text{I67a13}')

I67a21 = Parameter(name = 'I67a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x1*Yu3x3*complexconjugate(CKMR3x2)',
                   texname = '\\text{I67a21}')

I67a22 = Parameter(name = 'I67a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x2*Yu3x3*complexconjugate(CKMR3x2)',
                   texname = '\\text{I67a22}')

I67a23 = Parameter(name = 'I67a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yu3x3*complexconjugate(CKMR3x2)',
                   texname = '\\text{I67a23}')

I67a31 = Parameter(name = 'I67a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x1*Yu3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I67a31}')

I67a32 = Parameter(name = 'I67a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x2*Yu3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I67a32}')

I67a33 = Parameter(name = 'I67a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yu3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I67a33}')

I68a11 = Parameter(name = 'I68a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x1*Yu3x3*complexconjugate(CKML3x1)',
                   texname = '\\text{I68a11}')

I68a12 = Parameter(name = 'I68a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x2*Yu3x3*complexconjugate(CKML3x1)',
                   texname = '\\text{I68a12}')

I68a13 = Parameter(name = 'I68a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yu3x3*complexconjugate(CKML3x1)',
                   texname = '\\text{I68a13}')

I68a21 = Parameter(name = 'I68a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x1*Yu3x3*complexconjugate(CKML3x2)',
                   texname = '\\text{I68a21}')

I68a22 = Parameter(name = 'I68a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x2*Yu3x3*complexconjugate(CKML3x2)',
                   texname = '\\text{I68a22}')

I68a23 = Parameter(name = 'I68a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yu3x3*complexconjugate(CKML3x2)',
                   texname = '\\text{I68a23}')

I68a31 = Parameter(name = 'I68a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x1*Yu3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I68a31}')

I68a32 = Parameter(name = 'I68a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x2*Yu3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I68a32}')

I68a33 = Parameter(name = 'I68a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yu3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I68a33}')

I69a13 = Parameter(name = 'I69a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML1x3*Yd3x3',
                   texname = '\\text{I69a13}')

I69a23 = Parameter(name = 'I69a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML2x3*Yd3x3',
                   texname = '\\text{I69a23}')

I69a33 = Parameter(name = 'I69a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yd3x3',
                   texname = '\\text{I69a33}')

I70a13 = Parameter(name = 'I70a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR1x3*Yd3x3',
                   texname = '\\text{I70a13}')

I70a23 = Parameter(name = 'I70a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR2x3*Yd3x3',
                   texname = '\\text{I70a23}')

I70a33 = Parameter(name = 'I70a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yd3x3',
                   texname = '\\text{I70a33}')

I71a31 = Parameter(name = 'I71a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x1*Yu3x3',
                   texname = '\\text{I71a31}')

I71a32 = Parameter(name = 'I71a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x2*Yu3x3',
                   texname = '\\text{I71a32}')

I71a33 = Parameter(name = 'I71a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yu3x3',
                   texname = '\\text{I71a33}')

I72a31 = Parameter(name = 'I72a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x1*Yu3x3',
                   texname = '\\text{I72a31}')

I72a32 = Parameter(name = 'I72a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x2*Yu3x3',
                   texname = '\\text{I72a32}')

I72a33 = Parameter(name = 'I72a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yu3x3',
                   texname = '\\text{I72a33}')

I73a31 = Parameter(name = 'I73a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKML1x3)',
                   texname = '\\text{I73a31}')

I73a32 = Parameter(name = 'I73a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKML2x3)',
                   texname = '\\text{I73a32}')

I73a33 = Parameter(name = 'I73a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I73a33}')

I74a31 = Parameter(name = 'I74a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKMR1x3)',
                   texname = '\\text{I74a31}')

I74a32 = Parameter(name = 'I74a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKMR2x3)',
                   texname = '\\text{I74a32}')

I74a33 = Parameter(name = 'I74a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yd3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I74a33}')

I75a13 = Parameter(name = 'I75a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKML3x1)',
                   texname = '\\text{I75a13}')

I75a23 = Parameter(name = 'I75a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKML3x2)',
                   texname = '\\text{I75a23}')

I75a33 = Parameter(name = 'I75a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I75a33}')

I76a13 = Parameter(name = 'I76a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKMR3x1)',
                   texname = '\\text{I76a13}')

I76a23 = Parameter(name = 'I76a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKMR3x2)',
                   texname = '\\text{I76a23}')

I76a33 = Parameter(name = 'I76a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'Yu3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I76a33}')

I77a11 = Parameter(name = 'I77a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML1x3*Yd3x3*complexconjugate(CKMR1x3)',
                   texname = '\\text{I77a11}')

I77a12 = Parameter(name = 'I77a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML1x3*Yd3x3*complexconjugate(CKMR2x3)',
                   texname = '\\text{I77a12}')

I77a13 = Parameter(name = 'I77a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML1x3*Yd3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I77a13}')

I77a21 = Parameter(name = 'I77a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML2x3*Yd3x3*complexconjugate(CKMR1x3)',
                   texname = '\\text{I77a21}')

I77a22 = Parameter(name = 'I77a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML2x3*Yd3x3*complexconjugate(CKMR2x3)',
                   texname = '\\text{I77a22}')

I77a23 = Parameter(name = 'I77a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML2x3*Yd3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I77a23}')

I77a31 = Parameter(name = 'I77a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yd3x3*complexconjugate(CKMR1x3)',
                   texname = '\\text{I77a31}')

I77a32 = Parameter(name = 'I77a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yd3x3*complexconjugate(CKMR2x3)',
                   texname = '\\text{I77a32}')

I77a33 = Parameter(name = 'I77a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKML3x3*Yd3x3*complexconjugate(CKMR3x3)',
                   texname = '\\text{I77a33}')

I78a11 = Parameter(name = 'I78a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR1x3*Yd3x3*complexconjugate(CKML1x3)',
                   texname = '\\text{I78a11}')

I78a12 = Parameter(name = 'I78a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR1x3*Yd3x3*complexconjugate(CKML2x3)',
                   texname = '\\text{I78a12}')

I78a13 = Parameter(name = 'I78a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR1x3*Yd3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I78a13}')

I78a21 = Parameter(name = 'I78a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR2x3*Yd3x3*complexconjugate(CKML1x3)',
                   texname = '\\text{I78a21}')

I78a22 = Parameter(name = 'I78a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR2x3*Yd3x3*complexconjugate(CKML2x3)',
                   texname = '\\text{I78a22}')

I78a23 = Parameter(name = 'I78a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR2x3*Yd3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I78a23}')

I78a31 = Parameter(name = 'I78a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yd3x3*complexconjugate(CKML1x3)',
                   texname = '\\text{I78a31}')

I78a32 = Parameter(name = 'I78a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yd3x3*complexconjugate(CKML2x3)',
                   texname = '\\text{I78a32}')

I78a33 = Parameter(name = 'I78a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'CKMR3x3*Yd3x3*complexconjugate(CKML3x3)',
                   texname = '\\text{I78a33}')

I79a11 = Parameter(name = 'I79a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta1x1 + PMNSL2x1*yDelta2x1 + PMNSL3x1*yDelta3x1',
                   texname = '\\text{I79a11}')

I79a12 = Parameter(name = 'I79a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta1x2 + PMNSL2x1*yDelta2x2 + PMNSL3x1*yDelta3x2',
                   texname = '\\text{I79a12}')

I79a13 = Parameter(name = 'I79a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta1x3 + PMNSL2x1*yDelta2x3 + PMNSL3x1*yDelta3x3',
                   texname = '\\text{I79a13}')

I79a21 = Parameter(name = 'I79a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta1x1 + PMNSL2x2*yDelta2x1 + PMNSL3x2*yDelta3x1',
                   texname = '\\text{I79a21}')

I79a22 = Parameter(name = 'I79a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta1x2 + PMNSL2x2*yDelta2x2 + PMNSL3x2*yDelta3x2',
                   texname = '\\text{I79a22}')

I79a23 = Parameter(name = 'I79a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta1x3 + PMNSL2x2*yDelta2x3 + PMNSL3x2*yDelta3x3',
                   texname = '\\text{I79a23}')

I79a31 = Parameter(name = 'I79a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta1x1 + PMNSL2x3*yDelta2x1 + PMNSL3x3*yDelta3x1',
                   texname = '\\text{I79a31}')

I79a32 = Parameter(name = 'I79a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta1x2 + PMNSL2x3*yDelta2x2 + PMNSL3x3*yDelta3x2',
                   texname = '\\text{I79a32}')

I79a33 = Parameter(name = 'I79a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta1x3 + PMNSL2x3*yDelta2x3 + PMNSL3x3*yDelta3x3',
                   texname = '\\text{I79a33}')

I8a11 = Parameter(name = 'I8a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x1)*complexconjugate(PMNSR3x1)',
                  texname = '\\text{I8a11}')

I8a12 = Parameter(name = 'I8a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x1)*complexconjugate(PMNSR3x2)',
                  texname = '\\text{I8a12}')

I8a13 = Parameter(name = 'I8a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x1)*complexconjugate(PMNSR3x3)',
                  texname = '\\text{I8a13}')

I8a21 = Parameter(name = 'I8a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x2)*complexconjugate(PMNSR3x1)',
                  texname = '\\text{I8a21}')

I8a22 = Parameter(name = 'I8a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x2)*complexconjugate(PMNSR3x2)',
                  texname = '\\text{I8a22}')

I8a23 = Parameter(name = 'I8a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x2)*complexconjugate(PMNSR3x3)',
                  texname = '\\text{I8a23}')

I8a31 = Parameter(name = 'I8a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x3)*complexconjugate(PMNSR3x1)',
                  texname = '\\text{I8a31}')

I8a32 = Parameter(name = 'I8a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x3)*complexconjugate(PMNSR3x2)',
                  texname = '\\text{I8a32}')

I8a33 = Parameter(name = 'I8a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'Ylep3x3*complexconjugate(PMNSL3x3)*complexconjugate(PMNSR3x3)',
                  texname = '\\text{I8a33}')

I80a11 = Parameter(name = 'I80a11',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta1x1 + PMNSL2x1*yDelta1x2 + PMNSL3x1*yDelta1x3',
                   texname = '\\text{I80a11}')

I80a12 = Parameter(name = 'I80a12',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta2x1 + PMNSL2x1*yDelta2x2 + PMNSL3x1*yDelta2x3',
                   texname = '\\text{I80a12}')

I80a13 = Parameter(name = 'I80a13',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x1*yDelta3x1 + PMNSL2x1*yDelta3x2 + PMNSL3x1*yDelta3x3',
                   texname = '\\text{I80a13}')

I80a21 = Parameter(name = 'I80a21',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta1x1 + PMNSL2x2*yDelta1x2 + PMNSL3x2*yDelta1x3',
                   texname = '\\text{I80a21}')

I80a22 = Parameter(name = 'I80a22',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta2x1 + PMNSL2x2*yDelta2x2 + PMNSL3x2*yDelta2x3',
                   texname = '\\text{I80a22}')

I80a23 = Parameter(name = 'I80a23',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x2*yDelta3x1 + PMNSL2x2*yDelta3x2 + PMNSL3x2*yDelta3x3',
                   texname = '\\text{I80a23}')

I80a31 = Parameter(name = 'I80a31',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta1x1 + PMNSL2x3*yDelta1x2 + PMNSL3x3*yDelta1x3',
                   texname = '\\text{I80a31}')

I80a32 = Parameter(name = 'I80a32',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta2x1 + PMNSL2x3*yDelta2x2 + PMNSL3x3*yDelta2x3',
                   texname = '\\text{I80a32}')

I80a33 = Parameter(name = 'I80a33',
                   nature = 'internal',
                   type = 'complex',
                   value = 'PMNSL1x3*yDelta3x1 + PMNSL2x3*yDelta3x2 + PMNSL3x3*yDelta3x3',
                   texname = '\\text{I80a33}')

