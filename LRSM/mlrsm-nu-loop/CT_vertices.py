# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Fri 20 Sep 2024 20:29:56


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_305_359,(0,1,1):C.R2GC_238_302})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV11, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,1,0):C.R2GC_251_316,(2,1,1):C.R2GC_251_317,(0,1,0):C.R2GC_251_316,(0,1,1):C.R2GC_251_317,(4,1,0):C.R2GC_249_312,(4,1,1):C.R2GC_249_313,(3,1,0):C.R2GC_249_312,(3,1,1):C.R2GC_249_313,(8,1,0):C.R2GC_250_314,(8,1,1):C.R2GC_250_315,(6,1,0):C.R2GC_254_321,(6,1,1):C.R2GC_310_365,(7,1,0):C.R2GC_255_322,(7,1,1):C.R2GC_309_364,(5,1,0):C.R2GC_249_312,(5,1,1):C.R2GC_249_313,(1,1,0):C.R2GC_249_312,(1,1,1):C.R2GC_249_313,(11,0,0):C.R2GC_253_319,(11,0,1):C.R2GC_253_320,(10,0,0):C.R2GC_253_319,(10,0,1):C.R2GC_253_320,(9,0,1):C.R2GC_252_318,(0,2,0):C.R2GC_251_316,(0,2,1):C.R2GC_251_317,(2,2,0):C.R2GC_251_316,(2,2,1):C.R2GC_251_317,(5,2,0):C.R2GC_249_312,(5,2,1):C.R2GC_249_313,(1,2,0):C.R2GC_249_312,(1,2,1):C.R2GC_249_313,(7,2,0):C.R2GC_255_322,(7,2,1):C.R2GC_250_315,(4,2,0):C.R2GC_249_312,(4,2,1):C.R2GC_249_313,(3,2,0):C.R2GC_249_312,(3,2,1):C.R2GC_249_313,(8,2,0):C.R2GC_250_314,(8,2,1):C.R2GC_309_364,(6,2,0):C.R2GC_306_360,(6,2,1):C.R2GC_306_361,(0,3,0):C.R2GC_251_316,(0,3,1):C.R2GC_251_317,(2,3,0):C.R2GC_251_316,(2,3,1):C.R2GC_251_317,(5,3,0):C.R2GC_249_312,(5,3,1):C.R2GC_249_313,(1,3,0):C.R2GC_249_312,(1,3,1):C.R2GC_249_313,(7,3,0):C.R2GC_307_362,(7,3,1):C.R2GC_307_363,(4,3,0):C.R2GC_249_312,(4,3,1):C.R2GC_249_313,(3,3,0):C.R2GC_249_312,(3,3,1):C.R2GC_249_313,(8,3,0):C.R2GC_250_314,(8,3,1):C.R2GC_307_363,(6,3,0):C.R2GC_254_321})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.Phi0L ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_1739_286})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.d__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_750_495,(0,1,0):C.R2GC_702_447})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.s__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_751_496,(0,1,0):C.R2GC_703_448})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.b__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.b, P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_752_497,(0,1,0):C.R2GC_704_449})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.d__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_762_507,(0,1,0):C.R2GC_714_459})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.s__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_763_508,(0,1,0):C.R2GC_715_460})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.b__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.b, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_764_509,(0,1,0):C.R2GC_716_461})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_782_527,(0,1,0):C.R2GC_734_479})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_783_528,(0,1,0):C.R2GC_735_480})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1434_90,(0,1,0):C.R2GC_1433_89})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_753_498,(0,1,0):C.R2GC_705_450})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_754_499,(0,1,0):C.R2GC_706_451})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_755_500,(0,1,0):C.R2GC_707_452})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_765_510,(0,1,0):C.R2GC_717_462})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_766_511,(0,1,0):C.R2GC_718_463})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_767_512,(0,1,0):C.R2GC_719_464})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_784_529,(0,1,0):C.R2GC_736_481})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_785_530,(0,1,0):C.R2GC_737_482})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1436_92,(0,1,0):C.R2GC_1435_91})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_756_501,(0,1,0):C.R2GC_708_453})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_757_502,(0,1,0):C.R2GC_709_454})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_758_503,(0,1,0):C.R2GC_710_455})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_768_513,(0,1,0):C.R2GC_720_465})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_769_514,(0,1,0):C.R2GC_721_466})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_770_515,(0,1,0):C.R2GC_722_467})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_786_531,(0,1,0):C.R2GC_738_483})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_787_532,(0,1,0):C.R2GC_739_484})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,2,0):C.R2GC_291_344,(0,0,0):C.R2GC_1438_94,(0,1,0):C.R2GC_1437_93})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_759_504,(0,1,0):C.R2GC_711_456})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_760_505,(0,1,0):C.R2GC_712_457})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_761_506,(0,1,0):C.R2GC_713_458})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_771_516,(0,1,0):C.R2GC_723_468})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_772_517,(0,1,0):C.R2GC_724_469})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_773_518,(0,1,0):C.R2GC_725_470})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_788_533,(0,1,0):C.R2GC_740_485})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_789_534,(0,1,0):C.R2GC_741_486})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,2,0):C.R2GC_292_345,(0,0,0):C.R2GC_1440_96,(0,1,0):C.R2GC_1439_95})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Phi0R ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1531_276})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_844_535,(0,0,0):C.R2GC_1477_251})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_845_536,(0,0,0):C.R2GC_1478_252})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_848_539,(0,1,0):C.R2GC_1481_255})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_849_540,(0,1,0):C.R2GC_1482_256})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_894_548,(0,1,0):C.R2GC_895_549})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_339_391,(0,1,0):C.R2GC_1431_87})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_340_392,(0,1,0):C.R2GC_1432_88})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_343_395,(0,0,0):C.R2GC_1443_99})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_344_396,(0,0,0):C.R2GC_1444_100})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1456_112,(0,1,0):C.R2GC_1455_111})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_289_342})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_290_343})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_316_368})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_317_369})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_315_367,(0,1,0):C.R2GC_318_370})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_345_397,(0,1,0):C.R2GC_1453_109})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_346_398,(0,1,0):C.R2GC_1454_110})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.u__tilde__, P.b, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_341_393,(0,0,0):C.R2GC_1441_97})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.c__tilde__, P.b, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_342_394,(0,0,0):C.R2GC_1442_98})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1457_113,(0,1,0):C.R2GC_1458_114})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_662_407,(0,1,0):C.R2GC_678_423})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.c__tilde__, P.u, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_670_415,(0,1,0):C.R2GC_686_431})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.t__tilde__, P.u, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_726_471,(0,1,0):C.R2GC_774_519})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.u__tilde__, P.c, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_663_408,(0,1,0):C.R2GC_679_424})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_671_416,(0,1,0):C.R2GC_687_432})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.t__tilde__, P.c, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_727_472,(0,1,0):C.R2GC_775_520})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.u__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_694_439,(0,1,0):C.R2GC_742_487})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.c__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_698_443,(0,1,0):C.R2GC_746_491})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_319_371,(0,0,0):C.R2GC_1445_101,(0,1,0):C.R2GC_1446_102})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_664_409,(0,1,0):C.R2GC_680_425})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.c__tilde__, P.u, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_672_417,(0,1,0):C.R2GC_688_433})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.t__tilde__, P.u, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_728_473,(0,1,0):C.R2GC_776_521})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.u__tilde__, P.c, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_665_410,(0,1,0):C.R2GC_681_426})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_673_418,(0,1,0):C.R2GC_689_434})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.t__tilde__, P.c, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_729_474,(0,1,0):C.R2GC_777_522})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.u__tilde__, P.t, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_695_440,(0,1,0):C.R2GC_743_488})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.c__tilde__, P.t, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_699_444,(0,1,0):C.R2GC_747_492})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_320_372,(0,0,0):C.R2GC_1447_103,(0,1,0):C.R2GC_1448_104})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_666_411,(0,1,0):C.R2GC_682_427})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.c__tilde__, P.u, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_674_419,(0,1,0):C.R2GC_690_435})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.t__tilde__, P.u, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_730_475,(0,1,0):C.R2GC_778_523})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.u__tilde__, P.c, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_667_412,(0,1,0):C.R2GC_683_428})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_675_420,(0,1,0):C.R2GC_691_436})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.t__tilde__, P.c, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_731_476,(0,1,0):C.R2GC_779_524})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.u__tilde__, P.t, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_696_441,(0,1,0):C.R2GC_744_489})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.c__tilde__, P.t, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_700_445,(0,1,0):C.R2GC_748_493})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_321_373,(0,0,0):C.R2GC_1449_105,(0,1,0):C.R2GC_1450_106})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_668_413,(0,1,0):C.R2GC_684_429})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.c__tilde__, P.u, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_676_421,(0,1,0):C.R2GC_692_437})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.t__tilde__, P.u, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_732_477,(0,1,0):C.R2GC_780_525})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.u__tilde__, P.c, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_669_414,(0,1,0):C.R2GC_685_430})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_677_422,(0,1,0):C.R2GC_693_438})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.t__tilde__, P.c, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_733_478,(0,1,0):C.R2GC_781_526})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.u__tilde__, P.t, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.t, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_697_442,(0,1,0):C.R2GC_745_490})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.c__tilde__, P.t, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_701_446,(0,1,0):C.R2GC_749_494})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_322_374,(0,0,0):C.R2GC_1451_107,(0,1,0):C.R2GC_1452_108})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_323_375})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_324_376})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.u__tilde__, P.b, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_293_346})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_294_347})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_326_378,(0,1,0):C.R2GC_325_377})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1740_287})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.d, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_850_541,(0,0,0):C.R2GC_1483_257})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.s, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_851_542,(0,0,0):C.R2GC_1484_258})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_846_537,(0,1,0):C.R2GC_1479_253})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_847_538,(0,1,0):C.R2GC_1480_254})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_897_551,(0,1,0):C.R2GC_896_550})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1532_277})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_946_555})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_946_555})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_946_555})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1697_282,(0,1,0):C.R2GC_1698_283})

V_113 = CTVertex(name = 'V_113',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_1697_282,(0,1,0):C.R2GC_1698_283})

V_114 = CTVertex(name = 'V_114',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1697_282,(0,1,0):C.R2GC_1698_283})

V_115 = CTVertex(name = 'V_115',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_2073_298,(0,1,0):C.R2GC_2074_299})

V_116 = CTVertex(name = 'V_116',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_2073_298,(0,1,0):C.R2GC_2074_299})

V_117 = CTVertex(name = 'V_117',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_2073_298,(0,1,0):C.R2GC_2074_299})

V_118 = CTVertex(name = 'V_118',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_278_333,(0,1,0):C.R2GC_1126_28})

V_119 = CTVertex(name = 'V_119',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_282_337,(0,1,0):C.R2GC_1130_32})

V_120 = CTVertex(name = 'V_120',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_295_348,(0,1,0):C.R2GC_1135_37})

V_121 = CTVertex(name = 'V_121',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_270_325,(0,1,0):C.R2GC_1118_20})

V_122 = CTVertex(name = 'V_122',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_274_329,(0,1,0):C.R2GC_1122_24})

V_123 = CTVertex(name = 'V_123',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_296_349,(0,1,0):C.R2GC_1136_38})

V_124 = CTVertex(name = 'V_124',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_327_379,(0,1,0):C.R2GC_1144_46})

V_125 = CTVertex(name = 'V_125',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_328_380,(0,1,0):C.R2GC_1145_47})

V_126 = CTVertex(name = 'V_126',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_329_381,(0,1,0):C.R2GC_1146_48})

V_127 = CTVertex(name = 'V_127',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_279_334,(0,0,0):C.R2GC_1125_27})

V_128 = CTVertex(name = 'V_128',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_283_338,(0,0,0):C.R2GC_1129_31})

V_129 = CTVertex(name = 'V_129',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_297_350,(0,0,0):C.R2GC_1133_35})

V_130 = CTVertex(name = 'V_130',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_271_326,(0,0,0):C.R2GC_1117_19})

V_131 = CTVertex(name = 'V_131',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.R2GC_275_330,(0,0,0):C.R2GC_1121_23})

V_132 = CTVertex(name = 'V_132',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_298_351,(0,0,0):C.R2GC_1134_36})

V_133 = CTVertex(name = 'V_133',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_330_382,(0,0,0):C.R2GC_1141_43})

V_134 = CTVertex(name = 'V_134',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_331_383,(0,0,0):C.R2GC_1142_44})

V_135 = CTVertex(name = 'V_135',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_332_384,(0,0,0):C.R2GC_1143_45})

V_136 = CTVertex(name = 'V_136',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_137 = CTVertex(name = 'V_137',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_138 = CTVertex(name = 'V_138',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_139 = CTVertex(name = 'V_139',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_945_554})

V_140 = CTVertex(name = 'V_140',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_945_554})

V_141 = CTVertex(name = 'V_141',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_945_554})

V_142 = CTVertex(name = 'V_142',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_1699_284,(0,1,0):C.R2GC_1700_285})

V_143 = CTVertex(name = 'V_143',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_1699_284,(0,1,0):C.R2GC_1700_285})

V_144 = CTVertex(name = 'V_144',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_1699_284,(0,1,0):C.R2GC_1700_285})

V_145 = CTVertex(name = 'V_145',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_2075_300,(0,1,0):C.R2GC_2076_301})

V_146 = CTVertex(name = 'V_146',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_2075_300,(0,1,0):C.R2GC_2076_301})

V_147 = CTVertex(name = 'V_147',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_2075_300,(0,1,0):C.R2GC_2076_301})

V_148 = CTVertex(name = 'V_148',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_280_335,(0,1,0):C.R2GC_1128_30})

V_149 = CTVertex(name = 'V_149',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_272_327,(0,1,0):C.R2GC_1120_22})

V_150 = CTVertex(name = 'V_150',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_333_385,(0,1,0):C.R2GC_1150_52})

V_151 = CTVertex(name = 'V_151',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_284_339,(0,1,0):C.R2GC_1132_34})

V_152 = CTVertex(name = 'V_152',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_276_331,(0,1,0):C.R2GC_1124_26})

V_153 = CTVertex(name = 'V_153',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_334_386,(0,1,0):C.R2GC_1151_53})

V_154 = CTVertex(name = 'V_154',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_299_352,(0,1,0):C.R2GC_1139_41})

V_155 = CTVertex(name = 'V_155',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_300_353,(0,1,0):C.R2GC_1140_42})

V_156 = CTVertex(name = 'V_156',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_335_387,(0,1,0):C.R2GC_1152_54})

V_157 = CTVertex(name = 'V_157',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_281_336,(0,0,0):C.R2GC_1127_29})

V_158 = CTVertex(name = 'V_158',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_273_328,(0,0,0):C.R2GC_1119_21})

V_159 = CTVertex(name = 'V_159',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_336_388,(0,0,0):C.R2GC_1147_49})

V_160 = CTVertex(name = 'V_160',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_285_340,(0,0,0):C.R2GC_1131_33})

V_161 = CTVertex(name = 'V_161',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.R2GC_277_332,(0,0,0):C.R2GC_1123_25})

V_162 = CTVertex(name = 'V_162',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_337_389,(0,0,0):C.R2GC_1148_50})

V_163 = CTVertex(name = 'V_163',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_301_354,(0,0,0):C.R2GC_1137_39})

V_164 = CTVertex(name = 'V_164',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_302_355,(0,0,0):C.R2GC_1138_40})

V_165 = CTVertex(name = 'V_165',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_338_390,(0,0,0):C.R2GC_1149_51})

V_166 = CTVertex(name = 'V_166',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_167 = CTVertex(name = 'V_167',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_168 = CTVertex(name = 'V_168',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_256_323})

V_169 = CTVertex(name = 'V_169',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_257_324})

V_170 = CTVertex(name = 'V_170',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_257_324})

V_171 = CTVertex(name = 'V_171',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_314_366,(0,2,0):C.R2GC_314_366,(0,1,0):C.R2GC_257_324,(0,3,0):C.R2GC_257_324})

V_172 = CTVertex(name = 'V_172',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_257_324})

V_173 = CTVertex(name = 'V_173',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_257_324})

V_174 = CTVertex(name = 'V_174',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_288_341,(0,2,0):C.R2GC_288_341,(0,1,0):C.R2GC_257_324,(0,3,0):C.R2GC_257_324})

V_175 = CTVertex(name = 'V_175',
                 type = 'R2',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV2, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                 couplings = {(0,0,2):C.R2GC_304_358,(0,1,0):C.R2GC_241_305,(0,1,3):C.R2GC_241_306,(0,2,1):C.R2GC_303_356,(0,2,2):C.R2GC_303_357})

V_176 = CTVertex(name = 'V_176',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1425_73,(0,0,1):C.R2GC_1425_74})

V_177 = CTVertex(name = 'V_177',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1426_75,(0,0,1):C.R2GC_1426_76})

V_178 = CTVertex(name = 'V_178',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1427_77,(0,0,1):C.R2GC_1427_78})

V_179 = CTVertex(name = 'V_179',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1428_79,(0,0,1):C.R2GC_1428_80})

V_180 = CTVertex(name = 'V_180',
                 type = 'R2',
                 particles = [ P.a, P.a, P.g, P.g ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_965_556,(0,0,1):C.R2GC_965_557,(0,1,0):C.R2GC_965_556,(0,1,1):C.R2GC_965_557,(0,2,0):C.R2GC_965_556,(0,2,1):C.R2GC_965_557})

V_181 = CTVertex(name = 'V_181',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__plus__, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1115_1,(0,0,1):C.R2GC_1115_2,(0,0,2):C.R2GC_1115_3,(0,0,3):C.R2GC_1115_4,(0,0,4):C.R2GC_1115_5,(0,0,5):C.R2GC_1115_6,(0,0,6):C.R2GC_1115_7,(0,0,7):C.R2GC_1115_8,(0,0,8):C.R2GC_1115_9,(0,1,0):C.R2GC_1115_1,(0,1,1):C.R2GC_1115_2,(0,1,2):C.R2GC_1115_3,(0,1,3):C.R2GC_1115_4,(0,1,4):C.R2GC_1115_5,(0,1,5):C.R2GC_1115_6,(0,1,6):C.R2GC_1115_7,(0,1,7):C.R2GC_1115_8,(0,1,8):C.R2GC_1115_9,(0,2,0):C.R2GC_1115_1,(0,2,1):C.R2GC_1115_2,(0,2,2):C.R2GC_1115_3,(0,2,3):C.R2GC_1115_4,(0,2,4):C.R2GC_1115_5,(0,2,5):C.R2GC_1115_6,(0,2,6):C.R2GC_1115_7,(0,2,7):C.R2GC_1115_8,(0,2,8):C.R2GC_1115_9})

V_182 = CTVertex(name = 'V_182',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1116_10,(0,0,1):C.R2GC_1116_11,(0,0,2):C.R2GC_1116_12,(0,0,3):C.R2GC_1116_13,(0,0,4):C.R2GC_1116_14,(0,0,5):C.R2GC_1116_15,(0,0,6):C.R2GC_1116_16,(0,0,7):C.R2GC_1116_17,(0,0,8):C.R2GC_1116_18,(0,1,0):C.R2GC_1116_10,(0,1,1):C.R2GC_1116_11,(0,1,2):C.R2GC_1116_12,(0,1,3):C.R2GC_1116_13,(0,1,4):C.R2GC_1116_14,(0,1,5):C.R2GC_1116_15,(0,1,6):C.R2GC_1116_16,(0,1,7):C.R2GC_1116_17,(0,1,8):C.R2GC_1116_18,(0,2,0):C.R2GC_1116_10,(0,2,1):C.R2GC_1116_11,(0,2,2):C.R2GC_1116_12,(0,2,3):C.R2GC_1116_13,(0,2,4):C.R2GC_1116_14,(0,2,5):C.R2GC_1116_15,(0,2,6):C.R2GC_1116_16,(0,2,7):C.R2GC_1116_17,(0,2,8):C.R2GC_1116_18})

V_183 = CTVertex(name = 'V_183',
                 type = 'R2',
                 particles = [ P.g, P.g, P.WR__minus__, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1318_55,(0,0,1):C.R2GC_1318_56,(0,0,2):C.R2GC_1318_57,(0,0,3):C.R2GC_1318_58,(0,0,4):C.R2GC_1318_59,(0,0,5):C.R2GC_1318_60,(0,0,6):C.R2GC_1318_61,(0,0,7):C.R2GC_1318_62,(0,0,8):C.R2GC_1318_63,(0,1,0):C.R2GC_1318_55,(0,1,1):C.R2GC_1318_56,(0,1,2):C.R2GC_1318_57,(0,1,3):C.R2GC_1318_58,(0,1,4):C.R2GC_1318_59,(0,1,5):C.R2GC_1318_60,(0,1,6):C.R2GC_1318_61,(0,1,7):C.R2GC_1318_62,(0,1,8):C.R2GC_1318_63,(0,2,0):C.R2GC_1318_55,(0,2,1):C.R2GC_1318_56,(0,2,2):C.R2GC_1318_57,(0,2,3):C.R2GC_1318_58,(0,2,4):C.R2GC_1318_59,(0,2,5):C.R2GC_1318_60,(0,2,6):C.R2GC_1318_61,(0,2,7):C.R2GC_1318_62,(0,2,8):C.R2GC_1318_63})

V_184 = CTVertex(name = 'V_184',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1319_64,(0,0,1):C.R2GC_1319_65,(0,0,2):C.R2GC_1319_66,(0,0,3):C.R2GC_1319_67,(0,0,4):C.R2GC_1319_68,(0,0,5):C.R2GC_1319_69,(0,0,6):C.R2GC_1319_70,(0,0,7):C.R2GC_1319_71,(0,0,8):C.R2GC_1319_72,(0,1,0):C.R2GC_1319_64,(0,1,1):C.R2GC_1319_65,(0,1,2):C.R2GC_1319_66,(0,1,3):C.R2GC_1319_67,(0,1,4):C.R2GC_1319_68,(0,1,5):C.R2GC_1319_69,(0,1,6):C.R2GC_1319_70,(0,1,7):C.R2GC_1319_71,(0,1,8):C.R2GC_1319_72,(0,2,0):C.R2GC_1319_64,(0,2,1):C.R2GC_1319_65,(0,2,2):C.R2GC_1319_66,(0,2,3):C.R2GC_1319_67,(0,2,4):C.R2GC_1319_68,(0,2,5):C.R2GC_1319_69,(0,2,6):C.R2GC_1319_70,(0,2,7):C.R2GC_1319_71,(0,2,8):C.R2GC_1319_72})

V_185 = CTVertex(name = 'V_185',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.g ],
                 color = [ 'd(2,3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_944_552,(0,0,1):C.R2GC_944_553,(0,1,0):C.R2GC_944_552,(0,1,1):C.R2GC_944_553,(0,2,0):C.R2GC_944_552,(0,2,1):C.R2GC_944_553})

V_186 = CTVertex(name = 'V_186',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.Z ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_1696_280,(0,1,1):C.R2GC_1696_281,(0,2,0):C.R2GC_1696_280,(0,2,1):C.R2GC_1696_281,(0,3,0):C.R2GC_1696_280,(0,3,1):C.R2GC_1696_281,(1,0,0):C.R2GC_348_401,(1,0,1):C.R2GC_348_402})

V_187 = CTVertex(name = 'V_187',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.Z ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1495_274,(0,0,1):C.R2GC_1495_275,(0,1,0):C.R2GC_1495_274,(0,1,1):C.R2GC_1495_275,(0,2,0):C.R2GC_1495_274,(0,2,1):C.R2GC_1495_275})

V_188 = CTVertex(name = 'V_188',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_350_405,(0,0,1):C.R2GC_350_406,(0,1,0):C.R2GC_350_405,(0,1,1):C.R2GC_350_406,(0,2,0):C.R2GC_350_405,(0,2,1):C.R2GC_350_406})

V_189 = CTVertex(name = 'V_189',
                 type = 'R2',
                 particles = [ P.g, P.g, P.ZR, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1992_288,(0,0,1):C.R2GC_1992_289,(0,1,0):C.R2GC_1992_288,(0,1,1):C.R2GC_1992_289,(0,2,0):C.R2GC_1992_288,(0,2,1):C.R2GC_1992_289})

V_190 = CTVertex(name = 'V_190',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.ZR ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1995_294,(0,0,1):C.R2GC_1995_295,(0,1,0):C.R2GC_1995_294,(0,1,1):C.R2GC_1995_295,(0,2,0):C.R2GC_1995_294,(0,2,1):C.R2GC_1995_295})

V_191 = CTVertex(name = 'V_191',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.ZR ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_2072_296,(0,1,1):C.R2GC_2072_297,(0,2,0):C.R2GC_2072_296,(0,2,1):C.R2GC_2072_297,(0,3,0):C.R2GC_2072_296,(0,3,1):C.R2GC_2072_297,(1,0,0):C.R2GC_1993_290,(1,0,1):C.R2GC_1993_291})

V_192 = CTVertex(name = 'V_192',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1994_292,(0,0,1):C.R2GC_1994_293,(0,1,0):C.R2GC_1994_292,(0,1,1):C.R2GC_1994_293,(0,2,0):C.R2GC_1994_292,(0,2,1):C.R2GC_1994_293})

V_193 = CTVertex(name = 'V_193',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipL__tilde__, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_893_543,(0,0,1):C.R2GC_893_544,(0,0,2):C.R2GC_893_545,(0,0,3):C.R2GC_893_546,(0,0,4):C.R2GC_893_547})

V_194 = CTVertex(name = 'V_194',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1429_81,(0,0,1):C.R2GC_1430_86,(0,0,2):C.R2GC_1429_83,(0,0,3):C.R2GC_1429_84,(0,0,4):C.R2GC_1429_85})

V_195 = CTVertex(name = 'V_195',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipL__tilde__, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_240_304})

V_196 = CTVertex(name = 'V_196',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1429_81,(0,0,1):C.R2GC_1429_82,(0,0,2):C.R2GC_1429_83,(0,0,3):C.R2GC_1429_84,(0,0,4):C.R2GC_1429_85})

V_197 = CTVertex(name = 'V_197',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipL, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_239_303})

V_198 = CTVertex(name = 'V_198',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipR__tilde__, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_242_307,(0,0,1):C.R2GC_242_308,(0,0,2):C.R2GC_242_309,(0,0,3):C.R2GC_242_310,(0,0,4):C.R2GC_242_311})

V_199 = CTVertex(name = 'V_199',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1459_115,(0,0,3):C.R2GC_1459_116,(0,0,6):C.R2GC_1459_117,(0,0,8):C.R2GC_1459_118,(0,0,9):C.R2GC_1459_119,(0,0,11):C.R2GC_1459_120,(0,0,1):C.R2GC_1459_121,(0,0,2):C.R2GC_1459_122,(0,0,4):C.R2GC_1459_123,(0,0,5):C.R2GC_1459_124,(0,0,7):C.R2GC_1459_125,(0,0,10):C.R2GC_1459_126})

V_200 = CTVertex(name = 'V_200',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1460_127,(0,0,3):C.R2GC_1460_128,(0,0,6):C.R2GC_1460_129,(0,0,8):C.R2GC_1460_130,(0,0,9):C.R2GC_1460_131,(0,0,11):C.R2GC_1460_132,(0,0,1):C.R2GC_1460_133,(0,0,2):C.R2GC_1460_134,(0,0,4):C.R2GC_1460_135,(0,0,5):C.R2GC_1460_136,(0,0,7):C.R2GC_1460_137,(0,0,10):C.R2GC_1460_138})

V_201 = CTVertex(name = 'V_201',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1461_139,(0,0,3):C.R2GC_1461_140,(0,0,6):C.R2GC_1461_141,(0,0,8):C.R2GC_1461_142,(0,0,9):C.R2GC_1461_143,(0,0,11):C.R2GC_1461_144,(0,0,1):C.R2GC_1461_145,(0,0,2):C.R2GC_1461_146,(0,0,4):C.R2GC_1461_147,(0,0,5):C.R2GC_1461_148,(0,0,7):C.R2GC_1461_149,(0,0,10):C.R2GC_1461_150})

V_202 = CTVertex(name = 'V_202',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1462_151,(0,0,3):C.R2GC_1462_152,(0,0,6):C.R2GC_1462_153,(0,0,8):C.R2GC_1462_154,(0,0,9):C.R2GC_1462_155,(0,0,11):C.R2GC_1462_156,(0,0,1):C.R2GC_1462_157,(0,0,2):C.R2GC_1462_158,(0,0,4):C.R2GC_1462_159,(0,0,5):C.R2GC_1462_160,(0,0,7):C.R2GC_1462_161,(0,0,10):C.R2GC_1462_162})

V_203 = CTVertex(name = 'V_203',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1463_163,(0,0,3):C.R2GC_1463_164,(0,0,6):C.R2GC_1463_165,(0,0,8):C.R2GC_1463_166,(0,0,9):C.R2GC_1463_167,(0,0,11):C.R2GC_1463_168,(0,0,1):C.R2GC_1463_169,(0,0,2):C.R2GC_1463_170,(0,0,4):C.R2GC_1463_171,(0,0,5):C.R2GC_1463_172,(0,0,7):C.R2GC_1463_173,(0,0,10):C.R2GC_1463_174})

V_204 = CTVertex(name = 'V_204',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1464_175,(0,0,3):C.R2GC_1464_176,(0,0,6):C.R2GC_1464_177,(0,0,8):C.R2GC_1464_178,(0,0,9):C.R2GC_1464_179,(0,0,11):C.R2GC_1464_180,(0,0,1):C.R2GC_1464_181,(0,0,2):C.R2GC_1464_182,(0,0,4):C.R2GC_1464_183,(0,0,5):C.R2GC_1464_184,(0,0,7):C.R2GC_1464_185,(0,0,10):C.R2GC_1464_186})

V_205 = CTVertex(name = 'V_205',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1465_187,(0,0,3):C.R2GC_1465_188,(0,0,6):C.R2GC_1465_189,(0,0,8):C.R2GC_1465_190,(0,0,9):C.R2GC_1465_191,(0,0,11):C.R2GC_1465_192,(0,0,1):C.R2GC_1465_193,(0,0,2):C.R2GC_1465_194,(0,0,4):C.R2GC_1465_195,(0,0,5):C.R2GC_1465_196,(0,0,7):C.R2GC_1465_197,(0,0,10):C.R2GC_1465_198})

V_206 = CTVertex(name = 'V_206',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1466_199,(0,0,3):C.R2GC_1466_200,(0,0,6):C.R2GC_1466_201,(0,0,8):C.R2GC_1466_202,(0,0,9):C.R2GC_1466_203,(0,0,11):C.R2GC_1466_204,(0,0,1):C.R2GC_1466_205,(0,0,2):C.R2GC_1466_206,(0,0,4):C.R2GC_1466_207,(0,0,5):C.R2GC_1466_208,(0,0,7):C.R2GC_1466_209,(0,0,10):C.R2GC_1466_210})

V_207 = CTVertex(name = 'V_207',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1467_211,(0,0,3):C.R2GC_1467_212,(0,0,6):C.R2GC_1467_213,(0,0,8):C.R2GC_1467_214,(0,0,9):C.R2GC_1467_215,(0,0,11):C.R2GC_1467_216,(0,0,1):C.R2GC_1467_217,(0,0,2):C.R2GC_1467_218,(0,0,4):C.R2GC_1467_219,(0,0,5):C.R2GC_1467_220,(0,0,7):C.R2GC_1467_221,(0,0,10):C.R2GC_1467_222})

V_208 = CTVertex(name = 'V_208',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.c] ], [ [P.c, P.t] ], [ [P.c, P.u] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.t, P.u] ], [ [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1468_223,(0,0,3):C.R2GC_1468_224,(0,0,6):C.R2GC_1468_225,(0,0,8):C.R2GC_1468_226,(0,0,9):C.R2GC_1468_227,(0,0,11):C.R2GC_1468_228,(0,0,1):C.R2GC_1468_229,(0,0,2):C.R2GC_1468_230,(0,0,4):C.R2GC_1468_231,(0,0,5):C.R2GC_1468_232,(0,0,7):C.R2GC_1468_233,(0,0,10):C.R2GC_1468_234})

V_209 = CTVertex(name = 'V_209',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1486_264,(0,0,1):C.R2GC_1486_265,(0,0,2):C.R2GC_1486_266,(0,0,3):C.R2GC_1486_267,(0,0,4):C.R2GC_1486_268})

V_210 = CTVertex(name = 'V_210',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1485_259,(0,0,1):C.R2GC_1485_260,(0,0,2):C.R2GC_1485_261,(0,0,3):C.R2GC_1485_262,(0,0,4):C.R2GC_1485_263})

V_211 = CTVertex(name = 'V_211',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1487_269,(0,0,1):C.R2GC_1487_270,(0,0,2):C.R2GC_1487_271,(0,0,3):C.R2GC_1487_272,(0,0,4):C.R2GC_1487_273})

V_212 = CTVertex(name = 'V_212',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1469_235,(0,0,1):C.R2GC_1469_236})

V_213 = CTVertex(name = 'V_213',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1470_237,(0,0,1):C.R2GC_1470_238})

V_214 = CTVertex(name = 'V_214',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1471_239,(0,0,1):C.R2GC_1471_240})

V_215 = CTVertex(name = 'V_215',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1472_241,(0,0,1):C.R2GC_1472_242})

V_216 = CTVertex(name = 'V_216',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1473_243,(0,0,1):C.R2GC_1473_244})

V_217 = CTVertex(name = 'V_217',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1474_245,(0,0,1):C.R2GC_1474_246})

V_218 = CTVertex(name = 'V_218',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1475_247,(0,0,1):C.R2GC_1475_248})

V_219 = CTVertex(name = 'V_219',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1476_249,(0,0,1):C.R2GC_1476_250})

V_220 = CTVertex(name = 'V_220',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0L, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_349_403,(0,0,1):C.R2GC_349_404})

V_221 = CTVertex(name = 'V_221',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0R, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_347_399,(0,0,1):C.R2GC_347_400})

V_222 = CTVertex(name = 'V_222',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0L, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1665_278,(0,0,1):C.R2GC_1665_279})

V_223 = CTVertex(name = 'V_223',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV1, L.VVV3, L.VVV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_305_296,(0,0,1):C.UVGC_305_297,(0,0,4):C.UVGC_305_298,(0,1,2):C.UVGC_243_197,(0,2,3):C.UVGC_244_198})

V_224 = CTVertex(name = 'V_224',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV11, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(2,1,3):C.UVGC_250_203,(2,1,4):C.UVGC_250_202,(0,1,3):C.UVGC_250_203,(0,1,4):C.UVGC_250_202,(4,1,3):C.UVGC_249_200,(4,1,4):C.UVGC_249_201,(3,1,3):C.UVGC_249_200,(3,1,4):C.UVGC_249_201,(8,1,3):C.UVGC_250_202,(8,1,4):C.UVGC_250_203,(6,1,0):C.UVGC_309_309,(6,1,2):C.UVGC_309_310,(6,1,3):C.UVGC_310_314,(6,1,4):C.UVGC_310_315,(6,1,5):C.UVGC_309_313,(7,1,0):C.UVGC_309_309,(7,1,2):C.UVGC_309_310,(7,1,3):C.UVGC_309_311,(7,1,4):C.UVGC_309_312,(7,1,5):C.UVGC_309_313,(5,1,3):C.UVGC_249_200,(5,1,4):C.UVGC_249_201,(1,1,3):C.UVGC_249_200,(1,1,4):C.UVGC_249_201,(11,0,3):C.UVGC_253_206,(11,0,4):C.UVGC_253_207,(10,0,3):C.UVGC_253_206,(10,0,4):C.UVGC_253_207,(9,0,3):C.UVGC_252_204,(9,0,4):C.UVGC_252_205,(0,2,3):C.UVGC_250_203,(0,2,4):C.UVGC_250_202,(2,2,3):C.UVGC_250_203,(2,2,4):C.UVGC_250_202,(5,2,3):C.UVGC_249_200,(5,2,4):C.UVGC_249_201,(1,2,3):C.UVGC_249_200,(1,2,4):C.UVGC_249_201,(7,2,1):C.UVGC_254_208,(7,2,3):C.UVGC_250_202,(7,2,4):C.UVGC_255_209,(4,2,3):C.UVGC_249_200,(4,2,4):C.UVGC_249_201,(3,2,3):C.UVGC_249_200,(3,2,4):C.UVGC_249_201,(8,2,0):C.UVGC_311_316,(8,2,2):C.UVGC_311_317,(8,2,3):C.UVGC_309_311,(8,2,4):C.UVGC_311_318,(8,2,5):C.UVGC_311_319,(6,2,0):C.UVGC_306_299,(6,2,3):C.UVGC_306_300,(6,2,4):C.UVGC_306_301,(6,2,5):C.UVGC_306_302,(0,3,3):C.UVGC_250_203,(0,3,4):C.UVGC_250_202,(2,3,3):C.UVGC_250_203,(2,3,4):C.UVGC_250_202,(5,3,3):C.UVGC_249_200,(5,3,4):C.UVGC_249_201,(1,3,3):C.UVGC_249_200,(1,3,4):C.UVGC_249_201,(7,3,0):C.UVGC_306_299,(7,3,3):C.UVGC_307_303,(7,3,4):C.UVGC_307_304,(7,3,5):C.UVGC_306_302,(4,3,3):C.UVGC_249_200,(4,3,4):C.UVGC_249_201,(3,3,3):C.UVGC_249_200,(3,3,4):C.UVGC_249_201,(8,3,0):C.UVGC_308_305,(8,3,2):C.UVGC_308_306,(8,3,3):C.UVGC_307_303,(8,3,4):C.UVGC_308_307,(8,3,5):C.UVGC_308_308,(6,3,1):C.UVGC_254_208,(6,3,4):C.UVGC_252_204})

V_225 = CTVertex(name = 'V_225',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_1739_191})

V_226 = CTVertex(name = 'V_226',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_750_691,(0,0,1):C.UVGC_750_692,(0,1,0):C.UVGC_702_523,(0,1,1):C.UVGC_702_524})

V_227 = CTVertex(name = 'V_227',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_751_693,(0,0,2):C.UVGC_751_694,(0,0,1):C.UVGC_751_695,(0,1,0):C.UVGC_703_525,(0,1,2):C.UVGC_703_526,(0,1,1):C.UVGC_703_527})

V_228 = CTVertex(name = 'V_228',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_752_696,(0,0,2):C.UVGC_752_697,(0,0,3):C.UVGC_752_698,(0,0,0):C.UVGC_752_699,(0,1,1):C.UVGC_704_528,(0,1,2):C.UVGC_704_529,(0,1,3):C.UVGC_704_530,(0,1,0):C.UVGC_704_531})

V_229 = CTVertex(name = 'V_229',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_762_727,(0,0,2):C.UVGC_762_728,(0,0,1):C.UVGC_762_729,(0,1,0):C.UVGC_714_559,(0,1,2):C.UVGC_714_560,(0,1,1):C.UVGC_714_561})

V_230 = CTVertex(name = 'V_230',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_763_730,(0,0,1):C.UVGC_763_731,(0,1,0):C.UVGC_715_562,(0,1,1):C.UVGC_715_563})

V_231 = CTVertex(name = 'V_231',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_764_732,(0,0,2):C.UVGC_764_733,(0,0,3):C.UVGC_764_734,(0,0,1):C.UVGC_764_735,(0,1,0):C.UVGC_716_564,(0,1,2):C.UVGC_716_565,(0,1,3):C.UVGC_716_566,(0,1,1):C.UVGC_716_567})

V_232 = CTVertex(name = 'V_232',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_782_795,(0,0,2):C.UVGC_782_796,(0,0,3):C.UVGC_782_797,(0,0,0):C.UVGC_782_798,(0,1,1):C.UVGC_734_627,(0,1,2):C.UVGC_734_628,(0,1,3):C.UVGC_734_629,(0,1,0):C.UVGC_734_630})

V_233 = CTVertex(name = 'V_233',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_783_799,(0,0,2):C.UVGC_783_800,(0,0,3):C.UVGC_783_801,(0,0,1):C.UVGC_783_802,(0,1,0):C.UVGC_735_631,(0,1,2):C.UVGC_735_632,(0,1,3):C.UVGC_735_633,(0,1,1):C.UVGC_735_634})

V_234 = CTVertex(name = 'V_234',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1434_101,(0,0,1):C.UVGC_1434_102,(0,1,0):C.UVGC_1433_99,(0,1,1):C.UVGC_1433_100})

V_235 = CTVertex(name = 'V_235',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_753_700,(0,0,1):C.UVGC_753_701,(0,1,0):C.UVGC_705_532,(0,1,1):C.UVGC_705_533})

V_236 = CTVertex(name = 'V_236',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_754_702,(0,0,2):C.UVGC_754_703,(0,0,1):C.UVGC_754_704,(0,1,0):C.UVGC_706_534,(0,1,2):C.UVGC_706_535,(0,1,1):C.UVGC_706_536})

V_237 = CTVertex(name = 'V_237',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_755_705,(0,0,2):C.UVGC_755_706,(0,0,3):C.UVGC_755_707,(0,0,0):C.UVGC_755_708,(0,1,1):C.UVGC_707_537,(0,1,2):C.UVGC_707_538,(0,1,3):C.UVGC_707_539,(0,1,0):C.UVGC_707_540})

V_238 = CTVertex(name = 'V_238',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_765_736,(0,0,2):C.UVGC_765_737,(0,0,1):C.UVGC_765_738,(0,1,0):C.UVGC_717_568,(0,1,2):C.UVGC_717_569,(0,1,1):C.UVGC_717_570})

V_239 = CTVertex(name = 'V_239',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_766_739,(0,0,1):C.UVGC_766_740,(0,1,0):C.UVGC_718_571,(0,1,1):C.UVGC_718_572})

V_240 = CTVertex(name = 'V_240',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_767_741,(0,0,2):C.UVGC_767_742,(0,0,3):C.UVGC_767_743,(0,0,1):C.UVGC_767_744,(0,1,0):C.UVGC_719_573,(0,1,2):C.UVGC_719_574,(0,1,3):C.UVGC_719_575,(0,1,1):C.UVGC_719_576})

V_241 = CTVertex(name = 'V_241',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_784_803,(0,0,2):C.UVGC_784_804,(0,0,3):C.UVGC_784_805,(0,0,0):C.UVGC_784_806,(0,1,1):C.UVGC_736_635,(0,1,2):C.UVGC_736_636,(0,1,3):C.UVGC_736_637,(0,1,0):C.UVGC_736_638})

V_242 = CTVertex(name = 'V_242',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_785_807,(0,0,2):C.UVGC_785_808,(0,0,3):C.UVGC_785_809,(0,0,1):C.UVGC_785_810,(0,1,0):C.UVGC_737_639,(0,1,2):C.UVGC_737_640,(0,1,3):C.UVGC_737_641,(0,1,1):C.UVGC_737_642})

V_243 = CTVertex(name = 'V_243',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1436_105,(0,0,1):C.UVGC_1436_106,(0,1,0):C.UVGC_1435_103,(0,1,1):C.UVGC_1435_104})

V_244 = CTVertex(name = 'V_244',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_756_709,(0,0,1):C.UVGC_756_710,(0,1,0):C.UVGC_708_541,(0,1,1):C.UVGC_708_542})

V_245 = CTVertex(name = 'V_245',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_757_711,(0,0,2):C.UVGC_757_712,(0,0,1):C.UVGC_757_713,(0,1,0):C.UVGC_709_543,(0,1,2):C.UVGC_709_544,(0,1,1):C.UVGC_709_545})

V_246 = CTVertex(name = 'V_246',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_758_714,(0,0,2):C.UVGC_758_715,(0,0,3):C.UVGC_758_716,(0,0,0):C.UVGC_758_717,(0,1,1):C.UVGC_710_546,(0,1,2):C.UVGC_710_547,(0,1,3):C.UVGC_710_548,(0,1,0):C.UVGC_710_549})

V_247 = CTVertex(name = 'V_247',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_768_745,(0,0,2):C.UVGC_768_746,(0,0,1):C.UVGC_768_747,(0,1,0):C.UVGC_720_577,(0,1,2):C.UVGC_720_578,(0,1,1):C.UVGC_720_579})

V_248 = CTVertex(name = 'V_248',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_769_748,(0,0,1):C.UVGC_769_749,(0,1,0):C.UVGC_721_580,(0,1,1):C.UVGC_721_581})

V_249 = CTVertex(name = 'V_249',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_770_750,(0,0,2):C.UVGC_770_751,(0,0,3):C.UVGC_770_752,(0,0,1):C.UVGC_770_753,(0,1,0):C.UVGC_722_582,(0,1,2):C.UVGC_722_583,(0,1,3):C.UVGC_722_584,(0,1,1):C.UVGC_722_585})

V_250 = CTVertex(name = 'V_250',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_786_811,(0,0,2):C.UVGC_786_812,(0,0,3):C.UVGC_786_813,(0,0,0):C.UVGC_786_814,(0,1,1):C.UVGC_738_643,(0,1,2):C.UVGC_738_644,(0,1,3):C.UVGC_738_645,(0,1,0):C.UVGC_738_646})

V_251 = CTVertex(name = 'V_251',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_787_815,(0,0,2):C.UVGC_787_816,(0,0,3):C.UVGC_787_817,(0,0,1):C.UVGC_787_818,(0,1,0):C.UVGC_739_647,(0,1,2):C.UVGC_739_648,(0,1,3):C.UVGC_739_649,(0,1,1):C.UVGC_739_650})

V_252 = CTVertex(name = 'V_252',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_291_258,(0,0,0):C.UVGC_1438_109,(0,0,1):C.UVGC_1438_110,(0,1,0):C.UVGC_1437_107,(0,1,1):C.UVGC_1437_108})

V_253 = CTVertex(name = 'V_253',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_759_718,(0,0,1):C.UVGC_759_719,(0,1,0):C.UVGC_711_550,(0,1,1):C.UVGC_711_551})

V_254 = CTVertex(name = 'V_254',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_760_720,(0,0,2):C.UVGC_760_721,(0,0,1):C.UVGC_760_722,(0,1,0):C.UVGC_712_552,(0,1,2):C.UVGC_712_553,(0,1,1):C.UVGC_712_554})

V_255 = CTVertex(name = 'V_255',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_761_723,(0,0,2):C.UVGC_761_724,(0,0,3):C.UVGC_761_725,(0,0,0):C.UVGC_761_726,(0,1,1):C.UVGC_713_555,(0,1,2):C.UVGC_713_556,(0,1,3):C.UVGC_713_557,(0,1,0):C.UVGC_713_558})

V_256 = CTVertex(name = 'V_256',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_771_754,(0,0,2):C.UVGC_771_755,(0,0,1):C.UVGC_771_756,(0,1,0):C.UVGC_723_586,(0,1,2):C.UVGC_723_587,(0,1,1):C.UVGC_723_588})

V_257 = CTVertex(name = 'V_257',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_772_757,(0,0,1):C.UVGC_772_758,(0,1,0):C.UVGC_724_589,(0,1,1):C.UVGC_724_590})

V_258 = CTVertex(name = 'V_258',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_773_759,(0,0,2):C.UVGC_773_760,(0,0,3):C.UVGC_773_761,(0,0,1):C.UVGC_773_762,(0,1,0):C.UVGC_725_591,(0,1,2):C.UVGC_725_592,(0,1,3):C.UVGC_725_593,(0,1,1):C.UVGC_725_594})

V_259 = CTVertex(name = 'V_259',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g] ], [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_788_819,(0,0,2):C.UVGC_788_820,(0,0,3):C.UVGC_788_821,(0,0,0):C.UVGC_788_822,(0,1,1):C.UVGC_740_651,(0,1,2):C.UVGC_740_652,(0,1,3):C.UVGC_740_653,(0,1,0):C.UVGC_740_654})

V_260 = CTVertex(name = 'V_260',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.s] ], [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_789_823,(0,0,2):C.UVGC_789_824,(0,0,3):C.UVGC_789_825,(0,0,1):C.UVGC_789_826,(0,1,0):C.UVGC_741_655,(0,1,2):C.UVGC_741_656,(0,1,3):C.UVGC_741_657,(0,1,1):C.UVGC_741_658})

V_261 = CTVertex(name = 'V_261',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_292_259,(0,0,0):C.UVGC_1440_113,(0,0,1):C.UVGC_1440_114,(0,1,0):C.UVGC_1439_111,(0,1,1):C.UVGC_1439_112})

V_262 = CTVertex(name = 'V_262',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_1531_185})

V_263 = CTVertex(name = 'V_263',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_844_827,(0,1,2):C.UVGC_844_828,(0,1,1):C.UVGC_844_829,(0,0,0):C.UVGC_1477_161,(0,0,2):C.UVGC_1477_162,(0,0,1):C.UVGC_1477_163})

V_264 = CTVertex(name = 'V_264',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_845_830,(0,1,2):C.UVGC_845_831,(0,1,0):C.UVGC_845_832,(0,0,1):C.UVGC_1478_164,(0,0,2):C.UVGC_1478_165,(0,0,0):C.UVGC_1478_166})

V_265 = CTVertex(name = 'V_265',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_848_839,(0,0,2):C.UVGC_848_840,(0,0,1):C.UVGC_848_841,(0,1,0):C.UVGC_1481_173,(0,1,2):C.UVGC_1481_174,(0,1,1):C.UVGC_1481_175})

V_266 = CTVertex(name = 'V_266',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_849_842,(0,0,2):C.UVGC_849_843,(0,0,1):C.UVGC_849_844,(0,1,0):C.UVGC_1482_176,(0,1,2):C.UVGC_1482_177,(0,1,1):C.UVGC_1482_178})

V_267 = CTVertex(name = 'V_267',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_894_851,(0,0,2):C.UVGC_894_852,(0,0,1):C.UVGC_894_853,(0,1,0):C.UVGC_895_854,(0,1,2):C.UVGC_895_855,(0,1,1):C.UVGC_895_856})

V_268 = CTVertex(name = 'V_268',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_339_387,(0,0,2):C.UVGC_339_388,(0,0,1):C.UVGC_339_389,(0,1,0):C.UVGC_1431_93,(0,1,2):C.UVGC_1431_94,(0,1,1):C.UVGC_1431_95})

V_269 = CTVertex(name = 'V_269',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_340_390,(0,0,2):C.UVGC_340_391,(0,0,0):C.UVGC_340_392,(0,1,1):C.UVGC_1432_96,(0,1,2):C.UVGC_1432_97,(0,1,0):C.UVGC_1432_98})

V_270 = CTVertex(name = 'V_270',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_343_399,(0,1,2):C.UVGC_343_400,(0,1,1):C.UVGC_343_401,(0,0,0):C.UVGC_1443_121,(0,0,2):C.UVGC_1443_122,(0,0,1):C.UVGC_1443_123})

V_271 = CTVertex(name = 'V_271',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_344_402,(0,1,2):C.UVGC_344_403,(0,1,1):C.UVGC_344_404,(0,0,0):C.UVGC_1444_124,(0,0,2):C.UVGC_1444_125,(0,0,1):C.UVGC_1444_126})

V_272 = CTVertex(name = 'V_272',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1456_152,(0,0,2):C.UVGC_1456_153,(0,0,1):C.UVGC_1456_154,(0,1,0):C.UVGC_1455_149,(0,1,2):C.UVGC_1455_150,(0,1,1):C.UVGC_1455_151})

V_273 = CTVertex(name = 'V_273',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_289_252,(0,0,2):C.UVGC_289_253,(0,0,1):C.UVGC_289_254})

V_274 = CTVertex(name = 'V_274',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_290_255,(0,0,2):C.UVGC_290_256,(0,0,0):C.UVGC_290_257})

V_275 = CTVertex(name = 'V_275',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_316_326,(0,0,2):C.UVGC_316_327,(0,0,1):C.UVGC_316_328})

V_276 = CTVertex(name = 'V_276',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_317_329,(0,0,2):C.UVGC_317_330,(0,0,1):C.UVGC_317_331})

V_277 = CTVertex(name = 'V_277',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_315_323,(0,0,2):C.UVGC_315_324,(0,0,1):C.UVGC_315_325,(0,1,0):C.UVGC_318_332,(0,1,2):C.UVGC_318_333,(0,1,1):C.UVGC_318_334})

V_278 = CTVertex(name = 'V_278',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_345_405,(0,0,2):C.UVGC_345_406,(0,0,1):C.UVGC_345_407,(0,1,0):C.UVGC_1453_143,(0,1,2):C.UVGC_1453_144,(0,1,1):C.UVGC_1453_145})

V_279 = CTVertex(name = 'V_279',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_346_408,(0,0,2):C.UVGC_346_409,(0,0,1):C.UVGC_346_410,(0,1,0):C.UVGC_1454_146,(0,1,2):C.UVGC_1454_147,(0,1,1):C.UVGC_1454_148})

V_280 = CTVertex(name = 'V_280',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_341_393,(0,1,2):C.UVGC_341_394,(0,1,1):C.UVGC_341_395,(0,0,0):C.UVGC_1441_115,(0,0,2):C.UVGC_1441_116,(0,0,1):C.UVGC_1441_117})

V_281 = CTVertex(name = 'V_281',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_342_396,(0,1,2):C.UVGC_342_397,(0,1,0):C.UVGC_342_398,(0,0,1):C.UVGC_1442_118,(0,0,2):C.UVGC_1442_119,(0,0,0):C.UVGC_1442_120})

V_282 = CTVertex(name = 'V_282',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1457_155,(0,0,2):C.UVGC_1457_156,(0,0,1):C.UVGC_1457_157,(0,1,0):C.UVGC_1458_158,(0,1,2):C.UVGC_1458_159,(0,1,1):C.UVGC_1458_160})

V_283 = CTVertex(name = 'V_283',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_662_411,(0,0,1):C.UVGC_662_412,(0,1,0):C.UVGC_678_451,(0,1,1):C.UVGC_678_452})

V_284 = CTVertex(name = 'V_284',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.u, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_670_431,(0,0,1):C.UVGC_670_432,(0,0,2):C.UVGC_670_433,(0,1,0):C.UVGC_686_471,(0,1,1):C.UVGC_686_472,(0,1,2):C.UVGC_686_473})

V_285 = CTVertex(name = 'V_285',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.u, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_726_595,(0,0,1):C.UVGC_726_596,(0,0,3):C.UVGC_726_597,(0,0,2):C.UVGC_726_598,(0,1,0):C.UVGC_774_763,(0,1,1):C.UVGC_774_764,(0,1,3):C.UVGC_774_765,(0,1,2):C.UVGC_774_766})

V_286 = CTVertex(name = 'V_286',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.c, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_663_413,(0,0,1):C.UVGC_663_414,(0,0,2):C.UVGC_663_415,(0,1,0):C.UVGC_679_453,(0,1,1):C.UVGC_679_454,(0,1,2):C.UVGC_679_455})

V_287 = CTVertex(name = 'V_287',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_671_434,(0,0,1):C.UVGC_671_435,(0,1,0):C.UVGC_687_474,(0,1,1):C.UVGC_687_475})

V_288 = CTVertex(name = 'V_288',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.c, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_727_599,(0,0,1):C.UVGC_727_600,(0,0,3):C.UVGC_727_601,(0,0,2):C.UVGC_727_602,(0,1,0):C.UVGC_775_767,(0,1,1):C.UVGC_775_768,(0,1,3):C.UVGC_775_769,(0,1,2):C.UVGC_775_770})

V_289 = CTVertex(name = 'V_289',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.t, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_694_491,(0,0,1):C.UVGC_694_492,(0,0,3):C.UVGC_694_493,(0,0,2):C.UVGC_694_494,(0,1,0):C.UVGC_742_659,(0,1,1):C.UVGC_742_660,(0,1,3):C.UVGC_742_661,(0,1,2):C.UVGC_742_662})

V_290 = CTVertex(name = 'V_290',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.t, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_698_507,(0,0,1):C.UVGC_698_508,(0,0,3):C.UVGC_698_509,(0,0,2):C.UVGC_698_510,(0,1,0):C.UVGC_746_675,(0,1,1):C.UVGC_746_676,(0,1,3):C.UVGC_746_677,(0,1,2):C.UVGC_746_678})

V_291 = CTVertex(name = 'V_291',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,1):C.UVGC_319_335,(0,0,0):C.UVGC_1445_127,(0,0,1):C.UVGC_1445_128,(0,1,0):C.UVGC_1446_129,(0,1,1):C.UVGC_1446_130})

V_292 = CTVertex(name = 'V_292',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_664_416,(0,0,1):C.UVGC_664_417,(0,1,0):C.UVGC_680_456,(0,1,1):C.UVGC_680_457})

V_293 = CTVertex(name = 'V_293',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.u, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_672_436,(0,0,1):C.UVGC_672_437,(0,0,2):C.UVGC_672_438,(0,1,0):C.UVGC_688_476,(0,1,1):C.UVGC_688_477,(0,1,2):C.UVGC_688_478})

V_294 = CTVertex(name = 'V_294',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.u, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_728_603,(0,0,1):C.UVGC_728_604,(0,0,3):C.UVGC_728_605,(0,0,2):C.UVGC_728_606,(0,1,0):C.UVGC_776_771,(0,1,1):C.UVGC_776_772,(0,1,3):C.UVGC_776_773,(0,1,2):C.UVGC_776_774})

V_295 = CTVertex(name = 'V_295',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.c, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_665_418,(0,0,1):C.UVGC_665_419,(0,0,2):C.UVGC_665_420,(0,1,0):C.UVGC_681_458,(0,1,1):C.UVGC_681_459,(0,1,2):C.UVGC_681_460})

V_296 = CTVertex(name = 'V_296',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_673_439,(0,0,1):C.UVGC_673_440,(0,1,0):C.UVGC_689_479,(0,1,1):C.UVGC_689_480})

V_297 = CTVertex(name = 'V_297',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.c, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_729_607,(0,0,1):C.UVGC_729_608,(0,0,3):C.UVGC_729_609,(0,0,2):C.UVGC_729_610,(0,1,0):C.UVGC_777_775,(0,1,1):C.UVGC_777_776,(0,1,3):C.UVGC_777_777,(0,1,2):C.UVGC_777_778})

V_298 = CTVertex(name = 'V_298',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.t, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_695_495,(0,0,1):C.UVGC_695_496,(0,0,3):C.UVGC_695_497,(0,0,2):C.UVGC_695_498,(0,1,0):C.UVGC_743_663,(0,1,1):C.UVGC_743_664,(0,1,3):C.UVGC_743_665,(0,1,2):C.UVGC_743_666})

V_299 = CTVertex(name = 'V_299',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.t, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_699_511,(0,0,1):C.UVGC_699_512,(0,0,3):C.UVGC_699_513,(0,0,2):C.UVGC_699_514,(0,1,0):C.UVGC_747_679,(0,1,1):C.UVGC_747_680,(0,1,3):C.UVGC_747_681,(0,1,2):C.UVGC_747_682})

V_300 = CTVertex(name = 'V_300',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,1):C.UVGC_320_336,(0,0,0):C.UVGC_1447_131,(0,0,1):C.UVGC_1447_132,(0,1,0):C.UVGC_1448_133,(0,1,1):C.UVGC_1448_134})

V_301 = CTVertex(name = 'V_301',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_666_421,(0,0,1):C.UVGC_666_422,(0,1,0):C.UVGC_682_461,(0,1,1):C.UVGC_682_462})

V_302 = CTVertex(name = 'V_302',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.u, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_674_441,(0,0,1):C.UVGC_674_442,(0,0,2):C.UVGC_674_443,(0,1,0):C.UVGC_690_481,(0,1,1):C.UVGC_690_482,(0,1,2):C.UVGC_690_483})

V_303 = CTVertex(name = 'V_303',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.u, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_730_611,(0,0,1):C.UVGC_730_612,(0,0,3):C.UVGC_730_613,(0,0,2):C.UVGC_730_614,(0,1,0):C.UVGC_778_779,(0,1,1):C.UVGC_778_780,(0,1,3):C.UVGC_778_781,(0,1,2):C.UVGC_778_782})

V_304 = CTVertex(name = 'V_304',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.c, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_667_423,(0,0,1):C.UVGC_667_424,(0,0,2):C.UVGC_667_425,(0,1,0):C.UVGC_683_463,(0,1,1):C.UVGC_683_464,(0,1,2):C.UVGC_683_465})

V_305 = CTVertex(name = 'V_305',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_675_444,(0,0,1):C.UVGC_675_445,(0,1,0):C.UVGC_691_484,(0,1,1):C.UVGC_691_485})

V_306 = CTVertex(name = 'V_306',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.c, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_731_615,(0,0,1):C.UVGC_731_616,(0,0,3):C.UVGC_731_617,(0,0,2):C.UVGC_731_618,(0,1,0):C.UVGC_779_783,(0,1,1):C.UVGC_779_784,(0,1,3):C.UVGC_779_785,(0,1,2):C.UVGC_779_786})

V_307 = CTVertex(name = 'V_307',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.t, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_696_499,(0,0,1):C.UVGC_696_500,(0,0,3):C.UVGC_696_501,(0,0,2):C.UVGC_696_502,(0,1,0):C.UVGC_744_667,(0,1,1):C.UVGC_744_668,(0,1,3):C.UVGC_744_669,(0,1,2):C.UVGC_744_670})

V_308 = CTVertex(name = 'V_308',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.t, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_700_515,(0,0,1):C.UVGC_700_516,(0,0,3):C.UVGC_700_517,(0,0,2):C.UVGC_700_518,(0,1,0):C.UVGC_748_683,(0,1,1):C.UVGC_748_684,(0,1,3):C.UVGC_748_685,(0,1,2):C.UVGC_748_686})

V_309 = CTVertex(name = 'V_309',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,1):C.UVGC_321_337,(0,0,0):C.UVGC_1449_135,(0,0,1):C.UVGC_1449_136,(0,1,0):C.UVGC_1450_137,(0,1,1):C.UVGC_1450_138})

V_310 = CTVertex(name = 'V_310',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_668_426,(0,0,1):C.UVGC_668_427,(0,1,0):C.UVGC_684_466,(0,1,1):C.UVGC_684_467})

V_311 = CTVertex(name = 'V_311',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.u, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_676_446,(0,0,1):C.UVGC_676_447,(0,0,2):C.UVGC_676_448,(0,1,0):C.UVGC_692_486,(0,1,1):C.UVGC_692_487,(0,1,2):C.UVGC_692_488})

V_312 = CTVertex(name = 'V_312',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.u, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_732_619,(0,0,1):C.UVGC_732_620,(0,0,3):C.UVGC_732_621,(0,0,2):C.UVGC_732_622,(0,1,0):C.UVGC_780_787,(0,1,1):C.UVGC_780_788,(0,1,3):C.UVGC_780_789,(0,1,2):C.UVGC_780_790})

V_313 = CTVertex(name = 'V_313',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.c, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g], [P.g, P.u] ], [ [P.c, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_669_428,(0,0,1):C.UVGC_669_429,(0,0,2):C.UVGC_669_430,(0,1,0):C.UVGC_685_468,(0,1,1):C.UVGC_685_469,(0,1,2):C.UVGC_685_470})

V_314 = CTVertex(name = 'V_314',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_677_449,(0,0,1):C.UVGC_677_450,(0,1,0):C.UVGC_693_489,(0,1,1):C.UVGC_693_490})

V_315 = CTVertex(name = 'V_315',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.c, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_733_623,(0,0,1):C.UVGC_733_624,(0,0,3):C.UVGC_733_625,(0,0,2):C.UVGC_733_626,(0,1,0):C.UVGC_781_791,(0,1,1):C.UVGC_781_792,(0,1,3):C.UVGC_781_793,(0,1,2):C.UVGC_781_794})

V_316 = CTVertex(name = 'V_316',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.t, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ], [ [P.g, P.t, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_697_503,(0,0,1):C.UVGC_697_504,(0,0,3):C.UVGC_697_505,(0,0,2):C.UVGC_697_506,(0,1,0):C.UVGC_745_671,(0,1,1):C.UVGC_745_672,(0,1,3):C.UVGC_745_673,(0,1,2):C.UVGC_745_674})

V_317 = CTVertex(name = 'V_317',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.t, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.c, P.g] ], [ [P.c, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_701_519,(0,0,1):C.UVGC_701_520,(0,0,3):C.UVGC_701_521,(0,0,2):C.UVGC_701_522,(0,1,0):C.UVGC_749_687,(0,1,1):C.UVGC_749_688,(0,1,3):C.UVGC_749_689,(0,1,2):C.UVGC_749_690})

V_318 = CTVertex(name = 'V_318',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,2,1):C.UVGC_322_338,(0,0,0):C.UVGC_1451_139,(0,0,1):C.UVGC_1451_140,(0,1,0):C.UVGC_1452_141,(0,1,1):C.UVGC_1452_142})

V_319 = CTVertex(name = 'V_319',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_323_339,(0,0,2):C.UVGC_323_340,(0,0,1):C.UVGC_323_341})

V_320 = CTVertex(name = 'V_320',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_324_342,(0,0,2):C.UVGC_324_343,(0,0,1):C.UVGC_324_344})

V_321 = CTVertex(name = 'V_321',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_293_260,(0,0,2):C.UVGC_293_261,(0,0,1):C.UVGC_293_262})

V_322 = CTVertex(name = 'V_322',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_294_263,(0,0,2):C.UVGC_294_264,(0,0,0):C.UVGC_294_265})

V_323 = CTVertex(name = 'V_323',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_326_348,(0,0,2):C.UVGC_326_349,(0,0,1):C.UVGC_326_350,(0,1,0):C.UVGC_325_345,(0,1,2):C.UVGC_325_346,(0,1,1):C.UVGC_325_347})

V_324 = CTVertex(name = 'V_324',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1740_192})

V_325 = CTVertex(name = 'V_325',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_850_845,(0,1,2):C.UVGC_850_846,(0,1,1):C.UVGC_850_847,(0,0,0):C.UVGC_1483_179,(0,0,2):C.UVGC_1483_180,(0,0,1):C.UVGC_1483_181})

V_326 = CTVertex(name = 'V_326',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_851_848,(0,1,2):C.UVGC_851_849,(0,1,1):C.UVGC_851_850,(0,0,0):C.UVGC_1484_182,(0,0,2):C.UVGC_1484_183,(0,0,1):C.UVGC_1484_184})

V_327 = CTVertex(name = 'V_327',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_846_833,(0,0,2):C.UVGC_846_834,(0,0,1):C.UVGC_846_835,(0,1,0):C.UVGC_1479_167,(0,1,2):C.UVGC_1479_168,(0,1,1):C.UVGC_1479_169})

V_328 = CTVertex(name = 'V_328',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_847_836,(0,0,2):C.UVGC_847_837,(0,0,0):C.UVGC_847_838,(0,1,1):C.UVGC_1480_170,(0,1,2):C.UVGC_1480_171,(0,1,0):C.UVGC_1480_172})

V_329 = CTVertex(name = 'V_329',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_897_860,(0,0,2):C.UVGC_897_861,(0,0,1):C.UVGC_897_862,(0,1,0):C.UVGC_896_857,(0,1,2):C.UVGC_896_858,(0,1,1):C.UVGC_896_859})

V_330 = CTVertex(name = 'V_330',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1532_186})

V_331 = CTVertex(name = 'V_331',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_946_866,(0,1,0):C.UVGC_935_863,(0,2,0):C.UVGC_935_863})

V_332 = CTVertex(name = 'V_332',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_946_866,(0,1,0):C.UVGC_935_863,(0,2,0):C.UVGC_935_863})

V_333 = CTVertex(name = 'V_333',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_946_866,(0,1,0):C.UVGC_952_868,(0,2,0):C.UVGC_952_868})

V_334 = CTVertex(name = 'V_334',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1707_189,(0,1,0):C.UVGC_1708_190})

V_335 = CTVertex(name = 'V_335',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_2083_195,(0,1,0):C.UVGC_2084_196})

V_336 = CTVertex(name = 'V_336',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_278_233,(0,0,1):C.UVGC_278_234,(0,1,0):C.UVGC_1126_19,(0,1,1):C.UVGC_1126_20})

V_337 = CTVertex(name = 'V_337',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_282_241,(0,0,1):C.UVGC_282_242,(0,1,0):C.UVGC_1130_27,(0,1,1):C.UVGC_1130_28})

V_338 = CTVertex(name = 'V_338',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_295_266,(0,0,2):C.UVGC_295_267,(0,0,1):C.UVGC_295_268,(0,1,0):C.UVGC_1135_39,(0,1,2):C.UVGC_1135_40,(0,1,1):C.UVGC_1135_41})

V_339 = CTVertex(name = 'V_339',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_270_217,(0,0,0):C.UVGC_270_218,(0,1,1):C.UVGC_1118_3,(0,1,0):C.UVGC_1118_4})

V_340 = CTVertex(name = 'V_340',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_274_225,(0,0,1):C.UVGC_274_226,(0,1,0):C.UVGC_1122_11,(0,1,1):C.UVGC_1122_12})

V_341 = CTVertex(name = 'V_341',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_296_269,(0,0,2):C.UVGC_296_270,(0,0,0):C.UVGC_296_271,(0,1,1):C.UVGC_1136_42,(0,1,2):C.UVGC_1136_43,(0,1,0):C.UVGC_1136_44})

V_342 = CTVertex(name = 'V_342',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_327_351,(0,0,2):C.UVGC_327_352,(0,0,1):C.UVGC_327_353,(0,1,0):C.UVGC_1144_66,(0,1,2):C.UVGC_1144_67,(0,1,1):C.UVGC_1144_68})

V_343 = CTVertex(name = 'V_343',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_328_354,(0,0,2):C.UVGC_328_355,(0,0,1):C.UVGC_328_356,(0,1,0):C.UVGC_1145_69,(0,1,2):C.UVGC_1145_70,(0,1,1):C.UVGC_1145_71})

V_344 = CTVertex(name = 'V_344',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_329_357,(0,0,2):C.UVGC_329_358,(0,0,1):C.UVGC_329_359,(0,1,0):C.UVGC_1146_72,(0,1,2):C.UVGC_1146_73,(0,1,1):C.UVGC_1146_74})

V_345 = CTVertex(name = 'V_345',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_279_235,(0,1,1):C.UVGC_279_236,(0,0,0):C.UVGC_1125_17,(0,0,1):C.UVGC_1125_18})

V_346 = CTVertex(name = 'V_346',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_283_243,(0,1,1):C.UVGC_283_244,(0,0,0):C.UVGC_1129_25,(0,0,1):C.UVGC_1129_26})

V_347 = CTVertex(name = 'V_347',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_297_272,(0,1,2):C.UVGC_297_273,(0,1,1):C.UVGC_297_274,(0,0,0):C.UVGC_1133_33,(0,0,2):C.UVGC_1133_34,(0,0,1):C.UVGC_1133_35})

V_348 = CTVertex(name = 'V_348',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_271_219,(0,1,0):C.UVGC_271_220,(0,0,1):C.UVGC_1117_1,(0,0,0):C.UVGC_1117_2})

V_349 = CTVertex(name = 'V_349',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.UVGC_275_227,(0,1,1):C.UVGC_275_228,(0,0,0):C.UVGC_1121_9,(0,0,1):C.UVGC_1121_10})

V_350 = CTVertex(name = 'V_350',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_298_275,(0,1,2):C.UVGC_298_276,(0,1,0):C.UVGC_298_277,(0,0,1):C.UVGC_1134_36,(0,0,2):C.UVGC_1134_37,(0,0,0):C.UVGC_1134_38})

V_351 = CTVertex(name = 'V_351',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_330_360,(0,1,2):C.UVGC_330_361,(0,1,1):C.UVGC_330_362,(0,0,0):C.UVGC_1141_57,(0,0,2):C.UVGC_1141_58,(0,0,1):C.UVGC_1141_59})

V_352 = CTVertex(name = 'V_352',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_331_363,(0,1,2):C.UVGC_331_364,(0,1,1):C.UVGC_331_365,(0,0,0):C.UVGC_1142_60,(0,0,2):C.UVGC_1142_61,(0,0,1):C.UVGC_1142_62})

V_353 = CTVertex(name = 'V_353',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_332_366,(0,1,2):C.UVGC_332_367,(0,1,1):C.UVGC_332_368,(0,0,0):C.UVGC_1143_63,(0,0,2):C.UVGC_1143_64,(0,0,1):C.UVGC_1143_65})

V_354 = CTVertex(name = 'V_354',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,4):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,1):C.UVGC_258_213,(0,1,2):C.UVGC_258_214,(0,1,3):C.UVGC_258_215,(0,1,4):C.UVGC_258_216,(0,2,0):C.UVGC_258_212,(0,2,1):C.UVGC_258_213,(0,2,2):C.UVGC_258_214,(0,2,3):C.UVGC_258_215,(0,2,4):C.UVGC_258_216})

V_355 = CTVertex(name = 'V_355',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,2):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,1):C.UVGC_258_213,(0,1,3):C.UVGC_258_214,(0,1,4):C.UVGC_258_215,(0,1,2):C.UVGC_258_216,(0,2,0):C.UVGC_258_212,(0,2,1):C.UVGC_258_213,(0,2,3):C.UVGC_258_214,(0,2,4):C.UVGC_258_215,(0,2,2):C.UVGC_258_216})

V_356 = CTVertex(name = 'V_356',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,1):C.UVGC_258_213,(0,1,2):C.UVGC_258_214,(0,1,3):C.UVGC_258_215,(0,1,4):C.UVGC_313_321,(0,2,0):C.UVGC_258_212,(0,2,1):C.UVGC_258_213,(0,2,2):C.UVGC_258_214,(0,2,3):C.UVGC_258_215,(0,2,4):C.UVGC_313_321})

V_357 = CTVertex(name = 'V_357',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_945_865,(0,1,0):C.UVGC_936_864,(0,2,0):C.UVGC_936_864})

V_358 = CTVertex(name = 'V_358',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_945_865,(0,1,0):C.UVGC_936_864,(0,2,0):C.UVGC_936_864})

V_359 = CTVertex(name = 'V_359',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_945_865,(0,1,0):C.UVGC_951_867,(0,2,0):C.UVGC_951_867})

V_360 = CTVertex(name = 'V_360',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_1705_187,(0,1,0):C.UVGC_1706_188})

V_361 = CTVertex(name = 'V_361',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_2081_193,(0,1,0):C.UVGC_2082_194})

V_362 = CTVertex(name = 'V_362',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_280_237,(0,0,1):C.UVGC_280_238,(0,1,0):C.UVGC_1128_23,(0,1,1):C.UVGC_1128_24})

V_363 = CTVertex(name = 'V_363',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_272_221,(0,0,0):C.UVGC_272_222,(0,1,1):C.UVGC_1120_7,(0,1,0):C.UVGC_1120_8})

V_364 = CTVertex(name = 'V_364',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_333_369,(0,0,2):C.UVGC_333_370,(0,0,1):C.UVGC_333_371,(0,1,0):C.UVGC_1150_84,(0,1,2):C.UVGC_1150_85,(0,1,1):C.UVGC_1150_86})

V_365 = CTVertex(name = 'V_365',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_284_245,(0,0,1):C.UVGC_284_246,(0,1,0):C.UVGC_1132_31,(0,1,1):C.UVGC_1132_32})

V_366 = CTVertex(name = 'V_366',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_276_229,(0,0,1):C.UVGC_276_230,(0,1,0):C.UVGC_1124_15,(0,1,1):C.UVGC_1124_16})

V_367 = CTVertex(name = 'V_367',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_334_372,(0,0,2):C.UVGC_334_373,(0,0,1):C.UVGC_334_374,(0,1,0):C.UVGC_1151_87,(0,1,2):C.UVGC_1151_88,(0,1,1):C.UVGC_1151_89})

V_368 = CTVertex(name = 'V_368',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_299_278,(0,0,2):C.UVGC_299_279,(0,0,1):C.UVGC_299_280,(0,1,0):C.UVGC_1139_51,(0,1,2):C.UVGC_1139_52,(0,1,1):C.UVGC_1139_53})

V_369 = CTVertex(name = 'V_369',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_300_281,(0,0,2):C.UVGC_300_282,(0,0,0):C.UVGC_300_283,(0,1,1):C.UVGC_1140_54,(0,1,2):C.UVGC_1140_55,(0,1,0):C.UVGC_1140_56})

V_370 = CTVertex(name = 'V_370',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_335_375,(0,0,2):C.UVGC_335_376,(0,0,1):C.UVGC_335_377,(0,1,0):C.UVGC_1152_90,(0,1,2):C.UVGC_1152_91,(0,1,1):C.UVGC_1152_92})

V_371 = CTVertex(name = 'V_371',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_281_239,(0,1,1):C.UVGC_281_240,(0,0,0):C.UVGC_1127_21,(0,0,1):C.UVGC_1127_22})

V_372 = CTVertex(name = 'V_372',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_273_223,(0,1,0):C.UVGC_273_224,(0,0,1):C.UVGC_1119_5,(0,0,0):C.UVGC_1119_6})

V_373 = CTVertex(name = 'V_373',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_336_378,(0,1,2):C.UVGC_336_379,(0,1,1):C.UVGC_336_380,(0,0,0):C.UVGC_1147_75,(0,0,2):C.UVGC_1147_76,(0,0,1):C.UVGC_1147_77})

V_374 = CTVertex(name = 'V_374',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_285_247,(0,1,1):C.UVGC_285_248,(0,0,0):C.UVGC_1131_29,(0,0,1):C.UVGC_1131_30})

V_375 = CTVertex(name = 'V_375',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.UVGC_277_231,(0,1,1):C.UVGC_277_232,(0,0,0):C.UVGC_1123_13,(0,0,1):C.UVGC_1123_14})

V_376 = CTVertex(name = 'V_376',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_337_381,(0,1,2):C.UVGC_337_382,(0,1,1):C.UVGC_337_383,(0,0,0):C.UVGC_1148_78,(0,0,2):C.UVGC_1148_79,(0,0,1):C.UVGC_1148_80})

V_377 = CTVertex(name = 'V_377',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_301_284,(0,1,2):C.UVGC_301_285,(0,1,1):C.UVGC_301_286,(0,0,0):C.UVGC_1137_45,(0,0,2):C.UVGC_1137_46,(0,0,1):C.UVGC_1137_47})

V_378 = CTVertex(name = 'V_378',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_302_287,(0,1,2):C.UVGC_302_288,(0,1,0):C.UVGC_302_289,(0,0,1):C.UVGC_1138_48,(0,0,2):C.UVGC_1138_49,(0,0,0):C.UVGC_1138_50})

V_379 = CTVertex(name = 'V_379',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_338_384,(0,1,2):C.UVGC_338_385,(0,1,1):C.UVGC_338_386,(0,0,0):C.UVGC_1149_81,(0,0,2):C.UVGC_1149_82,(0,0,1):C.UVGC_1149_83})

V_380 = CTVertex(name = 'V_380',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,2):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,1):C.UVGC_258_213,(0,1,3):C.UVGC_258_214,(0,1,4):C.UVGC_258_215,(0,1,2):C.UVGC_258_216,(0,2,0):C.UVGC_258_212,(0,2,1):C.UVGC_258_213,(0,2,3):C.UVGC_258_214,(0,2,4):C.UVGC_258_215,(0,2,2):C.UVGC_258_216})

V_381 = CTVertex(name = 'V_381',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,4):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,1):C.UVGC_258_213,(0,1,2):C.UVGC_258_214,(0,1,3):C.UVGC_258_215,(0,1,4):C.UVGC_258_216,(0,2,0):C.UVGC_258_212,(0,2,1):C.UVGC_258_213,(0,2,2):C.UVGC_258_214,(0,2,3):C.UVGC_258_215,(0,2,4):C.UVGC_258_216})

V_382 = CTVertex(name = 'V_382',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,1):C.UVGC_256_210,(0,1,0):C.UVGC_258_212,(0,1,2):C.UVGC_258_213,(0,1,3):C.UVGC_258_214,(0,1,4):C.UVGC_258_215,(0,1,1):C.UVGC_287_250,(0,2,0):C.UVGC_258_212,(0,2,2):C.UVGC_258_213,(0,2,3):C.UVGC_258_214,(0,2,4):C.UVGC_258_215,(0,2,1):C.UVGC_287_250})

V_383 = CTVertex(name = 'V_383',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_257_211,(0,1,0):C.UVGC_245_199,(0,2,0):C.UVGC_245_199})

V_384 = CTVertex(name = 'V_384',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_257_211,(0,1,0):C.UVGC_245_199,(0,2,0):C.UVGC_245_199})

V_385 = CTVertex(name = 'V_385',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_314_322,(0,2,0):C.UVGC_314_322,(0,1,0):C.UVGC_312_320,(0,3,0):C.UVGC_312_320})

V_386 = CTVertex(name = 'V_386',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_257_211,(0,1,0):C.UVGC_245_199,(0,2,0):C.UVGC_245_199})

V_387 = CTVertex(name = 'V_387',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_257_211,(0,1,0):C.UVGC_245_199,(0,2,0):C.UVGC_245_199})

V_388 = CTVertex(name = 'V_388',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_288_251,(0,2,0):C.UVGC_288_251,(0,1,0):C.UVGC_286_249,(0,3,0):C.UVGC_286_249})

V_389 = CTVertex(name = 'V_389',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_304_292,(0,0,1):C.UVGC_304_293,(0,0,2):C.UVGC_304_294,(0,0,3):C.UVGC_304_295,(0,1,0):C.UVGC_303_290,(0,1,3):C.UVGC_303_291})

