# This file was automatically created by FeynRules 2.1.78
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Wed 15 Oct 2014 17:50:53


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_108_1 = Coupling(name = 'R2GC_108_1',
                      value = '-G**4/(192.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_108_2 = Coupling(name = 'R2GC_108_2',
                      value = 'G**4/(64.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_109_3 = Coupling(name = 'R2GC_109_3',
                      value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_109_4 = Coupling(name = 'R2GC_109_4',
                      value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_110_5 = Coupling(name = 'R2GC_110_5',
                      value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_110_6 = Coupling(name = 'R2GC_110_6',
                      value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_111_7 = Coupling(name = 'R2GC_111_7',
                      value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_112_8 = Coupling(name = 'R2GC_112_8',
                      value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_112_9 = Coupling(name = 'R2GC_112_9',
                      value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_113_10 = Coupling(name = 'R2GC_113_10',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_114_11 = Coupling(name = 'R2GC_114_11',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_115_12 = Coupling(name = 'R2GC_115_12',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_116_13 = Coupling(name = 'R2GC_116_13',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_117_14 = Coupling(name = 'R2GC_117_14',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_118_15 = Coupling(name = 'R2GC_118_15',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_124_16 = Coupling(name = 'R2GC_124_16',
                       value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_125_17 = Coupling(name = 'R2GC_125_17',
                       value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_126_18 = Coupling(name = 'R2GC_126_18',
                       value = '(-53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_127_19 = Coupling(name = 'R2GC_127_19',
                       value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_128_20 = Coupling(name = 'R2GC_128_20',
                       value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_128_21 = Coupling(name = 'R2GC_128_21',
                       value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_138_22 = Coupling(name = 'R2GC_138_22',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_140_23 = Coupling(name = 'R2GC_140_23',
                       value = '(complex(0,1)*G**2*m3**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_144_24 = Coupling(name = 'R2GC_144_24',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_145_25 = Coupling(name = 'R2GC_145_25',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_146_26 = Coupling(name = 'R2GC_146_26',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_147_27 = Coupling(name = 'R2GC_147_27',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_148_28 = Coupling(name = 'R2GC_148_28',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_150_29 = Coupling(name = 'R2GC_150_29',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_150_30 = Coupling(name = 'R2GC_150_30',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_151_31 = Coupling(name = 'R2GC_151_31',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_32 = Coupling(name = 'R2GC_151_32',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_152_33 = Coupling(name = 'R2GC_152_33',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_152_34 = Coupling(name = 'R2GC_152_34',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_154_35 = Coupling(name = 'R2GC_154_35',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_155_36 = Coupling(name = 'R2GC_155_36',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_160_37 = Coupling(name = 'R2GC_160_37',
                       value = '-(complex(0,1)*G**2*gtL)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_161_38 = Coupling(name = 'R2GC_161_38',
                       value = '-(complex(0,1)*G**2*gtR)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_163_39 = Coupling(name = 'R2GC_163_39',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_164_40 = Coupling(name = 'R2GC_164_40',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_165_41 = Coupling(name = 'R2GC_165_41',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_166_42 = Coupling(name = 'R2GC_166_42',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_167_43 = Coupling(name = 'R2GC_167_43',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_168_44 = Coupling(name = 'R2GC_168_44',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_169_45 = Coupling(name = 'R2GC_169_45',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_170_46 = Coupling(name = 'R2GC_170_46',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_171_47 = Coupling(name = 'R2GC_171_47',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_75_48 = Coupling(name = 'R2GC_75_48',
                      value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_82_49 = Coupling(name = 'R2GC_82_49',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_82_50 = Coupling(name = 'R2GC_82_50',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_83_51 = Coupling(name = 'R2GC_83_51',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_83_52 = Coupling(name = 'R2GC_83_52',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_84_53 = Coupling(name = 'R2GC_84_53',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_84_54 = Coupling(name = 'R2GC_84_54',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_85_55 = Coupling(name = 'R2GC_85_55',
                      value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_86_56 = Coupling(name = 'R2GC_86_56',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_86_57 = Coupling(name = 'R2GC_86_57',
                      value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_87_58 = Coupling(name = 'R2GC_87_58',
                      value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_87_59 = Coupling(name = 'R2GC_87_59',
                      value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_88_60 = Coupling(name = 'R2GC_88_60',
                      value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_88_61 = Coupling(name = 'R2GC_88_61',
                      value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_89_62 = Coupling(name = 'R2GC_89_62',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_89_63 = Coupling(name = 'R2GC_89_63',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_90_64 = Coupling(name = 'R2GC_90_64',
                      value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_90_65 = Coupling(name = 'R2GC_90_65',
                      value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_91_66 = Coupling(name = 'R2GC_91_66',
                      value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_91_67 = Coupling(name = 'R2GC_91_67',
                      value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_92_68 = Coupling(name = 'R2GC_92_68',
                      value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_92_69 = Coupling(name = 'R2GC_92_69',
                      value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_96_70 = Coupling(name = 'R2GC_96_70',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_97_71 = Coupling(name = 'R2GC_97_71',
                      value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                      order = {'QCD':2,'QED':2})

UVGC_100_1 = Coupling(name = 'UVGC_100_1',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_101_2 = Coupling(name = 'UVGC_101_2',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_102_3 = Coupling(name = 'UVGC_102_3',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_107_4 = Coupling(name = 'UVGC_107_4',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_107_5 = Coupling(name = 'UVGC_107_5',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_108_6 = Coupling(name = 'UVGC_108_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_108_7 = Coupling(name = 'UVGC_108_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_109_8 = Coupling(name = 'UVGC_109_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_109_9 = Coupling(name = 'UVGC_109_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_111_10 = Coupling(name = 'UVGC_111_10',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_111_11 = Coupling(name = 'UVGC_111_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_112_12 = Coupling(name = 'UVGC_112_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_112_13 = Coupling(name = 'UVGC_112_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_113_14 = Coupling(name = 'UVGC_113_14',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_113_15 = Coupling(name = 'UVGC_113_15',
                       value = {-1:'(complex(0,1)*G**4)/(48.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_114_16 = Coupling(name = 'UVGC_114_16',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_115_17 = Coupling(name = 'UVGC_115_17',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_116_18 = Coupling(name = 'UVGC_116_18',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_117_19 = Coupling(name = 'UVGC_117_19',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_118_20 = Coupling(name = 'UVGC_118_20',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_119_21 = Coupling(name = 'UVGC_119_21',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_119_22 = Coupling(name = 'UVGC_119_22',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_119_23 = Coupling(name = 'UVGC_119_23',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_119_24 = Coupling(name = 'UVGC_119_24',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_119_25 = Coupling(name = 'UVGC_119_25',
                       value = {-1:'( 0 if m3 else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_119_26 = Coupling(name = 'UVGC_119_26',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_124_27 = Coupling(name = 'UVGC_124_27',
                       value = {-1:'(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_124_28 = Coupling(name = 'UVGC_124_28',
                       value = {-1:'(3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_29 = Coupling(name = 'UVGC_125_29',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if m3 else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if m3 else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_126_30 = Coupling(name = 'UVGC_126_30',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if m3 else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if m3 else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_127_31 = Coupling(name = 'UVGC_127_31',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_127_32 = Coupling(name = 'UVGC_127_32',
                       value = {-1:'-(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_127_33 = Coupling(name = 'UVGC_127_33',
                       value = {-1:'(19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_127_34 = Coupling(name = 'UVGC_127_34',
                       value = {-1:'(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_127_35 = Coupling(name = 'UVGC_127_35',
                       value = {-1:'( 0 if m3 else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_127_36 = Coupling(name = 'UVGC_127_36',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if m3 else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if m3 else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_128_37 = Coupling(name = 'UVGC_128_37',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(24.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_128_38 = Coupling(name = 'UVGC_128_38',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_39 = Coupling(name = 'UVGC_128_39',
                       value = {-1:'(-7*complex(0,1)*G**4)/(16.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_40 = Coupling(name = 'UVGC_128_40',
                       value = {-1:'-(complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_41 = Coupling(name = 'UVGC_128_41',
                       value = {-1:'( 0 if m3 else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_128_42 = Coupling(name = 'UVGC_128_42',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if m3 else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if m3 else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_138_43 = Coupling(name = 'UVGC_138_43',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_138_44 = Coupling(name = 'UVGC_138_44',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_140_45 = Coupling(name = 'UVGC_140_45',
                       value = {-1:'( (complex(0,1)*G**2*m3**2)/(6.*cmath.pi**2) if m3 else (complex(0,1)*G**2*m3**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*m3**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*m3**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*m3**2*reglog(m3/MU_R))/(2.*cmath.pi**2) if m3 else -(complex(0,1)*G**2*m3**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*m3**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_141_46 = Coupling(name = 'UVGC_141_46',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_142_47 = Coupling(name = 'UVGC_142_47',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_143_48 = Coupling(name = 'UVGC_143_48',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_144_49 = Coupling(name = 'UVGC_144_49',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_145_50 = Coupling(name = 'UVGC_145_50',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_146_51 = Coupling(name = 'UVGC_146_51',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_147_52 = Coupling(name = 'UVGC_147_52',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_148_53 = Coupling(name = 'UVGC_148_53',
                       value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_149_54 = Coupling(name = 'UVGC_149_54',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_149_55 = Coupling(name = 'UVGC_149_55',
                       value = {-1:'( 0 if m3 else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_149_56 = Coupling(name = 'UVGC_149_56',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_150_57 = Coupling(name = 'UVGC_150_57',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_150_58 = Coupling(name = 'UVGC_150_58',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_59 = Coupling(name = 'UVGC_150_59',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_60 = Coupling(name = 'UVGC_150_60',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_61 = Coupling(name = 'UVGC_150_61',
                       value = {-1:'( 0 if m3 else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_62 = Coupling(name = 'UVGC_150_62',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_151_63 = Coupling(name = 'UVGC_151_63',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_151_64 = Coupling(name = 'UVGC_151_64',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_65 = Coupling(name = 'UVGC_151_65',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_66 = Coupling(name = 'UVGC_151_66',
                       value = {-1:'( 0 if m3 else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_67 = Coupling(name = 'UVGC_151_67',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_68 = Coupling(name = 'UVGC_152_68',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_69 = Coupling(name = 'UVGC_152_69',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_70 = Coupling(name = 'UVGC_152_70',
                       value = {-1:'( 0 if m3 else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_71 = Coupling(name = 'UVGC_153_71',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_153_72 = Coupling(name = 'UVGC_153_72',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_73 = Coupling(name = 'UVGC_153_73',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_74 = Coupling(name = 'UVGC_153_74',
                       value = {-1:'( 0 if m3 else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_75 = Coupling(name = 'UVGC_153_75',
                       value = {0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_76 = Coupling(name = 'UVGC_154_76',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_154_77 = Coupling(name = 'UVGC_154_77',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_78 = Coupling(name = 'UVGC_154_78',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_79 = Coupling(name = 'UVGC_154_79',
                       value = {-1:'( 0 if m3 else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_80 = Coupling(name = 'UVGC_154_80',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_155_81 = Coupling(name = 'UVGC_155_81',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_155_82 = Coupling(name = 'UVGC_155_82',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_156_83 = Coupling(name = 'UVGC_156_83',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_156_84 = Coupling(name = 'UVGC_156_84',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_156_85 = Coupling(name = 'UVGC_156_85',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_156_86 = Coupling(name = 'UVGC_156_86',
                       value = {-1:'( 0 if m3 else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(m3/MU_R))/(48.*cmath.pi**2) if m3 else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_156_87 = Coupling(name = 'UVGC_156_87',
                       value = {0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_157_88 = Coupling(name = 'UVGC_157_88',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_158_89 = Coupling(name = 'UVGC_158_89',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_159_90 = Coupling(name = 'UVGC_159_90',
                       value = {-1:'-(complex(0,1)*G**3)/(6.*cmath.pi**2)',0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_160_91 = Coupling(name = 'UVGC_160_91',
                       value = {-1:'( -(complex(0,1)*G**2*gtL)/(12.*cmath.pi**2) if m3 else -(complex(0,1)*G**2*gtL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gtL)/(144.*cmath.pi**2) if m3 else (complex(0,1)*G**2*gtL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gtL)/(144.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_160_92 = Coupling(name = 'UVGC_160_92',
                       value = {-1:'-(complex(0,1)*G**2*gtL)/(12.*cmath.pi**2)',0:'-(complex(0,1)*G**2*gtL)/(6.*cmath.pi**2) + (complex(0,1)*G**2*gtL*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_160_93 = Coupling(name = 'UVGC_160_93',
                       value = {-1:'-(complex(0,1)*G**2*gtL)/(12.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_161_94 = Coupling(name = 'UVGC_161_94',
                       value = {-1:'( -(complex(0,1)*G**2*gtR)/(12.*cmath.pi**2) if m3 else -(complex(0,1)*G**2*gtR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gtR)/(144.*cmath.pi**2) if m3 else (complex(0,1)*G**2*gtR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gtR)/(144.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_161_95 = Coupling(name = 'UVGC_161_95',
                       value = {-1:'-(complex(0,1)*G**2*gtR)/(12.*cmath.pi**2)',0:'-(complex(0,1)*G**2*gtR)/(6.*cmath.pi**2) + (complex(0,1)*G**2*gtR*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_161_96 = Coupling(name = 'UVGC_161_96',
                       value = {-1:'-(complex(0,1)*G**2*gtR)/(12.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_162_97 = Coupling(name = 'UVGC_162_97',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_162_98 = Coupling(name = 'UVGC_162_98',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',0:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_163_99 = Coupling(name = 'UVGC_163_99',
                       value = {-1:'-(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'-(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_164_100 = Coupling(name = 'UVGC_164_100',
                        value = {-1:'(ee*complex(0,1)*G**2*sw)/(6.*cw*cmath.pi**2)',0:'(2*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_165_101 = Coupling(name = 'UVGC_165_101',
                        value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_165_102 = Coupling(name = 'UVGC_165_102',
                        value = {-1:'(G**2*yb)/(12.*cmath.pi**2)',0:'(G**2*yb)/(6.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_165_103 = Coupling(name = 'UVGC_165_103',
                        value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_166_104 = Coupling(name = 'UVGC_166_104',
                        value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_166_105 = Coupling(name = 'UVGC_166_105',
                        value = {-1:'-(G**2*yb)/(12.*cmath.pi**2)',0:'-(G**2*yb)/(6.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_166_106 = Coupling(name = 'UVGC_166_106',
                        value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_167_107 = Coupling(name = 'UVGC_167_107',
                        value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'( (complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else 0 ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_168_108 = Coupling(name = 'UVGC_168_108',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_168_109 = Coupling(name = 'UVGC_168_109',
                        value = {-1:'(G**2*yt)/(12.*cmath.pi**2)',0:'( (G**2*yt)/(3.*cmath.pi**2) - (G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else 0 ) + (G**2*yt)/(6.*cmath.pi**2) - (G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_168_110 = Coupling(name = 'UVGC_168_110',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_169_111 = Coupling(name = 'UVGC_169_111',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_169_112 = Coupling(name = 'UVGC_169_112',
                        value = {-1:'-(G**2*yt)/(12.*cmath.pi**2)',0:'( -(G**2*yt)/(3.*cmath.pi**2) + (G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else 0 ) - (G**2*yt)/(6.*cmath.pi**2) + (G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_169_113 = Coupling(name = 'UVGC_169_113',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_170_114 = Coupling(name = 'UVGC_170_114',
                        value = {-1:'(G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'( (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MT else 0 ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_171_115 = Coupling(name = 'UVGC_171_115',
                        value = {-1:'(complex(0,1)*G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'( (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MT else 0 ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

