Requestor: Qimin Jiang
Contents: scalar octet sgluon model
Link: https://arxiv.org/abs/1805.10835
Twiki: https://twiki.org/cgi-bin/view/Sandbox/TopPhilicModels
JIRA: https://its.cern.ch/jira/browse/AGENE-2215