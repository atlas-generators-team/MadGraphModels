# This file was automatically created by FeynRules 2.3.28
# Mathematica version: 11.2.0 for Linux x86 (64-bit) (September 11, 2017)
# Date: Thu 8 Mar 2018 14:15:13


from object_library import all_orders, CouplingOrder


DVLL = CouplingOrder(name = 'DVLL',
                     expansion_order = 99,
                     hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

