Requestor: Judita Mamuzic
Content: Vector-like leptons in the 4321 model
Source: https://feynrules.irmp.ucl.ac.be/wiki/LeptoQuark
Paper 1: https://link.springer.com/article/10.1140/epjc/s10052-019-6853-x
Paper 2: https://link.springer.com/article/10.1007/JHEP11(2018)081
Paper 3: https://link.springer.com/article/10.1007/JHEP08(2021)050
Authors: Javier Fuentes-Martin, Darius A. Faroughy
Comment: Extends for vector-like fermions (VLQ and VLL), with couplings to new gauge bosons (vector leptoquark U1, coloured vector  and SM singlet ).
JIRA: https://its.cern.ch/jira/browse/AGENE-2155
