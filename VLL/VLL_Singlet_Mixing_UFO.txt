Requestor: Merve Nazlim Agaras (model from Stefan B., Gudrun Hiller, and Clara Hormigos-Feliu)
Content: Singlet vector-like-lepton flavourful model 
Source: https://github.com/MerveNazlim/vector-like-leptons-with-flavor
Paper: https://arxiv.org/pdf/2011.12964.pdf

