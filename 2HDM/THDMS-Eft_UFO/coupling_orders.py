# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.1 for Linux x86 (64-bit) (April 18, 2017)
# Date: Tue 14 Sep 2021 18:00:28


from object_library import all_orders, CouplingOrder


HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

QNP = CouplingOrder(name = 'QNP',
                    expansion_order = 99,
                    hierarchy = 1)

