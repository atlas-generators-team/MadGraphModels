# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.1 for Linux x86 (64-bit) (April 18, 2017)
# Date: Tue 14 Sep 2021 18:00:28



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

kpV = Parameter(name = 'kpV',
                nature = 'external',
                type = 'real',
                value = 0.7,
                texname = 'k_v',
                lhablock = 'HINPUTS',
                lhacode = [ 1 ])

btb = Parameter(name = 'btb',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{btb}',
                lhablock = 'HINPUTS',
                lhacode = [ 2 ])

bta = Parameter(name = 'bta',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{bta}',
                lhablock = 'HINPUTS',
                lhacode = [ 3 ])

gHHR = Parameter(name = 'gHHR',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'g_{\\text{HHR}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 4 ])

gHRS = Parameter(name = 'gHRS',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'g_{\\text{HRS}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 5 ])

gRSS = Parameter(name = 'gRSS',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'g_{\\text{RSS}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 6 ])

GphiR = Parameter(name = 'GphiR',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'g_{\\text{phiR}}',
                  lhablock = 'HINPUTS',
                  lhacode = [ 7 ])

bttR = Parameter(name = 'bttR',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'b_{\\text{tt}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 8 ])

btbR = Parameter(name = 'btbR',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'b_{\\text{tb}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 9 ])

GH = Parameter(name = 'GH',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'g_{\\text{h3}}',
               lhablock = 'HINPUTS',
               lhacode = [ 10 ])

kzrh = Parameter(name = 'kzrh',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'k_{\\text{zrh}}',
                 lhablock = 'HINPUTS',
                 lhacode = [ 11 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Mh3 = Parameter(name = 'Mh3',
                nature = 'external',
                type = 'real',
                value = 270.,
                texname = '\\text{Mh3}',
                lhablock = 'MASS',
                lhacode = [ 36 ])

Mh2 = Parameter(name = 'Mh2',
                nature = 'external',
                type = 'real',
                value = 150.,
                texname = '\\text{Mh2}',
                lhablock = 'MASS',
                lhacode = [ 35 ])

MR = Parameter(name = 'MR',
               nature = 'external',
               type = 'real',
               value = 400.,
               texname = '\\text{MR}',
               lhablock = 'MASS',
               lhacode = [ 40 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WTA = Parameter(name = 'WTA',
                nature = 'external',
                type = 'real',
                value = 2.265e-12,
                texname = '\\text{WTA}',
                lhablock = 'DECAY',
                lhacode = [ 15 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Wh3 = Parameter(name = 'Wh3',
                nature = 'external',
                type = 'real',
                value = 3.4952,
                texname = '\\text{Wh3}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

Wh2 = Parameter(name = 'Wh2',
                nature = 'external',
                type = 'real',
                value = 0.006382339,
                texname = '\\text{Wh2}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

WR = Parameter(name = 'WR',
               nature = 'external',
               type = 'real',
               value = 0.006382339,
               texname = '\\text{WR}',
               lhablock = 'DECAY',
               lhacode = [ 40 ])

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

kVV = Parameter(name = 'kVV',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1. - kpV**2)',
                texname = 'k_{\\text{vv}}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

Gh2 = Parameter(name = 'Gh2',
                nature = 'internal',
                type = 'real',
                value = '(47*ee**2*(1 - (2*Mh2**4)/(987.*MT**4) - (14*Mh2**2)/(705.*MT**2) + (213*Mh2**12)/(2.634632e7*MW**12) + (5*Mh2**10)/(119756.*MW**10) + (41*Mh2**8)/(180950.*MW**8) + (87*Mh2**6)/(65800.*MW**6) + (57*Mh2**4)/(6580.*MW**4) + (33*Mh2**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                texname = 'g_{\\text{h2}}')

GhSM = Parameter(name = 'GhSM',
                 nature = 'internal',
                 type = 'real',
                 value = '(47*ee**2*(1 - (2*MH**4)/(987.*MT**4) - (14*MH**2)/(705.*MT**2) + (213*MH**12)/(2.634632e7*MW**12) + (5*MH**10)/(119756.*MW**10) + (41*MH**8)/(180950.*MW**8) + (87*MH**6)/(65800.*MW**6) + (57*MH**4)/(6580.*MW**4) + (33*MH**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                 texname = 'g_{\\text{hSM}}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

