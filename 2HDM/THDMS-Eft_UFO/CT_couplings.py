# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.1 for Linux x86 (64-bit) (April 18, 2017)
# Date: Tue 14 Sep 2021 18:00:28


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



