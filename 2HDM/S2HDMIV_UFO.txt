Requestor: Baptiste Ravina
Contents: 2HDM type-IV model, extended with a complex singlet yielding a third Higgs boson and a pseudo-scalar DM candidate
Source: private communication from Thomas Biekoetter
Paper: arXiv:2108.10864
