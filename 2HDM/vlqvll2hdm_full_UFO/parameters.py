# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 12.0.0 for Mac OS X x86 (64-bit) (April 7, 2019)
# Date: Wed 26 Jun 2024 14:36:21



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

tanbeta = Parameter(name = 'tanbeta',
                    nature = 'external',
                    type = 'real',
                    value = 7.,
                    texname = '\\text{tanbeta}',
                    lhablock = 'HiggsPAR',
                    lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

gzmul = Parameter(name = 'gzmul',
                  nature = 'external',
                  type = 'real',
                  value = 0.005,
                  texname = '\\text{gzmul}',
                  lhablock = 'VLPAR',
                  lhacode = [ 1 ])

gzmur = Parameter(name = 'gzmur',
                  nature = 'external',
                  type = 'real',
                  value = -0.005,
                  texname = '\\text{gzmur}',
                  lhablock = 'VLPAR',
                  lhacode = [ 2 ])

gawmu = Parameter(name = 'gawmu',
                  nature = 'external',
                  type = 'real',
                  value = 0.002,
                  texname = '\\text{gawmu}',
                  lhablock = 'VLPAR',
                  lhacode = [ 3 ])

gdmzl = Parameter(name = 'gdmzl',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{gdmzl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 4 ])

gdmzr = Parameter(name = 'gdmzr',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{gdmzr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 5 ])

gemzl = Parameter(name = 'gemzl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gemzl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 6 ])

gemzr = Parameter(name = 'gemzr',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{gemzr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 7 ])

gdzr = Parameter(name = 'gdzr',
                 nature = 'external',
                 type = 'real',
                 value = 0.370454,
                 texname = '\\text{gdzr}',
                 lhablock = 'VLPAR',
                 lhacode = [ 8 ])

gezl = Parameter(name = 'gezl',
                 nature = 'external',
                 type = 'real',
                 value = 0.370454,
                 texname = '\\text{gezl}',
                 lhablock = 'VLPAR',
                 lhacode = [ 9 ])

gdwl = Parameter(name = 'gdwl',
                 nature = 'external',
                 type = 'real',
                 value = 0.461759,
                 texname = '\\text{gdwl}',
                 lhablock = 'VLPAR',
                 lhacode = [ 10 ])

gewl = Parameter(name = 'gewl',
                 nature = 'external',
                 type = 'real',
                 value = 0.461759,
                 texname = '\\text{gewl}',
                 lhablock = 'VLPAR',
                 lhacode = [ 11 ])

gnwl = Parameter(name = 'gnwl',
                 nature = 'external',
                 type = 'real',
                 value = 0.461759,
                 texname = '\\text{gnwl}',
                 lhablock = 'VLPAR',
                 lhacode = [ 12 ])

gnnwl = Parameter(name = 'gnnwl',
                  nature = 'external',
                  type = 'real',
                  value = 0.461759,
                  texname = '\\text{gnnwl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 13 ])

gnwr = Parameter(name = 'gnwr',
                 nature = 'external',
                 type = 'real',
                 value = 0.461759,
                 texname = '\\text{gnwr}',
                 lhablock = 'VLPAR',
                 lhacode = [ 14 ])

gnnwr = Parameter(name = 'gnnwr',
                  nature = 'external',
                  type = 'real',
                  value = 0.461759,
                  texname = '\\text{gnnwr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 15 ])

gnzl = Parameter(name = 'gnzl',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{gnzl}',
                 lhablock = 'VLPAR',
                 lhacode = [ 16 ])

ghmu1 = Parameter(name = 'ghmu1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{ghmu1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 17 ])

ghdl1 = Parameter(name = 'ghdl1',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghdl1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 18 ])

ghdr1 = Parameter(name = 'ghdr1',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghdr1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 19 ])

ghel1 = Parameter(name = 'ghel1',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghel1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 20 ])

gher1 = Parameter(name = 'gher1',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{gher1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 21 ])

ghel1H = Parameter(name = 'ghel1H',
                   nature = 'external',
                   type = 'real',
                   value = -0.1,
                   texname = '\\text{ghel1H}',
                   lhablock = 'VLPAR',
                   lhacode = [ 22 ])

gher1H = Parameter(name = 'gher1H',
                   nature = 'external',
                   type = 'real',
                   value = -0.1,
                   texname = '\\text{gher1H}',
                   lhablock = 'VLPAR',
                   lhacode = [ 23 ])

gdyuk1 = Parameter(name = 'gdyuk1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{gdyuk1}',
                   lhablock = 'VLPAR',
                   lhacode = [ 24 ])

geyuk1 = Parameter(name = 'geyuk1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{geyuk1}',
                   lhablock = 'VLPAR',
                   lhacode = [ 25 ])

gnhr1 = Parameter(name = 'gnhr1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{gnhr1}',
                  lhablock = 'VLPAR',
                  lhacode = [ 26 ])

gnyuk1 = Parameter(name = 'gnyuk1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{gnyuk1}',
                   lhablock = 'VLPAR',
                   lhacode = [ 27 ])

ghmu2 = Parameter(name = 'ghmu2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{ghmu2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 28 ])

ghdl2 = Parameter(name = 'ghdl2',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghdl2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 29 ])

ghdr2 = Parameter(name = 'ghdr2',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghdr2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 30 ])

ghel2 = Parameter(name = 'ghel2',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{ghel2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 31 ])

gher2 = Parameter(name = 'gher2',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\text{gher2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 32 ])

gdyuk2 = Parameter(name = 'gdyuk2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{gdyuk2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 33 ])

geyuk2 = Parameter(name = 'geyuk2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{geyuk2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 34 ])

gnhr2 = Parameter(name = 'gnhr2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{gnhr2}',
                  lhablock = 'VLPAR',
                  lhacode = [ 35 ])

gnyuk2 = Parameter(name = 'gnyuk2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{gnyuk2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 36 ])

gdezl = Parameter(name = 'gdezl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gdezl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 37 ])

gdezr = Parameter(name = 'gdezr',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gdezr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 38 ])

geezl = Parameter(name = 'geezl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{geezl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 39 ])

geezr = Parameter(name = 'geezr',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{geezr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 40 ])

gdewl = Parameter(name = 'gdewl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gdewl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 41 ])

geewl = Parameter(name = 'geewl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{geewl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 42 ])

gnewl = Parameter(name = 'gnewl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gnewl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 43 ])

gnewr = Parameter(name = 'gnewr',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gnewr}',
                  lhablock = 'VLPAR',
                  lhacode = [ 44 ])

gnezl = Parameter(name = 'gnezl',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gnezl}',
                  lhablock = 'VLPAR',
                  lhacode = [ 45 ])

gdtazl = Parameter(name = 'gdtazl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gdtazl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 46 ])

gdtazr = Parameter(name = 'gdtazr',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gdtazr}',
                   lhablock = 'VLPAR',
                   lhacode = [ 47 ])

getazl = Parameter(name = 'getazl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{getazl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 48 ])

getazr = Parameter(name = 'getazr',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{getazr}',
                   lhablock = 'VLPAR',
                   lhacode = [ 49 ])

gdtawl = Parameter(name = 'gdtawl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gdtawl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 50 ])

getawl = Parameter(name = 'getawl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{getawl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 51 ])

gntawl = Parameter(name = 'gntawl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gntawl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 52 ])

gntawr = Parameter(name = 'gntawr',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gntawr}',
                   lhablock = 'VLPAR',
                   lhacode = [ 53 ])

gntazl = Parameter(name = 'gntazl',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gntazl}',
                   lhablock = 'VLPAR',
                   lhacode = [ 54 ])

ghdel = Parameter(name = 'ghdel',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{ghdel}',
                  lhablock = 'VLPAR',
                  lhacode = [ 55 ])

ghder = Parameter(name = 'ghder',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{ghder}',
                  lhablock = 'VLPAR',
                  lhacode = [ 56 ])

gheel = Parameter(name = 'gheel',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gheel}',
                  lhablock = 'VLPAR',
                  lhacode = [ 57 ])

gheer = Parameter(name = 'gheer',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gheer}',
                  lhablock = 'VLPAR',
                  lhacode = [ 58 ])

gnher = Parameter(name = 'gnher',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{gnher}',
                  lhablock = 'VLPAR',
                  lhacode = [ 59 ])

ghdtal = Parameter(name = 'ghdtal',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghdtal}',
                   lhablock = 'VLPAR',
                   lhacode = [ 60 ])

ghdtar = Parameter(name = 'ghdtar',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghdtar}',
                   lhablock = 'VLPAR',
                   lhacode = [ 61 ])

ghetal = Parameter(name = 'ghetal',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghetal}',
                   lhablock = 'VLPAR',
                   lhacode = [ 62 ])

ghetar = Parameter(name = 'ghetar',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghetar}',
                   lhablock = 'VLPAR',
                   lhacode = [ 63 ])

gnhtar = Parameter(name = 'gnhtar',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gnhtar}',
                   lhablock = 'VLPAR',
                   lhacode = [ 64 ])

ghdel2 = Parameter(name = 'ghdel2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghdel2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 65 ])

ghder2 = Parameter(name = 'ghder2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{ghder2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 66 ])

gheel2 = Parameter(name = 'gheel2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gheel2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 67 ])

gheer2 = Parameter(name = 'gheer2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gheer2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 68 ])

gnher2 = Parameter(name = 'gnher2',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{gnher2}',
                   lhablock = 'VLPAR',
                   lhacode = [ 69 ])

ghdtal2 = Parameter(name = 'ghdtal2',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = '\\text{ghdtal2}',
                    lhablock = 'VLPAR',
                    lhacode = [ 70 ])

ghdtar2 = Parameter(name = 'ghdtar2',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = '\\text{ghdtar2}',
                    lhablock = 'VLPAR',
                    lhacode = [ 71 ])

ghetal2 = Parameter(name = 'ghetal2',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = '\\text{ghetal2}',
                    lhablock = 'VLPAR',
                    lhacode = [ 72 ])

ghetar2 = Parameter(name = 'ghetar2',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = '\\text{ghetar2}',
                    lhablock = 'VLPAR',
                    lhacode = [ 73 ])

gnhtar2 = Parameter(name = 'gnhtar2',
                    nature = 'external',
                    type = 'real',
                    value = 0.1,
                    texname = '\\text{gnhtar2}',
                    lhablock = 'VLPAR',
                    lhacode = [ 74 ])

lammu = Parameter(name = 'lammu',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\text{lammu}',
                  lhablock = 'VLPAR',
                  lhacode = [ 75 ])

lam3mu = Parameter(name = 'lam3mu',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{lam3mu}',
                   lhablock = 'VLPAR',
                   lhacode = [ 76 ])

gzh = Parameter(name = 'gzh',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{gzh}',
                lhablock = 'VLPAR',
                lhacode = [ 77 ])

gzvlqtl = Parameter(name = 'gzvlqtl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gzvlqtl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 1 ])

gzvlqtr = Parameter(name = 'gzvlqtr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gzvlqtr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 2 ])

gzvlqbl = Parameter(name = 'gzvlqbl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gzvlqbl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 3 ])

gzvlqbr = Parameter(name = 'gzvlqbr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gzvlqbr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 4 ])

gzvlqt0l = Parameter(name = 'gzvlqt0l',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gzvlqt0l}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 5 ])

gzvlqt0r = Parameter(name = 'gzvlqt0r',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gzvlqt0r}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 6 ])

gzvlqb0l = Parameter(name = 'gzvlqb0l',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gzvlqb0l}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 7 ])

gzvlqb0r = Parameter(name = 'gzvlqb0r',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gzvlqb0r}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 8 ])

gwvlqtl = Parameter(name = 'gwvlqtl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlqtl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 9 ])

gwvlqtr = Parameter(name = 'gwvlqtr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlqtr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 10 ])

gwvlqbl = Parameter(name = 'gwvlqbl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlqbl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 11 ])

gwvlqbr = Parameter(name = 'gwvlqbr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlqbr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 12 ])

gwvlq0l = Parameter(name = 'gwvlq0l',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlq0l}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 13 ])

gwvlq0r = Parameter(name = 'gwvlq0r',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{gwvlq0r}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 14 ])

ghvlqtl = Parameter(name = 'ghvlqtl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqtl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 15 ])

ghvlqtr = Parameter(name = 'ghvlqtr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqtr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 16 ])

ghvlqbl = Parameter(name = 'ghvlqbl',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqbl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 17 ])

ghvlqbr = Parameter(name = 'ghvlqbr',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqbr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 18 ])

ghvlqt0 = Parameter(name = 'ghvlqt0',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqt0}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 19 ])

ghvlqb0 = Parameter(name = 'ghvlqb0',
                    nature = 'external',
                    type = 'real',
                    value = -0.1,
                    texname = '\\text{ghvlqb0}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 20 ])

gh2vlqtl = Parameter(name = 'gh2vlqtl',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqtl}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 21 ])

gh2vlqtr = Parameter(name = 'gh2vlqtr',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqtr}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 22 ])

gh2vlqbl = Parameter(name = 'gh2vlqbl',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqbl}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 23 ])

gh2vlqbr = Parameter(name = 'gh2vlqbr',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqbr}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 24 ])

gh2vlqt0 = Parameter(name = 'gh2vlqt0',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqt0}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 25 ])

gh2vlqb0 = Parameter(name = 'gh2vlqb0',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gh2vlqb0}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 26 ])

gHpvlqtl = Parameter(name = 'gHpvlqtl',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gHpvlqtl}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 27 ])

gHmvlqtr = Parameter(name = 'gHmvlqtr',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gHmvlqtr}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 28 ])

gHmvlqbl = Parameter(name = 'gHmvlqbl',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gHmvlqbl}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 29 ])

gHpvlqbr = Parameter(name = 'gHpvlqbr',
                     nature = 'external',
                     type = 'real',
                     value = -0.1,
                     texname = '\\text{gHpvlqbr}',
                     lhablock = 'VLQPAR',
                     lhacode = [ 30 ])

gAvlqtl = Parameter(name = 'gAvlqtl',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqtl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 31 ])

gAvlqbl = Parameter(name = 'gAvlqbl',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqbl}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 32 ])

gAvlqtr = Parameter(name = 'gAvlqtr',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqtr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 33 ])

gAvlqbr = Parameter(name = 'gAvlqbr',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqbr}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 34 ])

gAvlqt0 = Parameter(name = 'gAvlqt0',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqt0}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 35 ])

gAvlqb0 = Parameter(name = 'gAvlqb0',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\text{gAvlqb0}',
                    lhablock = 'VLQPAR',
                    lhacode = [ 36 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.385,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 173.07,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

Mnv = Parameter(name = 'Mnv',
                nature = 'external',
                type = 'real',
                value = 150,
                texname = '\\text{Mnv}',
                lhablock = 'MASS',
                lhacode = [ 6000018 ])

Mnl = Parameter(name = 'Mnl',
                nature = 'external',
                type = 'real',
                value = 150,
                texname = '\\text{Mnl}',
                lhablock = 'MASS',
                lhacode = [ 6000017 ])

Mnnv = Parameter(name = 'Mnnv',
                 nature = 'external',
                 type = 'real',
                 value = 150,
                 texname = '\\text{Mnnv}',
                 lhablock = 'MASS',
                 lhacode = [ 5000018 ])

Mne = Parameter(name = 'Mne',
                nature = 'external',
                type = 'real',
                value = 150,
                texname = '\\text{Mne}',
                lhablock = 'MASS',
                lhacode = [ 5000017 ])

Mvlqt = Parameter(name = 'Mvlqt',
                  nature = 'external',
                  type = 'real',
                  value = 1000,
                  texname = '\\text{Mvlqt}',
                  lhablock = 'MASS',
                  lhacode = [ 8 ])

Mvlqb = Parameter(name = 'Mvlqb',
                  nature = 'external',
                  type = 'real',
                  value = 1000,
                  texname = '\\text{Mvlqb}',
                  lhablock = 'MASS',
                  lhacode = [ 7 ])

MH01 = Parameter(name = 'MH01',
                 nature = 'external',
                 type = 'real',
                 value = 125,
                 texname = '\\text{MH01}',
                 lhablock = 'MASS',
                 lhacode = [ 25 ])

MH02 = Parameter(name = 'MH02',
                 nature = 'external',
                 type = 'real',
                 value = 200,
                 texname = '\\text{MH02}',
                 lhablock = 'MASS',
                 lhacode = [ 35 ])

MA0 = Parameter(name = 'MA0',
                nature = 'external',
                type = 'real',
                value = 250,
                texname = '\\text{MA0}',
                lhablock = 'MASS',
                lhacode = [ 36 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 200,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 37 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 2.,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

Wnv = Parameter(name = 'Wnv',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{Wnv}',
                lhablock = 'DECAY',
                lhacode = [ 6000018 ])

Wnl = Parameter(name = 'Wnl',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{Wnl}',
                lhablock = 'DECAY',
                lhacode = [ 6000017 ])

Wnnv = Parameter(name = 'Wnnv',
                 nature = 'external',
                 type = 'real',
                 value = 0.00407,
                 texname = '\\text{Wnnv}',
                 lhablock = 'DECAY',
                 lhacode = [ 5000018 ])

Wne = Parameter(name = 'Wne',
                nature = 'external',
                type = 'real',
                value = 0.00407,
                texname = '\\text{Wne}',
                lhablock = 'DECAY',
                lhacode = [ 5000017 ])

Wvlqt = Parameter(name = 'Wvlqt',
                  nature = 'external',
                  type = 'real',
                  value = 1,
                  texname = '\\text{Wvlqt}',
                  lhablock = 'DECAY',
                  lhacode = [ 8 ])

Wvlqb = Parameter(name = 'Wvlqb',
                  nature = 'external',
                  type = 'real',
                  value = 1,
                  texname = '\\text{Wvlqb}',
                  lhablock = 'DECAY',
                  lhacode = [ 7 ])

WH01 = Parameter(name = 'WH01',
                 nature = 'external',
                 type = 'real',
                 value = 0.00407,
                 texname = '\\text{WH01}',
                 lhablock = 'DECAY',
                 lhacode = [ 25 ])

WH02 = Parameter(name = 'WH02',
                 nature = 'external',
                 type = 'real',
                 value = 0.00407, 
                 texname = '\\text{WH02}',
                 lhablock = 'DECAY',
                 lhacode = [ 35 ])

WA0 = Parameter(name = 'WA0',
                nature = 'external',
                type = 'real',
                value = 0.001,
                texname = '\\text{WA0}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 37 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

beta = Parameter(name = 'beta',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.atan(tanbeta)',
                 texname = '\\beta')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

gHhh = Parameter(name = 'gHhh',
                 nature = 'internal',
                 type = 'real',
                 value = '3.*cmath.cos(2.*beta)*cmath.sin(2.*beta)',
                 texname = '\\text{gHhh}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

vd = Parameter(name = 'vd',
               nature = 'internal',
               type = 'real',
               value = 'vev*cmath.cos(beta)',
               texname = '\\text{vd}')

vu = Parameter(name = 'vu',
               nature = 'internal',
               type = 'real',
               value = 'vev*cmath.sin(beta)',
               texname = '\\text{vu}')

Ah = Parameter(name = 'Ah',
               nature = 'internal',
               type = 'real',
               value = '(47*ee**2*(1 - (2*MH01**4)/(987.*MT**4) - (14*MH01**2)/(705.*MT**2) + (213*MH01**12)/(2.634632e7*MW**12) + (5*MH01**10)/(119756.*MW**10) + (41*MH01**8)/(180950.*MW**8) + (87*MH01**6)/(65800.*MW**6) + (57*MH01**4)/(6580.*MW**4) + (33*MH01**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
               texname = 'A_h')

AHH = Parameter(name = 'AHH',
                nature = 'internal',
                type = 'real',
                value = '(47*ee**2*(1 - (2*MH02**4)/(987.*MT**4) - (14*MH02**2)/(705.*MT**2) + (213*MH02**12)/(2.634632e7*MW**12) + (5*MH02**10)/(119756.*MW**10) + (41*MH02**8)/(180950.*MW**8) + (87*MH02**6)/(65800.*MW**6) + (57*MH02**4)/(6580.*MW**4) + (33*MH02**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                texname = 'A_H')

gdzl = Parameter(name = 'gdzl',
                 nature = 'internal',
                 type = 'real',
                 value = '(gw*(-0.5 + sw2))/cw',
                 texname = '\\text{gdzl}')

gezr = Parameter(name = 'gezr',
                 nature = 'internal',
                 type = 'real',
                 value = '(gw*sw2)/cw',
                 texname = '\\text{gezr}')

Gh = Parameter(name = 'Gh',
               nature = 'internal',
               type = 'real',
               value = '-(G**2*(1 + (13*MH01**6)/(16800.*MT**6) + MH01**4/(168.*MT**4) + (7*MH01**2)/(120.*MT**2)))/(12.*cmath.pi**2*vev)',
               texname = 'G_h')

GHH = Parameter(name = 'GHH',
                nature = 'internal',
                type = 'real',
                value = '-(G**2*(1 + (13*MH02**6)/(16800.*MT**6) + MH02**4/(168.*MT**4) + (7*MH02**2)/(120.*MT**2)))/(12.*cmath.pi**2*vev)',
                texname = 'G_H')

gnvzl = Parameter(name = 'gnvzl',
                  nature = 'internal',
                  type = 'real',
                  value = 'gw/(2.*cw)',
                  texname = '\\text{gnvzl}')

gnvzr = Parameter(name = 'gnvzr',
                  nature = 'internal',
                  type = 'real',
                  value = 'gw/(2.*cw)',
                  texname = '\\text{gnvzr}')

Gphi = Parameter(name = 'Gphi',
                 nature = 'internal',
                 type = 'real',
                 value = '-(G**2*(1 + MA0**6/(560.*MT**6) + MA0**4/(90.*MT**4) + MA0**2/(12.*MT**2)))/(8.*cmath.pi**2*vev)',
                 texname = 'G_A')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2)*sec(beta))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*csc(beta)*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2)*sec(beta))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2)*sec(beta))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2)*sec(beta))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2)*sec(beta))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*csc(beta)*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2)*sec(beta))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*csc(beta)*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1a11 = Parameter(name = 'I1a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM1x1)',
                  texname = '\\text{I1a11}')

I1a12 = Parameter(name = 'I1a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM2x1)',
                  texname = '\\text{I1a12}')

I1a13 = Parameter(name = 'I1a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM3x1)',
                  texname = '\\text{I1a13}')

I1a21 = Parameter(name = 'I1a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM1x2)',
                  texname = '\\text{I1a21}')

I1a22 = Parameter(name = 'I1a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM2x2)',
                  texname = '\\text{I1a22}')

I1a23 = Parameter(name = 'I1a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM3x2)',
                  texname = '\\text{I1a23}')

I1a31 = Parameter(name = 'I1a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM1x3)',
                  texname = '\\text{I1a31}')

I1a32 = Parameter(name = 'I1a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM2x3)',
                  texname = '\\text{I1a32}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM3x3)',
                  texname = '\\text{I1a33}')

I2a11 = Parameter(name = 'I2a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x1)',
                  texname = '\\text{I2a11}')

I2a12 = Parameter(name = 'I2a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x1)',
                  texname = '\\text{I2a12}')

I2a13 = Parameter(name = 'I2a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x1)',
                  texname = '\\text{I2a13}')

I2a21 = Parameter(name = 'I2a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x2)',
                  texname = '\\text{I2a21}')

I2a22 = Parameter(name = 'I2a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x2)',
                  texname = '\\text{I2a22}')

I2a23 = Parameter(name = 'I2a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x2)',
                  texname = '\\text{I2a23}')

I2a31 = Parameter(name = 'I2a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yup*complexconjugate(CKM1x3)',
                  texname = '\\text{I2a31}')

I2a32 = Parameter(name = 'I2a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yc*complexconjugate(CKM2x3)',
                  texname = '\\text{I2a32}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt*complexconjugate(CKM3x3)',
                  texname = '\\text{I2a33}')

I3a11 = Parameter(name = 'I3a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x1*yup',
                  texname = '\\text{I3a11}')

I3a12 = Parameter(name = 'I3a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x2*yup',
                  texname = '\\text{I3a12}')

I3a13 = Parameter(name = 'I3a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x3*yup',
                  texname = '\\text{I3a13}')

I3a21 = Parameter(name = 'I3a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x1*yc',
                  texname = '\\text{I3a21}')

I3a22 = Parameter(name = 'I3a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x2*yc',
                  texname = '\\text{I3a22}')

I3a23 = Parameter(name = 'I3a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x3*yc',
                  texname = '\\text{I3a23}')

I3a31 = Parameter(name = 'I3a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x1*yt',
                  texname = '\\text{I3a31}')

I3a32 = Parameter(name = 'I3a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x2*yt',
                  texname = '\\text{I3a32}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x3*yt',
                  texname = '\\text{I3a33}')

I4a11 = Parameter(name = 'I4a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x1*ydo',
                  texname = '\\text{I4a11}')

I4a12 = Parameter(name = 'I4a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x2*ys',
                  texname = '\\text{I4a12}')

I4a13 = Parameter(name = 'I4a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x3*yb',
                  texname = '\\text{I4a13}')

I4a21 = Parameter(name = 'I4a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x1*ydo',
                  texname = '\\text{I4a21}')

I4a22 = Parameter(name = 'I4a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x2*ys',
                  texname = '\\text{I4a22}')

I4a23 = Parameter(name = 'I4a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x3*yb',
                  texname = '\\text{I4a23}')

I4a31 = Parameter(name = 'I4a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x1*ydo',
                  texname = '\\text{I4a31}')

I4a32 = Parameter(name = 'I4a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x2*ys',
                  texname = '\\text{I4a32}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x3*yb',
                  texname = '\\text{I4a33}')

