# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 12.0.0 for Mac OS X x86 (64-bit) (April 7, 2019)
# Date: Tue 15 Oct 2019 22:22:58


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 99,
                    hierarchy = 1)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

