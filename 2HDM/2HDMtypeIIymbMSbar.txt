Requestor: Pawel Bruckman De Renstrom
Contents: Intermediate-mass H+ at LO
Paper: https://arxiv.org/abs/1607.05291
Website: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/chargedHiggs#no1
JIRA: https://its.cern.ch/jira/browse/AGENE-1474