Requestor: Malak Ait Tamlihat
Content:  New model including new real scalar S in a two Higgs-doublet model (2HDM)
Source: Mukesh Kumar [mukesh.kumar@cern.ch]
Papers: https://inspirehep.net/literature/1467465
