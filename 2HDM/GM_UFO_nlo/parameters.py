# This file was automatically created by FeynRules 2.1.88
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Tue 3 Feb 2015 18:16:32



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 80.398,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
lam2 = Parameter(name = 'lam2',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\lambda _2',
                 lhablock = 'POTENTIALPARAM',
                 lhacode = [ 1 ])

lam3 = Parameter(name = 'lam3',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\lambda _3',
                 lhablock = 'POTENTIALPARAM',
                 lhacode = [ 2 ])

lam4 = Parameter(name = 'lam4',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\lambda _4',
                 lhablock = 'POTENTIALPARAM',
                 lhacode = [ 3 ])

lam5 = Parameter(name = 'lam5',
                 nature = 'external',
                 type = 'real',
                 value = 0.1,
                 texname = '\\lambda _5',
                 lhablock = 'POTENTIALPARAM',
                 lhacode = [ 4 ])

M1coeff = Parameter(name = 'M1coeff',
                    nature = 'external',
                    type = 'real',
                    value = 100,
                    texname = 'M_1',
                    lhablock = 'POTENTIALPARAM',
                    lhacode = [ 5 ])

M2coeff = Parameter(name = 'M2coeff',
                    nature = 'external',
                    type = 'real',
                    value = 100,
                    texname = 'M_2',
                    lhablock = 'POTENTIALPARAM',
                    lhacode = [ 6 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 132.4,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

tanth = Parameter(name = 'tanth',
                  nature = 'external',
                  type = 'real',
                  value = 0.198273,
                  texname = 't_H',
                  lhablock = 'VEV',
                  lhacode = [ 1 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172.,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MM = Parameter(name = 'MM',
               nature = 'external',
               type = 'real',
               value = 0.10566,
               texname = '\\text{MM}',
               lhablock = 'MASS',
               lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Mh = Parameter(name = 'Mh',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{Mh}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MH5 = Parameter(name = 'MH5',
                nature = 'external',
                type = 'real',
                value = 339.749,
                texname = 'M_5',
                lhablock = 'MASS',
                lhacode = [ 257 ])
#                value = 'cmath.sqrt(12*M2coeff*vchi + 8*lam3*vchi**2 + (3*lam5*vphi**2)/2. + (M1coeff*vphi**2)/(4.*vchi))',


MH3 = Parameter(name = 'MH3',
                nature = 'external',
                type = 'real',
                value = 304.222,
                texname = 'M_3',
                lhablock = 'MASS',
                lhacode = [ 254 ])
#                value = 'cmath.sqrt(v**2*(lam5/2. + M1coeff/(4.*vchi)))',

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 288.268,
               texname = 'M_H',
               lhablock = 'MASS',
               lhacode = [ 252 ])
#               value = 'cmath.sqrt(Mat11sq + Mat22sq - Mh**2)',

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

Wh = Parameter(name = 'Wh',
               nature = 'external',
               type = 'real',
               value = 0.00575308848,
               texname = '\\text{Wh}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 252 ])

WH3p = Parameter(name = 'WH3p',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WH3p}',
                 lhablock = 'DECAY',
                 lhacode = [ 253 ])

WH3z = Parameter(name = 'WH3z',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WH3z}',
                 lhablock = 'DECAY',
                 lhacode = [ 254 ])

WH5pp = Parameter(name = 'WH5pp',
                  nature = 'external',
                  type = 'real',
                  value = 1,
                  texname = '\\text{WH5pp}',
                  lhablock = 'DECAY',
                  lhacode = [ 255 ])

WH5p = Parameter(name = 'WH5p',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WH5p}',
                 lhablock = 'DECAY',
                 lhacode = [ 256 ])

WH5z = Parameter(name = 'WH5z',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WH5z}',
                 lhablock = 'DECAY',
                 lhacode = [ 257 ])

sh = Parameter(name = 'sh',
               nature = 'internal',
               type = 'real',
               value = 'tanth/cmath.sqrt(1 + tanth**2)',
               texname = 's_H')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\text{aEW}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

v = Parameter(name = 'v',
              nature = 'internal',
              type = 'real',
              value = '1/(2**0.25*cmath.sqrt(Gf))',
              texname = 'v')

ch = Parameter(name = 'ch',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sh**2)',
               texname = 'c_H')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

vchi = Parameter(name = 'vchi',
                 nature = 'internal',
                 type = 'real',
                 value = 'sh/(2.*2**0.75*cmath.sqrt(Gf))',
                 texname = 'v_{\\chi }')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

vphi = Parameter(name = 'vphi',
                 nature = 'internal',
                 type = 'real',
                 value = '(2*vchi*cmath.sqrt(2))/tanth',
                 texname = 'v_{\\phi }')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

Mat12sq = Parameter(name = 'Mat12sq',
                    nature = 'internal',
                    type = 'real',
                    value = '((-M1coeff + 4*(2*lam2 - lam5)*vchi)*vphi*cmath.sqrt(3))/2.',
                    texname = '\\text{Mat12sq}')

Mat22sq = Parameter(name = 'Mat22sq',
                    nature = 'internal',
                    type = 'real',
                    value = '-6*M2coeff*vchi + 8*(lam3 + 3*lam4)*vchi**2 + (M1coeff*vphi**2)/(4.*vchi)',
                    texname = '\\text{Mat22sq}')

mu3sq = Parameter(name = 'mu3sq',
                  nature = 'internal',
                  type = 'real',
                  value = '6*M2coeff*vchi - 4*(lam3 + 3*lam4)*vchi**2 - (2*lam2 - lam5)*vphi**2 + (M1coeff*vphi**2)/(4.*vchi)',
                  texname = '\\text{mu3sq}')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vphi',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vphi',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vphi',
                 texname = '\\text{ytau}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

lam1 = Parameter(name = 'lam1',
                 nature = 'internal',
                 type = 'real',
                 value = '(Mh**2 + Mat12sq**2/(Mat22sq - Mh**2))/(8.*vphi**2)',
                 texname = '\\lambda _1')

Mat11sq = Parameter(name = 'Mat11sq',
                    nature = 'internal',
                    type = 'real',
                    value = '8*lam1*vphi**2',
                    texname = '\\text{Mat11sq}')

mu2sq = Parameter(name = 'mu2sq',
                  nature = 'internal',
                  type = 'real',
                  value = '(3*M1coeff*vchi)/2. - 3*(2*lam2 - lam5)*vchi**2 - 4*lam1*vphi**2',
                  texname = '\\text{mu2sq}')

sa = Parameter(name = 'sa',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(0.5*cmath.asin((2*Mat12sq)/(-Mh**2 + MH**2)))',
               texname = 's_{\\alpha }')

ca = Parameter(name = 'ca',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sa**2)',
               texname = 'c_{\\alpha }')

