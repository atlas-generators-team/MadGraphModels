# This file was automatically created by FeynRules 2.1.91
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 20 Mar 2015 20:27:27


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_269_1 = Coupling(name = 'R2GC_269_1',
                      value = '-(cw*complex(0,1)*G**2*g1)/(9.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_270_2 = Coupling(name = 'R2GC_270_2',
                      value = '(complex(0,1)*G**2*g1*sw)/(9.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_271_3 = Coupling(name = 'R2GC_271_3',
                      value = '(cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_272_4 = Coupling(name = 'R2GC_272_4',
                      value = '-(complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_277_5 = Coupling(name = 'R2GC_277_5',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_277_6 = Coupling(name = 'R2GC_277_6',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_278_7 = Coupling(name = 'R2GC_278_7',
                      value = '-(complex(0,1)*G**2*ymb**2)/(8.*cmath.pi**2*vev**2)',
                      order = {'QCD':2,'QED':2,'YB':2})

R2GC_278_8 = Coupling(name = 'R2GC_278_8',
                      value = '-(complex(0,1)*G**2*ymt**2)/(8.*cmath.pi**2*vev**2)',
                      order = {'QCD':2,'QED':2,'YT':2})

R2GC_279_9 = Coupling(name = 'R2GC_279_9',
                      value = '(cw*G**2*gw)/(48.*cmath.pi**2) + (G**2*g1*sw)/(48.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_279_10 = Coupling(name = 'R2GC_279_10',
                       value = '-(cw*G**2*gw)/(48.*cmath.pi**2) - (G**2*g1*sw)/(48.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_281_11 = Coupling(name = 'R2GC_281_11',
                       value = '-(cw*complex(0,1)*G**3*gw)/(192.*cmath.pi**2) + (complex(0,1)*G**3*g1*sw)/(576.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_281_12 = Coupling(name = 'R2GC_281_12',
                       value = '(cw*complex(0,1)*G**3*gw)/(192.*cmath.pi**2) - (5*complex(0,1)*G**3*g1*sw)/(576.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_282_13 = Coupling(name = 'R2GC_282_13',
                       value = '(-3*cw*complex(0,1)*G**3*gw)/(64.*cmath.pi**2) - (3*complex(0,1)*G**3*g1*sw)/(64.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_282_14 = Coupling(name = 'R2GC_282_14',
                       value = '(3*cw*complex(0,1)*G**3*gw)/(64.*cmath.pi**2) + (3*complex(0,1)*G**3*g1*sw)/(64.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_283_15 = Coupling(name = 'R2GC_283_15',
                       value = '-(cw*G**2*g1)/(48.*cmath.pi**2) + (G**2*gw*sw)/(48.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_283_16 = Coupling(name = 'R2GC_283_16',
                       value = '(cw*G**2*g1)/(48.*cmath.pi**2) - (G**2*gw*sw)/(48.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_285_17 = Coupling(name = 'R2GC_285_17',
                       value = '-(cw*complex(0,1)*G**3*g1)/(576.*cmath.pi**2) - (complex(0,1)*G**3*gw*sw)/(192.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_285_18 = Coupling(name = 'R2GC_285_18',
                       value = '(5*cw*complex(0,1)*G**3*g1)/(576.*cmath.pi**2) + (complex(0,1)*G**3*gw*sw)/(192.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_286_19 = Coupling(name = 'R2GC_286_19',
                       value = '(-3*cw*complex(0,1)*G**3*g1)/(64.*cmath.pi**2) + (3*complex(0,1)*G**3*gw*sw)/(64.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_286_20 = Coupling(name = 'R2GC_286_20',
                       value = '(3*cw*complex(0,1)*G**3*g1)/(64.*cmath.pi**2) - (3*complex(0,1)*G**3*gw*sw)/(64.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_287_21 = Coupling(name = 'R2GC_287_21',
                       value = '(cw**2*complex(0,1)*G**2*gw**2)/(192.*cmath.pi**2) + (cw*complex(0,1)*G**2*g1*gw*sw)/(288.*cmath.pi**2) + (5*complex(0,1)*G**2*g1**2*sw**2)/(1728.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_287_22 = Coupling(name = 'R2GC_287_22',
                       value = '(cw**2*complex(0,1)*G**2*gw**2)/(192.*cmath.pi**2) - (cw*complex(0,1)*G**2*g1*gw*sw)/(288.*cmath.pi**2) + (17*complex(0,1)*G**2*g1**2*sw**2)/(1728.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_288_23 = Coupling(name = 'R2GC_288_23',
                       value = '-(cw**2*complex(0,1)*G**2*g1*gw)/(576.*cmath.pi**2) - (5*cw*complex(0,1)*G**2*g1**2*sw)/(1728.*cmath.pi**2) + (cw*complex(0,1)*G**2*gw**2*sw)/(192.*cmath.pi**2) + (complex(0,1)*G**2*g1*gw*sw**2)/(576.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_288_24 = Coupling(name = 'R2GC_288_24',
                       value = '(cw**2*complex(0,1)*G**2*g1*gw)/(576.*cmath.pi**2) - (17*cw*complex(0,1)*G**2*g1**2*sw)/(1728.*cmath.pi**2) + (cw*complex(0,1)*G**2*gw**2*sw)/(192.*cmath.pi**2) - (complex(0,1)*G**2*g1*gw*sw**2)/(576.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_289_25 = Coupling(name = 'R2GC_289_25',
                       value = '(5*cw**2*complex(0,1)*G**2*g1**2)/(1728.*cmath.pi**2) - (cw*complex(0,1)*G**2*g1*gw*sw)/(288.*cmath.pi**2) + (complex(0,1)*G**2*gw**2*sw**2)/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_289_26 = Coupling(name = 'R2GC_289_26',
                       value = '(17*cw**2*complex(0,1)*G**2*g1**2)/(1728.*cmath.pi**2) + (cw*complex(0,1)*G**2*g1*gw*sw)/(288.*cmath.pi**2) + (complex(0,1)*G**2*gw**2*sw**2)/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_290_27 = Coupling(name = 'R2GC_290_27',
                       value = '-(cw*complex(0,1)*G**2*gw)/(12.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_291_28 = Coupling(name = 'R2GC_291_28',
                       value = '-(cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(12.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_292_29 = Coupling(name = 'R2GC_292_29',
                       value = '(cw*complex(0,1)*G**2*gw)/(12.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_293_30 = Coupling(name = 'R2GC_293_30',
                       value = '-(cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(12.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_296_31 = Coupling(name = 'R2GC_296_31',
                       value = '-(complex(0,1)*G**2*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*ymt**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_297_32 = Coupling(name = 'R2GC_297_32',
                       value = '(complex(0,1)*G**2*gw**2)/(96.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_304_33 = Coupling(name = 'R2GC_304_33',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_304_34 = Coupling(name = 'R2GC_304_34',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_305_35 = Coupling(name = 'R2GC_305_35',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_305_36 = Coupling(name = 'R2GC_305_36',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_306_37 = Coupling(name = 'R2GC_306_37',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_306_38 = Coupling(name = 'R2GC_306_38',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_307_39 = Coupling(name = 'R2GC_307_39',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_308_40 = Coupling(name = 'R2GC_308_40',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_308_41 = Coupling(name = 'R2GC_308_41',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_309_42 = Coupling(name = 'R2GC_309_42',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_309_43 = Coupling(name = 'R2GC_309_43',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_310_44 = Coupling(name = 'R2GC_310_44',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_310_45 = Coupling(name = 'R2GC_310_45',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_311_46 = Coupling(name = 'R2GC_311_46',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_312_47 = Coupling(name = 'R2GC_312_47',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_325_48 = Coupling(name = 'R2GC_325_48',
                       value = '-(complex(0,1)*G**2*gw)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_330_49 = Coupling(name = 'R2GC_330_49',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_334_50 = Coupling(name = 'R2GC_334_50',
                       value = '-(G**2*ymb)/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_335_51 = Coupling(name = 'R2GC_335_51',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_335_52 = Coupling(name = 'R2GC_335_52',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_336_53 = Coupling(name = 'R2GC_336_53',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_337_54 = Coupling(name = 'R2GC_337_54',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_337_55 = Coupling(name = 'R2GC_337_55',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_338_56 = Coupling(name = 'R2GC_338_56',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_338_57 = Coupling(name = 'R2GC_338_57',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_339_58 = Coupling(name = 'R2GC_339_58',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_340_59 = Coupling(name = 'R2GC_340_59',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_341_60 = Coupling(name = 'R2GC_341_60',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_342_61 = Coupling(name = 'R2GC_342_61',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_343_62 = Coupling(name = 'R2GC_343_62',
                       value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_348_63 = Coupling(name = 'R2GC_348_63',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_352_64 = Coupling(name = 'R2GC_352_64',
                       value = '(complex(0,1)*G**2*ymb*cmath.sqrt(2))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_353_65 = Coupling(name = 'R2GC_353_65',
                       value = '(G**2*ymt)/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_354_66 = Coupling(name = 'R2GC_354_66',
                       value = '-(complex(0,1)*G**2*ymt*cmath.sqrt(2))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_355_67 = Coupling(name = 'R2GC_355_67',
                       value = '(complex(0,1)*G**2*ymt/cmath.tan(beta)*cmath.sqrt(2))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_356_68 = Coupling(name = 'R2GC_356_68',
                       value = '(complex(0,1)*G**2*TH1x1*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_357_69 = Coupling(name = 'R2GC_357_69',
                       value = '(complex(0,1)*G**2*TH1x1*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_358_70 = Coupling(name = 'R2GC_358_70',
                       value = '(complex(0,1)*G**2*TH1x2*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_359_71 = Coupling(name = 'R2GC_359_71',
                       value = '(complex(0,1)*G**2*TH1x2*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_360_72 = Coupling(name = 'R2GC_360_72',
                       value = '(complex(0,1)*G**2*TH1x3*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_361_73 = Coupling(name = 'R2GC_361_73',
                       value = '(complex(0,1)*G**2*TH1x3*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_376_74 = Coupling(name = 'R2GC_376_74',
                       value = '-(complex(0,1)*G**2*MB*TH1x1*ymb)/(8.*cmath.pi**2*vev) - (complex(0,1)*G**2*MB*TH2x1*ymb*cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_376_75 = Coupling(name = 'R2GC_376_75',
                       value = '-(complex(0,1)*G**2*MT*TH1x1*ymt)/(8.*cmath.pi**2*vev) + (complex(0,1)*G**2*MT*TH2x1*ymt/cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_377_76 = Coupling(name = 'R2GC_377_76',
                       value = '-(complex(0,1)*G**2*MB*TH1x2*ymb)/(8.*cmath.pi**2*vev) - (complex(0,1)*G**2*MB*TH2x2*ymb*cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_377_77 = Coupling(name = 'R2GC_377_77',
                       value = '-(complex(0,1)*G**2*MT*TH1x2*ymt)/(8.*cmath.pi**2*vev) + (complex(0,1)*G**2*MT*TH2x2*ymt/cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_378_78 = Coupling(name = 'R2GC_378_78',
                       value = '-(complex(0,1)*G**2*MB*TH1x3*ymb)/(8.*cmath.pi**2*vev) - (complex(0,1)*G**2*MB*TH2x3*ymb*cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_378_79 = Coupling(name = 'R2GC_378_79',
                       value = '-(complex(0,1)*G**2*MT*TH1x3*ymt)/(8.*cmath.pi**2*vev) + (complex(0,1)*G**2*MT*TH2x3*ymt/cmath.tan(beta))/(8.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YT':1})

R2GC_379_80 = Coupling(name = 'R2GC_379_80',
                       value = '-(complex(0,1)*G**2*TH3x1*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_379_81 = Coupling(name = 'R2GC_379_81',
                       value = '(complex(0,1)*G**2*TH3x1*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_380_82 = Coupling(name = 'R2GC_380_82',
                       value = '-(complex(0,1)*G**2*TH3x2*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_380_83 = Coupling(name = 'R2GC_380_83',
                       value = '(complex(0,1)*G**2*TH3x2*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_381_84 = Coupling(name = 'R2GC_381_84',
                       value = '-(complex(0,1)*G**2*TH3x3*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_381_85 = Coupling(name = 'R2GC_381_85',
                       value = '(complex(0,1)*G**2*TH3x3*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_382_86 = Coupling(name = 'R2GC_382_86',
                       value = '(complex(0,1)*G**2*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_383_87 = Coupling(name = 'R2GC_383_87',
                       value = '(complex(0,1)*G**2*TH1x1*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_384_88 = Coupling(name = 'R2GC_384_88',
                       value = '(complex(0,1)*G**2*TH1x1*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_385_89 = Coupling(name = 'R2GC_385_89',
                       value = '(complex(0,1)*G**2*TH1x2*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_386_90 = Coupling(name = 'R2GC_386_90',
                       value = '(complex(0,1)*G**2*TH1x2*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_387_91 = Coupling(name = 'R2GC_387_91',
                       value = '(complex(0,1)*G**2*TH1x3*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_388_92 = Coupling(name = 'R2GC_388_92',
                       value = '(complex(0,1)*G**2*TH1x3*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_389_93 = Coupling(name = 'R2GC_389_93',
                       value = '(complex(0,1)*G**2*ymb*cmath.sqrt(2)*cmath.tan(beta))/(3.*cmath.pi**2*vev)',
                       order = {'QCD':2,'QED':1,'YB':1})

R2GC_390_94 = Coupling(name = 'R2GC_390_94',
                       value = '-(complex(0,1)*G**2*TH1x1**2*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x1*TH2x1*ymb**2*cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_390_95 = Coupling(name = 'R2GC_390_95',
                       value = '-(complex(0,1)*G**2*TH1x1**2*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x1*TH2x1*ymt**2/cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_391_96 = Coupling(name = 'R2GC_391_96',
                       value = '-(complex(0,1)*G**2*TH1x1*TH1x2*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x2*TH2x1*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x1*TH2x2*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1*TH2x2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1*TH3x2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_391_97 = Coupling(name = 'R2GC_391_97',
                       value = '-(complex(0,1)*G**2*TH1x1*TH1x2*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x2*TH2x1*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x1*TH2x2*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1*TH2x2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1*TH3x2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_392_98 = Coupling(name = 'R2GC_392_98',
                       value = '-(complex(0,1)*G**2*TH1x2**2*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x2*TH2x2*ymb**2*cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x2**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x2**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YB':2})

R2GC_392_99 = Coupling(name = 'R2GC_392_99',
                       value = '-(complex(0,1)*G**2*TH1x2**2*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x2*TH2x2*ymt**2/cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x2**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x2**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                       order = {'QCD':2,'QED':2,'YT':2})

R2GC_393_100 = Coupling(name = 'R2GC_393_100',
                        value = '-(complex(0,1)*G**2*TH1x1*TH1x3*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x3*TH2x1*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x1*TH2x3*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1*TH2x3*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1*TH3x3*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YB':2})

R2GC_393_101 = Coupling(name = 'R2GC_393_101',
                        value = '-(complex(0,1)*G**2*TH1x1*TH1x3*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x3*TH2x1*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x1*TH2x3*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x1*TH2x3*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x1*TH3x3*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YT':2})

R2GC_394_102 = Coupling(name = 'R2GC_394_102',
                        value = '-(complex(0,1)*G**2*TH1x2*TH1x3*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x3*TH2x2*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x2*TH2x3*ymb**2*cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x2*TH2x3*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x2*TH3x3*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YB':2})

R2GC_394_103 = Coupling(name = 'R2GC_394_103',
                        value = '-(complex(0,1)*G**2*TH1x2*TH1x3*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x3*TH2x2*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x2*TH2x3*ymt**2/cmath.tan(beta))/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x2*TH2x3*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x2*TH3x3*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YT':2})

R2GC_395_104 = Coupling(name = 'R2GC_395_104',
                        value = '-(complex(0,1)*G**2*TH1x3**2*ymb**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH1x3*TH2x3*ymb**2*cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x3**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x3**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YB':2})

R2GC_395_105 = Coupling(name = 'R2GC_395_105',
                        value = '-(complex(0,1)*G**2*TH1x3**2*ymt**2)/(8.*cmath.pi**2*vev**2) + (complex(0,1)*G**2*TH1x3*TH2x3*ymt**2/cmath.tan(beta))/(4.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH2x3**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*TH3x3**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YT':2})

R2GC_396_106 = Coupling(name = 'R2GC_396_106',
                        value = '-(complex(0,1)*G**2*ymt**2/cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2) - (complex(0,1)*G**2*ymb**2*cmath.tan(beta)**2)/(8.*cmath.pi**2*vev**2)',
                        order = {'QCD':2,'QED':2,'YB':2})

UVGC_300_1 = Coupling(name = 'UVGC_300_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_304_2 = Coupling(name = 'UVGC_304_2',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_304_3 = Coupling(name = 'UVGC_304_3',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_305_4 = Coupling(name = 'UVGC_305_4',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_305_5 = Coupling(name = 'UVGC_305_5',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_307_6 = Coupling(name = 'UVGC_307_6',
                      value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_307_7 = Coupling(name = 'UVGC_307_7',
                      value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_308_8 = Coupling(name = 'UVGC_308_8',
                      value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_308_9 = Coupling(name = 'UVGC_308_9',
                      value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_309_10 = Coupling(name = 'UVGC_309_10',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_309_11 = Coupling(name = 'UVGC_309_11',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_310_12 = Coupling(name = 'UVGC_310_12',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_310_13 = Coupling(name = 'UVGC_310_13',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_311_14 = Coupling(name = 'UVGC_311_14',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_312_15 = Coupling(name = 'UVGC_312_15',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_313_16 = Coupling(name = 'UVGC_313_16',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_313_17 = Coupling(name = 'UVGC_313_17',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_313_18 = Coupling(name = 'UVGC_313_18',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_313_19 = Coupling(name = 'UVGC_313_19',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_313_20 = Coupling(name = 'UVGC_313_20',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_313_21 = Coupling(name = 'UVGC_313_21',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_325_22 = Coupling(name = 'UVGC_325_22',
                       value = {-1:'(complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_325_23 = Coupling(name = 'UVGC_325_23',
                       value = {-1:'-(complex(0,1)*G**2*gw)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_327_24 = Coupling(name = 'UVGC_327_24',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_328_25 = Coupling(name = 'UVGC_328_25',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_329_26 = Coupling(name = 'UVGC_329_26',
                       value = {-1:'( (cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2) if MB else -(cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2)',0:'( (5*cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) - (cw*complex(0,1)*G**2*g1*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_330_27 = Coupling(name = 'UVGC_330_27',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_331_28 = Coupling(name = 'UVGC_331_28',
                       value = {-1:'( (cw*complex(0,1)*G**2*gw)/(12.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2) if MB else -(cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2)',0:'( (5*cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (5*complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) - (cw*complex(0,1)*G**2*gw*reglog(MB/MU_R))/(4.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_332_29 = Coupling(name = 'UVGC_332_29',
                       value = {-1:'( -(complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2) if MB else (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2) ) - (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2) ) + (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_333_30 = Coupling(name = 'UVGC_333_30',
                       value = {-1:'( -(cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(12.*cmath.pi**2) if MB else (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2)',0:'( (-5*cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) + (5*complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) + (cw*complex(0,1)*G**2*g1*reglog(MB/MU_R))/(12.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_334_31 = Coupling(name = 'UVGC_334_31',
                       value = {-1:'( -(G**2*ymb)/(6.*cmath.pi**2*vev) if MB else (G**2*ymb)/(12.*cmath.pi**2*vev) ) - (G**2*ymb)/(3.*cmath.pi**2*vev)',0:'( (-5*G**2*ymb)/(12.*cmath.pi**2*vev) + (G**2*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) if MB else -(G**2*ymb)/(12.*cmath.pi**2*vev) ) + (G**2*ymb)/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_335_32 = Coupling(name = 'UVGC_335_32',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_335_33 = Coupling(name = 'UVGC_335_33',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_336_34 = Coupling(name = 'UVGC_336_34',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_336_35 = Coupling(name = 'UVGC_336_35',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_336_36 = Coupling(name = 'UVGC_336_36',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_336_37 = Coupling(name = 'UVGC_336_37',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_337_38 = Coupling(name = 'UVGC_337_38',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_337_39 = Coupling(name = 'UVGC_337_39',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_337_40 = Coupling(name = 'UVGC_337_40',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_337_41 = Coupling(name = 'UVGC_337_41',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_337_42 = Coupling(name = 'UVGC_337_42',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_338_43 = Coupling(name = 'UVGC_338_43',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_338_44 = Coupling(name = 'UVGC_338_44',
                       value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_338_45 = Coupling(name = 'UVGC_338_45',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_338_46 = Coupling(name = 'UVGC_338_46',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_339_47 = Coupling(name = 'UVGC_339_47',
                       value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_339_48 = Coupling(name = 'UVGC_339_48',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_340_49 = Coupling(name = 'UVGC_340_49',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_340_50 = Coupling(name = 'UVGC_340_50',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_340_51 = Coupling(name = 'UVGC_340_51',
                       value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_340_52 = Coupling(name = 'UVGC_340_52',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_340_53 = Coupling(name = 'UVGC_340_53',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_341_54 = Coupling(name = 'UVGC_341_54',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_341_55 = Coupling(name = 'UVGC_341_55',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_341_56 = Coupling(name = 'UVGC_341_56',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_341_57 = Coupling(name = 'UVGC_341_57',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_341_58 = Coupling(name = 'UVGC_341_58',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_342_59 = Coupling(name = 'UVGC_342_59',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_342_60 = Coupling(name = 'UVGC_342_60',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_343_61 = Coupling(name = 'UVGC_343_61',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_343_62 = Coupling(name = 'UVGC_343_62',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_343_63 = Coupling(name = 'UVGC_343_63',
                       value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_343_64 = Coupling(name = 'UVGC_343_64',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_343_65 = Coupling(name = 'UVGC_343_65',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_344_66 = Coupling(name = 'UVGC_344_66',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_345_67 = Coupling(name = 'UVGC_345_67',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_346_68 = Coupling(name = 'UVGC_346_68',
                       value = {-1:'( -(cw*complex(0,1)*G**2*g1)/(9.*cmath.pi**2) if MT else (cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2)',0:'( (-5*cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2) + (cw*complex(0,1)*G**2*g1*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*g1)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_347_69 = Coupling(name = 'UVGC_347_69',
                       value = {-1:'( -(complex(0,1)*G**2*gw)/(12.*cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_347_70 = Coupling(name = 'UVGC_347_70',
                       value = {-1:'( -(complex(0,1)*G**2*gw)/(12.*cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) )',0:'( (-5*complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*gw)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_348_71 = Coupling(name = 'UVGC_348_71',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_349_72 = Coupling(name = 'UVGC_349_72',
                       value = {-1:'( -(cw*complex(0,1)*G**2*gw)/(12.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(36.*cmath.pi**2) if MT else (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2)',0:'( (-5*cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (5*complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) + (cw*complex(0,1)*G**2*gw*reglog(MT/MU_R))/(4.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else -(cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) + (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*gw)/(24.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw)/(72.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_350_73 = Coupling(name = 'UVGC_350_73',
                       value = {-1:'( (complex(0,1)*G**2*g1*sw)/(9.*cmath.pi**2) if MT else -(complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2) ) + (complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2) - (complex(0,1)*G**2*g1*sw*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else (complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2) ) - (complex(0,1)*G**2*g1*sw)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_351_74 = Coupling(name = 'UVGC_351_74',
                       value = {-1:'( -(cw*complex(0,1)*G**2*g1)/(36.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(12.*cmath.pi**2) if MT else (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) ) - (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2)',0:'( (-5*cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) - (5*complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) + (cw*complex(0,1)*G**2*g1*reglog(MT/MU_R))/(12.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) - (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2) ) + (cw*complex(0,1)*G**2*g1)/(72.*cmath.pi**2) + (complex(0,1)*G**2*gw*sw)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_352_75 = Coupling(name = 'UVGC_352_75',
                       value = {-1:'( (complex(0,1)*G**2*ymb)/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (5*complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (complex(0,1)*G**2*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_352_76 = Coupling(name = 'UVGC_352_76',
                       value = {-1:'( (complex(0,1)*G**2*ymb)/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (5*complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (complex(0,1)*G**2*ymb*reglog(MT/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymb)/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_352_77 = Coupling(name = 'UVGC_352_77',
                       value = {-1:'(complex(0,1)*G**2*ymb*cmath.sqrt(2))/(3.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_353_78 = Coupling(name = 'UVGC_353_78',
                       value = {-1:'( (G**2*ymt)/(6.*cmath.pi**2*vev) if MT else -(G**2*ymt)/(12.*cmath.pi**2*vev) ) + (G**2*ymt)/(3.*cmath.pi**2*vev)',0:'( (3*G**2*ymt)/(4.*cmath.pi**2*vev) - (G**2*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (G**2*ymt)/(12.*cmath.pi**2*vev) ) - (G**2*ymt)/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_354_79 = Coupling(name = 'UVGC_354_79',
                       value = {-1:'( -(complex(0,1)*G**2*ymt)/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (-5*complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) + (complex(0,1)*G**2*ymt*reglog(MB/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) + (complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_354_80 = Coupling(name = 'UVGC_354_80',
                       value = {-1:'( -(complex(0,1)*G**2*ymt)/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (-13*complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) + (3*complex(0,1)*G**2*ymt*reglog(MT/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) + (complex(0,1)*G**2*ymt)/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_354_81 = Coupling(name = 'UVGC_354_81',
                       value = {-1:'-(complex(0,1)*G**2*ymt*cmath.sqrt(2))/(3.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_355_82 = Coupling(name = 'UVGC_355_82',
                       value = {-1:'( (complex(0,1)*G**2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (5*complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (complex(0,1)*G**2*ymt/cmath.tan(beta)*reglog(MB/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_355_83 = Coupling(name = 'UVGC_355_83',
                       value = {-1:'( (complex(0,1)*G**2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (13*complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (3*complex(0,1)*G**2*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_355_84 = Coupling(name = 'UVGC_355_84',
                       value = {-1:'(complex(0,1)*G**2*ymt/cmath.tan(beta)*cmath.sqrt(2))/(3.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_356_85 = Coupling(name = 'UVGC_356_85',
                       value = {-1:'( (complex(0,1)*G**2*TH1x1*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x1*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x1*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) + (3*G**2*TH3x1*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x1*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_357_86 = Coupling(name = 'UVGC_357_86',
                       value = {-1:'( (complex(0,1)*G**2*TH1x1*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x1*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x1*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (3*G**2*TH3x1*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x1*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x1*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_358_87 = Coupling(name = 'UVGC_358_87',
                       value = {-1:'( (complex(0,1)*G**2*TH1x2*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x2*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x2*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) + (3*G**2*TH3x2*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x2*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_359_88 = Coupling(name = 'UVGC_359_88',
                       value = {-1:'( (complex(0,1)*G**2*TH1x2*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x2*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x2*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (3*G**2*TH3x2*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x2*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x2*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_360_89 = Coupling(name = 'UVGC_360_89',
                       value = {-1:'( (complex(0,1)*G**2*TH1x3*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x3*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x3*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) + (3*G**2*TH3x3*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x3*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_361_90 = Coupling(name = 'UVGC_361_90',
                       value = {-1:'( (complex(0,1)*G**2*TH1x3*ymt)/(6.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(6.*cmath.pi**2*vev) if MT else -(complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x3*ymt)/(3.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (3*complex(0,1)*G**2*TH1x3*ymt)/(4.*cmath.pi**2*vev) - (3*complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (3*G**2*TH3x3*ymt/cmath.tan(beta))/(4.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x3*ymt*reglog(MT/MU_R))/(cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta)*reglog(MT/MU_R))/(cmath.pi**2*vev) if MT else (complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x3*ymt)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymt/cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YT':1})

UVGC_383_91 = Coupling(name = 'UVGC_383_91',
                       value = {-1:'( (complex(0,1)*G**2*TH1x1*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x1*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x1*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (5*G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_384_92 = Coupling(name = 'UVGC_384_92',
                       value = {-1:'( (complex(0,1)*G**2*TH1x1*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x1*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x1*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (5*G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x1*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x1*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_385_93 = Coupling(name = 'UVGC_385_93',
                       value = {-1:'( (complex(0,1)*G**2*TH1x2*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x2*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x2*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (5*G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_386_94 = Coupling(name = 'UVGC_386_94',
                       value = {-1:'( (complex(0,1)*G**2*TH1x2*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x2*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x2*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (5*G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x2*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_387_95 = Coupling(name = 'UVGC_387_95',
                       value = {-1:'( (complex(0,1)*G**2*TH1x3*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x3*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x3*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (5*G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_388_96 = Coupling(name = 'UVGC_388_96',
                       value = {-1:'( (complex(0,1)*G**2*TH1x3*ymb)/(6.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev) if MB else -(complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) + (complex(0,1)*G**2*TH1x3*ymb)/(3.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(3.*cmath.pi**2*vev)',0:'( (5*complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH1x3*ymb*reglog(MB/MU_R))/(2.*cmath.pi**2*vev) + (5*complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (5*G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev) if MB else (complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) + (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) - (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) ) - (complex(0,1)*G**2*TH1x3*ymb)/(12.*cmath.pi**2*vev) - (complex(0,1)*G**2*TH2x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev) + (G**2*TH3x3*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_389_97 = Coupling(name = 'UVGC_389_97',
                       value = {-1:'( (complex(0,1)*G**2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (5*complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (complex(0,1)*G**2*ymb*reglog(MB/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_389_98 = Coupling(name = 'UVGC_389_98',
                       value = {-1:'( (complex(0,1)*G**2*ymb*cmath.tan(beta))/(6.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) )',0:'( (5*complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) - (complex(0,1)*G**2*ymb*reglog(MT/MU_R)*cmath.tan(beta))/(2.*cmath.pi**2*vev*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2)) ) - (complex(0,1)*G**2*ymb*cmath.tan(beta))/(12.*cmath.pi**2*vev*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1,'YB':1})

UVGC_389_99 = Coupling(name = 'UVGC_389_99',
                       value = {-1:'(complex(0,1)*G**2*ymb*cmath.sqrt(2)*cmath.tan(beta))/(3.*cmath.pi**2*vev)'},
                       order = {'QCD':2,'QED':1,'YB':1})

