# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.2.1 for Microsoft Windows (64-bit) (January 27, 2023)
# Date: Wed 24 May 2023 23:28:48


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(cg*cw**2)/5.e11',
                order = {'GP':1,'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(cgm*cw**2)/5.e11',
                order = {'GM':1,'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-G',
                order = {'QCD':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_10 = Coupling(name = 'GC_10',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-2.e-12*(cg*cw*sw)',
                 order = {'GP':1,'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '(cg*cw*sw)/5.e11',
                 order = {'GP':1,'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-2.e-12*(cgm*cw*sw)',
                 order = {'GM':1,'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '(cgm*cw*sw)/5.e11',
                 order = {'GM':1,'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '(cg*cw*sw)/(5.e11*ee)',
                 order = {'GP':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-2.e-12*(cgm*cw*sw)/ee',
                 order = {'GM':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(cg*cw**3*sw)/(5.e11*ee)',
                 order = {'GP':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '-2.e-12*(cgm*cw**3*sw)/ee',
                 order = {'GM':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(cg*sw**2)/5.e11',
                 order = {'GP':1,'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-2.e-12*(cgm*sw**2)',
                 order = {'GM':1,'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-2.e-12*(cg*sw**2)/ee',
                 order = {'GP':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(cgm*sw**2)/(5.e11*ee)',
                 order = {'GM':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-2.e-12*(cg*cw**2*sw**2)/ee',
                 order = {'GP':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(cg*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GP':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '-2.e-12*(cgm*cw**2*sw**2)/ee',
                 order = {'GM':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(cgm*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GM':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-2.e-12*(cg*cw*sw**3)/ee',
                 order = {'GP':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(cg*cw*sw**3)/(5.e11*ee)',
                 order = {'GP':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-2.e-12*(cgm*cw*sw**3)/ee',
                 order = {'GM':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(cgm*cw*sw**3)/(5.e11*ee)',
                 order = {'GM':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-2.e-12*(cg*sw**4)/ee',
                 order = {'GP':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(cgm*sw**4)/(5.e11*ee)',
                 order = {'GM':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-0.5*(cw*ee*complex(0,1))/sw + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-2.e-12*(CBtWL4*ee*vev)',
                 order = {'BTW':1,'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(CBWL4*ee*vev)/1.e12',
                 order = {'BW':1,'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-2.e-12*(CBtWL4*cw*ee*vev)/sw',
                 order = {'BTW':1,'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-1.e-12*(CBWL4*cw*ee*vev)/sw',
                 order = {'BW':1,'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-2.e-12*(CBtWL4*cw**3*ee*vev)/sw',
                 order = {'BTW':1,'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-1.e-12*(CBtWL4*cw*ee*sw*vev)',
                 order = {'BTW':1,'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-1.e-12*(CBtWL4*ee*sw**3*vev)/cw',
                 order = {'BTW':1,'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-1.e-12*(CBtWL4*ee*vev**2)',
                 order = {'BTW':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(CBWL4*ee*vev**2)/2.e12',
                 order = {'BW':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(CBtWL4*ee**2*vev**2)/2.e12',
                 order = {'BTW':1,'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-2.5e-13*(CBWL4*ee**2*vev**2)',
                 order = {'BW':1,'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-5.e-13*(CBtWL4*cw**2*ee**2*vev**2)/sw**2',
                 order = {'BTW':1,'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '(CBWL4*cw**2*ee**2*vev**2)/(4.e12*sw**2)',
                 order = {'BW':1,'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-1.e-12*(CBtWL4*cw*ee*vev**2)/sw',
                 order = {'BTW':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-5.e-13*(CBWL4*cw*ee*vev**2)/sw',
                 order = {'BW':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-1.e-12*(CBtWL4*cw**3*ee*vev**2)/sw',
                 order = {'BTW':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-5.e-13*(CBtWL4*cw*ee**2*vev**2)/sw',
                 order = {'BTW':1,'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(CBtWL4*cw*ee**2*vev**2)/(1.e12*sw)',
                 order = {'BTW':1,'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-5.e-13*(CBWL4*cw*ee**2*vev**2)/sw',
                 order = {'BW':1,'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-2.5e-13*(CBWL4*cw*ee**2*vev**2)/sw',
                 order = {'BW':1,'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-5.e-13*(CBtWL4*cw*ee*sw*vev**2)',
                 order = {'BTW':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(CBtWL4*ee**2*sw*vev**2)/(2.e12*cw)',
                 order = {'BTW':1,'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-2.5e-13*(CBWL4*ee**2*sw*vev**2)/cw',
                 order = {'BW':1,'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-5.e-13*(CBtWL4*ee*sw**3*vev**2)/cw',
                 order = {'BTW':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(CBBL4*cw**3*ee*vev)/(5.e11*sw) + (CBBL4*cw*ee*sw*vev)/5.e11',
                 order = {'BB':1,'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cw*CWWL4*ee*vev)/(2.e12*sw) + (CWWL4*ee*sw*vev)/(2.e12*cw)',
                 order = {'QED':1,'WW':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '(3*cw**3*CWWL4*ee*vev)/(2.e12*sw) + (3*cw*CWWL4*ee*sw*vev)/2.e12',
                 order = {'QED':1,'WW':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-4.e-12*(CBBL4*cw**2*ee*vev) - (CBBL4*ee*sw**2*vev)/2.5e11',
                 order = {'BB':1,'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '-2.e-12*(CBtWL4*cw**2*ee*vev) - (CBtWL4*ee*sw**2*vev)/5.e11',
                 order = {'BTW':1,'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(CBtWL4*cw**2*ee*vev)/1.e12 + (CBtWL4*ee*sw**2*vev)/1.e12',
                 order = {'BTW':1,'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-1.e-12*(CBWL4*cw**2*ee*vev) - (CBWL4*ee*sw**2*vev)/1.e12',
                 order = {'BW':1,'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(3*CBWL4*cw**2*ee*vev)/1.e12 + (3*CBWL4*ee*sw**2*vev)/1.e12',
                 order = {'BW':1,'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(cw**2*CWWL4*ee*vev)/1.e12 + (CWWL4*ee*sw**2*vev)/1.e12',
                 order = {'QED':1,'WW':1})

GC_87 = Coupling(name = 'GC_87',
                 value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '(3*CBBL4*cw*ee*sw*vev)/5.e11 + (3*CBBL4*ee*sw**3*vev)/(5.e11*cw)',
                 order = {'BB':1,'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-1.e-12*(CBWL4*cw**3*ee*vev)/sw + (CBWL4*ee*sw**3*vev)/(1.e12*cw)',
                 order = {'BW':1,'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(cw*CWWL4*ee*sw*vev)/2.e12 + (CWWL4*ee*sw**3*vev)/(2.e12*cw)',
                 order = {'QED':1,'WW':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-2.5e-13*(CWWL4*ee**2*vev**2) - (cw**2*CWWL4*ee**2*vev**2)/(4.e12*sw**2)',
                 order = {'QED':1,'WW':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '(CBBL4*cw**3*ee*vev**2)/(1.e12*sw) + (CBBL4*cw*ee*sw*vev**2)/1.e12',
                 order = {'BB':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(cw*CWWL4*ee*vev**2)/(4.e12*sw) + (CWWL4*ee*sw*vev**2)/(4.e12*cw)',
                 order = {'WW':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(3*cw**3*CWWL4*ee*vev**2)/(4.e12*sw) + (3*cw*CWWL4*ee*sw*vev**2)/4.e12',
                 order = {'WW':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-2.5e-13*(cw*CWWL4*ee**2*vev**2)/sw - (CWWL4*ee**2*sw*vev**2)/(4.e12*cw)',
                 order = {'QED':1,'WW':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-2.e-12*(CBBL4*cw**2*ee*vev**2) - (CBBL4*ee*sw**2*vev**2)/5.e11',
                 order = {'BB':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-1.e-12*(CBtWL4*cw**2*ee*vev**2) - (CBtWL4*ee*sw**2*vev**2)/1.e12',
                 order = {'BTW':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(CBtWL4*cw**2*ee*vev**2)/2.e12 + (CBtWL4*ee*sw**2*vev**2)/2.e12',
                 order = {'BTW':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-5.e-13*(CBWL4*cw**2*ee*vev**2) - (CBWL4*ee*sw**2*vev**2)/2.e12',
                 order = {'BW':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(3*CBWL4*cw**2*ee*vev**2)/2.e12 + (3*CBWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'BW':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(cw**2*CWWL4*ee*vev**2)/2.e12 + (CWWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'WW':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(3*CBBL4*cw*ee*sw*vev**2)/1.e12 + (3*CBBL4*ee*sw**3*vev**2)/(1.e12*cw)',
                  order = {'BB':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '-5.e-13*(CBWL4*cw**3*ee*vev**2)/sw + (CBWL4*ee*sw**3*vev**2)/(2.e12*cw)',
                  order = {'BW':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(cw*CWWL4*ee*sw*vev**2)/4.e12 + (CWWL4*ee*sw**3*vev**2)/(4.e12*cw)',
                  order = {'WW':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

