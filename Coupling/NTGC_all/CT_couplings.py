# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.2.1 for Microsoft Windows (64-bit) (January 27, 2023)
# Date: Wed 24 May 2023 23:28:48


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



