Requestor: Sebastian Mergelmeyer
Content: NLO version of the top flavor-changing neutral currents model (TFCNC_UFO / topFCNC_UFO)
Link: https://feynrules.irmp.ucl.ac.be/wiki/TopFCNC
JIRA: https://its.cern.ch/jira/browse/AGENE-1164