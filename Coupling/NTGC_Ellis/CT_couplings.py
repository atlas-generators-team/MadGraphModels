# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (July 15, 2020)
# Date: Thu 30 Jun 2022 14:46:11


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



