# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Fri 15 Dec 2023 19:17:17


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(cg*cw**2)/5.e11',
                order = {'GP':1,'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(cgm*cw**2)/5.e11',
                order = {'GM':1,'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(CGMtL4*cw**2)/5.e11',
                order = {'GMT':1,'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = '(CGPtL4*cw**2)/5.e11',
                order = {'GPT':1,'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '-(cg*cw*sw)/5.e11',
                 order = {'GP':1,'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '(cg*cw*sw)/5.e11',
                 order = {'GP':1,'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-(cgm*cw*sw)/5.e11',
                 order = {'GM':1,'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(cgm*cw*sw)/5.e11',
                 order = {'GM':1,'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-(CGMtL4*cw*sw)/5.e11',
                 order = {'GMT':1,'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(CGMtL4*cw*sw)/5.e11',
                 order = {'GMT':1,'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-(CGPtL4*cw*sw)/5.e11',
                 order = {'GPT':1,'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(cg*cw*sw)/(5.e11*ee)',
                 order = {'GP':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(cgm*cw*sw)/(5.e11*ee)',
                 order = {'GM':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(CGMtL4*cw*sw)/(5.e11*ee)',
                 order = {'GMT':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(CGPtL4*cw*sw)/(1.e12*ee)',
                 order = {'GPT':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(cg*cw**3*sw)/(5.e11*ee)',
                 order = {'GP':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(cgm*cw**3*sw)/(5.e11*ee)',
                 order = {'GM':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(CGMtL4*cw**3*sw)/(5.e11*ee)',
                 order = {'GMT':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '(cg*sw**2)/5.e11',
                 order = {'GP':1,'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(cgm*sw**2)/5.e11',
                 order = {'GM':1,'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(CGMtL4*sw**2)/5.e11',
                 order = {'GMT':1,'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-(CGPtL4*sw**2)/5.e11',
                 order = {'GPT':1,'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-(cg*sw**2)/(5.e11*ee)',
                 order = {'GP':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(cgm*sw**2)/(5.e11*ee)',
                 order = {'GM':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(CGMtL4*sw**2)/(1.e12*ee)',
                 order = {'GMT':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(CGPtL4*sw**2)/(1.e12*ee)',
                 order = {'GPT':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(cg*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GP':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cg*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GP':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(cgm*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GM':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(cgm*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GM':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-(CGMtL4*cw**2*sw**2)/(1.e12*ee)',
                 order = {'GMT':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(CGMtL4*cw**2*sw**2)/(5.e11*ee)',
                 order = {'GMT':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(cg*cw*sw**3)/(5.e11*ee)',
                 order = {'GP':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(cg*cw*sw**3)/(5.e11*ee)',
                 order = {'GP':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-(cgm*cw*sw**3)/(5.e11*ee)',
                 order = {'GM':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(cgm*cw*sw**3)/(5.e11*ee)',
                 order = {'GM':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-(CGMtL4*cw*sw**3)/(5.e11*ee)',
                 order = {'GMT':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(CGMtL4*cw*sw**3)/(5.e11*ee)',
                 order = {'GMT':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-(cg*sw**4)/(5.e11*ee)',
                 order = {'GP':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(cgm*sw**4)/(5.e11*ee)',
                 order = {'GM':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(CGMtL4*sw**4)/(1.e12*ee)',
                 order = {'GMT':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(CGPtL4*cw**3*sw)/(1.e12*ee) - (CGPtL4*cw*sw**3)/(1.e12*ee)',
                 order = {'GPT':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(CGPtL4*cw**2*sw**2)/(1.e12*ee) + (CGPtL4*sw**4)/(1.e12*ee)',
                 order = {'GPT':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(CBtWL4*ee*vev)/5.e11',
                 order = {'BTW':1,'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(CBWL4*ee*vev)/1.e12',
                 order = {'BW':1,'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(CBtWL4*cw*ee*vev)/(5.e11*sw)',
                 order = {'BTW':1,'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(CBWL4*cw*ee*vev)/(1.e12*sw)',
                 order = {'BW':1,'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-(CBtWL4*cw**3*ee*vev)/(5.e11*sw)',
                 order = {'BTW':1,'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-(CBtWL4*cw*ee*sw*vev)/1.e12',
                 order = {'BTW':1,'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-(CBtWL4*ee*sw**3*vev)/(1.e12*cw)',
                 order = {'BTW':1,'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(CBtWL4*ee*vev**2)/1.e12',
                 order = {'BTW':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(CBWL4*ee*vev**2)/2.e12',
                 order = {'BW':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(CBtWL4*ee**2*vev**2)/2.e12',
                 order = {'BTW':1,'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(CBWL4*ee**2*vev**2)/4.e12',
                 order = {'BW':1,'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-(CBtWL4*cw**2*ee**2*vev**2)/(2.e12*sw**2)',
                 order = {'BTW':1,'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(CBWL4*cw**2*ee**2*vev**2)/(4.e12*sw**2)',
                 order = {'BW':1,'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '-(CBtWL4*cw*ee*vev**2)/(1.e12*sw)',
                 order = {'BTW':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '-(CBWL4*cw*ee*vev**2)/(2.e12*sw)',
                 order = {'BW':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-(CBtWL4*cw**3*ee*vev**2)/(1.e12*sw)',
                 order = {'BTW':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(CBtWL4*cw*ee**2*vev**2)/(2.e12*sw)',
                 order = {'BTW':1,'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(CBtWL4*cw*ee**2*vev**2)/(1.e12*sw)',
                 order = {'BTW':1,'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-(CBWL4*cw*ee**2*vev**2)/(2.e12*sw)',
                 order = {'BW':1,'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(CBWL4*cw*ee**2*vev**2)/(4.e12*sw)',
                 order = {'BW':1,'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-(CBtWL4*cw*ee*sw*vev**2)/2.e12',
                 order = {'BTW':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(CBtWL4*ee**2*sw*vev**2)/(2.e12*cw)',
                 order = {'BTW':1,'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-(CBWL4*ee**2*sw*vev**2)/(4.e12*cw)',
                 order = {'BW':1,'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(CBtWL4*ee*sw**3*vev**2)/(2.e12*cw)',
                 order = {'BTW':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(CtBWL4*cw*ee*vev)/(5.e11*sw) + (CtBWL4*ee*sw*vev)/(5.e11*cw)',
                 order = {'QED':1,'TBW':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(CBBL4*cw**3*ee*vev)/(5.e11*sw) + (CBBL4*cw*ee*sw*vev)/5.e11',
                 order = {'BB':1,'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(cw*CWWL4*ee*vev)/(2.e12*sw) + (CWWL4*ee*sw*vev)/(2.e12*cw)',
                 order = {'QED':1,'WW':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(3*cw**3*CWWL4*ee*vev)/(2.e12*sw) + (3*cw*CWWL4*ee*sw*vev)/2.e12',
                  order = {'QED':1,'WW':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(CBBL4*cw**2*ee*vev)/2.5e11 - (CBBL4*ee*sw**2*vev)/2.5e11',
                  order = {'BB':1,'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '-(CBtWL4*cw**2*ee*vev)/5.e11 - (CBtWL4*ee*sw**2*vev)/5.e11',
                  order = {'BTW':1,'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(CBtWL4*cw**2*ee*vev)/1.e12 + (CBtWL4*ee*sw**2*vev)/1.e12',
                  order = {'BTW':1,'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(CBWL4*cw**2*ee*vev)/1.e12 - (CBWL4*ee*sw**2*vev)/1.e12',
                  order = {'BW':1,'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(3*CBWL4*cw**2*ee*vev)/1.e12 + (3*CBWL4*ee*sw**2*vev)/1.e12',
                  order = {'BW':1,'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(cw**2*CWWL4*ee*vev)/1.e12 + (CWWL4*ee*sw**2*vev)/1.e12',
                  order = {'QED':1,'WW':1})

GC_107 = Coupling(name = 'GC_107',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(3*CBBL4*cw*ee*sw*vev)/5.e11 + (3*CBBL4*ee*sw**3*vev)/(5.e11*cw)',
                  order = {'BB':1,'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(CBWL4*cw**3*ee*vev)/(1.e12*sw) + (CBWL4*ee*sw**3*vev)/(1.e12*cw)',
                  order = {'BW':1,'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(CtBWL4*cw**3*ee*vev)/(5.e11*sw) + (CtBWL4*cw*ee*sw*vev)/2.5e11 + (CtBWL4*ee*sw**3*vev)/(5.e11*cw)',
                  order = {'QED':1,'TBW':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(cw*CWWL4*ee*sw*vev)/2.e12 + (CWWL4*ee*sw**3*vev)/(2.e12*cw)',
                  order = {'QED':1,'WW':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '(CtBWL4*ee**2*vev**2)/1.e12 + (CtBWL4*cw**2*ee**2*vev**2)/(1.e12*sw**2)',
                  order = {'QED':1,'TBW':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(CWWL4*ee**2*vev**2)/4.e12 - (cw**2*CWWL4*ee**2*vev**2)/(4.e12*sw**2)',
                  order = {'QED':1,'WW':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(CtBWL4*cw*ee*vev**2)/(1.e12*sw) + (CtBWL4*ee*sw*vev**2)/(1.e12*cw)',
                  order = {'TBW':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(CBBL4*cw**3*ee*vev**2)/(1.e12*sw) + (CBBL4*cw*ee*sw*vev**2)/1.e12',
                  order = {'BB':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '(cw*CWWL4*ee*vev**2)/(4.e12*sw) + (CWWL4*ee*sw*vev**2)/(4.e12*cw)',
                  order = {'WW':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(3*cw**3*CWWL4*ee*vev**2)/(4.e12*sw) + (3*cw*CWWL4*ee*sw*vev**2)/4.e12',
                  order = {'WW':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(CtBWL4*cw*ee**2*vev**2)/(1.e12*sw) - (CtBWL4*ee**2*sw*vev**2)/(1.e12*cw)',
                  order = {'QED':1,'TBW':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(cw*CWWL4*ee**2*vev**2)/(4.e12*sw) - (CWWL4*ee**2*sw*vev**2)/(4.e12*cw)',
                  order = {'QED':1,'WW':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(CBBL4*cw**2*ee*vev**2)/5.e11 - (CBBL4*ee*sw**2*vev**2)/5.e11',
                  order = {'BB':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(CBtWL4*cw**2*ee*vev**2)/1.e12 - (CBtWL4*ee*sw**2*vev**2)/1.e12',
                  order = {'BTW':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(CBtWL4*cw**2*ee*vev**2)/2.e12 + (CBtWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'BTW':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(CBWL4*cw**2*ee*vev**2)/2.e12 - (CBWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'BW':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(3*CBWL4*cw**2*ee*vev**2)/2.e12 + (3*CBWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'BW':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(cw**2*CWWL4*ee*vev**2)/2.e12 + (CWWL4*ee*sw**2*vev**2)/2.e12',
                  order = {'WW':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(3*CBBL4*cw*ee*sw*vev**2)/1.e12 + (3*CBBL4*ee*sw**3*vev**2)/(1.e12*cw)',
                  order = {'BB':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(CBWL4*cw**3*ee*vev**2)/(2.e12*sw) + (CBWL4*ee*sw**3*vev**2)/(2.e12*cw)',
                  order = {'BW':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(CtBWL4*cw**3*ee*vev**2)/(1.e12*sw) + (CtBWL4*cw*ee*sw*vev**2)/5.e11 + (CtBWL4*ee*sw**3*vev**2)/(1.e12*cw)',
                  order = {'TBW':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '(cw*CWWL4*ee*sw*vev**2)/4.e12 + (CWWL4*ee*sw**3*vev**2)/(4.e12*cw)',
                  order = {'WW':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

