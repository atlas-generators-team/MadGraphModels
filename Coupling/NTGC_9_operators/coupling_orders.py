# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Fri 15 Dec 2023 19:17:17


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

GP = CouplingOrder(name = 'GP',
                   expansion_order = 99,
                   hierarchy = 1)

GM = CouplingOrder(name = 'GM',
                   expansion_order = 99,
                   hierarchy = 1)

BTW = CouplingOrder(name = 'BTW',
                    expansion_order = 99,
                    hierarchy = 1)

BW = CouplingOrder(name = 'BW',
                   expansion_order = 99,
                   hierarchy = 1)

BB = CouplingOrder(name = 'BB',
                   expansion_order = 99,
                   hierarchy = 1)

WW = CouplingOrder(name = 'WW',
                   expansion_order = 99,
                   hierarchy = 1)

TBW = CouplingOrder(name = 'TBW',
                    expansion_order = 99,
                    hierarchy = 1)

GPT = CouplingOrder(name = 'GPT',
                    expansion_order = 99,
                    hierarchy = 1)

GMT = CouplingOrder(name = 'GMT',
                    expansion_order = 99,
                    hierarchy = 1)

