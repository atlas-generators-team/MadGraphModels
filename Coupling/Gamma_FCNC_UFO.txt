Requestor: Dominic Hirschbuehl
Contents: Photon-induced single-top production via FCNC
Paper: https://arxiv.org/abs/1609.04838
JIRA: https://its.cern.ch/jira/browse/AGENE-1243