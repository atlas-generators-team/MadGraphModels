# MadGraphModels Model Repository

This project contains all the external models that can be used in production with the ATLAS MadGraph5\_aMC@NLO / MadGraphControl setup.  

Further documentation for extra models is [here](https://gitlab.cern.ch/atlas/athena/tree/main/Generators/MadGraphControl/#extra-models), along with further documentation for the use of MadGraph in Athena.

## Adding a new model

Please open a merge request for any new models.  Note that mdoels should all come with a short description in a text file, including the source of model (website, private communication, etc), a brief description, and links to the relevant websites or papers describing the model.  This documentation is also available in the `model\_list.txt` text file in this project.

Some common issues that come up when updating a model are described below, along with ways to fix them.

### Updating models

There is only one deployment of the models on cvmfs. That means if a model is updated, it is updated instantly for all releases. If production was run with old job options and an older version of the model, this means that the old runs will no longer be reproducible. For that reason, we generally ask if production was run with the old version of the model, and if it was we suggest either including a new version of the model separately (as a separate model), or using a restrict card to enforce behavior in the new or old model for consistency.

### Python3 compatibility

Because Athena main (release 22+) now uses Python3, we must ensure that all models are Python3 compatible. To check if your model is Python3 compatible, try this:

<code>lsetup "asetup Athena,here,latest,main"
export PYTHONPATH=/path/to/my/model:$PYTHONPATH
$MADPATH/bin/mg5_aMC
</code>

Then, at the MadGraph command prompt, simply type `import model`. If that doesn't work, then you might be able to covert your model by typing:

<code>convert model /path/to/my/model
</code>

Be sure to check the differences that were applied to make sure that none of them were particularly strange; you might want to generate a few events to cross-check that things are working as intended.

One quick way to check this by hand is to look in the `write_param_card.py` file for your model; if it uses old-style print statements, then the model needs to be updated. If it uses new-style `print()` statements, the model is probably ok.

### Models via private communication

If any models have been received via private communication, please encourage the authors to post them on a website; this could be their personal website or [FeynRules](http://feynrules.irmp.ucl.ac.be). This helps greatly with bookkeeping and communication with other communities that might be interested in the details of the model (CMS or pheno colleagues).

### Particle IDs

New particles in each model are given unique particle identification codes in the `particles.py` file. The rules for these codes are provided by the [PDG Group](http://pdg.lbl.gov/2024/reviews/rpp2024-rev-monte-carlo-numbering.pdf), which is why they are usually referred to as PDG IDs. Often model authors are not attentive to these rules, and codes are simply made up or guessed for new particles. For ghost particles (which do not appear in output files), this is not a significant problem. In case the particles do appear in LHE files, this can cause unfortunate issues downstream. The most common issues are:

- TestHepMC [checks for invalid PDG IDs](https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/EvgenProdTools/src/TestHepMC.cxx#L546). This is really a test to see if the code _disobeys_ the rules for a PDG ID, rather than whether it is a sensible PDG ID for the particle. For example, one can use the PDG ID for a pion (211) to refer to a dark matter particle. The PDG ID is valid, so this test will pass. But you will have other issues (see below). In case you have already generated a large number of LHE samples with invalid PDG IDs, you can disable the test by setting `testSeq.TestHepMC.UknownPDGIDTest = False` in your job options, or provide a file to `testSeq.TestHepMC.UnknownPDGIDFile` containing a list of PDG IDs that should be treated as allowed (one PDG ID per line, negative codes for anti-particles must be included separately). Better is to change the PDG IDs to comply with the rules, of course.
- TestHepMC also [checks for stable particles not known to Geant4](https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/EvgenProdTools/src/TestHepMC.cxx#L650). If a stable particle uses an unknown PDG ID, and if that PDG maps to something that is not known to be stable and neutral (like a SUSY neutralino), this test may fail. This is a problem in some dark matter and hidden valley models where the light stable states use unusual PDG IDs. In that case, again, the model can be fixed to use a well-defined PDG ID for the particle. If absolutely necessary, the test can be disabled in TestHepMC with `testSeq.TestHepMC.EnergyG4Test = False`.
- The BSM particle collection in truth derivations, PHYSLITE, and some other derivations [uses the isBSM function](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/DerivationFramework/DerivationFrameworkMCTruth/python/TruthDerivationToolsConfig.py#L105) to decide what particles to include. This function is [implemented here](https://gitlab.cern.ch/atlas/athena/-/blob/main/Event/xAOD/xAODTruth/Root/TruthParticle_v1.cxx#L312) to pass through to the [corresponding function in TruthUtils](https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/TruthUtils/TruthUtils/AtlasPID.h#L416). Again, if a PDG ID is non-standard or does not satisfy the normal rules for identification as a BSM particle, the new particles might be ommitted from this collection.
- [MCTruthClassifier](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCTruthClassifier) and the newer [MonteCarloTruthClassifier](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MonteCarloTruthClassifier) use particle parentage to decide whether leptons, photons, or other particles are to be considered "prompt" (e.g. coming from a W, Z, or H boson) or "non-prompt" (e.g. coming from a hadron). A common issue is the use of a non-standard PDG ID that can be interpreted as a hadron PDG ID. In these cases, leptons from BSM particles may be identified as "non-prompt" because of the BSM particle with the hadron PDG ID. In case the decorations from MCTruthClassifier are used later in the analysis (e.g. for lepton truth-level classification), they might provide incorrect results.
- It is in principle possible that use of the wrong PDG ID can lead to incorrect treatment in Pythia8 or Geant4, in case the PDG ID used overlaps with a well-known Standard Model particle in particular, or if it overlaps with a SUSY particle with fairly standard properties (e.g. if a neutralino PDG ID is used for a charged particle). We have not seen an issue from this "in the wild" yet.

One way to test for these sorts of issues is to run a few hundred events through generation and the not [make a TRUTH1 derivation](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TruthDAOD#TRUTH1). TRUTH1 has the advantage that the entire particle and vertex content of the event record is included, along with the various light-weight collections that you will find in TRUTH3 or other small derivation formats. Within the light-weight lepton collections, decorations from MCTruthClassifier and MonteCarloTruthClassifier are saved. With these derivations, the above issues can all be checked.

## Deployment of new models

This repository is replicated to <code>/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest</code> every few hours. After the replication following your merge request, any code you have requested will be available for you to run worldwide in any release. There is no need to wait for a new production cache before submitting your request.

## Testing new models

To test locally before requesting installation, put your model into a directory other than the one in which you are running the job, and do:

<code>export PYTHONPATH=/path/to/my/model:$PYTHONPATH
</code>

The job should then be able to pick up your model. Note that the repository has now been structured so that models are organized. You can put your preferred model into one of the subdirectories here, and the deployment should automatically pick it up. To test, add your model into a clone of this repository and then:

<code>source setup.sh</code>

to set up the relevant links and modify your PYTHONPATH appropriately. Note: because the developers are lazy, you do need to source or execute that script from the main directory of the repository, not from elsewhere, in order for it to work its magic properly.

## Depreciation of models

There is a DEPRECATED folder that contains models that are superseded and potentially buggy. These might be useful for comparisons but should not be used otherwise. To use the models, the folder as to be added to the PYTHONPATH manually:

<code>export PYTHONPATH=$PYTHONPATH:/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/DEPRECATED
</code>

## Historical models

This package previously was held in svn [here](https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MadGraphModels/trunk).  The old change log containing previous commits is [here](https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MadGraphModels/trunk/ChangeLog) (warning: links are now dead)