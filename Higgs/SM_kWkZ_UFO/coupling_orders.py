# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.0.0 for Mac OS X ARM (64-bit) (December 3, 2021)
# Date: Wed 16 Mar 2022 16:53:33


from object_library import all_orders, CouplingOrder


CHWW = CouplingOrder(name = 'CHWW',
                     expansion_order = 99,
                     hierarchy = 2)

CHZZ = CouplingOrder(name = 'CHZZ',
                     expansion_order = 99,
                     hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

