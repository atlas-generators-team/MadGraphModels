Requestor: John Keller
Content: Standard Model with modifiable Higgs couplings to W and Z bosons in the kappa framework.
Paper: https://arxiv.org/abs/2006.09374
Authors: Yongcheng Wu, ycwu0830@gmail.com
