# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 14.1.0 for Linux x86 (64-bit) (July 16, 2024)
# Date: Mon 11 Nov 2024 10:56:38


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '(-3*complex(0,1)*lamp)/2.',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '(calpha*complex(0,1)*lamp*salpha)/2. - calpha*complex(0,1)*lampd*salpha',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-(calpha**2*complex(0,1)*lampd) - (complex(0,1)*lamp*salpha**2)/2.',
                 order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '-0.5*(calpha**2*complex(0,1)*lamp) - complex(0,1)*lampd*salpha**2',
                 order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '(-3*calpha**3*complex(0,1)*lamd*salpha)/2. + 3*calpha**3*complex(0,1)*lampd*salpha + (3*calpha*complex(0,1)*lamp*salpha**3)/2. - 3*calpha*complex(0,1)*lampd*salpha**3',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(3*calpha**3*complex(0,1)*lamp*salpha)/2. - 3*calpha**3*complex(0,1)*lampd*salpha - (3*calpha*complex(0,1)*lamd*salpha**3)/2. + 3*calpha*complex(0,1)*lampd*salpha**3',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '(-3*calpha**4*complex(0,1)*lamp)/2. - 6*calpha**2*complex(0,1)*lampd*salpha**2 - (3*complex(0,1)*lamd*salpha**4)/2.',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '(-3*calpha**4*complex(0,1)*lamd)/2. - 6*calpha**2*complex(0,1)*lampd*salpha**2 - (3*complex(0,1)*lamp*salpha**4)/2.',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(calpha**4*complex(0,1)*lampd) - (3*calpha**2*complex(0,1)*lamd*salpha**2)/2. - (3*calpha**2*complex(0,1)*lamp*salpha**2)/2. + 4*calpha**2*complex(0,1)*lampd*salpha**2 - complex(0,1)*lampd*salpha**4',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-0.5*(ee**2*sbeta)/cw',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '(ee**2*sbeta)/(2.*cw)',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '(2*calpha**2*ee**2*complex(0,1))/sw**2 + (ee**2*complex(0,1)*salpha**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '(calpha**2*ee**2*complex(0,1))/(2.*sw**2) + (2*ee**2*complex(0,1)*salpha**2)/sw**2',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '(3*calpha*ee**2*complex(0,1)*salpha)/(2.*sw**2)',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '(-2*ee**2*complex(0,1)*sbeta**2)/sw**2',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-0.5*(ee*sbeta)/sw',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee*sbeta)/(2.*sw)',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-0.5*(ee**2*sbeta)/sw',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '(ee**2*sbeta)/(2.*sw)',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-0.5*(cw*ee*complex(0,1))/sw + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-0.5*(calpha*cw*ee)/sw - (calpha*ee*sw)/(2.*cw)',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-0.5*(cw*ee*salpha)/sw - (ee*salpha*sw)/(2.*cw)',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = 'calpha**2*ee**2*complex(0,1) + (calpha**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (calpha**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(calpha*ee**2*complex(0,1)*salpha) - (calpha*cw**2*ee**2*complex(0,1)*salpha)/(2.*sw**2) - (calpha*ee**2*complex(0,1)*salpha*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = 'ee**2*complex(0,1)*salpha**2 + (cw**2*ee**2*complex(0,1)*salpha**2)/(2.*sw**2) + (ee**2*complex(0,1)*salpha**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '(complex(0,1)*muA*salpha)/2. - complex(0,1)*lampd*salpha*vevd - (calpha*complex(0,1)*lamp*vevp)/2.',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(calpha*complex(0,1)*muA)/2. - calpha*complex(0,1)*lampd*vevd + (complex(0,1)*lamp*salpha*vevp)/2.',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-(calpha**2*complex(0,1)*muA*salpha) + (complex(0,1)*muA*salpha**3)/2. - (3*calpha**2*complex(0,1)*lamd*salpha*vevd)/2. + 2*calpha**2*complex(0,1)*lampd*salpha*vevd - complex(0,1)*lampd*salpha**3*vevd - calpha**3*complex(0,1)*lampd*vevp - (3*calpha*complex(0,1)*lamp*salpha**2*vevp)/2. + 2*calpha*complex(0,1)*lampd*salpha**2*vevp',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(3*calpha**2*complex(0,1)*muA*salpha)/2. - 3*calpha**2*complex(0,1)*lampd*salpha*vevd - (3*complex(0,1)*lamd*salpha**3*vevd)/2. - (3*calpha**3*complex(0,1)*lamp*vevp)/2. - 3*calpha*complex(0,1)*lampd*salpha**2*vevp',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(3*calpha*complex(0,1)*muA*salpha**2)/2. - (3*calpha**3*complex(0,1)*lamd*vevd)/2. - 3*calpha*complex(0,1)*lampd*salpha**2*vevd + 3*calpha**2*complex(0,1)*lampd*salpha*vevp + (3*complex(0,1)*lamp*salpha**3*vevp)/2.',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(calpha**3*complex(0,1)*muA)/2. - calpha*complex(0,1)*muA*salpha**2 - calpha**3*complex(0,1)*lampd*vevd - (3*calpha*complex(0,1)*lamd*salpha**2*vevd)/2. + 2*calpha*complex(0,1)*lampd*salpha**2*vevd + (3*calpha**2*complex(0,1)*lamp*salpha*vevp)/2. - 2*calpha**2*complex(0,1)*lampd*salpha*vevp + complex(0,1)*lampd*salpha**3*vevp',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(2*ee**2*complex(0,1)*salpha*vevd)/sw**2 + (calpha*ee**2*complex(0,1)*vevp)/(2.*sw**2)',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(2*calpha*ee**2*complex(0,1)*vevd)/sw**2 - (ee**2*complex(0,1)*salpha*vevp)/(2.*sw**2)',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = 'calpha*ee**2*complex(0,1)*vevp + (calpha*cw**2*ee**2*complex(0,1)*vevp)/(2.*sw**2) + (calpha*ee**2*complex(0,1)*sw**2*vevp)/(2.*cw**2)',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-(ee**2*complex(0,1)*salpha*vevp) - (cw**2*ee**2*complex(0,1)*salpha*vevp)/(2.*sw**2) - (ee**2*complex(0,1)*salpha*sw**2*vevp)/(2.*cw**2)',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '(-2*ee**2*complex(0,1)*vevp**2)/(sw**2*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '-0.5*(ee**2*vevp)/(cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '(ee**2*vevp)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ee**2*complex(0,1)*sbeta*vevp)/(2.*sw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '(-2*ee**2*complex(0,1)*sbeta*vevp)/(sw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '(ee*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-0.5*(ee**2*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '(ee**2*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '(-16*complex(0,1)*lamd*sbeta**4*vevd**4)/(4*vevd**2 + vevp**2)**2 - (16*complex(0,1)*lampd*sbeta**2*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (8*complex(0,1)*lamd*sbeta**4*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamp*vevp**4)/(4*vevd**2 + vevp**2)**2 - (4*complex(0,1)*lampd*sbeta**2*vevp**4)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamd*sbeta**4*vevp**4)/(4*vevd**2 + vevp**2)**2',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '(-16*complex(0,1)*lamp*sbeta**4*vevd**4)/(4*vevd**2 + vevp**2)**2 - (16*complex(0,1)*lampd*sbeta**2*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (8*complex(0,1)*lamp*sbeta**4*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamd*vevp**4)/(4*vevd**2 + vevp**2)**2 - (4*complex(0,1)*lampd*sbeta**2*vevp**4)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamp*sbeta**4*vevp**4)/(4*vevd**2 + vevp**2)**2',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '(-16*complex(0,1)*lampd*sbeta**4*vevd**4)/(4*vevd**2 + vevp**2)**2 - (4*complex(0,1)*lamd*sbeta**2*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (4*complex(0,1)*lamp*sbeta**2*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 + (8*complex(0,1)*lampd*sbeta**2*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (8*complex(0,1)*lampd*sbeta**4*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lampd*vevp**4)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamd*sbeta**2*vevp**4)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lamp*sbeta**2*vevp**4)/(4*vevd**2 + vevp**2)**2 + (2*complex(0,1)*lampd*sbeta**2*vevp**4)/(4*vevd**2 + vevp**2)**2 - (complex(0,1)*lampd*sbeta**4*vevp**4)/(4*vevd**2 + vevp**2)**2',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '(4*complex(0,1)*lamp*sbeta**3*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (8*complex(0,1)*lampd*sbeta**3*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (complex(0,1)*lamd*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 + (2*complex(0,1)*lampd*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 + (complex(0,1)*lamp*sbeta**3*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*lampd*sbeta**3*vevp**3)/(4*vevd**2 + vevp**2)**1.5',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '(-4*complex(0,1)*lamd*sbeta**3*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 + (8*complex(0,1)*lampd*sbeta**3*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 + (complex(0,1)*lamp*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*lampd*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (complex(0,1)*lamd*sbeta**3*vevp**3)/(4*vevd**2 + vevp**2)**1.5 + (2*complex(0,1)*lampd*sbeta**3*vevp**3)/(4*vevd**2 + vevp**2)**1.5',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(-4*ee*complex(0,1)*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (ee*complex(0,1)*vevp**2)/(4*vevd**2 + vevp**2) - (ee*complex(0,1)*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '(8*ee**2*complex(0,1)*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (2*ee**2*complex(0,1)*vevp**2)/(4*vevd**2 + vevp**2) + (2*ee**2*complex(0,1)*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '(-2*complex(0,1)*lamp*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lampd*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamp*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '(-4*complex(0,1)*lampd*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamp*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '-((complex(0,1)*lamd*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)) - (complex(0,1)*lamp*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2) + (4*complex(0,1)*lampd*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '(-2*muA*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (muA*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (muA*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(2*muA*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (muA*vevp**2)/(2.*(4*vevd**2 + vevp**2)) + (muA*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(2*complex(0,1)*muA*salpha*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*lamd*salpha*sbeta*vevd**3*vevp)/(4*vevd**2 + vevp**2)**1.5 + (4*complex(0,1)*lampd*salpha*sbeta*vevd**3*vevp)/(4*vevd**2 + vevp**2)**1.5 + (2*calpha*complex(0,1)*lamp*sbeta*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**1.5 - (4*calpha*complex(0,1)*lampd*sbeta*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**1.5 + (complex(0,1)*muA*salpha*sbeta*vevp**3)/(2.*(4*vevd**2 + vevp**2)**1.5) - (complex(0,1)*lamd*salpha*sbeta*vevd*vevp**3)/(2.*(4*vevd**2 + vevp**2)**1.5) + (complex(0,1)*lampd*salpha*sbeta*vevd*vevp**3)/(4*vevd**2 + vevp**2)**1.5 + (calpha*complex(0,1)*lamp*sbeta*vevp**4)/(2.*(4*vevd**2 + vevp**2)**1.5) - (calpha*complex(0,1)*lampd*sbeta*vevp**4)/(4*vevd**2 + vevp**2)**1.5 + (2*calpha*complex(0,1)*muA*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*muA*vevp**2)/(2.*(4*vevd**2 + vevp**2)) + (calpha*complex(0,1)*muA*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(2*calpha*complex(0,1)*lamp*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (4*calpha*complex(0,1)*lampd*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lamd*salpha*vevp**2)/(2.*(4*vevd**2 + vevp**2)) + (calpha*complex(0,1)*lampd*salpha*vevp**2)/(4*vevd**2 + vevp**2) + (calpha*complex(0,1)*lamp*salpha*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lampd*salpha*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '(-2*calpha*complex(0,1)*lamd*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (4*calpha*complex(0,1)*lampd*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (calpha*complex(0,1)*lamp*salpha*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lampd*salpha*vevp**2)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lamd*salpha*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) + (calpha*complex(0,1)*lampd*salpha*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '(2*calpha*complex(0,1)*muA*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (2*calpha*complex(0,1)*lamd*sbeta*vevd**3*vevp)/(4*vevd**2 + vevp**2)**1.5 + (4*calpha*complex(0,1)*lampd*sbeta*vevd**3*vevp)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*lamp*salpha*sbeta*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**1.5 + (4*complex(0,1)*lampd*salpha*sbeta*vevd**2*vevp**2)/(4*vevd**2 + vevp**2)**1.5 + (calpha*complex(0,1)*muA*sbeta*vevp**3)/(2.*(4*vevd**2 + vevp**2)**1.5) - (calpha*complex(0,1)*lamd*sbeta*vevd*vevp**3)/(2.*(4*vevd**2 + vevp**2)**1.5) + (calpha*complex(0,1)*lampd*sbeta*vevd*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (complex(0,1)*lamp*salpha*sbeta*vevp**4)/(2.*(4*vevd**2 + vevp**2)**1.5) + (complex(0,1)*lampd*salpha*sbeta*vevp**4)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*muA*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (complex(0,1)*muA*salpha*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*muA*salpha*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(-4*calpha**2*complex(0,1)*lampd*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (2*complex(0,1)*lamd*salpha**2*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lamp*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha**2*vevp**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lampd*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamd*salpha**2*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '(-4*calpha**2*complex(0,1)*lampd*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (2*complex(0,1)*lamp*salpha**2*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lamd*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha**2*vevp**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lampd*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamp*salpha**2*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '(-2*calpha**2*complex(0,1)*lamd*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (4*complex(0,1)*lampd*salpha**2*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lampd*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamp*salpha**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha**2*complex(0,1)*lamd*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha**2*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '(-2*calpha**2*complex(0,1)*lamp*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (4*complex(0,1)*lampd*salpha**2*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (calpha**2*complex(0,1)*lampd*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamd*salpha**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha**2*complex(0,1)*lamp*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha**2*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2)',
                 order = {'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '(2*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*vevp**2)/(sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*sbeta**2*vevp**2)/(2.*sw**2*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_95 = Coupling(name = 'GC_95',
                 value = '(4*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*vevp**2)/(2.*sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*sbeta**2*vevp**2)/(sw**2*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_96 = Coupling(name = 'GC_96',
                 value = '(-4*cw*ee*complex(0,1)*sbeta**2*vevd**2)/(sw*(4*vevd**2 + vevp**2)) - (cw*ee*complex(0,1)*vevp**2)/(2.*sw*(4*vevd**2 + vevp**2)) - (cw*ee*complex(0,1)*sbeta**2*vevp**2)/(sw*(4*vevd**2 + vevp**2)) + (ee*complex(0,1)*sw*vevp**2)/(2.*cw*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(8*cw*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw*(4*vevd**2 + vevp**2)) + (cw*ee**2*complex(0,1)*vevp**2)/(sw*(4*vevd**2 + vevp**2)) + (2*cw*ee**2*complex(0,1)*sbeta**2*vevp**2)/(sw*(4*vevd**2 + vevp**2)) - (ee**2*complex(0,1)*sw*vevp**2)/(cw*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = '(-2*cw*ee*complex(0,1)*sbeta**2*vevd**2)/(sw*(4*vevd**2 + vevp**2)) + (2*ee*complex(0,1)*sbeta**2*sw*vevd**2)/(cw*(4*vevd**2 + vevp**2)) - (cw*ee*complex(0,1)*vevp**2)/(sw*(4*vevd**2 + vevp**2)) - (cw*ee*complex(0,1)*sbeta**2*vevp**2)/(2.*sw*(4*vevd**2 + vevp**2)) + (ee*complex(0,1)*sbeta**2*sw*vevp**2)/(2.*cw*(4*vevd**2 + vevp**2))',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(4*cw*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw*(4*vevd**2 + vevp**2)) - (4*ee**2*complex(0,1)*sbeta**2*sw*vevd**2)/(cw*(4*vevd**2 + vevp**2)) + (2*cw*ee**2*complex(0,1)*vevp**2)/(sw*(4*vevd**2 + vevp**2)) + (cw*ee**2*complex(0,1)*sbeta**2*vevp**2)/(sw*(4*vevd**2 + vevp**2)) - (ee**2*complex(0,1)*sbeta**2*sw*vevp**2)/(cw*(4*vevd**2 + vevp**2))',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(8*cw**2*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw**2*(4*vevd**2 + vevp**2)) - (ee**2*complex(0,1)*vevp**2)/(4*vevd**2 + vevp**2) + (cw**2*ee**2*complex(0,1)*vevp**2)/(2.*sw**2*(4*vevd**2 + vevp**2)) + (2*cw**2*ee**2*complex(0,1)*sbeta**2*vevp**2)/(sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*sw**2*vevp**2)/(2.*cw**2*(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '(-4*ee**2*complex(0,1)*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) + (2*cw**2*ee**2*complex(0,1)*sbeta**2*vevd**2)/(sw**2*(4*vevd**2 + vevp**2)) + (2*ee**2*complex(0,1)*sbeta**2*sw**2*vevd**2)/(cw**2*(4*vevd**2 + vevp**2)) - (ee**2*complex(0,1)*sbeta**2*vevp**2)/(4*vevd**2 + vevp**2) + (2*cw**2*ee**2*complex(0,1)*vevp**2)/(sw**2*(4*vevd**2 + vevp**2)) + (cw**2*ee**2*complex(0,1)*sbeta**2*vevp**2)/(2.*sw**2*(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*sbeta**2*sw**2*vevp**2)/(2.*cw**2*(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '(4*calpha*complex(0,1)*muA*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 + (calpha*complex(0,1)*muA*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*muA*salpha*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (4*complex(0,1)*lampd*salpha*sbeta**2*vevd**3)/(4*vevd**2 + vevp**2) - (2*calpha*complex(0,1)*lamp*sbeta**2*vevd**2*vevp)/(4*vevd**2 + vevp**2) - (complex(0,1)*muA*salpha*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lamd*salpha*vevd*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha*sbeta**2*vevd*vevp**2)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lampd*vevp**3)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lamp*sbeta**2*vevp**3)/(2.*(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(-4*calpha*complex(0,1)*muA*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (calpha*complex(0,1)*muA*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*complex(0,1)*lamd*salpha*sbeta**2*vevd**3)/(4*vevd**2 + vevp**2) - (4*calpha*complex(0,1)*lampd*sbeta**2*vevd**2*vevp)/(4*vevd**2 + vevp**2) - (complex(0,1)*muA*salpha*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha*vevd*vevp**2)/(4*vevd**2 + vevp**2) - (complex(0,1)*lamd*salpha*sbeta**2*vevd*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lamp*vevp**3)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lampd*sbeta**2*vevp**3)/(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(-4*complex(0,1)*muA*salpha*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 - (complex(0,1)*muA*salpha*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*calpha*complex(0,1)*muA*sbeta**2*vevd**2)/(4*vevd**2 + vevp**2) - (4*calpha*complex(0,1)*lampd*sbeta**2*vevd**3)/(4*vevd**2 + vevp**2) + (2*complex(0,1)*lamp*salpha*sbeta**2*vevd**2*vevp)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*muA*sbeta**2*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lamd*vevd*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lampd*sbeta**2*vevd*vevp**2)/(4*vevd**2 + vevp**2) + (complex(0,1)*lampd*salpha*vevp**3)/(4*vevd**2 + vevp**2) + (complex(0,1)*lamp*salpha*sbeta**2*vevp**3)/(2.*(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(4*complex(0,1)*muA*salpha*sbeta*vevd**2*vevp)/(4*vevd**2 + vevp**2)**1.5 + (complex(0,1)*muA*salpha*sbeta*vevp**3)/(4*vevd**2 + vevp**2)**1.5 - (2*calpha*complex(0,1)*lamd*sbeta**2*vevd**3)/(4*vevd**2 + vevp**2) + (4*complex(0,1)*lampd*salpha*sbeta**2*vevd**2*vevp)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*muA*vevp**2)/(2.*(4*vevd**2 + vevp**2)) - (calpha*complex(0,1)*lampd*vevd*vevp**2)/(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lamd*sbeta**2*vevd*vevp**2)/(2.*(4*vevd**2 + vevp**2)) + (complex(0,1)*lamp*salpha*vevp**3)/(2.*(4*vevd**2 + vevp**2)) + (complex(0,1)*lampd*salpha*sbeta**2*vevp**3)/(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '-((cw*ee**2*complex(0,1)*salpha*sbeta)/sw**2) - (calpha*ee**2*complex(0,1)*vevp)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '-((calpha*cw*ee**2*complex(0,1)*sbeta)/sw**2) + (ee**2*complex(0,1)*salpha*vevp)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '(complex(0,1)*lamp*sbeta*vevp)/(2.*cmath.sqrt(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '-0.5*(calpha*complex(0,1)*lamd*salpha*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2) - (calpha*complex(0,1)*lamp*salpha*sbeta*vevp)/(2.*cmath.sqrt(4*vevd**2 + vevp**2)) + (2*calpha*complex(0,1)*lampd*salpha*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '-0.5*(calpha**2*complex(0,1)*lamd*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2) + (calpha**2*complex(0,1)*lampd*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2) + (complex(0,1)*lamp*salpha**2*sbeta*vevp)/(2.*cmath.sqrt(4*vevd**2 + vevp**2)) - (complex(0,1)*lampd*salpha**2*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(calpha**2*complex(0,1)*lamp*sbeta*vevp)/(2.*cmath.sqrt(4*vevd**2 + vevp**2)) - (calpha**2*complex(0,1)*lampd*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2) - (complex(0,1)*lamd*salpha**2*sbeta*vevp)/(2.*cmath.sqrt(4*vevd**2 + vevp**2)) + (complex(0,1)*lampd*salpha**2*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-0.5*(ee**2*complex(0,1)*salpha*sbeta)/cw - (calpha*cw*ee**2*complex(0,1)*vevp)/(sw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(calpha*ee**2*complex(0,1)*sbeta)/(2.*cw) - (cw*ee**2*complex(0,1)*salpha*vevp)/(sw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '(ee*complex(0,1)*salpha*sbeta)/sw - (calpha*ee*complex(0,1)*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '-((ee*complex(0,1)*salpha*sbeta)/sw) + (calpha*ee*complex(0,1)*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '(ee*complex(0,1)*salpha*sbeta)/(2.*sw) - (calpha*ee*complex(0,1)*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-0.5*(ee*complex(0,1)*salpha*sbeta)/sw + (calpha*ee*complex(0,1)*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-((ee**2*complex(0,1)*salpha*sbeta)/sw) + (calpha*ee**2*complex(0,1)*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '(ee**2*complex(0,1)*salpha*sbeta)/(2.*sw) - (calpha*ee**2*complex(0,1)*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-((calpha*ee*complex(0,1)*sbeta)/sw) - (ee*complex(0,1)*salpha*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '(calpha*ee*complex(0,1)*sbeta)/sw + (ee*complex(0,1)*salpha*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '-0.5*(calpha*ee*complex(0,1)*sbeta)/sw - (ee*complex(0,1)*salpha*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(calpha*ee*complex(0,1)*sbeta)/(2.*sw) + (ee*complex(0,1)*salpha*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-((calpha*ee**2*complex(0,1)*sbeta)/sw) - (ee**2*complex(0,1)*salpha*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '-0.5*(calpha*ee**2*complex(0,1)*sbeta)/sw - (ee**2*complex(0,1)*salpha*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '-0.5*(cw*ee*complex(0,1)*sbeta*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2)) - (ee*complex(0,1)*sbeta*sw*vevp)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(cw*ee*complex(0,1)*sbeta*vevp)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2)) + (ee*complex(0,1)*sbeta*sw*vevp)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(cw*ee**2*complex(0,1)*sbeta*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2)) + (ee**2*complex(0,1)*sbeta*sw*vevp)/(cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '(ee**2*complex(0,1)*sbeta*vevp)/cmath.sqrt(4*vevd**2 + vevp**2) + (3*cw**2*ee**2*complex(0,1)*sbeta*vevp)/(2.*sw**2*cmath.sqrt(4*vevd**2 + vevp**2)) - (ee**2*complex(0,1)*sbeta*sw**2*vevp)/(2.*cw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(ee**2*complex(0,1)*sbeta*vevp)/(2.*cw) - (cw*ee**2*complex(0,1)*vevd*vevp)/(sw**2*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-0.5*(ee**2*complex(0,1)*sbeta*vevp)/sw - (ee**2*complex(0,1)*vevd*vevp)/(sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((cw*ee**2*complex(0,1)*sbeta*vevd)/sw**2) - (ee**2*complex(0,1)*vevp**2)/(2.*cw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-((ee**2*complex(0,1)*sbeta*vevd)/sw) + (ee**2*complex(0,1)*vevp**2)/(2.*sw*cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-(yb/cmath.sqrt(2))',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = 'yb/cmath.sqrt(2)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-((calpha*complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '(complex(0,1)*salpha*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = 'CKM1x3*complex(0,1)*sbeta*yb',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = 'CKM2x3*complex(0,1)*sbeta*yb',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = 'CKM3x3*complex(0,1)*sbeta*yb',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-((CKM1x3*complex(0,1)*vevp*yb)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-((CKM2x3*complex(0,1)*vevp*yb)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-((CKM3x3*complex(0,1)*vevp*yb)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(yc/cmath.sqrt(2))',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = 'yc/cmath.sqrt(2)',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-((calpha*complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(complex(0,1)*salpha*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(CKM2x1*complex(0,1)*sbeta*yc)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(CKM2x2*complex(0,1)*sbeta*yc)',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(CKM2x3*complex(0,1)*sbeta*yc)',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(CKM2x1*complex(0,1)*vevp*yc)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '(CKM2x2*complex(0,1)*vevp*yc)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(CKM2x3*complex(0,1)*vevp*yc)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(ydo/cmath.sqrt(2))',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = 'ydo/cmath.sqrt(2)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-((calpha*complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '(complex(0,1)*salpha*ydo)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = 'CKM1x1*complex(0,1)*sbeta*ydo',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = 'CKM2x1*complex(0,1)*sbeta*ydo',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = 'CKM3x1*complex(0,1)*sbeta*ydo',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '-((CKM1x1*complex(0,1)*vevp*ydo)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-((CKM2x1*complex(0,1)*vevp*ydo)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '-((CKM3x1*complex(0,1)*vevp*ydo)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-(ye/cmath.sqrt(2))',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = 'ye/cmath.sqrt(2)',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '-((calpha*complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '(complex(0,1)*salpha*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = 'complex(0,1)*sbeta*ye',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '-((complex(0,1)*vevp*ye)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(ym/cmath.sqrt(2))',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = 'ym/cmath.sqrt(2)',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((calpha*complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '(complex(0,1)*salpha*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = 'complex(0,1)*sbeta*ym',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((complex(0,1)*vevp*ym)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-(ys/cmath.sqrt(2))',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = 'ys/cmath.sqrt(2)',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '-((calpha*complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '(complex(0,1)*salpha*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = 'CKM1x2*complex(0,1)*sbeta*ys',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = 'CKM2x2*complex(0,1)*sbeta*ys',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = 'CKM3x2*complex(0,1)*sbeta*ys',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((CKM1x2*complex(0,1)*vevp*ys)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-((CKM2x2*complex(0,1)*vevp*ys)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-((CKM3x2*complex(0,1)*vevp*ys)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(yt/cmath.sqrt(2))',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-((calpha*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '(complex(0,1)*salpha*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-(CKM3x1*complex(0,1)*sbeta*yt)',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-(CKM3x2*complex(0,1)*sbeta*yt)',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-(CKM3x3*complex(0,1)*sbeta*yt)',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(CKM3x1*complex(0,1)*vevp*yt)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '(CKM3x2*complex(0,1)*vevp*yt)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '(CKM3x3*complex(0,1)*vevp*yt)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = 'ytau/cmath.sqrt(2)',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-((calpha*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(complex(0,1)*salpha*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = 'complex(0,1)*sbeta*ytau',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-((complex(0,1)*vevp*ytau)/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(yup/cmath.sqrt(2))',
                  order = {'QED':1})

GC_203 = Coupling(name = 'GC_203',
                  value = 'yup/cmath.sqrt(2)',
                  order = {'QED':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-((calpha*complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '(complex(0,1)*salpha*yup)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(CKM1x1*complex(0,1)*sbeta*yup)',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(CKM1x2*complex(0,1)*sbeta*yup)',
                  order = {'QED':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '-(CKM1x3*complex(0,1)*sbeta*yup)',
                  order = {'QED':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '(CKM1x1*complex(0,1)*vevp*yup)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '(CKM1x2*complex(0,1)*vevp*yup)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '(CKM1x3*complex(0,1)*vevp*yup)/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = 'complex(0,1)*sbeta*ydo*complexconjugate(CKM1x1)',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-((complex(0,1)*vevp*ydo*complexconjugate(CKM1x1))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-(complex(0,1)*sbeta*yup*complexconjugate(CKM1x1))',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(complex(0,1)*vevp*yup*complexconjugate(CKM1x1))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = 'complex(0,1)*sbeta*ys*complexconjugate(CKM1x2)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '-((complex(0,1)*vevp*ys*complexconjugate(CKM1x2))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '-(complex(0,1)*sbeta*yup*complexconjugate(CKM1x2))',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(complex(0,1)*vevp*yup*complexconjugate(CKM1x2))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = 'complex(0,1)*sbeta*yb*complexconjugate(CKM1x3)',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '-((complex(0,1)*vevp*yb*complexconjugate(CKM1x3))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '-(complex(0,1)*sbeta*yup*complexconjugate(CKM1x3))',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '(complex(0,1)*vevp*yup*complexconjugate(CKM1x3))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '-(complex(0,1)*sbeta*yc*complexconjugate(CKM2x1))',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '(complex(0,1)*vevp*yc*complexconjugate(CKM2x1))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = 'complex(0,1)*sbeta*ydo*complexconjugate(CKM2x1)',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-((complex(0,1)*vevp*ydo*complexconjugate(CKM2x1))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '-(complex(0,1)*sbeta*yc*complexconjugate(CKM2x2))',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '(complex(0,1)*vevp*yc*complexconjugate(CKM2x2))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = 'complex(0,1)*sbeta*ys*complexconjugate(CKM2x2)',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '-((complex(0,1)*vevp*ys*complexconjugate(CKM2x2))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = 'complex(0,1)*sbeta*yb*complexconjugate(CKM2x3)',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-((complex(0,1)*vevp*yb*complexconjugate(CKM2x3))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(complex(0,1)*sbeta*yc*complexconjugate(CKM2x3))',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '(complex(0,1)*vevp*yc*complexconjugate(CKM2x3))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = 'complex(0,1)*sbeta*ydo*complexconjugate(CKM3x1)',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '-((complex(0,1)*vevp*ydo*complexconjugate(CKM3x1))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-(complex(0,1)*sbeta*yt*complexconjugate(CKM3x1))',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(complex(0,1)*vevp*yt*complexconjugate(CKM3x1))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = 'complex(0,1)*sbeta*ys*complexconjugate(CKM3x2)',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '-((complex(0,1)*vevp*ys*complexconjugate(CKM3x2))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-(complex(0,1)*sbeta*yt*complexconjugate(CKM3x2))',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '(complex(0,1)*vevp*yt*complexconjugate(CKM3x2))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = 'complex(0,1)*sbeta*yb*complexconjugate(CKM3x3)',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-((complex(0,1)*vevp*yb*complexconjugate(CKM3x3))/cmath.sqrt(4*vevd**2 + vevp**2))',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '-(complex(0,1)*sbeta*yt*complexconjugate(CKM3x3))',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(complex(0,1)*vevp*yt*complexconjugate(CKM3x3))/cmath.sqrt(4*vevd**2 + vevp**2)',
                  order = {'QED':1})

