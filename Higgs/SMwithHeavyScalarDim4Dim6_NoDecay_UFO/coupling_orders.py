# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 17, 2016)
# Date: Fri 4 Jan 2019 10:28:54


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

EFF4 = CouplingOrder(name = 'EFF4',
                     expansion_order = 99,
                     hierarchy = 2)

EFF6 = CouplingOrder(name = 'EFF6',
                     expansion_order = 99,
                     hierarchy = 2)

