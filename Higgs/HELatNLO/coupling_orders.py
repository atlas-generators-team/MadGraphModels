# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Tue 23 Aug 2016 20:14:27


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NP = CouplingOrder(name = 'NP',
                   expansion_order = 1,
                   hierarchy = 2)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 1,
                    hierarchy = 4)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 1,
                    hierarchy = 6)

