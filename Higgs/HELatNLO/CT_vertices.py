# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Tue 23 Aug 2016 20:14:28


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.VVS11, L.VVS12, L.VVS13, L.VVS14, L.VVS15, L.VVS2, L.VVS4, L.VVS7, L.VVS8, L.VVS9 ],
               loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.t] ] ],
               couplings = {(0,5,1):C.R2GC_691_73,(0,0,1):C.R2GC_692_74,(0,6,1):C.R2GC_649_41,(0,7,1):C.R2GC_649_41,(0,8,1):C.R2GC_597_1,(0,2,1):C.R2GC_648_40,(0,1,1):C.R2GC_651_42,(0,3,1):C.R2GC_652_43,(0,4,1):C.R2GC_649_41,(0,9,0):C.R2GC_611_6,(0,9,2):C.R2GC_611_7})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVS1, L.VVVS10, L.VVVS11, L.VVVS2, L.VVVS5, L.VVVS7 ],
               loop_particles = [ [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_693_75,(0,3,0):C.R2GC_694_76,(0,4,0):C.R2GC_694_76,(0,5,0):C.R2GC_693_75,(0,1,0):C.R2GC_693_75,(0,2,0):C.R2GC_694_76})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV11, L.VVV12, L.VVV4, L.VVV5, L.VVV7, L.VVV9 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,2,0):C.R2GC_685_61,(0,2,1):C.R2GC_685_62,(0,3,0):C.R2GC_686_63,(0,3,1):C.R2GC_686_64,(0,4,0):C.R2GC_686_63,(0,4,1):C.R2GC_686_64,(0,5,0):C.R2GC_685_61,(0,5,1):C.R2GC_685_62,(0,0,0):C.R2GC_685_61,(0,0,1):C.R2GC_685_62,(0,1,0):C.R2GC_686_63,(0,1,1):C.R2GC_686_64})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_655_48,(2,0,1):C.R2GC_655_49,(0,0,0):C.R2GC_655_48,(0,0,1):C.R2GC_655_49,(4,0,0):C.R2GC_653_44,(4,0,1):C.R2GC_653_45,(3,0,0):C.R2GC_653_44,(3,0,1):C.R2GC_653_45,(8,0,0):C.R2GC_654_46,(8,0,1):C.R2GC_654_47,(7,0,0):C.R2GC_689_69,(7,0,1):C.R2GC_689_70,(6,0,0):C.R2GC_690_71,(6,0,1):C.R2GC_690_72,(5,0,0):C.R2GC_653_44,(5,0,1):C.R2GC_653_45,(1,0,0):C.R2GC_653_44,(1,0,1):C.R2GC_653_45,(11,0,0):C.R2GC_657_51,(11,0,1):C.R2GC_657_52,(10,0,0):C.R2GC_657_51,(10,0,1):C.R2GC_657_52,(9,0,1):C.R2GC_656_50,(2,1,0):C.R2GC_655_48,(2,1,1):C.R2GC_655_49,(0,1,0):C.R2GC_655_48,(0,1,1):C.R2GC_655_49,(4,1,0):C.R2GC_653_44,(4,1,1):C.R2GC_653_45,(3,1,0):C.R2GC_653_44,(3,1,1):C.R2GC_653_45,(8,1,0):C.R2GC_689_69,(8,1,1):C.R2GC_689_70,(6,1,0):C.R2GC_688_67,(6,1,1):C.R2GC_688_68,(5,1,0):C.R2GC_653_44,(5,1,1):C.R2GC_653_45,(1,1,0):C.R2GC_653_44,(1,1,1):C.R2GC_653_45,(7,1,0):C.R2GC_654_46,(7,1,1):C.R2GC_654_47,(11,1,0):C.R2GC_657_51,(11,1,1):C.R2GC_657_52,(10,1,0):C.R2GC_657_51,(10,1,1):C.R2GC_657_52,(9,1,1):C.R2GC_656_50,(2,2,0):C.R2GC_655_48,(2,2,1):C.R2GC_655_49,(0,2,0):C.R2GC_655_48,(0,2,1):C.R2GC_655_49,(4,2,0):C.R2GC_653_44,(4,2,1):C.R2GC_653_45,(3,2,0):C.R2GC_653_44,(3,2,1):C.R2GC_653_45,(8,2,0):C.R2GC_687_65,(8,2,1):C.R2GC_687_66,(7,2,0):C.R2GC_687_65,(7,2,1):C.R2GC_687_66,(5,2,0):C.R2GC_653_44,(5,2,1):C.R2GC_653_45,(1,2,0):C.R2GC_653_44,(1,2,1):C.R2GC_653_45,(11,2,0):C.R2GC_657_51,(11,2,1):C.R2GC_657_52,(10,2,0):C.R2GC_657_51,(10,2,1):C.R2GC_657_52,(9,2,1):C.R2GC_656_50})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS4, L.FFS6 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_704_79,(0,1,0):C.R2GC_705_80})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_682_57})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4, L.FFS5, L.FFS6, L.FFS7 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,1,0):C.R2GC_624_28,(0,3,0):C.R2GC_624_28,(0,2,0):C.R2GC_599_3,(0,4,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS4, L.FFS6 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_706_81,(0,1,0):C.R2GC_703_78})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_707_82})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5, L.FFS6, L.FFS7 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_638_38,(0,3,0):C.R2GC_638_38,(0,2,0):C.R2GC_599_3,(0,4,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_625_29,(0,1,0):C.R2GC_626_30})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_625_29,(0,1,0):C.R2GC_626_30})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_625_29,(0,1,0):C.R2GC_626_30})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_627_31,(0,1,0):C.R2GC_628_32})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_627_31,(0,1,0):C.R2GC_628_32})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_627_31,(0,1,0):C.R2GC_628_32})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_629_33,(0,1,0):C.R2GC_630_34})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_629_33,(0,1,0):C.R2GC_630_34})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_629_33,(0,1,0):C.R2GC_630_34})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_631_35,(0,1,0):C.R2GC_632_36})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_631_35,(0,1,0):C.R2GC_632_36})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_631_35,(0,1,0):C.R2GC_632_36})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_658_53})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_672_55})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_659_54})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_659_54})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_697_77,(0,2,0):C.R2GC_697_77,(0,1,0):C.R2GC_659_54,(0,3,0):C.R2GC_659_54})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_659_54})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_659_54})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_676_56,(0,2,0):C.R2GC_676_56,(0,1,0):C.R2GC_659_54,(0,3,0):C.R2GC_659_54})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.R2GC_684_60,(0,1,0):C.R2GC_610_4,(0,1,3):C.R2GC_610_5,(0,2,1):C.R2GC_683_58,(0,2,2):C.R2GC_683_59})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.a, P.g, P.g ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVV2, L.VVV3 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_613_10,(0,0,1):C.R2GC_613_11,(0,1,0):C.R2GC_613_11,(0,1,1):C.R2GC_613_10})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.g, P.g, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1, L.VVV2 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_617_17,(0,0,1):C.R2GC_617_16,(0,1,0):C.R2GC_617_16,(0,1,1):C.R2GC_617_17})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS5, L.FFS7 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_599_3,(0,2,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS5, L.FFS7 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_599_3,(0,2,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS5, L.FFS7 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_599_3,(0,2,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS5, L.FFS7 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,1,0):C.R2GC_599_3,(0,2,0):C.R2GC_599_3,(0,0,0):C.R2GC_598_2})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_621_22,(0,0,1):C.R2GC_621_23,(0,1,0):C.R2GC_621_22,(0,1,1):C.R2GC_621_23,(0,2,0):C.R2GC_621_22,(0,2,1):C.R2GC_621_23})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_622_24,(0,0,1):C.R2GC_622_25,(0,1,0):C.R2GC_622_24,(0,1,1):C.R2GC_622_25,(0,2,0):C.R2GC_622_24,(0,2,1):C.R2GC_622_25})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_623_26,(0,0,1):C.R2GC_623_27,(0,1,0):C.R2GC_623_26,(0,1,1):C.R2GC_623_27,(0,2,0):C.R2GC_623_26,(0,2,1):C.R2GC_623_27})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_639_39,(0,1,0):C.R2GC_639_39,(0,2,0):C.R2GC_639_39})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)', 'f(2,3,4)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_616_14,(1,0,1):C.R2GC_616_15,(0,1,0):C.R2GC_615_12,(0,1,1):C.R2GC_615_13,(0,2,0):C.R2GC_615_12,(0,2,1):C.R2GC_615_13,(0,3,0):C.R2GC_615_12,(0,3,1):C.R2GC_615_13})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_620_20,(1,0,1):C.R2GC_620_21,(0,1,0):C.R2GC_619_18,(0,1,1):C.R2GC_619_19,(0,2,0):C.R2GC_619_18,(0,2,1):C.R2GC_619_19,(0,3,0):C.R2GC_619_18,(0,3,1):C.R2GC_619_19})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS14 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_612_8,(0,0,1):C.R2GC_612_9})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS14 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_612_8,(0,0,1):C.R2GC_612_9})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS14 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_637_37})

V_57 = CTVertex(name = 'V_57',
                type = 'UV',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS11, L.VVS12, L.VVS13, L.VVS14, L.VVS15, L.VVS2, L.VVS4, L.VVS7 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,5,0):C.UVGC_691_63,(0,5,1):C.UVGC_691_64,(0,5,2):C.UVGC_691_65,(0,5,3):C.UVGC_691_66,(0,5,4):C.UVGC_691_67,(0,0,0):C.UVGC_692_68,(0,0,1):C.UVGC_692_69,(0,0,2):C.UVGC_692_70,(0,0,3):C.UVGC_692_71,(0,0,4):C.UVGC_692_72,(0,6,2):C.UVGC_650_4,(0,7,2):C.UVGC_650_4,(0,2,2):C.UVGC_648_2,(0,1,2):C.UVGC_651_5,(0,3,2):C.UVGC_652_6,(0,4,2):C.UVGC_649_3})

V_58 = CTVertex(name = 'V_58',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.H ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVVS1, L.VVVS10, L.VVVS11, L.VVVS2, L.VVVS5, L.VVVS7 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_693_73,(0,0,1):C.UVGC_693_74,(0,0,2):C.UVGC_693_75,(0,0,3):C.UVGC_693_76,(0,0,4):C.UVGC_693_77,(0,3,0):C.UVGC_694_78,(0,3,1):C.UVGC_694_79,(0,3,2):C.UVGC_694_80,(0,3,3):C.UVGC_694_81,(0,3,4):C.UVGC_694_82,(0,4,0):C.UVGC_694_78,(0,4,1):C.UVGC_694_79,(0,4,2):C.UVGC_694_80,(0,4,3):C.UVGC_694_81,(0,4,4):C.UVGC_694_82,(0,5,0):C.UVGC_693_73,(0,5,1):C.UVGC_693_74,(0,5,2):C.UVGC_693_75,(0,5,3):C.UVGC_693_76,(0,5,4):C.UVGC_693_77,(0,1,0):C.UVGC_693_73,(0,1,1):C.UVGC_693_74,(0,1,2):C.UVGC_693_75,(0,1,3):C.UVGC_693_76,(0,1,4):C.UVGC_693_77,(0,2,0):C.UVGC_694_78,(0,2,1):C.UVGC_694_79,(0,2,2):C.UVGC_694_80,(0,2,3):C.UVGC_694_81,(0,2,4):C.UVGC_694_82})

V_59 = CTVertex(name = 'V_59',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV11, L.VVV12, L.VVV4, L.VVV5, L.VVV7, L.VVV9 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,2,0):C.UVGC_685_39,(0,2,1):C.UVGC_685_40,(0,2,2):C.UVGC_685_41,(0,2,3):C.UVGC_685_42,(0,2,4):C.UVGC_685_43,(0,3,0):C.UVGC_686_44,(0,3,1):C.UVGC_686_45,(0,3,2):C.UVGC_686_46,(0,3,3):C.UVGC_686_47,(0,3,4):C.UVGC_686_48,(0,4,0):C.UVGC_686_44,(0,4,1):C.UVGC_686_45,(0,4,2):C.UVGC_686_46,(0,4,3):C.UVGC_686_47,(0,4,4):C.UVGC_686_48,(0,5,0):C.UVGC_685_39,(0,5,1):C.UVGC_685_40,(0,5,2):C.UVGC_685_41,(0,5,3):C.UVGC_685_42,(0,5,4):C.UVGC_685_43,(0,0,0):C.UVGC_685_39,(0,0,1):C.UVGC_685_40,(0,0,2):C.UVGC_685_41,(0,0,3):C.UVGC_685_42,(0,0,4):C.UVGC_685_43,(0,1,0):C.UVGC_686_44,(0,1,1):C.UVGC_686_45,(0,1,2):C.UVGC_686_46,(0,1,3):C.UVGC_686_47,(0,1,4):C.UVGC_686_48})

V_60 = CTVertex(name = 'V_60',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(2,0,2):C.UVGC_654_10,(2,0,3):C.UVGC_654_9,(0,0,2):C.UVGC_654_10,(0,0,3):C.UVGC_654_9,(4,0,2):C.UVGC_653_7,(4,0,3):C.UVGC_653_8,(3,0,2):C.UVGC_653_7,(3,0,3):C.UVGC_653_8,(8,0,2):C.UVGC_654_9,(8,0,3):C.UVGC_654_10,(7,0,0):C.UVGC_689_56,(7,0,1):C.UVGC_689_57,(7,0,2):C.UVGC_689_58,(7,0,3):C.UVGC_689_59,(7,0,4):C.UVGC_689_60,(6,0,0):C.UVGC_689_56,(6,0,1):C.UVGC_689_57,(6,0,2):C.UVGC_690_61,(6,0,3):C.UVGC_690_62,(6,0,4):C.UVGC_689_60,(5,0,2):C.UVGC_653_7,(5,0,3):C.UVGC_653_8,(1,0,2):C.UVGC_653_7,(1,0,3):C.UVGC_653_8,(11,0,2):C.UVGC_657_13,(11,0,3):C.UVGC_657_14,(10,0,2):C.UVGC_657_13,(10,0,3):C.UVGC_657_14,(9,0,2):C.UVGC_656_11,(9,0,3):C.UVGC_656_12,(2,1,2):C.UVGC_654_10,(2,1,3):C.UVGC_654_9,(0,1,2):C.UVGC_654_10,(0,1,3):C.UVGC_654_9,(4,1,2):C.UVGC_653_7,(4,1,3):C.UVGC_653_8,(3,1,2):C.UVGC_653_7,(3,1,3):C.UVGC_653_8,(8,1,0):C.UVGC_689_56,(8,1,1):C.UVGC_689_57,(8,1,2):C.UVGC_689_58,(8,1,3):C.UVGC_689_59,(8,1,4):C.UVGC_689_60,(6,1,0):C.UVGC_687_49,(6,1,1):C.UVGC_687_50,(6,1,2):C.UVGC_688_54,(6,1,3):C.UVGC_688_55,(6,1,4):C.UVGC_687_53,(5,1,2):C.UVGC_653_7,(5,1,3):C.UVGC_653_8,(1,1,2):C.UVGC_653_7,(1,1,3):C.UVGC_653_8,(7,1,2):C.UVGC_654_9,(7,1,3):C.UVGC_654_10,(11,1,2):C.UVGC_657_13,(11,1,3):C.UVGC_657_14,(10,1,2):C.UVGC_657_13,(10,1,3):C.UVGC_657_14,(9,1,2):C.UVGC_656_11,(9,1,3):C.UVGC_656_12,(2,2,2):C.UVGC_654_10,(2,2,3):C.UVGC_654_9,(0,2,2):C.UVGC_654_10,(0,2,3):C.UVGC_654_9,(4,2,2):C.UVGC_653_7,(4,2,3):C.UVGC_653_8,(3,2,2):C.UVGC_653_7,(3,2,3):C.UVGC_653_8,(8,2,0):C.UVGC_687_49,(8,2,1):C.UVGC_687_50,(8,2,2):C.UVGC_687_51,(8,2,3):C.UVGC_687_52,(8,2,4):C.UVGC_687_53,(7,2,0):C.UVGC_687_49,(7,2,1):C.UVGC_687_50,(7,2,2):C.UVGC_687_51,(7,2,3):C.UVGC_687_52,(7,2,4):C.UVGC_687_53,(5,2,2):C.UVGC_653_7,(5,2,3):C.UVGC_653_8,(1,2,2):C.UVGC_653_7,(1,2,3):C.UVGC_653_8,(11,2,2):C.UVGC_657_13,(11,2,3):C.UVGC_657_14,(10,2,2):C.UVGC_657_13,(10,2,3):C.UVGC_657_14,(9,2,2):C.UVGC_656_11,(9,2,3):C.UVGC_656_12})

V_61 = CTVertex(name = 'V_61',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4, L.FFS6 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_704_95,(0,0,2):C.UVGC_704_96,(0,0,1):C.UVGC_704_97,(0,1,0):C.UVGC_705_98,(0,1,2):C.UVGC_705_99,(0,1,1):C.UVGC_705_100})

V_62 = CTVertex(name = 'V_62',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_682_32})

V_63 = CTVertex(name = 'V_63',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_681_31})

V_64 = CTVertex(name = 'V_64',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4, L.FFS6 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_706_101,(0,0,2):C.UVGC_706_102,(0,0,1):C.UVGC_706_103,(0,1,0):C.UVGC_703_92,(0,1,2):C.UVGC_703_93,(0,1,1):C.UVGC_703_94})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_707_104})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_708_105})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_699_88,(0,1,0):C.UVGC_700_89})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_701_90,(0,1,0):C.UVGC_702_91})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_677_27,(0,1,0):C.UVGC_678_28})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_679_29,(0,1,0):C.UVGC_680_30})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,4):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,1):C.UVGC_660_18,(0,1,2):C.UVGC_660_19,(0,1,3):C.UVGC_660_20,(0,1,4):C.UVGC_660_21,(0,2,0):C.UVGC_660_17,(0,2,1):C.UVGC_660_18,(0,2,2):C.UVGC_660_19,(0,2,3):C.UVGC_660_20,(0,2,4):C.UVGC_660_21})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,2):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,1):C.UVGC_660_18,(0,1,3):C.UVGC_660_19,(0,1,4):C.UVGC_660_20,(0,1,2):C.UVGC_660_21,(0,2,0):C.UVGC_660_17,(0,2,1):C.UVGC_660_18,(0,2,3):C.UVGC_660_19,(0,2,4):C.UVGC_660_20,(0,2,2):C.UVGC_660_21})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,4):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,1):C.UVGC_660_18,(0,1,2):C.UVGC_660_19,(0,1,3):C.UVGC_660_20,(0,1,4):C.UVGC_696_84,(0,2,0):C.UVGC_660_17,(0,2,1):C.UVGC_660_18,(0,2,2):C.UVGC_660_19,(0,2,3):C.UVGC_660_20,(0,2,4):C.UVGC_696_84})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,2):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,1):C.UVGC_660_18,(0,1,3):C.UVGC_660_19,(0,1,4):C.UVGC_660_20,(0,1,2):C.UVGC_660_21,(0,2,0):C.UVGC_660_17,(0,2,1):C.UVGC_660_18,(0,2,3):C.UVGC_660_19,(0,2,4):C.UVGC_660_20,(0,2,2):C.UVGC_660_21})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                couplings = {(0,0,4):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,1):C.UVGC_660_18,(0,1,2):C.UVGC_660_19,(0,1,3):C.UVGC_660_20,(0,1,4):C.UVGC_660_21,(0,2,0):C.UVGC_660_17,(0,2,1):C.UVGC_660_18,(0,2,2):C.UVGC_660_19,(0,2,3):C.UVGC_660_20,(0,2,4):C.UVGC_660_21})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ] ],
                couplings = {(0,0,1):C.UVGC_658_15,(0,1,0):C.UVGC_660_17,(0,1,2):C.UVGC_660_18,(0,1,3):C.UVGC_660_19,(0,1,4):C.UVGC_660_20,(0,1,1):C.UVGC_675_25,(0,2,0):C.UVGC_660_17,(0,2,2):C.UVGC_660_18,(0,2,3):C.UVGC_660_19,(0,2,4):C.UVGC_660_20,(0,2,1):C.UVGC_675_25})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_672_22,(0,0,1):C.UVGC_672_23})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_672_22,(0,0,1):C.UVGC_672_23})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_698_86,(0,0,2):C.UVGC_698_87,(0,0,1):C.UVGC_672_23})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_672_22,(0,0,1):C.UVGC_672_23})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_672_22,(0,0,1):C.UVGC_672_23})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_698_86,(0,0,2):C.UVGC_698_87,(0,0,1):C.UVGC_672_23})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_659_16,(0,1,0):C.UVGC_644_1,(0,2,0):C.UVGC_644_1})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_659_16,(0,1,0):C.UVGC_644_1,(0,2,0):C.UVGC_644_1})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_697_85,(0,2,0):C.UVGC_697_85,(0,1,0):C.UVGC_695_83,(0,3,0):C.UVGC_695_83})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_659_16,(0,1,0):C.UVGC_644_1,(0,2,0):C.UVGC_644_1})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_659_16,(0,1,0):C.UVGC_644_1,(0,2,0):C.UVGC_644_1})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_676_26,(0,2,0):C.UVGC_676_26,(0,1,0):C.UVGC_674_24,(0,3,0):C.UVGC_674_24})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_684_35,(0,0,1):C.UVGC_684_36,(0,0,2):C.UVGC_684_37,(0,0,3):C.UVGC_684_38,(0,1,0):C.UVGC_683_33,(0,1,3):C.UVGC_683_34})

