Requestor: Nikola Whallon
Content: Mono-Higgs effective field theory
Paper 1: http://arxiv.org/abs/1312.2592
Paper 2: http://arxiv.org/abs/1506.01081
Validation: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MonoHiggsEFTModelValidation
JIRA: https://its.cern.ch/jira/browse/AGENE-1026