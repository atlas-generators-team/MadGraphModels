# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 13.0.0 for Linux x86 (64-bit) (December 3, 2021)
# Date: Mon 9 May 2022 11:24:32


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_161_1 = Coupling(name = 'R2GC_161_1',
                      value = '-0.0625*(complex(0,1)*G**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_162_2 = Coupling(name = 'R2GC_162_2',
                      value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_163_3 = Coupling(name = 'R2GC_163_3',
                      value = '-0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_165_4 = Coupling(name = 'R2GC_165_4',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_172_5 = Coupling(name = 'R2GC_172_5',
                      value = '-0.125*(complex(0,1)*G**2*KHTT1*MT)/cmath.pi**2 - (complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'HTTMOD':1,'QCD':2})

R2GC_173_6 = Coupling(name = 'R2GC_173_6',
                      value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_174_7 = Coupling(name = 'R2GC_174_7',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_174_8 = Coupling(name = 'R2GC_174_8',
                      value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_174_9 = Coupling(name = 'R2GC_174_9',
                      value = '(25*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_174_10 = Coupling(name = 'R2GC_174_10',
                       value = '(2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_175_11 = Coupling(name = 'R2GC_175_11',
                       value = '-0.006944444444444444*(ee*complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3,'QED':1})

R2GC_175_12 = Coupling(name = 'R2GC_175_12',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_175_13 = Coupling(name = 'R2GC_175_13',
                       value = '(5*ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_175_14 = Coupling(name = 'R2GC_175_14',
                       value = '-0.027777777777777776*(ee*complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3,'QED':1})

R2GC_176_15 = Coupling(name = 'R2GC_176_15',
                       value = '(complex(0,1)*G**2*KS10BB*MBP)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_176_16 = Coupling(name = 'R2GC_176_16',
                       value = '(complex(0,1)*G**2*KS10U3x3*MT)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'SX':1})

R2GC_176_17 = Coupling(name = 'R2GC_176_17',
                       value = '(complex(0,1)*G**2*KS10TT*MTP)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_176_18 = Coupling(name = 'R2GC_176_18',
                       value = '(complex(0,1)*G**2*KS10XX*MX)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_176_19 = Coupling(name = 'R2GC_176_19',
                       value = '(complex(0,1)*G**2*KS10YY*MY)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_177_20 = Coupling(name = 'R2GC_177_20',
                       value = '-0.125*(complex(0,1)*G**2*MBP**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_177_21 = Coupling(name = 'R2GC_177_21',
                       value = '-0.125*(complex(0,1)*G**2*MT**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_177_22 = Coupling(name = 'R2GC_177_22',
                       value = '-0.125*(complex(0,1)*G**2*MTP**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_177_23 = Coupling(name = 'R2GC_177_23',
                       value = '-0.125*(complex(0,1)*G**2*MX**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_177_24 = Coupling(name = 'R2GC_177_24',
                       value = '-0.125*(complex(0,1)*G**2*MY**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_178_25 = Coupling(name = 'R2GC_178_25',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_178_26 = Coupling(name = 'R2GC_178_26',
                       value = '-0.003472222222222222*(ee**2*complex(0,1)*G**2*KZBBL)/(cw*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*KZBBR)/(288.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_178_27 = Coupling(name = 'R2GC_178_27',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_178_28 = Coupling(name = 'R2GC_178_28',
                       value = '(ee**2*complex(0,1)*G**2*KZTTL)/(144.*cw*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*KZTTR)/(144.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_178_29 = Coupling(name = 'R2GC_178_29',
                       value = '(5*ee**2*complex(0,1)*G**2*KZXXL)/(288.*cw*cmath.pi**2*sw) + (5*ee**2*complex(0,1)*G**2*KZXXR)/(288.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_178_30 = Coupling(name = 'R2GC_178_30',
                       value = '-0.013888888888888888*(ee**2*complex(0,1)*G**2*KZYYL)/(cw*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*KZYYR)/(72.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_179_31 = Coupling(name = 'R2GC_179_31',
                       value = '-0.005208333333333333*(cw*ee*complex(0,1)*G**3)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_179_32 = Coupling(name = 'R2GC_179_32',
                       value = '(ee*complex(0,1)*G**3*KZBBL)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*KZBBR)/(192.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_179_33 = Coupling(name = 'R2GC_179_33',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_179_34 = Coupling(name = 'R2GC_179_34',
                       value = '(ee*complex(0,1)*G**3*KZTTL)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*KZTTR)/(192.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_179_35 = Coupling(name = 'R2GC_179_35',
                       value = '(ee*complex(0,1)*G**3*KZXXL)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*KZXXR)/(192.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_179_36 = Coupling(name = 'R2GC_179_36',
                       value = '(ee*complex(0,1)*G**3*KZYYL)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*KZYYR)/(192.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_180_37 = Coupling(name = 'R2GC_180_37',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_180_38 = Coupling(name = 'R2GC_180_38',
                       value = '(3*ee*complex(0,1)*G**3*KZBBL)/(64.*cw*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*KZBBR)/(64.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_180_39 = Coupling(name = 'R2GC_180_39',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_180_40 = Coupling(name = 'R2GC_180_40',
                       value = '(3*ee*complex(0,1)*G**3*KZTTL)/(64.*cw*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*KZTTR)/(64.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_180_41 = Coupling(name = 'R2GC_180_41',
                       value = '(3*ee*complex(0,1)*G**3*KZXXL)/(64.*cw*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*KZXXR)/(64.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_180_42 = Coupling(name = 'R2GC_180_42',
                       value = '(3*ee*complex(0,1)*G**3*KZYYL)/(64.*cw*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*KZYYR)/(64.*cw*cmath.pi**2*sw)',
                       order = {'QCD':3,'VLQ':1})

R2GC_181_43 = Coupling(name = 'R2GC_181_43',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_182_44 = Coupling(name = 'R2GC_182_44',
                       value = '-0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_185_45 = Coupling(name = 'R2GC_185_45',
                       value = '-0.125*(complex(0,1)*G**2*KP10D3x3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10D3x3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'SX':2})

R2GC_185_46 = Coupling(name = 'R2GC_185_46',
                       value = '-0.125*(complex(0,1)*G**2*KP10BB**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10BB**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_185_47 = Coupling(name = 'R2GC_185_47',
                       value = '-0.125*(complex(0,1)*G**2*KP10U3x3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10U3x3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'SX':2})

R2GC_185_48 = Coupling(name = 'R2GC_185_48',
                       value = '-0.125*(complex(0,1)*G**2*KP10TT**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10TT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_185_49 = Coupling(name = 'R2GC_185_49',
                       value = '-0.125*(complex(0,1)*G**2*KP10XX**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10XX**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_185_50 = Coupling(name = 'R2GC_185_50',
                       value = '-0.125*(complex(0,1)*G**2*KP10YY**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10YY**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_185_51 = Coupling(name = 'R2GC_185_51',
                       value = '-0.125*(complex(0,1)*G**2*KS10BL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10BR3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_185_52 = Coupling(name = 'R2GC_185_52',
                       value = '-0.125*(complex(0,1)*G**2*KS10TL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS10TR3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_186_53 = Coupling(name = 'R2GC_186_53',
                       value = '(complex(0,1)*G**2*KHTT1*KS10U3x3)/(8.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'HTTMOD':1,'QCD':2,'SX':1})

R2GC_186_54 = Coupling(name = 'R2GC_186_54',
                       value = '-0.125*(complex(0,1)*G**2*KBLh3*KS10BL3)/cmath.pi**2 - (complex(0,1)*G**2*KBRh3*KS10BR3)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_186_55 = Coupling(name = 'R2GC_186_55',
                       value = '-0.125*(complex(0,1)*G**2*KS10TL3*KTLh3)/cmath.pi**2 - (complex(0,1)*G**2*KS10TR3*KTRh3)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_187_56 = Coupling(name = 'R2GC_187_56',
                       value = '-0.125*(complex(0,1)*G**2*KHTT1**2)/cmath.pi**2 - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*KHTT1*yt)/(4.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'HTTMOD':2,'QCD':2})

R2GC_187_57 = Coupling(name = 'R2GC_187_57',
                       value = '-0.125*(complex(0,1)*G**2*KBLh3**2)/cmath.pi**2 - (complex(0,1)*G**2*KBRh3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_187_58 = Coupling(name = 'R2GC_187_58',
                       value = '-0.125*(complex(0,1)*G**2*KTLh3**2)/cmath.pi**2 - (complex(0,1)*G**2*KTRh3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_59 = Coupling(name = 'R2GC_188_59',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_188_60 = Coupling(name = 'R2GC_188_60',
                       value = '(ee**2*complex(0,1)*G**2*KZBBL**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KZBBR**2)/(192.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_61 = Coupling(name = 'R2GC_188_61',
                       value = '-0.003472222222222222*(ee**2*complex(0,1)*G**2)/cmath.pi**2 + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_188_62 = Coupling(name = 'R2GC_188_62',
                       value = '(ee**2*complex(0,1)*G**2*KZTTL**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KZTTR**2)/(192.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_63 = Coupling(name = 'R2GC_188_63',
                       value = '(ee**2*complex(0,1)*G**2*KZXXL**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KZXXR**2)/(192.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_64 = Coupling(name = 'R2GC_188_64',
                       value = '(ee**2*complex(0,1)*G**2*KZYYL**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KZYYR**2)/(192.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_65 = Coupling(name = 'R2GC_188_65',
                       value = '(ee**2*complex(0,1)*G**2*KBLz3**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRz3**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_188_66 = Coupling(name = 'R2GC_188_66',
                       value = '(ee**2*complex(0,1)*G**2*KTLz3**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRz3**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_190_67 = Coupling(name = 'R2GC_190_67',
                       value = '-0.0625*(complex(0,1)*G**2*KS12XDL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS12XDR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_190_68 = Coupling(name = 'R2GC_190_68',
                       value = '-0.0625*(complex(0,1)*G**2*KS12XBL**2)/cmath.pi**2 - (complex(0,1)*G**2*KS12XBR**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_190_69 = Coupling(name = 'R2GC_190_69',
                       value = '-0.0625*(complex(0,1)*G**2*KS12YUL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS12YUR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_190_70 = Coupling(name = 'R2GC_190_70',
                       value = '-0.0625*(complex(0,1)*G**2*KS12YTL**2)/cmath.pi**2 - (complex(0,1)*G**2*KS12YTR**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_71 = Coupling(name = 'R2GC_191_71',
                       value = '-0.0625*(complex(0,1)*G**2*KS11qqL3x3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11qqR3x3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'SX':2})

R2GC_191_72 = Coupling(name = 'R2GC_191_72',
                       value = '-0.0625*(complex(0,1)*G**2*KS11TdL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11TdR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_73 = Coupling(name = 'R2GC_191_73',
                       value = '-0.0625*(complex(0,1)*G**2*KS11YdL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11YdR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_74 = Coupling(name = 'R2GC_191_74',
                       value = '-0.0625*(complex(0,1)*G**2*KS11BuL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11BuR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_75 = Coupling(name = 'R2GC_191_75',
                       value = '-0.0625*(complex(0,1)*G**2*KS11TBL**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11TBR**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_76 = Coupling(name = 'R2GC_191_76',
                       value = '-0.0625*(complex(0,1)*G**2*KS11BYL**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11BYR**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_77 = Coupling(name = 'R2GC_191_77',
                       value = '-0.0625*(complex(0,1)*G**2*KS11XuL3**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11XuR3**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_191_78 = Coupling(name = 'R2GC_191_78',
                       value = '-0.0625*(complex(0,1)*G**2*KS11XTL**2)/cmath.pi**2 - (complex(0,1)*G**2*KS11XTR**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_192_79 = Coupling(name = 'R2GC_192_79',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_192_80 = Coupling(name = 'R2GC_192_80',
                       value = '(ee**2*complex(0,1)*G**2*KTLw3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRw3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_192_81 = Coupling(name = 'R2GC_192_81',
                       value = '(ee**2*complex(0,1)*G**2*KYL3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KYR3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_192_82 = Coupling(name = 'R2GC_192_82',
                       value = '(ee**2*complex(0,1)*G**2*KBLw3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRw3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_192_83 = Coupling(name = 'R2GC_192_83',
                       value = '(ee**2*complex(0,1)*G**2*KWTBL**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KWTBR**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_192_84 = Coupling(name = 'R2GC_192_84',
                       value = '(ee**2*complex(0,1)*G**2*KWBYL**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KWBYR**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_192_85 = Coupling(name = 'R2GC_192_85',
                       value = '(ee**2*complex(0,1)*G**2*KXL3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KXR3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_192_86 = Coupling(name = 'R2GC_192_86',
                       value = '(ee**2*complex(0,1)*G**2*KWXTL**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KWXTR**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_206_87 = Coupling(name = 'R2GC_206_87',
                       value = '-0.005208333333333333*G**4/cmath.pi**2',
                       order = {'QCD':4})

R2GC_206_88 = Coupling(name = 'R2GC_206_88',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_207_89 = Coupling(name = 'R2GC_207_89',
                       value = '-0.005208333333333333*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_207_90 = Coupling(name = 'R2GC_207_90',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_208_91 = Coupling(name = 'R2GC_208_91',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_208_92 = Coupling(name = 'R2GC_208_92',
                       value = '-0.015625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_209_93 = Coupling(name = 'R2GC_209_93',
                       value = '-0.020833333333333332*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_210_94 = Coupling(name = 'R2GC_210_94',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_210_95 = Coupling(name = 'R2GC_210_95',
                       value = '-0.03125*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_211_96 = Coupling(name = 'R2GC_211_96',
                       value = '-0.0625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_211_97 = Coupling(name = 'R2GC_211_97',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_212_98 = Coupling(name = 'R2GC_212_98',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_212_99 = Coupling(name = 'R2GC_212_99',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_213_100 = Coupling(name = 'R2GC_213_100',
                        value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_215_101 = Coupling(name = 'R2GC_215_101',
                        value = '-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',
                        order = {'QCD':3})

R2GC_216_102 = Coupling(name = 'R2GC_216_102',
                        value = '-0.3333333333333333*(G**2*KP10D3x3)/cmath.pi**2 - (complex(0,1)*G**2*KS10D3x3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'SX':1})

R2GC_217_103 = Coupling(name = 'R2GC_217_103',
                        value = '(G**2*KP10D3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*KS10D3x3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'SX':1})

R2GC_220_104 = Coupling(name = 'R2GC_220_104',
                        value = '-0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_236_105 = Coupling(name = 'R2GC_236_105',
                        value = '(-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_238_106 = Coupling(name = 'R2GC_238_106',
                        value = '(2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_240_107 = Coupling(name = 'R2GC_240_107',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_245_108 = Coupling(name = 'R2GC_245_108',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KBLh3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_246_109 = Coupling(name = 'R2GC_246_109',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KBRh3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_247_110 = Coupling(name = 'R2GC_247_110',
                        value = '-0.3333333333333333*(G**2*KP10BB)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_248_111 = Coupling(name = 'R2GC_248_111',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10BB)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_249_112 = Coupling(name = 'R2GC_249_112',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10BL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_250_113 = Coupling(name = 'R2GC_250_113',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10BR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_251_114 = Coupling(name = 'R2GC_251_114',
                        value = '(complex(0,1)*G**2*MBP)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_252_115 = Coupling(name = 'R2GC_252_115',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KBLz3)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_253_116 = Coupling(name = 'R2GC_253_116',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KBRz3)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_254_117 = Coupling(name = 'R2GC_254_117',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZBBL)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_255_118 = Coupling(name = 'R2GC_255_118',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZBBR)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_259_119 = Coupling(name = 'R2GC_259_119',
                        value = '(G**2*KP10U3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*KS10U3x3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'SX':1})

R2GC_260_120 = Coupling(name = 'R2GC_260_120',
                        value = '-0.3333333333333333*(G**2*KP10U3x3)/cmath.pi**2 - (complex(0,1)*G**2*KS10U3x3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'SX':1})

R2GC_261_121 = Coupling(name = 'R2GC_261_121',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_262_122 = Coupling(name = 'R2GC_262_122',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_263_123 = Coupling(name = 'R2GC_263_123',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11qqL3x3)/cmath.pi**2',
                        order = {'QCD':2,'SX':1})

R2GC_264_124 = Coupling(name = 'R2GC_264_124',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11qqR3x3)/cmath.pi**2',
                        order = {'QCD':2,'SX':1})

R2GC_265_125 = Coupling(name = 'R2GC_265_125',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_267_126 = Coupling(name = 'R2GC_267_126',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_268_127 = Coupling(name = 'R2GC_268_127',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_271_128 = Coupling(name = 'R2GC_271_128',
                        value = '(complex(0,1)*G**2*KHTT1)/(3.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'HTTMOD':1,'QCD':2})

R2GC_275_129 = Coupling(name = 'R2GC_275_129',
                        value = '-0.3333333333333333*(G**2*KP10TT)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_276_130 = Coupling(name = 'R2GC_276_130',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_277_131 = Coupling(name = 'R2GC_277_131',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_278_132 = Coupling(name = 'R2GC_278_132',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10TT)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_279_133 = Coupling(name = 'R2GC_279_133',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_280_134 = Coupling(name = 'R2GC_280_134',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_281_135 = Coupling(name = 'R2GC_281_135',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11TdL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_282_136 = Coupling(name = 'R2GC_282_136',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11TdR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_283_137 = Coupling(name = 'R2GC_283_137',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KTLh3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_284_138 = Coupling(name = 'R2GC_284_138',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KTRh3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_285_139 = Coupling(name = 'R2GC_285_139',
                        value = '(complex(0,1)*G**2*MTP)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_286_140 = Coupling(name = 'R2GC_286_140',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KTLw3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_287_141 = Coupling(name = 'R2GC_287_141',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_288_142 = Coupling(name = 'R2GC_288_142',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KTRw3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_289_143 = Coupling(name = 'R2GC_289_143',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_290_144 = Coupling(name = 'R2GC_290_144',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_291_145 = Coupling(name = 'R2GC_291_145',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_292_146 = Coupling(name = 'R2GC_292_146',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZTTL)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_293_147 = Coupling(name = 'R2GC_293_147',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZTTR)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_297_148 = Coupling(name = 'R2GC_297_148',
                        value = '-0.3333333333333333*(G**2*KP10XX)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_298_149 = Coupling(name = 'R2GC_298_149',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10XX)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_299_150 = Coupling(name = 'R2GC_299_150',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_300_151 = Coupling(name = 'R2GC_300_151',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_301_152 = Coupling(name = 'R2GC_301_152',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_302_153 = Coupling(name = 'R2GC_302_153',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_303_154 = Coupling(name = 'R2GC_303_154',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_304_155 = Coupling(name = 'R2GC_304_155',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_305_156 = Coupling(name = 'R2GC_305_156',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12XDL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_306_157 = Coupling(name = 'R2GC_306_157',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12XDR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_307_158 = Coupling(name = 'R2GC_307_158',
                        value = '(complex(0,1)*G**2*MX)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_308_159 = Coupling(name = 'R2GC_308_159',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_309_160 = Coupling(name = 'R2GC_309_160',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_310_161 = Coupling(name = 'R2GC_310_161',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_311_162 = Coupling(name = 'R2GC_311_162',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_312_163 = Coupling(name = 'R2GC_312_163',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZXXL)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_313_164 = Coupling(name = 'R2GC_313_164',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZXXR)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_315_165 = Coupling(name = 'R2GC_315_165',
                        value = 'G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_315_166 = Coupling(name = 'R2GC_315_166',
                        value = '(11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_316_167 = Coupling(name = 'R2GC_316_167',
                        value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_317_168 = Coupling(name = 'R2GC_317_168',
                        value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_317_169 = Coupling(name = 'R2GC_317_169',
                        value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_318_170 = Coupling(name = 'R2GC_318_170',
                        value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_319_171 = Coupling(name = 'R2GC_319_171',
                        value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_320_172 = Coupling(name = 'R2GC_320_172',
                        value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_321_173 = Coupling(name = 'R2GC_321_173',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_325_174 = Coupling(name = 'R2GC_325_174',
                        value = '-0.3333333333333333*(G**2*KP10YY)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_326_175 = Coupling(name = 'R2GC_326_175',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS10YY)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_327_176 = Coupling(name = 'R2GC_327_176',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_328_177 = Coupling(name = 'R2GC_328_177',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_329_178 = Coupling(name = 'R2GC_329_178',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11YdL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_330_179 = Coupling(name = 'R2GC_330_179',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS11YdR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_331_180 = Coupling(name = 'R2GC_331_180',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_332_181 = Coupling(name = 'R2GC_332_181',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_333_182 = Coupling(name = 'R2GC_333_182',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_334_183 = Coupling(name = 'R2GC_334_183',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2',
                        order = {'QCD':2,'VLQ':1})

R2GC_335_184 = Coupling(name = 'R2GC_335_184',
                        value = '(complex(0,1)*G**2*MY)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_336_185 = Coupling(name = 'R2GC_336_185',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_337_186 = Coupling(name = 'R2GC_337_186',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_338_187 = Coupling(name = 'R2GC_338_187',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KYL3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_339_188 = Coupling(name = 'R2GC_339_188',
                        value = '-0.16666666666666666*(ee*complex(0,1)*G**2*KYR3)/(cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_340_189 = Coupling(name = 'R2GC_340_189',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZYYL)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_341_190 = Coupling(name = 'R2GC_341_190',
                        value = '-0.08333333333333333*(ee*complex(0,1)*G**2*KZYYR)/(cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_193_1 = Coupling(name = 'UVGC_193_1',
                      value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_194_2 = Coupling(name = 'UVGC_194_2',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_195_3 = Coupling(name = 'UVGC_195_3',
                      value = {-1:'-0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2})

UVGC_196_4 = Coupling(name = 'UVGC_196_4',
                      value = {-1:'-0.027777777777777776*(ee*complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2,'QED':1})

UVGC_198_5 = Coupling(name = 'UVGC_198_5',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_205_6 = Coupling(name = 'UVGC_205_6',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_205_7 = Coupling(name = 'UVGC_205_7',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_206_8 = Coupling(name = 'UVGC_206_8',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_206_9 = Coupling(name = 'UVGC_206_9',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_207_10 = Coupling(name = 'UVGC_207_10',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_207_11 = Coupling(name = 'UVGC_207_11',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_209_12 = Coupling(name = 'UVGC_209_12',
                       value = {-1:'-0.0078125*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_209_13 = Coupling(name = 'UVGC_209_13',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_14 = Coupling(name = 'UVGC_210_14',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_15 = Coupling(name = 'UVGC_210_15',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_211_16 = Coupling(name = 'UVGC_211_16',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_211_17 = Coupling(name = 'UVGC_211_17',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_18 = Coupling(name = 'UVGC_212_18',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_19 = Coupling(name = 'UVGC_212_19',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_213_20 = Coupling(name = 'UVGC_213_20',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_214_21 = Coupling(name = 'UVGC_214_21',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_214_22 = Coupling(name = 'UVGC_214_22',
                       value = {-1:'( 0 if MBP else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_214_23 = Coupling(name = 'UVGC_214_23',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_214_24 = Coupling(name = 'UVGC_214_24',
                       value = {-1:'-0.0078125*(complex(0,1)*G**3)/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_214_25 = Coupling(name = 'UVGC_214_25',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_214_26 = Coupling(name = 'UVGC_214_26',
                       value = {-1:'( 0 if MTP else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_214_27 = Coupling(name = 'UVGC_214_27',
                       value = {-1:'( 0 if MX else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_214_28 = Coupling(name = 'UVGC_214_28',
                       value = {-1:'( 0 if MY else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_214_29 = Coupling(name = 'UVGC_214_29',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_215_30 = Coupling(name = 'UVGC_215_30',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_216_31 = Coupling(name = 'UVGC_216_31',
                       value = {-1:'-0.25*(G**2*KP10D3x3)/cmath.pi**2 - (complex(0,1)*G**2*KS10D3x3)/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_217_32 = Coupling(name = 'UVGC_217_32',
                       value = {-1:'(G**2*KP10D3x3)/(4.*cmath.pi**2) - (complex(0,1)*G**2*KS10D3x3)/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_220_33 = Coupling(name = 'UVGC_220_33',
                       value = {-1:'-0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2'},
                       order = {'QCD':2,'QED':1})

UVGC_236_34 = Coupling(name = 'UVGC_236_34',
                       value = {-1:'(-5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_238_35 = Coupling(name = 'UVGC_238_35',
                       value = {-1:'(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_240_36 = Coupling(name = 'UVGC_240_36',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_240_37 = Coupling(name = 'UVGC_240_37',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_242_38 = Coupling(name = 'UVGC_242_38',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MBP else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MBP/MU_R))/(2.*cmath.pi**2) if MBP else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_243_39 = Coupling(name = 'UVGC_243_39',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MBP else -0.027777777777777776*(ee*complex(0,1)*G**2)/cmath.pi**2 )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MBP/MU_R))/(6.*cmath.pi**2) if MBP else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_244_40 = Coupling(name = 'UVGC_244_40',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MBP else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MBP/MU_R))/(2.*cmath.pi**2) if MBP else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_245_41 = Coupling(name = 'UVGC_245_41',
                       value = {-1:'(complex(0,1)*G**2*KBLh3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_245_42 = Coupling(name = 'UVGC_245_42',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KBLh3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KBLh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KBLh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KBLh3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KBLh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KBLh3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_245_43 = Coupling(name = 'UVGC_245_43',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KBLh3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_246_44 = Coupling(name = 'UVGC_246_44',
                       value = {-1:'(complex(0,1)*G**2*KBRh3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_246_45 = Coupling(name = 'UVGC_246_45',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KBRh3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KBRh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KBRh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KBRh3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KBRh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KBRh3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_246_46 = Coupling(name = 'UVGC_246_46',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KBRh3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_247_47 = Coupling(name = 'UVGC_247_47',
                       value = {-1:'( -0.16666666666666666*(G**2*KP10BB)/cmath.pi**2 if MBP else (G**2*KP10BB)/(12.*cmath.pi**2) ) - (G**2*KP10BB)/(3.*cmath.pi**2)',0:'( (-5*G**2*KP10BB)/(12.*cmath.pi**2) + (G**2*KP10BB*reglog(MBP/MU_R))/(2.*cmath.pi**2) if MBP else -0.08333333333333333*(G**2*KP10BB)/cmath.pi**2 ) + (G**2*KP10BB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_248_48 = Coupling(name = 'UVGC_248_48',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**2*KS10BB)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS10BB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*KS10BB)/(3.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*KS10BB)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10BB*reglog(MBP/MU_R))/(2.*cmath.pi**2) if MBP else -0.08333333333333333*(complex(0,1)*G**2*KS10BB)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10BB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_249_49 = Coupling(name = 'UVGC_249_49',
                       value = {-1:'(complex(0,1)*G**2*KS10BL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_249_50 = Coupling(name = 'UVGC_249_50',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10BL3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS10BL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10BL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10BL3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS10BL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10BL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_249_51 = Coupling(name = 'UVGC_249_51',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS10BL3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_250_52 = Coupling(name = 'UVGC_250_52',
                       value = {-1:'(complex(0,1)*G**2*KS10BR3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_250_53 = Coupling(name = 'UVGC_250_53',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10BR3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS10BR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10BR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10BR3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS10BR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10BR3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_250_54 = Coupling(name = 'UVGC_250_54',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS10BR3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_251_55 = Coupling(name = 'UVGC_251_55',
                       value = {-1:'( (complex(0,1)*G**2*MBP)/(6.*cmath.pi**2) if MBP else -0.08333333333333333*(complex(0,1)*G**2*MBP)/cmath.pi**2 ) + (complex(0,1)*G**2*MBP)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MBP)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MBP*reglog(MBP/MU_R))/cmath.pi**2 if MBP else (complex(0,1)*G**2*MBP)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MBP)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_252_56 = Coupling(name = 'UVGC_252_56',
                       value = {-1:'(ee*complex(0,1)*G**2*KBLz3)/(48.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_252_57 = Coupling(name = 'UVGC_252_57',
                       value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KBLz3)/(cw*cmath.pi**2*sw) if MBP else (ee*complex(0,1)*G**2*KBLz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KBLz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KBLz3*reglog(MBP/MU_R))/(8.*cw*cmath.pi**2*sw) if MBP else -0.020833333333333332*(ee*complex(0,1)*G**2*KBLz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KBLz3)/(48.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_252_58 = Coupling(name = 'UVGC_252_58',
                       value = {-1:'-0.041666666666666664*(ee*complex(0,1)*G**2*KBLz3)/(cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_253_59 = Coupling(name = 'UVGC_253_59',
                       value = {-1:'(ee*complex(0,1)*G**2*KBRz3)/(48.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_253_60 = Coupling(name = 'UVGC_253_60',
                       value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KBRz3)/(cw*cmath.pi**2*sw) if MBP else (ee*complex(0,1)*G**2*KBRz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KBRz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KBRz3*reglog(MBP/MU_R))/(8.*cw*cmath.pi**2*sw) if MBP else -0.020833333333333332*(ee*complex(0,1)*G**2*KBRz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KBRz3)/(48.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_253_61 = Coupling(name = 'UVGC_253_61',
                       value = {-1:'-0.041666666666666664*(ee*complex(0,1)*G**2*KBRz3)/(cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_254_62 = Coupling(name = 'UVGC_254_62',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZBBL)/(cw*cmath.pi**2*sw) if MBP else (ee*complex(0,1)*G**2*KZBBL)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZBBL)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZBBL)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZBBL*reglog(MBP/MU_R))/(4.*cw*cmath.pi**2*sw) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KZBBL)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZBBL)/(24.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_255_63 = Coupling(name = 'UVGC_255_63',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZBBR)/(cw*cmath.pi**2*sw) if MBP else (ee*complex(0,1)*G**2*KZBBR)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZBBR)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZBBR)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZBBR*reglog(MBP/MU_R))/(4.*cw*cmath.pi**2*sw) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KZBBR)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZBBR)/(24.*cw*cmath.pi**2*sw)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_256_64 = Coupling(name = 'UVGC_256_64',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_257_65 = Coupling(name = 'UVGC_257_65',
                       value = {-1:'( -0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2 if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2 ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_258_66 = Coupling(name = 'UVGC_258_66',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_259_67 = Coupling(name = 'UVGC_259_67',
                       value = {-1:'( (G**2*KP10U3x3)/(6.*cmath.pi**2) - (complex(0,1)*G**2*KS10U3x3)/(6.*cmath.pi**2) if MT else -0.08333333333333333*(G**2*KP10U3x3)/cmath.pi**2 + (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) ) + (G**2*KP10U3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*KS10U3x3)/(3.*cmath.pi**2)',0:'( (5*G**2*KP10U3x3)/(12.*cmath.pi**2) - (5*complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) - (G**2*KP10U3x3*reglog(MT/MU_R))/(2.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (G**2*KP10U3x3)/(12.*cmath.pi**2) - (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) ) - (G**2*KP10U3x3)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_260_68 = Coupling(name = 'UVGC_260_68',
                       value = {-1:'( -0.16666666666666666*(G**2*KP10U3x3)/cmath.pi**2 - (complex(0,1)*G**2*KS10U3x3)/(6.*cmath.pi**2) if MT else (G**2*KP10U3x3)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) ) - (G**2*KP10U3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*KS10U3x3)/(3.*cmath.pi**2)',0:'( (-5*G**2*KP10U3x3)/(12.*cmath.pi**2) - (5*complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) + (G**2*KP10U3x3*reglog(MT/MU_R))/(2.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -0.08333333333333333*(G**2*KP10U3x3)/cmath.pi**2 - (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2) ) + (G**2*KP10U3x3)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10U3x3)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_261_69 = Coupling(name = 'UVGC_261_69',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BuL3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_261_70 = Coupling(name = 'UVGC_261_70',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BuL3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BuL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_261_71 = Coupling(name = 'UVGC_261_71',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11BuL3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_262_72 = Coupling(name = 'UVGC_262_72',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BuR3*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_262_73 = Coupling(name = 'UVGC_262_73',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BuR3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BuR3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_262_74 = Coupling(name = 'UVGC_262_74',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11BuR3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_263_75 = Coupling(name = 'UVGC_263_75',
                       value = {-1:'(complex(0,1)*G**2*KS11qqL3x3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_263_76 = Coupling(name = 'UVGC_263_76',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11qqL3x3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11qqL3x3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11qqL3x3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11qqL3x3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11qqL3x3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11qqL3x3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_263_77 = Coupling(name = 'UVGC_263_77',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11qqL3x3)/cmath.pi**2'},
                       order = {'QCD':2,'SX':1})

UVGC_264_78 = Coupling(name = 'UVGC_264_78',
                       value = {-1:'(complex(0,1)*G**2*KS11qqR3x3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_264_79 = Coupling(name = 'UVGC_264_79',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11qqR3x3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11qqR3x3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11qqR3x3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11qqR3x3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11qqR3x3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11qqR3x3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'SX':1})

UVGC_264_80 = Coupling(name = 'UVGC_264_80',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11qqR3x3)/cmath.pi**2'},
                       order = {'QCD':2,'SX':1})

UVGC_265_81 = Coupling(name = 'UVGC_265_81',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -0.08333333333333333*(complex(0,1)*G**2*MT)/cmath.pi**2 ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_266_82 = Coupling(name = 'UVGC_266_82',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_267_83 = Coupling(name = 'UVGC_267_83',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KBLw3*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_267_84 = Coupling(name = 'UVGC_267_84',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KBLw3*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KBLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_267_85 = Coupling(name = 'UVGC_267_85',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KBLw3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_268_86 = Coupling(name = 'UVGC_268_86',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KBRw3*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_268_87 = Coupling(name = 'UVGC_268_87',
                       value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KBRw3*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KBRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_268_88 = Coupling(name = 'UVGC_268_88',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KBRw3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'VLQ':1})

UVGC_269_89 = Coupling(name = 'UVGC_269_89',
                       value = {-1:'( -0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -0.041666666666666664*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_270_90 = Coupling(name = 'UVGC_270_90',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_271_91 = Coupling(name = 'UVGC_271_91',
                       value = {-1:'( (complex(0,1)*G**2*KHTT1)/(6.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -0.08333333333333333*(complex(0,1)*G**2*KHTT1)/cmath.pi**2 - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*KHTT1)/(3.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (5*complex(0,1)*G**2*KHTT1)/(12.*cmath.pi**2) + (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*KHTT1*reglog(MT/MU_R))/(2.*cmath.pi**2) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*KHTT1)/(12.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*KHTT1)/(12.*cmath.pi**2) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'HTTMOD':1,'QCD':2})

UVGC_272_92 = Coupling(name = 'UVGC_272_92',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MTP else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MTP/MU_R))/(2.*cmath.pi**2) if MTP else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_273_93 = Coupling(name = 'UVGC_273_93',
                       value = {-1:'( -0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2 if MTP else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MTP/MU_R))/(3.*cmath.pi**2) if MTP else -0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2 ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_274_94 = Coupling(name = 'UVGC_274_94',
                       value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MTP else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MTP/MU_R))/(2.*cmath.pi**2) if MTP else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_275_95 = Coupling(name = 'UVGC_275_95',
                       value = {-1:'( -0.16666666666666666*(G**2*KP10TT)/cmath.pi**2 if MTP else (G**2*KP10TT)/(12.*cmath.pi**2) ) - (G**2*KP10TT)/(3.*cmath.pi**2)',0:'( (-5*G**2*KP10TT)/(12.*cmath.pi**2) + (G**2*KP10TT*reglog(MTP/MU_R))/(2.*cmath.pi**2) if MTP else -0.08333333333333333*(G**2*KP10TT)/cmath.pi**2 ) + (G**2*KP10TT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_276_96 = Coupling(name = 'UVGC_276_96',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10TL3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_276_97 = Coupling(name = 'UVGC_276_97',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10TL3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10TL3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_276_98 = Coupling(name = 'UVGC_276_98',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS10TL3)/cmath.pi**2'},
                       order = {'QCD':2,'VLQ':1})

UVGC_277_99 = Coupling(name = 'UVGC_277_99',
                       value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10TR3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'VLQ':1})

UVGC_277_100 = Coupling(name = 'UVGC_277_100',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS10TR3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10TR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_277_101 = Coupling(name = 'UVGC_277_101',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS10TR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_278_102 = Coupling(name = 'UVGC_278_102',
                        value = {-1:'( -0.16666666666666666*(complex(0,1)*G**2*KS10TT)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS10TT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*KS10TT)/(3.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*KS10TT)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10TT*reglog(MTP/MU_R))/(2.*cmath.pi**2) if MTP else -0.08333333333333333*(complex(0,1)*G**2*KS10TT)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10TT)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_279_103 = Coupling(name = 'UVGC_279_103',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TBL*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_279_104 = Coupling(name = 'UVGC_279_104',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TBL*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TBL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_279_105 = Coupling(name = 'UVGC_279_105',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11TBL)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_280_106 = Coupling(name = 'UVGC_280_106',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TBR*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_280_107 = Coupling(name = 'UVGC_280_107',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TBR*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TBR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_280_108 = Coupling(name = 'UVGC_280_108',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11TBR)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_281_109 = Coupling(name = 'UVGC_281_109',
                        value = {-1:'(complex(0,1)*G**2*KS11TdL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_281_110 = Coupling(name = 'UVGC_281_110',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TdL3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11TdL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TdL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TdL3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11TdL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TdL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_281_111 = Coupling(name = 'UVGC_281_111',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11TdL3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_282_112 = Coupling(name = 'UVGC_282_112',
                        value = {-1:'(complex(0,1)*G**2*KS11TdR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_282_113 = Coupling(name = 'UVGC_282_113',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11TdR3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11TdR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11TdR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11TdR3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11TdR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11TdR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_282_114 = Coupling(name = 'UVGC_282_114',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11TdR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_283_115 = Coupling(name = 'UVGC_283_115',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KTLh3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KTLh3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KTLh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_283_116 = Coupling(name = 'UVGC_283_116',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KTLh3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KTLh3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KTLh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KTLh3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_283_117 = Coupling(name = 'UVGC_283_117',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KTLh3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_284_118 = Coupling(name = 'UVGC_284_118',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KTRh3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KTRh3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KTRh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_284_119 = Coupling(name = 'UVGC_284_119',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KTRh3)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KTRh3*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KTRh3)/cmath.pi**2 ) + (complex(0,1)*G**2*KTRh3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_284_120 = Coupling(name = 'UVGC_284_120',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KTRh3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_285_121 = Coupling(name = 'UVGC_285_121',
                        value = {-1:'( (complex(0,1)*G**2*MTP)/(6.*cmath.pi**2) if MTP else -0.08333333333333333*(complex(0,1)*G**2*MTP)/cmath.pi**2 ) + (complex(0,1)*G**2*MTP)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MTP)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MTP*reglog(MTP/MU_R))/cmath.pi**2 if MTP else (complex(0,1)*G**2*MTP)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MTP)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_286_122 = Coupling(name = 'UVGC_286_122',
                        value = {-1:'(ee*complex(0,1)*G**2*KTLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_286_123 = Coupling(name = 'UVGC_286_123',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KTLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KTLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KTLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KTLw3*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KTLw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KTLw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_286_124 = Coupling(name = 'UVGC_286_124',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KTLw3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_287_125 = Coupling(name = 'UVGC_287_125',
                        value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw) if MT else (ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KTLz3*reglog(MT/MU_R))/(8.*cw*cmath.pi**2*sw) if MT else -0.020833333333333332*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_287_126 = Coupling(name = 'UVGC_287_126',
                        value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw) if MTP else (ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KTLz3*reglog(MTP/MU_R))/(8.*cw*cmath.pi**2*sw) if MTP else -0.020833333333333332*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KTLz3)/(48.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_287_127 = Coupling(name = 'UVGC_287_127',
                        value = {-1:'-0.041666666666666664*(ee*complex(0,1)*G**2*KTLz3)/(cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_288_128 = Coupling(name = 'UVGC_288_128',
                        value = {-1:'(ee*complex(0,1)*G**2*KTRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_288_129 = Coupling(name = 'UVGC_288_129',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KTRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KTRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KTRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KTRw3*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KTRw3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KTRw3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_288_130 = Coupling(name = 'UVGC_288_130',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KTRw3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_289_131 = Coupling(name = 'UVGC_289_131',
                        value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw) if MT else (ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KTRz3*reglog(MT/MU_R))/(8.*cw*cmath.pi**2*sw) if MT else -0.020833333333333332*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_289_132 = Coupling(name = 'UVGC_289_132',
                        value = {-1:'( -0.041666666666666664*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw) if MTP else (ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw) )',0:'( (-5*ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KTRz3*reglog(MTP/MU_R))/(8.*cw*cmath.pi**2*sw) if MTP else -0.020833333333333332*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KTRz3)/(48.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_289_133 = Coupling(name = 'UVGC_289_133',
                        value = {-1:'-0.041666666666666664*(ee*complex(0,1)*G**2*KTRz3)/(cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_290_134 = Coupling(name = 'UVGC_290_134',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWTBL*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_290_135 = Coupling(name = 'UVGC_290_135',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWTBL*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWTBL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_290_136 = Coupling(name = 'UVGC_290_136',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWTBL)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_291_137 = Coupling(name = 'UVGC_291_137',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWTBR*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_291_138 = Coupling(name = 'UVGC_291_138',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWTBR*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWTBR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_291_139 = Coupling(name = 'UVGC_291_139',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWTBR)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_292_140 = Coupling(name = 'UVGC_292_140',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZTTL)/(cw*cmath.pi**2*sw) if MTP else (ee*complex(0,1)*G**2*KZTTL)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZTTL)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZTTL)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZTTL*reglog(MTP/MU_R))/(4.*cw*cmath.pi**2*sw) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KZTTL)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZTTL)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_293_141 = Coupling(name = 'UVGC_293_141',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZTTR)/(cw*cmath.pi**2*sw) if MTP else (ee*complex(0,1)*G**2*KZTTR)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZTTR)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZTTR)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZTTR*reglog(MTP/MU_R))/(4.*cw*cmath.pi**2*sw) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KZTTR)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZTTR)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_294_142 = Coupling(name = 'UVGC_294_142',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MX else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MX/MU_R))/(2.*cmath.pi**2) if MX else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_295_143 = Coupling(name = 'UVGC_295_143',
                        value = {-1:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MX else (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (-25*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (5*ee*complex(0,1)*G**2*reglog(MX/MU_R))/(6.*cmath.pi**2) if MX else (-5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) + (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_296_144 = Coupling(name = 'UVGC_296_144',
                        value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MX else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MX/MU_R))/(2.*cmath.pi**2) if MX else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_297_145 = Coupling(name = 'UVGC_297_145',
                        value = {-1:'( -0.16666666666666666*(G**2*KP10XX)/cmath.pi**2 if MX else (G**2*KP10XX)/(12.*cmath.pi**2) ) - (G**2*KP10XX)/(3.*cmath.pi**2)',0:'( (-5*G**2*KP10XX)/(12.*cmath.pi**2) + (G**2*KP10XX*reglog(MX/MU_R))/(2.*cmath.pi**2) if MX else -0.08333333333333333*(G**2*KP10XX)/cmath.pi**2 ) + (G**2*KP10XX)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_298_146 = Coupling(name = 'UVGC_298_146',
                        value = {-1:'( -0.16666666666666666*(complex(0,1)*G**2*KS10XX)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS10XX)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*KS10XX)/(3.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*KS10XX)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10XX*reglog(MX/MU_R))/(2.*cmath.pi**2) if MX else -0.08333333333333333*(complex(0,1)*G**2*KS10XX)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10XX)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_299_147 = Coupling(name = 'UVGC_299_147',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XTL*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_299_148 = Coupling(name = 'UVGC_299_148',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XTL*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_299_149 = Coupling(name = 'UVGC_299_149',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11XTL)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_300_150 = Coupling(name = 'UVGC_300_150',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XTR*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_300_151 = Coupling(name = 'UVGC_300_151',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XTR*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_300_152 = Coupling(name = 'UVGC_300_152',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11XTR)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_301_153 = Coupling(name = 'UVGC_301_153',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XuL3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_301_154 = Coupling(name = 'UVGC_301_154',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XuL3*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XuL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_301_155 = Coupling(name = 'UVGC_301_155',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11XuL3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_302_156 = Coupling(name = 'UVGC_302_156',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XuR3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_302_157 = Coupling(name = 'UVGC_302_157',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11XuR3*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11XuR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_302_158 = Coupling(name = 'UVGC_302_158',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11XuR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_303_159 = Coupling(name = 'UVGC_303_159',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XBL*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_303_160 = Coupling(name = 'UVGC_303_160',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XBL*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XBL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_303_161 = Coupling(name = 'UVGC_303_161',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12XBL)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_304_162 = Coupling(name = 'UVGC_304_162',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XBR*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_304_163 = Coupling(name = 'UVGC_304_163',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XBR*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XBR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_304_164 = Coupling(name = 'UVGC_304_164',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12XBR)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_305_165 = Coupling(name = 'UVGC_305_165',
                        value = {-1:'(complex(0,1)*G**2*KS12XDL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_305_166 = Coupling(name = 'UVGC_305_166',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XDL3)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS12XDL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XDL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XDL3*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS12XDL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XDL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_305_167 = Coupling(name = 'UVGC_305_167',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12XDL3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_306_168 = Coupling(name = 'UVGC_306_168',
                        value = {-1:'(complex(0,1)*G**2*KS12XDR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_306_169 = Coupling(name = 'UVGC_306_169',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12XDR3)/cmath.pi**2 if MX else (complex(0,1)*G**2*KS12XDR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12XDR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12XDR3*reglog(MX/MU_R))/(4.*cmath.pi**2) if MX else -0.041666666666666664*(complex(0,1)*G**2*KS12XDR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12XDR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_306_170 = Coupling(name = 'UVGC_306_170',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12XDR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_307_171 = Coupling(name = 'UVGC_307_171',
                        value = {-1:'( (complex(0,1)*G**2*MX)/(6.*cmath.pi**2) if MX else -0.08333333333333333*(complex(0,1)*G**2*MX)/cmath.pi**2 ) + (complex(0,1)*G**2*MX)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MX)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MX*reglog(MX/MU_R))/cmath.pi**2 if MX else (complex(0,1)*G**2*MX)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MX)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_308_172 = Coupling(name = 'UVGC_308_172',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWXTL*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_308_173 = Coupling(name = 'UVGC_308_173',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MX else (ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWXTL*reglog(MX/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWXTL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_308_174 = Coupling(name = 'UVGC_308_174',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWXTL)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_309_175 = Coupling(name = 'UVGC_309_175',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MTP else (ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWXTR*reglog(MTP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MTP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_309_176 = Coupling(name = 'UVGC_309_176',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MX else (ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWXTR*reglog(MX/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWXTR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_309_177 = Coupling(name = 'UVGC_309_177',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWXTR)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_310_178 = Coupling(name = 'UVGC_310_178',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KXL3*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_310_179 = Coupling(name = 'UVGC_310_179',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MX else (ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KXL3*reglog(MX/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KXL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_310_180 = Coupling(name = 'UVGC_310_180',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KXL3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_311_181 = Coupling(name = 'UVGC_311_181',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KXR3*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -0.041666666666666664*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_311_182 = Coupling(name = 'UVGC_311_182',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MX else (ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KXR3*reglog(MX/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KXR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_311_183 = Coupling(name = 'UVGC_311_183',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KXR3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_312_184 = Coupling(name = 'UVGC_312_184',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZXXL)/(cw*cmath.pi**2*sw) if MX else (ee*complex(0,1)*G**2*KZXXL)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZXXL)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZXXL)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZXXL*reglog(MX/MU_R))/(4.*cw*cmath.pi**2*sw) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KZXXL)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZXXL)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_313_185 = Coupling(name = 'UVGC_313_185',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZXXR)/(cw*cmath.pi**2*sw) if MX else (ee*complex(0,1)*G**2*KZXXR)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZXXR)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZXXR)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZXXR*reglog(MX/MU_R))/(4.*cw*cmath.pi**2*sw) if MX else -0.041666666666666664*(ee*complex(0,1)*G**2*KZXXR)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZXXR)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_314_186 = Coupling(name = 'UVGC_314_186',
                        value = {-1:'( 0 if MBP else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MBP/MU_R))/(12.*cmath.pi**2) if MBP else 0 )'},
                        order = {'QCD':2})

UVGC_314_187 = Coupling(name = 'UVGC_314_187',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_314_188 = Coupling(name = 'UVGC_314_188',
                        value = {-1:'( 0 if MTP else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MTP/MU_R))/(12.*cmath.pi**2) if MTP else 0 )'},
                        order = {'QCD':2})

UVGC_314_189 = Coupling(name = 'UVGC_314_189',
                        value = {-1:'( 0 if MX else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MX/MU_R))/(12.*cmath.pi**2) if MX else 0 )'},
                        order = {'QCD':2})

UVGC_314_190 = Coupling(name = 'UVGC_314_190',
                        value = {-1:'( 0 if MY else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MY/MU_R))/(12.*cmath.pi**2) if MY else 0 )'},
                        order = {'QCD':2})

UVGC_315_191 = Coupling(name = 'UVGC_315_191',
                        value = {-1:'-0.020833333333333332*G**3/cmath.pi**2'},
                        order = {'QCD':3})

UVGC_315_192 = Coupling(name = 'UVGC_315_192',
                        value = {-1:'( 0 if MBP else -0.0625*G**3/cmath.pi**2 ) + G**3/(24.*cmath.pi**2)',0:'( -0.08333333333333333*(G**3*reglog(MBP/MU_R))/cmath.pi**2 if MBP else 0 )'},
                        order = {'QCD':3})

UVGC_315_193 = Coupling(name = 'UVGC_315_193',
                        value = {-1:'( 0 if MT else -0.0625*G**3/cmath.pi**2 ) + G**3/(24.*cmath.pi**2)',0:'( -0.08333333333333333*(G**3*reglog(MT/MU_R))/cmath.pi**2 if MT else 0 )'},
                        order = {'QCD':3})

UVGC_315_194 = Coupling(name = 'UVGC_315_194',
                        value = {-1:'( 0 if MTP else -0.0625*G**3/cmath.pi**2 ) + G**3/(24.*cmath.pi**2)',0:'( -0.08333333333333333*(G**3*reglog(MTP/MU_R))/cmath.pi**2 if MTP else 0 )'},
                        order = {'QCD':3})

UVGC_315_195 = Coupling(name = 'UVGC_315_195',
                        value = {-1:'( 0 if MX else -0.0625*G**3/cmath.pi**2 ) + G**3/(24.*cmath.pi**2)',0:'( -0.08333333333333333*(G**3*reglog(MX/MU_R))/cmath.pi**2 if MX else 0 )'},
                        order = {'QCD':3})

UVGC_315_196 = Coupling(name = 'UVGC_315_196',
                        value = {-1:'( 0 if MY else -0.0625*G**3/cmath.pi**2 ) + G**3/(24.*cmath.pi**2)',0:'( -0.08333333333333333*(G**3*reglog(MY/MU_R))/cmath.pi**2 if MY else 0 )'},
                        order = {'QCD':3})

UVGC_316_197 = Coupling(name = 'UVGC_316_197',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2'},
                        order = {'QCD':4})

UVGC_316_198 = Coupling(name = 'UVGC_316_198',
                        value = {-1:'( 0 if MBP else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 )',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MBP/MU_R))/cmath.pi**2 if MBP else 0 )'},
                        order = {'QCD':4})

UVGC_316_199 = Coupling(name = 'UVGC_316_199',
                        value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_316_200 = Coupling(name = 'UVGC_316_200',
                        value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_316_201 = Coupling(name = 'UVGC_316_201',
                        value = {-1:'( 0 if MT else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 )',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2 if MT else 0 )'},
                        order = {'QCD':4})

UVGC_316_202 = Coupling(name = 'UVGC_316_202',
                        value = {-1:'( 0 if MTP else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 )',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MTP/MU_R))/cmath.pi**2 if MTP else 0 )'},
                        order = {'QCD':4})

UVGC_316_203 = Coupling(name = 'UVGC_316_203',
                        value = {-1:'( 0 if MX else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 )',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MX/MU_R))/cmath.pi**2 if MX else 0 )'},
                        order = {'QCD':4})

UVGC_316_204 = Coupling(name = 'UVGC_316_204',
                        value = {-1:'( 0 if MY else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 )',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MY/MU_R))/cmath.pi**2 if MY else 0 )'},
                        order = {'QCD':4})

UVGC_317_205 = Coupling(name = 'UVGC_317_205',
                        value = {-1:'( 0 if MBP else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MBP/MU_R))/cmath.pi**2 if MBP else 0 )'},
                        order = {'QCD':4})

UVGC_317_206 = Coupling(name = 'UVGC_317_206',
                        value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_317_207 = Coupling(name = 'UVGC_317_207',
                        value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_317_208 = Coupling(name = 'UVGC_317_208',
                        value = {-1:'( 0 if MT else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2 if MT else 0 )'},
                        order = {'QCD':4})

UVGC_317_209 = Coupling(name = 'UVGC_317_209',
                        value = {-1:'( 0 if MTP else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MTP/MU_R))/cmath.pi**2 if MTP else 0 )'},
                        order = {'QCD':4})

UVGC_317_210 = Coupling(name = 'UVGC_317_210',
                        value = {-1:'( 0 if MX else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MX/MU_R))/cmath.pi**2 if MX else 0 )'},
                        order = {'QCD':4})

UVGC_317_211 = Coupling(name = 'UVGC_317_211',
                        value = {-1:'( 0 if MY else -0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2 ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -0.08333333333333333*(complex(0,1)*G**4*reglog(MY/MU_R))/cmath.pi**2 if MY else 0 )'},
                        order = {'QCD':4})

UVGC_318_212 = Coupling(name = 'UVGC_318_212',
                        value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_318_213 = Coupling(name = 'UVGC_318_213',
                        value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_319_214 = Coupling(name = 'UVGC_319_214',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_319_215 = Coupling(name = 'UVGC_319_215',
                        value = {-1:'( 0 if MBP else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MBP/MU_R))/(12.*cmath.pi**2) if MBP else 0 )'},
                        order = {'QCD':4})

UVGC_319_216 = Coupling(name = 'UVGC_319_216',
                        value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_319_217 = Coupling(name = 'UVGC_319_217',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_319_218 = Coupling(name = 'UVGC_319_218',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_319_219 = Coupling(name = 'UVGC_319_219',
                        value = {-1:'( 0 if MTP else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MTP/MU_R))/(12.*cmath.pi**2) if MTP else 0 )'},
                        order = {'QCD':4})

UVGC_319_220 = Coupling(name = 'UVGC_319_220',
                        value = {-1:'( 0 if MX else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MX/MU_R))/(12.*cmath.pi**2) if MX else 0 )'},
                        order = {'QCD':4})

UVGC_319_221 = Coupling(name = 'UVGC_319_221',
                        value = {-1:'( 0 if MY else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MY/MU_R))/(12.*cmath.pi**2) if MY else 0 )'},
                        order = {'QCD':4})

UVGC_320_222 = Coupling(name = 'UVGC_320_222',
                        value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_320_223 = Coupling(name = 'UVGC_320_223',
                        value = {-1:'( 0 if MBP else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MBP/MU_R))/(12.*cmath.pi**2) if MBP else 0 )'},
                        order = {'QCD':4})

UVGC_320_224 = Coupling(name = 'UVGC_320_224',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_320_225 = Coupling(name = 'UVGC_320_225',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_320_226 = Coupling(name = 'UVGC_320_226',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_320_227 = Coupling(name = 'UVGC_320_227',
                        value = {-1:'( 0 if MTP else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MTP/MU_R))/(12.*cmath.pi**2) if MTP else 0 )'},
                        order = {'QCD':4})

UVGC_320_228 = Coupling(name = 'UVGC_320_228',
                        value = {-1:'( 0 if MX else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MX/MU_R))/(12.*cmath.pi**2) if MX else 0 )'},
                        order = {'QCD':4})

UVGC_320_229 = Coupling(name = 'UVGC_320_229',
                        value = {-1:'( 0 if MY else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MY/MU_R))/(12.*cmath.pi**2) if MY else 0 )'},
                        order = {'QCD':4})

UVGC_321_230 = Coupling(name = 'UVGC_321_230',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_321_231 = Coupling(name = 'UVGC_321_231',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_322_232 = Coupling(name = 'UVGC_322_232',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MY else -0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2 ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MY/MU_R))/(2.*cmath.pi**2) if MY else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_323_233 = Coupling(name = 'UVGC_323_233',
                        value = {-1:'( (2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MY else -0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2 )',0:'( (5*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (2*ee*complex(0,1)*G**2*reglog(MY/MU_R))/(3.*cmath.pi**2) if MY else (ee*complex(0,1)*G**2)/(9.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(9.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_324_234 = Coupling(name = 'UVGC_324_234',
                        value = {-1:'( -0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2 if MY else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MY/MU_R))/(2.*cmath.pi**2) if MY else -0.08333333333333333*(complex(0,1)*G**3)/cmath.pi**2 ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_325_235 = Coupling(name = 'UVGC_325_235',
                        value = {-1:'( -0.16666666666666666*(G**2*KP10YY)/cmath.pi**2 if MY else (G**2*KP10YY)/(12.*cmath.pi**2) ) - (G**2*KP10YY)/(3.*cmath.pi**2)',0:'( (-5*G**2*KP10YY)/(12.*cmath.pi**2) + (G**2*KP10YY*reglog(MY/MU_R))/(2.*cmath.pi**2) if MY else -0.08333333333333333*(G**2*KP10YY)/cmath.pi**2 ) + (G**2*KP10YY)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_326_236 = Coupling(name = 'UVGC_326_236',
                        value = {-1:'( -0.16666666666666666*(complex(0,1)*G**2*KS10YY)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS10YY)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*KS10YY)/(3.*cmath.pi**2)',0:'( (-5*complex(0,1)*G**2*KS10YY)/(12.*cmath.pi**2) + (complex(0,1)*G**2*KS10YY*reglog(MY/MU_R))/(2.*cmath.pi**2) if MY else -0.08333333333333333*(complex(0,1)*G**2*KS10YY)/cmath.pi**2 ) + (complex(0,1)*G**2*KS10YY)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_327_237 = Coupling(name = 'UVGC_327_237',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BYL*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_327_238 = Coupling(name = 'UVGC_327_238',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BYL*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BYL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_327_239 = Coupling(name = 'UVGC_327_239',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11BYL)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_328_240 = Coupling(name = 'UVGC_328_240',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2 if MBP else (complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BYR*reglog(MBP/MU_R))/(4.*cmath.pi**2) if MBP else -0.041666666666666664*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_328_241 = Coupling(name = 'UVGC_328_241',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11BYR*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11BYR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_328_242 = Coupling(name = 'UVGC_328_242',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11BYR)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_329_243 = Coupling(name = 'UVGC_329_243',
                        value = {-1:'(complex(0,1)*G**2*KS11YdL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_329_244 = Coupling(name = 'UVGC_329_244',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11YdL3)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS11YdL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11YdL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11YdL3*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS11YdL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11YdL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_329_245 = Coupling(name = 'UVGC_329_245',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11YdL3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_330_246 = Coupling(name = 'UVGC_330_246',
                        value = {-1:'(complex(0,1)*G**2*KS11YdR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_330_247 = Coupling(name = 'UVGC_330_247',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS11YdR3)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS11YdR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS11YdR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS11YdR3*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS11YdR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS11YdR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_330_248 = Coupling(name = 'UVGC_330_248',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS11YdR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_331_249 = Coupling(name = 'UVGC_331_249',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YTL*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_331_250 = Coupling(name = 'UVGC_331_250',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YTL*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_331_251 = Coupling(name = 'UVGC_331_251',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12YTL)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_332_252 = Coupling(name = 'UVGC_332_252',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2 if MTP else (complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YTR*reglog(MTP/MU_R))/(4.*cmath.pi**2) if MTP else -0.041666666666666664*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_332_253 = Coupling(name = 'UVGC_332_253',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YTR*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_332_254 = Coupling(name = 'UVGC_332_254',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12YTR)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_333_255 = Coupling(name = 'UVGC_333_255',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YUL3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_333_256 = Coupling(name = 'UVGC_333_256',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YUL3*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YUL3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_333_257 = Coupling(name = 'UVGC_333_257',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12YUL3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_334_258 = Coupling(name = 'UVGC_334_258',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2 if MT else (complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YUR3*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -0.041666666666666664*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_334_259 = Coupling(name = 'UVGC_334_259',
                        value = {-1:'( -0.08333333333333333*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2 if MY else (complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*KS12YUR3*reglog(MY/MU_R))/(4.*cmath.pi**2) if MY else -0.041666666666666664*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2 ) + (complex(0,1)*G**2*KS12YUR3)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_334_260 = Coupling(name = 'UVGC_334_260',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*KS12YUR3)/cmath.pi**2'},
                        order = {'QCD':2,'VLQ':1})

UVGC_335_261 = Coupling(name = 'UVGC_335_261',
                        value = {-1:'( (complex(0,1)*G**2*MY)/(6.*cmath.pi**2) if MY else -0.08333333333333333*(complex(0,1)*G**2*MY)/cmath.pi**2 ) + (complex(0,1)*G**2*MY)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MY)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MY*reglog(MY/MU_R))/cmath.pi**2 if MY else (complex(0,1)*G**2*MY)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MY)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_336_262 = Coupling(name = 'UVGC_336_262',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWBYL*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_336_263 = Coupling(name = 'UVGC_336_263',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2)) if MY else (ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWBYL*reglog(MY/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWBYL)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_336_264 = Coupling(name = 'UVGC_336_264',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWBYL)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_337_265 = Coupling(name = 'UVGC_337_265',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MBP else (ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWBYR*reglog(MBP/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MBP else -0.041666666666666664*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_337_266 = Coupling(name = 'UVGC_337_266',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2)) if MY else (ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KWBYR*reglog(MY/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KWBYR)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_337_267 = Coupling(name = 'UVGC_337_267',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KWBYR)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'VLQ':1})

UVGC_338_268 = Coupling(name = 'UVGC_338_268',
                        value = {-1:'(ee*complex(0,1)*G**2*KYL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_338_269 = Coupling(name = 'UVGC_338_269',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KYL3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MY else (ee*complex(0,1)*G**2*KYL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KYL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KYL3*reglog(MY/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KYL3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KYL3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_338_270 = Coupling(name = 'UVGC_338_270',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KYL3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_339_271 = Coupling(name = 'UVGC_339_271',
                        value = {-1:'(ee*complex(0,1)*G**2*KYR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_339_272 = Coupling(name = 'UVGC_339_272',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KYR3)/(cmath.pi**2*sw*cmath.sqrt(2)) if MY else (ee*complex(0,1)*G**2*KYR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*KYR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*KYR3*reglog(MY/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KYR3)/(cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*KYR3)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_339_273 = Coupling(name = 'UVGC_339_273',
                        value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*KYR3)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_340_274 = Coupling(name = 'UVGC_340_274',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZYYL)/(cw*cmath.pi**2*sw) if MY else (ee*complex(0,1)*G**2*KZYYL)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZYYL)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZYYL)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZYYL*reglog(MY/MU_R))/(4.*cw*cmath.pi**2*sw) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KZYYL)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZYYL)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

UVGC_341_275 = Coupling(name = 'UVGC_341_275',
                        value = {-1:'( -0.08333333333333333*(ee*complex(0,1)*G**2*KZYYR)/(cw*cmath.pi**2*sw) if MY else (ee*complex(0,1)*G**2*KZYYR)/(24.*cw*cmath.pi**2*sw) ) - (ee*complex(0,1)*G**2*KZYYR)/(24.*cw*cmath.pi**2*sw)',0:'( (-5*ee*complex(0,1)*G**2*KZYYR)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*KZYYR*reglog(MY/MU_R))/(4.*cw*cmath.pi**2*sw) if MY else -0.041666666666666664*(ee*complex(0,1)*G**2*KZYYR)/(cw*cmath.pi**2*sw) ) + (ee*complex(0,1)*G**2*KZYYR)/(24.*cw*cmath.pi**2*sw)'},
                        order = {'QCD':2,'VLQ':1})

