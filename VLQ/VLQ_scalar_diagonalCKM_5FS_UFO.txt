Requester: Yoav Afik and Venugopal Ellajosyula
Contents: VLQ model with an additional BSM scalar
Source: Venugopal Ellajosyula (private communication)
JIRA: https://its.cern.ch/jira/browse/AGENE-1718