# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 22 Aug 2019 12:22:53


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_101_3 = Coupling(name = 'R2GC_101_3',
                      value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_103_8 = Coupling(name = 'R2GC_103_8',
                      value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_104_9 = Coupling(name = 'R2GC_104_9',
                      value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_104_10 = Coupling(name = 'R2GC_104_10',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_105_11 = Coupling(name = 'R2GC_105_11',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_105_12 = Coupling(name = 'R2GC_105_12',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_106_13 = Coupling(name = 'R2GC_106_13',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_107_14 = Coupling(name = 'R2GC_107_14',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_107_15 = Coupling(name = 'R2GC_107_15',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_107_16 = Coupling(name = 'R2GC_107_16',
                       value = '(25*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_107_17 = Coupling(name = 'R2GC_107_17',
                       value = '(2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_108_18 = Coupling(name = 'R2GC_108_18',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_108_19 = Coupling(name = 'R2GC_108_19',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_108_20 = Coupling(name = 'R2GC_108_20',
                       value = '(5*ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_108_21 = Coupling(name = 'R2GC_108_21',
                       value = '-(ee*complex(0,1)*G**3)/(36.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_109_22 = Coupling(name = 'R2GC_109_22',
                       value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_109_23 = Coupling(name = 'R2GC_109_23',
                       value = '-(complex(0,1)*G**2*MBP**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_109_24 = Coupling(name = 'R2GC_109_24',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_109_25 = Coupling(name = 'R2GC_109_25',
                       value = '-(complex(0,1)*G**2*MTP**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_109_26 = Coupling(name = 'R2GC_109_26',
                       value = '-(complex(0,1)*G**2*MX**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_109_27 = Coupling(name = 'R2GC_109_27',
                       value = '-(complex(0,1)*G**2*MY**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_110_28 = Coupling(name = 'R2GC_110_28',
                       value = '(ee**2*complex(0,1)*G**2*KTLw3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRw3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_110_29 = Coupling(name = 'R2GC_110_29',
                       value = '(ee**2*complex(0,1)*G**2*KBLw2**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRw2**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_110_30 = Coupling(name = 'R2GC_110_30',
                       value = '(ee**2*complex(0,1)*G**2*KBLw3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRw3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_110_31 = Coupling(name = 'R2GC_110_31',
                       value = '(ee**2*complex(0,1)*G**2*KBLw1**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRw1**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_110_32 = Coupling(name = 'R2GC_110_32',
                       value = '(ee**2*complex(0,1)*G**2*KTLw1**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRw1**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_110_33 = Coupling(name = 'R2GC_110_33',
                       value = '(ee**2*complex(0,1)*G**2*KTLw2**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRw2**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_111_34 = Coupling(name = 'R2GC_111_34',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_112_35 = Coupling(name = 'R2GC_112_35',
                       value = '-(complex(0,1)*G**2*KBLh3**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KBRh3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_112_36 = Coupling(name = 'R2GC_112_36',
                       value = '-(complex(0,1)*G**2*KBLh1**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KBRh1**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_112_37 = Coupling(name = 'R2GC_112_37',
                       value = '-(complex(0,1)*G**2*KBLh2**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KBRh2**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_112_38 = Coupling(name = 'R2GC_112_38',
                       value = '-(complex(0,1)*G**2*KTLh2**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KTRh2**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_112_39 = Coupling(name = 'R2GC_112_39',
                       value = '-(complex(0,1)*G**2*KTLh3**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KTRh3**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_112_40 = Coupling(name = 'R2GC_112_40',
                       value = '-(complex(0,1)*G**2*KTLh1**2)/(8.*cmath.pi**2) - (complex(0,1)*G**2*KTRh1**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_41 = Coupling(name = 'R2GC_113_41',
                       value = '(ee**2*complex(0,1)*G**2*KBLz3**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRz3**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_42 = Coupling(name = 'R2GC_113_42',
                       value = '(ee**2*complex(0,1)*G**2*KBLz1**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRz1**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_43 = Coupling(name = 'R2GC_113_43',
                       value = '(ee**2*complex(0,1)*G**2*KBLz2**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KBRz2**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_44 = Coupling(name = 'R2GC_113_44',
                       value = '(ee**2*complex(0,1)*G**2*KTLz2**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRz2**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_45 = Coupling(name = 'R2GC_113_45',
                       value = '(ee**2*complex(0,1)*G**2*KTLz3**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRz3**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_113_46 = Coupling(name = 'R2GC_113_46',
                       value = '(ee**2*complex(0,1)*G**2*KTLz1**2)/(96.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KTRz1**2)/(96.*cw**2*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'VLQ':2})

R2GC_114_47 = Coupling(name = 'R2GC_114_47',
                       value = '(ee**2*complex(0,1)*G**2*KYL3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KYR3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_114_48 = Coupling(name = 'R2GC_114_48',
                       value = '(ee**2*complex(0,1)*G**2*KXL2**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KXR2**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_114_49 = Coupling(name = 'R2GC_114_49',
                       value = '(ee**2*complex(0,1)*G**2*KYL1**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KYR1**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_114_50 = Coupling(name = 'R2GC_114_50',
                       value = '(ee**2*complex(0,1)*G**2*KYL2**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KYR2**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_114_51 = Coupling(name = 'R2GC_114_51',
                       value = '(ee**2*complex(0,1)*G**2*KXL3**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KXR3**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_114_52 = Coupling(name = 'R2GC_114_52',
                       value = '(ee**2*complex(0,1)*G**2*KXL1**2)/(96.*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*KXR1**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2,'VLQ':2})

R2GC_182_53 = Coupling(name = 'R2GC_182_53',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_54 = Coupling(name = 'R2GC_182_54',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_55 = Coupling(name = 'R2GC_183_55',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_56 = Coupling(name = 'R2GC_183_56',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_57 = Coupling(name = 'R2GC_184_57',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_58 = Coupling(name = 'R2GC_184_58',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_185_59 = Coupling(name = 'R2GC_185_59',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_186_60 = Coupling(name = 'R2GC_186_60',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_186_61 = Coupling(name = 'R2GC_186_61',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_188_62 = Coupling(name = 'R2GC_188_62',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_189_63 = Coupling(name = 'R2GC_189_63',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_191_64 = Coupling(name = 'R2GC_191_64',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_191_65 = Coupling(name = 'R2GC_191_65',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_192_66 = Coupling(name = 'R2GC_192_66',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_193_67 = Coupling(name = 'R2GC_193_67',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_194_68 = Coupling(name = 'R2GC_194_68',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_196_69 = Coupling(name = 'R2GC_196_69',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_196_70 = Coupling(name = 'R2GC_196_70',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_197_71 = Coupling(name = 'R2GC_197_71',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_198_72 = Coupling(name = 'R2GC_198_72',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_199_73 = Coupling(name = 'R2GC_199_73',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_200_74 = Coupling(name = 'R2GC_200_74',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_201_75 = Coupling(name = 'R2GC_201_75',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_202_76 = Coupling(name = 'R2GC_202_76',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_203_77 = Coupling(name = 'R2GC_203_77',
                       value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_204_78 = Coupling(name = 'R2GC_204_78',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_208_79 = Coupling(name = 'R2GC_208_79',
                       value = '(complex(0,1)*G**2*MBP)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_210_80 = Coupling(name = 'R2GC_210_80',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_212_81 = Coupling(name = 'R2GC_212_81',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_227_82 = Coupling(name = 'R2GC_227_82',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_230_83 = Coupling(name = 'R2GC_230_83',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_234_84 = Coupling(name = 'R2GC_234_84',
                       value = '(complex(0,1)*G**2*MTP)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_238_85 = Coupling(name = 'R2GC_238_85',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_239_86 = Coupling(name = 'R2GC_239_86',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_241_87 = Coupling(name = 'R2GC_241_87',
                       value = '(-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_243_88 = Coupling(name = 'R2GC_243_88',
                       value = '(complex(0,1)*G**2*MX)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_245_89 = Coupling(name = 'R2GC_245_89',
                       value = '(2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_247_90 = Coupling(name = 'R2GC_247_90',
                       value = '(complex(0,1)*G**2*MY)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_248_91 = Coupling(name = 'R2GC_248_91',
                       value = '-(complex(0,1)*G**2*KBLh3)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_249_92 = Coupling(name = 'R2GC_249_92',
                       value = '-(complex(0,1)*G**2*KBRh3)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'VLQ':1})

R2GC_250_93 = Coupling(name = 'R2GC_250_93',
                       value = '-(ee*complex(0,1)*G**2*KBLz3)/(12.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'VLQ':1})

R2GC_251_94 = Coupling(name = 'R2GC_251_94',
                       value = '-(ee*complex(0,1)*G**2*KBRz3)/(12.*cw*cmath.pi**2*sw)',
                       order = {'QCD':2,'VLQ':1})

R2GC_252_95 = Coupling(name = 'R2GC_252_95',
                       value = '-(ee*complex(0,1)*G**2*KTLw3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'VLQ':1})

R2GC_253_96 = Coupling(name = 'R2GC_253_96',
                       value = '-(ee*complex(0,1)*G**2*KTRw3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'VLQ':1})

R2GC_254_97 = Coupling(name = 'R2GC_254_97',
                       value = '-(ee*complex(0,1)*G**2*KYL3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_255_98 = Coupling(name = 'R2GC_255_98',
                       value = '-(ee*complex(0,1)*G**2*KYR3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_256_99 = Coupling(name = 'R2GC_256_99',
                       value = '-(ee*complex(0,1)*G**2*KBLw2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'VLQ':1})

R2GC_257_100 = Coupling(name = 'R2GC_257_100',
                        value = '-(ee*complex(0,1)*G**2*KBRw2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_258_101 = Coupling(name = 'R2GC_258_101',
                        value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_259_102 = Coupling(name = 'R2GC_259_102',
                        value = '-(complex(0,1)*G**2*KTLh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_260_103 = Coupling(name = 'R2GC_260_103',
                        value = '-(complex(0,1)*G**2*KTRh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_261_104 = Coupling(name = 'R2GC_261_104',
                        value = '-(ee*complex(0,1)*G**2*KTLz2)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_262_105 = Coupling(name = 'R2GC_262_105',
                        value = '-(ee*complex(0,1)*G**2*KTRz2)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_263_106 = Coupling(name = 'R2GC_263_106',
                        value = '-(ee*complex(0,1)*G**2*KXL2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_264_107 = Coupling(name = 'R2GC_264_107',
                        value = '-(ee*complex(0,1)*G**2*KXR2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_265_108 = Coupling(name = 'R2GC_265_108',
                        value = '-(complex(0,1)*G**2*KBLh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_266_109 = Coupling(name = 'R2GC_266_109',
                        value = '-(complex(0,1)*G**2*KBRh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_267_110 = Coupling(name = 'R2GC_267_110',
                        value = '-(ee*complex(0,1)*G**2*KBLz1)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_268_111 = Coupling(name = 'R2GC_268_111',
                        value = '-(ee*complex(0,1)*G**2*KBRz1)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_269_112 = Coupling(name = 'R2GC_269_112',
                        value = '-(ee*complex(0,1)*G**2*KTLw1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_270_113 = Coupling(name = 'R2GC_270_113',
                        value = '-(ee*complex(0,1)*G**2*KTRw1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_271_114 = Coupling(name = 'R2GC_271_114',
                        value = '-(ee*complex(0,1)*G**2*KYL1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_272_115 = Coupling(name = 'R2GC_272_115',
                        value = '-(ee*complex(0,1)*G**2*KYR1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_273_116 = Coupling(name = 'R2GC_273_116',
                        value = '-(complex(0,1)*G**2*KBLh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_274_117 = Coupling(name = 'R2GC_274_117',
                        value = '-(complex(0,1)*G**2*KBRh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_275_118 = Coupling(name = 'R2GC_275_118',
                        value = '-(ee*complex(0,1)*G**2*KBLz2)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_276_119 = Coupling(name = 'R2GC_276_119',
                        value = '-(ee*complex(0,1)*G**2*KBRz2)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_277_120 = Coupling(name = 'R2GC_277_120',
                        value = '-(ee*complex(0,1)*G**2*KTLw2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_278_121 = Coupling(name = 'R2GC_278_121',
                        value = '-(ee*complex(0,1)*G**2*KTRw2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_279_122 = Coupling(name = 'R2GC_279_122',
                        value = '-(ee*complex(0,1)*G**2*KYL2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_280_123 = Coupling(name = 'R2GC_280_123',
                        value = '-(ee*complex(0,1)*G**2*KYR2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_282_124 = Coupling(name = 'R2GC_282_124',
                        value = '-(ee*complex(0,1)*G**2*KBLw3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_283_125 = Coupling(name = 'R2GC_283_125',
                        value = '-(ee*complex(0,1)*G**2*KBRw3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_284_126 = Coupling(name = 'R2GC_284_126',
                        value = '-(complex(0,1)*G**2*KTLh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_285_127 = Coupling(name = 'R2GC_285_127',
                        value = '-(complex(0,1)*G**2*KTRh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_286_128 = Coupling(name = 'R2GC_286_128',
                        value = '-(ee*complex(0,1)*G**2*KTLz3)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_287_129 = Coupling(name = 'R2GC_287_129',
                        value = '-(ee*complex(0,1)*G**2*KTRz3)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_288_130 = Coupling(name = 'R2GC_288_130',
                        value = '-(ee*complex(0,1)*G**2*KXL3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_289_131 = Coupling(name = 'R2GC_289_131',
                        value = '-(ee*complex(0,1)*G**2*KXR3)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_290_132 = Coupling(name = 'R2GC_290_132',
                        value = '-(ee*complex(0,1)*G**2*KBLw1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_291_133 = Coupling(name = 'R2GC_291_133',
                        value = '-(ee*complex(0,1)*G**2*KBRw1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

R2GC_293_134 = Coupling(name = 'R2GC_293_134',
                        value = '-(complex(0,1)*G**2*KTLh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_294_135 = Coupling(name = 'R2GC_294_135',
                        value = '-(complex(0,1)*G**2*KTRh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

R2GC_295_136 = Coupling(name = 'R2GC_295_136',
                        value = '-(ee*complex(0,1)*G**2*KTLz1)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_296_137 = Coupling(name = 'R2GC_296_137',
                        value = '-(ee*complex(0,1)*G**2*KTRz1)/(12.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

R2GC_297_138 = Coupling(name = 'R2GC_297_138',
                        value = '-(ee*complex(0,1)*G**2*KXL1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_298_139 = Coupling(name = 'R2GC_298_139',
                        value = '-(ee*complex(0,1)*G**2*KXR1)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

R2GC_98_140 = Coupling(name = 'R2GC_98_140',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_99_141 = Coupling(name = 'R2GC_99_141',
                       value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_99_142 = Coupling(name = 'R2GC_99_142',
                       value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_115_1 = Coupling(name = 'UVGC_115_1',
                      value = '(11*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                      order = {'QCD':2})

UVGC_116_2 = Coupling(name = 'UVGC_116_2',
                      value = '-(complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                      order = {'QCD':2})

UVGC_117_3 = Coupling(name = 'UVGC_117_3',
                      value = '-(FRCTdeltaZxbbLxbG*complex(0,1))',
                      order = {'QCD':2})

UVGC_118_4 = Coupling(name = 'UVGC_118_4',
                      value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1))/3.',
                      order = {'QCD':2,'QED':1})

UVGC_119_5 = Coupling(name = 'UVGC_119_5',
                      value = '-(FRCTdeltaZxbbRxbG*complex(0,1))',
                      order = {'QCD':2})

UVGC_120_6 = Coupling(name = 'UVGC_120_6',
                      value = '-(ee*FRCTdeltaZxbbRxbG*complex(0,1))/3.',
                      order = {'QCD':2,'QED':1})

UVGC_121_7 = Coupling(name = 'UVGC_121_7',
                      value = 'FRCTdeltaZxbbLxbG*complex(0,1)*G',
                      order = {'QCD':3})

UVGC_122_8 = Coupling(name = 'UVGC_122_8',
                      value = 'FRCTdeltaZxbbRxbG*complex(0,1)*G',
                      order = {'QCD':3})

UVGC_123_9 = Coupling(name = 'UVGC_123_9',
                      value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*sw)/(3.*cw)',
                      order = {'QCD':2,'QED':1})

UVGC_124_10 = Coupling(name = 'UVGC_124_10',
                       value = '-(FRCTdeltaZxbpbpLxbpG*complex(0,1))',
                       order = {'QCD':2})

UVGC_125_11 = Coupling(name = 'UVGC_125_11',
                       value = '-(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_126_12 = Coupling(name = 'UVGC_126_12',
                       value = '-(FRCTdeltaZxbpbpRxbpG*complex(0,1))',
                       order = {'QCD':2})

UVGC_127_13 = Coupling(name = 'UVGC_127_13',
                       value = '-(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_128_14 = Coupling(name = 'UVGC_128_14',
                       value = 'FRCTdeltaZxbpbpLxbpG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_129_15 = Coupling(name = 'UVGC_129_15',
                       value = 'FRCTdeltaZxbpbpRxbpG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_130_16 = Coupling(name = 'UVGC_130_16',
                       value = '-(FRCTdeltaZxccLxcG*complex(0,1))',
                       order = {'QCD':2})

UVGC_131_17 = Coupling(name = 'UVGC_131_17',
                       value = '(2*ee*FRCTdeltaZxccLxcG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_132_18 = Coupling(name = 'UVGC_132_18',
                       value = '-(FRCTdeltaZxccRxcG*complex(0,1))',
                       order = {'QCD':2})

UVGC_133_19 = Coupling(name = 'UVGC_133_19',
                       value = '(2*ee*FRCTdeltaZxccRxcG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_134_20 = Coupling(name = 'UVGC_134_20',
                       value = 'FRCTdeltaZxccLxcG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_135_21 = Coupling(name = 'UVGC_135_21',
                       value = 'FRCTdeltaZxccRxcG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_136_22 = Coupling(name = 'UVGC_136_22',
                       value = '(-2*ee*FRCTdeltaZxccRxcG*complex(0,1)*sw)/(3.*cw)',
                       order = {'QCD':2,'QED':1})

UVGC_137_23 = Coupling(name = 'UVGC_137_23',
                       value = '-(FRCTdeltaZxddLxdG*complex(0,1))',
                       order = {'QCD':2})

UVGC_138_24 = Coupling(name = 'UVGC_138_24',
                       value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_139_25 = Coupling(name = 'UVGC_139_25',
                       value = '-(FRCTdeltaZxddRxdG*complex(0,1))',
                       order = {'QCD':2})

UVGC_140_26 = Coupling(name = 'UVGC_140_26',
                       value = '-(ee*FRCTdeltaZxddRxdG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_141_27 = Coupling(name = 'UVGC_141_27',
                       value = 'FRCTdeltaZxddLxdG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_142_28 = Coupling(name = 'UVGC_142_28',
                       value = 'FRCTdeltaZxddRxdG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_143_29 = Coupling(name = 'UVGC_143_29',
                       value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*sw)/(3.*cw)',
                       order = {'QCD':2,'QED':1})

UVGC_144_30 = Coupling(name = 'UVGC_144_30',
                       value = '-(FRCTdeltaZxssLxsG*complex(0,1))',
                       order = {'QCD':2})

UVGC_145_31 = Coupling(name = 'UVGC_145_31',
                       value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_146_32 = Coupling(name = 'UVGC_146_32',
                       value = '-(FRCTdeltaZxssRxsG*complex(0,1))',
                       order = {'QCD':2})

UVGC_147_33 = Coupling(name = 'UVGC_147_33',
                       value = '-(ee*FRCTdeltaZxssRxsG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_148_34 = Coupling(name = 'UVGC_148_34',
                       value = 'FRCTdeltaZxssLxsG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_149_35 = Coupling(name = 'UVGC_149_35',
                       value = 'FRCTdeltaZxssRxsG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_150_36 = Coupling(name = 'UVGC_150_36',
                       value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*sw)/(3.*cw)',
                       order = {'QCD':2,'QED':1})

UVGC_151_37 = Coupling(name = 'UVGC_151_37',
                       value = '-(FRCTdeltaZxttLxtG*complex(0,1))',
                       order = {'QCD':2})

UVGC_152_38 = Coupling(name = 'UVGC_152_38',
                       value = '(2*ee*FRCTdeltaZxttLxtG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_153_39 = Coupling(name = 'UVGC_153_39',
                       value = '-(FRCTdeltaZxttRxtG*complex(0,1))',
                       order = {'QCD':2})

UVGC_154_40 = Coupling(name = 'UVGC_154_40',
                       value = '(2*ee*FRCTdeltaZxttRxtG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_155_41 = Coupling(name = 'UVGC_155_41',
                       value = 'FRCTdeltaZxttLxtG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_156_42 = Coupling(name = 'UVGC_156_42',
                       value = 'FRCTdeltaZxttRxtG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_157_43 = Coupling(name = 'UVGC_157_43',
                       value = '(-2*ee*FRCTdeltaZxttRxtG*complex(0,1)*sw)/(3.*cw)',
                       order = {'QCD':2,'QED':1})

UVGC_158_44 = Coupling(name = 'UVGC_158_44',
                       value = '-(FRCTdeltaZxtptpLxtpG*complex(0,1))',
                       order = {'QCD':2})

UVGC_159_45 = Coupling(name = 'UVGC_159_45',
                       value = '(2*ee*FRCTdeltaZxtptpLxtpG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_160_46 = Coupling(name = 'UVGC_160_46',
                       value = '-(FRCTdeltaZxtptpRxtpG*complex(0,1))',
                       order = {'QCD':2})

UVGC_161_47 = Coupling(name = 'UVGC_161_47',
                       value = '(2*ee*FRCTdeltaZxtptpRxtpG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_162_48 = Coupling(name = 'UVGC_162_48',
                       value = 'FRCTdeltaZxtptpLxtpG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_163_49 = Coupling(name = 'UVGC_163_49',
                       value = 'FRCTdeltaZxtptpRxtpG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_164_50 = Coupling(name = 'UVGC_164_50',
                       value = '-(FRCTdeltaZxuuLxuG*complex(0,1))',
                       order = {'QCD':2})

UVGC_165_51 = Coupling(name = 'UVGC_165_51',
                       value = '(2*ee*FRCTdeltaZxuuLxuG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_166_52 = Coupling(name = 'UVGC_166_52',
                       value = '-(FRCTdeltaZxuuRxuG*complex(0,1))',
                       order = {'QCD':2})

UVGC_167_53 = Coupling(name = 'UVGC_167_53',
                       value = '(2*ee*FRCTdeltaZxuuRxuG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_168_54 = Coupling(name = 'UVGC_168_54',
                       value = 'FRCTdeltaZxuuLxuG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_169_55 = Coupling(name = 'UVGC_169_55',
                       value = 'FRCTdeltaZxuuRxuG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_170_56 = Coupling(name = 'UVGC_170_56',
                       value = '-(FRCTdeltaZxxxLxxG*complex(0,1))',
                       order = {'QCD':2})

UVGC_171_57 = Coupling(name = 'UVGC_171_57',
                       value = '(5*ee*FRCTdeltaZxxxLxxG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_172_58 = Coupling(name = 'UVGC_172_58',
                       value = '-(FRCTdeltaZxxxRxxG*complex(0,1))',
                       order = {'QCD':2})

UVGC_173_59 = Coupling(name = 'UVGC_173_59',
                       value = '(5*ee*FRCTdeltaZxxxRxxG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_174_60 = Coupling(name = 'UVGC_174_60',
                       value = 'FRCTdeltaZxxxLxxG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_175_61 = Coupling(name = 'UVGC_175_61',
                       value = 'FRCTdeltaZxxxRxxG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_176_62 = Coupling(name = 'UVGC_176_62',
                       value = '-(FRCTdeltaZxyyLxyG*complex(0,1))',
                       order = {'QCD':2})

UVGC_177_63 = Coupling(name = 'UVGC_177_63',
                       value = '(-4*ee*FRCTdeltaZxyyLxyG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_178_64 = Coupling(name = 'UVGC_178_64',
                       value = '-(FRCTdeltaZxyyRxyG*complex(0,1))',
                       order = {'QCD':2})

UVGC_179_65 = Coupling(name = 'UVGC_179_65',
                       value = '(-4*ee*FRCTdeltaZxyyRxyG*complex(0,1))/3.',
                       order = {'QCD':2,'QED':1})

UVGC_180_66 = Coupling(name = 'UVGC_180_66',
                       value = 'FRCTdeltaZxyyLxyG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_181_67 = Coupling(name = 'UVGC_181_67',
                       value = 'FRCTdeltaZxyyRxyG*complex(0,1)*G',
                       order = {'QCD':3})

UVGC_182_68 = Coupling(name = 'UVGC_182_68',
                       value = '(3*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_182_69 = Coupling(name = 'UVGC_182_69',
                       value = '(-3*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_183_70 = Coupling(name = 'UVGC_183_70',
                       value = '(3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_183_71 = Coupling(name = 'UVGC_183_71',
                       value = '(-3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_185_72 = Coupling(name = 'UVGC_185_72',
                       value = '-(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_185_73 = Coupling(name = 'UVGC_185_73',
                       value = '(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_186_74 = Coupling(name = 'UVGC_186_74',
                       value = '(-3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_186_75 = Coupling(name = 'UVGC_186_75',
                       value = '(3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_187_76 = Coupling(name = 'UVGC_187_76',
                       value = '(FRCTdeltaxaSxb*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_77 = Coupling(name = 'UVGC_187_77',
                       value = '(FRCTdeltaxaSxbp*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxbp*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_78 = Coupling(name = 'UVGC_187_78',
                       value = '(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_79 = Coupling(name = 'UVGC_187_79',
                       value = '(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_80 = Coupling(name = 'UVGC_187_80',
                       value = '(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_81 = Coupling(name = 'UVGC_187_81',
                       value = '(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_82 = Coupling(name = 'UVGC_187_82',
                       value = '(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_83 = Coupling(name = 'UVGC_187_83',
                       value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_84 = Coupling(name = 'UVGC_187_84',
                       value = '(FRCTdeltaxaSxtp*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxtp*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_85 = Coupling(name = 'UVGC_187_85',
                       value = '(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_86 = Coupling(name = 'UVGC_187_86',
                       value = '(FRCTdeltaxaSxx*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxx*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_187_87 = Coupling(name = 'UVGC_187_87',
                       value = '(FRCTdeltaxaSxy*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxy*complex(0,1)*G)/2.',
                       order = {'QCD':3})

UVGC_188_88 = Coupling(name = 'UVGC_188_88',
                       value = '-((FRCTdeltaxaSxb*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_89 = Coupling(name = 'UVGC_188_89',
                       value = '-((FRCTdeltaxaSxbp*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxbp*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_90 = Coupling(name = 'UVGC_188_90',
                       value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_91 = Coupling(name = 'UVGC_188_91',
                       value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_92 = Coupling(name = 'UVGC_188_92',
                       value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (31*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_188_93 = Coupling(name = 'UVGC_188_93',
                       value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_188_94 = Coupling(name = 'UVGC_188_94',
                       value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_95 = Coupling(name = 'UVGC_188_95',
                       value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_96 = Coupling(name = 'UVGC_188_96',
                       value = '-((FRCTdeltaxaSxtp*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxtp*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_97 = Coupling(name = 'UVGC_188_97',
                       value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_98 = Coupling(name = 'UVGC_188_98',
                       value = '-((FRCTdeltaxaSxx*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxx*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_188_99 = Coupling(name = 'UVGC_188_99',
                       value = '-((FRCTdeltaxaSxy*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxy*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_189_100 = Coupling(name = 'UVGC_189_100',
                        value = '(FRCTdeltaxaSxb*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_101 = Coupling(name = 'UVGC_189_101',
                        value = '(FRCTdeltaxaSxbp*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxbp*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_102 = Coupling(name = 'UVGC_189_102',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_103 = Coupling(name = 'UVGC_189_103',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_104 = Coupling(name = 'UVGC_189_104',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (37*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_189_105 = Coupling(name = 'UVGC_189_105',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_189_106 = Coupling(name = 'UVGC_189_106',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_107 = Coupling(name = 'UVGC_189_107',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_108 = Coupling(name = 'UVGC_189_108',
                        value = '(FRCTdeltaxaSxtp*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxtp*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_109 = Coupling(name = 'UVGC_189_109',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_110 = Coupling(name = 'UVGC_189_110',
                        value = '(FRCTdeltaxaSxx*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxx*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_189_111 = Coupling(name = 'UVGC_189_111',
                        value = '(FRCTdeltaxaSxy*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxy*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_190_112 = Coupling(name = 'UVGC_190_112',
                        value = 'FRCTdeltaZxGGxb*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_113 = Coupling(name = 'UVGC_190_113',
                        value = 'FRCTdeltaZxGGxbp*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_114 = Coupling(name = 'UVGC_190_114',
                        value = 'FRCTdeltaZxGGxc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_115 = Coupling(name = 'UVGC_190_115',
                        value = 'FRCTdeltaZxGGxd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_116 = Coupling(name = 'UVGC_190_116',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)',
                        order = {'QCD':2})

UVGC_190_117 = Coupling(name = 'UVGC_190_117',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)',
                        order = {'QCD':2})

UVGC_190_118 = Coupling(name = 'UVGC_190_118',
                        value = 'FRCTdeltaZxGGxs*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_119 = Coupling(name = 'UVGC_190_119',
                        value = 'FRCTdeltaZxGGxt*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_120 = Coupling(name = 'UVGC_190_120',
                        value = 'FRCTdeltaZxGGxtp*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_121 = Coupling(name = 'UVGC_190_121',
                        value = 'FRCTdeltaZxGGxu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_122 = Coupling(name = 'UVGC_190_122',
                        value = 'FRCTdeltaZxGGxx*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_190_123 = Coupling(name = 'UVGC_190_123',
                        value = 'FRCTdeltaZxGGxy*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_191_124 = Coupling(name = 'UVGC_191_124',
                        value = '-(FRCTdeltaxaSxb*G)/(2.*aS) - (3*FRCTdeltaZxGGxb*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_125 = Coupling(name = 'UVGC_191_125',
                        value = '-(FRCTdeltaxaSxbp*G)/(2.*aS) - (3*FRCTdeltaZxGGxbp*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_126 = Coupling(name = 'UVGC_191_126',
                        value = '(-3*FRCTdeltaZxGGxc*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_127 = Coupling(name = 'UVGC_191_127',
                        value = '(-3*FRCTdeltaZxGGxd*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_128 = Coupling(name = 'UVGC_191_128',
                        value = '(-3*FRCTdeltaZxGGxG*G)/2. - (15*G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_129 = Coupling(name = 'UVGC_191_129',
                        value = '(-3*FRCTdeltaZxGGxghG*G)/2. - (G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_130 = Coupling(name = 'UVGC_191_130',
                        value = '(-3*FRCTdeltaZxGGxs*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_131 = Coupling(name = 'UVGC_191_131',
                        value = '-(FRCTdeltaxaSxt*G)/(2.*aS) - (3*FRCTdeltaZxGGxt*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_132 = Coupling(name = 'UVGC_191_132',
                        value = '-(FRCTdeltaxaSxtp*G)/(2.*aS) - (3*FRCTdeltaZxGGxtp*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_133 = Coupling(name = 'UVGC_191_133',
                        value = '(-3*FRCTdeltaZxGGxu*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_134 = Coupling(name = 'UVGC_191_134',
                        value = '-(FRCTdeltaxaSxx*G)/(2.*aS) - (3*FRCTdeltaZxGGxx*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_135 = Coupling(name = 'UVGC_191_135',
                        value = '-(FRCTdeltaxaSxy*G)/(2.*aS) - (3*FRCTdeltaZxGGxy*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_192_136 = Coupling(name = 'UVGC_192_136',
                        value = '-(complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_193_137 = Coupling(name = 'UVGC_193_137',
                        value = '(5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_138 = Coupling(name = 'UVGC_194_138',
                        value = '(FRCTdeltaxaSxb*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxb*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_139 = Coupling(name = 'UVGC_194_139',
                        value = '(FRCTdeltaxaSxbp*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxbp*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_140 = Coupling(name = 'UVGC_194_140',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_141 = Coupling(name = 'UVGC_194_141',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_142 = Coupling(name = 'UVGC_194_142',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_143 = Coupling(name = 'UVGC_194_143',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_144 = Coupling(name = 'UVGC_194_144',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_145 = Coupling(name = 'UVGC_194_145',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_146 = Coupling(name = 'UVGC_194_146',
                        value = '(FRCTdeltaxaSxtp*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxtp*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_147 = Coupling(name = 'UVGC_194_147',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_148 = Coupling(name = 'UVGC_194_148',
                        value = '(FRCTdeltaxaSxx*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxx*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_194_149 = Coupling(name = 'UVGC_194_149',
                        value = '(FRCTdeltaxaSxy*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxy*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_195_150 = Coupling(name = 'UVGC_195_150',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_151 = Coupling(name = 'UVGC_196_151',
                        value = '-((FRCTdeltaxaSxb*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxb*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_152 = Coupling(name = 'UVGC_196_152',
                        value = '-((FRCTdeltaxaSxbp*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxbp*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_153 = Coupling(name = 'UVGC_196_153',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_154 = Coupling(name = 'UVGC_196_154',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_155 = Coupling(name = 'UVGC_196_155',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_156 = Coupling(name = 'UVGC_196_156',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_157 = Coupling(name = 'UVGC_196_157',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_158 = Coupling(name = 'UVGC_196_158',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_159 = Coupling(name = 'UVGC_196_159',
                        value = '-((FRCTdeltaxaSxtp*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxtp*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_160 = Coupling(name = 'UVGC_196_160',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_161 = Coupling(name = 'UVGC_196_161',
                        value = '-((FRCTdeltaxaSxx*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxx*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_196_162 = Coupling(name = 'UVGC_196_162',
                        value = '-((FRCTdeltaxaSxy*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxy*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_197_163 = Coupling(name = 'UVGC_197_163',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_198_164 = Coupling(name = 'UVGC_198_164',
                        value = '(complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_199_165 = Coupling(name = 'UVGC_199_165',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_200_166 = Coupling(name = 'UVGC_200_166',
                        value = '(-13*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_201_167 = Coupling(name = 'UVGC_201_167',
                        value = '-(FRCTdeltaxMBxbG*complex(0,1)) - (FRCTdeltaZxbbLxbG*complex(0,1)*MB)/2. - (FRCTdeltaZxbbRxbG*complex(0,1)*MB)/2. + (complex(0,1)*G**2*invFREps*MB)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_202_168 = Coupling(name = 'UVGC_202_168',
                        value = '-(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_203_169 = Coupling(name = 'UVGC_203_169',
                        value = '(ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_204_170 = Coupling(name = 'UVGC_204_170',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1)*yb)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxbbRxbG*complex(0,1)*yb)/(2.*cmath.sqrt(2)) + (complex(0,1)*G**2*invFREps*yb)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMBxbG*complex(0,1)*yb)/(MB*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_208_171 = Coupling(name = 'UVGC_208_171',
                        value = '-(FRCTdeltaxMBPxbpG*complex(0,1)) - (FRCTdeltaZxbpbpLxbpG*complex(0,1)*MBP)/2. - (FRCTdeltaZxbpbpRxbpG*complex(0,1)*MBP)/2. + (complex(0,1)*G**2*invFREps*MBP)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_210_172 = Coupling(name = 'UVGC_210_172',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_212_173 = Coupling(name = 'UVGC_212_173',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_217_174 = Coupling(name = 'UVGC_217_174',
                        value = '-(cw*ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_222_175 = Coupling(name = 'UVGC_222_175',
                        value = '-(cw*ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_227_176 = Coupling(name = 'UVGC_227_176',
                        value = '-(FRCTdeltaxMTxtG*complex(0,1)) - (FRCTdeltaZxttLxtG*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxtG*complex(0,1)*MT)/2. + (complex(0,1)*G**2*invFREps*MT)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_228_177 = Coupling(name = 'UVGC_228_177',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_230_178 = Coupling(name = 'UVGC_230_178',
                        value = '-(FRCTdeltaZxttLxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) + (complex(0,1)*G**2*invFREps*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMTxtG*complex(0,1)*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_234_179 = Coupling(name = 'UVGC_234_179',
                        value = '-(FRCTdeltaxMTPxtpG*complex(0,1)) - (FRCTdeltaZxtptpLxtpG*complex(0,1)*MTP)/2. - (FRCTdeltaZxtptpRxtpG*complex(0,1)*MTP)/2. + (complex(0,1)*G**2*invFREps*MTP)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_238_180 = Coupling(name = 'UVGC_238_180',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_239_181 = Coupling(name = 'UVGC_239_181',
                        value = '(-2*ee*FRCTdeltaZxuuRxuG*complex(0,1)*sw)/(3.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_241_182 = Coupling(name = 'UVGC_241_182',
                        value = '(-5*ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_243_183 = Coupling(name = 'UVGC_243_183',
                        value = '-(FRCTdeltaxMXxxG*complex(0,1)) - (FRCTdeltaZxxxLxxG*complex(0,1)*MX)/2. - (FRCTdeltaZxxxRxxG*complex(0,1)*MX)/2. + (complex(0,1)*G**2*invFREps*MX)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_245_184 = Coupling(name = 'UVGC_245_184',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_247_185 = Coupling(name = 'UVGC_247_185',
                        value = '-(FRCTdeltaxMYxyG*complex(0,1)) - (FRCTdeltaZxyyLxyG*complex(0,1)*MY)/2. - (FRCTdeltaZxyyRxyG*complex(0,1)*MY)/2. + (complex(0,1)*G**2*invFREps*MY)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_248_186 = Coupling(name = 'UVGC_248_186',
                        value = '(FRCTdeltaZxbbLxbG*complex(0,1)*KBLh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_248_187 = Coupling(name = 'UVGC_248_187',
                        value = '(FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBLh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_248_188 = Coupling(name = 'UVGC_248_188',
                        value = '-(complex(0,1)*G**2*invFREps*KBLh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_249_189 = Coupling(name = 'UVGC_249_189',
                        value = '(FRCTdeltaZxbbRxbG*complex(0,1)*KBRh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_249_190 = Coupling(name = 'UVGC_249_190',
                        value = '(FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBRh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_249_191 = Coupling(name = 'UVGC_249_191',
                        value = '-(complex(0,1)*G**2*invFREps*KBRh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_250_192 = Coupling(name = 'UVGC_250_192',
                        value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1)*KBLz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_250_193 = Coupling(name = 'UVGC_250_193',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_250_194 = Coupling(name = 'UVGC_250_194',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLz3)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_251_195 = Coupling(name = 'UVGC_251_195',
                        value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*KBRz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_251_196 = Coupling(name = 'UVGC_251_196',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_251_197 = Coupling(name = 'UVGC_251_197',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRz3)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_252_198 = Coupling(name = 'UVGC_252_198',
                        value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1)*KTLw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_252_199 = Coupling(name = 'UVGC_252_199',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_252_200 = Coupling(name = 'UVGC_252_200',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLw3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_253_201 = Coupling(name = 'UVGC_253_201',
                        value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*KTRw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_253_202 = Coupling(name = 'UVGC_253_202',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_253_203 = Coupling(name = 'UVGC_253_203',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRw3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_254_204 = Coupling(name = 'UVGC_254_204',
                        value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1)*KYL3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_254_205 = Coupling(name = 'UVGC_254_205',
                        value = '(ee*FRCTdeltaZxyyLxyG*complex(0,1)*KYL3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_254_206 = Coupling(name = 'UVGC_254_206',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYL3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_255_207 = Coupling(name = 'UVGC_255_207',
                        value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*KYR3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_255_208 = Coupling(name = 'UVGC_255_208',
                        value = '(ee*FRCTdeltaZxyyRxyG*complex(0,1)*KYR3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_255_209 = Coupling(name = 'UVGC_255_209',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYR3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_256_210 = Coupling(name = 'UVGC_256_210',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_256_211 = Coupling(name = 'UVGC_256_211',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1)*KBLw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_256_212 = Coupling(name = 'UVGC_256_212',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLw2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_257_213 = Coupling(name = 'UVGC_257_213',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_257_214 = Coupling(name = 'UVGC_257_214',
                        value = '(ee*FRCTdeltaZxccRxcG*complex(0,1)*KBRw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_257_215 = Coupling(name = 'UVGC_257_215',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRw2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_258_216 = Coupling(name = 'UVGC_258_216',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_258_217 = Coupling(name = 'UVGC_258_217',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_258_218 = Coupling(name = 'UVGC_258_218',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_259_219 = Coupling(name = 'UVGC_259_219',
                        value = '(FRCTdeltaZxccLxcG*complex(0,1)*KTLh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_259_220 = Coupling(name = 'UVGC_259_220',
                        value = '(FRCTdeltaZxtptpRxtpG*complex(0,1)*KTLh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_259_221 = Coupling(name = 'UVGC_259_221',
                        value = '-(complex(0,1)*G**2*invFREps*KTLh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_260_222 = Coupling(name = 'UVGC_260_222',
                        value = '(FRCTdeltaZxccRxcG*complex(0,1)*KTRh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_260_223 = Coupling(name = 'UVGC_260_223',
                        value = '(FRCTdeltaZxtptpLxtpG*complex(0,1)*KTRh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_260_224 = Coupling(name = 'UVGC_260_224',
                        value = '-(complex(0,1)*G**2*invFREps*KTRh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_261_225 = Coupling(name = 'UVGC_261_225',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1)*KTLz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_261_226 = Coupling(name = 'UVGC_261_226',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_261_227 = Coupling(name = 'UVGC_261_227',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLz2)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_262_228 = Coupling(name = 'UVGC_262_228',
                        value = '(ee*FRCTdeltaZxccRxcG*complex(0,1)*KTRz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_262_229 = Coupling(name = 'UVGC_262_229',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_262_230 = Coupling(name = 'UVGC_262_230',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRz2)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_263_231 = Coupling(name = 'UVGC_263_231',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1)*KXL2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_263_232 = Coupling(name = 'UVGC_263_232',
                        value = '(ee*FRCTdeltaZxxxLxxG*complex(0,1)*KXL2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_263_233 = Coupling(name = 'UVGC_263_233',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXL2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_264_234 = Coupling(name = 'UVGC_264_234',
                        value = '(ee*FRCTdeltaZxccRxcG*complex(0,1)*KXR2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_264_235 = Coupling(name = 'UVGC_264_235',
                        value = '(ee*FRCTdeltaZxxxRxxG*complex(0,1)*KXR2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_264_236 = Coupling(name = 'UVGC_264_236',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXR2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_265_237 = Coupling(name = 'UVGC_265_237',
                        value = '(FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBLh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_265_238 = Coupling(name = 'UVGC_265_238',
                        value = '(FRCTdeltaZxddLxdG*complex(0,1)*KBLh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_265_239 = Coupling(name = 'UVGC_265_239',
                        value = '-(complex(0,1)*G**2*invFREps*KBLh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_266_240 = Coupling(name = 'UVGC_266_240',
                        value = '(FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBRh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_266_241 = Coupling(name = 'UVGC_266_241',
                        value = '(FRCTdeltaZxddRxdG*complex(0,1)*KBRh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_266_242 = Coupling(name = 'UVGC_266_242',
                        value = '-(complex(0,1)*G**2*invFREps*KBRh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_267_243 = Coupling(name = 'UVGC_267_243',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_267_244 = Coupling(name = 'UVGC_267_244',
                        value = '(ee*FRCTdeltaZxddLxdG*complex(0,1)*KBLz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_267_245 = Coupling(name = 'UVGC_267_245',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLz1)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_268_246 = Coupling(name = 'UVGC_268_246',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_268_247 = Coupling(name = 'UVGC_268_247',
                        value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*KBRz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_268_248 = Coupling(name = 'UVGC_268_248',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRz1)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_269_249 = Coupling(name = 'UVGC_269_249',
                        value = '(ee*FRCTdeltaZxddLxdG*complex(0,1)*KTLw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_269_250 = Coupling(name = 'UVGC_269_250',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_269_251 = Coupling(name = 'UVGC_269_251',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLw1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_270_252 = Coupling(name = 'UVGC_270_252',
                        value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*KTRw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_270_253 = Coupling(name = 'UVGC_270_253',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_270_254 = Coupling(name = 'UVGC_270_254',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRw1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_271_255 = Coupling(name = 'UVGC_271_255',
                        value = '(ee*FRCTdeltaZxddLxdG*complex(0,1)*KYL1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_271_256 = Coupling(name = 'UVGC_271_256',
                        value = '(ee*FRCTdeltaZxyyLxyG*complex(0,1)*KYL1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_271_257 = Coupling(name = 'UVGC_271_257',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYL1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_272_258 = Coupling(name = 'UVGC_272_258',
                        value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*KYR1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_272_259 = Coupling(name = 'UVGC_272_259',
                        value = '(ee*FRCTdeltaZxyyRxyG*complex(0,1)*KYR1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_272_260 = Coupling(name = 'UVGC_272_260',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYR1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_273_261 = Coupling(name = 'UVGC_273_261',
                        value = '(FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBLh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_273_262 = Coupling(name = 'UVGC_273_262',
                        value = '(FRCTdeltaZxssLxsG*complex(0,1)*KBLh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_273_263 = Coupling(name = 'UVGC_273_263',
                        value = '-(complex(0,1)*G**2*invFREps*KBLh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_274_264 = Coupling(name = 'UVGC_274_264',
                        value = '(FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBRh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_274_265 = Coupling(name = 'UVGC_274_265',
                        value = '(FRCTdeltaZxssRxsG*complex(0,1)*KBRh2)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_274_266 = Coupling(name = 'UVGC_274_266',
                        value = '-(complex(0,1)*G**2*invFREps*KBRh2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_275_267 = Coupling(name = 'UVGC_275_267',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_275_268 = Coupling(name = 'UVGC_275_268',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1)*KBLz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_275_269 = Coupling(name = 'UVGC_275_269',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLz2)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_276_270 = Coupling(name = 'UVGC_276_270',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_276_271 = Coupling(name = 'UVGC_276_271',
                        value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*KBRz2)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_276_272 = Coupling(name = 'UVGC_276_272',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRz2)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_277_273 = Coupling(name = 'UVGC_277_273',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1)*KTLw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_277_274 = Coupling(name = 'UVGC_277_274',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_277_275 = Coupling(name = 'UVGC_277_275',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLw2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_278_276 = Coupling(name = 'UVGC_278_276',
                        value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*KTRw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_278_277 = Coupling(name = 'UVGC_278_277',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRw2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_278_278 = Coupling(name = 'UVGC_278_278',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRw2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_279_279 = Coupling(name = 'UVGC_279_279',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1)*KYL2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_279_280 = Coupling(name = 'UVGC_279_280',
                        value = '(ee*FRCTdeltaZxyyLxyG*complex(0,1)*KYL2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_279_281 = Coupling(name = 'UVGC_279_281',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYL2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_280_282 = Coupling(name = 'UVGC_280_282',
                        value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*KYR2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_280_283 = Coupling(name = 'UVGC_280_283',
                        value = '(ee*FRCTdeltaZxyyRxyG*complex(0,1)*KYR2)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_280_284 = Coupling(name = 'UVGC_280_284',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KYR2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_281_285 = Coupling(name = 'UVGC_281_285',
                        value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_281_286 = Coupling(name = 'UVGC_281_286',
                        value = '(ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_282_287 = Coupling(name = 'UVGC_282_287',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_282_288 = Coupling(name = 'UVGC_282_288',
                        value = '(ee*FRCTdeltaZxttLxtG*complex(0,1)*KBLw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_282_289 = Coupling(name = 'UVGC_282_289',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLw3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_283_290 = Coupling(name = 'UVGC_283_290',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_283_291 = Coupling(name = 'UVGC_283_291',
                        value = '(ee*FRCTdeltaZxttRxtG*complex(0,1)*KBRw3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_283_292 = Coupling(name = 'UVGC_283_292',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRw3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_284_293 = Coupling(name = 'UVGC_284_293',
                        value = '(FRCTdeltaZxttLxtG*complex(0,1)*KTLh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_284_294 = Coupling(name = 'UVGC_284_294',
                        value = '(FRCTdeltaZxtptpRxtpG*complex(0,1)*KTLh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_284_295 = Coupling(name = 'UVGC_284_295',
                        value = '-(complex(0,1)*G**2*invFREps*KTLh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_285_296 = Coupling(name = 'UVGC_285_296',
                        value = '(FRCTdeltaZxttRxtG*complex(0,1)*KTRh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_285_297 = Coupling(name = 'UVGC_285_297',
                        value = '(FRCTdeltaZxtptpLxtpG*complex(0,1)*KTRh3)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_285_298 = Coupling(name = 'UVGC_285_298',
                        value = '-(complex(0,1)*G**2*invFREps*KTRh3)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_286_299 = Coupling(name = 'UVGC_286_299',
                        value = '(ee*FRCTdeltaZxttLxtG*complex(0,1)*KTLz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_286_300 = Coupling(name = 'UVGC_286_300',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_286_301 = Coupling(name = 'UVGC_286_301',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLz3)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_287_302 = Coupling(name = 'UVGC_287_302',
                        value = '(ee*FRCTdeltaZxttRxtG*complex(0,1)*KTRz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_287_303 = Coupling(name = 'UVGC_287_303',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRz3)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_287_304 = Coupling(name = 'UVGC_287_304',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRz3)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_288_305 = Coupling(name = 'UVGC_288_305',
                        value = '(ee*FRCTdeltaZxttLxtG*complex(0,1)*KXL3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_288_306 = Coupling(name = 'UVGC_288_306',
                        value = '(ee*FRCTdeltaZxxxLxxG*complex(0,1)*KXL3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_288_307 = Coupling(name = 'UVGC_288_307',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXL3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_289_308 = Coupling(name = 'UVGC_289_308',
                        value = '(ee*FRCTdeltaZxttRxtG*complex(0,1)*KXR3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_289_309 = Coupling(name = 'UVGC_289_309',
                        value = '(ee*FRCTdeltaZxxxRxxG*complex(0,1)*KXR3)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_289_310 = Coupling(name = 'UVGC_289_310',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXR3)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_290_311 = Coupling(name = 'UVGC_290_311',
                        value = '(ee*FRCTdeltaZxbpbpLxbpG*complex(0,1)*KBLw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_290_312 = Coupling(name = 'UVGC_290_312',
                        value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1)*KBLw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_290_313 = Coupling(name = 'UVGC_290_313',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBLw1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_291_314 = Coupling(name = 'UVGC_291_314',
                        value = '(ee*FRCTdeltaZxbpbpRxbpG*complex(0,1)*KBRw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_291_315 = Coupling(name = 'UVGC_291_315',
                        value = '(ee*FRCTdeltaZxuuRxuG*complex(0,1)*KBRw1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_291_316 = Coupling(name = 'UVGC_291_316',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KBRw1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'VLQ':1})

UVGC_292_317 = Coupling(name = 'UVGC_292_317',
                        value = '(ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_292_318 = Coupling(name = 'UVGC_292_318',
                        value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_293_319 = Coupling(name = 'UVGC_293_319',
                        value = '(FRCTdeltaZxtptpRxtpG*complex(0,1)*KTLh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_293_320 = Coupling(name = 'UVGC_293_320',
                        value = '(FRCTdeltaZxuuLxuG*complex(0,1)*KTLh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_293_321 = Coupling(name = 'UVGC_293_321',
                        value = '-(complex(0,1)*G**2*invFREps*KTLh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_294_322 = Coupling(name = 'UVGC_294_322',
                        value = '(FRCTdeltaZxtptpLxtpG*complex(0,1)*KTRh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_294_323 = Coupling(name = 'UVGC_294_323',
                        value = '(FRCTdeltaZxuuRxuG*complex(0,1)*KTRh1)/2.',
                        order = {'QCD':2,'VLQ':1})

UVGC_294_324 = Coupling(name = 'UVGC_294_324',
                        value = '-(complex(0,1)*G**2*invFREps*KTRh1)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'VLQ':1})

UVGC_295_325 = Coupling(name = 'UVGC_295_325',
                        value = '(ee*FRCTdeltaZxtptpLxtpG*complex(0,1)*KTLz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_295_326 = Coupling(name = 'UVGC_295_326',
                        value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1)*KTLz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_295_327 = Coupling(name = 'UVGC_295_327',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTLz1)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_296_328 = Coupling(name = 'UVGC_296_328',
                        value = '(ee*FRCTdeltaZxtptpRxtpG*complex(0,1)*KTRz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_296_329 = Coupling(name = 'UVGC_296_329',
                        value = '(ee*FRCTdeltaZxuuRxuG*complex(0,1)*KTRz1)/(4.*cw*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_296_330 = Coupling(name = 'UVGC_296_330',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KTRz1)/(24.*cw*cmath.pi**2*sw)',
                        order = {'QCD':2,'VLQ':1})

UVGC_297_331 = Coupling(name = 'UVGC_297_331',
                        value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1)*KXL1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_297_332 = Coupling(name = 'UVGC_297_332',
                        value = '(ee*FRCTdeltaZxxxLxxG*complex(0,1)*KXL1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_297_333 = Coupling(name = 'UVGC_297_333',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXL1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_298_334 = Coupling(name = 'UVGC_298_334',
                        value = '(ee*FRCTdeltaZxuuRxuG*complex(0,1)*KXR1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_298_335 = Coupling(name = 'UVGC_298_335',
                        value = '(ee*FRCTdeltaZxxxRxxG*complex(0,1)*KXR1)/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

UVGC_298_336 = Coupling(name = 'UVGC_298_336',
                        value = '-(ee*complex(0,1)*G**2*invFREps*KXR1)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1,'VLQ':1})

