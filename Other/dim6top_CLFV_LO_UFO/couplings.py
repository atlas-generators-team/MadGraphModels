# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 15 Nov 2019 18:52:48


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-6*complex(0,1)*lam',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(cQeIx12x31/Lambda**2) + (cQex12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'cQeIx12x31/Lambda**2 + (cQex12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(cQeIx12x32/Lambda**2) + (cQex12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'cQeIx12x32/Lambda**2 + (cQex12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(cQeIx13x31/Lambda**2) + (cQex13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'cQeIx13x31/Lambda**2 + (cQex13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(cQeIx13x32/Lambda**2) + (cQex13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'cQeIx13x32/Lambda**2 + (cQex13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(cQeIx21x31/Lambda**2) + (cQex21x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'cQeIx21x31/Lambda**2 + (cQex21x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(cQeIx21x32/Lambda**2) + (cQex21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_21 = Coupling(name = 'GC_21',
                 value = 'cQeIx21x32/Lambda**2 + (cQex21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(cQeIx23x31/Lambda**2) + (cQex23x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'cQeIx23x31/Lambda**2 + (cQex23x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-(cQeIx23x32/Lambda**2) + (cQex23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'cQeIx23x32/Lambda**2 + (cQex23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-(cQeIx31x31/Lambda**2) + (cQex31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'cQeIx31x31/Lambda**2 + (cQex31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-(cQeIx31x32/Lambda**2) + (cQex31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'cQeIx31x32/Lambda**2 + (cQex31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-(cQeIx32x31/Lambda**2) + (cQex32x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'cQeIx32x31/Lambda**2 + (cQex32x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(cQeI323x32/Lambda**2) + (cQex32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'cQeI323x32/Lambda**2 + (cQex32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_eq':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(-2*CKM1x2*cQl3Ix12x31)/Lambda**2 - (2*CKM2x2*cQl3Ix12x32)/Lambda**2 + (2*CKM1x2*cQl3x12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(2*CKM3x1*cQl3Ix12x31)/Lambda**2 + (2*CKM3x1*cQl3x12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(2*CKM3x2*cQl3Ix12x31)/Lambda**2 + (2*CKM3x2*cQl3x12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(2*CKM3x3*cQl3Ix12x31)/Lambda**2 + (2*CKM3x3*cQl3x12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(-2*CKM1x1*cQl3Ix12x31)/Lambda**2 - (2*CKM2x1*cQl3Ix12x32)/Lambda**2 + (2*CKM1x1*cQl3x12x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(-2*CKM1x3*cQl3Ix12x31)/Lambda**2 - (2*CKM2x3*cQl3Ix12x32)/Lambda**2 + (2*CKM1x3*cQl3x12x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(2*CKM3x1*cQl3Ix12x32)/Lambda**2 + (2*CKM3x1*cQl3x12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '(2*CKM3x2*cQl3Ix12x32)/Lambda**2 + (2*CKM3x2*cQl3x12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(2*CKM3x3*cQl3Ix12x32)/Lambda**2 + (2*CKM3x3*cQl3x12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '(2*CKM3x1*cQl3Ix13x31)/Lambda**2 + (2*CKM3x1*cQl3x13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(2*CKM3x2*cQl3Ix13x31)/Lambda**2 + (2*CKM3x2*cQl3x13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(2*CKM3x3*cQl3Ix13x31)/Lambda**2 + (2*CKM3x3*cQl3x13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(-2*CKM1x1*cQl3Ix13x31)/Lambda**2 - (2*CKM2x1*cQl3Ix13x32)/Lambda**2 + (2*CKM1x1*cQl3x13x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(-2*CKM1x2*cQl3Ix13x31)/Lambda**2 - (2*CKM2x2*cQl3Ix13x32)/Lambda**2 + (2*CKM1x2*cQl3x13x31*complex(0,1))/Lambda**2 + (2*CKM2x2*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(-2*CKM1x3*cQl3Ix13x31)/Lambda**2 - (2*CKM2x3*cQl3Ix13x32)/Lambda**2 + (2*CKM1x3*cQl3x13x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(2*CKM3x1*cQl3Ix13x32)/Lambda**2 + (2*CKM3x1*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(2*CKM3x2*cQl3Ix13x32)/Lambda**2 + (2*CKM3x2*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(2*CKM3x3*cQl3Ix13x32)/Lambda**2 + (2*CKM3x3*cQl3x13x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(2*CKM3x1*cQl3Ix21x31)/Lambda**2 + (2*CKM3x1*cQl3x21x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(2*CKM3x2*cQl3Ix21x31)/Lambda**2 + (2*CKM3x2*cQl3x21x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(2*CKM3x3*cQl3Ix21x31)/Lambda**2 + (2*CKM3x3*cQl3x21x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(-2*CKM1x1*cQl3Ix21x31)/Lambda**2 - (2*CKM2x1*cQl3Ix21x32)/Lambda**2 + (2*CKM1x1*cQl3x21x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(-2*CKM1x2*cQl3Ix21x31)/Lambda**2 - (2*CKM2x2*cQl3Ix21x32)/Lambda**2 + (2*CKM1x2*cQl3x21x31*complex(0,1))/Lambda**2 + (2*CKM2x2*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(-2*CKM1x3*cQl3Ix21x31)/Lambda**2 - (2*CKM2x3*cQl3Ix21x32)/Lambda**2 + (2*CKM1x3*cQl3x21x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(2*CKM3x1*cQl3Ix21x32)/Lambda**2 + (2*CKM3x1*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(2*CKM3x2*cQl3Ix21x32)/Lambda**2 + (2*CKM3x2*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(2*CKM3x3*cQl3Ix21x32)/Lambda**2 + (2*CKM3x3*cQl3x21x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(2*CKM3x1*cQl3Ix23x31)/Lambda**2 + (2*CKM3x1*cQl3x23x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(2*CKM3x2*cQl3Ix23x31)/Lambda**2 + (2*CKM3x2*cQl3x23x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(2*CKM3x3*cQl3Ix23x31)/Lambda**2 + (2*CKM3x3*cQl3x23x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(-2*CKM1x1*cQl3Ix23x31)/Lambda**2 - (2*CKM2x1*cQl3Ix23x32)/Lambda**2 + (2*CKM1x1*cQl3x23x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(-2*CKM1x2*cQl3Ix23x31)/Lambda**2 - (2*CKM2x2*cQl3Ix23x32)/Lambda**2 + (2*CKM1x2*cQl3x23x31*complex(0,1))/Lambda**2 + (2*CKM2x2*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '(-2*CKM1x3*cQl3Ix23x31)/Lambda**2 - (2*CKM2x3*cQl3Ix23x32)/Lambda**2 + (2*CKM1x3*cQl3x23x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(2*CKM3x1*cQl3Ix23x32)/Lambda**2 + (2*CKM3x1*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(2*CKM3x2*cQl3Ix23x32)/Lambda**2 + (2*CKM3x2*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(2*CKM3x3*cQl3Ix23x32)/Lambda**2 + (2*CKM3x3*cQl3x23x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(2*CKM3x1*cQl3Ix31x31)/Lambda**2 + (2*CKM3x1*cQl3x31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(2*CKM3x2*cQl3Ix31x31)/Lambda**2 + (2*CKM3x2*cQl3x31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(2*CKM3x3*cQl3Ix31x31)/Lambda**2 + (2*CKM3x3*cQl3x31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(-2*CKM1x1*cQl3Ix31x31)/Lambda**2 - (2*CKM2x1*cQl3Ix31x32)/Lambda**2 + (2*CKM1x1*cQl3x31x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(-2*CKM1x2*cQl3Ix31x31)/Lambda**2 - (2*CKM2x2*cQl3Ix31x32)/Lambda**2 + (2*CKM1x2*cQl3x31x31*complex(0,1))/Lambda**2 + (2*CKM2x2*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(-2*CKM1x3*cQl3Ix31x31)/Lambda**2 - (2*CKM2x3*cQl3Ix31x32)/Lambda**2 + (2*CKM1x3*cQl3x31x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(2*CKM3x1*cQl3Ix31x32)/Lambda**2 + (2*CKM3x1*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(2*CKM3x2*cQl3Ix31x32)/Lambda**2 + (2*CKM3x2*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(2*CKM3x3*cQl3Ix31x32)/Lambda**2 + (2*CKM3x3*cQl3x31x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(2*CKM3x1*cQl3Ix32x31)/Lambda**2 + (2*CKM3x1*cQl3x32x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '(2*CKM3x2*cQl3Ix32x31)/Lambda**2 + (2*CKM3x2*cQl3x32x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(2*CKM3x3*cQl3Ix32x31)/Lambda**2 + (2*CKM3x3*cQl3x32x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(-2*CKM1x1*cQl3Ix32x31)/Lambda**2 - (2*CKM2x1*cQl3Ix32x32)/Lambda**2 + (2*CKM1x1*cQl3x32x31*complex(0,1))/Lambda**2 + (2*CKM2x1*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(-2*CKM1x2*cQl3Ix32x31)/Lambda**2 - (2*CKM2x2*cQl3Ix32x32)/Lambda**2 + (2*CKM1x2*cQl3x32x31*complex(0,1))/Lambda**2 + (2*CKM2x2*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '(-2*CKM1x3*cQl3Ix32x31)/Lambda**2 - (2*CKM2x3*cQl3Ix32x32)/Lambda**2 + (2*CKM1x3*cQl3x32x31*complex(0,1))/Lambda**2 + (2*CKM2x3*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(2*CKM3x1*cQl3Ix32x32)/Lambda**2 + (2*CKM3x1*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(2*CKM3x2*cQl3Ix32x32)/Lambda**2 + (2*CKM3x2*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(2*CKM3x3*cQl3Ix32x32)/Lambda**2 + (2*CKM3x3*cQl3x32x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '(2*cQl3Ix31x31)/Lambda**2 + cQlMIx31x31/Lambda**2 + (2*cQl3x31x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(cQlMIx12x31/Lambda**2) + (cQlMx12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(-2*cQl3Ix12x31)/Lambda**2 - cQlMIx12x31/Lambda**2 + (2*cQl3x12x31*complex(0,1))/Lambda**2 + (cQlMx12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_91 = Coupling(name = 'GC_91',
                 value = 'cQlMIx12x31/Lambda**2 + (cQlMx12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '(2*cQl3Ix12x31)/Lambda**2 + cQlMIx12x31/Lambda**2 + (2*cQl3x12x31*complex(0,1))/Lambda**2 + (cQlMx12x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-(cQlMIx12x32/Lambda**2) + (cQlMx12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(-2*cQl3Ix12x32)/Lambda**2 - cQlMIx12x32/Lambda**2 + (2*cQl3x12x32*complex(0,1))/Lambda**2 + (cQlMx12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_95 = Coupling(name = 'GC_95',
                 value = 'cQlMIx12x32/Lambda**2 + (cQlMx12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(2*cQl3Ix12x32)/Lambda**2 + cQlMIx12x32/Lambda**2 + (2*cQl3x12x32*complex(0,1))/Lambda**2 + (cQlMx12x32*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(cQlMIx13x31/Lambda**2) + (cQlMx13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(-2*cQl3Ix13x31)/Lambda**2 - cQlMIx13x31/Lambda**2 + (2*cQl3x13x31*complex(0,1))/Lambda**2 + (cQlMx13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_99 = Coupling(name = 'GC_99',
                 value = 'cQlMIx13x31/Lambda**2 + (cQlMx13x31*complex(0,1))/Lambda**2',
                 order = {'CLFV_Ql':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(2*cQl3Ix13x31)/Lambda**2 + cQlMIx13x31/Lambda**2 + (2*cQl3x13x31*complex(0,1))/Lambda**2 + (cQlMx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(cQlMIx13x32/Lambda**2) + (cQlMx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(-2*cQl3Ix13x32)/Lambda**2 - cQlMIx13x32/Lambda**2 + (2*cQl3x13x32*complex(0,1))/Lambda**2 + (cQlMx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_103 = Coupling(name = 'GC_103',
                  value = 'cQlMIx13x32/Lambda**2 + (cQlMx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(2*cQl3Ix13x32)/Lambda**2 + cQlMIx13x32/Lambda**2 + (2*cQl3x13x32*complex(0,1))/Lambda**2 + (cQlMx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(cQlMIx21x31/Lambda**2) + (cQlMx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(-2*cQl3Ix21x31)/Lambda**2 - cQlMIx21x31/Lambda**2 + (2*cQl3x21x31*complex(0,1))/Lambda**2 + (cQlMx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_107 = Coupling(name = 'GC_107',
                  value = 'cQlMIx21x31/Lambda**2 + (cQlMx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(2*cQl3Ix21x31)/Lambda**2 + cQlMIx21x31/Lambda**2 + (2*cQl3x21x31*complex(0,1))/Lambda**2 + (cQlMx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(cQlMIx21x32/Lambda**2) + (cQlMx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '(-2*cQl3Ix21x32)/Lambda**2 - cQlMIx21x32/Lambda**2 + (2*cQl3x21x32*complex(0,1))/Lambda**2 + (cQlMx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_111 = Coupling(name = 'GC_111',
                  value = 'cQlMIx21x32/Lambda**2 + (cQlMx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '(2*cQl3Ix21x32)/Lambda**2 + cQlMIx21x32/Lambda**2 + (2*cQl3x21x32*complex(0,1))/Lambda**2 + (cQlMx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(cQlMIx23x31/Lambda**2) + (cQlMx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(-2*cQl3Ix23x31)/Lambda**2 - cQlMIx23x31/Lambda**2 + (2*cQl3x23x31*complex(0,1))/Lambda**2 + (cQlMx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_115 = Coupling(name = 'GC_115',
                  value = 'cQlMIx23x31/Lambda**2 + (cQlMx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '(2*cQl3Ix23x31)/Lambda**2 + cQlMIx23x31/Lambda**2 + (2*cQl3x23x31*complex(0,1))/Lambda**2 + (cQlMx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-(cQlMIx23x32/Lambda**2) + (cQlMx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '(-2*cQl3Ix23x32)/Lambda**2 - cQlMIx23x32/Lambda**2 + (2*cQl3x23x32*complex(0,1))/Lambda**2 + (cQlMx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_119 = Coupling(name = 'GC_119',
                  value = 'cQlMIx23x32/Lambda**2 + (cQlMx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(2*cQl3Ix23x32)/Lambda**2 + cQlMIx23x32/Lambda**2 + (2*cQl3x23x32*complex(0,1))/Lambda**2 + (cQlMx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(cQlMIx31x31/Lambda**2) + (cQlMx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(-2*cQl3Ix31x31)/Lambda**2 - cQlMIx31x31/Lambda**2 + (2*cQl3x31x31*complex(0,1))/Lambda**2 + (cQlMx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_123 = Coupling(name = 'GC_123',
                  value = 'cQlMIx31x31/Lambda**2 + (cQlMx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(cQlMIx31x32/Lambda**2) + (cQlMx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(-2*cQl3Ix31x32)/Lambda**2 - cQlMIx31x32/Lambda**2 + (2*cQl3x31x32*complex(0,1))/Lambda**2 + (cQlMx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_126 = Coupling(name = 'GC_126',
                  value = 'cQlMIx31x32/Lambda**2 + (cQlMx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(2*cQl3Ix31x32)/Lambda**2 + cQlMIx31x32/Lambda**2 + (2*cQl3x31x32*complex(0,1))/Lambda**2 + (cQlMx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(cQlMIx32x31/Lambda**2) + (cQlMx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '(-2*cQl3Ix32x31)/Lambda**2 - cQlMIx32x31/Lambda**2 + (2*cQl3x32x31*complex(0,1))/Lambda**2 + (cQlMx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_130 = Coupling(name = 'GC_130',
                  value = 'cQlMIx32x31/Lambda**2 + (cQlMx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '(2*cQl3Ix32x31)/Lambda**2 + cQlMIx32x31/Lambda**2 + (2*cQl3x32x31*complex(0,1))/Lambda**2 + (cQlMx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(cQlMIx32x32/Lambda**2) + (cQlMx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(-2*cQl3Ix32x32)/Lambda**2 - cQlMIx32x32/Lambda**2 + (2*cQl3x32x32*complex(0,1))/Lambda**2 + (cQlMx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_134 = Coupling(name = 'GC_134',
                  value = 'cQlMIx32x32/Lambda**2 + (cQlMx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(2*cQl3Ix32x32)/Lambda**2 + cQlMIx32x32/Lambda**2 + (2*cQl3x32x32*complex(0,1))/Lambda**2 + (cQlMx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(cteIx12x31/Lambda**2) + (ctex12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_137 = Coupling(name = 'GC_137',
                  value = 'cteIx12x31/Lambda**2 + (ctex12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-(cteIx12x32/Lambda**2) + (ctex12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_139 = Coupling(name = 'GC_139',
                  value = 'cteIx12x32/Lambda**2 + (ctex12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(cteIx13x31/Lambda**2) + (ctex13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_141 = Coupling(name = 'GC_141',
                  value = 'cteIx13x31/Lambda**2 + (ctex13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(cteIx13x32/Lambda**2) + (ctex13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_143 = Coupling(name = 'GC_143',
                  value = 'cteIx13x32/Lambda**2 + (ctex13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(cteIx21x31/Lambda**2) + (ctex21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_145 = Coupling(name = 'GC_145',
                  value = 'cteIx21x31/Lambda**2 + (ctex21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(cteIx21x32/Lambda**2) + (ctex21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_147 = Coupling(name = 'GC_147',
                  value = 'cteIx21x32/Lambda**2 + (ctex21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(cteIx23x31/Lambda**2) + (ctex23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_149 = Coupling(name = 'GC_149',
                  value = 'cteIx23x31/Lambda**2 + (ctex23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(cteIx23x32/Lambda**2) + (ctex23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_151 = Coupling(name = 'GC_151',
                  value = 'cteIx23x32/Lambda**2 + (ctex23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(cteIx31x31/Lambda**2) + (ctex31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_153 = Coupling(name = 'GC_153',
                  value = 'cteIx31x31/Lambda**2 + (ctex31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(cteIx31x32/Lambda**2) + (ctex31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_155 = Coupling(name = 'GC_155',
                  value = 'cteIx31x32/Lambda**2 + (ctex31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(cteIx32x31/Lambda**2) + (ctex32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_157 = Coupling(name = 'GC_157',
                  value = 'cteIx32x31/Lambda**2 + (ctex32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(cteIx32x32/Lambda**2) + (ctex32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_159 = Coupling(name = 'GC_159',
                  value = 'cteIx32x32/Lambda**2 + (ctex32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_eu':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(ctlSIx12x13/Lambda**2) - (ctlSx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_161 = Coupling(name = 'GC_161',
                  value = 'ctlSIx12x13/Lambda**2 - (ctlSx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(ctlSIx12x23/Lambda**2) - (ctlSx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_163 = Coupling(name = 'GC_163',
                  value = 'ctlSIx12x23/Lambda**2 - (ctlSx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '(CKM1x1*ctlSIx12x13)/Lambda**2 + (CKM2x1*ctlSIx12x23)/Lambda**2 + (CKM1x1*ctlSx12x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '(CKM1x2*ctlSIx12x13)/Lambda**2 + (CKM2x2*ctlSIx12x23)/Lambda**2 + (CKM1x2*ctlSx12x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(CKM1x3*ctlSIx12x13)/Lambda**2 + (CKM2x3*ctlSIx12x23)/Lambda**2 + (CKM1x3*ctlSx12x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '-(ctlSIx12x31/Lambda**2) - (ctlSx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_168 = Coupling(name = 'GC_168',
                  value = 'ctlSIx12x31/Lambda**2 - (ctlSx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(CKM3x1*ctlSIx12x31)/Lambda**2 + (CKM3x1*ctlSx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(CKM3x2*ctlSIx12x31)/Lambda**2 + (CKM3x2*ctlSx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(CKM3x3*ctlSIx12x31)/Lambda**2 + (CKM3x3*ctlSx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-(ctlSIx12x32/Lambda**2) - (ctlSx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_173 = Coupling(name = 'GC_173',
                  value = 'ctlSIx12x32/Lambda**2 - (ctlSx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '(CKM3x1*ctlSIx12x32)/Lambda**2 + (CKM3x1*ctlSx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(CKM3x2*ctlSIx12x32)/Lambda**2 + (CKM3x2*ctlSx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(CKM3x3*ctlSIx12x32)/Lambda**2 + (CKM3x3*ctlSx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '-(ctlSIx13x13/Lambda**2) - (ctlSx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_178 = Coupling(name = 'GC_178',
                  value = 'ctlSIx13x13/Lambda**2 - (ctlSx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(ctlSIx13x23/Lambda**2) - (ctlSx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_180 = Coupling(name = 'GC_180',
                  value = 'ctlSIx13x23/Lambda**2 - (ctlSx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '(CKM1x1*ctlSIx13x13)/Lambda**2 + (CKM2x1*ctlSIx13x23)/Lambda**2 + (CKM1x1*ctlSx13x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '(CKM1x2*ctlSIx13x13)/Lambda**2 + (CKM2x2*ctlSIx13x23)/Lambda**2 + (CKM1x2*ctlSx13x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '(CKM1x3*ctlSIx13x13)/Lambda**2 + (CKM2x3*ctlSIx13x23)/Lambda**2 + (CKM1x3*ctlSx13x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(ctlSIx13x31/Lambda**2) - (ctlSx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_185 = Coupling(name = 'GC_185',
                  value = 'ctlSIx13x31/Lambda**2 - (ctlSx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(CKM3x1*ctlSIx13x31)/Lambda**2 + (CKM3x1*ctlSx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '(CKM3x2*ctlSIx13x31)/Lambda**2 + (CKM3x2*ctlSx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '(CKM3x3*ctlSIx13x31)/Lambda**2 + (CKM3x3*ctlSx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-(ctlSIx13x32/Lambda**2) - (ctlSx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_190 = Coupling(name = 'GC_190',
                  value = 'ctlSIx13x32/Lambda**2 - (ctlSx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(CKM3x1*ctlSIx13x32)/Lambda**2 + (CKM3x1*ctlSx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '(CKM3x2*ctlSIx13x32)/Lambda**2 + (CKM3x2*ctlSx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(CKM3x3*ctlSIx13x32)/Lambda**2 + (CKM3x3*ctlSx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '-(ctlSIx21x13/Lambda**2) - (ctlSx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_195 = Coupling(name = 'GC_195',
                  value = 'ctlSIx21x13/Lambda**2 - (ctlSx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-(ctlSIx21x23/Lambda**2) - (ctlSx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_197 = Coupling(name = 'GC_197',
                  value = 'ctlSIx21x23/Lambda**2 - (ctlSx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '(CKM1x1*ctlSIx21x13)/Lambda**2 + (CKM2x1*ctlSIx21x23)/Lambda**2 + (CKM1x1*ctlSx21x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(CKM1x2*ctlSIx21x13)/Lambda**2 + (CKM2x2*ctlSIx21x23)/Lambda**2 + (CKM1x2*ctlSx21x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '(CKM1x3*ctlSIx21x13)/Lambda**2 + (CKM2x3*ctlSIx21x23)/Lambda**2 + (CKM1x3*ctlSx21x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(ctlSIx21x31/Lambda**2) - (ctlSx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_202 = Coupling(name = 'GC_202',
                  value = 'ctlSIx21x31/Lambda**2 - (ctlSx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '(CKM3x1*ctlSIx21x31)/Lambda**2 + (CKM3x1*ctlSx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '(CKM3x2*ctlSIx21x31)/Lambda**2 + (CKM3x2*ctlSx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '(CKM3x3*ctlSIx21x31)/Lambda**2 + (CKM3x3*ctlSx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(ctlSIx21x32/Lambda**2) - (ctlSx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_207 = Coupling(name = 'GC_207',
                  value = 'ctlSIx21x32/Lambda**2 - (ctlSx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '(CKM3x1*ctlSIx21x32)/Lambda**2 + (CKM3x1*ctlSx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '(CKM3x2*ctlSIx21x32)/Lambda**2 + (CKM3x2*ctlSx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '(CKM3x3*ctlSIx21x32)/Lambda**2 + (CKM3x3*ctlSx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-(ctlSIx23x13/Lambda**2) - (ctlSx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_212 = Coupling(name = 'GC_212',
                  value = 'ctlSIx23x13/Lambda**2 - (ctlSx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '-(ctlSIx23x23/Lambda**2) - (ctlSx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_214 = Coupling(name = 'GC_214',
                  value = 'ctlSIx23x23/Lambda**2 - (ctlSx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '(CKM1x1*ctlSIx23x13)/Lambda**2 + (CKM2x1*ctlSIx23x23)/Lambda**2 + (CKM1x1*ctlSx23x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(CKM1x2*ctlSIx23x13)/Lambda**2 + (CKM2x2*ctlSIx23x23)/Lambda**2 + (CKM1x2*ctlSx23x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(CKM1x3*ctlSIx23x13)/Lambda**2 + (CKM2x3*ctlSIx23x23)/Lambda**2 + (CKM1x3*ctlSx23x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-(ctlSIx23x31/Lambda**2) - (ctlSx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_219 = Coupling(name = 'GC_219',
                  value = 'ctlSIx23x31/Lambda**2 - (ctlSx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '(CKM3x1*ctlSIx23x31)/Lambda**2 + (CKM3x1*ctlSx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(CKM3x2*ctlSIx23x31)/Lambda**2 + (CKM3x2*ctlSx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(CKM3x3*ctlSIx23x31)/Lambda**2 + (CKM3x3*ctlSx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-(ctlSIx23x32/Lambda**2) - (ctlSx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_224 = Coupling(name = 'GC_224',
                  value = 'ctlSIx23x32/Lambda**2 - (ctlSx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '(CKM3x1*ctlSIx23x32)/Lambda**2 + (CKM3x1*ctlSx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '(CKM3x2*ctlSIx23x32)/Lambda**2 + (CKM3x2*ctlSx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(CKM3x3*ctlSIx23x32)/Lambda**2 + (CKM3x3*ctlSx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '-(ctlSIx31x13/Lambda**2) - (ctlSx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_229 = Coupling(name = 'GC_229',
                  value = 'ctlSIx31x13/Lambda**2 - (ctlSx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '-(ctlSIx31x23/Lambda**2) - (ctlSx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_231 = Coupling(name = 'GC_231',
                  value = 'ctlSIx31x23/Lambda**2 - (ctlSx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(CKM1x1*ctlSIx31x13)/Lambda**2 + (CKM2x1*ctlSIx31x23)/Lambda**2 + (CKM1x1*ctlSx31x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '(CKM1x2*ctlSIx31x13)/Lambda**2 + (CKM2x2*ctlSIx31x23)/Lambda**2 + (CKM1x2*ctlSx31x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '(CKM1x3*ctlSIx31x13)/Lambda**2 + (CKM2x3*ctlSIx31x23)/Lambda**2 + (CKM1x3*ctlSx31x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-(ctlSIx31x31/Lambda**2) - (ctlSx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_236 = Coupling(name = 'GC_236',
                  value = 'ctlSIx31x31/Lambda**2 - (ctlSx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '(CKM3x1*ctlSIx31x31)/Lambda**2 + (CKM3x1*ctlSx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(CKM3x2*ctlSIx31x31)/Lambda**2 + (CKM3x2*ctlSx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '(CKM3x3*ctlSIx31x31)/Lambda**2 + (CKM3x3*ctlSx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(ctlSIx31x32/Lambda**2) - (ctlSx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_241 = Coupling(name = 'GC_241',
                  value = 'ctlSIx31x32/Lambda**2 - (ctlSx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '(CKM3x1*ctlSIx31x32)/Lambda**2 + (CKM3x1*ctlSx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '(CKM3x2*ctlSIx31x32)/Lambda**2 + (CKM3x2*ctlSx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(CKM3x3*ctlSIx31x32)/Lambda**2 + (CKM3x3*ctlSx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-(ctlSIx32x13/Lambda**2) - (ctlSx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_246 = Coupling(name = 'GC_246',
                  value = 'ctlSIx32x13/Lambda**2 - (ctlSx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-(ctlSIx32x23/Lambda**2) - (ctlSx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_248 = Coupling(name = 'GC_248',
                  value = 'ctlSIx32x23/Lambda**2 - (ctlSx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '(CKM1x1*ctlSIx32x13)/Lambda**2 + (CKM2x1*ctlSIx32x23)/Lambda**2 + (CKM1x1*ctlSx32x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlSx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '(CKM1x2*ctlSIx32x13)/Lambda**2 + (CKM2x2*ctlSIx32x23)/Lambda**2 + (CKM1x2*ctlSx32x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlSx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '(CKM1x3*ctlSIx32x13)/Lambda**2 + (CKM2x3*ctlSIx32x23)/Lambda**2 + (CKM1x3*ctlSx32x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlSx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-(ctlSIx32x31/Lambda**2) - (ctlSx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_253 = Coupling(name = 'GC_253',
                  value = 'ctlSIx32x31/Lambda**2 - (ctlSx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '(CKM3x1*ctlSIx32x31)/Lambda**2 + (CKM3x1*ctlSx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(CKM3x2*ctlSIx32x31)/Lambda**2 + (CKM3x2*ctlSx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(CKM3x3*ctlSIx32x31)/Lambda**2 + (CKM3x3*ctlSx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '-(ctlSIx32x32/Lambda**2) - (ctlSx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_258 = Coupling(name = 'GC_258',
                  value = 'ctlSIx32x32/Lambda**2 - (ctlSx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(CKM3x1*ctlSIx32x32)/Lambda**2 + (CKM3x1*ctlSx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(CKM3x2*ctlSIx32x32)/Lambda**2 + (CKM3x2*ctlSx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(CKM3x3*ctlSIx32x32)/Lambda**2 + (CKM3x3*ctlSx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '-(ctlTIx21x31/Lambda**2) - (ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_263 = Coupling(name = 'GC_263',
                  value = 'ctlTIx21x31/Lambda**2 - (ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-(ctlTIx21x31/Lambda**2) + (ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_265 = Coupling(name = 'GC_265',
                  value = 'ctlTIx21x31/Lambda**2 + (ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '(-4*ctlTIx21x31)/Lambda**2 + (4*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(4*ctlTIx21x31)/Lambda**2 + (4*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '-((CKM3x1*ctlTIx21x31)/Lambda**2) - (CKM3x1*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(CKM3x1*ctlTIx21x31)/Lambda**2 + (CKM3x1*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '(-4*CKM3x1*ctlTIx21x31)/Lambda**2 - (4*CKM3x1*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-((CKM3x2*ctlTIx21x31)/Lambda**2) - (CKM3x2*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(CKM3x2*ctlTIx21x31)/Lambda**2 + (CKM3x2*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(-4*CKM3x2*ctlTIx21x31)/Lambda**2 - (4*CKM3x2*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-((CKM3x3*ctlTIx21x31)/Lambda**2) - (CKM3x3*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '(CKM3x3*ctlTIx21x31)/Lambda**2 + (CKM3x3*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '(-4*CKM3x3*ctlTIx21x31)/Lambda**2 - (4*CKM3x3*ctlTx121x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '-(ctlTIx12x13/Lambda**2) - (ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_278 = Coupling(name = 'GC_278',
                  value = 'ctlTIx12x13/Lambda**2 - (ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-(ctlTIx12x13/Lambda**2) + (ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_280 = Coupling(name = 'GC_280',
                  value = 'ctlTIx12x13/Lambda**2 + (ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '(-4*ctlTIx12x13)/Lambda**2 + (4*ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(4*ctlTIx12x13)/Lambda**2 + (4*ctlTx12x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-(ctlTIx12x23/Lambda**2) - (ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_284 = Coupling(name = 'GC_284',
                  value = 'ctlTIx12x23/Lambda**2 - (ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '-(ctlTIx12x23/Lambda**2) + (ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_286 = Coupling(name = 'GC_286',
                  value = 'ctlTIx12x23/Lambda**2 + (ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(-4*ctlTIx12x23)/Lambda**2 + (4*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '(4*ctlTIx12x23)/Lambda**2 + (4*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '-((CKM1x1*ctlTIx12x13)/Lambda**2) - (CKM2x1*ctlTIx12x23)/Lambda**2 - (CKM1x1*ctlTx12x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '(CKM1x1*ctlTIx12x13)/Lambda**2 + (CKM2x1*ctlTIx12x23)/Lambda**2 + (CKM1x1*ctlTx12x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(-4*CKM1x1*ctlTIx12x13)/Lambda**2 - (4*CKM2x1*ctlTIx12x23)/Lambda**2 - (4*CKM1x1*ctlTx12x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '-((CKM1x2*ctlTIx12x13)/Lambda**2) - (CKM2x2*ctlTIx12x23)/Lambda**2 - (CKM1x2*ctlTx12x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(CKM1x2*ctlTIx12x13)/Lambda**2 + (CKM2x2*ctlTIx12x23)/Lambda**2 + (CKM1x2*ctlTx12x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(-4*CKM1x2*ctlTIx12x13)/Lambda**2 - (4*CKM2x2*ctlTIx12x23)/Lambda**2 - (4*CKM1x2*ctlTx12x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '-((CKM1x3*ctlTIx12x13)/Lambda**2) - (CKM2x3*ctlTIx12x23)/Lambda**2 - (CKM1x3*ctlTx12x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '(CKM1x3*ctlTIx12x13)/Lambda**2 + (CKM2x3*ctlTIx12x23)/Lambda**2 + (CKM1x3*ctlTx12x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '(-4*CKM1x3*ctlTIx12x13)/Lambda**2 - (4*CKM2x3*ctlTIx12x23)/Lambda**2 - (4*CKM1x3*ctlTx12x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx12x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '-(ctlTIx12x31/Lambda**2) - (ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_299 = Coupling(name = 'GC_299',
                  value = 'ctlTIx12x31/Lambda**2 - (ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '-(ctlTIx12x31/Lambda**2) + (ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_301 = Coupling(name = 'GC_301',
                  value = 'ctlTIx12x31/Lambda**2 + (ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '(-4*ctlTIx12x31)/Lambda**2 + (4*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(4*ctlTIx12x31)/Lambda**2 + (4*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '-((CKM3x1*ctlTIx12x31)/Lambda**2) - (CKM3x1*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '(CKM3x1*ctlTIx12x31)/Lambda**2 + (CKM3x1*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '(-4*CKM3x1*ctlTIx12x31)/Lambda**2 - (4*CKM3x1*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '-((CKM3x2*ctlTIx12x31)/Lambda**2) - (CKM3x2*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(CKM3x2*ctlTIx12x31)/Lambda**2 + (CKM3x2*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '(-4*CKM3x2*ctlTIx12x31)/Lambda**2 - (4*CKM3x2*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '-((CKM3x3*ctlTIx12x31)/Lambda**2) - (CKM3x3*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '(CKM3x3*ctlTIx12x31)/Lambda**2 + (CKM3x3*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(-4*CKM3x3*ctlTIx12x31)/Lambda**2 - (4*CKM3x3*ctlTx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(ctlTIx12x32/Lambda**2) - (ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_314 = Coupling(name = 'GC_314',
                  value = 'ctlTIx12x32/Lambda**2 - (ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '-(ctlTIx12x32/Lambda**2) + (ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_316 = Coupling(name = 'GC_316',
                  value = 'ctlTIx12x32/Lambda**2 + (ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '(-4*ctlTIx12x32)/Lambda**2 + (4*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '(4*ctlTIx12x32)/Lambda**2 + (4*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '-((CKM3x1*ctlTIx12x32)/Lambda**2) - (CKM3x1*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '(CKM3x1*ctlTIx12x32)/Lambda**2 + (CKM3x1*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(-4*CKM3x1*ctlTIx12x32)/Lambda**2 - (4*CKM3x1*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-((CKM3x2*ctlTIx12x32)/Lambda**2) - (CKM3x2*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(CKM3x2*ctlTIx12x32)/Lambda**2 + (CKM3x2*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '(-4*CKM3x2*ctlTIx12x32)/Lambda**2 - (4*CKM3x2*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '-((CKM3x3*ctlTIx12x32)/Lambda**2) - (CKM3x3*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '(CKM3x3*ctlTIx12x32)/Lambda**2 + (CKM3x3*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '(-4*CKM3x3*ctlTIx12x32)/Lambda**2 - (4*CKM3x3*ctlTx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '-(ctlTIx13x13/Lambda**2) - (ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_329 = Coupling(name = 'GC_329',
                  value = 'ctlTIx13x13/Lambda**2 - (ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(ctlTIx13x13/Lambda**2) + (ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_331 = Coupling(name = 'GC_331',
                  value = 'ctlTIx13x13/Lambda**2 + (ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '(-4*ctlTIx13x13)/Lambda**2 + (4*ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(4*ctlTIx13x13)/Lambda**2 + (4*ctlTx13x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-(ctlTIx13x23/Lambda**2) - (ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_335 = Coupling(name = 'GC_335',
                  value = 'ctlTIx13x23/Lambda**2 - (ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '-(ctlTIx13x23/Lambda**2) + (ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_337 = Coupling(name = 'GC_337',
                  value = 'ctlTIx13x23/Lambda**2 + (ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(-4*ctlTIx13x23)/Lambda**2 + (4*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '(4*ctlTIx13x23)/Lambda**2 + (4*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-((CKM1x1*ctlTIx13x13)/Lambda**2) - (CKM2x1*ctlTIx13x23)/Lambda**2 - (CKM1x1*ctlTx13x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '(CKM1x1*ctlTIx13x13)/Lambda**2 + (CKM2x1*ctlTIx13x23)/Lambda**2 + (CKM1x1*ctlTx13x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '(-4*CKM1x1*ctlTIx13x13)/Lambda**2 - (4*CKM2x1*ctlTIx13x23)/Lambda**2 - (4*CKM1x1*ctlTx13x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-((CKM1x2*ctlTIx13x13)/Lambda**2) - (CKM2x2*ctlTIx13x23)/Lambda**2 - (CKM1x2*ctlTx13x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '(CKM1x2*ctlTIx13x13)/Lambda**2 + (CKM2x2*ctlTIx13x23)/Lambda**2 + (CKM1x2*ctlTx13x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(-4*CKM1x2*ctlTIx13x13)/Lambda**2 - (4*CKM2x2*ctlTIx13x23)/Lambda**2 - (4*CKM1x2*ctlTx13x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '-((CKM1x3*ctlTIx13x13)/Lambda**2) - (CKM2x3*ctlTIx13x23)/Lambda**2 - (CKM1x3*ctlTx13x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(CKM1x3*ctlTIx13x13)/Lambda**2 + (CKM2x3*ctlTIx13x23)/Lambda**2 + (CKM1x3*ctlTx13x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(-4*CKM1x3*ctlTIx13x13)/Lambda**2 - (4*CKM2x3*ctlTIx13x23)/Lambda**2 - (4*CKM1x3*ctlTx13x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx13x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '-(ctlTIx13x31/Lambda**2) - (ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_350 = Coupling(name = 'GC_350',
                  value = 'ctlTIx13x31/Lambda**2 - (ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '-(ctlTIx13x31/Lambda**2) + (ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_352 = Coupling(name = 'GC_352',
                  value = 'ctlTIx13x31/Lambda**2 + (ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(-4*ctlTIx13x31)/Lambda**2 + (4*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(4*ctlTIx13x31)/Lambda**2 + (4*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '-((CKM3x1*ctlTIx13x31)/Lambda**2) - (CKM3x1*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '(CKM3x1*ctlTIx13x31)/Lambda**2 + (CKM3x1*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(-4*CKM3x1*ctlTIx13x31)/Lambda**2 - (4*CKM3x1*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '-((CKM3x2*ctlTIx13x31)/Lambda**2) - (CKM3x2*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(CKM3x2*ctlTIx13x31)/Lambda**2 + (CKM3x2*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(-4*CKM3x2*ctlTIx13x31)/Lambda**2 - (4*CKM3x2*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '-((CKM3x3*ctlTIx13x31)/Lambda**2) - (CKM3x3*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '(CKM3x3*ctlTIx13x31)/Lambda**2 + (CKM3x3*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(-4*CKM3x3*ctlTIx13x31)/Lambda**2 - (4*CKM3x3*ctlTx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '-(ctlTIx13x32/Lambda**2) - (ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_365 = Coupling(name = 'GC_365',
                  value = 'ctlTIx13x32/Lambda**2 - (ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '-(ctlTIx13x32/Lambda**2) + (ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_367 = Coupling(name = 'GC_367',
                  value = 'ctlTIx13x32/Lambda**2 + (ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '(-4*ctlTIx13x32)/Lambda**2 + (4*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(4*ctlTIx13x32)/Lambda**2 + (4*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '-((CKM3x1*ctlTIx13x32)/Lambda**2) - (CKM3x1*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(CKM3x1*ctlTIx13x32)/Lambda**2 + (CKM3x1*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '(-4*CKM3x1*ctlTIx13x32)/Lambda**2 - (4*CKM3x1*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '-((CKM3x2*ctlTIx13x32)/Lambda**2) - (CKM3x2*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(CKM3x2*ctlTIx13x32)/Lambda**2 + (CKM3x2*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(-4*CKM3x2*ctlTIx13x32)/Lambda**2 - (4*CKM3x2*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '-((CKM3x3*ctlTIx13x32)/Lambda**2) - (CKM3x3*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(CKM3x3*ctlTIx13x32)/Lambda**2 + (CKM3x3*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(-4*CKM3x3*ctlTIx13x32)/Lambda**2 - (4*CKM3x3*ctlTx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '-(ctlTIx21x13/Lambda**2) - (ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_380 = Coupling(name = 'GC_380',
                  value = 'ctlTIx21x13/Lambda**2 - (ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '-(ctlTIx21x13/Lambda**2) + (ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_382 = Coupling(name = 'GC_382',
                  value = 'ctlTIx21x13/Lambda**2 + (ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(-4*ctlTIx21x13)/Lambda**2 + (4*ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(4*ctlTIx21x13)/Lambda**2 + (4*ctlTx21x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '-(ctlTIx21x23/Lambda**2) - (ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_386 = Coupling(name = 'GC_386',
                  value = 'ctlTIx21x23/Lambda**2 - (ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '-(ctlTIx21x23/Lambda**2) + (ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_388 = Coupling(name = 'GC_388',
                  value = 'ctlTIx21x23/Lambda**2 + (ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(-4*ctlTIx21x23)/Lambda**2 + (4*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '(4*ctlTIx21x23)/Lambda**2 + (4*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '-((CKM1x1*ctlTIx21x13)/Lambda**2) - (CKM2x1*ctlTIx21x23)/Lambda**2 - (CKM1x1*ctlTx21x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(CKM1x1*ctlTIx21x13)/Lambda**2 + (CKM2x1*ctlTIx21x23)/Lambda**2 + (CKM1x1*ctlTx21x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '(-4*CKM1x1*ctlTIx21x13)/Lambda**2 - (4*CKM2x1*ctlTIx21x23)/Lambda**2 - (4*CKM1x1*ctlTx21x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '-((CKM1x2*ctlTIx21x13)/Lambda**2) - (CKM2x2*ctlTIx21x23)/Lambda**2 - (CKM1x2*ctlTx21x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(CKM1x2*ctlTIx21x13)/Lambda**2 + (CKM2x2*ctlTIx21x23)/Lambda**2 + (CKM1x2*ctlTx21x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(-4*CKM1x2*ctlTIx21x13)/Lambda**2 - (4*CKM2x2*ctlTIx21x23)/Lambda**2 - (4*CKM1x2*ctlTx21x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '-((CKM1x3*ctlTIx21x13)/Lambda**2) - (CKM2x3*ctlTIx21x23)/Lambda**2 - (CKM1x3*ctlTx21x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '(CKM1x3*ctlTIx21x13)/Lambda**2 + (CKM2x3*ctlTIx21x23)/Lambda**2 + (CKM1x3*ctlTx21x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(-4*CKM1x3*ctlTIx21x13)/Lambda**2 - (4*CKM2x3*ctlTIx21x23)/Lambda**2 - (4*CKM1x3*ctlTx21x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx21x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '-(ctlTIx21x32/Lambda**2) - (ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_401 = Coupling(name = 'GC_401',
                  value = 'ctlTIx21x32/Lambda**2 - (ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '-(ctlTIx21x32/Lambda**2) + (ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_403 = Coupling(name = 'GC_403',
                  value = 'ctlTIx21x32/Lambda**2 + (ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '(-4*ctlTIx21x32)/Lambda**2 + (4*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '(4*ctlTIx21x32)/Lambda**2 + (4*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '-((CKM3x1*ctlTIx21x32)/Lambda**2) - (CKM3x1*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '(CKM3x1*ctlTIx21x32)/Lambda**2 + (CKM3x1*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_408 = Coupling(name = 'GC_408',
                  value = '(-4*CKM3x1*ctlTIx21x32)/Lambda**2 - (4*CKM3x1*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_409 = Coupling(name = 'GC_409',
                  value = '-((CKM3x2*ctlTIx21x32)/Lambda**2) - (CKM3x2*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '(CKM3x2*ctlTIx21x32)/Lambda**2 + (CKM3x2*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '(-4*CKM3x2*ctlTIx21x32)/Lambda**2 - (4*CKM3x2*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_412 = Coupling(name = 'GC_412',
                  value = '-((CKM3x3*ctlTIx21x32)/Lambda**2) - (CKM3x3*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '(CKM3x3*ctlTIx21x32)/Lambda**2 + (CKM3x3*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_414 = Coupling(name = 'GC_414',
                  value = '(-4*CKM3x3*ctlTIx21x32)/Lambda**2 - (4*CKM3x3*ctlTx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '-(ctlTIx23x13/Lambda**2) - (ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_416 = Coupling(name = 'GC_416',
                  value = 'ctlTIx23x13/Lambda**2 - (ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_417 = Coupling(name = 'GC_417',
                  value = '-(ctlTIx23x13/Lambda**2) + (ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_418 = Coupling(name = 'GC_418',
                  value = 'ctlTIx23x13/Lambda**2 + (ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '(-4*ctlTIx23x13)/Lambda**2 + (4*ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_420 = Coupling(name = 'GC_420',
                  value = '(4*ctlTIx23x13)/Lambda**2 + (4*ctlTx23x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '-(ctlTIx23x23/Lambda**2) - (ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_422 = Coupling(name = 'GC_422',
                  value = 'ctlTIx23x23/Lambda**2 - (ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(ctlTIx23x23/Lambda**2) + (ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_424 = Coupling(name = 'GC_424',
                  value = 'ctlTIx23x23/Lambda**2 + (ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_425 = Coupling(name = 'GC_425',
                  value = '(-4*ctlTIx23x23)/Lambda**2 + (4*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_426 = Coupling(name = 'GC_426',
                  value = '(4*ctlTIx23x23)/Lambda**2 + (4*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '-((CKM1x1*ctlTIx23x13)/Lambda**2) - (CKM2x1*ctlTIx23x23)/Lambda**2 - (CKM1x1*ctlTx23x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '(CKM1x1*ctlTIx23x13)/Lambda**2 + (CKM2x1*ctlTIx23x23)/Lambda**2 + (CKM1x1*ctlTx23x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '(-4*CKM1x1*ctlTIx23x13)/Lambda**2 - (4*CKM2x1*ctlTIx23x23)/Lambda**2 - (4*CKM1x1*ctlTx23x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '-((CKM1x2*ctlTIx23x13)/Lambda**2) - (CKM2x2*ctlTIx23x23)/Lambda**2 - (CKM1x2*ctlTx23x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '(CKM1x2*ctlTIx23x13)/Lambda**2 + (CKM2x2*ctlTIx23x23)/Lambda**2 + (CKM1x2*ctlTx23x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '(-4*CKM1x2*ctlTIx23x13)/Lambda**2 - (4*CKM2x2*ctlTIx23x23)/Lambda**2 - (4*CKM1x2*ctlTx23x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '-((CKM1x3*ctlTIx23x13)/Lambda**2) - (CKM2x3*ctlTIx23x23)/Lambda**2 - (CKM1x3*ctlTx23x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '(CKM2x3*ctlTIx23x23)/Lambda**2 + (CKM1x3*ctlTx23x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '(CKM1x3*ctlTIx23x13)/Lambda**2 + (CKM2x3*ctlTIx23x23)/Lambda**2 + (CKM1x3*ctlTx23x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '(-4*CKM1x3*ctlTIx23x13)/Lambda**2 - (4*CKM2x3*ctlTIx23x23)/Lambda**2 - (4*CKM1x3*ctlTx23x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx23x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '-(ctlTIx23x31/Lambda**2) - (ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_438 = Coupling(name = 'GC_438',
                  value = 'ctlTIx23x31/Lambda**2 - (ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '-(ctlTIx23x31/Lambda**2) + (ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_440 = Coupling(name = 'GC_440',
                  value = 'ctlTIx23x31/Lambda**2 + (ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '(-4*ctlTIx23x31)/Lambda**2 + (4*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '(4*ctlTIx23x31)/Lambda**2 + (4*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '-((CKM3x1*ctlTIx23x31)/Lambda**2) - (CKM3x1*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_444 = Coupling(name = 'GC_444',
                  value = '(CKM3x1*ctlTIx23x31)/Lambda**2 + (CKM3x1*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '(-4*CKM3x1*ctlTIx23x31)/Lambda**2 - (4*CKM3x1*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '-((CKM3x2*ctlTIx23x31)/Lambda**2) - (CKM3x2*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '(CKM3x2*ctlTIx23x31)/Lambda**2 + (CKM3x2*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_448 = Coupling(name = 'GC_448',
                  value = '(-4*CKM3x2*ctlTIx23x31)/Lambda**2 - (4*CKM3x2*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '-((CKM3x3*ctlTIx23x31)/Lambda**2) - (CKM3x3*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '(CKM3x3*ctlTIx23x31)/Lambda**2 + (CKM3x3*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(-4*CKM3x3*ctlTIx23x31)/Lambda**2 - (4*CKM3x3*ctlTx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '-(ctlTIx23x32/Lambda**2) - (ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_453 = Coupling(name = 'GC_453',
                  value = 'ctlTIx23x32/Lambda**2 - (ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '-(ctlTIx23x32/Lambda**2) + (ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_455 = Coupling(name = 'GC_455',
                  value = 'ctlTIx23x32/Lambda**2 + (ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(-4*ctlTIx23x32)/Lambda**2 + (4*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '(4*ctlTIx23x32)/Lambda**2 + (4*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '-((CKM3x1*ctlTIx23x32)/Lambda**2) - (CKM3x1*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '(CKM3x1*ctlTIx23x32)/Lambda**2 + (CKM3x1*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '(-4*CKM3x1*ctlTIx23x32)/Lambda**2 - (4*CKM3x1*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '-((CKM3x2*ctlTIx23x32)/Lambda**2) - (CKM3x2*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '(CKM3x2*ctlTIx23x32)/Lambda**2 + (CKM3x2*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '(-4*CKM3x2*ctlTIx23x32)/Lambda**2 - (4*CKM3x2*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_464 = Coupling(name = 'GC_464',
                  value = '-((CKM3x3*ctlTIx23x32)/Lambda**2) - (CKM3x3*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_465 = Coupling(name = 'GC_465',
                  value = '(CKM3x3*ctlTIx23x32)/Lambda**2 + (CKM3x3*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '(-4*CKM3x3*ctlTIx23x32)/Lambda**2 - (4*CKM3x3*ctlTx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_467 = Coupling(name = 'GC_467',
                  value = '-(ctlTIx31x13/Lambda**2) - (ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_468 = Coupling(name = 'GC_468',
                  value = 'ctlTIx31x13/Lambda**2 - (ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '-(ctlTIx31x13/Lambda**2) + (ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_470 = Coupling(name = 'GC_470',
                  value = 'ctlTIx31x13/Lambda**2 + (ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '(-4*ctlTIx31x13)/Lambda**2 + (4*ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(4*ctlTIx31x13)/Lambda**2 + (4*ctlTx31x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '-(ctlTIx31x23/Lambda**2) - (ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_474 = Coupling(name = 'GC_474',
                  value = 'ctlTIx31x23/Lambda**2 - (ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '-(ctlTIx31x23/Lambda**2) + (ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_476 = Coupling(name = 'GC_476',
                  value = 'ctlTIx31x23/Lambda**2 + (ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '(-4*ctlTIx31x23)/Lambda**2 + (4*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '(4*ctlTIx31x23)/Lambda**2 + (4*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_479 = Coupling(name = 'GC_479',
                  value = '-((CKM1x1*ctlTIx31x13)/Lambda**2) - (CKM2x1*ctlTIx31x23)/Lambda**2 - (CKM1x1*ctlTx31x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '(CKM1x1*ctlTIx31x13)/Lambda**2 + (CKM2x1*ctlTIx31x23)/Lambda**2 + (CKM1x1*ctlTx31x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_481 = Coupling(name = 'GC_481',
                  value = '(-4*CKM1x1*ctlTIx31x13)/Lambda**2 - (4*CKM2x1*ctlTIx31x23)/Lambda**2 - (4*CKM1x1*ctlTx31x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_482 = Coupling(name = 'GC_482',
                  value = '-((CKM1x2*ctlTIx31x13)/Lambda**2) - (CKM2x2*ctlTIx31x23)/Lambda**2 - (CKM1x2*ctlTx31x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_483 = Coupling(name = 'GC_483',
                  value = '(CKM1x2*ctlTIx31x13)/Lambda**2 + (CKM2x2*ctlTIx31x23)/Lambda**2 + (CKM1x2*ctlTx31x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_484 = Coupling(name = 'GC_484',
                  value = '(-4*CKM1x2*ctlTIx31x13)/Lambda**2 - (4*CKM2x2*ctlTIx31x23)/Lambda**2 - (4*CKM1x2*ctlTx31x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_485 = Coupling(name = 'GC_485',
                  value = '-((CKM1x3*ctlTIx31x13)/Lambda**2) - (CKM2x3*ctlTIx31x23)/Lambda**2 - (CKM1x3*ctlTx31x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_486 = Coupling(name = 'GC_486',
                  value = '(CKM1x3*ctlTIx31x13)/Lambda**2 + (CKM2x3*ctlTIx31x23)/Lambda**2 + (CKM1x3*ctlTx31x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_487 = Coupling(name = 'GC_487',
                  value = '(-4*CKM1x3*ctlTIx31x13)/Lambda**2 - (4*CKM2x3*ctlTIx31x23)/Lambda**2 - (4*CKM1x3*ctlTx31x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx31x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '-(ctlTIx31x31/Lambda**2) - (ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_489 = Coupling(name = 'GC_489',
                  value = 'ctlTIx31x31/Lambda**2 - (ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_490 = Coupling(name = 'GC_490',
                  value = '-(ctlTIx31x31/Lambda**2) + (ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_491 = Coupling(name = 'GC_491',
                  value = 'ctlTIx31x31/Lambda**2 + (ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '(-4*ctlTIx31x31)/Lambda**2 + (4*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_493 = Coupling(name = 'GC_493',
                  value = '(4*ctlTIx31x31)/Lambda**2 + (4*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '-((CKM3x1*ctlTIx31x31)/Lambda**2) - (CKM3x1*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '(CKM3x1*ctlTIx31x31)/Lambda**2 + (CKM3x1*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_496 = Coupling(name = 'GC_496',
                  value = '(-4*CKM3x1*ctlTIx31x31)/Lambda**2 - (4*CKM3x1*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '-((CKM3x2*ctlTIx31x31)/Lambda**2) - (CKM3x2*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '(CKM3x2*ctlTIx31x31)/Lambda**2 + (CKM3x2*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '(-4*CKM3x2*ctlTIx31x31)/Lambda**2 - (4*CKM3x2*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '-((CKM3x3*ctlTIx31x31)/Lambda**2) - (CKM3x3*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '(CKM3x3*ctlTIx31x31)/Lambda**2 + (CKM3x3*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '(-4*CKM3x3*ctlTIx31x31)/Lambda**2 - (4*CKM3x3*ctlTx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '-(ctlTIx31x32/Lambda**2) - (ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_504 = Coupling(name = 'GC_504',
                  value = 'ctlTIx31x32/Lambda**2 - (ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_505 = Coupling(name = 'GC_505',
                  value = '-(ctlTIx31x32/Lambda**2) + (ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_506 = Coupling(name = 'GC_506',
                  value = '(-4*ctlTIx31x32)/Lambda**2 + (4*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_507 = Coupling(name = 'GC_507',
                  value = '(4*ctlTIx31x32)/Lambda**2 + (4*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_508 = Coupling(name = 'GC_508',
                  value = '(CKM3x1*ctlTIx31x32)/Lambda**2 + (CKM3x1*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_509 = Coupling(name = 'GC_509',
                  value = '(-4*CKM3x1*ctlTIx31x32)/Lambda**2 - (4*CKM3x1*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_510 = Coupling(name = 'GC_510',
                  value = '-((CKM3x2*ctlTIx31x32)/Lambda**2) - (CKM3x2*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '(CKM3x2*ctlTIx31x32)/Lambda**2 + (CKM3x2*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '(-4*CKM3x2*ctlTIx31x32)/Lambda**2 - (4*CKM3x2*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '-((CKM3x3*ctlTIx31x32)/Lambda**2) - (CKM3x3*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_514 = Coupling(name = 'GC_514',
                  value = '(CKM3x3*ctlTIx31x32)/Lambda**2 + (CKM3x3*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '(-4*CKM3x3*ctlTIx31x32)/Lambda**2 - (4*CKM3x3*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '-(ctlTIx32x13/Lambda**2) - (ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_517 = Coupling(name = 'GC_517',
                  value = 'ctlTIx32x13/Lambda**2 - (ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '-(ctlTIx32x13/Lambda**2) + (ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_519 = Coupling(name = 'GC_519',
                  value = 'ctlTIx32x13/Lambda**2 + (ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '(-4*ctlTIx32x13)/Lambda**2 + (4*ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(4*ctlTIx32x13)/Lambda**2 + (4*ctlTx32x13*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '-(ctlTIx32x23/Lambda**2) - (ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_523 = Coupling(name = 'GC_523',
                  value = 'ctlTIx32x23/Lambda**2 - (ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '-(ctlTIx32x23/Lambda**2) + (ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_525 = Coupling(name = 'GC_525',
                  value = 'ctlTIx32x23/Lambda**2 + (ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '(-4*ctlTIx32x23)/Lambda**2 + (4*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '(4*ctlTIx32x23)/Lambda**2 + (4*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '-((CKM1x1*ctlTIx32x13)/Lambda**2) - (CKM2x1*ctlTIx32x23)/Lambda**2 - (CKM1x1*ctlTx32x13*complex(0,1))/Lambda**2 - (CKM2x1*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '(CKM1x1*ctlTIx32x13)/Lambda**2 + (CKM2x1*ctlTIx32x23)/Lambda**2 + (CKM1x1*ctlTx32x13*complex(0,1))/Lambda**2 + (CKM2x1*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '(-4*CKM1x1*ctlTIx32x13)/Lambda**2 - (4*CKM2x1*ctlTIx32x23)/Lambda**2 - (4*CKM1x1*ctlTx32x13*complex(0,1))/Lambda**2 - (4*CKM2x1*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_531 = Coupling(name = 'GC_531',
                  value = '-((CKM1x2*ctlTIx32x13)/Lambda**2) - (CKM2x2*ctlTIx32x23)/Lambda**2 - (CKM1x2*ctlTx32x13*complex(0,1))/Lambda**2 - (CKM2x2*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_532 = Coupling(name = 'GC_532',
                  value = '(CKM1x2*ctlTIx32x13)/Lambda**2 + (CKM2x2*ctlTIx32x23)/Lambda**2 + (CKM1x2*ctlTx32x13*complex(0,1))/Lambda**2 + (CKM2x2*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_533 = Coupling(name = 'GC_533',
                  value = '(-4*CKM1x2*ctlTIx32x13)/Lambda**2 - (4*CKM2x2*ctlTIx32x23)/Lambda**2 - (4*CKM1x2*ctlTx32x13*complex(0,1))/Lambda**2 - (4*CKM2x2*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_534 = Coupling(name = 'GC_534',
                  value = '-((CKM1x3*ctlTIx32x13)/Lambda**2) - (CKM2x3*ctlTIx32x23)/Lambda**2 - (CKM1x3*ctlTx32x13*complex(0,1))/Lambda**2 - (CKM2x3*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_535 = Coupling(name = 'GC_535',
                  value = '(CKM1x3*ctlTIx32x13)/Lambda**2 + (CKM2x3*ctlTIx32x23)/Lambda**2 + (CKM1x3*ctlTx32x13*complex(0,1))/Lambda**2 + (CKM2x3*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_536 = Coupling(name = 'GC_536',
                  value = '(-4*CKM1x3*ctlTIx32x13)/Lambda**2 - (4*CKM2x3*ctlTIx32x23)/Lambda**2 - (4*CKM1x3*ctlTx32x13*complex(0,1))/Lambda**2 - (4*CKM2x3*ctlTx32x23*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_537 = Coupling(name = 'GC_537',
                  value = '-(ctlTIx32x31/Lambda**2) - (ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_538 = Coupling(name = 'GC_538',
                  value = 'ctlTIx32x31/Lambda**2 - (ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_539 = Coupling(name = 'GC_539',
                  value = '-(ctlTIx32x31/Lambda**2) + (ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_540 = Coupling(name = 'GC_540',
                  value = 'ctlTIx32x31/Lambda**2 + (ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_541 = Coupling(name = 'GC_541',
                  value = '(-4*ctlTIx32x31)/Lambda**2 + (4*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '(4*ctlTIx32x31)/Lambda**2 + (4*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_543 = Coupling(name = 'GC_543',
                  value = '-((CKM3x1*ctlTIx32x31)/Lambda**2) - (CKM3x1*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_544 = Coupling(name = 'GC_544',
                  value = '(CKM3x1*ctlTIx32x31)/Lambda**2 + (CKM3x1*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_545 = Coupling(name = 'GC_545',
                  value = '(-4*CKM3x1*ctlTIx32x31)/Lambda**2 - (4*CKM3x1*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_546 = Coupling(name = 'GC_546',
                  value = '-((CKM3x2*ctlTIx32x31)/Lambda**2) - (CKM3x2*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_547 = Coupling(name = 'GC_547',
                  value = '(CKM3x2*ctlTIx32x31)/Lambda**2 + (CKM3x2*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_548 = Coupling(name = 'GC_548',
                  value = '(-4*CKM3x2*ctlTIx32x31)/Lambda**2 - (4*CKM3x2*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '-((CKM3x3*ctlTIx32x31)/Lambda**2) - (CKM3x3*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_550 = Coupling(name = 'GC_550',
                  value = '(CKM3x3*ctlTIx32x31)/Lambda**2 + (CKM3x3*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '(-4*CKM3x3*ctlTIx32x31)/Lambda**2 - (4*CKM3x3*ctlTx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '-(ctlTIx32x32/Lambda**2) - (ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_553 = Coupling(name = 'GC_553',
                  value = 'ctlTIx32x32/Lambda**2 - (ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_554 = Coupling(name = 'GC_554',
                  value = '-(ctlTIx32x32/Lambda**2) + (ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_555 = Coupling(name = 'GC_555',
                  value = 'ctlTIx32x32/Lambda**2 + (ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_556 = Coupling(name = 'GC_556',
                  value = '(-4*ctlTIx32x32)/Lambda**2 + (4*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_557 = Coupling(name = 'GC_557',
                  value = '(4*ctlTIx32x32)/Lambda**2 + (4*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '-((CKM3x1*ctlTIx32x32)/Lambda**2) - (CKM3x1*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '(CKM3x1*ctlTIx32x32)/Lambda**2 + (CKM3x1*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_560 = Coupling(name = 'GC_560',
                  value = '(-4*CKM3x1*ctlTIx32x32)/Lambda**2 - (4*CKM3x1*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_561 = Coupling(name = 'GC_561',
                  value = '-((CKM3x2*ctlTIx32x32)/Lambda**2) - (CKM3x2*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_562 = Coupling(name = 'GC_562',
                  value = '(CKM3x2*ctlTIx32x32)/Lambda**2 + (CKM3x2*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_563 = Coupling(name = 'GC_563',
                  value = '(-4*CKM3x2*ctlTIx32x32)/Lambda**2 - (4*CKM3x2*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '-((CKM3x3*ctlTIx32x32)/Lambda**2) - (CKM3x3*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_565 = Coupling(name = 'GC_565',
                  value = '(CKM3x3*ctlTIx32x32)/Lambda**2 + (CKM3x3*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '(-4*CKM3x3*ctlTIx32x32)/Lambda**2 - (4*CKM3x3*ctlTx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '-(ctlIx12x31/Lambda**2) + (ctlx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_568 = Coupling(name = 'GC_568',
                  value = 'ctlIx12x31/Lambda**2 + (ctlx12x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '-(ctlIx12x32/Lambda**2) + (ctlx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_570 = Coupling(name = 'GC_570',
                  value = 'ctlIx12x32/Lambda**2 + (ctlx12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '-(ctlIx13x31/Lambda**2) + (ctlx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_572 = Coupling(name = 'GC_572',
                  value = 'ctlIx13x31/Lambda**2 + (ctlx13x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_573 = Coupling(name = 'GC_573',
                  value = '-(ctlIx13x32/Lambda**2) + (ctlx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_574 = Coupling(name = 'GC_574',
                  value = 'ctlIx13x32/Lambda**2 + (ctlx13x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '-(ctlIx21x31/Lambda**2) + (ctlx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_576 = Coupling(name = 'GC_576',
                  value = 'ctlIx21x31/Lambda**2 + (ctlx21x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '-(ctlIx21x32/Lambda**2) + (ctlx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_578 = Coupling(name = 'GC_578',
                  value = 'ctlIx21x32/Lambda**2 + (ctlx21x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '-(ctlIx23x31/Lambda**2) + (ctlx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_580 = Coupling(name = 'GC_580',
                  value = 'ctlIx23x31/Lambda**2 + (ctlx23x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '-(ctlIx23x32/Lambda**2) + (ctlx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_582 = Coupling(name = 'GC_582',
                  value = 'ctlIx23x32/Lambda**2 + (ctlx23x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '-(ctlIx31x31/Lambda**2) + (ctlx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_584 = Coupling(name = 'GC_584',
                  value = 'ctlIx31x31/Lambda**2 + (ctlx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_585 = Coupling(name = 'GC_585',
                  value = '-(ctlIx31x32/Lambda**2) + (ctlx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_586 = Coupling(name = 'GC_586',
                  value = 'ctlIx31x32/Lambda**2 + (ctlx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '-(ctlIx32x31/Lambda**2) + (ctlx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_588 = Coupling(name = 'GC_588',
                  value = 'ctlIx32x31/Lambda**2 + (ctlx32x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '-(ctlIx32x32/Lambda**2) + (ctlx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_590 = Coupling(name = 'GC_590',
                  value = 'ctlIx32x32/Lambda**2 + (ctlx32x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lu':1})

GC_591 = Coupling(name = 'GC_591',
                  value = '(2*CKM2x2*cQl3x12x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '(cQlMx31x31*complex(0,1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_593 = Coupling(name = 'GC_593',
                  value = '(CKM1x3*ctlTIx23x13)/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_594 = Coupling(name = 'GC_594',
                  value = 'ctlTIx31x32/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '-((CKM3x1*ctlTIx31x32)/Lambda**2)',
                  order = {'CLFV_lequ3':1})

GC_596 = Coupling(name = 'GC_596',
                  value = '(CKM3x3*ctlTIx31x32)/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_597 = Coupling(name = 'GC_597',
                  value = '(ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_598 = Coupling(name = 'GC_598',
                  value = '-((CKM3x1*ctlTx31x32*complex(0,1))/Lambda**2)',
                  order = {'CLFV_lequ3':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '(CKM3x3*ctlTx31x32*complex(0,1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_600 = Coupling(name = 'GC_600',
                  value = 'ee**2*(0. - 1.*complex(0,1)) + (ee**2*(0. + 1.*complex(0,1)))/sw**2',
                  order = {'QED':2})

GC_601 = Coupling(name = 'GC_601',
                  value = '(ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_602 = Coupling(name = 'GC_602',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_603 = Coupling(name = 'GC_603',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_604 = Coupling(name = 'GC_604',
                  value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_605 = Coupling(name = 'GC_605',
                  value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_608 = Coupling(name = 'GC_608',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_610 = Coupling(name = 'GC_610',
                  value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_612 = Coupling(name = 'GC_612',
                  value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_613 = Coupling(name = 'GC_613',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_614 = Coupling(name = 'GC_614',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_615 = Coupling(name = 'GC_615',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_616 = Coupling(name = 'GC_616',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_618 = Coupling(name = 'GC_618',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_619 = Coupling(name = 'GC_619',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_622 = Coupling(name = 'GC_622',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_623 = Coupling(name = 'GC_623',
                  value = 'ee**2*(0. + 0.5*complex(0,1)) + (ee**2*(0. + 0.5*complex(0,1)))/sw**2 + (ee**2*(0. + 0.5*complex(0,1))*sw**2)/cw**2',
                  order = {'QED':2})

GC_624 = Coupling(name = 'GC_624',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '-(ee**2*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_627 = Coupling(name = 'GC_627',
                  value = 'ee**2*(0. - 0.25*complex(0,1))*vev + (ee**2*(0. - 0.25*complex(0,1))*vev)/sw**2 + (ee**2*(0. - 0.25*complex(0,1))*sw**2*vev)/cw**2',
                  order = {'QED':1})

GC_628 = Coupling(name = 'GC_628',
                  value = 'ee**2*(0. + 0.5*complex(0,1))*vev + (ee**2*(0. + 0.5*complex(0,1))*vev)/sw**2 + (ee**2*(0. + 0.5*complex(0,1))*sw**2*vev)/cw**2',
                  order = {'QED':1})

GC_629 = Coupling(name = 'GC_629',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_630 = Coupling(name = 'GC_630',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_631 = Coupling(name = 'GC_631',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_632 = Coupling(name = 'GC_632',
                  value = '(CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_633 = Coupling(name = 'GC_633',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_634 = Coupling(name = 'GC_634',
                  value = '(2*CKM3x2*cQl3Ix12x31*complexconjugate(CKM1x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_635 = Coupling(name = 'GC_635',
                  value = '(2*CKM3x3*cQl3Ix12x31*complexconjugate(CKM1x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_636 = Coupling(name = 'GC_636',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_637 = Coupling(name = 'GC_637',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_638 = Coupling(name = 'GC_638',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_639 = Coupling(name = 'GC_639',
                  value = '(2*cQl3Ix12x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix12x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_640 = Coupling(name = 'GC_640',
                  value = '(2*cQl3Ix13x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix13x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_641 = Coupling(name = 'GC_641',
                  value = '(2*cQl3Ix21x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix21x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_642 = Coupling(name = 'GC_642',
                  value = '(2*cQl3Ix23x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix23x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_643 = Coupling(name = 'GC_643',
                  value = '(2*cQl3Ix31x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix31x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_644 = Coupling(name = 'GC_644',
                  value = '(2*cQl3Ix32x31*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*cQl3Ix32x32*complexconjugate(CKM2x1))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_645 = Coupling(name = 'GC_645',
                  value = '-((ctlSIx12x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx12x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx12x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx12x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_646 = Coupling(name = 'GC_646',
                  value = '-((ctlSIx13x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx13x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx13x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx13x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_647 = Coupling(name = 'GC_647',
                  value = '-((ctlSIx21x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx21x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx21x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx21x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_648 = Coupling(name = 'GC_648',
                  value = '-((ctlSIx23x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx23x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx23x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx23x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_649 = Coupling(name = 'GC_649',
                  value = '-((ctlSIx31x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx31x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx31x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx31x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_650 = Coupling(name = 'GC_650',
                  value = '-((ctlSIx32x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlSx32x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlSIx32x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlSx32x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_651 = Coupling(name = 'GC_651',
                  value = '(ctlTIx12x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx12x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_652 = Coupling(name = 'GC_652',
                  value = '-((ctlTIx12x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx12x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_653 = Coupling(name = 'GC_653',
                  value = '(4*ctlTIx12x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx12x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx12x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx12x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_654 = Coupling(name = 'GC_654',
                  value = '(ctlTIx13x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx13x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_655 = Coupling(name = 'GC_655',
                  value = '-((ctlTIx13x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx13x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_656 = Coupling(name = 'GC_656',
                  value = '(4*ctlTIx13x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx13x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx13x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx13x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_657 = Coupling(name = 'GC_657',
                  value = '(ctlTIx21x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx21x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx21x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_658 = Coupling(name = 'GC_658',
                  value = '-((ctlTIx21x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx21x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx21x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_659 = Coupling(name = 'GC_659',
                  value = '(4*ctlTIx21x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx21x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx21x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx21x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_660 = Coupling(name = 'GC_660',
                  value = '(ctlTIx23x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx23x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_661 = Coupling(name = 'GC_661',
                  value = '-((ctlTIx23x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx23x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_662 = Coupling(name = 'GC_662',
                  value = '(4*ctlTIx23x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx23x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx23x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx23x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_663 = Coupling(name = 'GC_663',
                  value = '(ctlTIx31x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx31x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_664 = Coupling(name = 'GC_664',
                  value = '-((ctlTIx31x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx31x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_665 = Coupling(name = 'GC_665',
                  value = '(4*ctlTIx31x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx31x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx31x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx31x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_666 = Coupling(name = 'GC_666',
                  value = '(ctlTIx32x13*complexconjugate(CKM1x1))/Lambda**2 - (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (ctlTIx32x23*complexconjugate(CKM2x1))/Lambda**2 - (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_667 = Coupling(name = 'GC_667',
                  value = '-((ctlTIx32x13*complexconjugate(CKM1x1))/Lambda**2) + (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 - (ctlTIx32x23*complexconjugate(CKM2x1))/Lambda**2 + (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_668 = Coupling(name = 'GC_668',
                  value = '(4*ctlTIx32x13*complexconjugate(CKM1x1))/Lambda**2 - (4*ctlTx32x13*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (4*ctlTIx32x23*complexconjugate(CKM2x1))/Lambda**2 - (4*ctlTx32x23*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_669 = Coupling(name = 'GC_669',
                  value = '(CKM3x1*cQeI323x32*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_670 = Coupling(name = 'GC_670',
                  value = '-((ctlTx21x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2)',
                  order = {'CLFV_lequ3':1})

GC_671 = Coupling(name = 'GC_671',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_672 = Coupling(name = 'GC_672',
                  value = '(2*cQl3Ix12x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix12x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_673 = Coupling(name = 'GC_673',
                  value = '(2*cQl3Ix13x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix13x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_674 = Coupling(name = 'GC_674',
                  value = '(2*cQl3Ix21x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix21x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_675 = Coupling(name = 'GC_675',
                  value = '(2*cQl3Ix23x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix23x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_676 = Coupling(name = 'GC_676',
                  value = '(2*cQl3Ix31x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix31x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_677 = Coupling(name = 'GC_677',
                  value = '(2*cQl3Ix32x31*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*cQl3Ix32x32*complexconjugate(CKM2x2))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_678 = Coupling(name = 'GC_678',
                  value = '-((ctlSIx12x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx12x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx12x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx12x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_679 = Coupling(name = 'GC_679',
                  value = '-((ctlSIx13x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx13x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx13x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx13x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_680 = Coupling(name = 'GC_680',
                  value = '-((ctlSIx21x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx21x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx21x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx21x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_681 = Coupling(name = 'GC_681',
                  value = '-((ctlSIx23x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx23x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx23x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx23x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_682 = Coupling(name = 'GC_682',
                  value = '-((ctlSIx31x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx31x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx31x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx31x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_683 = Coupling(name = 'GC_683',
                  value = '-((ctlSIx32x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlSx32x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlSIx32x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlSx32x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_684 = Coupling(name = 'GC_684',
                  value = '(ctlTIx21x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx21x23*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_685 = Coupling(name = 'GC_685',
                  value = '(ctlTIx12x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx12x23*complexconjugate(CKM2x2))/Lambda**2 - (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_686 = Coupling(name = 'GC_686',
                  value = '-((ctlTIx12x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx12x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_687 = Coupling(name = 'GC_687',
                  value = '(4*ctlTIx12x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx12x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx12x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx12x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_688 = Coupling(name = 'GC_688',
                  value = '(ctlTIx13x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx13x23*complexconjugate(CKM2x2))/Lambda**2 - (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_689 = Coupling(name = 'GC_689',
                  value = '-((ctlTIx13x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx13x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_690 = Coupling(name = 'GC_690',
                  value = '(4*ctlTIx13x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx13x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx13x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx13x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_691 = Coupling(name = 'GC_691',
                  value = '-((ctlTIx21x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx21x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx21x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_692 = Coupling(name = 'GC_692',
                  value = '(4*ctlTIx21x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx21x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx21x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx21x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_693 = Coupling(name = 'GC_693',
                  value = '(ctlTIx23x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx23x23*complexconjugate(CKM2x2))/Lambda**2 - (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_694 = Coupling(name = 'GC_694',
                  value = '-((ctlTIx23x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx23x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_695 = Coupling(name = 'GC_695',
                  value = '(4*ctlTIx23x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx23x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx23x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx23x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_696 = Coupling(name = 'GC_696',
                  value = '(ctlTIx31x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx31x23*complexconjugate(CKM2x2))/Lambda**2 - (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_697 = Coupling(name = 'GC_697',
                  value = '-((ctlTIx31x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx31x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_698 = Coupling(name = 'GC_698',
                  value = '(4*ctlTIx31x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx31x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx31x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx31x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_699 = Coupling(name = 'GC_699',
                  value = '(ctlTIx32x13*complexconjugate(CKM1x2))/Lambda**2 - (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (ctlTIx32x23*complexconjugate(CKM2x2))/Lambda**2 - (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_700 = Coupling(name = 'GC_700',
                  value = '-((ctlTIx32x13*complexconjugate(CKM1x2))/Lambda**2) + (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 - (ctlTIx32x23*complexconjugate(CKM2x2))/Lambda**2 + (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_701 = Coupling(name = 'GC_701',
                  value = '(4*ctlTIx32x13*complexconjugate(CKM1x2))/Lambda**2 - (4*ctlTx32x13*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (4*ctlTIx32x23*complexconjugate(CKM2x2))/Lambda**2 - (4*ctlTx32x23*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_702 = Coupling(name = 'GC_702',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_703 = Coupling(name = 'GC_703',
                  value = '(2*cQl3Ix12x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix12x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_704 = Coupling(name = 'GC_704',
                  value = '(2*cQl3Ix13x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix13x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_705 = Coupling(name = 'GC_705',
                  value = '(2*cQl3Ix21x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix21x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_706 = Coupling(name = 'GC_706',
                  value = '(2*cQl3Ix23x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix23x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_707 = Coupling(name = 'GC_707',
                  value = '(2*cQl3Ix31x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix31x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_708 = Coupling(name = 'GC_708',
                  value = '(2*cQl3Ix32x31*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*cQl3Ix32x32*complexconjugate(CKM2x3))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_709 = Coupling(name = 'GC_709',
                  value = '-((ctlSIx12x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx12x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx12x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx12x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_710 = Coupling(name = 'GC_710',
                  value = '-((ctlSIx13x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx13x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx13x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx13x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_711 = Coupling(name = 'GC_711',
                  value = '-((ctlSIx21x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx21x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx21x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx21x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_712 = Coupling(name = 'GC_712',
                  value = '-((ctlSIx23x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx23x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx23x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx23x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_713 = Coupling(name = 'GC_713',
                  value = '-((ctlSIx31x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx31x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx31x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx31x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_714 = Coupling(name = 'GC_714',
                  value = '-((ctlSIx32x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlSx32x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlSIx32x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlSx32x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_715 = Coupling(name = 'GC_715',
                  value = '(ctlTIx12x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx12x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_716 = Coupling(name = 'GC_716',
                  value = '-((ctlTIx12x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx12x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx12x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx12x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_717 = Coupling(name = 'GC_717',
                  value = '(4*ctlTIx12x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx12x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx12x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx12x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_718 = Coupling(name = 'GC_718',
                  value = '(ctlTIx13x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx13x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_719 = Coupling(name = 'GC_719',
                  value = '-((ctlTIx13x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx13x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx13x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx13x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_720 = Coupling(name = 'GC_720',
                  value = '(4*ctlTIx13x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx13x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx13x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx13x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_721 = Coupling(name = 'GC_721',
                  value = '(ctlTIx21x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx21x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx21x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_722 = Coupling(name = 'GC_722',
                  value = '-((ctlTIx21x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx21x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx21x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx21x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_723 = Coupling(name = 'GC_723',
                  value = '(4*ctlTIx21x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx21x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx21x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx21x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_724 = Coupling(name = 'GC_724',
                  value = '(ctlTIx23x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx23x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_725 = Coupling(name = 'GC_725',
                  value = '-((ctlTIx23x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx23x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx23x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx23x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_726 = Coupling(name = 'GC_726',
                  value = '(4*ctlTIx23x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx23x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx23x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx23x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_727 = Coupling(name = 'GC_727',
                  value = '(ctlTIx31x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx31x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_728 = Coupling(name = 'GC_728',
                  value = '-((ctlTIx31x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx31x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx31x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx31x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_729 = Coupling(name = 'GC_729',
                  value = '(4*ctlTIx31x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx31x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx31x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx31x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_730 = Coupling(name = 'GC_730',
                  value = '(ctlTIx32x13*complexconjugate(CKM1x3))/Lambda**2 - (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (ctlTIx32x23*complexconjugate(CKM2x3))/Lambda**2 - (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_731 = Coupling(name = 'GC_731',
                  value = '-((ctlTIx32x13*complexconjugate(CKM1x3))/Lambda**2) + (ctlTx32x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 - (ctlTIx32x23*complexconjugate(CKM2x3))/Lambda**2 + (ctlTx32x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_732 = Coupling(name = 'GC_732',
                  value = '(4*ctlTIx32x13*complexconjugate(CKM1x3))/Lambda**2 - (4*ctlTx32x13*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (4*ctlTIx32x23*complexconjugate(CKM2x3))/Lambda**2 - (4*ctlTx32x23*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_733 = Coupling(name = 'GC_733',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_734 = Coupling(name = 'GC_734',
                  value = '(CKM3x1*cQeIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQeIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQeIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_735 = Coupling(name = 'GC_735',
                  value = '(CKM3x2*cQeIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQeIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQeIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_736 = Coupling(name = 'GC_736',
                  value = '(CKM3x3*cQeIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQeIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQeIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_737 = Coupling(name = 'GC_737',
                  value = '(CKM3x1*cQeIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQeIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQeIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_738 = Coupling(name = 'GC_738',
                  value = '(CKM3x2*cQeIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQeIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQeIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_739 = Coupling(name = 'GC_739',
                  value = '(CKM3x3*cQeIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQeIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQeIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_740 = Coupling(name = 'GC_740',
                  value = '(CKM3x1*cQeIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQeIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQeIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_741 = Coupling(name = 'GC_741',
                  value = '(CKM3x2*cQeIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQeIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQeIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_742 = Coupling(name = 'GC_742',
                  value = '(CKM3x3*cQeIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQeIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQeIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_743 = Coupling(name = 'GC_743',
                  value = '(CKM3x1*cQeIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeI323x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQeIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQeIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_744 = Coupling(name = 'GC_744',
                  value = '(CKM3x2*cQeIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeI323x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQeIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQeIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_745 = Coupling(name = 'GC_745',
                  value = '(CKM3x3*cQeIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeI323x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQeIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQeIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_746 = Coupling(name = 'GC_746',
                  value = '(CKM3x1*cQeIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQeIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQeIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_747 = Coupling(name = 'GC_747',
                  value = '(CKM3x2*cQeIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQeIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQeIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_748 = Coupling(name = 'GC_748',
                  value = '(CKM3x3*cQeIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQeIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQeIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_749 = Coupling(name = 'GC_749',
                  value = '(CKM3x1*cQeIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQex23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQeIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQex23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM2x1*cQeI323x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQeIx32x31*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQex32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQex32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_750 = Coupling(name = 'GC_750',
                  value = '(CKM3x2*cQeIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQex23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQeIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQex23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM2x2*cQeI323x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQeIx32x31*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQex32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQex32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_751 = Coupling(name = 'GC_751',
                  value = '(CKM3x3*cQeIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQex23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQeIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQex23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM2x3*cQeI323x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQeIx32x31*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQex32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQex32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_752 = Coupling(name = 'GC_752',
                  value = '(-2*cQl3Ix12x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_753 = Coupling(name = 'GC_753',
                  value = '(-2*cQl3Ix12x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_754 = Coupling(name = 'GC_754',
                  value = '(-2*cQl3Ix13x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_755 = Coupling(name = 'GC_755',
                  value = '(-2*cQl3Ix13x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_756 = Coupling(name = 'GC_756',
                  value = '(-2*cQl3Ix21x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_757 = Coupling(name = 'GC_757',
                  value = '(-2*cQl3Ix21x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_758 = Coupling(name = 'GC_758',
                  value = '(-2*cQl3Ix23x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_759 = Coupling(name = 'GC_759',
                  value = '(-2*cQl3Ix23x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_760 = Coupling(name = 'GC_760',
                  value = '(-2*cQl3Ix31x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_761 = Coupling(name = 'GC_761',
                  value = '(-2*cQl3Ix31x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_762 = Coupling(name = 'GC_762',
                  value = '(-2*cQl3Ix32x31*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_763 = Coupling(name = 'GC_763',
                  value = '(-2*cQl3Ix32x32*complexconjugate(CKM3x1))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_764 = Coupling(name = 'GC_764',
                  value = '(CKM3x1*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_765 = Coupling(name = 'GC_765',
                  value = '(2*CKM3x1*cQl3Ix21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix12x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix12x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_766 = Coupling(name = 'GC_766',
                  value = '(CKM3x2*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_767 = Coupling(name = 'GC_767',
                  value = '(2*CKM3x2*cQl3Ix21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix12x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix12x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_768 = Coupling(name = 'GC_768',
                  value = '(CKM3x3*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_769 = Coupling(name = 'GC_769',
                  value = '(2*CKM3x3*cQl3Ix21x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx21x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix21x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix12x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix12x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_770 = Coupling(name = 'GC_770',
                  value = '(CKM3x1*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_771 = Coupling(name = 'GC_771',
                  value = '(2*CKM3x1*cQl3Ix31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix13x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix13x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_772 = Coupling(name = 'GC_772',
                  value = '(CKM3x2*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_773 = Coupling(name = 'GC_773',
                  value = '(2*CKM3x2*cQl3Ix31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix13x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix13x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_774 = Coupling(name = 'GC_774',
                  value = '(CKM3x3*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_775 = Coupling(name = 'GC_775',
                  value = '(2*CKM3x3*cQl3Ix31x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx31x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix31x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix13x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix13x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_776 = Coupling(name = 'GC_776',
                  value = '(CKM3x1*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_777 = Coupling(name = 'GC_777',
                  value = '(2*CKM3x1*cQl3Ix12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix21x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix21x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_778 = Coupling(name = 'GC_778',
                  value = '(CKM3x2*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_779 = Coupling(name = 'GC_779',
                  value = '(2*CKM3x2*cQl3Ix12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix21x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix21x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_780 = Coupling(name = 'GC_780',
                  value = '(CKM3x3*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_781 = Coupling(name = 'GC_781',
                  value = '(2*CKM3x3*cQl3Ix12x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx12x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix12x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix21x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix21x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_782 = Coupling(name = 'GC_782',
                  value = '(CKM3x1*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_783 = Coupling(name = 'GC_783',
                  value = '(2*CKM3x1*cQl3Ix32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix23x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix23x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_784 = Coupling(name = 'GC_784',
                  value = '(CKM3x2*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_785 = Coupling(name = 'GC_785',
                  value = '(2*CKM3x2*cQl3Ix32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix23x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix23x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_786 = Coupling(name = 'GC_786',
                  value = '(CKM3x3*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_787 = Coupling(name = 'GC_787',
                  value = '(2*CKM3x3*cQl3Ix32x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx32x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix32x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix23x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix23x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_788 = Coupling(name = 'GC_788',
                  value = '(CKM3x1*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_789 = Coupling(name = 'GC_789',
                  value = '(2*CKM3x1*cQl3Ix13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix31x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix31x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_790 = Coupling(name = 'GC_790',
                  value = '(CKM3x2*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_791 = Coupling(name = 'GC_791',
                  value = '(2*CKM3x2*cQl3Ix13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix31x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix31x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_792 = Coupling(name = 'GC_792',
                  value = '(CKM3x3*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_793 = Coupling(name = 'GC_793',
                  value = '(2*CKM3x3*cQl3Ix13x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx13x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix13x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix31x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix31x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_794 = Coupling(name = 'GC_794',
                  value = '(CKM3x1*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_795 = Coupling(name = 'GC_795',
                  value = '(2*CKM3x1*cQl3Ix23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x1*cQl3Ix23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x1*cQl3Ix32x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x1*cQl3Ix32x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_796 = Coupling(name = 'GC_796',
                  value = '(CKM3x2*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_797 = Coupling(name = 'GC_797',
                  value = '(2*CKM3x2*cQl3Ix23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x2*cQl3Ix23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x2*cQl3Ix32x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x2*cQl3Ix32x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_798 = Coupling(name = 'GC_798',
                  value = '(CKM3x3*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_799 = Coupling(name = 'GC_799',
                  value = '(2*CKM3x3*cQl3Ix23x31*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMIx23x31*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x1))/Lambda**2 + (2*CKM3x3*cQl3Ix23x32*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x1))/Lambda**2 + (2*CKM3x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x1))/Lambda**2 - (2*CKM1x3*cQl3Ix32x31*complexconjugate(CKM3x1))/Lambda**2 - (2*CKM2x3*cQl3Ix32x32*complexconjugate(CKM3x1))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM1x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (2*CKM2x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_800 = Coupling(name = 'GC_800',
                  value = '-((ctlSIx12x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_801 = Coupling(name = 'GC_801',
                  value = '-((ctlSIx12x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_802 = Coupling(name = 'GC_802',
                  value = '-((ctlSIx13x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_803 = Coupling(name = 'GC_803',
                  value = '-((ctlSIx13x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_804 = Coupling(name = 'GC_804',
                  value = '-((ctlSIx21x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx21x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_805 = Coupling(name = 'GC_805',
                  value = '-((ctlSIx21x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_806 = Coupling(name = 'GC_806',
                  value = '-((ctlSIx23x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_807 = Coupling(name = 'GC_807',
                  value = '-((ctlSIx23x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_808 = Coupling(name = 'GC_808',
                  value = '-((ctlSIx31x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_809 = Coupling(name = 'GC_809',
                  value = '-((ctlSIx31x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_810 = Coupling(name = 'GC_810',
                  value = '-((ctlSIx32x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_811 = Coupling(name = 'GC_811',
                  value = '-((ctlSIx32x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlSx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_812 = Coupling(name = 'GC_812',
                  value = '(ctlTIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_813 = Coupling(name = 'GC_813',
                  value = '-((ctlTIx21x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_814 = Coupling(name = 'GC_814',
                  value = '(4*ctlTIx21x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx121x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_815 = Coupling(name = 'GC_815',
                  value = '(ctlTIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_816 = Coupling(name = 'GC_816',
                  value = '-((ctlTIx12x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_817 = Coupling(name = 'GC_817',
                  value = '(4*ctlTIx12x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx12x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_818 = Coupling(name = 'GC_818',
                  value = '(ctlTIx12x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_819 = Coupling(name = 'GC_819',
                  value = '-((ctlTIx12x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_820 = Coupling(name = 'GC_820',
                  value = '(4*ctlTIx12x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx12x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_821 = Coupling(name = 'GC_821',
                  value = '(ctlTIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_822 = Coupling(name = 'GC_822',
                  value = '-((ctlTIx13x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_823 = Coupling(name = 'GC_823',
                  value = '(4*ctlTIx13x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx13x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_824 = Coupling(name = 'GC_824',
                  value = '(ctlTIx13x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_825 = Coupling(name = 'GC_825',
                  value = '-((ctlTIx13x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_826 = Coupling(name = 'GC_826',
                  value = '(4*ctlTIx13x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx13x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_827 = Coupling(name = 'GC_827',
                  value = '(ctlTIx21x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_828 = Coupling(name = 'GC_828',
                  value = '-((ctlTIx21x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_829 = Coupling(name = 'GC_829',
                  value = '(4*ctlTIx21x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx21x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_830 = Coupling(name = 'GC_830',
                  value = '(ctlTIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_831 = Coupling(name = 'GC_831',
                  value = '-((ctlTIx23x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_832 = Coupling(name = 'GC_832',
                  value = '(4*ctlTIx23x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx23x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_833 = Coupling(name = 'GC_833',
                  value = '(ctlTIx23x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_834 = Coupling(name = 'GC_834',
                  value = '-((ctlTIx23x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_835 = Coupling(name = 'GC_835',
                  value = '(4*ctlTIx23x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx23x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_836 = Coupling(name = 'GC_836',
                  value = '(ctlTIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_837 = Coupling(name = 'GC_837',
                  value = '-((ctlTIx31x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_838 = Coupling(name = 'GC_838',
                  value = '(4*ctlTIx31x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx31x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_839 = Coupling(name = 'GC_839',
                  value = '(ctlTIx31x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_840 = Coupling(name = 'GC_840',
                  value = '-((ctlTIx31x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_841 = Coupling(name = 'GC_841',
                  value = '(4*ctlTIx31x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx31x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_842 = Coupling(name = 'GC_842',
                  value = '(ctlTIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_843 = Coupling(name = 'GC_843',
                  value = '-((ctlTIx32x31*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_844 = Coupling(name = 'GC_844',
                  value = '(4*ctlTIx32x31*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx32x31*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_845 = Coupling(name = 'GC_845',
                  value = '(ctlTIx32x32*complexconjugate(CKM3x1))/Lambda**2 - (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_846 = Coupling(name = 'GC_846',
                  value = '-((ctlTIx32x32*complexconjugate(CKM3x1))/Lambda**2) + (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_847 = Coupling(name = 'GC_847',
                  value = '(4*ctlTIx32x32*complexconjugate(CKM3x1))/Lambda**2 - (4*ctlTx32x32*complex(0,1)*complexconjugate(CKM3x1))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_848 = Coupling(name = 'GC_848',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_849 = Coupling(name = 'GC_849',
                  value = '(CKM3x1*cQeIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQeIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQex21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQeIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQeIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_850 = Coupling(name = 'GC_850',
                  value = '(CKM3x2*cQeIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQeIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQeIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_851 = Coupling(name = 'GC_851',
                  value = '(CKM3x3*cQeIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQeIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQeIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_852 = Coupling(name = 'GC_852',
                  value = '(CKM3x1*cQeIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQeIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQex31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQeIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQeIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_853 = Coupling(name = 'GC_853',
                  value = '(CKM3x2*cQeIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQeIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQeIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_854 = Coupling(name = 'GC_854',
                  value = '(CKM3x3*cQeIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQeIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQeIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_855 = Coupling(name = 'GC_855',
                  value = '(CKM3x1*cQeIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQeIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQex12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQeIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQeIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_856 = Coupling(name = 'GC_856',
                  value = '(CKM3x2*cQeIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQeIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQeIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_857 = Coupling(name = 'GC_857',
                  value = '(CKM3x3*cQeIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQeIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQeIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_858 = Coupling(name = 'GC_858',
                  value = '(CKM3x1*cQeIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQeIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQeIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_859 = Coupling(name = 'GC_859',
                  value = '(CKM3x2*cQeIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeI323x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQeIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQeIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_860 = Coupling(name = 'GC_860',
                  value = '(CKM3x3*cQeIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeI323x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQeIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQeIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_861 = Coupling(name = 'GC_861',
                  value = '(CKM3x1*cQeIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQeIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQex13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQeIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQeIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_862 = Coupling(name = 'GC_862',
                  value = '(CKM3x2*cQeIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQeIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQeIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_863 = Coupling(name = 'GC_863',
                  value = '(CKM3x3*cQeIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQeIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQeIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_864 = Coupling(name = 'GC_864',
                  value = '(CKM3x1*cQeIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQex23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQeIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQex23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM2x1*cQeI323x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQeIx32x31*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQex32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQex32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_865 = Coupling(name = 'GC_865',
                  value = '(CKM3x2*cQeIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQex23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQeIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQex23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM2x2*cQeI323x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQeIx32x31*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQex32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQex32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_866 = Coupling(name = 'GC_866',
                  value = '(CKM3x3*cQeIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQex23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQeIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQex23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM2x3*cQeI323x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQeIx32x31*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQex32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQex32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_867 = Coupling(name = 'GC_867',
                  value = '(-2*cQl3Ix12x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_868 = Coupling(name = 'GC_868',
                  value = '(-2*cQl3Ix12x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_869 = Coupling(name = 'GC_869',
                  value = '(-2*cQl3Ix13x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_870 = Coupling(name = 'GC_870',
                  value = '(-2*cQl3Ix13x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_871 = Coupling(name = 'GC_871',
                  value = '(-2*cQl3Ix21x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_872 = Coupling(name = 'GC_872',
                  value = '(-2*cQl3Ix21x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_873 = Coupling(name = 'GC_873',
                  value = '(-2*cQl3Ix23x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_874 = Coupling(name = 'GC_874',
                  value = '(-2*cQl3Ix23x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_875 = Coupling(name = 'GC_875',
                  value = '(-2*cQl3Ix31x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_876 = Coupling(name = 'GC_876',
                  value = '(-2*cQl3Ix31x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_877 = Coupling(name = 'GC_877',
                  value = '(-2*cQl3Ix32x31*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_878 = Coupling(name = 'GC_878',
                  value = '(-2*cQl3Ix32x32*complexconjugate(CKM3x2))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_879 = Coupling(name = 'GC_879',
                  value = '(CKM3x1*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_880 = Coupling(name = 'GC_880',
                  value = '(2*CKM3x1*cQl3Ix21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix12x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix12x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_881 = Coupling(name = 'GC_881',
                  value = '(CKM3x2*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_882 = Coupling(name = 'GC_882',
                  value = '(2*CKM3x2*cQl3Ix21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix12x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix12x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_883 = Coupling(name = 'GC_883',
                  value = '(CKM3x3*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_884 = Coupling(name = 'GC_884',
                  value = '(2*CKM3x3*cQl3Ix21x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx21x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix21x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix12x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix12x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_885 = Coupling(name = 'GC_885',
                  value = '(CKM3x1*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_886 = Coupling(name = 'GC_886',
                  value = '(2*CKM3x1*cQl3Ix31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix13x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix13x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_887 = Coupling(name = 'GC_887',
                  value = '(CKM3x2*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_888 = Coupling(name = 'GC_888',
                  value = '(2*CKM3x2*cQl3Ix31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix13x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix13x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_889 = Coupling(name = 'GC_889',
                  value = '(CKM3x3*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_890 = Coupling(name = 'GC_890',
                  value = '(2*CKM3x3*cQl3Ix31x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx31x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix31x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix13x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix13x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_891 = Coupling(name = 'GC_891',
                  value = '(CKM3x1*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_892 = Coupling(name = 'GC_892',
                  value = '(2*CKM3x1*cQl3Ix12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix21x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix21x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_893 = Coupling(name = 'GC_893',
                  value = '(CKM3x2*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_894 = Coupling(name = 'GC_894',
                  value = '(CKM3x2*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix21x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix21x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_895 = Coupling(name = 'GC_895',
                  value = '(CKM3x3*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_896 = Coupling(name = 'GC_896',
                  value = '(CKM3x3*cQlMIx12x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix12x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix21x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix21x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_897 = Coupling(name = 'GC_897',
                  value = '(CKM3x1*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_898 = Coupling(name = 'GC_898',
                  value = '(2*CKM3x1*cQl3Ix32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix23x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix23x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_899 = Coupling(name = 'GC_899',
                  value = '(CKM3x2*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_900 = Coupling(name = 'GC_900',
                  value = '(2*CKM3x2*cQl3Ix32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix23x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix23x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_901 = Coupling(name = 'GC_901',
                  value = '(CKM3x3*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_902 = Coupling(name = 'GC_902',
                  value = '(2*CKM3x3*cQl3Ix32x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx32x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix32x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix23x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix23x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_903 = Coupling(name = 'GC_903',
                  value = '(CKM3x1*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_904 = Coupling(name = 'GC_904',
                  value = '(2*CKM3x1*cQl3Ix13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix31x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix31x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_905 = Coupling(name = 'GC_905',
                  value = '(CKM3x2*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_906 = Coupling(name = 'GC_906',
                  value = '(2*CKM3x2*cQl3Ix13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix31x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix31x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_907 = Coupling(name = 'GC_907',
                  value = '(CKM3x3*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_908 = Coupling(name = 'GC_908',
                  value = '(2*CKM3x3*cQl3Ix13x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx13x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix13x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix31x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix31x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_909 = Coupling(name = 'GC_909',
                  value = '(CKM3x1*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_910 = Coupling(name = 'GC_910',
                  value = '(2*CKM3x1*cQl3Ix23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x1*cQl3Ix23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x1*cQl3Ix32x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x1*cQl3Ix32x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_911 = Coupling(name = 'GC_911',
                  value = '(CKM3x2*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_912 = Coupling(name = 'GC_912',
                  value = '(2*CKM3x2*cQl3Ix23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x2*cQl3Ix23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x2*cQl3Ix32x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x2*cQl3Ix32x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_913 = Coupling(name = 'GC_913',
                  value = '(CKM3x3*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_914 = Coupling(name = 'GC_914',
                  value = '(2*CKM3x3*cQl3Ix23x31*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMIx23x31*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x2))/Lambda**2 + (2*CKM3x3*cQl3Ix23x32*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x2))/Lambda**2 + (2*CKM3x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x2))/Lambda**2 - (2*CKM1x3*cQl3Ix32x31*complexconjugate(CKM3x2))/Lambda**2 - (2*CKM2x3*cQl3Ix32x32*complexconjugate(CKM3x2))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM1x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (2*CKM2x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_915 = Coupling(name = 'GC_915',
                  value = '-((ctlSIx12x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_916 = Coupling(name = 'GC_916',
                  value = '-((ctlSIx12x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_917 = Coupling(name = 'GC_917',
                  value = '-((ctlSIx13x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_918 = Coupling(name = 'GC_918',
                  value = '-((ctlSIx13x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_919 = Coupling(name = 'GC_919',
                  value = '-((ctlSIx21x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx21x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_920 = Coupling(name = 'GC_920',
                  value = '-((ctlSIx21x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_921 = Coupling(name = 'GC_921',
                  value = '-((ctlSIx23x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_922 = Coupling(name = 'GC_922',
                  value = '-((ctlSIx23x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_923 = Coupling(name = 'GC_923',
                  value = '-((ctlSIx31x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_924 = Coupling(name = 'GC_924',
                  value = '-((ctlSIx31x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_925 = Coupling(name = 'GC_925',
                  value = '-((ctlSIx32x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_926 = Coupling(name = 'GC_926',
                  value = '-((ctlSIx32x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlSx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ1':1})

GC_927 = Coupling(name = 'GC_927',
                  value = '(ctlTIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_928 = Coupling(name = 'GC_928',
                  value = '-((ctlTIx21x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_929 = Coupling(name = 'GC_929',
                  value = '(4*ctlTIx21x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx121x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_930 = Coupling(name = 'GC_930',
                  value = '(ctlTIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_931 = Coupling(name = 'GC_931',
                  value = '-((ctlTIx12x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_932 = Coupling(name = 'GC_932',
                  value = '(4*ctlTIx12x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx12x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_933 = Coupling(name = 'GC_933',
                  value = '(ctlTIx12x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_934 = Coupling(name = 'GC_934',
                  value = '-((ctlTIx12x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_935 = Coupling(name = 'GC_935',
                  value = '(4*ctlTIx12x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx12x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_936 = Coupling(name = 'GC_936',
                  value = '(ctlTIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_937 = Coupling(name = 'GC_937',
                  value = '-((ctlTIx13x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_938 = Coupling(name = 'GC_938',
                  value = '(4*ctlTIx13x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx13x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_939 = Coupling(name = 'GC_939',
                  value = '(ctlTIx13x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_940 = Coupling(name = 'GC_940',
                  value = '-((ctlTIx13x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_941 = Coupling(name = 'GC_941',
                  value = '(4*ctlTIx13x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx13x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_942 = Coupling(name = 'GC_942',
                  value = '(ctlTIx21x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_943 = Coupling(name = 'GC_943',
                  value = '-((ctlTIx21x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_944 = Coupling(name = 'GC_944',
                  value = '(4*ctlTIx21x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx21x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_945 = Coupling(name = 'GC_945',
                  value = '(ctlTIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_946 = Coupling(name = 'GC_946',
                  value = '-((ctlTIx23x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_947 = Coupling(name = 'GC_947',
                  value = '(4*ctlTIx23x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx23x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_948 = Coupling(name = 'GC_948',
                  value = '(ctlTIx23x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_949 = Coupling(name = 'GC_949',
                  value = '-((ctlTIx23x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_950 = Coupling(name = 'GC_950',
                  value = '(4*ctlTIx23x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx23x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_951 = Coupling(name = 'GC_951',
                  value = '(ctlTIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_952 = Coupling(name = 'GC_952',
                  value = '-((ctlTIx31x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_953 = Coupling(name = 'GC_953',
                  value = '(4*ctlTIx31x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx31x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_954 = Coupling(name = 'GC_954',
                  value = '(ctlTIx31x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_955 = Coupling(name = 'GC_955',
                  value = '-((ctlTIx31x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_956 = Coupling(name = 'GC_956',
                  value = '(4*ctlTIx31x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx31x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_957 = Coupling(name = 'GC_957',
                  value = '(ctlTIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_958 = Coupling(name = 'GC_958',
                  value = '-((ctlTIx32x31*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_959 = Coupling(name = 'GC_959',
                  value = '(4*ctlTIx32x31*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx32x31*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_960 = Coupling(name = 'GC_960',
                  value = '(ctlTIx32x32*complexconjugate(CKM3x2))/Lambda**2 - (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_961 = Coupling(name = 'GC_961',
                  value = '-((ctlTIx32x32*complexconjugate(CKM3x2))/Lambda**2) + (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_962 = Coupling(name = 'GC_962',
                  value = '(4*ctlTIx32x32*complexconjugate(CKM3x2))/Lambda**2 - (4*ctlTx32x32*complex(0,1)*complexconjugate(CKM3x2))/Lambda**2',
                  order = {'CLFV_lequ3':1})

GC_963 = Coupling(name = 'GC_963',
                  value = '(CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_964 = Coupling(name = 'GC_964',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_965 = Coupling(name = 'GC_965',
                  value = '(CKM3x1*cQeIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQeIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQeIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_966 = Coupling(name = 'GC_966',
                  value = '(CKM3x2*cQeIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQeIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQeIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_967 = Coupling(name = 'GC_967',
                  value = '(CKM3x3*cQeIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQeIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQeIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_968 = Coupling(name = 'GC_968',
                  value = '(CKM3x1*cQeIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQeIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQeIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_969 = Coupling(name = 'GC_969',
                  value = '(CKM3x2*cQeIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQeIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQeIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_970 = Coupling(name = 'GC_970',
                  value = '(CKM3x3*cQeIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQeIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQeIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_971 = Coupling(name = 'GC_971',
                  value = '(CKM3x1*cQeIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQeIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQeIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_972 = Coupling(name = 'GC_972',
                  value = '(CKM3x2*cQeIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQeIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQeIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_973 = Coupling(name = 'GC_973',
                  value = '(CKM3x3*cQeIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQeIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQeIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_974 = Coupling(name = 'GC_974',
                  value = '(CKM3x1*cQeIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeI323x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQeIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQeIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_975 = Coupling(name = 'GC_975',
                  value = '(CKM3x2*cQeIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeI323x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQeIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQeIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_976 = Coupling(name = 'GC_976',
                  value = '(CKM3x3*cQeIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeI323x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQeIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQeIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_977 = Coupling(name = 'GC_977',
                  value = '(CKM3x1*cQeIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQeIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQeIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_978 = Coupling(name = 'GC_978',
                  value = '(CKM3x2*cQeIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQeIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQeIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_979 = Coupling(name = 'GC_979',
                  value = '(CKM3x3*cQeIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQeIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQeIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_980 = Coupling(name = 'GC_980',
                  value = '(CKM3x1*cQeIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQex23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQeIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQex23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM2x1*cQeI323x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQeIx32x31*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQex32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQex32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_981 = Coupling(name = 'GC_981',
                  value = '(CKM3x2*cQeIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQex23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQeIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQex23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM2x2*cQeI323x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQeIx32x31*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQex32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQex32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_982 = Coupling(name = 'GC_982',
                  value = '(CKM3x3*cQeIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQex23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQeIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQex23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM2x3*cQeI323x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQeIx32x31*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQex32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQex32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_eq':1})

GC_983 = Coupling(name = 'GC_983',
                  value = '(-2*cQl3Ix12x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_984 = Coupling(name = 'GC_984',
                  value = '(-2*cQl3Ix12x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_985 = Coupling(name = 'GC_985',
                  value = '(-2*cQl3Ix13x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_986 = Coupling(name = 'GC_986',
                  value = '(-2*cQl3Ix13x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_987 = Coupling(name = 'GC_987',
                  value = '(-2*cQl3Ix21x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_988 = Coupling(name = 'GC_988',
                  value = '(-2*cQl3Ix21x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_989 = Coupling(name = 'GC_989',
                  value = '(-2*cQl3Ix23x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_990 = Coupling(name = 'GC_990',
                  value = '(-2*cQl3Ix23x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_991 = Coupling(name = 'GC_991',
                  value = '(-2*cQl3Ix31x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_992 = Coupling(name = 'GC_992',
                  value = '(-2*cQl3Ix31x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_993 = Coupling(name = 'GC_993',
                  value = '(-2*cQl3Ix32x31*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_994 = Coupling(name = 'GC_994',
                  value = '(-2*cQl3Ix32x32*complexconjugate(CKM3x3))/Lambda**2 + (2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_995 = Coupling(name = 'GC_995',
                  value = '(CKM3x1*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_996 = Coupling(name = 'GC_996',
                  value = '(2*CKM3x1*cQl3Ix21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix12x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix12x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_997 = Coupling(name = 'GC_997',
                  value = '(CKM3x2*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_998 = Coupling(name = 'GC_998',
                  value = '(2*CKM3x2*cQl3Ix21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix12x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix12x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_999 = Coupling(name = 'GC_999',
                  value = '(CKM3x3*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                  order = {'CLFV_Ql':1})

GC_1000 = Coupling(name = 'GC_1000',
                   value = '(2*CKM3x3*cQl3Ix21x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx21x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix21x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx21x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix12x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix12x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx12x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1001 = Coupling(name = 'GC_1001',
                   value = '(CKM3x1*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1002 = Coupling(name = 'GC_1002',
                   value = '(2*CKM3x1*cQl3Ix31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix13x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix13x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1003 = Coupling(name = 'GC_1003',
                   value = '(CKM3x2*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1004 = Coupling(name = 'GC_1004',
                   value = '(2*CKM3x2*cQl3Ix31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix13x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix13x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1005 = Coupling(name = 'GC_1005',
                   value = '(CKM3x3*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1006 = Coupling(name = 'GC_1006',
                   value = '(2*CKM3x3*cQl3Ix31x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx31x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix31x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx31x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix13x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix13x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx13x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1007 = Coupling(name = 'GC_1007',
                   value = '(CKM3x1*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1008 = Coupling(name = 'GC_1008',
                   value = '(2*CKM3x1*cQl3Ix12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix21x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix21x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1009 = Coupling(name = 'GC_1009',
                   value = '(CKM3x2*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1010 = Coupling(name = 'GC_1010',
                   value = '(2*CKM3x2*cQl3Ix12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix21x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix21x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1011 = Coupling(name = 'GC_1011',
                   value = '(CKM3x3*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1012 = Coupling(name = 'GC_1012',
                   value = '(2*CKM3x3*cQl3Ix12x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx12x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx12x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix12x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx12x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx12x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix21x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix21x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx21x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1013 = Coupling(name = 'GC_1013',
                   value = '(CKM3x1*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1014 = Coupling(name = 'GC_1014',
                   value = '(2*CKM3x1*cQl3Ix32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix23x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix23x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1015 = Coupling(name = 'GC_1015',
                   value = '(CKM3x2*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1016 = Coupling(name = 'GC_1016',
                   value = '(2*CKM3x2*cQl3Ix32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix23x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix23x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1017 = Coupling(name = 'GC_1017',
                   value = '(CKM3x3*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1018 = Coupling(name = 'GC_1018',
                   value = '(2*CKM3x3*cQl3Ix32x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx32x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix32x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx32x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix23x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix23x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx23x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1019 = Coupling(name = 'GC_1019',
                   value = '(CKM3x1*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1020 = Coupling(name = 'GC_1020',
                   value = '(2*CKM3x1*cQl3Ix13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix31x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix31x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1021 = Coupling(name = 'GC_1021',
                   value = '(CKM3x2*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1022 = Coupling(name = 'GC_1022',
                   value = '(2*CKM3x2*cQl3Ix13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix31x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix31x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1023 = Coupling(name = 'GC_1023',
                   value = '(CKM3x3*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1024 = Coupling(name = 'GC_1024',
                   value = '(2*CKM3x3*cQl3Ix13x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx13x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx13x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix13x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx13x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx13x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix31x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix31x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx31x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1025 = Coupling(name = 'GC_1025',
                   value = '(CKM3x1*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1026 = Coupling(name = 'GC_1026',
                   value = '(2*CKM3x1*cQl3Ix23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x1*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x1*cQl3Ix23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x1*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x1*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x1*cQl3Ix32x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x1*cQl3Ix32x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x1*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x1*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x1*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x1*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x1*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x1*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1027 = Coupling(name = 'GC_1027',
                   value = '(CKM3x2*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1028 = Coupling(name = 'GC_1028',
                   value = '(2*CKM3x2*cQl3Ix23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x2*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x2*cQl3Ix23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x2*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x2*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x2*cQl3Ix32x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x2*cQl3Ix32x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x2*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x2*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x2*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x2*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x2*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x2*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1029 = Coupling(name = 'GC_1029',
                   value = '(CKM3x3*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1030 = Coupling(name = 'GC_1030',
                   value = '(2*CKM3x3*cQl3Ix23x31*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMIx23x31*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3x23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (CKM3x3*cQlMx23x31*complex(0,1)*complexconjugate(CKM1x3))/Lambda**2 + (2*CKM3x3*cQl3Ix23x32*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMIx23x32*complexconjugate(CKM2x3))/Lambda**2 + (2*CKM3x3*cQl3x23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 + (CKM3x3*cQlMx23x32*complex(0,1)*complexconjugate(CKM2x3))/Lambda**2 - (2*CKM1x3*cQl3Ix32x31*complexconjugate(CKM3x3))/Lambda**2 - (2*CKM2x3*cQl3Ix32x32*complexconjugate(CKM3x3))/Lambda**2 - (CKM1x3*cQlMIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (CKM2x3*cQlMIx32x32*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM1x3*cQl3x32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (2*CKM2x3*cQl3x32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM1x3*cQlMx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2 + (CKM2x3*cQlMx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_Ql':1})

GC_1031 = Coupling(name = 'GC_1031',
                   value = '-((ctlSIx12x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1032 = Coupling(name = 'GC_1032',
                   value = '-((ctlSIx12x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1033 = Coupling(name = 'GC_1033',
                   value = '-((ctlSIx13x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1034 = Coupling(name = 'GC_1034',
                   value = '-((ctlSIx13x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1035 = Coupling(name = 'GC_1035',
                   value = '-((ctlSIx21x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx21x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1036 = Coupling(name = 'GC_1036',
                   value = '-((ctlSIx21x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1037 = Coupling(name = 'GC_1037',
                   value = '-((ctlSIx23x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1038 = Coupling(name = 'GC_1038',
                   value = '-((ctlSIx23x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1039 = Coupling(name = 'GC_1039',
                   value = '-((ctlSIx31x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1040 = Coupling(name = 'GC_1040',
                   value = '-((ctlSIx31x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1041 = Coupling(name = 'GC_1041',
                   value = '-((ctlSIx32x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1042 = Coupling(name = 'GC_1042',
                   value = '-((ctlSIx32x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlSx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ1':1})

GC_1043 = Coupling(name = 'GC_1043',
                   value = '(ctlTIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1044 = Coupling(name = 'GC_1044',
                   value = '-((ctlTIx21x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx121x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1045 = Coupling(name = 'GC_1045',
                   value = '(4*ctlTIx21x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx121x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1046 = Coupling(name = 'GC_1046',
                   value = '(ctlTIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1047 = Coupling(name = 'GC_1047',
                   value = '-((ctlTIx12x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1048 = Coupling(name = 'GC_1048',
                   value = '(4*ctlTIx12x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx12x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1049 = Coupling(name = 'GC_1049',
                   value = '(ctlTIx12x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1050 = Coupling(name = 'GC_1050',
                   value = '-((ctlTIx12x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1051 = Coupling(name = 'GC_1051',
                   value = '(4*ctlTIx12x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx12x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1052 = Coupling(name = 'GC_1052',
                   value = '(ctlTIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1053 = Coupling(name = 'GC_1053',
                   value = '-((ctlTIx13x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1054 = Coupling(name = 'GC_1054',
                   value = '(4*ctlTIx13x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx13x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1055 = Coupling(name = 'GC_1055',
                   value = '(ctlTIx13x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1056 = Coupling(name = 'GC_1056',
                   value = '-((ctlTIx13x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1057 = Coupling(name = 'GC_1057',
                   value = '(4*ctlTIx13x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx13x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1058 = Coupling(name = 'GC_1058',
                   value = '(ctlTIx21x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1059 = Coupling(name = 'GC_1059',
                   value = '-((ctlTIx21x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1060 = Coupling(name = 'GC_1060',
                   value = '(4*ctlTIx21x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx21x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1061 = Coupling(name = 'GC_1061',
                   value = '(ctlTIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1062 = Coupling(name = 'GC_1062',
                   value = '-((ctlTIx23x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1063 = Coupling(name = 'GC_1063',
                   value = '(4*ctlTIx23x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx23x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1064 = Coupling(name = 'GC_1064',
                   value = '(ctlTIx23x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1065 = Coupling(name = 'GC_1065',
                   value = '-((ctlTIx23x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1066 = Coupling(name = 'GC_1066',
                   value = '(4*ctlTIx23x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx23x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1067 = Coupling(name = 'GC_1067',
                   value = '(ctlTIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1068 = Coupling(name = 'GC_1068',
                   value = '-((ctlTIx31x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1069 = Coupling(name = 'GC_1069',
                   value = '(4*ctlTIx31x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx31x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1070 = Coupling(name = 'GC_1070',
                   value = '(ctlTIx31x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1071 = Coupling(name = 'GC_1071',
                   value = '-((ctlTIx31x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1072 = Coupling(name = 'GC_1072',
                   value = '(4*ctlTIx31x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx31x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1073 = Coupling(name = 'GC_1073',
                   value = '(ctlTIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1074 = Coupling(name = 'GC_1074',
                   value = '-((ctlTIx32x31*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1075 = Coupling(name = 'GC_1075',
                   value = '(4*ctlTIx32x31*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx32x31*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1076 = Coupling(name = 'GC_1076',
                   value = '(ctlTIx32x32*complexconjugate(CKM3x3))/Lambda**2 - (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1077 = Coupling(name = 'GC_1077',
                   value = '-((ctlTIx32x32*complexconjugate(CKM3x3))/Lambda**2) + (ctlTx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

GC_1078 = Coupling(name = 'GC_1078',
                   value = '(4*ctlTIx32x32*complexconjugate(CKM3x3))/Lambda**2 - (4*ctlTx32x32*complex(0,1)*complexconjugate(CKM3x3))/Lambda**2',
                   order = {'CLFV_lequ3':1})

