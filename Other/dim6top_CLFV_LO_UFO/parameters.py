# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 15 Nov 2019 18:52:47



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

cQlMx12x31 = Parameter(name = 'cQlMx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 1 ])

cQlMIx12x31 = Parameter(name = 'cQlMIx12x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx12x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 2 ])

cQlMx21x31 = Parameter(name = 'cQlMx21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 3 ])

cQlMIx21x31 = Parameter(name = 'cQlMIx21x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx21x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 4 ])

cQl3x12x31 = Parameter(name = 'cQl3x12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 5 ])

cQl3Ix12x31 = Parameter(name = 'cQl3Ix12x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix12x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 6 ])

cQl3x21x31 = Parameter(name = 'cQl3x21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 7 ])

cQl3Ix21x31 = Parameter(name = 'cQl3Ix21x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix21x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 8 ])

cQex12x31 = Parameter(name = 'cQex12x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex12x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 9 ])

cQeIx12x31 = Parameter(name = 'cQeIx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 10 ])

cQex21x31 = Parameter(name = 'cQex21x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex21x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 11 ])

cQeIx21x31 = Parameter(name = 'cQeIx21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 12 ])

ctlx12x31 = Parameter(name = 'ctlx12x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx12x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 13 ])

ctlIx12x31 = Parameter(name = 'ctlIx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 14 ])

ctlx21x31 = Parameter(name = 'ctlx21x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx21x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 15 ])

ctlIx21x31 = Parameter(name = 'ctlIx21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 16 ])

ctex12x31 = Parameter(name = 'ctex12x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex12x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 17 ])

cteIx12x31 = Parameter(name = 'cteIx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 18 ])

ctex21x31 = Parameter(name = 'ctex21x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex21x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 19 ])

cteIx21x31 = Parameter(name = 'cteIx21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 20 ])

cQlMx13x31 = Parameter(name = 'cQlMx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 21 ])

cQlMIx13x31 = Parameter(name = 'cQlMIx13x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx13x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 22 ])

cQlMx31x31 = Parameter(name = 'cQlMx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 23 ])

cQlMIx31x31 = Parameter(name = 'cQlMIx31x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx31x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 24 ])

cQl3x13x31 = Parameter(name = 'cQl3x13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 25 ])

cQl3Ix13x31 = Parameter(name = 'cQl3Ix13x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix13x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 26 ])

cQl3x31x31 = Parameter(name = 'cQl3x31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 27 ])

cQl3Ix31x31 = Parameter(name = 'cQl3Ix31x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix31x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 28 ])

cQex13x31 = Parameter(name = 'cQex13x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex13x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 29 ])

cQeIx13x31 = Parameter(name = 'cQeIx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 30 ])

cQex31x31 = Parameter(name = 'cQex31x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex31x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 31 ])

cQeIx31x31 = Parameter(name = 'cQeIx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 32 ])

ctlx13x31 = Parameter(name = 'ctlx13x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx13x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 33 ])

ctlIx13x31 = Parameter(name = 'ctlIx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 34 ])

ctlx31x31 = Parameter(name = 'ctlx31x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx31x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 35 ])

ctlIx31x31 = Parameter(name = 'ctlIx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 36 ])

ctex13x31 = Parameter(name = 'ctex13x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex13x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 37 ])

cteIx13x31 = Parameter(name = 'cteIx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 38 ])

ctex31x31 = Parameter(name = 'ctex31x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex31x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 39 ])

cteIx31x31 = Parameter(name = 'cteIx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 40 ])

cQlMx23x31 = Parameter(name = 'cQlMx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 41 ])

cQlMIx23x31 = Parameter(name = 'cQlMIx23x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx23x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 42 ])

cQlMx32x31 = Parameter(name = 'cQlMx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 43 ])

cQlMIx32x31 = Parameter(name = 'cQlMIx32x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx32x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 44 ])

cQl3x23x31 = Parameter(name = 'cQl3x23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 45 ])

cQl3Ix23x31 = Parameter(name = 'cQl3Ix23x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix23x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 46 ])

cQl3x32x31 = Parameter(name = 'cQl3x32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 47 ])

cQl3Ix32x31 = Parameter(name = 'cQl3Ix32x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix32x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 48 ])

cQex23x31 = Parameter(name = 'cQex23x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex23x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 49 ])

cQeIx23x31 = Parameter(name = 'cQeIx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 50 ])

cQex32x31 = Parameter(name = 'cQex32x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex32x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 51 ])

cQeIx32x31 = Parameter(name = 'cQeIx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 52 ])

ctlx23x31 = Parameter(name = 'ctlx23x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx23x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 53 ])

ctlIx23x31 = Parameter(name = 'ctlIx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 54 ])

ctlx32x31 = Parameter(name = 'ctlx32x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx32x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 55 ])

ctlIx32x31 = Parameter(name = 'ctlIx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 56 ])

ctex23x31 = Parameter(name = 'ctex23x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex23x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 57 ])

cteIx23x31 = Parameter(name = 'cteIx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 58 ])

ctex32x31 = Parameter(name = 'ctex32x31',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex32x31}',
                      lhablock = 'CLFV',
                      lhacode = [ 59 ])

cteIx32x31 = Parameter(name = 'cteIx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 60 ])

ctlSx12x31 = Parameter(name = 'ctlSx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 61 ])

ctlSIx12x31 = Parameter(name = 'ctlSIx12x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx12x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 62 ])

ctlSx21x31 = Parameter(name = 'ctlSx21x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx21x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 63 ])

ctlSIx21x31 = Parameter(name = 'ctlSIx21x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx21x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 64 ])

ctlSx12x13 = Parameter(name = 'ctlSx12x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx12x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 65 ])

ctlSIx12x13 = Parameter(name = 'ctlSIx12x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx12x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 66 ])

ctlSx21x13 = Parameter(name = 'ctlSx21x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx21x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 67 ])

ctlSIx21x13 = Parameter(name = 'ctlSIx21x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx21x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 68 ])

ctlTx12x31 = Parameter(name = 'ctlTx12x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx12x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 69 ])

ctlTIx12x31 = Parameter(name = 'ctlTIx12x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx12x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 70 ])

ctlTx121x31 = Parameter(name = 'ctlTx121x31',
                        nature = 'external',
                        type = 'real',
                        value = 0.5,
                        texname = '\\text{ctlTx121x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 71 ])

ctlTIx21x31 = Parameter(name = 'ctlTIx21x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx21x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 72 ])

ctlTx12x13 = Parameter(name = 'ctlTx12x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx12x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 73 ])

ctlTIx12x13 = Parameter(name = 'ctlTIx12x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx12x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 74 ])

ctlTx21x13 = Parameter(name = 'ctlTx21x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx21x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 75 ])

ctlTIx21x13 = Parameter(name = 'ctlTIx21x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx21x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 76 ])

ctlSx13x31 = Parameter(name = 'ctlSx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 77 ])

ctlSIx13x31 = Parameter(name = 'ctlSIx13x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx13x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 78 ])

ctlSx31x31 = Parameter(name = 'ctlSx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 79 ])

ctlSIx31x31 = Parameter(name = 'ctlSIx31x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx31x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 80 ])

ctlSx13x13 = Parameter(name = 'ctlSx13x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx13x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 81 ])

ctlSIx13x13 = Parameter(name = 'ctlSIx13x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx13x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 82 ])

ctlSx31x13 = Parameter(name = 'ctlSx31x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx31x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 83 ])

ctlSIx31x13 = Parameter(name = 'ctlSIx31x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx31x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 84 ])

ctlTx13x31 = Parameter(name = 'ctlTx13x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx13x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 85 ])

ctlTIx13x31 = Parameter(name = 'ctlTIx13x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx13x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 86 ])

ctlTx31x31 = Parameter(name = 'ctlTx31x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx31x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 87 ])

ctlTIx31x31 = Parameter(name = 'ctlTIx31x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx31x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 88 ])

ctlTx13x13 = Parameter(name = 'ctlTx13x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx13x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 89 ])

ctlTIx13x13 = Parameter(name = 'ctlTIx13x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx13x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 90 ])

ctlTx31x13 = Parameter(name = 'ctlTx31x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx31x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 91 ])

ctlTIx31x13 = Parameter(name = 'ctlTIx31x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx31x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 92 ])

ctlSx23x31 = Parameter(name = 'ctlSx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 93 ])

ctlSIx23x31 = Parameter(name = 'ctlSIx23x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx23x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 94 ])

ctlSx32x31 = Parameter(name = 'ctlSx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 95 ])

ctlSIx32x31 = Parameter(name = 'ctlSIx32x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx32x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 96 ])

ctlSx23x13 = Parameter(name = 'ctlSx23x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx23x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 97 ])

ctlSIx23x13 = Parameter(name = 'ctlSIx23x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx23x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 98 ])

ctlSx32x13 = Parameter(name = 'ctlSx32x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx32x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 99 ])

ctlSIx32x13 = Parameter(name = 'ctlSIx32x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx32x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 100 ])

ctlTx23x31 = Parameter(name = 'ctlTx23x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx23x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 101 ])

ctlTIx23x31 = Parameter(name = 'ctlTIx23x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx23x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 102 ])

ctlTx32x31 = Parameter(name = 'ctlTx32x31',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx32x31}',
                       lhablock = 'CLFV',
                       lhacode = [ 103 ])

ctlTIx32x31 = Parameter(name = 'ctlTIx32x31',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx32x31}',
                        lhablock = 'CLFV',
                        lhacode = [ 104 ])

ctlTx23x13 = Parameter(name = 'ctlTx23x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx23x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 105 ])

ctlTIx23x13 = Parameter(name = 'ctlTIx23x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx23x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 106 ])

ctlTx32x13 = Parameter(name = 'ctlTx32x13',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx32x13}',
                       lhablock = 'CLFV',
                       lhacode = [ 107 ])

ctlTIx32x13 = Parameter(name = 'ctlTIx32x13',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx32x13}',
                        lhablock = 'CLFV',
                        lhacode = [ 108 ])

cQlMx12x32 = Parameter(name = 'cQlMx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 109 ])

cQlMIx12x32 = Parameter(name = 'cQlMIx12x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx12x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 110 ])

cQlMx21x32 = Parameter(name = 'cQlMx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 111 ])

cQlMIx21x32 = Parameter(name = 'cQlMIx21x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx21x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 112 ])

cQl3x12x32 = Parameter(name = 'cQl3x12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 113 ])

cQl3Ix12x32 = Parameter(name = 'cQl3Ix12x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix12x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 114 ])

cQl3x21x32 = Parameter(name = 'cQl3x21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 115 ])

cQl3Ix21x32 = Parameter(name = 'cQl3Ix21x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix21x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 116 ])

cQex12x32 = Parameter(name = 'cQex12x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex12x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 117 ])

cQeIx12x32 = Parameter(name = 'cQeIx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 118 ])

cQex21x32 = Parameter(name = 'cQex21x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex21x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 119 ])

cQeIx21x32 = Parameter(name = 'cQeIx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 120 ])

ctlx12x32 = Parameter(name = 'ctlx12x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx12x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 121 ])

ctlIx12x32 = Parameter(name = 'ctlIx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 122 ])

ctlx21x32 = Parameter(name = 'ctlx21x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx21x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 123 ])

ctlIx21x32 = Parameter(name = 'ctlIx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 124 ])

ctex12x32 = Parameter(name = 'ctex12x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex12x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 125 ])

cteIx12x32 = Parameter(name = 'cteIx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 126 ])

ctex21x32 = Parameter(name = 'ctex21x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex21x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 127 ])

cteIx21x32 = Parameter(name = 'cteIx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 128 ])

cQlMx13x32 = Parameter(name = 'cQlMx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 129 ])

cQlMIx13x32 = Parameter(name = 'cQlMIx13x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx13x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 130 ])

cQlMx31x32 = Parameter(name = 'cQlMx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 131 ])

cQlMIx31x32 = Parameter(name = 'cQlMIx31x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx31x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 132 ])

cQl3x13x32 = Parameter(name = 'cQl3x13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 133 ])

cQl3Ix13x32 = Parameter(name = 'cQl3Ix13x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix13x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 134 ])

cQl3x31x32 = Parameter(name = 'cQl3x31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 135 ])

cQl3Ix31x32 = Parameter(name = 'cQl3Ix31x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix31x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 136 ])

cQex13x32 = Parameter(name = 'cQex13x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex13x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 137 ])

cQeIx13x32 = Parameter(name = 'cQeIx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 138 ])

cQex31x32 = Parameter(name = 'cQex31x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex31x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 139 ])

cQeIx31x32 = Parameter(name = 'cQeIx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 140 ])

ctlx13x32 = Parameter(name = 'ctlx13x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx13x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 141 ])

ctlIx13x32 = Parameter(name = 'ctlIx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 142 ])

ctlx31x32 = Parameter(name = 'ctlx31x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx31x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 143 ])

ctlIx31x32 = Parameter(name = 'ctlIx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 144 ])

ctex13x32 = Parameter(name = 'ctex13x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex13x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 145 ])

cteIx13x32 = Parameter(name = 'cteIx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 146 ])

ctex31x32 = Parameter(name = 'ctex31x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex31x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 147 ])

cteIx31x32 = Parameter(name = 'cteIx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 148 ])

cQlMx23x32 = Parameter(name = 'cQlMx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 149 ])

cQlMIx23x32 = Parameter(name = 'cQlMIx23x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx23x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 150 ])

cQlMx32x32 = Parameter(name = 'cQlMx32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQlMx32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 151 ])

cQlMIx32x32 = Parameter(name = 'cQlMIx32x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQlMIx32x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 152 ])

cQl3x23x32 = Parameter(name = 'cQl3x23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 153 ])

cQl3Ix23x32 = Parameter(name = 'cQl3Ix23x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix23x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 154 ])

cQl3x32x32 = Parameter(name = 'cQl3x32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{cQl3x32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 155 ])

cQl3Ix32x32 = Parameter(name = 'cQl3Ix32x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{cQl3Ix32x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 156 ])

cQex23x32 = Parameter(name = 'cQex23x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex23x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 157 ])

cQeIx23x32 = Parameter(name = 'cQeIx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeIx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 158 ])

cQex32x32 = Parameter(name = 'cQex32x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{cQex32x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 159 ])

cQeI323x32 = Parameter(name = 'cQeI323x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cQeI323x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 160 ])

ctlx23x32 = Parameter(name = 'ctlx23x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx23x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 161 ])

ctlIx23x32 = Parameter(name = 'ctlIx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 162 ])

ctlx32x32 = Parameter(name = 'ctlx32x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctlx32x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 163 ])

ctlIx32x32 = Parameter(name = 'ctlIx32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{ctlIx32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 164 ])

ctex23x32 = Parameter(name = 'ctex23x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex23x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 165 ])

cteIx23x32 = Parameter(name = 'cteIx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 166 ])

ctex32x32 = Parameter(name = 'ctex32x32',
                      nature = 'external',
                      type = 'real',
                      value = 0.5,
                      texname = '\\text{ctex32x32}',
                      lhablock = 'CLFV',
                      lhacode = [ 167 ])

cteIx32x32 = Parameter(name = 'cteIx32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0,
                       texname = '\\text{cteIx32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 168 ])

ctlSx12x32 = Parameter(name = 'ctlSx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 169 ])

ctlSIx12x32 = Parameter(name = 'ctlSIx12x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx12x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 170 ])

ctlSx21x32 = Parameter(name = 'ctlSx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 171 ])

ctlSIx21x32 = Parameter(name = 'ctlSIx21x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx21x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 172 ])

ctlSx12x23 = Parameter(name = 'ctlSx12x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx12x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 173 ])

ctlSIx12x23 = Parameter(name = 'ctlSIx12x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx12x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 174 ])

ctlSx21x23 = Parameter(name = 'ctlSx21x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx21x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 175 ])

ctlSIx21x23 = Parameter(name = 'ctlSIx21x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx21x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 176 ])

ctlTx12x32 = Parameter(name = 'ctlTx12x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx12x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 177 ])

ctlTIx12x32 = Parameter(name = 'ctlTIx12x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx12x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 178 ])

ctlTx21x32 = Parameter(name = 'ctlTx21x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx21x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 179 ])

ctlTIx21x32 = Parameter(name = 'ctlTIx21x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx21x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 180 ])

ctlTx12x23 = Parameter(name = 'ctlTx12x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx12x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 181 ])

ctlTIx12x23 = Parameter(name = 'ctlTIx12x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx12x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 182 ])

ctlTx21x23 = Parameter(name = 'ctlTx21x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx21x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 183 ])

ctlTIx21x23 = Parameter(name = 'ctlTIx21x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx21x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 184 ])

ctlSx13x32 = Parameter(name = 'ctlSx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 185 ])

ctlSIx13x32 = Parameter(name = 'ctlSIx13x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx13x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 186 ])

ctlSx31x32 = Parameter(name = 'ctlSx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 187 ])

ctlSIx31x32 = Parameter(name = 'ctlSIx31x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx31x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 188 ])

ctlSx13x23 = Parameter(name = 'ctlSx13x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx13x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 189 ])

ctlSIx13x23 = Parameter(name = 'ctlSIx13x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx13x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 190 ])

ctlSx31x23 = Parameter(name = 'ctlSx31x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx31x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 191 ])

ctlSIx31x23 = Parameter(name = 'ctlSIx31x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx31x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 192 ])

ctlTx13x32 = Parameter(name = 'ctlTx13x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx13x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 193 ])

ctlTIx13x32 = Parameter(name = 'ctlTIx13x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx13x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 194 ])

ctlTx31x32 = Parameter(name = 'ctlTx31x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx31x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 195 ])

ctlTIx31x32 = Parameter(name = 'ctlTIx31x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx31x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 196 ])

ctlTx13x23 = Parameter(name = 'ctlTx13x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx13x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 197 ])

ctlTIx13x23 = Parameter(name = 'ctlTIx13x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx13x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 198 ])

ctlTx31x23 = Parameter(name = 'ctlTx31x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx31x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 199 ])

ctlTIx31x23 = Parameter(name = 'ctlTIx31x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx31x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 200 ])

ctlSx23x32 = Parameter(name = 'ctlSx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 201 ])

ctlSIx23x32 = Parameter(name = 'ctlSIx23x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx23x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 202 ])

ctlSx32x32 = Parameter(name = 'ctlSx32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 203 ])

ctlSIx32x32 = Parameter(name = 'ctlSIx32x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx32x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 204 ])

ctlSx23x23 = Parameter(name = 'ctlSx23x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx23x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 205 ])

ctlSIx23x23 = Parameter(name = 'ctlSIx23x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx23x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 206 ])

ctlSx32x23 = Parameter(name = 'ctlSx32x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlSx32x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 207 ])

ctlSIx32x23 = Parameter(name = 'ctlSIx32x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlSIx32x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 208 ])

ctlTx23x32 = Parameter(name = 'ctlTx23x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx23x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 209 ])

ctlTIx23x32 = Parameter(name = 'ctlTIx23x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx23x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 210 ])

ctlTx32x32 = Parameter(name = 'ctlTx32x32',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx32x32}',
                       lhablock = 'CLFV',
                       lhacode = [ 211 ])

ctlTIx32x32 = Parameter(name = 'ctlTIx32x32',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx32x32}',
                        lhablock = 'CLFV',
                        lhacode = [ 212 ])

ctlTx23x23 = Parameter(name = 'ctlTx23x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx23x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 213 ])

ctlTIx23x23 = Parameter(name = 'ctlTIx23x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx23x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 214 ])

ctlTx32x23 = Parameter(name = 'ctlTx32x23',
                       nature = 'external',
                       type = 'real',
                       value = 0.5,
                       texname = '\\text{ctlTx32x23}',
                       lhablock = 'CLFV',
                       lhacode = [ 215 ])

ctlTIx32x23 = Parameter(name = 'ctlTIx32x23',
                        nature = 'external',
                        type = 'real',
                        value = 0,
                        texname = '\\text{ctlTIx32x23}',
                        lhablock = 'CLFV',
                        lhacode = [ 216 ])

Lambda = Parameter(name = 'Lambda',
                   nature = 'external',
                   type = 'real',
                   value = 1000,
                   texname = '\\text{Lambda}',
                   lhablock = 'DIM6',
                   lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172.5,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172.5,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

sq2 = Parameter(name = 'sq2',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(2)',
                texname = '\\text{sq2}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

