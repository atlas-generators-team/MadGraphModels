# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 15 Nov 2019 18:52:47


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_2 = Vertex(name = 'V_2',
             particles = [ P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSS1 ],
             couplings = {(0,0):C.GC_624})

V_3 = Vertex(name = 'V_3',
             particles = [ P.ghA, P.ghWm__tilde__, P.W__minus__ ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_3})

V_4 = Vertex(name = 'V_4',
             particles = [ P.ghA, P.ghWp__tilde__, P.W__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_4})

V_5 = Vertex(name = 'V_5',
             particles = [ P.ghWm, P.ghA__tilde__, P.W__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_3})

V_6 = Vertex(name = 'V_6',
             particles = [ P.ghWm, P.ghWm__tilde__, P.H ],
             color = [ '1' ],
             lorentz = [ L.UUS1 ],
             couplings = {(0,0):C.GC_625})

V_7 = Vertex(name = 'V_7',
             particles = [ P.ghWm, P.ghWm__tilde__, P.a ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_4})

V_8 = Vertex(name = 'V_8',
             particles = [ P.ghWm, P.ghWm__tilde__, P.Z ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_614})

V_9 = Vertex(name = 'V_9',
             particles = [ P.ghWm, P.ghZ__tilde__, P.W__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_613})

V_10 = Vertex(name = 'V_10',
              particles = [ P.ghWp, P.ghA__tilde__, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_4})

V_11 = Vertex(name = 'V_11',
              particles = [ P.ghWp, P.ghWp__tilde__, P.H ],
              color = [ '1' ],
              lorentz = [ L.UUS1 ],
              couplings = {(0,0):C.GC_625})

V_12 = Vertex(name = 'V_12',
              particles = [ P.ghWp, P.ghWp__tilde__, P.a ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_3})

V_13 = Vertex(name = 'V_13',
              particles = [ P.ghWp, P.ghWp__tilde__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_613})

V_14 = Vertex(name = 'V_14',
              particles = [ P.ghWp, P.ghZ__tilde__, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_614})

V_15 = Vertex(name = 'V_15',
              particles = [ P.ghZ, P.ghWm__tilde__, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_613})

V_16 = Vertex(name = 'V_16',
              particles = [ P.ghZ, P.ghWp__tilde__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_614})

V_17 = Vertex(name = 'V_17',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H ],
              color = [ '1' ],
              lorentz = [ L.UUS1 ],
              couplings = {(0,0):C.GC_627})

V_18 = Vertex(name = 'V_18',
              particles = [ P.ghG, P.ghG__tilde__, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_6})

V_19 = Vertex(name = 'V_19',
              particles = [ P.g, P.g, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVV1 ],
              couplings = {(0,0):C.GC_6})

V_20 = Vertex(name = 'V_20',
              particles = [ P.g, P.g, P.g, P.g ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVV1, L.VVVV4, L.VVVV5 ],
              couplings = {(1,1):C.GC_8,(0,0):C.GC_8,(2,2):C.GC_8})

V_21 = Vertex(name = 'V_21',
              particles = [ P.b__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF4, L.FFFF7, L.FFFF8 ],
              couplings = {(0,5):C.GC_513,(0,4):C.GC_78,(0,2):C.GC_514,(0,1):C.GC_596,(0,3):C.GC_599,(0,0):C.GC_244})

V_22 = Vertex(name = 'V_22',
              particles = [ P.b__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_515})

V_23 = Vertex(name = 'V_23',
              particles = [ P.vt__tilde__, P.e__minus__, P.b__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_48,(0,4):C.GC_485,(0,2):C.GC_486,(0,1):C.GC_486,(0,0):C.GC_234})

V_24 = Vertex(name = 'V_24',
              particles = [ P.vt__tilde__, P.e__minus__, P.b__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_487})

V_25 = Vertex(name = 'V_25',
              particles = [ P.vt__tilde__, P.e__minus__, P.b__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_72,(0,4):C.GC_500,(0,2):C.GC_501,(0,1):C.GC_501,(0,0):C.GC_239})

V_26 = Vertex(name = 'V_26',
              particles = [ P.vt__tilde__, P.e__minus__, P.b__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_502})

V_27 = Vertex(name = 'V_27',
              particles = [ P.d__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8, L.FFFF9 ],
              couplings = {(0,3):C.GC_76,(0,4):C.GC_595,(0,5):C.GC_598,(0,2):C.GC_508,(0,1):C.GC_508,(0,0):C.GC_242})

V_28 = Vertex(name = 'V_28',
              particles = [ P.d__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_509})

V_29 = Vertex(name = 'V_29',
              particles = [ P.vt__tilde__, P.e__minus__, P.d__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_46,(0,4):C.GC_479,(0,2):C.GC_480,(0,1):C.GC_480,(0,0):C.GC_232})

V_30 = Vertex(name = 'V_30',
              particles = [ P.vt__tilde__, P.e__minus__, P.d__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_481})

V_31 = Vertex(name = 'V_31',
              particles = [ P.vt__tilde__, P.e__minus__, P.d__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_70,(0,4):C.GC_494,(0,2):C.GC_495,(0,1):C.GC_495,(0,0):C.GC_237})

V_32 = Vertex(name = 'V_32',
              particles = [ P.vt__tilde__, P.e__minus__, P.d__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_496})

V_33 = Vertex(name = 'V_33',
              particles = [ P.s__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_77,(0,4):C.GC_510,(0,2):C.GC_511,(0,1):C.GC_511,(0,0):C.GC_243})

V_34 = Vertex(name = 'V_34',
              particles = [ P.s__tilde__, P.c, P.vt__tilde__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_512})

V_35 = Vertex(name = 'V_35',
              particles = [ P.vt__tilde__, P.e__minus__, P.s__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_47,(0,4):C.GC_482,(0,2):C.GC_483,(0,1):C.GC_483,(0,0):C.GC_233})

V_36 = Vertex(name = 'V_36',
              particles = [ P.vt__tilde__, P.e__minus__, P.s__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_484})

V_37 = Vertex(name = 'V_37',
              particles = [ P.vt__tilde__, P.e__minus__, P.s__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_71,(0,4):C.GC_497,(0,2):C.GC_498,(0,1):C.GC_498,(0,0):C.GC_238})

V_38 = Vertex(name = 'V_38',
              particles = [ P.vt__tilde__, P.e__minus__, P.s__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_499})

V_39 = Vertex(name = 'V_39',
              particles = [ P.b__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_87,(0,4):C.GC_564,(0,2):C.GC_565,(0,1):C.GC_565,(0,0):C.GC_261})

V_40 = Vertex(name = 'V_40',
              particles = [ P.b__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_566})

V_41 = Vertex(name = 'V_41',
              particles = [ P.vt__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_66,(0,4):C.GC_534,(0,2):C.GC_535,(0,1):C.GC_535,(0,0):C.GC_251})

V_42 = Vertex(name = 'V_42',
              particles = [ P.vt__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_536})

V_43 = Vertex(name = 'V_43',
              particles = [ P.vt__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_81,(0,4):C.GC_549,(0,2):C.GC_550,(0,1):C.GC_550,(0,0):C.GC_256})

V_44 = Vertex(name = 'V_44',
              particles = [ P.vt__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_551})

V_45 = Vertex(name = 'V_45',
              particles = [ P.d__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_85,(0,4):C.GC_558,(0,2):C.GC_559,(0,1):C.GC_559,(0,0):C.GC_259})

V_46 = Vertex(name = 'V_46',
              particles = [ P.d__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_560})

V_47 = Vertex(name = 'V_47',
              particles = [ P.vt__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_64,(0,4):C.GC_528,(0,2):C.GC_529,(0,1):C.GC_529,(0,0):C.GC_249})

V_48 = Vertex(name = 'V_48',
              particles = [ P.vt__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_530})

V_49 = Vertex(name = 'V_49',
              particles = [ P.vt__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_79,(0,4):C.GC_543,(0,2):C.GC_544,(0,1):C.GC_544,(0,0):C.GC_254})

V_50 = Vertex(name = 'V_50',
              particles = [ P.vt__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_545})

V_51 = Vertex(name = 'V_51',
              particles = [ P.s__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_86,(0,4):C.GC_561,(0,2):C.GC_562,(0,1):C.GC_562,(0,0):C.GC_260})

V_52 = Vertex(name = 'V_52',
              particles = [ P.s__tilde__, P.c, P.vt__tilde__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_563})

V_53 = Vertex(name = 'V_53',
              particles = [ P.vt__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_65,(0,4):C.GC_531,(0,2):C.GC_532,(0,1):C.GC_532,(0,0):C.GC_250})

V_54 = Vertex(name = 'V_54',
              particles = [ P.vt__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_533})

V_55 = Vertex(name = 'V_55',
              particles = [ P.vt__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,3):C.GC_80,(0,4):C.GC_546,(0,2):C.GC_547,(0,1):C.GC_547,(0,0):C.GC_255})

V_56 = Vertex(name = 'V_56',
              particles = [ P.vt__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1 ],
              couplings = {(0,0):C.GC_548})

V_57 = Vertex(name = 'V_57',
              particles = [ P.e__plus__, P.mu__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_109,(0,11):C.GC_286,(0,9):C.GC_283,(0,8):C.GC_283,(0,0):C.GC_162,(0,4):C.GC_20,(0,5):C.GC_577,(0,6):C.GC_146,(0,7):C.GC_402,(0,3):C.GC_401,(0,2):C.GC_401,(0,1):C.GC_207})

V_58 = Vertex(name = 'V_58',
              particles = [ P.e__plus__, P.mu__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_288,(0,1):C.GC_404})

V_59 = Vertex(name = 'V_59',
              particles = [ P.e__plus__, P.ta__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_124,(0,11):C.GC_337,(0,9):C.GC_334,(0,8):C.GC_334,(0,0):C.GC_179,(0,4):C.GC_28,(0,5):C.GC_585,(0,6):C.GC_154,(0,7):C.GC_505,(0,3):C.GC_504,(0,2):C.GC_504,(0,1):C.GC_241})

V_60 = Vertex(name = 'V_60',
              particles = [ P.e__plus__, P.ta__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_339,(0,1):C.GC_506})

V_61 = Vertex(name = 'V_61',
              particles = [ P.mu__plus__, P.e__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_93,(0,11):C.GC_388,(0,9):C.GC_385,(0,8):C.GC_385,(0,0):C.GC_196,(0,4):C.GC_12,(0,5):C.GC_569,(0,6):C.GC_138,(0,7):C.GC_315,(0,3):C.GC_314,(0,2):C.GC_314,(0,1):C.GC_173})

V_62 = Vertex(name = 'V_62',
              particles = [ P.mu__plus__, P.e__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_390,(0,1):C.GC_317})

V_63 = Vertex(name = 'V_63',
              particles = [ P.mu__plus__, P.ta__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_132,(0,11):C.GC_424,(0,9):C.GC_421,(0,8):C.GC_421,(0,0):C.GC_213,(0,4):C.GC_32,(0,5):C.GC_589,(0,6):C.GC_158,(0,7):C.GC_554,(0,3):C.GC_553,(0,2):C.GC_553,(0,1):C.GC_258})

V_64 = Vertex(name = 'V_64',
              particles = [ P.mu__plus__, P.ta__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_426,(0,1):C.GC_556})

V_65 = Vertex(name = 'V_65',
              particles = [ P.ta__plus__, P.e__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_101,(0,11):C.GC_476,(0,9):C.GC_473,(0,8):C.GC_473,(0,0):C.GC_230,(0,4):C.GC_16,(0,5):C.GC_573,(0,6):C.GC_142,(0,7):C.GC_366,(0,3):C.GC_365,(0,2):C.GC_365,(0,1):C.GC_190})

V_66 = Vertex(name = 'V_66',
              particles = [ P.ta__plus__, P.e__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_478,(0,1):C.GC_368})

V_67 = Vertex(name = 'V_67',
              particles = [ P.ta__plus__, P.mu__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_117,(0,11):C.GC_525,(0,9):C.GC_522,(0,8):C.GC_522,(0,0):C.GC_247,(0,4):C.GC_24,(0,5):C.GC_581,(0,6):C.GC_150,(0,7):C.GC_454,(0,3):C.GC_453,(0,2):C.GC_453,(0,1):C.GC_224})

V_68 = Vertex(name = 'V_68',
              particles = [ P.ta__plus__, P.mu__minus__, P.c__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_527,(0,1):C.GC_456})

V_69 = Vertex(name = 'V_69',
              particles = [ P.t__tilde__, P.c, P.e__plus__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_95,(0,11):C.GC_316,(0,9):C.GC_313,(0,8):C.GC_313,(0,0):C.GC_172,(0,4):C.GC_570,(0,5):C.GC_13,(0,6):C.GC_139,(0,7):C.GC_387,(0,3):C.GC_386,(0,2):C.GC_386,(0,1):C.GC_197})

V_70 = Vertex(name = 'V_70',
              particles = [ P.t__tilde__, P.c, P.e__plus__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_318,(0,1):C.GC_389})

V_71 = Vertex(name = 'V_71',
              particles = [ P.t__tilde__, P.c, P.e__plus__, P.ta__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_103,(0,11):C.GC_367,(0,9):C.GC_364,(0,8):C.GC_364,(0,0):C.GC_189,(0,4):C.GC_574,(0,5):C.GC_17,(0,6):C.GC_143,(0,7):C.GC_475,(0,3):C.GC_474,(0,2):C.GC_474,(0,1):C.GC_231})

V_72 = Vertex(name = 'V_72',
              particles = [ P.t__tilde__, P.c, P.e__plus__, P.ta__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_369,(0,1):C.GC_477})

V_73 = Vertex(name = 'V_73',
              particles = [ P.t__tilde__, P.c, P.mu__plus__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_111,(0,11):C.GC_403,(0,9):C.GC_400,(0,8):C.GC_400,(0,0):C.GC_206,(0,4):C.GC_578,(0,5):C.GC_21,(0,6):C.GC_147,(0,7):C.GC_285,(0,3):C.GC_284,(0,2):C.GC_284,(0,1):C.GC_163})

V_74 = Vertex(name = 'V_74',
              particles = [ P.t__tilde__, P.c, P.mu__plus__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_405,(0,1):C.GC_287})

V_75 = Vertex(name = 'V_75',
              particles = [ P.t__tilde__, P.c, P.mu__plus__, P.ta__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_119,(0,11):C.GC_455,(0,9):C.GC_452,(0,8):C.GC_452,(0,0):C.GC_223,(0,4):C.GC_582,(0,5):C.GC_25,(0,6):C.GC_151,(0,7):C.GC_524,(0,3):C.GC_523,(0,2):C.GC_523,(0,1):C.GC_248})

V_76 = Vertex(name = 'V_76',
              particles = [ P.t__tilde__, P.c, P.mu__plus__, P.ta__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_457,(0,1):C.GC_526})

V_77 = Vertex(name = 'V_77',
              particles = [ P.t__tilde__, P.c, P.ta__plus__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8, L.FFFF9 ],
              couplings = {(0,10):C.GC_126,(0,11):C.GC_594,(0,12):C.GC_597,(0,9):C.GC_503,(0,8):C.GC_503,(0,0):C.GC_240,(0,7):C.GC_336,(0,4):C.GC_586,(0,5):C.GC_29,(0,6):C.GC_155,(0,3):C.GC_335,(0,2):C.GC_335,(0,1):C.GC_180})

V_78 = Vertex(name = 'V_78',
              particles = [ P.t__tilde__, P.c, P.ta__plus__, P.e__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_507,(0,1):C.GC_338})

V_79 = Vertex(name = 'V_79',
              particles = [ P.t__tilde__, P.c, P.ta__plus__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_134,(0,11):C.GC_555,(0,9):C.GC_552,(0,8):C.GC_552,(0,0):C.GC_257,(0,4):C.GC_590,(0,5):C.GC_33,(0,6):C.GC_159,(0,7):C.GC_423,(0,3):C.GC_422,(0,2):C.GC_422,(0,1):C.GC_214})

V_80 = Vertex(name = 'V_80',
              particles = [ P.t__tilde__, P.c, P.ta__plus__, P.mu__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_557,(0,1):C.GC_425})

V_81 = Vertex(name = 'V_81',
              particles = [ P.e__plus__, P.mu__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_91,(0,11):C.GC_301,(0,9):C.GC_298,(0,8):C.GC_298,(0,0):C.GC_167,(0,4):C.GC_11,(0,5):C.GC_568,(0,6):C.GC_137,(0,7):C.GC_381,(0,3):C.GC_380,(0,2):C.GC_380,(0,1):C.GC_195})

V_82 = Vertex(name = 'V_82',
              particles = [ P.e__plus__, P.mu__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_303,(0,1):C.GC_383})

V_83 = Vertex(name = 'V_83',
              particles = [ P.e__plus__, P.ta__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_99,(0,11):C.GC_352,(0,9):C.GC_349,(0,8):C.GC_349,(0,0):C.GC_184,(0,4):C.GC_15,(0,5):C.GC_572,(0,6):C.GC_141,(0,7):C.GC_469,(0,3):C.GC_468,(0,2):C.GC_468,(0,1):C.GC_229})

V_84 = Vertex(name = 'V_84',
              particles = [ P.e__plus__, P.ta__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_354,(0,1):C.GC_471})

V_85 = Vertex(name = 'V_85',
              particles = [ P.mu__plus__, P.e__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_107,(0,11):C.GC_265,(0,9):C.GC_262,(0,8):C.GC_262,(0,0):C.GC_201,(0,4):C.GC_19,(0,5):C.GC_576,(0,6):C.GC_145,(0,7):C.GC_279,(0,3):C.GC_278,(0,2):C.GC_278,(0,1):C.GC_161})

V_86 = Vertex(name = 'V_86',
              particles = [ P.mu__plus__, P.e__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_267,(0,1):C.GC_281})

V_87 = Vertex(name = 'V_87',
              particles = [ P.mu__plus__, P.ta__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_115,(0,11):C.GC_440,(0,9):C.GC_437,(0,8):C.GC_437,(0,0):C.GC_218,(0,4):C.GC_23,(0,5):C.GC_580,(0,6):C.GC_149,(0,7):C.GC_518,(0,3):C.GC_517,(0,2):C.GC_517,(0,1):C.GC_246})

V_88 = Vertex(name = 'V_88',
              particles = [ P.mu__plus__, P.ta__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_442,(0,1):C.GC_520})

V_89 = Vertex(name = 'V_89',
              particles = [ P.ta__plus__, P.e__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_123,(0,11):C.GC_491,(0,9):C.GC_488,(0,8):C.GC_488,(0,0):C.GC_235,(0,4):C.GC_27,(0,5):C.GC_584,(0,6):C.GC_153,(0,7):C.GC_330,(0,3):C.GC_329,(0,2):C.GC_329,(0,1):C.GC_178})

V_90 = Vertex(name = 'V_90',
              particles = [ P.ta__plus__, P.e__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_493,(0,1):C.GC_332})

V_91 = Vertex(name = 'V_91',
              particles = [ P.ta__plus__, P.mu__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_130,(0,11):C.GC_540,(0,9):C.GC_537,(0,8):C.GC_537,(0,0):C.GC_252,(0,4):C.GC_31,(0,5):C.GC_588,(0,6):C.GC_157,(0,7):C.GC_417,(0,3):C.GC_416,(0,2):C.GC_416,(0,1):C.GC_212})

V_92 = Vertex(name = 'V_92',
              particles = [ P.ta__plus__, P.mu__minus__, P.t__tilde__, P.u ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_542,(0,1):C.GC_419})

V_93 = Vertex(name = 'V_93',
              particles = [ P.e__plus__, P.mu__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_105,(0,11):C.GC_280,(0,9):C.GC_277,(0,8):C.GC_277,(0,0):C.GC_160,(0,4):C.GC_18,(0,5):C.GC_575,(0,6):C.GC_144,(0,7):C.GC_264,(0,3):C.GC_263,(0,2):C.GC_263,(0,1):C.GC_202})

V_94 = Vertex(name = 'V_94',
              particles = [ P.e__plus__, P.mu__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_282,(0,1):C.GC_266})

V_95 = Vertex(name = 'V_95',
              particles = [ P.e__plus__, P.ta__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_121,(0,11):C.GC_331,(0,9):C.GC_328,(0,8):C.GC_328,(0,0):C.GC_177,(0,4):C.GC_26,(0,5):C.GC_583,(0,6):C.GC_152,(0,7):C.GC_490,(0,3):C.GC_489,(0,2):C.GC_489,(0,1):C.GC_236})

V_96 = Vertex(name = 'V_96',
              particles = [ P.e__plus__, P.ta__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_333,(0,1):C.GC_492})

V_97 = Vertex(name = 'V_97',
              particles = [ P.mu__plus__, P.e__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_89,(0,11):C.GC_382,(0,9):C.GC_379,(0,8):C.GC_379,(0,0):C.GC_194,(0,4):C.GC_10,(0,5):C.GC_567,(0,6):C.GC_136,(0,7):C.GC_300,(0,3):C.GC_299,(0,2):C.GC_299,(0,1):C.GC_168})

V_98 = Vertex(name = 'V_98',
              particles = [ P.mu__plus__, P.e__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10 ],
              couplings = {(0,0):C.GC_384,(0,1):C.GC_302})

V_99 = Vertex(name = 'V_99',
              particles = [ P.mu__plus__, P.ta__minus__, P.u__tilde__, P.t ],
              color = [ 'Identity(3,4)' ],
              lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
              couplings = {(0,10):C.GC_128,(0,11):C.GC_418,(0,9):C.GC_415,(0,8):C.GC_415,(0,0):C.GC_211,(0,4):C.GC_30,(0,5):C.GC_587,(0,6):C.GC_156,(0,7):C.GC_539,(0,3):C.GC_538,(0,2):C.GC_538,(0,1):C.GC_253})

V_100 = Vertex(name = 'V_100',
               particles = [ P.mu__plus__, P.ta__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10 ],
               couplings = {(0,0):C.GC_420,(0,1):C.GC_541})

V_101 = Vertex(name = 'V_101',
               particles = [ P.ta__plus__, P.e__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,10):C.GC_97,(0,11):C.GC_470,(0,9):C.GC_467,(0,8):C.GC_467,(0,0):C.GC_228,(0,4):C.GC_14,(0,5):C.GC_571,(0,6):C.GC_140,(0,7):C.GC_351,(0,3):C.GC_350,(0,2):C.GC_350,(0,1):C.GC_185})

V_102 = Vertex(name = 'V_102',
               particles = [ P.ta__plus__, P.e__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10 ],
               couplings = {(0,0):C.GC_472,(0,1):C.GC_353})

V_103 = Vertex(name = 'V_103',
               particles = [ P.ta__plus__, P.mu__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF17, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,10):C.GC_113,(0,11):C.GC_519,(0,9):C.GC_516,(0,8):C.GC_516,(0,0):C.GC_245,(0,4):C.GC_22,(0,5):C.GC_579,(0,6):C.GC_148,(0,7):C.GC_439,(0,3):C.GC_438,(0,2):C.GC_438,(0,1):C.GC_219})

V_104 = Vertex(name = 'V_104',
               particles = [ P.ta__plus__, P.mu__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10 ],
               couplings = {(0,0):C.GC_521,(0,1):C.GC_441})

V_105 = Vertex(name = 'V_105',
               particles = [ P.b__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_42,(0,4):C.GC_325,(0,2):C.GC_326,(0,1):C.GC_326,(0,0):C.GC_176})

V_106 = Vertex(name = 'V_106',
               particles = [ P.b__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_327})

V_107 = Vertex(name = 'V_107',
               particles = [ P.ve__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_57,(0,4):C.GC_295,(0,2):C.GC_296,(0,1):C.GC_296,(0,0):C.GC_166})

V_108 = Vertex(name = 'V_108',
               particles = [ P.ve__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_297})

V_109 = Vertex(name = 'V_109',
               particles = [ P.ve__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_37,(0,4):C.GC_310,(0,2):C.GC_311,(0,1):C.GC_311,(0,0):C.GC_171})

V_110 = Vertex(name = 'V_110',
               particles = [ P.ve__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_312})

V_111 = Vertex(name = 'V_111',
               particles = [ P.d__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_40,(0,4):C.GC_319,(0,2):C.GC_320,(0,1):C.GC_320,(0,0):C.GC_174})

V_112 = Vertex(name = 'V_112',
               particles = [ P.d__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_321})

V_113 = Vertex(name = 'V_113',
               particles = [ P.ve__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_55,(0,4):C.GC_289,(0,2):C.GC_290,(0,1):C.GC_290,(0,0):C.GC_164})

V_114 = Vertex(name = 'V_114',
               particles = [ P.ve__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_291})

V_115 = Vertex(name = 'V_115',
               particles = [ P.ve__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_35,(0,4):C.GC_304,(0,2):C.GC_305,(0,1):C.GC_305,(0,0):C.GC_169})

V_116 = Vertex(name = 'V_116',
               particles = [ P.ve__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_306})

V_117 = Vertex(name = 'V_117',
               particles = [ P.s__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_41,(0,4):C.GC_322,(0,2):C.GC_323,(0,1):C.GC_323,(0,0):C.GC_175})

V_118 = Vertex(name = 'V_118',
               particles = [ P.s__tilde__, P.c, P.ve__tilde__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_324})

V_119 = Vertex(name = 'V_119',
               particles = [ P.ve__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_56,(0,4):C.GC_292,(0,2):C.GC_293,(0,1):C.GC_293,(0,0):C.GC_165})

V_120 = Vertex(name = 'V_120',
               particles = [ P.ve__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_294})

V_121 = Vertex(name = 'V_121',
               particles = [ P.ve__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_36,(0,4):C.GC_307,(0,2):C.GC_308,(0,1):C.GC_308,(0,0):C.GC_170})

V_122 = Vertex(name = 'V_122',
               particles = [ P.ve__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_309})

V_123 = Vertex(name = 'V_123',
               particles = [ P.b__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_51,(0,4):C.GC_376,(0,2):C.GC_377,(0,1):C.GC_377,(0,0):C.GC_193})

V_124 = Vertex(name = 'V_124',
               particles = [ P.b__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_378})

V_125 = Vertex(name = 'V_125',
               particles = [ P.ve__tilde__, P.ta__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_75,(0,4):C.GC_346,(0,2):C.GC_347,(0,1):C.GC_347,(0,0):C.GC_183})

V_126 = Vertex(name = 'V_126',
               particles = [ P.ve__tilde__, P.ta__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_348})

V_127 = Vertex(name = 'V_127',
               particles = [ P.ve__tilde__, P.ta__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_45,(0,4):C.GC_361,(0,2):C.GC_362,(0,1):C.GC_362,(0,0):C.GC_188})

V_128 = Vertex(name = 'V_128',
               particles = [ P.ve__tilde__, P.ta__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_363})

V_129 = Vertex(name = 'V_129',
               particles = [ P.d__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_49,(0,4):C.GC_370,(0,2):C.GC_371,(0,1):C.GC_371,(0,0):C.GC_191})

V_130 = Vertex(name = 'V_130',
               particles = [ P.d__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_372})

V_131 = Vertex(name = 'V_131',
               particles = [ P.ve__tilde__, P.ta__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_73,(0,4):C.GC_340,(0,2):C.GC_341,(0,1):C.GC_341,(0,0):C.GC_181})

V_132 = Vertex(name = 'V_132',
               particles = [ P.ve__tilde__, P.ta__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_342})

V_133 = Vertex(name = 'V_133',
               particles = [ P.ve__tilde__, P.ta__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_43,(0,4):C.GC_355,(0,2):C.GC_356,(0,1):C.GC_356,(0,0):C.GC_186})

V_134 = Vertex(name = 'V_134',
               particles = [ P.ve__tilde__, P.ta__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_357})

V_135 = Vertex(name = 'V_135',
               particles = [ P.s__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_50,(0,4):C.GC_373,(0,2):C.GC_374,(0,1):C.GC_374,(0,0):C.GC_192})

V_136 = Vertex(name = 'V_136',
               particles = [ P.s__tilde__, P.c, P.ve__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_375})

V_137 = Vertex(name = 'V_137',
               particles = [ P.ve__tilde__, P.ta__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_74,(0,4):C.GC_343,(0,2):C.GC_344,(0,1):C.GC_344,(0,0):C.GC_182})

V_138 = Vertex(name = 'V_138',
               particles = [ P.ve__tilde__, P.ta__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_345})

V_139 = Vertex(name = 'V_139',
               particles = [ P.ve__tilde__, P.ta__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_44,(0,4):C.GC_358,(0,2):C.GC_359,(0,1):C.GC_359,(0,0):C.GC_187})

V_140 = Vertex(name = 'V_140',
               particles = [ P.ve__tilde__, P.ta__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_360})

V_141 = Vertex(name = 'V_141',
               particles = [ P.b__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_60,(0,4):C.GC_412,(0,2):C.GC_413,(0,1):C.GC_413,(0,0):C.GC_210})

V_142 = Vertex(name = 'V_142',
               particles = [ P.b__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_414})

V_143 = Vertex(name = 'V_143',
               particles = [ P.vm__tilde__, P.e__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_39,(0,4):C.GC_397,(0,2):C.GC_398,(0,1):C.GC_398,(0,0):C.GC_200})

V_144 = Vertex(name = 'V_144',
               particles = [ P.vm__tilde__, P.e__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_399})

V_145 = Vertex(name = 'V_145',
               particles = [ P.vm__tilde__, P.e__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_54,(0,4):C.GC_274,(0,2):C.GC_275,(0,1):C.GC_275,(0,0):C.GC_205})

V_146 = Vertex(name = 'V_146',
               particles = [ P.vm__tilde__, P.e__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_276})

V_147 = Vertex(name = 'V_147',
               particles = [ P.d__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_58,(0,4):C.GC_406,(0,2):C.GC_407,(0,1):C.GC_407,(0,0):C.GC_208})

V_148 = Vertex(name = 'V_148',
               particles = [ P.d__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_408})

V_149 = Vertex(name = 'V_149',
               particles = [ P.vm__tilde__, P.e__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_38,(0,4):C.GC_391,(0,2):C.GC_392,(0,1):C.GC_392,(0,0):C.GC_198})

V_150 = Vertex(name = 'V_150',
               particles = [ P.vm__tilde__, P.e__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_393})

V_151 = Vertex(name = 'V_151',
               particles = [ P.vm__tilde__, P.e__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_52,(0,4):C.GC_268,(0,2):C.GC_269,(0,1):C.GC_269,(0,0):C.GC_203})

V_152 = Vertex(name = 'V_152',
               particles = [ P.vm__tilde__, P.e__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_270})

V_153 = Vertex(name = 'V_153',
               particles = [ P.s__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_59,(0,4):C.GC_409,(0,2):C.GC_410,(0,1):C.GC_410,(0,0):C.GC_209})

V_154 = Vertex(name = 'V_154',
               particles = [ P.s__tilde__, P.c, P.vm__tilde__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_411})

V_155 = Vertex(name = 'V_155',
               particles = [ P.vm__tilde__, P.e__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF5, L.FFFF7, L.FFFF8 ],
               couplings = {(0,5):C.GC_394,(0,4):C.GC_34,(0,3):C.GC_591,(0,2):C.GC_395,(0,1):C.GC_395,(0,0):C.GC_199})

V_156 = Vertex(name = 'V_156',
               particles = [ P.vm__tilde__, P.e__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_396})

V_157 = Vertex(name = 'V_157',
               particles = [ P.vm__tilde__, P.e__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_53,(0,4):C.GC_271,(0,2):C.GC_272,(0,1):C.GC_272,(0,0):C.GC_204})

V_158 = Vertex(name = 'V_158',
               particles = [ P.vm__tilde__, P.e__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_273})

V_159 = Vertex(name = 'V_159',
               particles = [ P.b__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_69,(0,4):C.GC_464,(0,2):C.GC_465,(0,1):C.GC_465,(0,0):C.GC_227})

V_160 = Vertex(name = 'V_160',
               particles = [ P.b__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_466})

V_161 = Vertex(name = 'V_161',
               particles = [ P.vm__tilde__, P.ta__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF6, L.FFFF7, L.FFFF8 ],
               couplings = {(0,5):C.GC_433,(0,4):C.GC_84,(0,2):C.GC_434,(0,3):C.GC_593,(0,1):C.GC_435,(0,0):C.GC_217})

V_162 = Vertex(name = 'V_162',
               particles = [ P.vm__tilde__, P.ta__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_436})

V_163 = Vertex(name = 'V_163',
               particles = [ P.vm__tilde__, P.ta__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_63,(0,4):C.GC_449,(0,2):C.GC_450,(0,1):C.GC_450,(0,0):C.GC_222})

V_164 = Vertex(name = 'V_164',
               particles = [ P.vm__tilde__, P.ta__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_451})

V_165 = Vertex(name = 'V_165',
               particles = [ P.d__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_67,(0,4):C.GC_458,(0,2):C.GC_459,(0,1):C.GC_459,(0,0):C.GC_225})

V_166 = Vertex(name = 'V_166',
               particles = [ P.d__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_460})

V_167 = Vertex(name = 'V_167',
               particles = [ P.vm__tilde__, P.ta__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_82,(0,4):C.GC_427,(0,2):C.GC_428,(0,1):C.GC_428,(0,0):C.GC_215})

V_168 = Vertex(name = 'V_168',
               particles = [ P.vm__tilde__, P.ta__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_429})

V_169 = Vertex(name = 'V_169',
               particles = [ P.vm__tilde__, P.ta__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_61,(0,4):C.GC_443,(0,2):C.GC_444,(0,1):C.GC_444,(0,0):C.GC_220})

V_170 = Vertex(name = 'V_170',
               particles = [ P.vm__tilde__, P.ta__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_445})

V_171 = Vertex(name = 'V_171',
               particles = [ P.s__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_68,(0,4):C.GC_461,(0,2):C.GC_462,(0,1):C.GC_462,(0,0):C.GC_226})

V_172 = Vertex(name = 'V_172',
               particles = [ P.s__tilde__, P.c, P.vm__tilde__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_463})

V_173 = Vertex(name = 'V_173',
               particles = [ P.vm__tilde__, P.ta__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_83,(0,4):C.GC_430,(0,2):C.GC_431,(0,1):C.GC_431,(0,0):C.GC_216})

V_174 = Vertex(name = 'V_174',
               particles = [ P.vm__tilde__, P.ta__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_432})

V_175 = Vertex(name = 'V_175',
               particles = [ P.vm__tilde__, P.ta__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF7, L.FFFF8 ],
               couplings = {(0,3):C.GC_62,(0,4):C.GC_446,(0,2):C.GC_447,(0,1):C.GC_447,(0,0):C.GC_221})

V_176 = Vertex(name = 'V_176',
               particles = [ P.vm__tilde__, P.ta__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1 ],
               couplings = {(0,0):C.GC_448})

V_177 = Vertex(name = 'V_177',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_629})

V_178 = Vertex(name = 'V_178',
               particles = [ P.ta__plus__, P.ta__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_631})

V_179 = Vertex(name = 'V_179',
               particles = [ P.t__tilde__, P.t, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_630})

V_180 = Vertex(name = 'V_180',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_984,(0,3):C.GC_1049,(0,2):C.GC_1050,(0,1):C.GC_1050,(0,0):C.GC_1032})

V_181 = Vertex(name = 'V_181',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1051})

V_182 = Vertex(name = 'V_182',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_705,(0,3):C.GC_715,(0,2):C.GC_716,(0,1):C.GC_716,(0,0):C.GC_709})

V_183 = Vertex(name = 'V_183',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_717})

V_184 = Vertex(name = 'V_184',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_983,(0,3):C.GC_1046,(0,2):C.GC_1047,(0,1):C.GC_1047,(0,0):C.GC_1031})

V_185 = Vertex(name = 'V_185',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1048})

V_186 = Vertex(name = 'V_186',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_753,(0,3):C.GC_818,(0,2):C.GC_819,(0,1):C.GC_819,(0,0):C.GC_801})

V_187 = Vertex(name = 'V_187',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_820})

V_188 = Vertex(name = 'V_188',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_641,(0,3):C.GC_651,(0,2):C.GC_652,(0,1):C.GC_652,(0,0):C.GC_645})

V_189 = Vertex(name = 'V_189',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_653})

V_190 = Vertex(name = 'V_190',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_752,(0,3):C.GC_815,(0,2):C.GC_816,(0,1):C.GC_816,(0,0):C.GC_800})

V_191 = Vertex(name = 'V_191',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_817})

V_192 = Vertex(name = 'V_192',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_868,(0,3):C.GC_933,(0,2):C.GC_934,(0,1):C.GC_934,(0,0):C.GC_916})

V_193 = Vertex(name = 'V_193',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_935})

V_194 = Vertex(name = 'V_194',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_674,(0,3):C.GC_685,(0,2):C.GC_686,(0,1):C.GC_686,(0,0):C.GC_678})

V_195 = Vertex(name = 'V_195',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_687})

V_196 = Vertex(name = 'V_196',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_867,(0,3):C.GC_930,(0,2):C.GC_931,(0,1):C.GC_931,(0,0):C.GC_915})

V_197 = Vertex(name = 'V_197',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_932})

V_198 = Vertex(name = 'V_198',
               particles = [ P.c__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_986,(0,3):C.GC_1055,(0,2):C.GC_1056,(0,1):C.GC_1056,(0,0):C.GC_1034})

V_199 = Vertex(name = 'V_199',
               particles = [ P.c__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1057})

V_200 = Vertex(name = 'V_200',
               particles = [ P.t__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_707,(0,3):C.GC_718,(0,2):C.GC_719,(0,1):C.GC_719,(0,0):C.GC_710})

V_201 = Vertex(name = 'V_201',
               particles = [ P.t__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_720})

V_202 = Vertex(name = 'V_202',
               particles = [ P.u__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_985,(0,3):C.GC_1052,(0,2):C.GC_1053,(0,1):C.GC_1053,(0,0):C.GC_1033})

V_203 = Vertex(name = 'V_203',
               particles = [ P.u__tilde__, P.b, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1054})

V_204 = Vertex(name = 'V_204',
               particles = [ P.c__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_755,(0,3):C.GC_824,(0,2):C.GC_825,(0,1):C.GC_825,(0,0):C.GC_803})

V_205 = Vertex(name = 'V_205',
               particles = [ P.c__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_826})

V_206 = Vertex(name = 'V_206',
               particles = [ P.t__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_643,(0,3):C.GC_654,(0,2):C.GC_655,(0,1):C.GC_655,(0,0):C.GC_646})

V_207 = Vertex(name = 'V_207',
               particles = [ P.t__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_656})

V_208 = Vertex(name = 'V_208',
               particles = [ P.u__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_754,(0,3):C.GC_821,(0,2):C.GC_822,(0,1):C.GC_822,(0,0):C.GC_802})

V_209 = Vertex(name = 'V_209',
               particles = [ P.u__tilde__, P.d, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_823})

V_210 = Vertex(name = 'V_210',
               particles = [ P.c__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_870,(0,3):C.GC_939,(0,2):C.GC_940,(0,1):C.GC_940,(0,0):C.GC_918})

V_211 = Vertex(name = 'V_211',
               particles = [ P.c__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_941})

V_212 = Vertex(name = 'V_212',
               particles = [ P.t__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_676,(0,3):C.GC_688,(0,2):C.GC_689,(0,1):C.GC_689,(0,0):C.GC_679})

V_213 = Vertex(name = 'V_213',
               particles = [ P.t__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_690})

V_214 = Vertex(name = 'V_214',
               particles = [ P.u__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_869,(0,3):C.GC_936,(0,2):C.GC_937,(0,1):C.GC_937,(0,0):C.GC_917})

V_215 = Vertex(name = 'V_215',
               particles = [ P.u__tilde__, P.s, P.ta__plus__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_938})

V_216 = Vertex(name = 'V_216',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_988,(0,3):C.GC_1058,(0,2):C.GC_1059,(0,1):C.GC_1059,(0,0):C.GC_1036})

V_217 = Vertex(name = 'V_217',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1060})

V_218 = Vertex(name = 'V_218',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_703,(0,3):C.GC_721,(0,2):C.GC_722,(0,1):C.GC_722,(0,0):C.GC_711})

V_219 = Vertex(name = 'V_219',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_723})

V_220 = Vertex(name = 'V_220',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_987,(0,3):C.GC_1043,(0,2):C.GC_1044,(0,1):C.GC_1044,(0,0):C.GC_1035})

V_221 = Vertex(name = 'V_221',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1045})

V_222 = Vertex(name = 'V_222',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_757,(0,3):C.GC_827,(0,2):C.GC_828,(0,1):C.GC_828,(0,0):C.GC_805})

V_223 = Vertex(name = 'V_223',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_829})

V_224 = Vertex(name = 'V_224',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_639,(0,3):C.GC_657,(0,2):C.GC_658,(0,1):C.GC_658,(0,0):C.GC_647})

V_225 = Vertex(name = 'V_225',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_659})

V_226 = Vertex(name = 'V_226',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_756,(0,3):C.GC_812,(0,2):C.GC_813,(0,1):C.GC_813,(0,0):C.GC_804})

V_227 = Vertex(name = 'V_227',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_814})

V_228 = Vertex(name = 'V_228',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_872,(0,3):C.GC_942,(0,2):C.GC_943,(0,1):C.GC_943,(0,0):C.GC_920})

V_229 = Vertex(name = 'V_229',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_944})

V_230 = Vertex(name = 'V_230',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF18, L.FFFF7 ],
               couplings = {(0,5):C.GC_672,(0,3):C.GC_684,(0,4):C.GC_670,(0,2):C.GC_691,(0,1):C.GC_691,(0,0):C.GC_680})

V_231 = Vertex(name = 'V_231',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_692})

V_232 = Vertex(name = 'V_232',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_871,(0,3):C.GC_927,(0,2):C.GC_928,(0,1):C.GC_928,(0,0):C.GC_919})

V_233 = Vertex(name = 'V_233',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_929})

V_234 = Vertex(name = 'V_234',
               particles = [ P.c__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_990,(0,3):C.GC_1064,(0,2):C.GC_1065,(0,1):C.GC_1065,(0,0):C.GC_1038})

V_235 = Vertex(name = 'V_235',
               particles = [ P.c__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1066})

V_236 = Vertex(name = 'V_236',
               particles = [ P.t__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_708,(0,3):C.GC_724,(0,2):C.GC_725,(0,1):C.GC_725,(0,0):C.GC_712})

V_237 = Vertex(name = 'V_237',
               particles = [ P.t__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_726})

V_238 = Vertex(name = 'V_238',
               particles = [ P.u__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_989,(0,3):C.GC_1061,(0,2):C.GC_1062,(0,1):C.GC_1062,(0,0):C.GC_1037})

V_239 = Vertex(name = 'V_239',
               particles = [ P.u__tilde__, P.b, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1063})

V_240 = Vertex(name = 'V_240',
               particles = [ P.c__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_759,(0,3):C.GC_833,(0,2):C.GC_834,(0,1):C.GC_834,(0,0):C.GC_807})

V_241 = Vertex(name = 'V_241',
               particles = [ P.c__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_835})

V_242 = Vertex(name = 'V_242',
               particles = [ P.t__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_644,(0,3):C.GC_660,(0,2):C.GC_661,(0,1):C.GC_661,(0,0):C.GC_648})

V_243 = Vertex(name = 'V_243',
               particles = [ P.t__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_662})

V_244 = Vertex(name = 'V_244',
               particles = [ P.u__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_758,(0,3):C.GC_830,(0,2):C.GC_831,(0,1):C.GC_831,(0,0):C.GC_806})

V_245 = Vertex(name = 'V_245',
               particles = [ P.u__tilde__, P.d, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_832})

V_246 = Vertex(name = 'V_246',
               particles = [ P.c__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_874,(0,3):C.GC_948,(0,2):C.GC_949,(0,1):C.GC_949,(0,0):C.GC_922})

V_247 = Vertex(name = 'V_247',
               particles = [ P.c__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_950})

V_248 = Vertex(name = 'V_248',
               particles = [ P.t__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_677,(0,3):C.GC_693,(0,2):C.GC_694,(0,1):C.GC_694,(0,0):C.GC_681})

V_249 = Vertex(name = 'V_249',
               particles = [ P.t__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_695})

V_250 = Vertex(name = 'V_250',
               particles = [ P.u__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_873,(0,3):C.GC_945,(0,2):C.GC_946,(0,1):C.GC_946,(0,0):C.GC_921})

V_251 = Vertex(name = 'V_251',
               particles = [ P.u__tilde__, P.s, P.ta__plus__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_947})

V_252 = Vertex(name = 'V_252',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_992,(0,3):C.GC_1070,(0,2):C.GC_1071,(0,1):C.GC_1071,(0,0):C.GC_1040})

V_253 = Vertex(name = 'V_253',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1072})

V_254 = Vertex(name = 'V_254',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_704,(0,3):C.GC_727,(0,2):C.GC_728,(0,1):C.GC_728,(0,0):C.GC_713})

V_255 = Vertex(name = 'V_255',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_729})

V_256 = Vertex(name = 'V_256',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_991,(0,3):C.GC_1067,(0,2):C.GC_1068,(0,1):C.GC_1068,(0,0):C.GC_1039})

V_257 = Vertex(name = 'V_257',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1069})

V_258 = Vertex(name = 'V_258',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_761,(0,3):C.GC_839,(0,2):C.GC_840,(0,1):C.GC_840,(0,0):C.GC_809})

V_259 = Vertex(name = 'V_259',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_841})

V_260 = Vertex(name = 'V_260',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_640,(0,3):C.GC_663,(0,2):C.GC_664,(0,1):C.GC_664,(0,0):C.GC_649})

V_261 = Vertex(name = 'V_261',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_665})

V_262 = Vertex(name = 'V_262',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_760,(0,3):C.GC_836,(0,2):C.GC_837,(0,1):C.GC_837,(0,0):C.GC_808})

V_263 = Vertex(name = 'V_263',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_838})

V_264 = Vertex(name = 'V_264',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_876,(0,3):C.GC_954,(0,2):C.GC_955,(0,1):C.GC_955,(0,0):C.GC_924})

V_265 = Vertex(name = 'V_265',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_956})

V_266 = Vertex(name = 'V_266',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_673,(0,3):C.GC_696,(0,2):C.GC_697,(0,1):C.GC_697,(0,0):C.GC_682})

V_267 = Vertex(name = 'V_267',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_698})

V_268 = Vertex(name = 'V_268',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_875,(0,3):C.GC_951,(0,2):C.GC_952,(0,1):C.GC_952,(0,0):C.GC_923})

V_269 = Vertex(name = 'V_269',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_953})

V_270 = Vertex(name = 'V_270',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_994,(0,3):C.GC_1076,(0,2):C.GC_1077,(0,1):C.GC_1077,(0,0):C.GC_1042})

V_271 = Vertex(name = 'V_271',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1078})

V_272 = Vertex(name = 'V_272',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_706,(0,3):C.GC_730,(0,2):C.GC_731,(0,1):C.GC_731,(0,0):C.GC_714})

V_273 = Vertex(name = 'V_273',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_732})

V_274 = Vertex(name = 'V_274',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_993,(0,3):C.GC_1073,(0,2):C.GC_1074,(0,1):C.GC_1074,(0,0):C.GC_1041})

V_275 = Vertex(name = 'V_275',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_1075})

V_276 = Vertex(name = 'V_276',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_763,(0,3):C.GC_845,(0,2):C.GC_846,(0,1):C.GC_846,(0,0):C.GC_811})

V_277 = Vertex(name = 'V_277',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_847})

V_278 = Vertex(name = 'V_278',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_642,(0,3):C.GC_666,(0,2):C.GC_667,(0,1):C.GC_667,(0,0):C.GC_650})

V_279 = Vertex(name = 'V_279',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_668})

V_280 = Vertex(name = 'V_280',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_762,(0,3):C.GC_842,(0,2):C.GC_843,(0,1):C.GC_843,(0,0):C.GC_810})

V_281 = Vertex(name = 'V_281',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_844})

V_282 = Vertex(name = 'V_282',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_878,(0,3):C.GC_960,(0,2):C.GC_961,(0,1):C.GC_961,(0,0):C.GC_926})

V_283 = Vertex(name = 'V_283',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_962})

V_284 = Vertex(name = 'V_284',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_675,(0,3):C.GC_699,(0,2):C.GC_700,(0,1):C.GC_700,(0,0):C.GC_683})

V_285 = Vertex(name = 'V_285',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_701})

V_286 = Vertex(name = 'V_286',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF12, L.FFFF13, L.FFFF17, L.FFFF7 ],
               couplings = {(0,4):C.GC_877,(0,3):C.GC_957,(0,2):C.GC_958,(0,1):C.GC_958,(0,0):C.GC_925})

V_287 = Vertex(name = 'V_287',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10 ],
               couplings = {(0,0):C.GC_959})

V_288 = Vertex(name = 'V_288',
               particles = [ P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_4})

V_289 = Vertex(name = 'V_289',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_601})

V_290 = Vertex(name = 'V_290',
               particles = [ P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_626})

V_291 = Vertex(name = 'V_291',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_5})

V_292 = Vertex(name = 'V_292',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_614})

V_293 = Vertex(name = 'V_293',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_602})

V_294 = Vertex(name = 'V_294',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV6 ],
               couplings = {(0,0):C.GC_615})

V_295 = Vertex(name = 'V_295',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_623})

V_296 = Vertex(name = 'V_296',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_628})

V_297 = Vertex(name = 'V_297',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV3 ],
               couplings = {(0,0):C.GC_600})

V_298 = Vertex(name = 'V_298',
               particles = [ P.t__tilde__, P.c, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_96,(0,0):C.GC_570})

V_299 = Vertex(name = 'V_299',
               particles = [ P.t__tilde__, P.u, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_92,(0,0):C.GC_568})

V_300 = Vertex(name = 'V_300',
               particles = [ P.t__tilde__, P.c, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_104,(0,0):C.GC_574})

V_301 = Vertex(name = 'V_301',
               particles = [ P.t__tilde__, P.u, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_100,(0,0):C.GC_572})

V_302 = Vertex(name = 'V_302',
               particles = [ P.t__tilde__, P.c, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_112,(0,0):C.GC_578})

V_303 = Vertex(name = 'V_303',
               particles = [ P.t__tilde__, P.u, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_108,(0,0):C.GC_576})

V_304 = Vertex(name = 'V_304',
               particles = [ P.t__tilde__, P.c, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_120,(0,0):C.GC_582})

V_305 = Vertex(name = 'V_305',
               particles = [ P.t__tilde__, P.u, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_116,(0,0):C.GC_580})

V_306 = Vertex(name = 'V_306',
               particles = [ P.t__tilde__, P.c, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_127,(0,0):C.GC_586})

V_307 = Vertex(name = 'V_307',
               particles = [ P.t__tilde__, P.u, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF5, L.FFFF7 ],
               couplings = {(0,2):C.GC_88,(0,1):C.GC_592,(0,0):C.GC_584})

V_308 = Vertex(name = 'V_308',
               particles = [ P.t__tilde__, P.c, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_135,(0,0):C.GC_590})

V_309 = Vertex(name = 'V_309',
               particles = [ P.t__tilde__, P.u, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_131,(0,0):C.GC_588})

V_310 = Vertex(name = 'V_310',
               particles = [ P.b__tilde__, P.b, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1011})

V_311 = Vertex(name = 'V_311',
               particles = [ P.b__tilde__, P.d, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_780})

V_312 = Vertex(name = 'V_312',
               particles = [ P.b__tilde__, P.s, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_895})

V_313 = Vertex(name = 'V_313',
               particles = [ P.d__tilde__, P.b, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF5, L.FFFF7 ],
               couplings = {(0,1):C.GC_1007,(0,0):C.GC_963})

V_314 = Vertex(name = 'V_314',
               particles = [ P.d__tilde__, P.d, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_776})

V_315 = Vertex(name = 'V_315',
               particles = [ P.d__tilde__, P.s, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_891})

V_316 = Vertex(name = 'V_316',
               particles = [ P.s__tilde__, P.b, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1009})

V_317 = Vertex(name = 'V_317',
               particles = [ P.s__tilde__, P.d, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_778})

V_318 = Vertex(name = 'V_318',
               particles = [ P.s__tilde__, P.s, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_893})

V_319 = Vertex(name = 'V_319',
               particles = [ P.b__tilde__, P.b, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1023})

V_320 = Vertex(name = 'V_320',
               particles = [ P.b__tilde__, P.d, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_792})

V_321 = Vertex(name = 'V_321',
               particles = [ P.b__tilde__, P.s, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_907})

V_322 = Vertex(name = 'V_322',
               particles = [ P.d__tilde__, P.b, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1019})

V_323 = Vertex(name = 'V_323',
               particles = [ P.d__tilde__, P.d, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_788})

V_324 = Vertex(name = 'V_324',
               particles = [ P.d__tilde__, P.s, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_903})

V_325 = Vertex(name = 'V_325',
               particles = [ P.s__tilde__, P.b, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1021})

V_326 = Vertex(name = 'V_326',
               particles = [ P.s__tilde__, P.d, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_790})

V_327 = Vertex(name = 'V_327',
               particles = [ P.s__tilde__, P.s, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_905})

V_328 = Vertex(name = 'V_328',
               particles = [ P.b__tilde__, P.b, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_999})

V_329 = Vertex(name = 'V_329',
               particles = [ P.b__tilde__, P.d, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_768})

V_330 = Vertex(name = 'V_330',
               particles = [ P.b__tilde__, P.s, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_883})

V_331 = Vertex(name = 'V_331',
               particles = [ P.d__tilde__, P.b, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_995})

V_332 = Vertex(name = 'V_332',
               particles = [ P.d__tilde__, P.d, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_764})

V_333 = Vertex(name = 'V_333',
               particles = [ P.d__tilde__, P.s, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_879})

V_334 = Vertex(name = 'V_334',
               particles = [ P.s__tilde__, P.b, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_997})

V_335 = Vertex(name = 'V_335',
               particles = [ P.s__tilde__, P.d, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_766})

V_336 = Vertex(name = 'V_336',
               particles = [ P.s__tilde__, P.s, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_881})

V_337 = Vertex(name = 'V_337',
               particles = [ P.b__tilde__, P.b, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1029})

V_338 = Vertex(name = 'V_338',
               particles = [ P.b__tilde__, P.d, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_798})

V_339 = Vertex(name = 'V_339',
               particles = [ P.b__tilde__, P.s, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_913})

V_340 = Vertex(name = 'V_340',
               particles = [ P.d__tilde__, P.b, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1025})

V_341 = Vertex(name = 'V_341',
               particles = [ P.d__tilde__, P.d, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_794})

V_342 = Vertex(name = 'V_342',
               particles = [ P.d__tilde__, P.s, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_909})

V_343 = Vertex(name = 'V_343',
               particles = [ P.s__tilde__, P.b, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1027})

V_344 = Vertex(name = 'V_344',
               particles = [ P.s__tilde__, P.d, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_796})

V_345 = Vertex(name = 'V_345',
               particles = [ P.s__tilde__, P.s, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_911})

V_346 = Vertex(name = 'V_346',
               particles = [ P.b__tilde__, P.b, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1005})

V_347 = Vertex(name = 'V_347',
               particles = [ P.b__tilde__, P.d, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_774})

V_348 = Vertex(name = 'V_348',
               particles = [ P.b__tilde__, P.s, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_889})

V_349 = Vertex(name = 'V_349',
               particles = [ P.d__tilde__, P.b, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1001})

V_350 = Vertex(name = 'V_350',
               particles = [ P.d__tilde__, P.d, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_770})

V_351 = Vertex(name = 'V_351',
               particles = [ P.d__tilde__, P.s, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_885})

V_352 = Vertex(name = 'V_352',
               particles = [ P.s__tilde__, P.b, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1003})

V_353 = Vertex(name = 'V_353',
               particles = [ P.s__tilde__, P.d, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_772})

V_354 = Vertex(name = 'V_354',
               particles = [ P.s__tilde__, P.s, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_887})

V_355 = Vertex(name = 'V_355',
               particles = [ P.b__tilde__, P.b, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1017})

V_356 = Vertex(name = 'V_356',
               particles = [ P.b__tilde__, P.d, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_786})

V_357 = Vertex(name = 'V_357',
               particles = [ P.b__tilde__, P.s, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_901})

V_358 = Vertex(name = 'V_358',
               particles = [ P.d__tilde__, P.b, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1013})

V_359 = Vertex(name = 'V_359',
               particles = [ P.d__tilde__, P.d, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_782})

V_360 = Vertex(name = 'V_360',
               particles = [ P.d__tilde__, P.s, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_897})

V_361 = Vertex(name = 'V_361',
               particles = [ P.s__tilde__, P.b, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_1015})

V_362 = Vertex(name = 'V_362',
               particles = [ P.s__tilde__, P.d, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_784})

V_363 = Vertex(name = 'V_363',
               particles = [ P.s__tilde__, P.s, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF7 ],
               couplings = {(0,0):C.GC_899})

V_364 = Vertex(name = 'V_364',
               particles = [ P.b__tilde__, P.b, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1012,(0,0):C.GC_973})

V_365 = Vertex(name = 'V_365',
               particles = [ P.b__tilde__, P.d, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_781,(0,0):C.GC_742})

V_366 = Vertex(name = 'V_366',
               particles = [ P.e__plus__, P.mu__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF5, L.FFFF7 ],
               couplings = {(0,2):C.GC_896,(0,1):C.GC_635,(0,0):C.GC_857})

V_367 = Vertex(name = 'V_367',
               particles = [ P.d__tilde__, P.b, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1008,(0,0):C.GC_971})

V_368 = Vertex(name = 'V_368',
               particles = [ P.d__tilde__, P.d, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_777,(0,0):C.GC_740})

V_369 = Vertex(name = 'V_369',
               particles = [ P.e__plus__, P.mu__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_892,(0,0):C.GC_855})

V_370 = Vertex(name = 'V_370',
               particles = [ P.b__tilde__, P.b, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1024,(0,0):C.GC_979})

V_371 = Vertex(name = 'V_371',
               particles = [ P.b__tilde__, P.d, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_793,(0,0):C.GC_748})

V_372 = Vertex(name = 'V_372',
               particles = [ P.b__tilde__, P.s, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_908,(0,0):C.GC_863})

V_373 = Vertex(name = 'V_373',
               particles = [ P.d__tilde__, P.b, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1020,(0,0):C.GC_977})

V_374 = Vertex(name = 'V_374',
               particles = [ P.d__tilde__, P.d, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_789,(0,0):C.GC_746})

V_375 = Vertex(name = 'V_375',
               particles = [ P.d__tilde__, P.s, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_904,(0,0):C.GC_861})

V_376 = Vertex(name = 'V_376',
               particles = [ P.b__tilde__, P.b, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1000,(0,0):C.GC_967})

V_377 = Vertex(name = 'V_377',
               particles = [ P.b__tilde__, P.d, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_769,(0,0):C.GC_736})

V_378 = Vertex(name = 'V_378',
               particles = [ P.mu__plus__, P.e__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_884,(0,0):C.GC_851})

V_379 = Vertex(name = 'V_379',
               particles = [ P.d__tilde__, P.b, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_996,(0,0):C.GC_965})

V_380 = Vertex(name = 'V_380',
               particles = [ P.d__tilde__, P.d, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_765,(0,0):C.GC_734})

V_381 = Vertex(name = 'V_381',
               particles = [ P.mu__plus__, P.e__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_880,(0,0):C.GC_849})

V_382 = Vertex(name = 'V_382',
               particles = [ P.b__tilde__, P.b, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1030,(0,0):C.GC_982})

V_383 = Vertex(name = 'V_383',
               particles = [ P.b__tilde__, P.d, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_799,(0,0):C.GC_751})

V_384 = Vertex(name = 'V_384',
               particles = [ P.b__tilde__, P.s, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_914,(0,0):C.GC_866})

V_385 = Vertex(name = 'V_385',
               particles = [ P.d__tilde__, P.b, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1026,(0,0):C.GC_980})

V_386 = Vertex(name = 'V_386',
               particles = [ P.d__tilde__, P.d, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_795,(0,0):C.GC_749})

V_387 = Vertex(name = 'V_387',
               particles = [ P.d__tilde__, P.s, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_910,(0,0):C.GC_864})

V_388 = Vertex(name = 'V_388',
               particles = [ P.s__tilde__, P.b, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1010,(0,0):C.GC_972})

V_389 = Vertex(name = 'V_389',
               particles = [ P.s__tilde__, P.b, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1022,(0,0):C.GC_978})

V_390 = Vertex(name = 'V_390',
               particles = [ P.s__tilde__, P.b, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_998,(0,0):C.GC_966})

V_391 = Vertex(name = 'V_391',
               particles = [ P.s__tilde__, P.b, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1028,(0,0):C.GC_981})

V_392 = Vertex(name = 'V_392',
               particles = [ P.s__tilde__, P.d, P.e__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_779,(0,0):C.GC_741})

V_393 = Vertex(name = 'V_393',
               particles = [ P.s__tilde__, P.d, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_791,(0,0):C.GC_747})

V_394 = Vertex(name = 'V_394',
               particles = [ P.s__tilde__, P.d, P.mu__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF5, L.FFFF7 ],
               couplings = {(0,2):C.GC_767,(0,1):C.GC_632,(0,0):C.GC_735})

V_395 = Vertex(name = 'V_395',
               particles = [ P.s__tilde__, P.d, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_797,(0,0):C.GC_750})

V_396 = Vertex(name = 'V_396',
               particles = [ P.e__plus__, P.mu__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF5, L.FFFF7 ],
               couplings = {(0,2):C.GC_894,(0,1):C.GC_634,(0,0):C.GC_856})

V_397 = Vertex(name = 'V_397',
               particles = [ P.s__tilde__, P.s, P.e__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_906,(0,0):C.GC_862})

V_398 = Vertex(name = 'V_398',
               particles = [ P.mu__plus__, P.e__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_882,(0,0):C.GC_850})

V_399 = Vertex(name = 'V_399',
               particles = [ P.s__tilde__, P.s, P.mu__plus__, P.ta__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_912,(0,0):C.GC_865})

V_400 = Vertex(name = 'V_400',
               particles = [ P.b__tilde__, P.b, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1006,(0,0):C.GC_970})

V_401 = Vertex(name = 'V_401',
               particles = [ P.b__tilde__, P.d, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_775,(0,0):C.GC_739})

V_402 = Vertex(name = 'V_402',
               particles = [ P.ta__plus__, P.e__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_890,(0,0):C.GC_854})

V_403 = Vertex(name = 'V_403',
               particles = [ P.d__tilde__, P.b, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1002,(0,0):C.GC_968})

V_404 = Vertex(name = 'V_404',
               particles = [ P.d__tilde__, P.d, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_771,(0,0):C.GC_737})

V_405 = Vertex(name = 'V_405',
               particles = [ P.ta__plus__, P.e__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_886,(0,0):C.GC_852})

V_406 = Vertex(name = 'V_406',
               particles = [ P.s__tilde__, P.b, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1004,(0,0):C.GC_969})

V_407 = Vertex(name = 'V_407',
               particles = [ P.s__tilde__, P.d, P.ta__plus__, P.e__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_773,(0,0):C.GC_738})

V_408 = Vertex(name = 'V_408',
               particles = [ P.ta__plus__, P.e__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_888,(0,0):C.GC_853})

V_409 = Vertex(name = 'V_409',
               particles = [ P.b__tilde__, P.b, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1018,(0,0):C.GC_976})

V_410 = Vertex(name = 'V_410',
               particles = [ P.b__tilde__, P.d, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_787,(0,0):C.GC_745})

V_411 = Vertex(name = 'V_411',
               particles = [ P.ta__plus__, P.mu__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_902,(0,0):C.GC_860})

V_412 = Vertex(name = 'V_412',
               particles = [ P.d__tilde__, P.b, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1014,(0,0):C.GC_974})

V_413 = Vertex(name = 'V_413',
               particles = [ P.d__tilde__, P.d, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_783,(0,0):C.GC_743})

V_414 = Vertex(name = 'V_414',
               particles = [ P.ta__plus__, P.mu__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF14, L.FFFF7 ],
               couplings = {(0,2):C.GC_898,(0,0):C.GC_669,(0,1):C.GC_858})

V_415 = Vertex(name = 'V_415',
               particles = [ P.s__tilde__, P.b, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_1016,(0,0):C.GC_975})

V_416 = Vertex(name = 'V_416',
               particles = [ P.s__tilde__, P.d, P.ta__plus__, P.mu__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF15, L.FFFF7 ],
               couplings = {(0,1):C.GC_785,(0,0):C.GC_744})

V_417 = Vertex(name = 'V_417',
               particles = [ P.ta__plus__, P.mu__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_900,(0,0):C.GC_859})

V_418 = Vertex(name = 'V_418',
               particles = [ P.c__tilde__, P.t, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_94,(0,0):C.GC_569})

V_419 = Vertex(name = 'V_419',
               particles = [ P.u__tilde__, P.t, P.vm__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_90,(0,0):C.GC_567})

V_420 = Vertex(name = 'V_420',
               particles = [ P.c__tilde__, P.t, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_102,(0,0):C.GC_573})

V_421 = Vertex(name = 'V_421',
               particles = [ P.u__tilde__, P.t, P.vt__tilde__, P.ve ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_98,(0,0):C.GC_571})

V_422 = Vertex(name = 'V_422',
               particles = [ P.c__tilde__, P.t, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_110,(0,0):C.GC_577})

V_423 = Vertex(name = 'V_423',
               particles = [ P.u__tilde__, P.t, P.ve__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_106,(0,0):C.GC_575})

V_424 = Vertex(name = 'V_424',
               particles = [ P.c__tilde__, P.t, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_118,(0,0):C.GC_581})

V_425 = Vertex(name = 'V_425',
               particles = [ P.u__tilde__, P.t, P.vt__tilde__, P.vm ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_114,(0,0):C.GC_579})

V_426 = Vertex(name = 'V_426',
               particles = [ P.c__tilde__, P.t, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_125,(0,0):C.GC_585})

V_427 = Vertex(name = 'V_427',
               particles = [ P.u__tilde__, P.t, P.ve__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_122,(0,0):C.GC_583})

V_428 = Vertex(name = 'V_428',
               particles = [ P.c__tilde__, P.t, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_133,(0,0):C.GC_589})

V_429 = Vertex(name = 'V_429',
               particles = [ P.u__tilde__, P.t, P.vm__tilde__, P.vt ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF14, L.FFFF7 ],
               couplings = {(0,1):C.GC_129,(0,0):C.GC_587})

V_430 = Vertex(name = 'V_430',
               particles = [ P.e__plus__, P.e__minus__, P.a ],
               color = [ '1' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_3})

V_431 = Vertex(name = 'V_431',
               particles = [ P.mu__plus__, P.mu__minus__, P.a ],
               color = [ '1' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_3})

V_432 = Vertex(name = 'V_432',
               particles = [ P.ta__plus__, P.ta__minus__, P.a ],
               color = [ '1' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_3})

V_433 = Vertex(name = 'V_433',
               particles = [ P.c__tilde__, P.c, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_2})

V_434 = Vertex(name = 'V_434',
               particles = [ P.t__tilde__, P.t, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_2})

V_435 = Vertex(name = 'V_435',
               particles = [ P.u__tilde__, P.u, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_2})

V_436 = Vertex(name = 'V_436',
               particles = [ P.b__tilde__, P.b, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_1})

V_437 = Vertex(name = 'V_437',
               particles = [ P.d__tilde__, P.d, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_1})

V_438 = Vertex(name = 'V_438',
               particles = [ P.s__tilde__, P.s, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_1})

V_439 = Vertex(name = 'V_439',
               particles = [ P.c__tilde__, P.c, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_440 = Vertex(name = 'V_440',
               particles = [ P.t__tilde__, P.t, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_441 = Vertex(name = 'V_441',
               particles = [ P.u__tilde__, P.u, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_442 = Vertex(name = 'V_442',
               particles = [ P.b__tilde__, P.b, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_443 = Vertex(name = 'V_443',
               particles = [ P.d__tilde__, P.d, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_444 = Vertex(name = 'V_444',
               particles = [ P.s__tilde__, P.s, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_7})

V_445 = Vertex(name = 'V_445',
               particles = [ P.b__tilde__, P.c, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_609})

V_446 = Vertex(name = 'V_446',
               particles = [ P.d__tilde__, P.c, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_607})

V_447 = Vertex(name = 'V_447',
               particles = [ P.s__tilde__, P.c, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_608})

V_448 = Vertex(name = 'V_448',
               particles = [ P.b__tilde__, P.t, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_612})

V_449 = Vertex(name = 'V_449',
               particles = [ P.d__tilde__, P.t, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_610})

V_450 = Vertex(name = 'V_450',
               particles = [ P.s__tilde__, P.t, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_611})

V_451 = Vertex(name = 'V_451',
               particles = [ P.b__tilde__, P.u, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_606})

V_452 = Vertex(name = 'V_452',
               particles = [ P.d__tilde__, P.u, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_604})

V_453 = Vertex(name = 'V_453',
               particles = [ P.s__tilde__, P.u, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_605})

V_454 = Vertex(name = 'V_454',
               particles = [ P.c__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_702})

V_455 = Vertex(name = 'V_455',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_964})

V_456 = Vertex(name = 'V_456',
               particles = [ P.u__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_637})

V_457 = Vertex(name = 'V_457',
               particles = [ P.c__tilde__, P.d, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_638})

V_458 = Vertex(name = 'V_458',
               particles = [ P.t__tilde__, P.d, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_733})

V_459 = Vertex(name = 'V_459',
               particles = [ P.u__tilde__, P.d, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_633})

V_460 = Vertex(name = 'V_460',
               particles = [ P.c__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_671})

V_461 = Vertex(name = 'V_461',
               particles = [ P.t__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_848})

V_462 = Vertex(name = 'V_462',
               particles = [ P.u__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_636})

V_463 = Vertex(name = 'V_463',
               particles = [ P.e__plus__, P.ve, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_464 = Vertex(name = 'V_464',
               particles = [ P.mu__plus__, P.vm, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_465 = Vertex(name = 'V_465',
               particles = [ P.ta__plus__, P.vt, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_466 = Vertex(name = 'V_466',
               particles = [ P.ve__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_467 = Vertex(name = 'V_467',
               particles = [ P.vm__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_468 = Vertex(name = 'V_468',
               particles = [ P.vt__tilde__, P.ta__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_603})

V_469 = Vertex(name = 'V_469',
               particles = [ P.c__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_620,(0,1):C.GC_617})

V_470 = Vertex(name = 'V_470',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_620,(0,1):C.GC_617})

V_471 = Vertex(name = 'V_471',
               particles = [ P.u__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_620,(0,1):C.GC_617})

V_472 = Vertex(name = 'V_472',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_619,(0,1):C.GC_616})

V_473 = Vertex(name = 'V_473',
               particles = [ P.d__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_619,(0,1):C.GC_616})

V_474 = Vertex(name = 'V_474',
               particles = [ P.s__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_619,(0,1):C.GC_616})

V_475 = Vertex(name = 'V_475',
               particles = [ P.ve__tilde__, P.ve, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_622})

V_476 = Vertex(name = 'V_476',
               particles = [ P.vm__tilde__, P.vm, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_622})

V_477 = Vertex(name = 'V_477',
               particles = [ P.vt__tilde__, P.vt, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_622})

V_478 = Vertex(name = 'V_478',
               particles = [ P.e__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_621,(0,1):C.GC_618})

V_479 = Vertex(name = 'V_479',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_621,(0,1):C.GC_618})

V_480 = Vertex(name = 'V_480',
               particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_621,(0,1):C.GC_618})

