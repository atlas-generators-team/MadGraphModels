# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.3.0 for Microsoft Windows (64-bit) (March 7, 2018)
# Date: Fri 20 Jul 2018 17:42:13


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

