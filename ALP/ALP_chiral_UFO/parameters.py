# This file was automatically created by FeynRules 2.3.26
# Mathematica version: 11.0.1 for Linux x86 (64-bit) (September 21, 2016)
# Date: Wed 20 Sep 2017 14:51:31



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
fa = Parameter(name = 'fa',
               nature = 'external',
               type = 'real',
               value = 1000,
               texname = 'f_a',
               lhablock = 'ALPPARS',
               lhacode = [ 1 ])

CGtil = Parameter(name = 'CGtil',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{\\tilde{G}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 2 ])

CWtil = Parameter(name = 'CWtil',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{\\tilde{W}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 3 ])

CBtil = Parameter(name = 'CBtil',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{\\tilde{B}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 4 ])

C1 = Parameter(name = 'C1',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_1',
               lhablock = 'ALPPARS',
               lhacode = [ 5 ])

a1 = Parameter(name = 'a1',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_1',
               lhablock = 'ALPPARS',
               lhacode = [ 6 ])

b1 = Parameter(name = 'b1',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_1',
               lhablock = 'ALPPARS',
               lhacode = [ 7 ])

C2 = Parameter(name = 'C2',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_2',
               lhablock = 'ALPPARS',
               lhacode = [ 8 ])

a2 = Parameter(name = 'a2',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_2',
               lhablock = 'ALPPARS',
               lhacode = [ 9 ])

b2 = Parameter(name = 'b2',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_2',
               lhablock = 'ALPPARS',
               lhacode = [ 10 ])

C3 = Parameter(name = 'C3',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_3',
               lhablock = 'ALPPARS',
               lhacode = [ 11 ])

a3 = Parameter(name = 'a3',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_3',
               lhablock = 'ALPPARS',
               lhacode = [ 12 ])

b3 = Parameter(name = 'b3',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_3',
               lhablock = 'ALPPARS',
               lhacode = [ 13 ])

C4 = Parameter(name = 'C4',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_4',
               lhablock = 'ALPPARS',
               lhacode = [ 14 ])

C5 = Parameter(name = 'C5',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_5',
               lhablock = 'ALPPARS',
               lhacode = [ 15 ])

C6 = Parameter(name = 'C6',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_6',
               lhablock = 'ALPPARS',
               lhacode = [ 16 ])

a6 = Parameter(name = 'a6',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_6',
               lhablock = 'ALPPARS',
               lhacode = [ 17 ])

b6 = Parameter(name = 'b6',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_6',
               lhablock = 'ALPPARS',
               lhacode = [ 18 ])

C7 = Parameter(name = 'C7',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_7',
               lhablock = 'ALPPARS',
               lhacode = [ 19 ])

a7 = Parameter(name = 'a7',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_7',
               lhablock = 'ALPPARS',
               lhacode = [ 20 ])

b7 = Parameter(name = 'b7',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_7',
               lhablock = 'ALPPARS',
               lhacode = [ 21 ])

C8 = Parameter(name = 'C8',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_8',
               lhablock = 'ALPPARS',
               lhacode = [ 22 ])

a8 = Parameter(name = 'a8',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_8',
               lhablock = 'ALPPARS',
               lhacode = [ 23 ])

b8 = Parameter(name = 'b8',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_8',
               lhablock = 'ALPPARS',
               lhacode = [ 24 ])

C9 = Parameter(name = 'C9',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'c_9',
               lhablock = 'ALPPARS',
               lhacode = [ 25 ])

C10 = Parameter(name = 'C10',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{10}',
                lhablock = 'ALPPARS',
                lhacode = [ 26 ])

a10 = Parameter(name = 'a10',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{10}',
                lhablock = 'ALPPARS',
                lhacode = [ 27 ])

b10 = Parameter(name = 'b10',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{10}',
                lhablock = 'ALPPARS',
                lhacode = [ 28 ])

C2D = Parameter(name = 'C2D',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{2 D}',
                lhablock = 'ALPPARS',
                lhacode = [ 29 ])

a2D = Parameter(name = 'a2D',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{2 D}',
                lhablock = 'ALPPARS',
                lhacode = [ 30 ])

b2D = Parameter(name = 'b2D',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{2 D}',
                lhablock = 'ALPPARS',
                lhacode = [ 31 ])

C11 = Parameter(name = 'C11',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{11}',
                lhablock = 'ALPPARS',
                lhacode = [ 32 ])

a11 = Parameter(name = 'a11',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{11}',
                lhablock = 'ALPPARS',
                lhacode = [ 33 ])

b11 = Parameter(name = 'b11',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{11}',
                lhablock = 'ALPPARS',
                lhacode = [ 34 ])

C12 = Parameter(name = 'C12',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{12}',
                lhablock = 'ALPPARS',
                lhacode = [ 35 ])

a12 = Parameter(name = 'a12',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{12}',
                lhablock = 'ALPPARS',
                lhacode = [ 36 ])

b12 = Parameter(name = 'b12',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{12}',
                lhablock = 'ALPPARS',
                lhacode = [ 37 ])

C13 = Parameter(name = 'C13',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{13}',
                lhablock = 'ALPPARS',
                lhacode = [ 38 ])

a13 = Parameter(name = 'a13',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{13}',
                lhablock = 'ALPPARS',
                lhacode = [ 39 ])

b13 = Parameter(name = 'b13',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{13}',
                lhablock = 'ALPPARS',
                lhacode = [ 40 ])

C14 = Parameter(name = 'C14',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{14}',
                lhablock = 'ALPPARS',
                lhacode = [ 41 ])

a14 = Parameter(name = 'a14',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{14}',
                lhablock = 'ALPPARS',
                lhacode = [ 42 ])

b14 = Parameter(name = 'b14',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{14}',
                lhablock = 'ALPPARS',
                lhacode = [ 43 ])

C15 = Parameter(name = 'C15',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{15}',
                lhablock = 'ALPPARS',
                lhacode = [ 44 ])

a15 = Parameter(name = 'a15',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{15}',
                lhablock = 'ALPPARS',
                lhacode = [ 45 ])

a15prime = Parameter(name = 'a15prime',
                     nature = 'external',
                     type = 'real',
                     value = 1.,
                     texname = '\\text{Subsuperscript}[a,15,\\prime ]',
                     lhablock = 'ALPPARS',
                     lhacode = [ 46 ])

C16 = Parameter(name = 'C16',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{16}',
                lhablock = 'ALPPARS',
                lhacode = [ 47 ])

a16 = Parameter(name = 'a16',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{16}',
                lhablock = 'ALPPARS',
                lhacode = [ 48 ])

a16prime = Parameter(name = 'a16prime',
                     nature = 'external',
                     type = 'real',
                     value = 1.,
                     texname = '\\text{Subsuperscript}[a,16,\\prime ]',
                     lhablock = 'ALPPARS',
                     lhacode = [ 49 ])

C17 = Parameter(name = 'C17',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'c_{17}',
                lhablock = 'ALPPARS',
                lhacode = [ 50 ])

a17 = Parameter(name = 'a17',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'a_{17}',
                lhablock = 'ALPPARS',
                lhacode = [ 51 ])

b17 = Parameter(name = 'b17',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'b_{17}',
                lhablock = 'ALPPARS',
                lhacode = [ 52 ])

aU = Parameter(name = 'aU',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_U',
               lhablock = 'ALPPARS',
               lhacode = [ 53 ])

bU = Parameter(name = 'bU',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_U',
               lhablock = 'ALPPARS',
               lhacode = [ 54 ])

aD = Parameter(name = 'aD',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_D',
               lhablock = 'ALPPARS',
               lhacode = [ 55 ])

bD = Parameter(name = 'bD',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_D',
               lhablock = 'ALPPARS',
               lhacode = [ 56 ])

aL = Parameter(name = 'aL',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'a_L',
               lhablock = 'ALPPARS',
               lhacode = [ 57 ])

bL = Parameter(name = 'bL',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = 'b_L',
               lhablock = 'ALPPARS',
               lhacode = [ 58 ])

cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Ma = Parameter(name = 'Ma',
               nature = 'external',
               type = 'real',
               value = 0.001,
               texname = '\\text{Ma}',
               lhablock = 'MASS',
               lhacode = [ 9000005 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1a11 = Parameter(name = 'I1a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM1x1)',
                  texname = '\\text{I1a11}')

I1a12 = Parameter(name = 'I1a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM2x1)',
                  texname = '\\text{I1a12}')

I1a13 = Parameter(name = 'I1a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ydo*complexconjugate(CKM3x1)',
                  texname = '\\text{I1a13}')

I1a21 = Parameter(name = 'I1a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM1x2)',
                  texname = '\\text{I1a21}')

I1a22 = Parameter(name = 'I1a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM2x2)',
                  texname = '\\text{I1a22}')

I1a23 = Parameter(name = 'I1a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'ys*complexconjugate(CKM3x2)',
                  texname = '\\text{I1a23}')

I1a31 = Parameter(name = 'I1a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM1x3)',
                  texname = '\\text{I1a31}')

I1a32 = Parameter(name = 'I1a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM2x3)',
                  texname = '\\text{I1a32}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb*complexconjugate(CKM3x3)',
                  texname = '\\text{I1a33}')

I2a11 = Parameter(name = 'I2a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x1*ydo',
                  texname = '\\text{I2a11}')

I2a12 = Parameter(name = 'I2a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x2*ys',
                  texname = '\\text{I2a12}')

I2a13 = Parameter(name = 'I2a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM1x3*yb',
                  texname = '\\text{I2a13}')

I2a21 = Parameter(name = 'I2a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x1*ydo',
                  texname = '\\text{I2a21}')

I2a22 = Parameter(name = 'I2a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x2*ys',
                  texname = '\\text{I2a22}')

I2a23 = Parameter(name = 'I2a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM2x3*yb',
                  texname = '\\text{I2a23}')

I2a31 = Parameter(name = 'I2a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x1*ydo',
                  texname = '\\text{I2a31}')

I2a32 = Parameter(name = 'I2a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x2*ys',
                  texname = '\\text{I2a32}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'CKM3x3*yb',
                  texname = '\\text{I2a33}')

