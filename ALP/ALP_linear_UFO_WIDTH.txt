Requestor: Victoria Sanchez Sebastian
Contents: Axion-like particle, 4 linear coupling operators, with ALP width in parameters for proper ALP decay calculations
Provided by Ilaria Brivio, author of other ALP_linear_UFO model and ALP EFT paper
Paper ALP EFT: https://arxiv.org/abs/1701.05379
JIRA: https://its.cern.ch/jira/browse/AGENE-2079
