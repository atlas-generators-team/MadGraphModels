#!/usr/bin/bash

# Add soft links for all the models to the main directory (also cleans up existing links)
./post-checkout.sh

# Add all the models to our python path
export PYTHONPATH=`pwd`:${PYTHONPATH}
