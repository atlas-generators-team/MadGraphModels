# This file was automatically created by FeynRules 2.3.36
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Tue 25 Apr 2023 15:27:19



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.225,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

th13 = Parameter(name = 'th13',
                 nature = 'external',
                 type = 'real',
                 value = 0.00369,
                 texname = '\\theta _{13}',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 2 ])

th23 = Parameter(name = 'th23',
                 nature = 'external',
                 type = 'real',
                 value = 0.04182,
                 texname = '\\theta _{23}',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 3 ])

del13 = Parameter(name = 'del13',
                  nature = 'external',
                  type = 'real',
                  value = 1.144,
                  texname = '\\delta _{13}',
                  lhablock = 'CKMBLOCK',
                  lhacode = [ 4 ])

fK = Parameter(name = 'fK',
               nature = 'external',
               type = 'real',
               value = 0.1557,
               texname = '\\text{fK}',
               lhablock = 'MESONBLOCK',
               lhacode = [ 2 ])

fpi = Parameter(name = 'fpi',
                nature = 'external',
                type = 'real',
                value = 0.1302,
                texname = '\\text{fpi}',
                lhablock = 'MESONBLOCK',
                lhacode = [ 3 ])

feta = Parameter(name = 'feta',
                 nature = 'external',
                 type = 'real',
                 value = 0.08159,
                 texname = '\\text{feta}',
                 lhablock = 'MESONBLOCK',
                 lhacode = [ 4 ])

fetap = Parameter(name = 'fetap',
                  nature = 'external',
                  type = 'real',
                  value = -0.0946,
                  texname = '\\text{fetap}',
                  lhablock = 'MESONBLOCK',
                  lhacode = [ 5 ])

fD = Parameter(name = 'fD',
               nature = 'external',
               type = 'real',
               value = 0.209,
               texname = '\\text{fD}',
               lhablock = 'MESONBLOCK',
               lhacode = [ 6 ])

frho = Parameter(name = 'frho',
                 nature = 'external',
                 type = 'real',
                 value = 0.17,
                 texname = '\\text{frho}',
                 lhablock = 'MESONBLOCK',
                 lhacode = [ 7 ])

fDs = Parameter(name = 'fDs',
                nature = 'external',
                type = 'real',
                value = 0.248,
                texname = '\\text{fDs}',
                lhablock = 'MESONBLOCK',
                lhacode = [ 8 ])

fomega = Parameter(name = 'fomega',
                   nature = 'external',
                   type = 'real',
                   value = 0.155,
                   texname = '\\text{fomega}',
                   lhablock = 'MESONBLOCK',
                   lhacode = [ 9 ])

fphi = Parameter(name = 'fphi',
                 nature = 'external',
                 type = 'real',
                 value = 0.232,
                 texname = '\\text{fphi}',
                 lhablock = 'MESONBLOCK',
                 lhacode = [ 10 ])

fB = Parameter(name = 'fB',
               nature = 'external',
               type = 'real',
               value = 0.192,
               texname = '\\text{fB}',
               lhablock = 'MESONBLOCK',
               lhacode = [ 11 ])

fBs = Parameter(name = 'fBs',
                nature = 'external',
                type = 'real',
                value = 0.2284,
                texname = '\\text{fBs}',
                lhablock = 'MESONBLOCK',
                lhacode = [ 12 ])

fBc = Parameter(name = 'fBc',
                nature = 'external',
                type = 'real',
                value = 0.19,
                texname = '\\text{fBc}',
                lhablock = 'MESONBLOCK',
                lhacode = [ 13 ])

fKstar = Parameter(name = 'fKstar',
                   nature = 'external',
                   type = 'real',
                   value = 0.177,
                   texname = '\\text{fKstar}',
                   lhablock = 'MESONBLOCK',
                   lhacode = [ 34 ])

VeN1 = Parameter(name = 'VeN1',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = 'V_{\\text{eN1}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 1 ])

VeN2 = Parameter(name = 'VeN2',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = 'V_{\\text{eN2}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 2 ])

VeN3 = Parameter(name = 'VeN3',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = 'V_{\\text{eN3}}',
                 lhablock = 'NUMIXING',
                 lhacode = [ 3 ])

VmuN1 = Parameter(name = 'VmuN1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{muN1}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 4 ])

VmuN2 = Parameter(name = 'VmuN2',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'V_{\\text{muN2}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 5 ])

VmuN3 = Parameter(name = 'VmuN3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{muN3}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 6 ])

VtaN1 = Parameter(name = 'VtaN1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{taN1}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 7 ])

VtaN2 = Parameter(name = 'VtaN2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'V_{\\text{taN2}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 8 ])

VtaN3 = Parameter(name = 'VtaN3',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'V_{\\text{taN3}}',
                  lhablock = 'NUMIXING',
                  lhacode = [ 9 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.951,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000117461,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.18,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172.69,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.00051099895,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.1056583755,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.77686,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.00051099895,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.1056583755,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.77686,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172.69,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.18,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125.7,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

mN1 = Parameter(name = 'mN1',
                nature = 'external',
                type = 'real',
                value = 300.,
                texname = '\\text{mN1}',
                lhablock = 'MASS',
                lhacode = [ 72 ])

mN2 = Parameter(name = 'mN2',
                nature = 'external',
                type = 'real',
                value = 500.,
                texname = '\\text{mN2}',
                lhablock = 'MASS',
                lhacode = [ 74 ])

mN3 = Parameter(name = 'mN3',
                nature = 'external',
                type = 'real',
                value = 1000.,
                texname = '\\text{mN3}',
                lhablock = 'MASS',
                lhacode = [ 76 ])

MK = Parameter(name = 'MK',
               nature = 'external',
               type = 'real',
               value = 0.493677,
               texname = '\\text{MK}',
               lhablock = 'MASS',
               lhacode = [ 321 ])

MPip = Parameter(name = 'MPip',
                 nature = 'external',
                 type = 'real',
                 value = 0.13957039,
                 texname = '\\text{MPip}',
                 lhablock = 'MASS',
                 lhacode = [ 211 ])

MDd = Parameter(name = 'MDd',
                nature = 'external',
                type = 'real',
                value = 1.869,
                texname = '\\text{MDd}',
                lhablock = 'MASS',
                lhacode = [ 411 ])

MPi0 = Parameter(name = 'MPi0',
                 nature = 'external',
                 type = 'real',
                 value = 0.1349768,
                 texname = '\\text{MPi0}',
                 lhablock = 'MASS',
                 lhacode = [ 111 ])

Meta = Parameter(name = 'Meta',
                 nature = 'external',
                 type = 'real',
                 value = 0.547862,
                 texname = '\\text{Meta}',
                 lhablock = 'MASS',
                 lhacode = [ 221 ])

Metap = Parameter(name = 'Metap',
                  nature = 'external',
                  type = 'real',
                  value = 0.95778,
                  texname = '\\text{Metap}',
                  lhablock = 'MASS',
                  lhacode = [ 331 ])

MDs = Parameter(name = 'MDs',
                nature = 'external',
                type = 'real',
                value = 1.96835,
                texname = '\\text{MDs}',
                lhablock = 'MASS',
                lhacode = [ 431 ])

MBu = Parameter(name = 'MBu',
                nature = 'external',
                type = 'real',
                value = 5.27934,
                texname = '\\text{MBu}',
                lhablock = 'MASS',
                lhacode = [ 521 ])

MBc = Parameter(name = 'MBc',
                nature = 'external',
                type = 'real',
                value = 6.27447,
                texname = '\\text{MBc}',
                lhablock = 'MASS',
                lhacode = [ 541 ])

Mrho0 = Parameter(name = 'Mrho0',
                  nature = 'external',
                  type = 'real',
                  value = 0.77526,
                  texname = '\\text{Mrho0}',
                  lhablock = 'MASS',
                  lhacode = [ 113 ])

Mrho = Parameter(name = 'Mrho',
                 nature = 'external',
                 type = 'real',
                 value = 0.77511,
                 texname = '\\text{Mrho}',
                 lhablock = 'MASS',
                 lhacode = [ 213 ])

Mom = Parameter(name = 'Mom',
                nature = 'external',
                type = 'real',
                value = 0.78266,
                texname = '\\text{Mom}',
                lhablock = 'MASS',
                lhacode = [ 223 ])

MKstar = Parameter(name = 'MKstar',
                   nature = 'external',
                   type = 'real',
                   value = 0.89167,
                   texname = '\\text{MKstar}',
                   lhablock = 'MASS',
                   lhacode = [ 323 ])

Mphi = Parameter(name = 'Mphi',
                 nature = 'external',
                 type = 'real',
                 value = 1.019461,
                 texname = '\\text{Mphi}',
                 lhablock = 'MASS',
                 lhacode = [ 333 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WTA = Parameter(name = 'WTA',
                nature = 'external',
                type = 'real',
                value = 2.267e-12,
                texname = '\\text{WTA}',
                lhablock = 'DECAY',
                lhacode = [ 15 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.35,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00417,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WN1 = Parameter(name = 'WN1',
                nature = 'external',
                type = 'real',
                value = 0.303,
                texname = '\\text{WN1}',
                lhablock = 'DECAY',
                lhacode = [ 72 ])

WN2 = Parameter(name = 'WN2',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = '\\text{WN2}',
                lhablock = 'DECAY',
                lhacode = [ 74 ])

WN3 = Parameter(name = 'WN3',
                nature = 'external',
                type = 'real',
                value = 12.3,
                texname = '\\text{WN3}',
                lhablock = 'DECAY',
                lhacode = [ 76 ])

Wrho0 = Parameter(name = 'Wrho0',
                  nature = 'external',
                  type = 'real',
                  value = 0.1478,
                  texname = '\\text{Wrho0}',
                  lhablock = 'DECAY',
                  lhacode = [ 113 ])

Wrho = Parameter(name = 'Wrho',
                 nature = 'external',
                 type = 'real',
                 value = 0.1491,
                 texname = '\\text{Wrho}',
                 lhablock = 'DECAY',
                 lhacode = [ 213 ])

Wom = Parameter(name = 'Wom',
                nature = 'external',
                type = 'real',
                value = 0.00849,
                texname = '\\text{Wom}',
                lhablock = 'DECAY',
                lhacode = [ 223 ])

WKstar = Parameter(name = 'WKstar',
                   nature = 'external',
                   type = 'real',
                   value = 0.0508,
                   texname = '\\text{WKstar}',
                   lhablock = 'DECAY',
                   lhacode = [ 323 ])

Wphi = Parameter(name = 'Wphi',
                 nature = 'external',
                 type = 'real',
                 value = 0.004249,
                 texname = '\\text{Wphi}',
                 lhablock = 'DECAY',
                 lhacode = [ 333 ])

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)*cmath.cos(th13)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(th13)*cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.exp(-(del13*complex(0,1)))*cmath.sin(th13)',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-(cmath.cos(th23)*cmath.sin(cabi)) - cmath.cos(cabi)*cmath.exp(del13*complex(0,1))*cmath.sin(th13)*cmath.sin(th23)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)*cmath.cos(th23) - cmath.exp(del13*complex(0,1))*cmath.sin(cabi)*cmath.sin(th13)*cmath.sin(th23)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(th13)*cmath.sin(th23)',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-(cmath.cos(cabi)*cmath.cos(th23)*cmath.exp(del13*complex(0,1))*cmath.sin(th13)) + cmath.sin(cabi)*cmath.sin(th23)',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '-(cmath.cos(th23)*cmath.exp(del13*complex(0,1))*cmath.sin(cabi)*cmath.sin(th13)) - cmath.cos(cabi)*cmath.sin(th23)',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(th13)*cmath.cos(th23)',
                   texname = '\\text{CKM3x3}')

PMNS1x4 = Parameter(name = 'PMNS1x4',
                    nature = 'internal',
                    type = 'real',
                    value = 'VeN1',
                    texname = '\\text{PMNS1x4}')

PMNS1x5 = Parameter(name = 'PMNS1x5',
                    nature = 'internal',
                    type = 'real',
                    value = 'VeN2',
                    texname = '\\text{PMNS1x5}')

PMNS1x6 = Parameter(name = 'PMNS1x6',
                    nature = 'internal',
                    type = 'real',
                    value = 'VeN3',
                    texname = '\\text{PMNS1x6}')

PMNS2x4 = Parameter(name = 'PMNS2x4',
                    nature = 'internal',
                    type = 'real',
                    value = 'VmuN1',
                    texname = '\\text{PMNS2x4}')

PMNS2x5 = Parameter(name = 'PMNS2x5',
                    nature = 'internal',
                    type = 'real',
                    value = 'VmuN2',
                    texname = '\\text{PMNS2x5}')

PMNS2x6 = Parameter(name = 'PMNS2x6',
                    nature = 'internal',
                    type = 'real',
                    value = 'VmuN3',
                    texname = '\\text{PMNS2x6}')

PMNS3x4 = Parameter(name = 'PMNS3x4',
                    nature = 'internal',
                    type = 'real',
                    value = 'VtaN1',
                    texname = '\\text{PMNS3x4}')

PMNS3x5 = Parameter(name = 'PMNS3x5',
                    nature = 'internal',
                    type = 'real',
                    value = 'VtaN2',
                    texname = '\\text{PMNS3x5}')

PMNS3x6 = Parameter(name = 'PMNS3x6',
                    nature = 'internal',
                    type = 'real',
                    value = 'VtaN3',
                    texname = '\\text{PMNS3x6}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

numass4x4 = Parameter(name = 'numass4x4',
                      nature = 'internal',
                      type = 'real',
                      value = 'mN1',
                      texname = '\\text{numass4x4}')

numass5x5 = Parameter(name = 'numass5x5',
                      nature = 'internal',
                      type = 'real',
                      value = 'mN2',
                      texname = '\\text{numass5x5}')

numass6x6 = Parameter(name = 'numass6x6',
                      nature = 'internal',
                      type = 'real',
                      value = 'mN3',
                      texname = '\\text{numass6x6}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gN = Parameter(name = 'gN',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_N')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

