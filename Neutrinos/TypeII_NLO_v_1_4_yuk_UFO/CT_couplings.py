# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Thu 7 Dec 2023 15:03:04


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_404_1 = Coupling(name = 'R2GC_404_1',
                      value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_405_2 = Coupling(name = 'R2GC_405_2',
                      value = '-0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2',
                      order = {'QCD':2,'QED':1})

R2GC_406_3 = Coupling(name = 'R2GC_406_3',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_408_4 = Coupling(name = 'R2GC_408_4',
                      value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_409_5 = Coupling(name = 'R2GC_409_5',
                      value = '-0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_416_6 = Coupling(name = 'R2GC_416_6',
                      value = '-0.125*(complex(0,1)*G**2*MB**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_416_7 = Coupling(name = 'R2GC_416_7',
                      value = '-0.125*(complex(0,1)*G**2*MT**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_417_8 = Coupling(name = 'R2GC_417_8',
                      value = '-0.125*(cxi*complex(0,1)*G**2*MB*yb)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_417_9 = Coupling(name = 'R2GC_417_9',
                      value = '-0.125*(cxi*complex(0,1)*G**2*MT*yt)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_418_10 = Coupling(name = 'R2GC_418_10',
                       value = '(complex(0,1)*G**2*MB*sxi*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_418_11 = Coupling(name = 'R2GC_418_11',
                       value = '(complex(0,1)*G**2*MT*sxi*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_419_12 = Coupling(name = 'R2GC_419_12',
                       value = '-0.0625*(cxi**2*complex(0,1)*G**2*yb**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_419_13 = Coupling(name = 'R2GC_419_13',
                       value = '-0.0625*(cxi**2*complex(0,1)*G**2*yt**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_420_14 = Coupling(name = 'R2GC_420_14',
                       value = '(cxi*complex(0,1)*G**2*sxi*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_420_15 = Coupling(name = 'R2GC_420_15',
                       value = '(cxi*complex(0,1)*G**2*sxi*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_421_16 = Coupling(name = 'R2GC_421_16',
                       value = '-0.0625*(complex(0,1)*G**2*sxi**2*yb**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_421_17 = Coupling(name = 'R2GC_421_17',
                       value = '-0.0625*(complex(0,1)*G**2*sxi**2*yt**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_422_18 = Coupling(name = 'R2GC_422_18',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yb**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_422_19 = Coupling(name = 'R2GC_422_19',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yt**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_423_20 = Coupling(name = 'R2GC_423_20',
                       value = '(complex(0,1)*G**2*vev*vevD*yb**2)/(8.*cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_423_21 = Coupling(name = 'R2GC_423_21',
                       value = '(complex(0,1)*G**2*vev*vevD*yt**2)/(8.*cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_424_22 = Coupling(name = 'R2GC_424_22',
                       value = '-0.25*(complex(0,1)*G**2*vevD**2*yb**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_424_23 = Coupling(name = 'R2GC_424_23',
                       value = '-0.25*(complex(0,1)*G**2*vevD**2*yt**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_425_24 = Coupling(name = 'R2GC_425_24',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_425_25 = Coupling(name = 'R2GC_425_25',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_426_26 = Coupling(name = 'R2GC_426_26',
                       value = '-0.006944444444444444*(ee*complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3,'QED':1})

R2GC_426_27 = Coupling(name = 'R2GC_426_27',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_427_28 = Coupling(name = 'R2GC_427_28',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_427_29 = Coupling(name = 'R2GC_427_29',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_428_30 = Coupling(name = 'R2GC_428_30',
                       value = '-0.005208333333333333*(cw*ee*complex(0,1)*G**3)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_428_31 = Coupling(name = 'R2GC_428_31',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_429_32 = Coupling(name = 'R2GC_429_32',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_429_33 = Coupling(name = 'R2GC_429_33',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_430_34 = Coupling(name = 'R2GC_430_34',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_430_35 = Coupling(name = 'R2GC_430_35',
                       value = '-0.003472222222222222*(ee**2*complex(0,1)*G**2)/cmath.pi**2 + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_431_36 = Coupling(name = 'R2GC_431_36',
                       value = '-0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_432_37 = Coupling(name = 'R2GC_432_37',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_434_38 = Coupling(name = 'R2GC_434_38',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yb**2)/(cmath.pi**2*(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vev**2*yt**2)/(16.*cmath.pi**2*(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_435_39 = Coupling(name = 'R2GC_435_39',
                       value = '(complex(0,1)*G**2*vev*vevD*yb**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (complex(0,1)*G**2*vev*vevD*yt**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':2})

R2GC_436_40 = Coupling(name = 'R2GC_436_40',
                       value = '-0.125*(complex(0,1)*G**2*vevD**2*yb**2)/(cmath.pi**2*(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vevD**2*yt**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_437_41 = Coupling(name = 'R2GC_437_41',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_440_42 = Coupling(name = 'R2GC_440_42',
                       value = '-0.015625*G**3/cmath.pi**2',
                       order = {'QCD':3})

R2GC_441_43 = Coupling(name = 'R2GC_441_43',
                       value = 'G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_442_44 = Coupling(name = 'R2GC_442_44',
                       value = '-0.005208333333333333*G**4/cmath.pi**2',
                       order = {'QCD':4})

R2GC_442_45 = Coupling(name = 'R2GC_442_45',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_443_46 = Coupling(name = 'R2GC_443_46',
                       value = '-0.005208333333333333*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_443_47 = Coupling(name = 'R2GC_443_47',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_444_48 = Coupling(name = 'R2GC_444_48',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_444_49 = Coupling(name = 'R2GC_444_49',
                       value = '-0.015625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_445_50 = Coupling(name = 'R2GC_445_50',
                       value = '-0.020833333333333332*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_446_51 = Coupling(name = 'R2GC_446_51',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_446_52 = Coupling(name = 'R2GC_446_52',
                       value = '-0.03125*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_447_53 = Coupling(name = 'R2GC_447_53',
                       value = '-0.0625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_448_54 = Coupling(name = 'R2GC_448_54',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_449_55 = Coupling(name = 'R2GC_449_55',
                       value = '-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3})

R2GC_453_56 = Coupling(name = 'R2GC_453_56',
                       value = '-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_458_57 = Coupling(name = 'R2GC_458_57',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_461_58 = Coupling(name = 'R2GC_461_58',
                       value = '(cxi*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_462_59 = Coupling(name = 'R2GC_462_59',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*sxi*yb)/(cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_463_60 = Coupling(name = 'R2GC_463_60',
                       value = '-0.3333333333333333*(G**2*vev*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_464_61 = Coupling(name = 'R2GC_464_61',
                       value = '(G**2*vevD*yb*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_465_62 = Coupling(name = 'R2GC_465_62',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_465_63 = Coupling(name = 'R2GC_465_63',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_466_64 = Coupling(name = 'R2GC_466_64',
                       value = '-0.0625*(complex(0,1)*G**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_467_65 = Coupling(name = 'R2GC_467_65',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_467_66 = Coupling(name = 'R2GC_467_66',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_468_67 = Coupling(name = 'R2GC_468_67',
                       value = '(5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_469_68 = Coupling(name = 'R2GC_469_68',
                       value = '(3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_470_69 = Coupling(name = 'R2GC_470_69',
                       value = '-0.041666666666666664*G**3/cmath.pi**2',
                       order = {'QCD':3})

R2GC_470_70 = Coupling(name = 'R2GC_470_70',
                       value = '(-5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_471_71 = Coupling(name = 'R2GC_471_71',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_472_72 = Coupling(name = 'R2GC_472_72',
                       value = '(-3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_473_73 = Coupling(name = 'R2GC_473_73',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_473_74 = Coupling(name = 'R2GC_473_74',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_474_75 = Coupling(name = 'R2GC_474_75',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_474_76 = Coupling(name = 'R2GC_474_76',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_476_77 = Coupling(name = 'R2GC_476_77',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_477_78 = Coupling(name = 'R2GC_477_78',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_482_79 = Coupling(name = 'R2GC_482_79',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_486_80 = Coupling(name = 'R2GC_486_80',
                       value = '(complex(0,1)*G**2*vev*yb)/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_487_81 = Coupling(name = 'R2GC_487_81',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*vevD*yb*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_488_82 = Coupling(name = 'R2GC_488_82',
                       value = '(cxi*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_489_83 = Coupling(name = 'R2GC_489_83',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*sxi*yt)/(cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_490_84 = Coupling(name = 'R2GC_490_84',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_491_85 = Coupling(name = 'R2GC_491_85',
                       value = '(complex(0,1)*G**2*vevD*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_492_86 = Coupling(name = 'R2GC_492_86',
                       value = '(G**2*vev*yt)/(3.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_493_87 = Coupling(name = 'R2GC_493_87',
                       value = '-0.3333333333333333*(G**2*vevD*yt*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

UVGC_439_1 = Coupling(name = 'UVGC_439_1',
                      value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_440_2 = Coupling(name = 'UVGC_440_2',
                      value = {-1:'(-11*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_440_3 = Coupling(name = 'UVGC_440_3',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_441_4 = Coupling(name = 'UVGC_441_4',
                      value = {-1:'(11*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_441_5 = Coupling(name = 'UVGC_441_5',
                      value = {-1:'G**3/(64.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_442_6 = Coupling(name = 'UVGC_442_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_442_7 = Coupling(name = 'UVGC_442_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_443_8 = Coupling(name = 'UVGC_443_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_443_9 = Coupling(name = 'UVGC_443_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_445_10 = Coupling(name = 'UVGC_445_10',
                       value = {-1:'-0.0078125*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_445_11 = Coupling(name = 'UVGC_445_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_446_12 = Coupling(name = 'UVGC_446_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_446_13 = Coupling(name = 'UVGC_446_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_447_14 = Coupling(name = 'UVGC_447_14',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_448_15 = Coupling(name = 'UVGC_448_15',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_449_16 = Coupling(name = 'UVGC_449_16',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_449_17 = Coupling(name = 'UVGC_449_17',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_449_18 = Coupling(name = 'UVGC_449_18',
                       value = {-1:'-0.0078125*(complex(0,1)*G**3)/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_449_19 = Coupling(name = 'UVGC_449_19',
                       value = {-1:'(-3*complex(0,1)*G**3)/(16.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_453_20 = Coupling(name = 'UVGC_453_20',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_453_21 = Coupling(name = 'UVGC_453_21',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_455_22 = Coupling(name = 'UVGC_455_22',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_456_23 = Coupling(name = 'UVGC_456_23',
                       value = {-1:'(ee*complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_457_24 = Coupling(name = 'UVGC_457_24',
                       value = {-1:'(-7*complex(0,1)*G**3)/(16.*cmath.pi**2)',0:'-0.3333333333333333*(complex(0,1)*G**3)/cmath.pi**2 + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_458_25 = Coupling(name = 'UVGC_458_25',
                       value = {-1:'(complex(0,1)*G**2*MB)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MB)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_459_26 = Coupling(name = 'UVGC_459_26',
                       value = {-1:'(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_460_27 = Coupling(name = 'UVGC_460_27',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',0:'-0.1111111111111111*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_461_28 = Coupling(name = 'UVGC_461_28',
                       value = {-1:'(cxi*complex(0,1)*G**2*yb)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(cxi*complex(0,1)*G**2*yb*cmath.sqrt(2))/(3.*cmath.pi**2) - (cxi*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_462_29 = Coupling(name = 'UVGC_462_29',
                       value = {-1:'-0.5*(complex(0,1)*G**2*sxi*yb)/(cmath.pi**2*cmath.sqrt(2))',0:'-0.3333333333333333*(complex(0,1)*G**2*sxi*yb*cmath.sqrt(2))/cmath.pi**2 + (complex(0,1)*G**2*sxi*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_463_30 = Coupling(name = 'UVGC_463_30',
                       value = {-1:'-0.5*(G**2*vev*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'-0.3333333333333333*(G**2*vev*yb*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) + (G**2*vev*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_464_31 = Coupling(name = 'UVGC_464_31',
                       value = {-1:'(G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'(2*G**2*vevD*yb*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) - (G**2*vevD*yb*cmath.sqrt(2)*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_465_32 = Coupling(name = 'UVGC_465_32',
                       value = {-1:'(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**2*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_465_33 = Coupling(name = 'UVGC_465_33',
                       value = {-1:'(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**2*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_466_34 = Coupling(name = 'UVGC_466_34',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**2)/cmath.pi**2',0:'(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_466_35 = Coupling(name = 'UVGC_466_35',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_466_36 = Coupling(name = 'UVGC_466_36',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_466_37 = Coupling(name = 'UVGC_466_37',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**2)/cmath.pi**2',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_467_38 = Coupling(name = 'UVGC_467_38',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-0.08333333333333333*(G**3*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_467_39 = Coupling(name = 'UVGC_467_39',
                       value = {-1:'-0.020833333333333332*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_467_40 = Coupling(name = 'UVGC_467_40',
                       value = {-1:'(39*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_467_41 = Coupling(name = 'UVGC_467_41',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-0.08333333333333333*(G**3*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_468_42 = Coupling(name = 'UVGC_468_42',
                       value = {-1:'(31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_468_43 = Coupling(name = 'UVGC_468_43',
                       value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_469_44 = Coupling(name = 'UVGC_469_44',
                       value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_469_45 = Coupling(name = 'UVGC_469_45',
                       value = {-1:'G**3/(32.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_470_46 = Coupling(name = 'UVGC_470_46',
                       value = {-1:'-0.041666666666666664*G**3/cmath.pi**2',0:'(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_470_47 = Coupling(name = 'UVGC_470_47',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_470_48 = Coupling(name = 'UVGC_470_48',
                       value = {-1:'(-31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_470_49 = Coupling(name = 'UVGC_470_49',
                       value = {-1:'-0.041666666666666664*G**3/cmath.pi**2',0:'(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_471_50 = Coupling(name = 'UVGC_471_50',
                       value = {-1:'(-45*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_471_51 = Coupling(name = 'UVGC_471_51',
                       value = {-1:'-0.015625*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_472_52 = Coupling(name = 'UVGC_472_52',
                       value = {-1:'(-53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_472_53 = Coupling(name = 'UVGC_472_53',
                       value = {-1:'-0.0078125*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_473_54 = Coupling(name = 'UVGC_473_54',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_473_55 = Coupling(name = 'UVGC_473_55',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_473_56 = Coupling(name = 'UVGC_473_56',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_473_57 = Coupling(name = 'UVGC_473_57',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_474_58 = Coupling(name = 'UVGC_474_58',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_474_59 = Coupling(name = 'UVGC_474_59',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_475_60 = Coupling(name = 'UVGC_475_60',
                       value = {0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_475_61 = Coupling(name = 'UVGC_475_61',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_475_62 = Coupling(name = 'UVGC_475_62',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_475_63 = Coupling(name = 'UVGC_475_63',
                       value = {0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_476_64 = Coupling(name = 'UVGC_476_64',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2',0:'(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_476_65 = Coupling(name = 'UVGC_476_65',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_476_66 = Coupling(name = 'UVGC_476_66',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_476_67 = Coupling(name = 'UVGC_476_67',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_476_68 = Coupling(name = 'UVGC_476_68',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_477_69 = Coupling(name = 'UVGC_477_69',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_477_70 = Coupling(name = 'UVGC_477_70',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_478_71 = Coupling(name = 'UVGC_478_71',
                       value = {0:'(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_478_72 = Coupling(name = 'UVGC_478_72',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_478_73 = Coupling(name = 'UVGC_478_73',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_478_74 = Coupling(name = 'UVGC_478_74',
                       value = {0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_479_75 = Coupling(name = 'UVGC_479_75',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_480_76 = Coupling(name = 'UVGC_480_76',
                       value = {-1:'-0.16666666666666666*(ee*complex(0,1)*G**2)/cmath.pi**2',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_481_77 = Coupling(name = 'UVGC_481_77',
                       value = {-1:'(-7*complex(0,1)*G**3)/(16.*cmath.pi**2)',0:'-0.3333333333333333*(complex(0,1)*G**3)/cmath.pi**2 + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_482_78 = Coupling(name = 'UVGC_482_78',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_483_79 = Coupling(name = 'UVGC_483_79',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',0:'-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_483_80 = Coupling(name = 'UVGC_483_80',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',0:'-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_484_81 = Coupling(name = 'UVGC_484_81',
                       value = {-1:'-0.125*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'-0.16666666666666666*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_485_82 = Coupling(name = 'UVGC_485_82',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(6.*cw*cmath.pi**2)',0:'(2*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_486_83 = Coupling(name = 'UVGC_486_83',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(12.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vev*yb)/(2.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) - (3*complex(0,1)*G**2*vev*yb*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_486_84 = Coupling(name = 'UVGC_486_84',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(12.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vev*yb)/(6.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vev*yb*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_486_85 = Coupling(name = 'UVGC_486_85',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_487_86 = Coupling(name = 'UVGC_487_86',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-((complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) + (3*complex(0,1)*G**2*vevD*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_487_87 = Coupling(name = 'UVGC_487_87',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.3333333333333333*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) + (complex(0,1)*G**2*vevD*yb*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_487_88 = Coupling(name = 'UVGC_487_88',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*vevD*yb*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_488_89 = Coupling(name = 'UVGC_488_89',
                       value = {-1:'(cxi*complex(0,1)*G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(cxi*complex(0,1)*G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (cxi*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_489_90 = Coupling(name = 'UVGC_489_90',
                       value = {-1:'-0.5*(complex(0,1)*G**2*sxi*yt)/(cmath.pi**2*cmath.sqrt(2))',0:'-0.3333333333333333*(complex(0,1)*G**2*sxi*yt*cmath.sqrt(2))/cmath.pi**2 + (complex(0,1)*G**2*sxi*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_490_91 = Coupling(name = 'UVGC_490_91',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.16666666666666666*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) + (complex(0,1)*G**2*vev*yt*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_490_92 = Coupling(name = 'UVGC_490_92',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.5*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) + (3*complex(0,1)*G**2*vev*yt*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_490_93 = Coupling(name = 'UVGC_490_93',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_491_94 = Coupling(name = 'UVGC_491_94',
                       value = {-1:'(complex(0,1)*G**2*vevD*yt)/(6.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vevD*yt)/(3.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vevD*yt*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_491_95 = Coupling(name = 'UVGC_491_95',
                       value = {-1:'(complex(0,1)*G**2*vevD*yt)/(6.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vevD*yt)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (3*complex(0,1)*G**2*vevD*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_491_96 = Coupling(name = 'UVGC_491_96',
                       value = {-1:'(complex(0,1)*G**2*vevD*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_492_97 = Coupling(name = 'UVGC_492_97',
                       value = {-1:'(G**2*vev*yt)/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'(G**2*vev*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) - (G**2*vev*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_493_98 = Coupling(name = 'UVGC_493_98',
                       value = {-1:'-((G**2*vevD*yt)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)))',0:'(-2*G**2*vevD*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) + (G**2*vevD*yt*cmath.sqrt(2)*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

