Requestor: Matthias Saimpert and Jonas Neundorf
Contents: HeavyN model for Majorana neutrinos at NLO
Source: http://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/HeavyN/SM_HeavyN_NLO_UFO.tgz
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/HeavyN
Paper1: http://arxiv.org/abs/0901.3589
Paper2: http://arxiv.org/abs/1411.7305
JIRA: https://its.cern.ch/jira/browse/AGENE-1820