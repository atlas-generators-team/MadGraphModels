Requestor: Lailin Xu, Zhiliang Chen
Content: Pseudo-Nambu-Goldstone Dark Matter model and Scalar DM model
Paper: https://arxiv.org/abs/1910.04170
Source: Ennio Salvioni (private communication)
Validation: https://its.cern.ch/jira/browse/ATLMCPROD-11551