Requestor: Priscilla Pani
Content: Scalar and pseudo-scalar dark matter models in heavy flavor final states
Paper: http://arxiv.org/abs/1410.6497
TWiki: https://twiki.cern.ch/twiki/bin/view/LHCDMF/ScalarModels
JIRA: https://its.cern.ch/jira/browse/AGENE-948
