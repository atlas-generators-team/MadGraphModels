# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Wed 5 Oct 2016 13:23:59



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

lam1 = Parameter(name = 'lam1',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{lam1}',
                 lhablock = 'FRBlock',
                 lhacode = [ 1 ])

lam2 = Parameter(name = 'lam2',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{lam2}',
                 lhablock = 'FRBlock',
                 lhacode = [ 2 ])

lam3 = Parameter(name = 'lam3',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{lam3}',
                 lhablock = 'FRBlock',
                 lhacode = [ 3 ])

thet12 = Parameter(name = 'thet12',
                   nature = 'external',
                   type = 'real',
                   value = 0.5,
                   texname = '\\text{thet12}',
                   lhablock = 'FRBlock',
                   lhacode = [ 4 ])

thet13 = Parameter(name = 'thet13',
                   nature = 'external',
                   type = 'real',
                   value = 0.5,
                   texname = '\\text{thet13}',
                   lhablock = 'FRBlock',
                   lhacode = [ 5 ])

thet23 = Parameter(name = 'thet23',
                   nature = 'external',
                   type = 'real',
                   value = 0.5,
                   texname = '\\text{thet23}',
                   lhablock = 'FRBlock',
                   lhacode = [ 6 ])

del12 = Parameter(name = 'del12',
                  nature = 'external',
                  type = 'real',
                  value = 1.5,
                  texname = '\\text{del12}',
                  lhablock = 'FRBlock',
                  lhacode = [ 7 ])

del13 = Parameter(name = 'del13',
                  nature = 'external',
                  type = 'real',
                  value = 1.5,
                  texname = '\\text{del13}',
                  lhablock = 'FRBlock',
                  lhacode = [ 8 ])

del23 = Parameter(name = 'del23',
                  nature = 'external',
                  type = 'real',
                  value = 1.5,
                  texname = '\\text{del23}',
                  lhablock = 'FRBlock',
                  lhacode = [ 9 ])

lambdaHphi = Parameter(name = 'lambdaHphi',
                       nature = 'external',
                       type = 'real',
                       value = 0.1,
                       texname = '\\text{lambdaHphi}',
                       lhablock = 'FRBlock',
                       lhacode = [ 10 ])

lambdaphiphi = Parameter(name = 'lambdaphiphi',
                         nature = 'external',
                         type = 'real',
                         value = 0.1,
                         texname = '\\text{lambdaphiphi}',
                         lhablock = 'FRBlock',
                         lhacode = [ 11 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Mphi = Parameter(name = 'Mphi',
                 nature = 'external',
                 type = 'real',
                 value = 850,
                 texname = '\\text{Mphi}',
                 lhablock = 'MASS',
                 lhacode = [ 53 ])

Mchiu = Parameter(name = 'Mchiu',
                  nature = 'external',
                  type = 'real',
                  value = 300,
                  texname = '\\text{Mchiu}',
                  lhablock = 'MASS',
                  lhacode = [ 9900022 ])

Mchic = Parameter(name = 'Mchic',
                  nature = 'external',
                  type = 'real',
                  value = 300,
                  texname = '\\text{Mchic}',
                  lhablock = 'MASS',
                  lhacode = [ 9900023 ])

Mchit = Parameter(name = 'Mchit',
                  nature = 'external',
                  type = 'real',
                  value = 300,
                  texname = '\\text{Mchit}',
                  lhablock = 'MASS',
                  lhacode = [ 9900025 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Wphi = Parameter(name = 'Wphi',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{Wphi}',
                 lhablock = 'DECAY',
                 lhacode = [ 53 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

lambda1x1 = Parameter(name = 'lambda1x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam1*cmath.cos(thet12)*cmath.cos(thet13)',
                      texname = '\\text{lambda1x1}')

lambda1x2 = Parameter(name = 'lambda1x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam2*cmath.cos(thet13)*cmath.exp(-(del12*complex(0,1)))*cmath.sin(thet12)',
                      texname = '\\text{lambda1x2}')

lambda1x3 = Parameter(name = 'lambda1x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam3*cmath.exp(-(del13*complex(0,1)))*cmath.sin(thet13)',
                      texname = '\\text{lambda1x3}')

lambda2x1 = Parameter(name = 'lambda2x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam1*(-(cmath.cos(thet23)*cmath.exp(del12*complex(0,1))*cmath.sin(thet12)) - cmath.cos(thet12)*cmath.exp((del13 - del23)*complex(0,1))*cmath.sin(thet13)*cmath.sin(thet23))',
                      texname = '\\text{lambda2x1}')

lambda2x2 = Parameter(name = 'lambda2x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam2*(cmath.cos(thet12)*cmath.cos(thet23) - cmath.exp((-del12 + del13 - del23)*complex(0,1))*cmath.sin(thet12)*cmath.sin(thet13)*cmath.sin(thet23))',
                      texname = '\\text{lambda2x2}')

lambda2x3 = Parameter(name = 'lambda2x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam3*cmath.cos(thet13)*cmath.exp(-(del23*complex(0,1)))*cmath.sin(thet23)',
                      texname = '\\text{lambda2x3}')

lambda3x1 = Parameter(name = 'lambda3x1',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam1*(-(cmath.cos(thet12)*cmath.cos(thet23)*cmath.exp(del13*complex(0,1))*cmath.sin(thet13)) + cmath.exp((del12 + del23)*complex(0,1))*cmath.sin(thet12)*cmath.sin(thet23))',
                      texname = '\\text{lambda3x1}')

lambda3x2 = Parameter(name = 'lambda3x2',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam2*(-(cmath.cos(thet23)*cmath.exp((-del12 + del13)*complex(0,1))*cmath.sin(thet12)*cmath.sin(thet13)) - cmath.cos(thet12)*cmath.exp(del23*complex(0,1))*cmath.sin(thet23))',
                      texname = '\\text{lambda3x2}')

lambda3x3 = Parameter(name = 'lambda3x3',
                      nature = 'internal',
                      type = 'complex',
                      value = 'lam3*cmath.cos(thet13)*cmath.cos(thet23)',
                      texname = '\\text{lambda3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1b33 = Parameter(name = 'I1b33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1b33}')

I2b33 = Parameter(name = 'I2b33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2b33}')

I3b33 = Parameter(name = 'I3b33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3b33}')

I4b33 = Parameter(name = 'I4b33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4b33}')

