# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.0.1 for Mac OS X x86 (64-bit) (September 27, 2016)
# Date: Mon 7 May 2018 12:23:38


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_133_1 = Coupling(name = 'R2GC_133_1',
                      value = '(3*G**3)/(16.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_134_2 = Coupling(name = 'R2GC_134_2',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_135_3 = Coupling(name = 'R2GC_135_3',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_136_4 = Coupling(name = 'R2GC_136_4',
                      value = '(complex(0,1)*G**2*gSu33*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'DMS':1,'QCD':2,'QED':1})

R2GC_137_5 = Coupling(name = 'R2GC_137_5',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_138_6 = Coupling(name = 'R2GC_138_6',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_142_7 = Coupling(name = 'R2GC_142_7',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_142_8 = Coupling(name = 'R2GC_142_8',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_143_9 = Coupling(name = 'R2GC_143_9',
                      value = '(complex(0,1)*G**2*gPd33*yb**2)/(16.*cmath.pi**2)',
                      order = {'DMS':1,'QCD':2,'QED':2})

R2GC_143_10 = Coupling(name = 'R2GC_143_10',
                       value = '-(complex(0,1)*G**2*gPu33*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_144_11 = Coupling(name = 'R2GC_144_11',
                       value = '(complex(0,1)*G**2*gSd33*yb**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_144_12 = Coupling(name = 'R2GC_144_12',
                       value = '(complex(0,1)*G**2*gSu33*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_145_13 = Coupling(name = 'R2GC_145_13',
                       value = '-(complex(0,1)*G**2*gPd33**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*gSd33**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'DMS':2,'QCD':2,'QED':2})

R2GC_145_14 = Coupling(name = 'R2GC_145_14',
                       value = '-(complex(0,1)*G**2*gPu33**2*yt**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*gSu33**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':2,'QCD':2,'QED':2})

R2GC_146_15 = Coupling(name = 'R2GC_146_15',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_146_16 = Coupling(name = 'R2GC_146_16',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_147_17 = Coupling(name = 'R2GC_147_17',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_147_18 = Coupling(name = 'R2GC_147_18',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_148_19 = Coupling(name = 'R2GC_148_19',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_148_20 = Coupling(name = 'R2GC_148_20',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_149_21 = Coupling(name = 'R2GC_149_21',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_149_22 = Coupling(name = 'R2GC_149_22',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_150_23 = Coupling(name = 'R2GC_150_23',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_150_24 = Coupling(name = 'R2GC_150_24',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_151_25 = Coupling(name = 'R2GC_151_25',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_151_26 = Coupling(name = 'R2GC_151_26',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_152_27 = Coupling(name = 'R2GC_152_27',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_153_28 = Coupling(name = 'R2GC_153_28',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_156_29 = Coupling(name = 'R2GC_156_29',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_30 = Coupling(name = 'R2GC_158_30',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_31 = Coupling(name = 'R2GC_158_31',
                       value = '(CKM2x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_32 = Coupling(name = 'R2GC_158_32',
                       value = '(CKM2x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_33 = Coupling(name = 'R2GC_158_33',
                       value = '(CKM1x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_34 = Coupling(name = 'R2GC_158_34',
                       value = '(CKM1x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_171_35 = Coupling(name = 'R2GC_171_35',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_171_36 = Coupling(name = 'R2GC_171_36',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_172_37 = Coupling(name = 'R2GC_172_37',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_172_38 = Coupling(name = 'R2GC_172_38',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_173_39 = Coupling(name = 'R2GC_173_39',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_173_40 = Coupling(name = 'R2GC_173_40',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_174_41 = Coupling(name = 'R2GC_174_41',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_175_42 = Coupling(name = 'R2GC_175_42',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_175_43 = Coupling(name = 'R2GC_175_43',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_176_44 = Coupling(name = 'R2GC_176_44',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_177_45 = Coupling(name = 'R2GC_177_45',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_178_46 = Coupling(name = 'R2GC_178_46',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_179_47 = Coupling(name = 'R2GC_179_47',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_181_48 = Coupling(name = 'R2GC_181_48',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_182_49 = Coupling(name = 'R2GC_182_49',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_183_50 = Coupling(name = 'R2GC_183_50',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_184_51 = Coupling(name = 'R2GC_184_51',
                       value = '-(G**2*gPd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_185_52 = Coupling(name = 'R2GC_185_52',
                       value = '-(complex(0,1)*G**2*gSd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_187_53 = Coupling(name = 'R2GC_187_53',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_204_54 = Coupling(name = 'R2GC_204_54',
                       value = '-(CKM2x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_205_55 = Coupling(name = 'R2GC_205_55',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_206_56 = Coupling(name = 'R2GC_206_56',
                       value = '-(CKM2x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_207_57 = Coupling(name = 'R2GC_207_57',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_208_58 = Coupling(name = 'R2GC_208_58',
                       value = '-(CKM1x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_209_59 = Coupling(name = 'R2GC_209_59',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_210_60 = Coupling(name = 'R2GC_210_60',
                       value = '-(CKM1x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_211_61 = Coupling(name = 'R2GC_211_61',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_212_62 = Coupling(name = 'R2GC_212_62',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_212_63 = Coupling(name = 'R2GC_212_63',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_213_64 = Coupling(name = 'R2GC_213_64',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_214_65 = Coupling(name = 'R2GC_214_65',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_215_66 = Coupling(name = 'R2GC_215_66',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_215_67 = Coupling(name = 'R2GC_215_67',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_216_68 = Coupling(name = 'R2GC_216_68',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_216_69 = Coupling(name = 'R2GC_216_69',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_218_70 = Coupling(name = 'R2GC_218_70',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_219_71 = Coupling(name = 'R2GC_219_71',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_224_72 = Coupling(name = 'R2GC_224_72',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_225_73 = Coupling(name = 'R2GC_225_73',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_228_74 = Coupling(name = 'R2GC_228_74',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_229_75 = Coupling(name = 'R2GC_229_75',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_230_76 = Coupling(name = 'R2GC_230_76',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_231_77 = Coupling(name = 'R2GC_231_77',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_232_78 = Coupling(name = 'R2GC_232_78',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_233_79 = Coupling(name = 'R2GC_233_79',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_234_80 = Coupling(name = 'R2GC_234_80',
                       value = '-(G**2*gPu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_235_81 = Coupling(name = 'R2GC_235_81',
                       value = '-(complex(0,1)*G**2*gSu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_159_1 = Coupling(name = 'UVGC_159_1',
                      value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_160_2 = Coupling(name = 'UVGC_160_2',
                      value = {-1:'G**3/(32.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_161_3 = Coupling(name = 'UVGC_161_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_162_4 = Coupling(name = 'UVGC_162_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_164_5 = Coupling(name = 'UVGC_164_5',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_171_6 = Coupling(name = 'UVGC_171_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_171_7 = Coupling(name = 'UVGC_171_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_172_8 = Coupling(name = 'UVGC_172_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_172_9 = Coupling(name = 'UVGC_172_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_174_10 = Coupling(name = 'UVGC_174_10',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_174_11 = Coupling(name = 'UVGC_174_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_175_12 = Coupling(name = 'UVGC_175_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_175_13 = Coupling(name = 'UVGC_175_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_176_14 = Coupling(name = 'UVGC_176_14',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_177_15 = Coupling(name = 'UVGC_177_15',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_178_16 = Coupling(name = 'UVGC_178_16',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_179_17 = Coupling(name = 'UVGC_179_17',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_180_18 = Coupling(name = 'UVGC_180_18',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_19 = Coupling(name = 'UVGC_180_19',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_20 = Coupling(name = 'UVGC_180_20',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_180_21 = Coupling(name = 'UVGC_180_21',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_180_22 = Coupling(name = 'UVGC_180_22',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_181_23 = Coupling(name = 'UVGC_181_23',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_182_24 = Coupling(name = 'UVGC_182_24',
                       value = {-1:'-(G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_183_25 = Coupling(name = 'UVGC_183_25',
                       value = {-1:'(complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_184_26 = Coupling(name = 'UVGC_184_26',
                       value = {-1:'-(G**2*gPd33*yb)/(4.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_185_27 = Coupling(name = 'UVGC_185_27',
                       value = {-1:'-(complex(0,1)*G**2*gSd33*yb)/(4.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_187_28 = Coupling(name = 'UVGC_187_28',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_204_29 = Coupling(name = 'UVGC_204_29',
                       value = {-1:'(CKM2x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_204_30 = Coupling(name = 'UVGC_204_30',
                       value = {-1:'-(CKM2x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_205_31 = Coupling(name = 'UVGC_205_31',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_205_32 = Coupling(name = 'UVGC_205_32',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_206_33 = Coupling(name = 'UVGC_206_33',
                       value = {-1:'(CKM2x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_206_34 = Coupling(name = 'UVGC_206_34',
                       value = {-1:'-(CKM2x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_207_35 = Coupling(name = 'UVGC_207_35',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_207_36 = Coupling(name = 'UVGC_207_36',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_208_37 = Coupling(name = 'UVGC_208_37',
                       value = {-1:'(CKM1x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_208_38 = Coupling(name = 'UVGC_208_38',
                       value = {-1:'-(CKM1x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_209_39 = Coupling(name = 'UVGC_209_39',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_209_40 = Coupling(name = 'UVGC_209_40',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_210_41 = Coupling(name = 'UVGC_210_41',
                       value = {-1:'(CKM1x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_210_42 = Coupling(name = 'UVGC_210_42',
                       value = {-1:'-(CKM1x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_211_43 = Coupling(name = 'UVGC_211_43',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_211_44 = Coupling(name = 'UVGC_211_44',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_212_45 = Coupling(name = 'UVGC_212_45',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_213_46 = Coupling(name = 'UVGC_213_46',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_213_47 = Coupling(name = 'UVGC_213_47',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_213_48 = Coupling(name = 'UVGC_213_48',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_214_49 = Coupling(name = 'UVGC_214_49',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_214_50 = Coupling(name = 'UVGC_214_50',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_215_51 = Coupling(name = 'UVGC_215_51',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_52 = Coupling(name = 'UVGC_215_52',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_53 = Coupling(name = 'UVGC_215_53',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_216_54 = Coupling(name = 'UVGC_216_54',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_216_55 = Coupling(name = 'UVGC_216_55',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_217_56 = Coupling(name = 'UVGC_217_56',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_217_57 = Coupling(name = 'UVGC_217_57',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_217_58 = Coupling(name = 'UVGC_217_58',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_218_59 = Coupling(name = 'UVGC_218_59',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_218_60 = Coupling(name = 'UVGC_218_60',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_218_61 = Coupling(name = 'UVGC_218_61',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_218_62 = Coupling(name = 'UVGC_218_62',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_219_63 = Coupling(name = 'UVGC_219_63',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_219_64 = Coupling(name = 'UVGC_219_64',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_65 = Coupling(name = 'UVGC_220_65',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_66 = Coupling(name = 'UVGC_220_66',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_67 = Coupling(name = 'UVGC_220_67',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_221_68 = Coupling(name = 'UVGC_221_68',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_222_69 = Coupling(name = 'UVGC_222_69',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_223_70 = Coupling(name = 'UVGC_223_70',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_224_71 = Coupling(name = 'UVGC_224_71',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_225_72 = Coupling(name = 'UVGC_225_72',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_225_73 = Coupling(name = 'UVGC_225_73',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_225_74 = Coupling(name = 'UVGC_225_74',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_226_75 = Coupling(name = 'UVGC_226_75',
                       value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_227_76 = Coupling(name = 'UVGC_227_76',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_228_77 = Coupling(name = 'UVGC_228_77',
                       value = {-1:'-(G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_228_78 = Coupling(name = 'UVGC_228_78',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_228_79 = Coupling(name = 'UVGC_228_79',
                       value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_229_80 = Coupling(name = 'UVGC_229_80',
                       value = {-1:'(G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_229_81 = Coupling(name = 'UVGC_229_81',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_229_82 = Coupling(name = 'UVGC_229_82',
                       value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_230_83 = Coupling(name = 'UVGC_230_83',
                       value = {-1:'-(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_230_84 = Coupling(name = 'UVGC_230_84',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_230_85 = Coupling(name = 'UVGC_230_85',
                       value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_86 = Coupling(name = 'UVGC_231_86',
                       value = {-1:'(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_87 = Coupling(name = 'UVGC_231_87',
                       value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_88 = Coupling(name = 'UVGC_231_88',
                       value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_232_89 = Coupling(name = 'UVGC_232_89',
                       value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_233_90 = Coupling(name = 'UVGC_233_90',
                       value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_234_91 = Coupling(name = 'UVGC_234_91',
                       value = {-1:'( -(G**2*gPu33*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*gPu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*gPu33*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*gPu33*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_235_92 = Coupling(name = 'UVGC_235_92',
                       value = {-1:'( -(complex(0,1)*G**2*gSu33*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*gSu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*complex(0,1)*G**2*gSu33*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gSu33*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

