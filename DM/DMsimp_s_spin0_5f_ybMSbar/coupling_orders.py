# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.0.1 for Mac OS X x86 (64-bit) (September 27, 2016)
# Date: Mon 7 May 2018 12:23:38


from object_library import all_orders, CouplingOrder


DMS = CouplingOrder(name = 'DMS',
                    expansion_order = 2,
                    hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

