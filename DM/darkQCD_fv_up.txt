Requestor: Danielle Wilson-Edwards
Content: t-channel mediator facilitating Yukawa-like couplings between SM up-type quarks and three generations of dark quarks 
Source:https://github.com/chscherb/t-channel_dark_QCD
Papers: https://arxiv.org/abs/1502.05409, https://arxiv.org/abs/1803.08080, https://arxiv.org/abs/2011.13990, https://arxiv.org/abs/2411.15073
