# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:23:13


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS11 = Lorentz(name = 'UUS11',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV14 = Lorentz(name = 'UUV14',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV15 = Lorentz(name = 'UUV15',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS11 = Lorentz(name = 'SSS11',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS22 = Lorentz(name = 'FFS22',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS23 = Lorentz(name = 'FFS23',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV43 = Lorentz(name = 'FFV43',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV44 = Lorentz(name = 'FFV44',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,2,-2)*Gamma(3,-2,1)')

FFV45 = Lorentz(name = 'FFV45',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*Gamma(-1,-2,1)*Gamma(3,2,-2)')

FFV46 = Lorentz(name = 'FFV46',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV47 = Lorentz(name = 'FFV47',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS11 = Lorentz(name = 'VSS11',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS11 = Lorentz(name = 'VVS11',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV11 = Lorentz(name = 'VVV11',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS11 = Lorentz(name = 'SSSS11',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

VVSS11 = Lorentz(name = 'VVSS11',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV51 = Lorentz(name = 'VVVV51',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV52 = Lorentz(name = 'VVVV52',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV53 = Lorentz(name = 'VVVV53',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV54 = Lorentz(name = 'VVVV54',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV55 = Lorentz(name = 'VVVV55',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

