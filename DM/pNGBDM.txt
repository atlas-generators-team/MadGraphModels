Requestor: Spyros Argyropoulos, Zirui Wang
Content: Pseudo-Nambu-Goldstone Dark Matter model
Paper: https://arxiv.org/pdf/2107.12389.pdf
Source: Uli Haisch (private communication)
Webpage: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DarkMatterInfo
